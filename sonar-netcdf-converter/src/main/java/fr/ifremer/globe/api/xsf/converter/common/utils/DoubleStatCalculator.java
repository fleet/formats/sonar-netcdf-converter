package fr.ifremer.globe.api.xsf.converter.common.utils;

/**
 * Specialized calculator ignoring NaN values
 * */
public class DoubleStatCalculator extends MinMaxCalculator<Double>{

	
	public DoubleStatCalculator() {
		super(Double.NaN);
	}

	/**
	 * 
	 * */
	@Override
	public void register(Double value)
	{
		if(!value.isNaN())
			super.register(value);
	}
	
	
}
