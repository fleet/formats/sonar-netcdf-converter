package fr.ifremer.globe.api.xsf.converter.all.datagram.seabed89;

import fr.ifremer.globe.api.xsf.converter.all.datagram.PingDatagram;
import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.*;

import java.nio.ByteBuffer;

public class Seabed89 extends PingDatagram {
    /**
     * {@inheritDoc}
     */
    @Override
    public Seabed89Metadata getSpecificMetadata(AllFile metadata) {
        return metadata.getSeabed89();
    }

    @Override
    protected void computeSpecificMetadata(AllFile metadata, ByteBuffer datagram, long epochTime, SwathIdentifier swathId,
                                           int serialNumber, DatagramPosition position) {
        Seabed89Metadata seabed89Metadata = metadata.getSeabed89();

        Seabed89CompletePingMetadata completing = seabed89Metadata.getPing(swathId);
        if (completing == null) {
            completing = new Seabed89CompletePingMetadata(metadata.getInstallation().getSerialNumbers());
            seabed89Metadata.addPing(swathId, completing);
        }


        int beamCount = getReceivedBeamsCounter(datagram).getU();
        int maxSampleCount = 0;
        for (int i = 0; i < beamCount; i++) {
            maxSampleCount = Math.max(maxSampleCount, getSampleCount(datagram, i).getU());
        }
        completing.addAntenna(serialNumber, new Seabed89CompleteAntennaPingMetadata(position, beamCount, maxSampleCount));
    }

    public static UShort getCounter(ByteBuffer datagram) {
        return TypeDecoder.read2U(datagram, 12);
    }

    public static UShort getSerialNumber(ByteBuffer datagram) {
        return TypeDecoder.read2U(datagram, 14);
    }

    public static FFloat getSamplingFn(ByteBuffer datagram) {
        return TypeDecoder.read4F(datagram, 16);
    }

    public static UShort getRangeToNormalIncidence(ByteBuffer datagram) {
        return TypeDecoder.read2U(datagram, 20);
    }

    public static SShort getNormalIncidenceBS(ByteBuffer datagram) {
        return TypeDecoder.read2S(datagram, 22);
    }

    public static SShort getObliqueBS(ByteBuffer datagram) {
        return TypeDecoder.read2S(datagram, 24);
    }

    public static UShort getTxBeamWidth(ByteBuffer datagram) {
        return TypeDecoder.read2U(datagram, 26);
    }

    public static UShort getTVGLawCrossOver(ByteBuffer datagram) {
        return TypeDecoder.read2U(datagram, 28);
    }

    public static UShort getReceivedBeamsCounter(ByteBuffer datagram) {
        return TypeDecoder.read2U(datagram, 30);
    }

    /**
     * START N entries Block
     */
    /**
     * return the given value
     *
     * @param datagram  the datagram buffer
     * @param beamIndex the index of valid beam index
     */
    public static SByte getSortingDirection(ByteBuffer datagram, int beamIndex) {
        return TypeDecoder.read1S(datagram, 32 + beamIndex * 6);
    }

    /**
     * return the given value
     *
     * @param datagram  the datagram buffer
     * @param beamIndex the index of valid beam index
     */
    public static UByte getDetectionInfo(ByteBuffer datagram, int beamIndex) {
        return TypeDecoder.read1U(datagram, 33 + beamIndex * 6);
    }


    /**
     * return the given value
     *
     * @param datagram  the datagram buffer
     * @param beamIndex the index of valid beam index
     */
    public static UShort getSampleCount(ByteBuffer datagram, int beamIndex) {
        return TypeDecoder.read2U(datagram, 34 + beamIndex * 6);
    }

    /**
     * return the given value
     *
     * @param datagram  the datagram buffer
     * @param beamIndex the index of valid beam index
     */
    public static SInt getCenterSample(ByteBuffer datagram, int beamIndex) {
        int center = TypeDecoder.read2U(datagram, 36 + beamIndex * 6).getU();
        // center number (in .all file) is the center index + 1
        if (getSortingDirection(datagram, beamIndex).get() > 0) {
            return new SInt(center);
        } else {
            return new SInt(getSampleCount(datagram, beamIndex).getU() - center + 1);
        }
    }


    public static short[] getAllSamples(ByteBuffer datagram) {
        int nSamples = 0;
        final int beamCount = getReceivedBeamsCounter(datagram).getU();

        for (int i = 0; i < beamCount; i++) {
            nSamples += getSampleCount(datagram, i).getU();
        }

        int sampleIndex = 0;
        int sampleOffset = 32 + 6 * beamCount;
        short[] ret = new short[nSamples];
        for (int i = 0; i < beamCount; i++) {
            int sampleCount = getSampleCount(datagram, i).getU();
            if (getSortingDirection(datagram, i).get() > 0) {
                for (int j = 0; j < sampleCount; j++) {
                    ret[sampleIndex++] = datagram.getShort(sampleOffset + 2 * j);
                }
            } else {
                for (int j = sampleCount - 1; j >= 0; j--) {
                    ret[sampleIndex++] = datagram.getShort(sampleOffset + 2 * j);
                }
            }
            sampleOffset += 2 * sampleCount;
        }
        return ret;
    }
}
