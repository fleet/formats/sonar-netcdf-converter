package fr.ifremer.globe.api.xsf.converter.common;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;

import fr.ifremer.globe.api.xsf.converter.MbgConverterParameters;
import fr.ifremer.globe.api.xsf.converter.SounderFileConverterParameters;
import fr.ifremer.globe.api.xsf.converter.XsfConverterParameters;
import fr.ifremer.globe.api.xsf.converter.common.exceptions.UnsupportedSounderException;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgBase;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgConstants;
import fr.ifremer.globe.api.xsf.converter.common.sounder.ISounderFile;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfBase;
import fr.ifremer.globe.api.xsf.util.Clock;
import fr.ifremer.globe.netcdf.ucar.NCException;

/**
 * Converter interface.
 */
public interface IConverter<T extends ISounderFile> {

	/**
	 * Sets logger.
	 */
	public Logger getLogger();

	/**
	 * Sets logger.
	 */
	public void setLogger(Logger logger);

	/**
	 * Applies conversion with file path parameters. (with overwriting)
	 */
	public default void convert(String inputFilePath, String outputFilePath ) throws NCException, IOException {
		if (outputFilePath.endsWith(XsfBase.EXTENSION_XSF))
			convertToXsf(new XsfConverterParameters(inputFilePath, outputFilePath, true,false));
		else if (outputFilePath.endsWith(MbgConstants.EXTENSION_MBG))
			convertToMbg(new MbgConverterParameters(inputFilePath, outputFilePath, true));
		else
			throw new IOException("Output file must be an .xsf.nc or .mbg");
	}

	/**
	 * @return true if the input file path can be converted.
	 */
	public boolean accept(String inputFilePath);

	/**
	 * Reads and indexes the input sounder file.
	 */
	public T indexFile(String inputFilePath, boolean parseWaterColumData) throws IOException,UnsupportedSounderException;

	/**
	 * @return {@link INcFileWriter} for XSF.
	 */
	public INcFileWriter<T, ? extends XsfBase, XsfConverterParameters> getXsfWriter();

	/**
	 * Performs a conversion to a new XSF file.
	 */
	public default void convertToXsf(XsfConverterParameters params) throws IOException, NCException {
		convert(getXsfWriter(), params);
	}

	/**
	 * @return {@link INcFileWriter} for MBG.
	 */
	public INcFileWriter<T, MbgBase, MbgConverterParameters> getMbgWriter();

	/**
	 * Performs a conversion to a new MBG file.
	 */
	public default void convertToMbg(MbgConverterParameters params) throws IOException, NCException {
		convert(getMbgWriter(), params);
	}

	/**
	 * Performs the conversion
	 * 
	 * @param <T> input file type
	 * @param <U> output netCDF file type
	 * @param <V> parameter type
	 */
	public default <U extends AutoCloseable, V extends SounderFileConverterParameters> void convert(
			INcFileWriter<T, U, V> netcdfFileWriter, V params) throws IOException {
		Clock clock = new Clock().start();
		getLogger().info("Start conversion...");
		params.log(getLogger());

		// Check output file
		File outputFile = new File(params.getOutFilePath());
		// xsf.nc and .nc are acceptable
		if (!outputFile.getName().endsWith(FilenameUtils.getExtension(params.getOutputFileExtension())))
			throw new IOException(String.format("Output file %s name not valid.", outputFile.getName()));
		if (outputFile.exists()) {
			if (!params.isOverwriteAllowed())
				throw new IOException("Output file " + outputFile.getCanonicalPath()
						+ " already exists (and overwrite not enabled).");
			if (outputFile.delete())
				getLogger().info("Existing file {} deleted.", outputFile.getName());
			else
				throw new IOException("Access denied to output file  : " + outputFile.getCanonicalPath());
		}

		// reads input file
		getLogger().info("Input file indexation... ");
		boolean readWaterColum = !(params instanceof MbgConverterParameters); // don't parse WC for MBG
		if(params.isIgnoreWaterColumn()) //check parameters if we need to skip wc datagrams
			readWaterColum=false;
		try (T stats = indexFile(params.getInputFilePath(), readWaterColum)) {

			// write output file
			getLogger().info("Write netcdf file... ");
			netcdfFileWriter.write(stats, params, getLogger());

			getLogger().info("Conversion to {} done in {}.", outputFile.getName(),
					clock.stop().getTimeElapsedAsString());
		} catch (Exception e) {
			getLogger().error("Error while converting file : {}", e.getMessage(), e);
			throw new IOException(e);
		}
	}

}
