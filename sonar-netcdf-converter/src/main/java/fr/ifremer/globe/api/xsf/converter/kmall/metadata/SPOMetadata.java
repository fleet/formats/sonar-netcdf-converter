package fr.ifremer.globe.api.xsf.converter.kmall.metadata;

import fr.ifremer.globe.api.xsf.converter.all.datagram.IndividualSensorMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.SensorMetadata;
import fr.ifremer.globe.api.xsf.util.types.UShort;

public class SPOMetadata extends SensorMetadata<UShort, IndividualSensorMetadata> {

	public double SOGMin = 99999;
	private double SOGMax = -99999f;

	/** Constructor **/
	public SPOMetadata() {
		super(new IndividualSensorMetadata());
	}

	public void computeSOG(float SOG) {
		SOGMin = Double.min(SOG, SOGMin);
		SOGMax = Double.max(SOG, SOGMax);
	}

	public double getSOGMin() {
		return SOGMin;
	}

	public double getSOGMax() {
		return SOGMax;
	}

}
