package fr.ifremer.globe.api.xsf.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import org.slf4j.Logger;

/***
 * simple util to measure and print time for benchmarks
 */
public class Clock {
	private long startTime;
	private long stopTime = -1;
	public String name = "";

	public Clock() {

	}

	public Clock(String clockName) {
		name = clockName;
	}

	public Clock start() {
		startTime = System.currentTimeMillis();
		return this;
	};

	public Clock stop() {
		if (startTime == -1) {
			throw new RuntimeException("Time counter was not started");
		}
		stopTime = System.currentTimeMillis();
		return this;
	};

	public void print(Logger logger) {
		String timeelapsed = new SimpleDateFormat("mm:ss:SSS").format(new Date(getTimeElapsed()));
		logger.info(name + " " + String.format("%4f (sec) %s (min:sec:msec)\n", getTimeElapsedInSec(), timeelapsed));
	}

	public long getTimeElapsed() {
		if (startTime == -1) {
			throw new RuntimeException("Time counter was not started");
		}
		if (stopTime == -1) {
			throw new RuntimeException("Time counter was not stopped");
		}
		long time = stopTime - startTime;
		return time;
	}

	public float getTimeElapsedInSec() {
		return getTimeElapsed() / 1000f;
	}
	
	public String getTimeElapsedAsString() {
		return new SimpleDateFormat("mm:ss:SSS").format(new Date(getTimeElapsed())) + " (min:sec:msec)";
	}

	public void println() {
		String timeelapsed = new SimpleDateFormat("mm:ss:SSS").format(new Date(getTimeElapsed()));
		System.out.println(
				name + " " + String.format("%4f (sec) %s (min:sec:msec)\n", getTimeElapsedInSec(), timeelapsed));
	}
}
