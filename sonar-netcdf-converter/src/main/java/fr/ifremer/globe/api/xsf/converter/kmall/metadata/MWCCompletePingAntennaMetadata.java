package fr.ifremer.globe.api.xsf.converter.kmall.metadata;

import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.AbstractCompleteAntennaPingSingleMetadata;

public class MWCCompletePingAntennaMetadata extends AbstractCompleteAntennaPingSingleMetadata {

    public MWCCompletePingAntennaMetadata(DatagramPosition position, int beamCount, int maxSampleCount) {
        super(position, beamCount, maxSampleCount);
    }

}
