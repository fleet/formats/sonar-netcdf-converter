package fr.ifremer.globe.api.xsf.converter.all.xsf.sensors;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.all.datagram.networkattitudevelocity.IndividualNetworkAttitudeVelocity;
import fr.ifremer.globe.api.xsf.converter.all.datagram.networkattitudevelocity.NetworkAttitudeVelocity;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.*;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfBase;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.NetworkAttitudeVelocity110Grp;
import fr.ifremer.globe.api.xsf.util.SimpleIdentifier;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.api.xsf.util.types.ULong;
import fr.ifremer.globe.api.xsf.util.types.UShort;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCGroup;
import fr.ifremer.globe.utils.date.DateUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Optional;

public class NetworkAttitudeVelocityGroup {

	private AllFile metadata;
	private short sensorId;
	private Optional<NetworkAttitudeVelocitySensorGroup> subgroups = Optional.empty();

	public NetworkAttitudeVelocityGroup(AllFile metadata, short sensorId) {
		this.metadata = metadata;
		this.sensorId = sensorId;
	}

	public void declare(AllFile allFile, XsfBase xsf, NCGroup parent) throws NCException {
		metadata.getNetworkAttitudeVelocity().forEachSensor((key, sensorMetadata) -> {
			if (key.getSecond() == sensorId) {
				subgroups = Optional.of(
						new NetworkAttitudeVelocitySensorGroup(key.getFirst(), key.getSecond(), sensorMetadata));
			}
		});

		if (subgroups.isPresent()) {
			subgroups.get().declare(allFile, xsf, parent);
		}
	}

	public void fillData(AllFile allFile) throws IOException, NCException {
		if (subgroups.isPresent()) {
			subgroups.get().fillData(allFile);
		}
	}

	private class NetworkAttitudeVelocitySensorGroup {
		long nEntries;
		private List<ValueD2> values;
		private IndividualNetworkAttitudeVelocity sensorMetadata;

		public NetworkAttitudeVelocitySensorGroup(UShort systemSerialNumber, Short sensorId,
				IndividualNetworkAttitudeVelocity sensorMetadata) {
			this.sensorMetadata = sensorMetadata;
			nEntries = sensorMetadata.getTotalNumberOfEntries();
			values = new LinkedList<>();
		}

		public void declare(AllFile allFile, XsfBase xsf, NCGroup parent) throws NCException {

			/**********************************************************************************************************
			 * xsf common variables
			 **********************************************************************************************************/
			// WE use Value2D to store data that will be effectively store in a 1D variable, values for some values will
			// be duplicated (like serial number and so on)

			// KongsbergVendorNetworkAttitude v = new KongsbergVendorNetworkAttitude(xsf, vendorHelper.getProxy(),
			// vendorGrp, String.format("network_attitude_velocity_%03d", sensorDescriptor.get(), nEntries),
			// nEntries);

			NetworkAttitudeVelocity110Grp network_subgroup = new NetworkAttitudeVelocity110Grp(parent, allFile,
					nEntries, new HashMap<String, Integer>());

			// declare all the variables
			values.add(new ValueD2U2(network_subgroup.getSystem_serial_number(), (int) nEntries,
					(buffer, i) -> NetworkAttitudeVelocity.getSerialNumber(buffer.byteBufferData)));

			values.add(new ValueD2U8(network_subgroup.getDatagram_time(), (int) nEntries, (buffer, i) -> new ULong(
					DateUtils.milliSecondToNano(NetworkAttitudeVelocity.getEpochTime(buffer.byteBufferData)
							+ NetworkAttitudeVelocity.getTimeSinceRecordStart(buffer.byteBufferData, i).getU()))));
			values.add(new ValueD2U2(network_subgroup.getAttitude_raw_count(), (int) nEntries,
					(buffer, i) -> NetworkAttitudeVelocity.getCounter(buffer.byteBufferData)));
			values.add(new ValueD2U1(network_subgroup.getSensor_status(), (int) nEntries,
					(buffer, i) -> NetworkAttitudeVelocity.getSensorDescriptor(buffer.byteBufferData)));

			values.add(new ValueD2F4(network_subgroup.getRoll(), (int) nEntries, (buffer, i) -> new FFloat(
					NetworkAttitudeVelocity.getRoll(buffer.byteBufferData, i).get() * 0.01f)));
			values.add(new ValueD2F4(network_subgroup.getPitch(), (int) nEntries, (buffer, i) -> new FFloat(
					NetworkAttitudeVelocity.getPitch(buffer.byteBufferData, i).get() * 0.01f)));
			values.add(new ValueD2F4(network_subgroup.getHeading(), (int) nEntries, (buffer, i) -> new FFloat(
					NetworkAttitudeVelocity.getHeading(buffer.byteBufferData, i).getU() * 0.01f)));

			values.add(new ValueD2F4(network_subgroup.getHeave(), (int) nEntries, (buffer, i) -> new FFloat(
					NetworkAttitudeVelocity.getHeave(buffer.byteBufferData, i).get() * 0.01f)));

			// DATA for NetworkAttitudeVelocity.getDataFromSensor are in a binary proprietary format, we do not keep it

		}

		public void fillData(AllFile allFile) throws IOException, NCException {

			DatagramBuffer buffer = new DatagramBuffer(metadata.getByteOrder());
			int count = 0;
			for (Entry<SimpleIdentifier, DatagramPosition> posentry : sensorMetadata.getDatagrams()) {
				final DatagramPosition pos = posentry.getValue();
				DatagramReader.readDatagramAt(pos.getFile(), buffer, pos.getSeek());
				long nEntries = NetworkAttitudeVelocity.getNumberEntries(buffer.byteBufferData).getU();

				// read the data from the source file
				int offset = NetworkAttitudeVelocity.getFirstInputOffset();
				for (int i = 0; i < nEntries; i++) {
					for (ValueD2 value : values) {
						value.fill(buffer, offset, count + i);
					}
					offset = NetworkAttitudeVelocity.getNextInputOffset(buffer.byteBufferData, offset);
				}
				count += nEntries;
			}

			// flush into the output file
			for (ValueD2 value : values) {
				value.write(new long[] { 0 }, new long[] { count });
			}
		}
	}

}
