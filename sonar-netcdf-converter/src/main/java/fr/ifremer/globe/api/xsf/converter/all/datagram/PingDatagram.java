package fr.ifremer.globe.api.xsf.converter.all.datagram;

import java.nio.ByteBuffer;

import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagram;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.UShort;

/**
 * base class for datagram having a ping and a serial number
 */
public abstract class PingDatagram extends BaseDatagram {

	/**
	 * compute and add specific metadata per datagram type
	 */
	@Override
	protected final void computeSpecificMetadata(AllFile metadata, ByteBuffer datagram, byte datagramType,
			long epochTime, DatagramPosition position) {
		UShort pingNumber = getPingNumber(datagram);
		UShort serialNumber = getSerialNumber(datagram);
		SwathIdentifier id = metadata.getGlobalMetadata().addSwath(pingNumber.getU(), epochTime);
		metadata.getGlobalMetadata().addSerial(serialNumber);
		metadata.getGlobalMetadata().addTime(epochTime);
		
		computeSpecificMetadata(metadata, datagram, epochTime, id, serialNumber.getU(), position);
	}

	public static UShort getPingNumber(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 12);

	}

	public static UShort getSerialNumber(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 14);
	}

	/**
	 * compute specific metadata per datagram, buffer is positionned just after the serial number field
	 */
	protected abstract void computeSpecificMetadata(AllFile metadata, ByteBuffer datagram, long epochTime,
			SwathIdentifier swathIdentifier, int serialNumber, DatagramPosition position);

}
