package fr.ifremer.globe.api.xsf.converter.s7k.metadata;

import java.util.Date;

import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K7030;

/**
 * This class contains information about installation parameters provided by one 7030 datagram.
 * 
 * IMPORTANT : S7K installation parameters are defined in the S7K coordinate system (Z : positive up, X : + starboard, -
 * port; and Y : + forward)
 */
public class S7K7030InstallationParameters {

	/** Origin of paramerters **/
	public enum Origin {
		FROM_S7K_FILE("S7K file"), FROM_XML_FILE("local XML file"), FROM_CONSTANTS("constants"), UNDEFINED("undefined");

		final String name;

		Origin(String name) {
			this.name = name;
		}

		@Override
		public String toString() {
			return name;
		}
	}

	private Origin origin = Origin.UNDEFINED;
	private String originFile = "undefined";

	/** Installation parameters **/
	@SuppressWarnings("unused") // used to serialize parameters in XML file
	private Date date;
	private int deviceId = -1;

	private float frequency = Float.NaN;
	private String firmwareVersion;
	private String softwareVersion;
	private String s7KSoftwareVersion;
	private String protocolVersionInfo;

	private float transmitArrayX = Float.NaN;
	private float transmitArrayY = Float.NaN;
	private float transmitArrayZ = Float.NaN;
	private float transmitArrayRoll = Float.NaN;
	private float transmitArrayPitch = Float.NaN;
	private float transmitArrayHeading = Float.NaN;

	private float receiveArrayX = Float.NaN;
	private float receiveArrayY = Float.NaN;
	private float receiveArrayZ = Float.NaN;
	private float receiveArrayRoll = Float.NaN;
	private float receiveArrayPitch = Float.NaN;
	private float receiveArrayHeading = Float.NaN;

	private float motionSensorX = Float.NaN;
	private float motionSensorY = Float.NaN;
	private float motionSensorZ = Float.NaN;
	private float motionSensorRollCalibration = Float.NaN;
	private float motionSensorPitchCalibration = Float.NaN;
	private float motionSensorHeadingCalibration = Float.NaN;
	private int motionSensorTimeDelay = -1;

	private float positionSensorX = Float.NaN;
	private float positionSensorY = Float.NaN;
	private float positionSensorZ = Float.NaN;
	private int positionSensorTimeDelay = -1;

	private float waterLineVerticalOffset = Float.NaN;

	/**
	 * Constructor : fills parameters with a 7030 {@link BaseDatagramBuffer}.
	 * 
	 * @param sk7030DatagramBuffer
	 */
	public S7K7030InstallationParameters(S7KFile metadata, BaseDatagramBuffer sk7030DatagramBuffer) {
		origin = Origin.FROM_S7K_FILE;
		originFile = metadata.getFilePath();
		date = new Date(S7K7030.getTimeMilli(sk7030DatagramBuffer));
		deviceId = S7K7030.getDeviceIdentifier(sk7030DatagramBuffer).getInt();

		frequency = S7K7030.getFrequency(sk7030DatagramBuffer).get();
		firmwareVersion = S7K7030.getFirmwareVersion(sk7030DatagramBuffer);
		softwareVersion = S7K7030.getSoftwareVersionInfo(sk7030DatagramBuffer);
		s7KSoftwareVersion = S7K7030.get7KSoftwareVersionInfo(sk7030DatagramBuffer);
		protocolVersionInfo = S7K7030.getRecordProtocolVersionInfo(sk7030DatagramBuffer);

		transmitArrayX = S7K7030.getTransmitArrayX(sk7030DatagramBuffer).get();
		transmitArrayY = S7K7030.getTransmitArrayY(sk7030DatagramBuffer).get();
		transmitArrayZ = S7K7030.getTransmitArrayZ(sk7030DatagramBuffer).get();
		transmitArrayPitch = S7K7030.getTransmitArrayPitch(sk7030DatagramBuffer).get();
		transmitArrayRoll = S7K7030.getTransmitArrayRoll(sk7030DatagramBuffer).get();
		transmitArrayHeading = S7K7030.getTransmitArrayHeading(sk7030DatagramBuffer).get();

		receiveArrayX = S7K7030.getReceiveArrayX(sk7030DatagramBuffer).get();
		receiveArrayY = S7K7030.getReceiveArrayY(sk7030DatagramBuffer).get();
		receiveArrayZ = S7K7030.getReceiveArrayZ(sk7030DatagramBuffer).get();
		receiveArrayPitch = S7K7030.getReceiveArrayPitch(sk7030DatagramBuffer).get();
		receiveArrayRoll = S7K7030.getReceiveArrayRoll(sk7030DatagramBuffer).get();
		receiveArrayHeading = S7K7030.getReceiveArrayHeading(sk7030DatagramBuffer).get();

		motionSensorX = S7K7030.getMotionSensorX(sk7030DatagramBuffer).get();
		motionSensorY = S7K7030.getMotionSensorY(sk7030DatagramBuffer).get();
		motionSensorZ = S7K7030.getMotionSensorZ(sk7030DatagramBuffer).get();
		motionSensorPitchCalibration = S7K7030.getMotionSensorPitchCalibration(sk7030DatagramBuffer).get();
		motionSensorRollCalibration = S7K7030.getMotionSensorRollCalibration(sk7030DatagramBuffer).get();
		motionSensorHeadingCalibration = S7K7030.getMotionSensorHeadingCalibration(sk7030DatagramBuffer).get();
		motionSensorTimeDelay = S7K7030.getMotionSensorTimeDelay(sk7030DatagramBuffer).getU();

		positionSensorX = S7K7030.getPositionSensorX(sk7030DatagramBuffer).get();
		positionSensorY = S7K7030.getPositionSensorY(sk7030DatagramBuffer).get();
		positionSensorZ = S7K7030.getPositionSensorZ(sk7030DatagramBuffer).get();
		positionSensorTimeDelay = S7K7030.getPositionSensorTimeDelay(sk7030DatagramBuffer).getU();

		waterLineVerticalOffset = S7K7030.getWaterLineVerticalOffset(sk7030DatagramBuffer).get();
	}

	/**
	 * @return all parameters in a {@link String} (as expected for MBG installation parameters).
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append("Freq=" + getFrequency() + ";");
		sb.append("FVI=" + getFirmwareVersion() + ";");
		sb.append("SVI=" + getSoftwareVersion() + ";");
		sb.append("S7KVI=" + getS7KSoftwareVersion() + ";");
		sb.append("RPI=" + getProtocolVersionInfo() + ";");

		sb.append("TAX=" + getTransmitArrayX() + ";");
		sb.append("TAY=" + getTransmitArrayY() + ";");
		sb.append("TAZ=" + getTransmitArrayZ() + ";");
		sb.append("TAR=" + getTransmitArrayRoll() + ";");
		sb.append("TAP=" + getTransmitArrayPitch() + ";");
		sb.append("TAD=" + getTransmitArrayHeading() + ";");

		sb.append("RAX=" + getReceiveArrayX() + ";");
		sb.append("RAY=" + getReceiveArrayY() + ";");
		sb.append("RAZ=" + getReceiveArrayZ() + ";");
		sb.append("RAR=" + getReceiveArrayRoll() + ";");
		sb.append("RAP=" + getReceiveArrayPitch() + ";");
		sb.append("RAD=" + getReceiveArrayHeading() + ";");

		sb.append("MSX=" + getMotionSensorX() + ";");
		sb.append("MSY=" + getMotionSensorY() + ";");
		sb.append("mSZ=" + getMotionSensorZ() + ";");
		sb.append("MSRC=" + getMotionSensorRollCalibration() + ";");
		sb.append("MSPC=" + getMotionSensorPitchCalibration() + ";");
		sb.append("MSHC=" + getMotionSensorHeadingCalibration() + ";");
		sb.append("MSTD=" + getMotionSensorTimeDelay() + ";");

		sb.append("PSX=" + getPositionSensorX() + ";");
		sb.append("PSY=" + getPositionSensorY() + ";");
		sb.append("PSZ=" + getPositionSensorZ() + ";");
		sb.append("PSTD=" + getPositionSensorTimeDelay() + ";");

		sb.append("WLVO=" + getWaterLineVerticalOffset() + ";");

		return sb.toString();
	}

	/***
	 * Getters
	 */
	public int getDeviceId() {
		return deviceId;
	}

	public float getFrequency() {
		return frequency;
	}

	public String getFirmwareVersion() {
		return firmwareVersion;
	}

	public String getSoftwareVersion() {
		return softwareVersion;
	}

	public String getS7KSoftwareVersion() {
		return s7KSoftwareVersion;
	}

	public String getProtocolVersionInfo() {
		return protocolVersionInfo;
	}

	public float getTransmitArrayX() {
		return transmitArrayX;
	}

	public float getTransmitArrayY() {
		return transmitArrayY;
	}

	public float getTransmitArrayZ() {
		return transmitArrayZ;
	}

	public float getTransmitArrayRoll() {
		return transmitArrayRoll;
	}

	public float getTransmitArrayPitch() {
		return transmitArrayPitch;
	}

	public float getTransmitArrayHeading() {
		return transmitArrayHeading;
	}

	public float getReceiveArrayX() {
		return receiveArrayX;
	}

	public float getReceiveArrayY() {
		return receiveArrayY;
	}

	public float getReceiveArrayZ() {
		return receiveArrayZ;
	}

	public float getReceiveArrayRoll() {
		return receiveArrayRoll;
	}

	public float getReceiveArrayPitch() {
		return receiveArrayPitch;
	}

	public float getReceiveArrayHeading() {
		return receiveArrayHeading;
	}

	public float getMotionSensorX() {
		return motionSensorX;
	}

	public float getMotionSensorY() {
		return motionSensorY;
	}

	public float getMotionSensorZ() {
		return motionSensorZ;
	}

	public float getMotionSensorRollCalibration() {
		return motionSensorRollCalibration;
	}

	public float getMotionSensorPitchCalibration() {
		return motionSensorPitchCalibration;
	}

	public float getMotionSensorHeadingCalibration() {
		return motionSensorHeadingCalibration;
	}

	public int getMotionSensorTimeDelay() {
		return motionSensorTimeDelay;
	}

	public float getPositionSensorX() {
		return positionSensorX;
	}

	public float getPositionSensorY() {
		return positionSensorY;
	}

	public float getPositionSensorZ() {
		return positionSensorZ;
	}

	public int getPositionSensorTimeDelay() {
		return positionSensorTimeDelay;
	}

	public float getWaterLineVerticalOffset() {
		return waterLineVerticalOffset;
	}

	public void setDeviceId(int deviceId) {
		this.deviceId = deviceId;
	}

	public Origin getOrigin() {
		return origin;
	}

	public void setOrigin(Origin origin) {
		this.origin = origin;
	}

	public String getOriginFile() {
		return originFile;
	}

	public void setOriginFile(String originFile) {
		this.originFile = originFile;
	}
}
