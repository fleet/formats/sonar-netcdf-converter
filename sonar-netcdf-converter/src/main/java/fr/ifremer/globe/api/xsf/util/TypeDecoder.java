package fr.ifremer.globe.api.xsf.util;

import java.nio.ByteBuffer;

import fr.ifremer.globe.api.xsf.util.types.DDouble;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.api.xsf.util.types.SByte;
import fr.ifremer.globe.api.xsf.util.types.SInt;
import fr.ifremer.globe.api.xsf.util.types.SShort;
import fr.ifremer.globe.api.xsf.util.types.UByte;
import fr.ifremer.globe.api.xsf.util.types.UInt;
import fr.ifremer.globe.api.xsf.util.types.ULong;
import fr.ifremer.globe.api.xsf.util.types.UShort;

/**
 * Utility to decode simrad types
 */
public class TypeDecoder {
	static public SByte read1S(ByteBuffer datagram, int positionInBuffer) {
		return new SByte(datagram.get(positionInBuffer));
	}

	static public UByte read1U(ByteBuffer datagram, int positionInBuffer) {
		return new UByte(datagram.get(positionInBuffer));
	}

	static public SShort read2S(ByteBuffer datagram, int positionInBuffer) {
		return new SShort(datagram.getShort(positionInBuffer));
	}

	static public UShort read2U(ByteBuffer datagram, int positionInBuffer) {
		return new UShort(datagram.getShort(positionInBuffer));
	}

	static public UInt read4U(ByteBuffer datagram, int positionInBuffer) {
		return new UInt(datagram.getInt(positionInBuffer));
	}
	
	static public ULong read8U(ByteBuffer datagram, int positionInBuffer) {
		return new ULong(datagram.getLong(positionInBuffer));
	}

	static public FFloat read4F(ByteBuffer datagram, int positionInBuffer) {
		return new FFloat(datagram.getFloat(positionInBuffer));
	}
	
	static public DDouble read8F(ByteBuffer datagram, int positionInBuffer) {
	    return new DDouble(datagram.getDouble(positionInBuffer));
	}
	
	static public SInt read4S(ByteBuffer datagram, int positionInBuffer) {
		return new SInt(datagram.getInt(positionInBuffer));
	}
	
}
