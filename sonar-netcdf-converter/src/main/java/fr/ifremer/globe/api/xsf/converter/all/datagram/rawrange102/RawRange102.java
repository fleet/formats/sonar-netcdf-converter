package fr.ifremer.globe.api.xsf.converter.all.datagram.rawrange102;

import java.nio.ByteBuffer;

import fr.ifremer.globe.api.xsf.converter.all.datagram.PingDatagram;
import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.api.xsf.util.types.SByte;
import fr.ifremer.globe.api.xsf.util.types.SShort;
import fr.ifremer.globe.api.xsf.util.types.UByte;
import fr.ifremer.globe.api.xsf.util.types.UInt;
import fr.ifremer.globe.api.xsf.util.types.UShort;

public class RawRange102 extends PingDatagram {
	/** {@inheritDoc} */
	@Override
	public RawRange102Metadata getSpecificMetadata(AllFile metadata) {
		return metadata.getRawRange102();
	}

	@Override
	protected void computeSpecificMetadata(AllFile metadata, ByteBuffer datagram, long epochTime, SwathIdentifier swathId,
			int serialNumber, DatagramPosition position) {

		RawRange102Metadata rawRange102Metadata = getSpecificMetadata(metadata);
		RawRange102CompletePingMetadata completeping = rawRange102Metadata.getPing(swathId);
		if (completeping == null) {
			completeping = new RawRange102CompletePingMetadata(
				getNTxTransmitSectorCounter(datagram).getU(),
				metadata.getInstallation().getSerialNumbers()
			);
			rawRange102Metadata.addPing(swathId, completeping);
		}
		
		completeping.addAntenna(serialNumber, new RawRange102CompleteAntennaPingMetadata(position, getTotalBeamCounter(datagram).getU()));
	}

	public static UShort getCounter(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 12);
	}

	public static UShort getSerialNumber(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 14);
	}

	public static UShort getNTxTransmitSectorCounter(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 16);
	}

	public static UShort getReceivedBeamCounter(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 18);
	}

	public static UInt getSamplingFn(ByteBuffer datagram) {
		return TypeDecoder.read4U(datagram, 20);
	}

	public static FFloat getROVDepth(ByteBuffer datagram) {
		return TypeDecoder.read4F(datagram, 24);
	}

	public static UShort getSoundSpeed(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 28);
	}

	public static UShort getTotalBeamCounter(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 30);
	}

	/**
	 * START TxSector Block
	 */
	/**
	 * return the given value
	 * 
	 * @param datagram
	 *            the datagram buffer
	 * @param txSectorIndex
	 *            the index of tx Sector
	 */
	// Spare 1 et 2 sur Deux octets chacun.
	public static SShort getTxTilt(ByteBuffer datagram, int txSectorIndex) {
		return TypeDecoder.read2S(datagram, 36 + txSectorIndex * 20);
	}
	
	/**
	 * return the given value
	 * 
	 * @param datagram
	 *            the datagram buffer
	 * @param txSectorIndex
	 *            the index of tx Sector
	 */
	public static UShort getFocusRange(ByteBuffer datagram, int txSectorIndex) {
		return TypeDecoder.read2U(datagram, 38 + txSectorIndex * 20);         
	}

	
	/**
	 * return the given value
	 * 
	 * @param datagram
	 *            the datagram buffer
	 * @param txSectorIndex
	 *            the index of tx Sector
	 */
	public static UInt getSignalLength(ByteBuffer datagram, int txSectorIndex) {   
		return TypeDecoder.read4U(datagram, 40 + txSectorIndex * 20);
	}		
	
	/**
	 * return the given value
	 * 
	 * @param datagram
	 *            the datagram buffer
	 * @param txSectorIndex
	 *            the index of tx Sector
	 */
	public static UInt getTransmitTimeOffset(ByteBuffer datagram, int txSectorIndex) {  
		return TypeDecoder.read4U(datagram, 44 + txSectorIndex * 20);         
	}	
	
	/**
	 * return the given value
	 * 
	 * @param datagram
	 *            the datagram buffer
	 * @param txSectorIndex
	 *            the index of tx Sector
	 */
	public static UInt getTxCenterFrequency(ByteBuffer datagram, int txSectorIndex) {  
		return TypeDecoder.read4U(datagram, 48 + txSectorIndex * 20);         
	}	
	
	/**
	 * return the given value
	 * 
	 * @param datagram
	 *            the datagram buffer
	 * @param txSectorIndex
	 *            the index of tx Sector
	 */
	public static UShort getSignalBandwidth(ByteBuffer datagram, int txSectorIndex) {  
		return TypeDecoder.read2U(datagram, 52 + txSectorIndex * 20);         
	}	
	
	/**
	 * return the given value
	 * 
	 * @param datagram
	 *            the datagram buffer
	 * @param txSectorIndex
	 *            the index of tx Sector
	 */
	public static UByte getWaveFormIdentifier(ByteBuffer datagram, int txSectorIndex) {  
		return TypeDecoder.read1U(datagram, 54 + txSectorIndex * 20);         
	}	
	
	/**
	 * return the given value
	 * 
	 * @param datagram
	 *            the datagram buffer
	 * @param txSectorIndex
	 *            the index of tx Sector
	 */
	public static UByte getTxSectorCounter(ByteBuffer datagram, int txSectorIndex) {  
		return TypeDecoder.read1U(datagram, 55 + txSectorIndex * 20);         
	}	

	
	/**
	 * START Entries Block (Samples)
	 */	
	public static int getStartingRxOffset(ByteBuffer datagram) {
	    int nTx = getNTxTransmitSectorCounter(datagram).getU();
	    return 36 + 20 * nTx;
	}
	
	public static int getBeamOffset(int startRxOffset, int idx) {
	    return startRxOffset + 12 * idx;
	}
	

	/**
	 * return the given value
	 * 
	 * @param datagram
	 *            the datagram buffer
	 * @param startingOffset
	 *            the starting offset of the considered rxBeam
	 */
	public static SShort getBeamPointingAngle(ByteBuffer datagram, int startingOffset) {
		return TypeDecoder.read2S(datagram, startingOffset);
	}

	/**
	 * return the given value
	 * 
	 * @param datagram
	 *            the datagram buffer
	 * @param startingOffset
	 *            the starting offset of the considered rxBeam
	 */
	public static UShort getRange(ByteBuffer datagram, int startingOffset) {
		return TypeDecoder.read2U(datagram, startingOffset + 2);
	}

	/**
	 * return the given value
	 * 
	 * @param datagram
	 *            the datagram buffer
	 * @param startingOffset
	 *            the starting offset of the considered rxBeam
	 */
	public static UByte getTxSectorIndex(ByteBuffer datagram, int startingOffset) {
		return TypeDecoder.read1U(datagram, startingOffset + 4);
	}

	/**
	 * return the given value
	 * 
	 * @param datagram
	 *            the datagram buffer
	 * @param startingOffset
	 *            the starting offset of the considered rxBeam
	 */
	public static SByte getReflectivity(ByteBuffer datagram, int startingOffset) {
		return TypeDecoder.read1S(datagram, startingOffset + 5);
	}

	/**
	 * return the given value
	 * 
	 * @param datagram
	 *            the datagram buffer
	 * @param startingOffset
	 *            the starting offset of the considered rxBeam
	 */
	public static UByte getQualityFactor(ByteBuffer datagram, int startingOffset) {
		return TypeDecoder.read1U(datagram, startingOffset + 6);
	}

	/**
	 * return the given value
	 * 
	 * @param datagram
	 *            the datagram buffer
	 * @param startingOffset
	 *            the starting offset of the considered rxBeam
	 */
	public static UByte getDetectionLength(ByteBuffer datagram, int startingOffset) {
		return TypeDecoder.read1U(datagram,  startingOffset + 7);
	}

	/**
	 * return the given value
	 * 
	 * @param datagram
	 *            the datagram buffer
	 * @param startingOffset
	 *            the starting offset of the considered rxBeam
	 */
	public static SShort getBeamIndex(ByteBuffer datagram, int startingOffset) {
		return TypeDecoder.read2S(datagram,  startingOffset + 8);
	}

	/**
	 * return the given value
	 * 
	 * @param datagram
	 *            the datagram buffer
	 * @param startingOffset
	 *            the starting offset of the considered rxBeam
	 */
	public static SShort getSpare(ByteBuffer datagram, int startingOffset) {
		return TypeDecoder.read2S(datagram,  startingOffset + 10);
	}




}
