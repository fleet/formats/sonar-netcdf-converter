package fr.ifremer.globe.api.xsf.converter.s7k.exceptions;

import java.io.IOException;

public class FragmentedDatagramException extends IOException {

	public FragmentedDatagramException(String string) {
		super(string);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}