package fr.ifremer.globe.api.xsf.util;

import fr.ifremer.globe.netcdf.util.Pair;

public class ComparablePair<T1 extends Comparable<T1>, T2 extends Comparable<T2>> extends Pair<T1, T2> implements Comparable<ComparablePair<T1, T2>> {

	public ComparablePair(T1 first, T2 second) {
		super(first, second);
	}

	@Override
	public int compareTo(ComparablePair<T1, T2> o) {
		int firstCmp = getFirst().compareTo(o.getFirst());
		if (firstCmp == 0) {
			return getSecond().compareTo(o.getSecond());
		} else {
			return firstCmp;
		}
	}

}
