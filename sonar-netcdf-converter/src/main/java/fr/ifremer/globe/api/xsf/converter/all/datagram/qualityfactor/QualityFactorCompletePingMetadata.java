package fr.ifremer.globe.api.xsf.converter.all.datagram.qualityfactor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.all.datagram.PingDatagram;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.util.ObjectBufferPool;
import fr.ifremer.globe.api.xsf.util.types.UShort;
import fr.ifremer.globe.netcdf.util.Pair;

/**
 * Bathymetry ping datagram can be stored in two parts, one per antenna
 * <p>
 * This class represents a complete ping metadata, ie the two datagrams
 */
public class QualityFactorCompletePingMetadata {
	private TreeMap<Integer, QualityFactorCompleteAntennaPingMetadata> map;
	private int beamCount;
	private int paramCount;
	
	
	public QualityFactorCompletePingMetadata() {
	    map = new TreeMap<>();
	    beamCount = 0;
	    paramCount = 0;
	}

	/**
	 * Register the received data for an antenna.
	 * 
	 * @param antenna the systemserialnumber of the antenna
	 * @param pos position of the datagram
	 * @param beamCount number of beams in datagram
	 * @param paramCount number of params in datagram
	 */
	public void addAntenna(int antenna, DatagramPosition pos, int beamCount, int paramCount) {
		if(!map.containsKey(antenna)) //datagram could be stored twice: once in .All files and the other one in .wcd files
		{
			map.put(antenna, new QualityFactorCompleteAntennaPingMetadata(pos, beamCount, paramCount));
			this.beamCount += beamCount;
			this.paramCount += paramCount;
		}
	}

	/**
	 * Get the number of beams for this ping
	 */
	public int getBeamCount() {
		return this.beamCount;
	}

	/**
	 * Get the number of params for this ping
	 */
	public int getParamCount() {
	    return this.paramCount;
	}
	
	/**
	 * Compute the overall index of a beam across multiple antennas
	 * @param serial
	 * @param beamIdxInDgm
	 * @return
	 */
	public int getBeamNumber(int serial, int beamIdxInDgm) {
	    int idx = beamIdxInDgm;
	    for(int antenna : map.keySet()) {
	        if (antenna == serial) {
	            break;
	        }
	        idx += map.get(antenna).getBeamCount();
	    }
	    return idx;
	}
	
	/**
	 * Given the serial number, tells how much preciding params are present in the ping
	 * 
	 * @param serial
	 * @return
	 */
	public int getParamOffset(int serial) {
	    int offset = 0;
	    for (int antenna : map.keySet()) {
	        if (antenna == serial) {
	            return offset;
	        }
	        offset += map.get(antenna).getParamCount();
	    }
	    return offset;
	}

	public List<Pair<DatagramBuffer, QualityFactorCompleteAntennaPingMetadata>> read(
			ObjectBufferPool<DatagramBuffer> pool) throws IOException {
		ArrayList<Pair<DatagramBuffer, QualityFactorCompleteAntennaPingMetadata>> result = new ArrayList<>();
		// for each antenna
		for (QualityFactorCompleteAntennaPingMetadata antennaPing : map.values()) {
			DatagramBuffer buffer = pool.borrow();
			DatagramReader.readDatagramAt(antennaPing.position.getFile(), buffer, antennaPing.position.getSeek());
			result.add(new Pair<DatagramBuffer, QualityFactorCompleteAntennaPingMetadata>(buffer, antennaPing));
		}
		return result;
	}

	public UShort getPingNumber(List<Pair<DatagramBuffer, QualityFactorCompleteAntennaPingMetadata>> data) {
		DatagramBuffer datagram = data.get(0).getFirst();
		return PingDatagram.getPingNumber(datagram.byteBufferData);
	}

	/**
	 * compute completeness for this ping, ie all datagram have been received
	 */
	public boolean isComplete(Set<Integer> allSNs) {
		boolean result = true;

		for (int sn : allSNs) {
			result &= map.containsKey(sn);
		}
		
		return result;
	}
}
