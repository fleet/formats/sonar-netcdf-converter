package fr.ifremer.globe.api.xsf.converter.common.utils.values.d2;

import java.util.Arrays;

import fr.ifremer.globe.api.xsf.converter.all.datagram.position.PositionMetadata;
import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCVariable;

public class ValueD2S4WithPosition extends ValueD2WithPosition {
	protected int[] dataOut;
	private ValueProvider valueProvider;
	
	public ValueD2S4WithPosition(NCVariable variable, ValueProvider filler) {
		super(variable);
		
		// allocate storage
		this.dataOut = new int[dim];
		this.valueProvider = filler;
	}
	
	@Override
	public void clear() {
		Arrays.fill(this.dataOut, variable.getIntFillValue());
	}
	
	/**
	 * Here the lambda expression will expect latitude and longitude data in addition of the input buffer
	 */
	public interface ValueProvider {
		public int get(BaseDatagramBuffer buffer, int beamIndexSource, double lat, double lon, double speed, PositionMetadata posMetadata);
	}

	@Override
	public void fill(BaseDatagramBuffer buffer, int beamIndexSource, int beamIndexDest, double lat, double lon,
			double speed, PositionMetadata posMetadata) {
		dataOut[beamIndexDest] = valueProvider.get(buffer, beamIndexSource, lat, lon, speed, posMetadata);
	}
	


	@Override
	public void write(long[] origin, long[] count) throws NCException {
		try {
			variable.put(origin, count, dataOut);
		} catch (Exception e) {
			logger.error(String.format("Error while writing variable %s (%s)", variable.getName(), e.getMessage()));
			throw e;

		}
	}

}
