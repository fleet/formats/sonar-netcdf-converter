package fr.ifremer.globe.api.xsf.converter.s7k.metadata;

import java.util.Set;
import java.util.TreeSet;

import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.DatagramMetadata;

public class S7KUnknownDatagramMetadata extends DatagramMetadata<Integer, DatagramPosition> {

	private Set<Long> unknownDatgrams;
	
	public S7KUnknownDatagramMetadata() {
		unknownDatgrams = new TreeSet<>();
	}
	
	public void pushUnknownType(long type) {
		unknownDatgrams.add(type);
	}
	
	@Override
	public String toString() {
		StringBuilder bld = new StringBuilder();
		bld.append(this.getClass().getSimpleName());
		for (Long dg : unknownDatgrams) {
			bld.append(System.lineSeparator() + "\t" + Long.toString(dg));
		}
		return bld.toString();
	}
}
