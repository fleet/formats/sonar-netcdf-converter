package fr.ifremer.globe.api.xsf.converter;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;

import fr.ifremer.globe.api.xsf.converter.all.KongsbergAllConverter;
import fr.ifremer.globe.api.xsf.converter.common.IConverter;
import fr.ifremer.globe.api.xsf.converter.common.sounder.ISounderFile;
import fr.ifremer.globe.api.xsf.converter.kmall.KongsbergKmAllConverter;
import fr.ifremer.globe.api.xsf.converter.s7k.S7KConverter;
import fr.ifremer.globe.api.xsf.util.DefaultLogger;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.soundingsfileconverter.BuildConfig;

/**
 * Main converter of sounder file (.all, .kmall, .s7k) to netcdf files (.mbg, .xsf.nc)
 */
public class SounderFileConverter {

	public static final String NAME = BuildConfig.APP_NAME;
	public static final String VERSION = BuildConfig.APP_VERSION;

	/**
	 * List of available converters
	 */
	private final List<IConverter<? extends ISounderFile>> converters = List.of(//
			new KongsbergAllConverter(), // .all
			new KongsbergKmAllConverter(), // .kmall
			new S7KConverter()); // .s7k

	/**
	 * Constructor
	 */
	public SounderFileConverter() {
	}

	/**
	 * Constructor with {@link Logger}.
	 */
	public SounderFileConverter(Logger logger) {
		setLogger(logger);
	}

	public void setLogger(Logger logger) {
		DefaultLogger.setLogger(logger);
		converters.forEach(converter -> converter.setLogger(logger));
	}

	/**
	 * @return true if input file path can be converted by at least one of available converters.
	 */
	public boolean accept(String inputFilePath) {
		return converters.stream().anyMatch(converter -> converter.accept(inputFilePath));
	}

	/**
	 * @return the suitable converter for the specified input file.
	 */
	private IConverter<? extends ISounderFile> getConverter(String inputFilePath) throws IOException {
		return converters.stream().filter(c -> c.accept(inputFilePath)).findFirst() //
				.orElseThrow(() -> new IOException("File " + inputFilePath + " can't be converted."));
	}

	/**
	 * Finds a converter which accepts the input file & performs the conversion to XSF.
	 */
	public void convertToXsf(XsfConverterParameters params) throws IOException, NCException {
		getConverter(params.getInputFilePath()).convertToXsf(params);
	}

	/**
	 * Finds a converter which accepts the input file & performs the conversion to MBG.
	 */
	public void convertToMbg(MbgConverterParameters params) throws IOException, NCException {
		getConverter(params.getInputFilePath()).convertToMbg(params);
	}

}
