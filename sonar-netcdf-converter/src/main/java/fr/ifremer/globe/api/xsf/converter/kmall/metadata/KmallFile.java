package fr.ifremer.globe.api.xsf.converter.kmall.metadata;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import fr.ifremer.globe.api.xsf.converter.common.exceptions.UnexpectedEndOfFile;
import fr.ifremer.globe.api.xsf.util.DefaultLogger;
import org.apache.commons.io.IOUtils;

import fr.ifremer.globe.api.xsf.converter.all.datagram.installation.InstallationMetadata.AntennaType;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.sounder.IKongsbergFile;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.DatagramParser;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.SwathIndexContainer;
import fr.ifremer.globe.api.xsf.util.SwathIndexGenerator;

public class KmallFile implements IKongsbergFile {

	public static final String EXTENSION_KMALL = ".kmall";
	public static final String EXTENSION_KMALL_WC = ".kmwcd";

	private final String filePath;
	private final List<RandomAccessFile> openedFiles = new LinkedList<>();

	private final Set<Integer> allFans = new TreeSet<>();
	private final SwathIndexGenerator swathGenerator = new SwathIndexGenerator();
	private final SwathIndexContainer retained_ids = new SwathIndexContainer();
	private final UnknownDatagramMetadata unkmownMetadata = new UnknownDatagramMetadata();
	private final IIPMetadata iip = new IIPMetadata();
	private final IOPMetadata iop = new IOPMetadata();
	private final MWCMetadata mwc = new MWCMetadata();
	private final MRZMetadata mrz = new MRZMetadata();
	private final SCLMetadata scl = new SCLMetadata();
	private final SPOMetadata spo = new SPOMetadata();
	private final SVPMetadata svp = new SVPMetadata();
	private final SKMMetadata skm = new SKMMetadata();
	private final SHIMetadata shi = new SHIMetadata();
	private final SDEMetadata sde = new SDEMetadata();
	private final GlobalMetadata globalMetadata = new GlobalMetadata();

	/**
	 * Default constructor (parse water column data)
	 * 
	 * @throws IOException
	 */
	public KmallFile(String filePath) throws IOException {
		this(filePath, true);
	}
	
	private void doRead(RandomAccessFile raf, DatagramParser parser) throws IOException {
		openedFiles.add(raf);
		long offsetInFile = 0;
		int byteRead = 0;
		DatagramBuffer buffer = new DatagramBuffer();
		try {
			while ((byteRead = DatagramReader.readDatagram(raf, buffer)) > 0) {
				parser.computeMetadata(this, buffer.byteBufferData, new DatagramPosition(raf, offsetInFile));
				offsetInFile += byteRead;
				if (offsetInFile != raf.getFilePointer()) {
					throw new IOException();
				}
			}
		} catch (UnexpectedEndOfFile e) {
			// invalid datagram (error with end flag) : error logged, but the conversion can be try with valid datagrams
			DefaultLogger.get().error("Invalid datagram at {} : {} ", offsetInFile, e.getMessage(), e);
			DefaultLogger.get().warn("Try conversion with valid datagrams...");
		}
	}

	/**
	 * Constructor
	 */
	public KmallFile(String filePath, boolean readWaterColum) throws IOException {
		DatagramParser parser = new DatagramParser(readWaterColum);
		this.filePath = filePath;

		// read and index file
		doRead( new RandomAccessFile(filePath, "r"), parser);
		// read and index file
		String waterColumnFilePath = filePath.replace(EXTENSION_KMALL, EXTENSION_KMALL_WC);
		//Read WC file only if readWaterColumn is set
		if (readWaterColum && new File(waterColumnFilePath).exists())
		{
			doRead(new RandomAccessFile(waterColumnFilePath, "r"), parser);
		}

		index();
	}

	@Override
	public void close() {
		openedFiles.forEach(IOUtils::closeQuietly);
	}

	public SwathIndexContainer getSwathIds() {
		return retained_ids;
	}

	public void addRxFan(int fanIdx) {
		allFans.add(fanIdx);
	}

	public SwathIdentifier getSwathId(int rawPingNum, int swathPerPing, int swathAlongPosition, long time) {
		return swathGenerator.addId(rawPingNum, swathPerPing, swathAlongPosition, time);
	}

	@Override
	public int getSwathCount() {
		return retained_ids.getIndexSize();
	}

	public int getRxAntennaCount() {
		return allFans.size();
	}

	public Set<Integer> getRxAntennas() {
		return allFans;
	}

	public UnknownDatagramMetadata getUnknown() {
		return unkmownMetadata;
	}

	public GlobalMetadata getGlobalMetadata() {
		return globalMetadata;
	}

	public IIPMetadata getIIPMetadata() {
		return iip;
	}

	public IOPMetadata getIOPMetadata() {
		return iop;
	}

	public MWCMetadata getMWCMetadata() {
		return mwc;
	}

	public MRZMetadata getMRZMetadata() {
		return mrz;
	}

	public SCLMetadata getSCLMetadata() {
		return scl;
	}

	public SPOMetadata getSPOMetadata() {
		return spo;
	}

	public SVPMetadata getSVPMetadata() {
		return svp;
	}

	public SKMMetadata getSKMMetadata() {
		return skm;
	}

	public SHIMetadata getSHIMetadata() {
		return shi;
	}

	public SDEMetadata getSDEMetadata() {
		return sde;
	}

	/**
	 * return true if the ping is not rejected
	 */
	private boolean isPingKept(SwathIdentifier is) {
		boolean hasBathyMetry = getMRZMetadata().contains(is) && getMRZMetadata().getPing(is).isComplete(allFans);
		boolean hasWaterColumn = getMWCMetadata().contains(is) && getMWCMetadata().getPing(is).isComplete(allFans);
		return hasBathyMetry || hasWaterColumn;
	}

	public void index() {
		retained_ids.generateIndex(this.swathGenerator, id -> this.isPingKept(id));

		mwc.computeIndex(allFans);
		mrz.computeIndex(allFans);
		iop.getGenerator().generateIndex();
	}

	@Override
	public String getFilePath() {
		return filePath;
	}

	@Override
	public int getModelNumberCode() {
		return getGlobalMetadata().getModelNumber();
	}

	@Override
	public String getModelNumber() {
		return getIIPMetadata().getModel();
	}

	@Override
	public int getDetectionCount() {
		return getMRZMetadata().getMaxBeamCount();
	}

	@Override
	public int getTxSectorCount() {
		if (getMRZMetadata().datagramCount == 0) {
			return getMWCMetadata().getTxSectorCount();
		}
		return getMRZMetadata().getTxSectorCount();
	}

	@Override
	public int getWcBeamCount() {
		return getMWCMetadata().getMaxBeamCount();
	}

	@Override
	public int getMaxSeabedSampleCount() {
		return getMRZMetadata().getMaxAbstractSampleCount();
	}

	@Override
	public int getMaxSeabedExtraSampleCount() {
		return 0;
	}

	@Override
	public int getRxAntennaNb() {
		return getIIPMetadata().getTrays().stream().filter(t -> t.type == AntennaType.RX).toArray().length;
	}

	@Override
	public int getTxAntennaNb() {
		return getIIPMetadata().getTrays().stream().filter(t -> t.type == AntennaType.TX).toArray().length;
	}

	@Override
	public int getAntennaNb() {
		return getIIPMetadata().getTrays().size();
	}

	@Override
	public long getMinDate() {
		return getGlobalMetadata().getMinDate();
	}

	@Override
	public long getMaxDate() {
		return getGlobalMetadata().getMaxDate();
	}

	@Override
	public double getLatMin() {
		return getMRZMetadata().getLatMin();
	}

	@Override
	public double getLatMax() {
		return getMRZMetadata().getLatMax();
	}

	@Override
	public double getLonMin() {
		return getMRZMetadata().getLonMin();
	}

	@Override
	public double getLonMax() {
		return getMRZMetadata().getLonMax();
	}

	@Override
	public double getSOGMin() {
		return getSPOMetadata().getSOGMin();
	}

	@Override
	public double getSOGMax() {
		return getSPOMetadata().getSOGMax();
	}

	@Override
	public double getDepthMin() {
		return getMRZMetadata().getDepthMin();
	}

	@Override
	public double getDepthMax() {
		return getMRZMetadata().getDepthMax();
	}

	@Override
	public float getDepthMean() {
		return getMRZMetadata().getDepthMean();
	}

	@Override
	public float getHeadingMin() {
		return getSKMMetadata().getHeadingMin();
	}

	@Override
	public float getHeadingMax() {
		return getSKMMetadata().getHeadingMax();
	}

	@Override
	public float getHeadingMean() {
		return getSKMMetadata().getHeadingMean();
	}

	@Override
	public float getRollMin() {
		return getSKMMetadata().getRollMin();
	}

	@Override
	public float getRollMax() {
		return getSKMMetadata().getRollMax();
	}

	@Override
	public float getRollMean() {
		return getSKMMetadata().getRollMean();
	}

	@Override
	public float getPitchMin() {
		return getSKMMetadata().getPitchMin();
	}

	@Override
	public float getPitchMax() {
		return getSKMMetadata().getPitchMax();
	}

	@Override
	public float getPitchMean() {
		return getSKMMetadata().getPitchMean();
	}

	@Override
	public float getHeaveMin() {
		return getSKMMetadata().getHeaveMin();
	}

	@Override
	public float getHeaveMax() {
		return getSKMMetadata().getHeaveMax();
	}

	@Override
	public float getHeaveMean() {
		return getSKMMetadata().getHeaveMean();
	}

	@Override
	public float getBsMin() {
		return getMRZMetadata().getBSMin();
	}

	@Override
	public float getBsMax() {
		return getMRZMetadata().getBSMax();
	}

	@Override
	public float getBsMean() {
		return getMRZMetadata().getBSMean();
	}

	@Override
	public float getSspMin() {
		return getSVPMetadata().getSSPMin();
	}

	@Override
	public float getSspMax() {
		return getSVPMetadata().getSSPMax();
	}

	@Override
	public float getSspMean() {
		return getSVPMetadata().getSSPMean();
	}

	@Override
	public int getRuntimeDgCount() {
		return getIOPMetadata().getEntries().size();
	}

	@Override
	public long getSVPProfileCount() {
		return getSVPMetadata().getEntries().size();
	}

	@Override
	public long getSVPMaxSampleCount() {
		return getSVPMetadata().getMaxEntriesPerDatagram();
	}

	@Override
	public long getSurfaceSoundSpeedCount() {
		return 0;
	}

	@Override
	public long getHeightCount() {
		return getSHIMetadata().getTotalNumberOfEntries();
	}

	@Override
	public long getClockCount() {
		return getSCLMetadata().getTotalNumberOfEntries();
	}

	@Override
	public long getDepthCount() {
		return getSDEMetadata().getTotalNumberOfEntries();
	}

	@Override
	public long getHeadingCount() {
		// NOT AVALAIBLE FOR KMALL
		return 0;
	}

	@Override
	public long getPositionCaptorCount() {
		long count = getIIPMetadata().getPositions().size();
		if(count == 0 && getSPOMetadata().datagramCount != 0) {
			count = getSPOMetadata().getSensors().size();
		}
		return count;
	}

	@Override
	public long getMRUCaptorCount() {
		long count = getIIPMetadata().getMrus().size();
		if(count == 0 && getSKMMetadata().datagramCount != 0) {
			count = getSKMMetadata().getSensors().keySet().size();
		}
		return count;
	}

	@Override
	public long getBeamDescriptionCount() {
		return 0;
	}

	@Override
	public long getSnippetCount() {
		return 0;
	}

	@Override
	public long getSidescanCount() {
		return 0;
	}

	@Override
	public int getMbgSerialNumber() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getRawInstallationParameters() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getMbgModelNumber() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double[] getTxAntennaLevelArm() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getDepthDatagramMinDate() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public long getDepthDatagramMaxDate() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean hasPhaseValue() {
		return getMWCMetadata().hasPhaseValues();
	}

}
