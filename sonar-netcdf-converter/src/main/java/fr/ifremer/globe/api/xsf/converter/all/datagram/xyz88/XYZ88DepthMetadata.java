package fr.ifremer.globe.api.xsf.converter.all.datagram.xyz88;

import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.AbstractBeamDatagramMetadata;

public class XYZ88DepthMetadata extends AbstractBeamDatagramMetadata<XYZ88DepthCompletePingMetadata> {

	private float depthSum;
	private long numberOfDepth;
	private double depthMin;
	private double depthMax;

	private long minDate = Long.MAX_VALUE;
	private long maxDate = Long.MIN_VALUE;

	public XYZ88DepthMetadata() {
		depthSum = 0;
		numberOfDepth = 0;
		depthMin = Float.MAX_VALUE;
		depthMax = Float.MIN_VALUE;
	}

	public void addDepth(double depth, long epoch) {
		this.depthSum += depth;
		this.numberOfDepth++;
		depthMin = Double.min(depth, depthMin);
		depthMax = Double.max(depth, depthMax);
		minDate = Math.min(minDate, epoch);
		maxDate = Math.max(maxDate, epoch);
	}

	public float getDepthMean() {
		return this.depthSum / this.numberOfDepth;
	}

	public double getDepthMin() {
		return depthMin;
	}

	public double getDepthMax() {
		return depthMax;
	}

	public long getMinDate() {
		return minDate;
	}

	public long getMaxDate() {
		return maxDate;
	}
}
