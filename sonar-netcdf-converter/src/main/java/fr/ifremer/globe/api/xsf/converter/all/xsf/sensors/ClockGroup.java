package fr.ifremer.globe.api.xsf.converter.all.xsf.sensors;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.all.datagram.clock.Clock;
import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagram;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.utils.DateUtil;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U2;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U8;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfFromKongsberg;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.ClockGrp;
import fr.ifremer.globe.api.xsf.util.SimpleIdentifier;
import fr.ifremer.globe.api.xsf.util.types.ULong;
import fr.ifremer.globe.netcdf.ucar.NCException;

public class ClockGroup implements IDatagramConverter<AllFile, XsfFromKongsberg> {

	private List<ValueD1> values;

	@Override
	public void declare(AllFile allFile, XsfFromKongsberg xsf) throws NCException {
		this.values = new LinkedList<>();
		ClockGrp vendorClockGroup = xsf.getClock();

		// declare all the variables
		values.add(new ValueD1U8(vendorClockGroup.getDatagram_time(),
				buffer -> new ULong(Clock.getXSFEpochTime(buffer.byteBufferData))));

		values.add(new ValueD1U2(vendorClockGroup.getClock_raw_count(),
				buffer -> Clock.getCounter(buffer.byteBufferData)));

		values.add(new ValueD1U8(vendorClockGroup.getExternal_time(), buffer -> {
			long dateKongsbergFormat = Clock.getDateFromExternalClock(buffer.byteBufferData).getU();
			long timeSinceMidnight = Clock.getTimeFromExternalClock(buffer.byteBufferData).getU();
			long epochTime = DateUtil.convertKongsbergTime(dateKongsbergFormat, timeSinceMidnight);

			return new ULong(BaseDatagram.milliSecondToNano(epochTime));
		}));

		values.add(new ValueD1U1(vendorClockGroup.getPps_use(), buffer -> Clock.getPPS(buffer.byteBufferData)));

	}

	@Override
	public void fillData(AllFile allFile) throws IOException, NCException {
		long[] origin = new long[] { 0 };
		DatagramBuffer buffer = new DatagramBuffer(allFile.getByteOrder());

		for (Entry<SimpleIdentifier, DatagramPosition> posentry : allFile.getClock().getDatagrams()) {
			final DatagramPosition pos = posentry.getValue();
			DatagramReader.readDatagramAt(pos.getFile(), buffer, pos.getSeek());

			// read the data from the source file
			for (ValueD1 value : this.values) {
				value.fill(buffer);
			}

			// flush into the output file
			for (ValueD1 value : this.values) {
				value.write(origin);
			}
			origin[0] += 1;
		}
	}

}
