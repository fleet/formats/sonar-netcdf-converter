package fr.ifremer.globe.api.xsf.converter.common.utils.processing;

import fr.ifremer.globe.api.xsf.converter.common.sounder.ISounderFile;
import fr.ifremer.globe.api.xsf.converter.common.utils.DoubleInterpolator;
import fr.ifremer.globe.api.xsf.converter.common.utils.FloatInterpolator;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfBase;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfFromKongsberg;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.*;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.BathymetryVendorSpecificGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.BeamGroup1VendorSpecificGrp;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCVariable;
import fr.ifremer.globe.utils.date.DateUtils;

import java.util.Arrays;
import java.util.TreeMap;
import java.util.TreeSet;

public class VariableComputer {

	/**
	 * Compute beam angles variables in order to give their angles relatively to the RX array. ie we substract the roll
	 * since they are given
	 *
	 * @param xsfBase : file being converted
	 * @throws NCException
	 */
	public static void computeBeamAngle(XsfFromKongsberg xsfBase) throws NCException {
		BeamGroup1Grp beamGroup = xsfBase.getBeamGroup();
		BeamGroup1VendorSpecificGrp beamGroupVendor = xsfBase.getBeamGroupVendorSpecific();

		if (beamGroup.getDim_beam() == null)
			// no water column, exit
			return;

		AttitudeSubGroup attGrp = xsfBase.getPreferredAttitudeSubGroup();
		NCVariable refTime = attGrp.getTime();
		// bad we create two pitch interpolator
		FloatInterpolator it = FloatInterpolator.build(attGrp.getPitch(), refTime)
				.orElseThrow(() -> new IllegalArgumentException("Pitch interpolator not available."));

		int swathCount = (int) beamGroup.getDim_ping_time().getLength();
		int beamCount = (int) beamGroup.getDim_beam().getLength();
		int txBeamCount = (int) beamGroup.getDim_tx_beam().getLength();
		int transducerCount = (int) xsfBase.getPlatformGrp().getDim_transducer().getLength();

		// RX
		if (swathCount > 0 && beamCount > 0) {
			// retrieve installation parameters
			// In a first approach we're gonna estimate that angles are stabilized and thus should be given
			// relatively to the vertical

			long[] start = { 0, 0 };
			long[] count = { swathCount, beamCount };

			// Copy /Sonar/Beam_group1/Vendor_specific/raw_rx_beam_pointing_angle
			// to /Sonar/Beam_group1/rx_beam_rotation_phi
			float[] values = beamGroupVendor.getRaw_rx_beam_pointing_angle().get_float(start, count);
			beamGroup.getRx_beam_rotation_phi().put(start, count, values);

			// Set all values to 0 in /Sonar/Beam_group1/rx_beam_rotation_psi
			// and /Sonar/Beam_group1/rx_beam_rotation_theta
			Arrays.fill(values, 0f);
			beamGroup.getRx_beam_rotation_theta().put(start, count, values);
			beamGroup.getRx_beam_rotation_psi().put(start, count, values);
		}

		// TX
		if (swathCount > 0 && txBeamCount > 0) {
			// In a first approach we're gonna estimate that angles are stabilized and thus should be given
			// relatively to the vertical

			long[] start = { 0, 0 };
			long[] count = { swathCount, txBeamCount };

			long[] pingTime = beamGroup.getPing_time().get_ulong(new long[] { 0l }, new long[] { swathCount });
			float[] txTimeDelaySec = beamGroupVendor.getTransmit_time_delay().get_float(start, count);

			float[] antennaRotationY = xsfBase.getPlatformGrp().getTransducer_rotation_y()
					.get_float(new long[] { 0l }, new long[] { transducerCount });
			int[] txTransducerIndex = beamGroup.getTransmit_transducer_index().get_int(start, count);

			// Raw values
			float[] values = beamGroupVendor.getRaw_tx_beam_tilt_angle().get_float(start, count);

			for (int swath = 0; swath < swathCount; swath++) {
				for (int tx_beam = 0; tx_beam < txBeamCount; tx_beam++) {
					int index = swath * txBeamCount + tx_beam;

					// If the antenna id is invalid, we use the first one ?
					float txRy = txTransducerIndex[index] >= 0
							? antennaRotationY[txTransducerIndex[index]]
							: antennaRotationY[0];

					// ref of tilt is relative to the tx_beam_array, we substract pitch to get an angle relative to
					// vertical
					float interpolatedPitch = it.floatInterpolate(
							pingTime[swath] + DateUtils.secondToNano(txTimeDelaySec[index]));

					values[index] = values[index] + interpolatedPitch + txRy;
				}
			}

			beamGroup.getTx_beam_rotation_theta().put(start, count, values);

			// Set all values to 0 in /Sonar/Beam_group1/tx_beam_rotation_phi
			// and /Sonar/Beam_group1/tx_beam_rotation_psi
			Arrays.fill(values, 0f);
			beamGroup.getTx_beam_rotation_phi().put(start, count, values);
			beamGroup.getTx_beam_rotation_psi().put(start, count, values);
		}
	}

	/**
	 * Compute multiping index in case this info is absent
	 *
	 * @param xsfBase : file being converted
	 * @throws NCException
	 */
	public static void computeMultipingIndex(XsfFromKongsberg xsfBase) throws NCException {
		BeamGroup1Grp beamGroup = xsfBase.getBeamGroup();
		BathymetryGrp bathymetryGrp = xsfBase.getBathyGrp();
		BeamGroup1VendorSpecificGrp beamGroupVendor = xsfBase.getBeamGroupVendorSpecific();

		if (beamGroup.getDim_ping_time() == null)
			return;

		long swathCount = beamGroup.getDim_ping_time().getLength();
		long[] startFreq = { 0, 0 };
		long[] countFreq = { swathCount, 1 }; // Only the first tx_beam is used
		float[] freq = beamGroupVendor.getCenter_frequency().get_float(startFreq, countFreq);
		float[] duration = beamGroup.getTransmit_duration_nominal().get_float(startFreq, countFreq);
		// build sorted array of center frequencies by mode (we consider that duration_nominal changes if the center_frequency set changes)
		TreeMap<Float, TreeSet<Float>> frequencyMap = new TreeMap<>();
		for (int swath = 0; swath < swathCount; swath++) {
			if (Float.isFinite(duration[swath])) {
				frequencyMap.putIfAbsent(duration[swath], new TreeSet<>());
				frequencyMap.get(duration[swath]).add(freq[swath]);
			}
		}
		TreeMap<Float, Float[]> frequencyArrays = new TreeMap<>();
		for (var entry : frequencyMap.entrySet()) {
			frequencyArrays.put(entry.getKey(), entry.getValue().toArray(new Float[0]));
		}

		short[] pingSequenceIndex = new short[(int) swathCount];
		short fillValue = bathymetryGrp.getMultiping_sequence().getShortFillValue();
		for (int swath = 0; swath < swathCount; swath++) {
			if (Float.isFinite(duration[swath])) {
				pingSequenceIndex[swath] = (short) Arrays.binarySearch(frequencyArrays.get(duration[swath]),
						freq[swath]);
			} else {
				pingSequenceIndex[swath] = fillValue;
			}
		}

		long[] start = { 0 };
		long[] count = { swathCount };
		bathymetryGrp.getMultiping_sequence().put(start, count, pingSequenceIndex);
	}

	/**
	 * Compute two way travel time in case this info is absent (RawRange70)
	 *
	 * @param xsfBase : file being converted
	 * @throws NCException
	 */
	public static void computeTwoWayTravelTime(XsfFromKongsberg xsfBase) throws NCException {
		BeamGroup1Grp beamGroup = xsfBase.getBeamGroup();
		BathymetryGrp bathymetryGrp = xsfBase.getBathyGrp();
		BathymetryVendorSpecificGrp bathymetryVendor = xsfBase.getBathy();

		if (beamGroup.getDim_ping_time() == null)
			return;

		long swathCount = beamGroup.getDim_ping_time().getLength();
		long detectionCount = bathymetryGrp.getDim_detection().getLength();
		long[] startSampling = { 0, 0 };
		long[] countSampling = { swathCount, 1 }; // Only the first rx_antenna is used
		float[] sampling = bathymetryVendor.getDetection_sampling_freq().get_float(startSampling, countSampling);

		float fillValue = bathymetryGrp.getDetection_two_way_travel_time().getFloatFillValue();
		for (int swath = 0; swath < swathCount; swath++) {
			if (Float.isFinite(sampling[swath])) {
				float[] twoWayTravelTime = new float[(int) detectionCount];
				Arrays.fill(twoWayTravelTime, fillValue);
				long[] start = { swath, 0 };
				long[] count = { 1, detectionCount };
				short[] range = bathymetryVendor.getEM3000detection_range().get_ushort(start, count);

				for (int detection = 0; detection < detectionCount; detection++) {
					twoWayTravelTime[detection] = Short.toUnsignedInt(range[detection])  / (4 * sampling[swath]);
				}
				bathymetryGrp.getDetection_two_way_travel_time().put(start, count, twoWayTravelTime);
			}
		}
	}

	/**
	 * Fill swath and tide draught values
	 *
	 * @throws NCException
	 */
	public static void fillSwathTideDraught(ISounderFile sounderFile, XsfBase xsfBase) throws NCException {
		BeamGroup1Grp beamGrp = xsfBase.getBeamGroup();
		long[] origin = { 0 };
		long[] count = { 1 };
		float[] values = { 0f };
		TideGrp tideGrp = xsfBase.getTideGrp();
		DynamicDraughtGrp draughtGrp = xsfBase.getDynamicDraughtGrp();

		// for .all files, sector ids are the same as sector indexes
		for (int i = 0; i < sounderFile.getSwathCount(); i++) {
			origin[0] = i;
			beamGrp.getWaterline_to_chart_datum().put(origin, count, values);
			tideGrp.getTide_indicative().put(origin, count, values);
			draughtGrp.getDelta_draught().put(origin, count, values);
			long[] timeValues = beamGrp.getPing_time().get_ulong(origin, count);
			tideGrp.getTime().putu(origin, count, timeValues);
			draughtGrp.getTime().putu(origin, count, timeValues);
		}
	}

	/**
	 * Interpolator for Attitude and Position in case of missing datagram in sounder files
	 *
	 * @throws NCException
	 */
	public static void fillPlatformAttitudeAndPosition(ISounderFile sounderFile, XsfBase xsfBase) throws NCException {
		//
		BeamGroup1Grp beamGrp = xsfBase.getBeamGroup();
		long[] origin = { 0 };
		long[] count = { 1 };
		double[] values_d = { 0f };
		float[] values_f = { 0f };

		AttitudeSubGroup attitudeGrp = xsfBase.getPreferredAttitudeSubGroup();
		PositionSubGroup positionGrp = xsfBase.getPreferredPositionSubGroup();

		DoubleInterpolator latitudeInt = new DoubleInterpolator(positionGrp.getLatitude(), positionGrp.getTime());
		DoubleInterpolator longitudeInt = new DoubleInterpolator(positionGrp.getLongitude(), positionGrp.getTime());

		// Try to get heading interpolator from position group, or else from attitude group.
		FloatInterpolator headingInt = FloatInterpolator.build(positionGrp.getHeading(), positionGrp.getTime())
				.orElse(FloatInterpolator.build(attitudeGrp.getHeading(), attitudeGrp.getTime())
						.orElseThrow(() -> new IllegalArgumentException("Heading interpolator not available.")));

		FloatInterpolator rollInt = FloatInterpolator.build(attitudeGrp.getRoll(), attitudeGrp.getTime())
				.orElseThrow(() -> new IllegalArgumentException("Roll interpolator not available."));
		FloatInterpolator pitchInt = FloatInterpolator.build(attitudeGrp.getPitch(), attitudeGrp.getTime())
				.orElseThrow(() -> new IllegalArgumentException("Pitch interpolator not available."));
		FloatInterpolator verticalOffsetInt = FloatInterpolator.build(beamGrp.getPlatform_vertical_offset(),
				beamGrp.getPing_time()).orElseThrow(
				() -> new IllegalArgumentException("Platform vertical offset interpolator not available."));
		FloatInterpolator depthInt = FloatInterpolator.build(beamGrp.getTx_transducer_depth(), beamGrp.getPing_time())
				.orElseThrow(() -> new IllegalArgumentException("Tx transducer depth interpolator not available."));

		// for .all files, sector ids are the same as sector indexes
		for (int i = 0; i < sounderFile.getSwathCount(); i++) {
			origin[0] = i;
			long[] timeValues = beamGrp.getPing_time().get_ulong(origin, count);

			// fill only swath not already filled
			// retrieve position/attitude by sensor datagram interpolation

			if (!Double.isFinite(beamGrp.getPlatform_latitude().get_double(origin, count)[0])) {
				values_d[0] = latitudeInt.doubleInterpolate(timeValues[0]);
				beamGrp.getPlatform_latitude().put(origin, count, values_d);
			}
			if (!Double.isFinite(beamGrp.getPlatform_longitude().get_double(origin, count)[0])) {
				values_d[0] = longitudeInt.angleInterpolate(timeValues[0]);
				beamGrp.getPlatform_longitude().put(origin, count, values_d);
			}
			if (!Double.isFinite(beamGrp.getPlatform_heading().get_float(origin, count)[0])) {
				values_f[0] = headingInt.angleInterpolate(timeValues[0]);
				beamGrp.getPlatform_heading().put(origin, count, values_f);
			}
			if (!Double.isFinite(beamGrp.getPlatform_roll().get_float(origin, count)[0])) {
				values_f[0] = rollInt.floatInterpolate(timeValues[0]);
				beamGrp.getPlatform_roll().put(origin, count, values_f);
			}
			if (!Double.isFinite(beamGrp.getPlatform_pitch().get_float(origin, count)[0])) {
				values_f[0] = pitchInt.floatInterpolate(timeValues[0]);
				beamGrp.getPlatform_pitch().put(origin, count, values_f);
			}
			// retrieve depth Nan values by neighboors interpolation
			values_f[0] = verticalOffsetInt.floatInterpolate(timeValues[0]);
			beamGrp.getPlatform_vertical_offset().put(origin, count, values_f);
			values_f[0] = depthInt.floatInterpolate(timeValues[0]);
			beamGrp.getTx_transducer_depth().put(origin, count, values_f);
		}
	}
}
