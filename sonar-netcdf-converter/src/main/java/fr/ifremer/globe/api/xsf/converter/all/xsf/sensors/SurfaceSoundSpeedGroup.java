package fr.ifremer.globe.api.xsf.converter.all.xsf.sensors;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.all.datagram.IndividualSensorMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.all.datagram.soundspeedprofile.SoundSpeedProfile;
import fr.ifremer.globe.api.xsf.converter.all.datagram.surfacesoundspeed.SurfaceSoundSpeed;
import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2F4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2U8;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfFromKongsberg;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.SurfaceSoundSpeedGrp;
import fr.ifremer.globe.api.xsf.util.SimpleIdentifier;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.api.xsf.util.types.ULong;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.date.DateUtils;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

public class SurfaceSoundSpeedGroup implements IDatagramConverter<AllFile, XsfFromKongsberg> {

	long nEntries;
	private List<ValueD2> values = new LinkedList<>();
	private IndividualSensorMetadata sensorMetadata;
	private AllFile metadata;

	public SurfaceSoundSpeedGroup(AllFile metadata) {
		this.metadata = metadata;
		this.sensorMetadata = metadata.getSurfaceSoundSpeed();
		this.nEntries = sensorMetadata.getTotalNumberOfEntries();
		this.values = new LinkedList<>();
	}

	@Override
	public void declare(AllFile allFile, XsfFromKongsberg xsf) throws NCException {
		if (!xsf.getSurfaceSoundSpeedGrp().isPresent()) {
			throw new NCException("Error, group for surface sound speed was not declared before");
		}
		SurfaceSoundSpeedGrp surface = xsf.getSurfaceSoundSpeedGrp().get();

		// declare all the variables

		values.add(new ValueD2U8(surface.getTime(), sensorMetadata.getMaxEntriesPerDatagram(), (buffer, i) -> {
			return new ULong(DateUtils.milliSecondToNano(
					SoundSpeedProfile.getEpochTime(buffer.byteBufferData) + SurfaceSoundSpeed.getTimeSinceRecordStart(
							buffer.byteBufferData, i).getU()));
		}));

		values.add(new ValueD2F4(surface.getSurface_sound_speed(), sensorMetadata.getMaxEntriesPerDatagram(),
				(buffer, i) -> {
					return new FFloat(SurfaceSoundSpeed.getSurfaceSoundSpeed(buffer.byteBufferData, i).getU() * 0.1f);
				}));

	}

	@Override
	public void fillData(AllFile allFile) throws IOException, NCException {
		long[] count = new long[] { 0 };
		long[] origin = new long[] { 0 };

		DatagramBuffer buffer = new DatagramBuffer(metadata.getByteOrder());
		for (Entry<SimpleIdentifier, DatagramPosition> posentry : sensorMetadata.getDatagrams()) {
			values.forEach(ValueD2::clear);
			final DatagramPosition pos = posentry.getValue();
			DatagramReader.readDatagramAt(pos.getFile(), buffer, pos.getSeek());
			long nEntries = SurfaceSoundSpeed.getNumberEntries(buffer.byteBufferData).getU();

			// read the data from the source file
			for (int i = 0; i < nEntries; i++) {
				for (ValueD2 value : this.values) {
					value.fill(buffer, i, i);
				}
			}
			// flush into the output file
			count[0] = nEntries;
			for (ValueD2 value : this.values) {
				value.write(origin, count);
			}
			origin[0] += nEntries;
		}
	}

}
