package fr.ifremer.globe.api.xsf.converter.all.datagram.rawrange70;

import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.AbstractCompleteAntennaPingSingleMetadata;

public class RawRange70CompleteAntennaPingMetadata extends AbstractCompleteAntennaPingSingleMetadata {

    public RawRange70CompleteAntennaPingMetadata(DatagramPosition position, int beamCount) {
        super(position, beamCount, 0);
    }

}
