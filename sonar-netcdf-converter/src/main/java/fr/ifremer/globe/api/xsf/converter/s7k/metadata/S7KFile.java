package fr.ifremer.globe.api.xsf.converter.s7k.metadata;

import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.DatagramMetadata;
import fr.ifremer.globe.api.xsf.converter.common.exceptions.UnexpectedEndOfFile;
import fr.ifremer.globe.api.xsf.converter.common.sounder.ISounderFile;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramParser;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.s7k.tools.S7K7030InstallationParamSerializer;
import fr.ifremer.globe.api.xsf.util.DefaultLogger;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.SwathIndexContainer;
import fr.ifremer.globe.api.xsf.util.SwathIndexGenerator;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.sounders.SounderDescription;
import fr.ifremer.globe.utils.sounders.SounderDescription.Constructor;
import fr.ifremer.globe.utils.sounders.SoundersLibrary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Optional;

public class S7KFile implements ISounderFile {

    public static final String EXTENSION_S7K = ".s7k";

    /**
     * logger
     */
    public static final Logger LOGGER = LoggerFactory.getLogger(S7KFile.class);

    private final String filePath;
    private final RandomAccessFile raf;

    private final SwathIndexGenerator swathGenerator = new SwathIndexGenerator();
    private final SwathIndexContainer retainedIds = new SwathIndexContainer();
    private final S7KUnknownDatagramMetadata unknownMetadata = new S7KUnknownDatagramMetadata();
    private final S7KStats stats = new S7KStats();

    private final S7K1003Metadata dg_1003 = new S7K1003Metadata();
    private final S7K1008Metadata dg_1008 = new S7K1008Metadata();
    private final S7K1010Metadata dg_1010 = new S7K1010Metadata();
    private final S7K1015Metadata dg_1015 = new S7K1015Metadata();
    private final S7K1016Metadata dg_1016 = new S7K1016Metadata();
    private final S7K7000Metadata dg_7000 = new S7K7000Metadata();
    private final S7K7001Metadata dg_7001 = new S7K7001Metadata();
    private final S7K7004Metadata dg_7004 = new S7K7004Metadata();
    private final S7K7006Metadata dg_7006 = new S7K7006Metadata();
    private final S7K7009Metadata dg_7009 = new S7K7009Metadata();
    private final S7K7027Metadata dg_7027 = new S7K7027Metadata();
    private final S7K7030Metadata dg_7030 = new S7K7030Metadata();
    private final S7K7041Metadata dg_7041 = new S7K7041Metadata();
    private final S7K7057Metadata dg_7057 = new S7K7057Metadata();
    private final S7K7058Metadata dg_7058 = new S7K7058Metadata();
    private final S7K7200Metadata dg_7200 = new S7K7200Metadata();

    /**
     * Default constructor : parse all datagrams.
     */
    public S7KFile(String filePath) throws IOException {
        this(filePath, true, true);
    }

    /**
     * Constructor
     */
    public S7KFile(String filePath, boolean readWC, boolean readAllDatagrams) throws IOException {
        this.filePath = filePath;

        // read and index file
        raf = new RandomAccessFile(filePath, "r");
        final DatagramParser parser = new DatagramParser(readWC, readAllDatagrams);
        final DatagramBuffer buffer = new DatagramBuffer();
        long offsetInFile = 0;
        int byteRead = 0;
        try {
            while ((byteRead = DatagramReader.readDatagram(raf, buffer)) > 0) {
                parser.computeMetadata(this, buffer, new DatagramPosition(raf, offsetInFile));
                offsetInFile += byteRead;
                if (offsetInFile != raf.getFilePointer()) {
                    throw new IOException();
                }
            }
        } catch (UnexpectedEndOfFile e) {
            // invalid datagram (error with end flag) : error logged, but the conversion can be try with valid datagrams
            DefaultLogger.get().error("Invalid datagram at {} : {} ", offsetInFile, e.getMessage(), e);
            DefaultLogger.get().warn("Try conversion with valid datagrams...");
        }
        index();
    }

    @Override
    public void close() throws IOException {
        raf.close();
    }

    public SwathIdentifier getSwathId(long rawPingNum, int multiPingSequence, long time) {
        return swathGenerator.addId(rawPingNum, 1, multiPingSequence, time);
    }

    public int getSwathCount() {
        return retainedIds.getIndexSize();
    }

    public SwathIndexContainer getSwathIndexer() {
        return retainedIds;
    }

    public S7KUnknownDatagramMetadata getUnknownMetadata() {
        return unknownMetadata;
    }

    public S7K1003Metadata get1003Metadata() {
        return dg_1003;
    }

    public S7K1008Metadata get1008Metadata() {
        return dg_1008;
    }

    public S7K1010Metadata get1010Metadata() {
        return dg_1010;
    }

    public S7K1015Metadata get1015Metadata() {
        return dg_1015;
    }

    public S7K1016Metadata get1016Metadata() {
        return dg_1016;
    }

    public S7K7000Metadata get7000Metadata() {
        return dg_7000;
    }

    public S7K7001Metadata get7001Metadata() {
        return dg_7001;
    }

    public S7K7004Metadata get7004Metadata() {
        return dg_7004;
    }

    public S7K7006Metadata get7006Metadata() {
        return dg_7006;
    }

    public S7K7009Metadata get7009Metadata() {
        return dg_7009;
    }

    public S7K7027Metadata get7027Metadata() {
        return dg_7027;
    }

    public S7K7030Metadata get7030Metadata() {
        return dg_7030;
    }

    public S7K7041Metadata get7041Metadata() {
        return dg_7041;
    }

    public S7K7057Metadata get7057Metadata() {
        return dg_7057;
    }

    public S7K7058Metadata get7058Metadata() {
        return dg_7058;
    }

    public S7K7200Metadata get7200Metadata() {
        return dg_7200;
    }

    /**
     * return true if the ping is not rejected
     */
    private boolean isPingKept(SwathIdentifier is) {
        boolean hasBathyMetry = get7006Metadata().contains(is) || get7027Metadata().contains(is);
		boolean hasWaterColumn = get7041Metadata().contains(is);
        return hasBathyMetry || hasWaterColumn;
    }

    public void index() {
        for (S7KMultipingDatagramMetadata datagramMd : new S7KMultipingDatagramMetadata[]{this.dg_7000, this.dg_7006,
                this.dg_7009, this.dg_7027, this.dg_7041, this.dg_7057, this.dg_7058}) {
            for (Entry<Long, S7KMultipingMetadata> md : datagramMd.getEntries()) {
                md.getValue().index();
            }
        }
        this.retainedIds.generateIndex(this.swathGenerator, id -> this.isPingKept(id));

    }

    public S7KStats getStats() {
        return this.stats;
    }

    /**
     * @return the most suitable {@link S7K7030InstallationParameters}
     */
    public S7K7030InstallationParameters getInstallationParameters(long date) {
        // get installation parameters from 7030 datagram
        if (!get7030Metadata().getEntries().isEmpty()) {
            S7K7030IndividualDeviceMetadata device = get7030Metadata().getEntries().iterator().next().getValue();
            S7K7030InstallationParameters parametersFromDatagram = device.getConfiguration(date);
            if (parametersFromDatagram != null)
                return parametersFromDatagram;
        }

        try {
            int deviceId = stats.getUniqueDeviceId();
            // get frequency from device ID and file name...(on PP, there is 2 antenna : 12kHz / 24kHz)
            String filename = new File(filePath).getName();
            Optional<Float> frequencyFromFileName = Optional.empty();
            if (filename.contains("12kHz"))
                frequencyFromFileName = Optional.of(12_000f);
            if (filename.contains("24kHz"))
                frequencyFromFileName = Optional.of(24_500f);

            // search XML file into the current directory...
            String directoryPath = new File(getFilePath()).getParent();
            Optional<S7K7030InstallationParameters> parametersFromXML = S7K7030InstallationParamSerializer
                    .deserialize(deviceId, frequencyFromFileName, directoryPath);
            if (parametersFromXML.isPresent())
                return parametersFromXML.get();

            // or with default values...
            LOGGER.debug("WARNING: installation parameters datagram missing (7030), default values are used.");
            parametersFromXML = S7K7030InstallationParamSerializer.deserializeDefault(deviceId, frequencyFromFileName);
            return parametersFromXML.orElseThrow(() -> new RuntimeException("Installation parameters missing"));
        } catch (NumberFormatException | IndexOutOfBoundsException e) {
            throw new RuntimeException("Installation parameters missing");
        }
    }

    public String toString() {
        StringBuilder bld = new StringBuilder();

        for (Field f : this.getClass().getDeclaredFields()) {
            try {
                if (f.get(this) instanceof @SuppressWarnings("rawtypes")DatagramMetadata md && f.getName().startsWith("dg_")) {
                    if (md.datagramCount > 0) {
                        bld.append(f.get(this).toString() + System.lineSeparator());
                    }
                }
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        if (unknownMetadata.datagramCount > 0) {
            bld.append(unknownMetadata + System.lineSeparator());
        }

        return bld.toString();
    }

    @Override
    public String getFilePath() {
        return filePath;
    }

    @Override
    public String getModelNumber() {
        StringBuilder r = new StringBuilder();
        Iterator<Long> it = getStats().getDeviceIds().iterator();
        r.append(String.format("%d", it.next()));
        while (it.hasNext()) {
            r.append(String.format("-%d", it.next()));
        }
        return r.toString();
    }

    @Override
    public SounderDescription getSounderDescription() {
        return SoundersLibrary.get().fromConstructorId(getStats().getUniqueDeviceId(), Constructor.Reson);
    }

    @Override
    public int getMbgModelNumber() {
        return getSounderDescription().getGenericId();
    }

    @Override
    public int getSwathCountFromDepthDatagrams() {
        return get7006Metadata().datagramCount > 0 ? get7006Metadata().datagramCount : get7027Metadata().datagramCount;
    }

    @Override
    public int getDetectionCount() {
        // detectionDim
        int detections = 0;
        if (this.get7006Metadata().datagramCount > 0) {
            detections = Math.max(detections,
                    this.get7006Metadata().getEntries().iterator().next().getValue().getMaxBeamCount());
        }
        if (this.get7027Metadata().datagramCount > 0) {
            detections = Math.max(detections,
                    this.get7027Metadata().getEntries().iterator().next().getValue().getMaxBeamCount());
        }
        if (this.get7058Metadata().datagramCount > 0) {
            detections = Math.max(detections,
                    this.get7058Metadata().getEntries().iterator().next().getValue().getMaxBeamCount());
        }
        return detections;
    }

    @Override
    public int getTxSectorCount() {
        return 1;
    }

    @Override
    public int getWcBeamCount() {
        int beamCount = 0;

        if (this.get7004Metadata().datagramCount > 0) {
            beamCount = Math.max(beamCount,
                    this.get7004Metadata().getEntries().iterator().next().getValue().getMaxEntriesPerDatagram());
        }
        if (this.get7041Metadata().datagramCount > 0) {
            beamCount = Math.max(beamCount,
                    this.get7041Metadata().getEntries().iterator().next().getValue().getMaxBeamCount());
        }
        return beamCount;
    }

    @Override
    public int getAntennaNb() {
        return getTxAntennaNb() + getRxAntennaNb();
    }

    @Override
    public int getRxAntennaNb() {
        return 1;
    }

    @Override
    public int getTxAntennaNb() {
        return 1;
    }

    @Override
    public long getMinDate() {
        return getStats().getDate().getMin();
    }

    @Override
    public long getMaxDate() {
        return getStats().getDate().getMax();
    }

    @Override
    public double getLatMin() {
        return getStats().getDetectionLat().getMin();
    }

    @Override
    public double getLatMax() {
        return getStats().getDetectionLat().getMax();
    }

    @Override
    public double getLonMin() {
        return getStats().getDetectionLon().getMin();
    }

    @Override
    public double getLonMax() {
        return getStats().getDetectionLon().getMax();
    }

    @Override
    public double getSOGMin() {
        if (getStats().getSog().getMin() == null)
            return 0;
        else
            return getStats().getSog().getMin();
    }

    @Override
    public double getSOGMax() {
        if (getStats().getSog().getMax() == null)
            return 0;
        else
            return getStats().getSog().getMax();
    }

    @Override
    public double getDepthMin() {
        if (getStats().getDepth().getMin() == null)
            return 0;
        else
            return getStats().getDepth().getMin();
    }

    @Override
    public double getDepthMax() {
        if (getStats().getDepth().getMax() == null)
            return 0;
        else
            return getStats().getDepth().getMax();
    }

    @Override
    public float getDepthMean() {
        return (float) getStats().getDepth().getMean();
    }

    @Override
    public float getHeadingMin() {
        if (getStats().getHeading().getMin() == null)
            return 0;
        else
            return getStats().getHeading().getMin();
    }

    @Override
    public float getHeadingMax() {
        if (getStats().getHeading().getMax() == null)
            return 0;
        else
            return getStats().getHeading().getMax();
    }

    @Override
    public float getHeadingMean() {
        return (float) getStats().getHeading().getMean();
    }

    @Override
    public float getRollMin() {
        if (getStats().getRoll().getMin() == null)
            return 0;
        else
            return getStats().getRoll().getMin();
    }

    @Override
    public float getRollMax() {
        if (getStats().getRoll().getMax() == null)
            return 0;
        else
            return getStats().getRoll().getMax();
    }

    @Override
    public float getRollMean() {
        return (float) getStats().getRoll().getMean();
    }

    @Override
    public float getPitchMin() {
        if (getStats().getPitch().getMin() == null)
            return 0;
        else
            return getStats().getPitch().getMin();
    }

    @Override
    public float getPitchMax() {
        if (getStats().getPitch().getMax() == null)
            return 0;
        else
            return getStats().getPitch().getMax();
    }

    @Override
    public float getPitchMean() {
        return (float) getStats().getPitch().getMean();
    }

    @Override
    public float getHeaveMin() {
        if (getStats().getHeave().getMin() == null)
            return 0;
        else
            return getStats().getHeave().getMin();
    }

    @Override
    public float getHeaveMax() {
        if (getStats().getHeave().getMax() == null)
            return 0;
        else
            return getStats().getHeave().getMax();
    }

    @Override
    public float getHeaveMean() {
        return (float) getStats().getHeave().getMean();
    }

    @Override
    public float getBsMin() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public float getBsMax() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public float getBsMean() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public float getSspMin() {
        if (getStats().getSsp().getMin() == null)
            return 0;
        else
            return getStats().getSsp().getMin();
    }

    @Override
    public float getSspMax() {
        if (getStats().getSsp().getMax() == null)
            return 0;
        else
            return getStats().getSsp().getMax();
    }

    @Override
    public float getSspMean() {
        return (float) getStats().getSsp().getMean();
    }

    @Override
    public int getMaxSeabedSampleCount() {
		return get7058Metadata().getMaxSampleCount();
    }

    @Override
    public int getMaxSeabedExtraSampleCount() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public long getSVPProfileCount() {
        switch (get1010Metadata().getEntries().size()) {
            case 0:
                return 1;
            case 1:
                return get1010Metadata().getEntries().iterator().next().getValue().datagramCount;
            default:
                throw new RuntimeException("This converter only supports one sound speed profile sensor");
        }

    }

    @Override
    public long getSVPMaxSampleCount() {
        switch (get1010Metadata().getEntries().size()) {
            case 0:
                return 1;
            case 1:
                return get1010Metadata().getEntries().iterator().next().getValue().getMaxEntriesPerDatagram();
            default:
                throw new RuntimeException("This converter only supports one sound speed profile sensor");
        }
    }

    @Override
    public long getSurfaceSoundSpeedCount() {
        return 1;
    }

    @Override
    public long getPositionCaptorCount() {
        return 1;
    }

    @Override
    public long getMRUCaptorCount() {
        return 1;
    }

    @Override
    public long getBeamDescriptionCount() {
		if (get7004Metadata().getEntries().isEmpty())
			return 0;
        return get7004Metadata().getEntries().iterator().next().getValue().datagramCount;
    }

    @Override
    public long getSnippetCount() {
        return get7058Metadata().getSnippetCount();
    }

    @Override
    public long getSidescanCount() {
        if (get7057Metadata().getEntries().size() < 1) {
            return 0;
        }

        return get7057Metadata().getEntries().iterator().next().getValue().getMaxBeamCount();
    }

    @Override
    public int getMbgSerialNumber() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public String getRawInstallationParameters() {
        return getInstallationParameters(DateUtils.milliSecondToNano(getMaxDate())).toString();
    }

    @Override
    public double[] getTxAntennaLevelArm() {
        S7K7030InstallationParameters installParams = getInstallationParameters(
                DateUtils.milliSecondToNano(getMaxDate()));
        // installation parameters are in S7K coordinate system (z up +, x/y inverted)
        return new double[]{installParams.getTransmitArrayY(), installParams.getTransmitArrayX(),
                -installParams.getTransmitArrayZ()};
    }

    @Override
    public boolean hasPhaseValue() {
        return false; // no datagram containing phase is handled
    }

}
