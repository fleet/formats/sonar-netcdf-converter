package fr.ifremer.globe.api.xsf.converter.common.datagram;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public abstract class BaseDatagramBuffer {

    public byte[] rawBufferHeader;
    public byte[] rawBufferData;
    public ByteBuffer byteBufferData;
    public ByteBuffer byteBufferSizeHeader;
    
    public ByteOrder byteOrder;

    public BaseDatagramBuffer(ByteOrder byteOrder) {
        this.byteOrder = byteOrder;
    }
}
