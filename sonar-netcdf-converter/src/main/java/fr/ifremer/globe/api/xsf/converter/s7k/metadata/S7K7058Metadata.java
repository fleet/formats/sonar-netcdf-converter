package fr.ifremer.globe.api.xsf.converter.s7k.metadata;

public class S7K7058Metadata extends S7KMultipingDatagramMetadata {
	long snippetCount=0;
	// max sample count by beam
	int maxSampleCount=0;
	public void addSnippets(int numOfSnippets, int sampleCount) {
		snippetCount+=numOfSnippets;
		maxSampleCount = Math.max(maxSampleCount, sampleCount);
	}
	public long getSnippetCount() {
		return snippetCount;
	}
	public int getMaxSampleCount() {
		return maxSampleCount;
	}
	

}
