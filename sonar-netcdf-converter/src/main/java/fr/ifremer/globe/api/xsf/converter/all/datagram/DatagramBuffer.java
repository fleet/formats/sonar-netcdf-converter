package fr.ifremer.globe.api.xsf.converter.all.datagram;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.common.exceptions.BadETXRecord;
import fr.ifremer.globe.api.xsf.converter.common.exceptions.BadSTXRecord;
import fr.ifremer.globe.api.xsf.converter.common.utils.XLog;

public class DatagramBuffer extends BaseDatagramBuffer {
    
    public DatagramBuffer(ByteOrder byteOrder) {
        super(byteOrder);
        rawBufferData = new byte[70000];
        byteBufferData = ByteBuffer.wrap(rawBufferData);
        byteBufferData.order(byteOrder);

        rawBufferHeader = new byte[4];
        byteBufferSizeHeader = ByteBuffer.wrap(rawBufferHeader);
        byteBufferSizeHeader.order(byteOrder);

    }

    public void checkSTX() throws BadSTXRecord {
        // check header
        byte stx = byteBufferData.get(0);

        
        if (stx != 2) {
        	XLog.error("Bad STX record detected, file is corrupted stx=" + stx );
        	throw new BadSTXRecord(); // bad file recorded, stop reading
        }
    }

    public void checkETX() throws BadETXRecord {
        // ckeck footer
        byte etx = byteBufferData.get(byteBufferData.limit() - 3);
        if (etx != 3) {
        	XLog.error("Bad ETX record detected, file is corrupted ext=" + etx );
        	throw new BadETXRecord();
        }

    }
    
    
    public void validate() throws BadSTXRecord, BadETXRecord {
        checkSTX();
        checkETX();
    }
    
    public boolean isValid() {
        try {
            validate();
        } catch (BadSTXRecord e) {
            return false;
        } catch (BadETXRecord e) {
            return false;
        }
        return true;
    }
}