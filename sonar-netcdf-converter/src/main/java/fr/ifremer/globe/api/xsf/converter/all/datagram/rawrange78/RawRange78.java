package fr.ifremer.globe.api.xsf.converter.all.datagram.rawrange78;

import java.nio.ByteBuffer;

import fr.ifremer.globe.api.xsf.converter.all.datagram.PingDatagram;
import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.api.xsf.util.types.SByte;
import fr.ifremer.globe.api.xsf.util.types.SShort;
import fr.ifremer.globe.api.xsf.util.types.UByte;
import fr.ifremer.globe.api.xsf.util.types.UInt;
import fr.ifremer.globe.api.xsf.util.types.UShort;

public class RawRange78 extends PingDatagram {
	/** {@inheritDoc} */
	@Override
	public RawRange78Metadata getSpecificMetadata(AllFile metadata) {
		return metadata.getRawRange78();
	}

	@Override
	protected void computeSpecificMetadata(AllFile metadata, ByteBuffer datagram, long epochTime, SwathIdentifier swathId,
			int serialNumber, DatagramPosition position) {
	    RawRange78Metadata rrMetadata = getSpecificMetadata(metadata);
		
		int ntx = getNTxTransmitSectorCounter(datagram).getU();
		int nbeams = getTotalBeamCount(datagram).getU();
		
		RawRange78CompletePingMetadata completing = rrMetadata.getPing(swathId);
		if (completing == null) {
		    completing = new RawRange78CompletePingMetadata(ntx, metadata.getInstallation().getSerialNumbers());
		    
		    rrMetadata.addPing(swathId, completing);
		}
		completing.addAntenna(serialNumber, new RawRange78CompleteAntennaPingMetadata(position, nbeams));
		
	}

	public static UShort getCounter(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 12);
	}

	public static UShort getSerialNumber(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 14);
	}

	public static UShort getSoundSpeed(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 16);
	}

	public static UShort getNTxTransmitSectorCounter(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 18);
	}

	public static UShort getTotalBeamCount(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 20);
	}

	public static UShort getValidBeamCounter(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 22);
	}

	public static FFloat getSamplingFn(ByteBuffer datagram) {
		return TypeDecoder.read4F(datagram, 24);
	}

	public static UInt getDscale(ByteBuffer datagram) {
		return TypeDecoder.read4U(datagram, 28);
	}

	/**
	 * START TxSector Block
	 */
	/**
	 * return the given value
	 * 
	 * @param datagram
	 *            the datagram buffer
	 * @param txSectorIndex
	 *            the index of tx Sector
	 */
	public static SShort getTxTilt(ByteBuffer datagram, int txSectorIndex) {
		return TypeDecoder.read2S(datagram, 32 + txSectorIndex * 24);
	}
	
	/**
	 * return the given value
	 * 
	 * @param datagram
	 *            the datagram buffer
	 * @param txSectorIndex
	 *            the index of tx Sector
	 */
	public static UShort getFocusRange(ByteBuffer datagram, int txSectorIndex) {
		return TypeDecoder.read2U(datagram, 34 + txSectorIndex * 24);         
	}

	
	/**
	 * return the given value
	 * 
	 * @param datagram
	 *            the datagram buffer
	 * @param txSectorIndex
	 *            the index of tx Sector
	 */
	public static FFloat getSignalLength(ByteBuffer datagram, int txSectorIndex) {   
		return TypeDecoder.read4F(datagram, 36 + txSectorIndex * 24);
	}		
	
	/**
	 * return the given value
	 * 
	 * @param datagram
	 *            the datagram buffer
	 * @param txSectorIndex
	 *            the index of tx Sector
	 */
	public static FFloat getSectorTransmitDelay(ByteBuffer datagram, int txSectorIndex) {  
		return TypeDecoder.read4F(datagram, 40 + txSectorIndex * 24);         
	}	
	
	/**
	 * return the given value
	 * 
	 * @param datagram
	 *            the datagram buffer
	 * @param txSectorIndex
	 *            the index of tx Sector
	 */
	public static FFloat getTxCenterFrequency(ByteBuffer datagram, int txSectorIndex) {  
		return TypeDecoder.read4F(datagram, 44 + txSectorIndex * 24);         
	}	
	
	/**
	 * return the given value
	 * 
	 * @param datagram
	 *            the datagram buffer
	 * @param txSectorIndex
	 *            the index of tx Sector
	 */
	public static UShort getMeanAbsorptionCoef(ByteBuffer datagram, int txSectorIndex) {  
		return TypeDecoder.read2U(datagram, 48 + txSectorIndex * 24);         
	}	
	
	/**
	 * return the given value
	 * 
	 * @param datagram
	 *            the datagram buffer
	 * @param txSectorIndex
	 *            the index of tx Sector
	 */
	public static UByte getWaveFormIdentifier(ByteBuffer datagram, int txSectorIndex) {  
		return TypeDecoder.read1U(datagram, 50 + txSectorIndex * 24);         
	}	
	
	/**
	 * return the given value
	 * 
	 * @param datagram
	 *            the datagram buffer
	 * @param txSectorIndex
	 *            the index of tx Sector
	 */
	public static UByte getTxSectorCounter(ByteBuffer datagram, int txSectorIndex) {  
		return TypeDecoder.read1U(datagram, 51 + txSectorIndex * 24);         
	}	

	/**
	 * return the given value
	 * 
	 * @param datagram
	 *            the datagram buffer
	 * @param txSectorIndex
	 *            the index of tx Sector
	 */
	public static FFloat getSignalBandwidth(ByteBuffer datagram, int txSectorIndex) {  
		return TypeDecoder.read4F(datagram, 52 + txSectorIndex * 24);         
	}	
	
	/*
	 * START Entries Block (Samples)
	 */
	
	public static int getStartingRxOffset(ByteBuffer datagram) {
	    int nTx = getNTxTransmitSectorCounter(datagram).getU();
	    return 32 + 24 * nTx;
	}
	
	public static int getBeamOffset(int startRxOffset, int idx) {
	    return startRxOffset + 16 * idx;
	}
	
	/**
	 * return the given value
	 * 
	 * @param datagram
	 *            the datagram buffer
	 * @param startingOffset
	 *            the starting offset of the considered rxBeam
	 */
	public static SShort getBeamPointingAngle(ByteBuffer datagram, int startingOffset) {
		return TypeDecoder.read2S(datagram, startingOffset);
	}


	/**
	 * return the given value
	 * 
	 * @param datagram
	 *            the datagram buffer
	 * @param startingOffset
	 *            the starting offset of the considered rxBeam
	 */
	public static UByte getTxSectorIndex(ByteBuffer datagram, int startingOffset) {
		return TypeDecoder.read1U(datagram, startingOffset + 2);
	}

	/**
	 * return the given value
	 * 
	 * @param datagram
	 *            the datagram buffer
	 * @param startingOffset
	 *            the starting offset of the considered rxBeam
	 */
	public static UByte getDetectionInfo(ByteBuffer datagram, int startingOffset) {
		return TypeDecoder.read1U(datagram, startingOffset + 3);
	}

	/**
	 * return the given value
	 * 
	 * @param datagram
	 *            the datagram buffer
	 * @param startingOffset
	 *            the starting offset of the considered rxBeam
	 */
	public static UShort getDetectionLength(ByteBuffer datagram, int startingOffset) {
		return TypeDecoder.read2U(datagram,  startingOffset + 4);
	}

	/**
	 * return the given value
	 * 
	 * @param datagram
	 *            the datagram buffer
	 * @param startingOffset
	 *            the starting offset of the considered rxBeam
	 */
	public static UByte getQualityFactor(ByteBuffer datagram, int startingOffset) {
		return TypeDecoder.read1U(datagram, startingOffset + 6);
	}

	/**
	 * return the given value
	 * 
	 * @param datagram
	 *            the datagram buffer
	 * @param startingOffset
	 *            the starting offset of the considered rxBeam
	 */
	public static SByte getDcorr(ByteBuffer datagram, int startingOffset) {
		return TypeDecoder.read1S(datagram, startingOffset + 7);
	}

	/**
	 * return the given value
	 * 
	 * @param datagram
	 *            the datagram buffer
	 * @param startingOffset
	 *            the starting offset of the considered rxBeam
	 */
	// Two Way Travel Times (s)
	public static FFloat getRange(ByteBuffer datagram, int startingOffset) {
		return TypeDecoder.read4F(datagram, startingOffset + 8);
	}

	/**
	 * return the given value
	 * 
	 * @param datagram
	 *            the datagram buffer
	 * @param startingOffset
	 *            the starting offset of the considered rxBeam
	 */
	public static SShort getReflectivity(ByteBuffer datagram, int startingOffset) {
		return TypeDecoder.read2S(datagram, startingOffset + 12);
	}

	/**
	 * return the given value
	 * 
	 * @param datagram
	 *            the datagram buffer
	 * @param startingOffset
	 *            the starting offset of the considered rxBeam
	 */
	public static SByte getRealTimeCleaningInfo(ByteBuffer datagram, int startingOffset) {
		return TypeDecoder.read1S(datagram,  startingOffset + 14);
	}

}
