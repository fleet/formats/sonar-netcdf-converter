package fr.ifremer.globe.api.xsf.converter.common.datagram;

import java.nio.ByteBuffer;

import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.DatagramMetadata;

public interface Datagram {
	public void computeMetaData(AllFile metadata, ByteBuffer datagram, byte datagramType, DatagramPosition position);

	/**
	 * return the specific metadata for the associated with the datagram
	 */
	public default DatagramMetadata<?,?> getSpecificMetadata(AllFile metadata) {
		return metadata.getUnknown();
	}
}
