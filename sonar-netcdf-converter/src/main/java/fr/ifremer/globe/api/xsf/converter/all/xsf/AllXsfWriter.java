package fr.ifremer.globe.api.xsf.converter.all.xsf;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;

import fr.ifremer.globe.api.xsf.converter.XsfConverterParameters;
import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.all.xsf.bathy.Depth68Group;
import fr.ifremer.globe.api.xsf.converter.all.xsf.bathy.QualityFactorGroup;
import fr.ifremer.globe.api.xsf.converter.all.xsf.bathy.RawRange102Group;
import fr.ifremer.globe.api.xsf.converter.all.xsf.bathy.RawRange70Group;
import fr.ifremer.globe.api.xsf.converter.all.xsf.bathy.RawRange78Group;
import fr.ifremer.globe.api.xsf.converter.all.xsf.bathy.Seabed83Group;
import fr.ifremer.globe.api.xsf.converter.all.xsf.bathy.Seabed89Group;
import fr.ifremer.globe.api.xsf.converter.all.xsf.bathy.XYZ88Group;
import fr.ifremer.globe.api.xsf.converter.all.xsf.extra.ExtraDetectionsGroup;
import fr.ifremer.globe.api.xsf.converter.all.xsf.intallation.ExtraParametersGroup;
import fr.ifremer.globe.api.xsf.converter.all.xsf.intallation.InstallationGroup;
import fr.ifremer.globe.api.xsf.converter.all.xsf.intallation.MechanicalTransducerTiltGroup;
import fr.ifremer.globe.api.xsf.converter.all.xsf.runtime.RunTimeGroup;
import fr.ifremer.globe.api.xsf.converter.all.xsf.sensors.AttitudeGroup;
import fr.ifremer.globe.api.xsf.converter.all.xsf.sensors.ClockGroup;
import fr.ifremer.globe.api.xsf.converter.all.xsf.sensors.HeadingGroup;
import fr.ifremer.globe.api.xsf.converter.all.xsf.sensors.HeightGroup;
import fr.ifremer.globe.api.xsf.converter.all.xsf.sensors.PositionGroup;
import fr.ifremer.globe.api.xsf.converter.all.xsf.sensors.SoundSpeedProfileGroup;
import fr.ifremer.globe.api.xsf.converter.all.xsf.sensors.SurfaceSoundSpeedGroup;
import fr.ifremer.globe.api.xsf.converter.all.xsf.sensors.TideGroup;
import fr.ifremer.globe.api.xsf.converter.all.xsf.wc.WCGroup;
import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.utils.processing.VariableComputer;
import fr.ifremer.globe.api.xsf.converter.common.xsf.TransverseGroup;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfFromKongsberg;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfWriter;
import fr.ifremer.globe.netcdf.ucar.NCException;

/**
 * {@link XsfWriter} for .all files
 */
public class AllXsfWriter extends XsfWriter<AllFile, XsfFromKongsberg> {

	@Override
	public XsfFromKongsberg createOutputNcFile(AllFile allFile, XsfConverterParameters params)
			throws NCException, IOException {
		return new XsfFromKongsberg(allFile, params);
	}

	@Override
	public List<IDatagramConverter<AllFile, XsfFromKongsberg>> getGroupConverters(AllFile allFile) {
		List<IDatagramConverter<AllFile, XsfFromKongsberg>> dataGroups = new ArrayList<>();

		// Common groups
		dataGroups.add(new TransverseGroup<>(allFile.getGlobalMetadata().getSwathIds()));

		// Installation group
		if (allFile.getInstallation().datagramCount > 0) {
			dataGroups.add(new InstallationGroup(allFile));
		}

		if (allFile.getExtraParameters().datagramCount > 0) {
			dataGroups.add(new ExtraParametersGroup(allFile));
		}

		if (allFile.getMechanicalTransducerTilt().datagramCount > 0) {
			dataGroups.add(new MechanicalTransducerTiltGroup(allFile));
		}

		// Bathy group
		if (allFile.getXyzdepth().datagramCount > 0) {
			dataGroups.add(new XYZ88Group(allFile));
		}

		if (allFile.getDepth68().datagramCount > 0) {
			dataGroups.add(new Depth68Group(allFile));
		}

		if (allFile.getRawRange70().datagramCount > 0) {
			dataGroups.add(new RawRange70Group(allFile));
		}

		if (allFile.getRawRange102().datagramCount > 0) {
			dataGroups.add(new RawRange102Group());
		}

		if (allFile.getRawRange78().datagramCount > 0) {
			dataGroups.add(new RawRange78Group(allFile));
		}

		// Water Column group
		if (allFile.getWc().datagramCount > 0) {
			dataGroups.add(new WCGroup(allFile));
		}

		// Sensors Group
		if (allFile.getAttitude().datagramCount > 0) {
			dataGroups.add(new AttitudeGroup(allFile));
		}

		if (allFile.getPosition().datagramCount > 0) {
			dataGroups.add(new PositionGroup(allFile));
		}

		if (allFile.getHeading().datagramCount > 0) {
			dataGroups.add(new HeadingGroup(allFile));
		}

		if (allFile.getClock().datagramCount > 0) {
			dataGroups.add(new ClockGroup());
		}

		if (allFile.getHeight().datagramCount > 0) {
			dataGroups.add(new HeightGroup(allFile));
		}

		if (allFile.getTide().datagramCount > 0) {
			dataGroups.add(new TideGroup(allFile));
		}

		// Seabed - snippet group
		if (allFile.getSeabed83().datagramCount > 0) {
			dataGroups.add(new Seabed83Group(allFile));
		}

		if (allFile.getSeabed89().datagramCount > 0) {
			dataGroups.add(new Seabed89Group(allFile));
		}

		// Others group
		if (allFile.getQualityFactor().datagramCount > 0) {
			dataGroups.add(new QualityFactorGroup());
		}

		if (allFile.getExtraDetections().datagramCount > 0) {
			dataGroups.add(new ExtraDetectionsGroup(allFile));
		}

		if (allFile.getRunTime().datagramCount > 0) {
			dataGroups.add(new RunTimeGroup(allFile));
		}

		if (allFile.getSoundSpeedProfile().datagramCount > 0) {
			dataGroups.add(new SoundSpeedProfileGroup(allFile));
		}

		if (allFile.getSurfaceSoundSpeed().datagramCount > 0) {
			dataGroups.add(new SurfaceSoundSpeedGroup(allFile));
		}

		return dataGroups;
	}

	@Override
	public void postProcess(AllFile allFile, XsfFromKongsberg xsf, XsfConverterParameters params, Logger logger)
			throws NCException {
		super.postProcess(allFile, xsf, params, logger);
		logger.info("Compute beam angles");
		VariableComputer.computeBeamAngle(xsf);
		logger.info("Compute multi ping indexes");
		VariableComputer.computeMultipingIndex(xsf);
		logger.info("Fill tide variable");
		VariableComputer.fillSwathTideDraught(allFile, xsf);
		if (allFile.getRawRange70().datagramCount > 0) {
			logger.info("Compute two way travel time");
			VariableComputer.computeTwoWayTravelTime(xsf);
		}
		if (allFile.getDetectionCount() <= 0 || allFile.getWcBeamCount() > 0) {
			logger.info("Platform and Attitude position");
			VariableComputer.fillPlatformAttitudeAndPosition(allFile, xsf);
		}
		// declare missing variable when WC is missing
		logger.info("Fill sample count");
		xsf.getBeamGroup().getSample_count();
	}

}
