package fr.ifremer.globe.api.xsf.converter.kmall.metadata;

import fr.ifremer.globe.api.xsf.converter.common.utils.DateUtil;

public class GlobalMetadata {
	private int datagramCount = 0;
	private long minDate = Long.MAX_VALUE;
	private long maxDate = Long.MIN_VALUE;
	private int modelNumber;

	public void addTime(long epochTime) {
		minDate = Math.min(epochTime, minDate);
		maxDate = Math.max(epochTime, maxDate);
	}

	@Override
	public String toString() {
		return "-->" + this.getClass().getSimpleName() + System.lineSeparator() + "datagram count " + datagramCount
				+ System.lineSeparator() + "model " + modelNumber + System.lineSeparator() + System.lineSeparator()
				+ "date times" + "[" + DateUtil.fromLong(minDate) + "," + DateUtil.fromLong(maxDate) + "]"
				+ System.lineSeparator();

	}

	public void setModelNumber(int modelNumber) {
		this.modelNumber = modelNumber;
	}

	/**
	 * @return the datagramCount
	 */
	public int getDatagramCount() {
		return datagramCount;
	}

	/**
	 * @return the minDate
	 */
	public long getMinDate() {
		return minDate;
	}

	/**
	 * @return the maxDate
	 */
	public long getMaxDate() {
		return maxDate;
	}

	/**
	 * @return the modelNumber
	 */
	public int getModelNumber() {
		return modelNumber;
	}

	/**
	 * increment datagram count
	 */
	public void incDatagram() {
		datagramCount++;
	}

}
