package fr.ifremer.globe.api.xsf.converter.s7k.metadata;

import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;

public class S7KMultipingSequenceMetadata {

	private int maxBeamCount;
	private TreeMap<Integer, S7KSwathMetadata> swaths;
	private boolean hasOptionalData;

	public S7KMultipingSequenceMetadata() {
		swaths = new TreeMap<>();
		hasOptionalData = false;
	}

	public S7KSwathMetadata registerSwath(DatagramPosition position, int multiPingSequence, int beamCount,
			boolean hasOptionalData) {
		S7KSwathMetadata md = new S7KSwathMetadata(position, beamCount, hasOptionalData);
		swaths.put(multiPingSequence, md);
		return md;
	}

	public boolean isComplete(int expectedNumberOfPings) {
		if (expectedNumberOfPings == 1) {
			return swaths.containsKey(0);
		} else {
			for (int i = 1; i < expectedNumberOfPings; i++) {
				if (!swaths.containsKey(i)) {
					return false;
				}
			}
			return true;
		}
	}

	public boolean contains(int multipingSequence) {
		return swaths.containsKey(multipingSequence);
	}

	public int getMaxBeamCount() {
		return this.maxBeamCount;
	}

	public void index() {
		this.maxBeamCount = 0;
		for (S7KSwathMetadata swathM : swaths.values()) {
			this.maxBeamCount = Integer.max(swathM.getNumBeams(), this.maxBeamCount);
			this.hasOptionalData = this.hasOptionalData || swathM.hasOptionalData();
		}
	}

	public Set<Entry<Integer, S7KSwathMetadata>> getEntries() {
		return swaths.entrySet();
	}

	public boolean hasOptionalData() {
		return hasOptionalData;
	}
}
