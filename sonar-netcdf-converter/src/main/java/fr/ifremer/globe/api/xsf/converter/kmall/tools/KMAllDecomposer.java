package fr.ifremer.globe.api.xsf.converter.kmall.tools;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;

import fr.ifremer.globe.api.xsf.converter.kmall.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.DatagramParser;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.DatagramReader;

public class KMAllDecomposer {

    static String file = "/media/exchange/Roger/KMALLFiles/0007_EM2040_140_20160906_1357.kmall";
    static String dest = "/home/lsix/tmp/KMALLFiles/";
    
    public static void main(String[] args) throws FileNotFoundException, IOException {
        try (RandomAccessFile raf = new RandomAccessFile(file, "r")) {
            final DatagramBuffer buffer = new DatagramBuffer();
            final DatagramParser parser = new DatagramParser(true);
            
            final File outFile = new File(dest);
            int dgmCount = 0;
            if (outFile.exists()) {
                outFile.delete();
            }
            outFile.mkdirs();
            
            while (DatagramReader.readDatagram(raf, buffer) > 0) {
                final File byTypeFolder = new File(outFile, parser.getDatagramType(buffer));
                if (!byTypeFolder.exists()) {
                    byTypeFolder.mkdir();
                }
                final File dgmFile = new File(byTypeFolder, String.format("%05d.dgm", dgmCount++));
                FileOutputStream binOut = new FileOutputStream(dgmFile);
                binOut.write(buffer.rawBufferHeader);
                binOut.write(buffer.rawBufferData, 0, buffer.byteBufferData.limit());
                binOut.close();
            }
        }
    }
}
