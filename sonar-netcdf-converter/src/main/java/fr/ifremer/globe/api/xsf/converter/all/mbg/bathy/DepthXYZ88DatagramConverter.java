package fr.ifremer.globe.api.xsf.converter.all.mbg.bathy;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.all.datagram.xyz88.XYZ88Depth;
import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgBase;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgConstants;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgConstants.SoundingValidity;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgUtils;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2S1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2S1WithHScale;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2S2;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2S4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2S4WithPosition;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2U2;
import fr.ifremer.globe.api.xsf.util.types.SByte;
import fr.ifremer.globe.api.xsf.util.types.SInt;
import fr.ifremer.globe.api.xsf.util.types.SShort;
import fr.ifremer.globe.api.xsf.util.types.UShort;
import fr.ifremer.globe.netcdf.api.convention.CFStandardNames;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCVariable;

/**
 * Converter for XYZ Depth datagram
 */
public class DepthXYZ88DatagramConverter extends DepthDatagramConverter {

	/** Constructor **/
	public DepthXYZ88DatagramConverter(AllFile stats) {
		super(stats.getXyzdepth());
	}

	@Override
	public void declare(AllFile allFile, MbgBase mbg) throws NCException {
		super.declare(allFile, mbg);

		/** Depth **/
		NCVariable depthVariable = mbg.getVariable(MbgConstants.Depth);
		double depthFactor = depthVariable.getAttributeDouble(CFStandardNames.CF_SCALE_FACTOR);
		double depthOffset = depthVariable.getAttributeDouble(CFStandardNames.CF_ADD_OFFSET);
		swathBeamValuesWithPosition
				.add(new ValueD2S4WithPosition(depthVariable, (buffer, iBeam, lat, lon, speed, positionMetadata) -> {
					// values as double to avoid a lost of precision during the next addition
					double depth = XYZ88Depth.getDepth(buffer.byteBufferData, iBeam).get();
					double transmitDepth = XYZ88Depth.getTransmitTransducerDepth(buffer.byteBufferData).get();
					if (depth > 0 && depth < Integer.MAX_VALUE) {
						double value = (depth + transmitDepth - depthOffset) / depthFactor;

						// computes geographic bounding box with valid detection
						int detectionInfo = XYZ88Depth.getDetectionInformation(buffer.byteBufferData, iBeam).getInt();
						if ((detectionInfo & 0X80) == 0) {
							double[] detectionLatLon = getDetectionPosition(buffer, iBeam, lat, lon);
							allFile.getDetectionBB().computeBoundingBoxLat(detectionLatLon[0]);
							allFile.getDetectionBB().computeBoundingBoxLon(detectionLatLon[1]);
						}
						return (int) Math.round(value);
					}
					return 0;
				}));

		/** Heading **/
		NCVariable headingVariable = mbg.getVariable(MbgConstants.Heading);
		double headingScaleFactor = headingVariable.getAttributeDouble(CFStandardNames.CF_SCALE_FACTOR);
		double headingOffset = headingVariable.getAttributeDouble(CFStandardNames.CF_ADD_OFFSET);
		swathAntennaValues.add(new ValueD2S2(headingVariable, (buffer, beamIndexSource) -> new SShort(
				(short) Math.round((XYZ88Depth.getVesselHeading(buffer.byteBufferData).getU() / 100. - headingOffset)
						/ headingScaleFactor))));

		/** Reflectivity **/
		NCVariable reflectivityVariable = mbg.getVariable(MbgConstants.Reflectivity);
		double reflectivityFactor = reflectivityVariable.getAttributeDouble(CFStandardNames.CF_SCALE_FACTOR);
		double reflectivityOffset = reflectivityVariable.getAttributeDouble(CFStandardNames.CF_ADD_OFFSET);
		swathBeamValues.add(new ValueD2S1(reflectivityVariable,
				(buffer, iBeam) -> new SByte((byte) Math.round(
						(XYZ88Depth.getReflectivity(buffer.byteBufferData, iBeam).get() / 10.0 - reflectivityOffset)
								/ reflectivityFactor))));

		/** Quality **/
		NCVariable qualityVariable = mbg.getVariable(MbgConstants.SQuality);
		int qualityFactor = qualityVariable.getAttributeInt(CFStandardNames.CF_SCALE_FACTOR);
		int qualityOffset = qualityVariable.getAttributeInt(CFStandardNames.CF_ADD_OFFSET);
		swathBeamValues.add(new ValueD2S1(qualityVariable, (buffer, iBeam) -> {
			byte quality = XYZ88Depth.getQF(buffer.byteBufferData, iBeam).getByte();
			int detectionInfo = XYZ88Depth.getDetectionInformation(buffer.byteBufferData, iBeam).getInt();
			if ((detectionInfo & 0X80) == 0 && (detectionInfo & 0X0F) == 1) {
				// phase detection
				quality += 128; // negative
			}
			return new SByte((byte) ((quality - qualityOffset) / qualityFactor));
		}));

		/** Immersion **/
		NCVariable refDepthVariable = mbg.getVariable(MbgConstants.Immersion);
		double refDepthFactor = refDepthVariable.getAttributeDouble(CFStandardNames.CF_SCALE_FACTOR);
		double refDepthOffset = refDepthVariable.getAttributeDouble(CFStandardNames.CF_ADD_OFFSET);
		swathAntennaValues.add(new ValueD2S4(refDepthVariable, (buffer, iBeam) -> new SInt(
				(int) Math.round(((XYZ88Depth.getTransmitTransducerDepth(buffer.byteBufferData).get() - refDepthOffset)
						/ refDepthFactor)))));

		/** Sampling rate **/
		NCVariable sampRateVariable = mbg.getVariable(MbgConstants.SamplingRate);
		int sampRateFactor = sampRateVariable.getAttributeInt(CFStandardNames.CF_SCALE_FACTOR);
		int sampRateOffset = sampRateVariable.getAttributeInt(CFStandardNames.CF_ADD_OFFSET);
		swathAntennaValues.add(new ValueD2S2(sampRateVariable,
				(buffer, iBeam) -> new SShort(
						(short) ((XYZ88Depth.getSamplingFrequency(buffer.byteBufferData).get() - sampRateOffset)
								/ sampRateFactor))));

		/** SLengthOfDetection **/
		NCVariable windowLengthVariable = mbg.getVariable(MbgConstants.SLengthOfDetection);
		int windowLengthFactor = windowLengthVariable.getAttributeInt(CFStandardNames.CF_SCALE_FACTOR);
		int windowLengthOffset = windowLengthVariable.getAttributeInt(CFStandardNames.CF_ADD_OFFSET);
		swathBeamValues.add(new ValueD2S1(windowLengthVariable, (buffer, iBeam) -> {
			short a = (short) (XYZ88Depth.getWindowsLengthInSample(buffer.byteBufferData, iBeam).getU() / 4);
			byte b = (byte) (a > 254 ? 254 : a);
			return new SByte((byte) ((b - windowLengthOffset) / windowLengthFactor));
		}));

		/** Sound velocity **/
		NCVariable soundVelVariable = mbg.getVariable(MbgConstants.SoundVelocity);
		double soundVelFactor = soundVelVariable.getAttributeDouble(CFStandardNames.CF_SCALE_FACTOR);
		double soundVelOffset = soundVelVariable.getAttributeDouble(CFStandardNames.CF_ADD_OFFSET);
		swathAntennaValues.add(new ValueD2U2(soundVelVariable, (buffer, iBeam) -> new UShort(
				(short) Math.round((XYZ88Depth.getSoundSpeedAtTx(buffer.byteBufferData).getU() / 10.0 - soundVelOffset)
						/ soundVelFactor))));

		/** Distance scale **/
		NCVariable distanceScaleVariable = mbg.getVariable(MbgConstants.DistanceScale);
		double scaleFactor = distanceScaleVariable.getAttributeDouble(CFStandardNames.CF_SCALE_FACTOR);
		double scaleOffset = distanceScaleVariable.getAttributeDouble(CFStandardNames.CF_ADD_OFFSET);
		swathAntennaValuesWithHScale.add(new ValueD2S1WithHScale(distanceScaleVariable,
				(buffer, beamIndexSource, hScale) -> new SByte((byte) ((hScale - scaleOffset) / scaleFactor))));

		/** Vertical depth **/
		NCVariable verticalDepthVariable = mbg.getVariable(MbgConstants.VerticalDepth);
		double vertDepthScaleFactor = verticalDepthVariable.getAttributeDouble(CFStandardNames.CF_SCALE_FACTOR);
		swathAntennaValues.add(new ValueD2S4(verticalDepthVariable, (buffer, antennaIndex) -> {
			// get the beam with the minimum across distance
			float minAcrossDistance = Float.MAX_VALUE;
			int verticalBeamIndex = 0;
			int beamCount = XYZ88Depth.getBeamCount(buffer.byteBufferData).getU();
			for (int i = 0; i < beamCount; i++) {
				int detectionInfo = XYZ88Depth.getDetectionInformation(buffer.byteBufferData, i).getInt();
				float acrossDistance = Math.abs(XYZ88Depth.getAcrossDistance(buffer.byteBufferData, i).get());
				if ((detectionInfo & 0X80) == 0 && acrossDistance < minAcrossDistance) {
					verticalBeamIndex = i;
					minAcrossDistance = acrossDistance;
				}
			}

			// get depth
			double verticalDepth = XYZ88Depth.getDepth(buffer.byteBufferData, verticalBeamIndex).get();
			verticalDepth += XYZ88Depth.getTransmitTransducerDepth(buffer.byteBufferData).get();
			return new SInt((int) Math.round(verticalDepth * (1 / vertDepthScaleFactor)));
		}));

	}

	@Override
	protected int getBeamCount(DatagramBuffer buffer) {
		return XYZ88Depth.getBeamCount(buffer.byteBufferData).getU();
	}

	@Override
	protected double computeHorizontaleScale(DatagramBuffer buffer) {
		int beamCount = XYZ88Depth.getBeamCount(buffer.byteBufferData).getU();
		float max = Float.MIN_VALUE;
		for (int i = 0; i < beamCount; i++) {
			if (getDectectionValidity(buffer, i) != SoundingValidity.MISSING) {
				float acrossDistance = Math.abs(XYZ88Depth.getAcrossDistance(buffer.byteBufferData, i).get());
				float alongDistance = Math.abs(XYZ88Depth.getAlongTrackDistance(buffer.byteBufferData, i).get());
				if (acrossDistance > max)
					max = acrossDistance;
				if (alongDistance > max)
					max = alongDistance;
			}
		}
		return MbgUtils.getHorizontalScale(max);
	}

	@Override
	protected SoundingValidity getDectectionValidity(BaseDatagramBuffer buffer, int iBeam) {
		float depth = XYZ88Depth.getDepth(buffer.byteBufferData, iBeam).get();
		int detectionInfo = XYZ88Depth.getDetectionInformation(buffer.byteBufferData, iBeam).getInt();
		if ((detectionInfo & 0X80) == 0) {
			return SoundingValidity.VALID;
		} else if (depth > 0 && depth < Integer.MAX_VALUE) {
			return SoundingValidity.UNVALID;
		}
		return SoundingValidity.MISSING;
	}

	@Override
	protected double getAlongDistance(BaseDatagramBuffer buffer, int iBeam) {
		return XYZ88Depth.getAlongTrackDistance(buffer.byteBufferData, iBeam).get();
	}

	@Override
	protected double getAcrossDistance(BaseDatagramBuffer buffer, int iBeam) {
		return XYZ88Depth.getAcrossDistance(buffer.byteBufferData, iBeam).get();
	}

	@Override
	protected double getVesselHeading(BaseDatagramBuffer buffer) {
		return XYZ88Depth.getVesselHeading(buffer.byteBufferData).getU() / 100.;
	}

}
