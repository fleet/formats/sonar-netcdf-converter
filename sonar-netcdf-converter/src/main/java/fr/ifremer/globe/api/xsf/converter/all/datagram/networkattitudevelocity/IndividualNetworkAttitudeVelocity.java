package fr.ifremer.globe.api.xsf.converter.all.datagram.networkattitudevelocity;

import fr.ifremer.globe.api.xsf.converter.all.datagram.IndividualSensorMetadata;

public class IndividualNetworkAttitudeVelocity extends IndividualSensorMetadata {

    private int maxRawSensorInputLen;
    
    public IndividualNetworkAttitudeVelocity() {
        maxRawSensorInputLen = 0;
    }
    
    public void addRawSensorInputLen(int len) {
        this.maxRawSensorInputLen = Integer.max(this.maxRawSensorInputLen, len);
    }

    public int getMaxRawSensorInputLen() {
        return maxRawSensorInputLen;
    }
}
