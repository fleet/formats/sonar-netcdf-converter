package fr.ifremer.globe.api.xsf.converter.s7k.metadata;

import java.util.Map.Entry;

import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.DatagramMetadata;

public class S7KMultipingMetadata extends DatagramMetadata<Long, S7KMultipingSequenceMetadata> {
	
	private int maxBeamCount;
	private int sequenceLength;
	private boolean hasOptionalData;
	
	public S7KMultipingMetadata() {
		super();
		sequenceLength = 0;
		hasOptionalData = false;
	}
	
	public S7KMultipingSequenceMetadata getOrCreatePingSequence(long pingId) {
		S7KMultipingSequenceMetadata md = index.get(pingId);
		if (md == null) {
			md = new S7KMultipingSequenceMetadata();
			index.put(pingId, md);
		}
		return md;
	}
	
	public S7KSwathMetadata registerSwath(DatagramPosition position, long pingId, int multiPingSequence, int beamCount, boolean hasOptionalData) {
		addDatagram();
		S7KMultipingSequenceMetadata md = getOrCreatePingSequence(pingId);
		sequenceLength = Integer.max(sequenceLength, multiPingSequence);
		return md.registerSwath(position, multiPingSequence, beamCount,  hasOptionalData);
	}

	public int getSequenceLength() {
		return sequenceLength;
	}
	
	public int getMaxBeamCount() {
		return this.maxBeamCount;
	}
	
	public void index() {
		this.maxBeamCount = 0;
		for (Entry<Long, S7KMultipingSequenceMetadata> entry : getEntries()) {
			entry.getValue().index();
			this.maxBeamCount = Integer.max(this.maxBeamCount, entry.getValue().getMaxBeamCount());
			this.hasOptionalData = this.hasOptionalData || entry.getValue().hasOptionalData();
		}
	}

	public boolean hasOptionalData() {
		return this.hasOptionalData;
	}
}
