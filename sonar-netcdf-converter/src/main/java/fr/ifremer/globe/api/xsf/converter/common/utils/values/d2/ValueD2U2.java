package fr.ifremer.globe.api.xsf.converter.common.utils.values.d2;

import java.util.Arrays;

import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.api.xsf.util.types.UShort;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCVariable;

/***
 * class allocating storage for 2D matrix, and giving a interface allowing to fill the data *
 */
public class ValueD2U2 extends ValueD2 {
	protected short[] dataOut;
	private ValueProvider valueProvider;
	private UShort fillValue;

	/***
	 * Constructor, allocate netcdf buffer for one variable and one ping
	 */
	public ValueD2U2(NCVariable variable, ValueProvider filler) {
		super(variable);
		// allocate storage
		this.dataOut = new short[dim];
		this.fillValue=new UShort(variable.getShortFillValue());
		clear();
		this.valueProvider = filler;

	}
	
	@Override
	public void clear() {
		Arrays.fill(this.dataOut,fillValue.getShort());
	}

	public ValueD2U2(NCVariable variable, int dim, ValueProvider filler) {
		super(variable, dim);
		// allocate storage
		this.dataOut = new short[dim];
		this.valueProvider = filler;
		this.fillValue=new UShort(variable.getShortFillValue());
		clear();

	}

	public ValueD2U2(NCVariable variable, int dim, ValueProvider filler, UShort fillValue) {
		this(variable, dim, filler);
		setDefault(fillValue);
		this.fillValue=fillValue;
	}

	public ValueD2U2(NCVariable variable, ValueProvider filler, UShort fillValue) {
		this(variable, filler);
		this.fillValue=fillValue;
		setDefault(fillValue);
	}

	public void setDefault(UShort dft) {
		for (int i = 0; i < dataOut.length; i++) {
			dataOut[i] = dft.getShort();
		}
	}

	public interface ValueProvider {
		public UShort get(BaseDatagramBuffer buffer, int beamIndexSource);
	}

	/**
	 * call the lambda to retrieve the value and set it in the allocated netcdf buffer dataOut
	 */
	@Override 
	public void fill(BaseDatagramBuffer buffer, int beamIndexSource, int beamIndexDest) {
		try {
			dataOut[beamIndexDest] = valueProvider.get(buffer, beamIndexSource).getShort();
		} catch (Exception e) {
			logger.error(String.format("Error while writing variable %s (%s)", variable.getName(), e.getMessage()));
			throw e;
		}
	}

	@Override
	public void write(long[] origin, long[] count) throws NCException {
		try {
			variable.putu(origin, count, dataOut);
		} catch (Exception e) {
			logger.error(String.format("Error while writing variable %s (%s)", variable.getName(), e.getMessage()));
			throw e;
		}
	}
}
