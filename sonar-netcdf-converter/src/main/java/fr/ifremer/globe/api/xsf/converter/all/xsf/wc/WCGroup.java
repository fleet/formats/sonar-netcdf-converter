package fr.ifremer.globe.api.xsf.converter.all.xsf.wc;

import com.sun.jna.Memory;
import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.all.datagram.runtime.RunTimeDg;
import fr.ifremer.globe.api.xsf.converter.all.datagram.wc.WC;
import fr.ifremer.globe.api.xsf.converter.all.datagram.wc.WCCompleteAntennaPingMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.wc.WCCompletePingMetadata;
import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagram;
import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.*;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.*;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfFromKongsberg;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.BeamGroup1Grp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.BeamGroup1VendorSpecificGrp;
import fr.ifremer.globe.api.xsf.util.DefaultLogger;
import fr.ifremer.globe.api.xsf.util.ObjectBufferPool;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.types.*;
import fr.ifremer.globe.netcdf.jna.Nc4prototypes.Vlen_t;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCVariable;
import fr.ifremer.globe.netcdf.util.Pair;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.*;
import java.util.Map.Entry;

public class WCGroup implements IDatagramConverter<AllFile, XsfFromKongsberg> {

	protected BeamGroup1Grp beamGrp;
	private AllFile stats;
	private ArrayList<ValueD2> values2DRxAntenna = new ArrayList<>();
	private ArrayList<ValueD1> valuesSwath = new ArrayList<>();
	private ArrayList<ValueD2> valuesTx = new ArrayList<>();
	private ArrayList<ValueD2Composite> valuesTxComposite = new ArrayList<>();
	private ArrayList<ValueD2> valuesSwathBeam = new ArrayList<>();
	private ArrayList<ValueD2Composite> swathBeamRuntime = new ArrayList<>();

	public WCGroup(AllFile stats) {
		this.stats = stats;
	}

	@Override
	public void declare(AllFile allFile, XsfFromKongsberg xsf) throws NCException {
		beamGrp = xsf.getBeamGroup();
		// declare dimension

		BeamGroup1VendorSpecificGrp beamVendorGroup = xsf.getBeamGroupVendorSpecific();

		valuesSwath.add(new ValueD1U4(beamVendorGroup.getPing_raw_count(),
				(buffer) -> new UInt(WC.getPingNumber(buffer.byteBufferData).getU())));

		valuesSwath.add(new ValueD1S1(beamVendorGroup.getPing_validity(), (buffer) -> new SByte((byte) 0)));

		valuesSwath.add(new ValueD1U8(beamGrp.getPing_time(),
				buffer -> new ULong(BaseDatagram.getXSFEpochTime(buffer.byteBufferData))));

		valuesSwath.add(new ValueD1F4(beamGrp.getSound_speed_at_transducer(),
				buffer -> new FFloat(WC.getSoundSpeed(buffer.byteBufferData).getU() * 0.1f)));

		valuesSwath.add(new ValueD1F4(beamGrp.getSample_interval(),
				buffer -> new FFloat(1 / (WC.getSamplingFreq(buffer.byteBufferData).getU() * 0.01f))));

		values2DRxAntenna.add(new ValueD2F4(beamVendorGroup.getTx_heave(),
				(buffer, rxantennaidx) -> new FFloat(WC.getTxTimeHeave(buffer.byteBufferData).get() * 0.01f)));

		values2DRxAntenna.add(new ValueD2U1(beamVendorGroup.getTvg_function_applied(),
				(buffer, i) -> WC.getTVGFunction(buffer.byteBufferData)));

		values2DRxAntenna.add(
				new ValueD2S1(beamVendorGroup.getTvg_offset(), (buffer, i) -> WC.getTVGOffset(buffer.byteBufferData)));

		values2DRxAntenna.add(new ValueD2U1(beamVendorGroup.getScanning_info(),
				(buffer, i) -> WC.getScanningInfo(buffer.byteBufferData)));

		values2DRxAntenna.add(new ValueD2U2(beamVendorGroup.getBeam_count(),
				(buffer, i) -> WC.getReceivedBeamCount(buffer.byteBufferData)));

		/** TX sector related data */
		valuesTx.add(new ValueD2U2(beamVendorGroup.getCenter_frequency(),
				(buffer, i) -> WC.getTxCenterFq(buffer.byteBufferData, i)));

		valuesTx.add(new ValueD2F4(xsf.getBeamGroupVendorSpecific().getRaw_tx_beam_tilt_angle(), (buffer, i) -> {
			double angleDeg = WC.getTxTilt(buffer.byteBufferData, i).get() * 0.01f;

			return new FFloat((float) angleDeg);
		}));

		// the frequencies are retrieved from Raw Range and beam angle datagram
		// valuesTx.add(new ValueD2F4(beamVendorGroup.getTxCenterFrequency(), (buffer, i) -> new
		// FFloat(WC.getTxCenterFq(buffer.byteBufferData, i).get()*10f)));

		// /** Beam related data */

		valuesSwathBeam.add(new ValueD2U1(beamVendorGroup.getWc_validity(), (buffer, offset) -> new UByte((byte) 1),
				new UByte((byte) 0)));

		// we use default values for X direction
		valuesSwathBeam.add(new ValueD2F4(beamVendorGroup.getRaw_rx_beam_pointing_angle(), (buffer, offset) -> {
			// compute beam pointing angle relative to transducer face
			// TODO prevoir un refactoring pour pouvoir traiter les navigations interpolées et ensuite gérer les
			// changements de reperes
			double angleDeg = WC.getBeamPointingAngle(buffer.byteBufferData, offset).get() * 0.01;

			return new FFloat((float) angleDeg);
		}));

		swathBeamRuntime.add(
				new ValueD2F4Composite(beamGrp.getBeamwidth_receive_major(), (buffer, beam, optionalRuntime) -> {
					if (optionalRuntime.isPresent()) {
						BaseDatagramBuffer runtime = optionalRuntime.get();
						float beamWidth = RunTimeDg.getReceiveBeamwidth(runtime.byteBufferData).getU() * 0.1f;
						float beamPointingAngle = WC.getBeamPointingAngle(buffer.byteBufferData, beam).get() * 0.01f;
						// approximated beam width is given by angle/cos(pointingAngle)
						return new FFloat((float) Math.abs(beamWidth / Math.cos(Math.toRadians(beamPointingAngle))));
					} else
						return new FFloat(Float.NaN);
				}));
		swathBeamRuntime.add(new ValueD2F4Composite(beamGrp.getBeamwidth_receive_minor(),
				(buffer, beam, optionalRuntime) -> new FFloat(Float.NaN)));
		// we assume a square opening angle

		valuesTxComposite.add(new ValueD2F4Composite(beamGrp.getBeamwidth_transmit_minor(),
				(buffer, sectorNumber, optionalRuntime) -> {
					if (optionalRuntime.isPresent()) {
						BaseDatagramBuffer runtime = optionalRuntime.get();
						float beamWidth = RunTimeDg.getTransmitBeamwidth(runtime.byteBufferData).getU() * 0.1f;
						float txTiltAngle = WC.getTxTilt(buffer.byteBufferData, sectorNumber).get() * 0.01f;
						// approximated beam width is given by angle/cos(pointingAngle)
						return new FFloat((float) Math.abs(beamWidth / Math.cos(Math.toRadians(txTiltAngle))));
					} else
						return new FFloat(Float.NaN);
				}));
		valuesTxComposite.add(new ValueD2F4Composite(beamGrp.getBeamwidth_transmit_major(),
				(buffer, beam, optionalRuntime) -> new FFloat(Float.NaN)));
		valuesSwathBeam.add(new ValueD2F4(beamGrp.getBlanking_interval(), (buffer, offset) -> new FFloat(
				WC.getStartRange(buffer.byteBufferData, offset).getU() / (
						WC.getSamplingFreq(buffer.byteBufferData).getU() * 0.01f))));

		valuesSwathBeam.add(new ValueD2F4(beamVendorGroup.getBeam_detection_range(), (buffer, offset) -> new FFloat(
				WC.getDetectedRangeInSample(buffer.byteBufferData, offset).getU() * 1.f)));

		valuesSwathBeam.add(new ValueD2F4(beamGrp.getDetected_bottom_range(), (buffer, offset) -> {
			double soundSpeed = WC.getSoundSpeed(buffer.byteBufferData).getU() * 0.1;
			double sampleRate = WC.getSamplingFreq(buffer.byteBufferData).getU() * 0.01;
			double rangeInSample = WC.getDetectedRangeInSample(buffer.byteBufferData, offset).getU() * 1f;
			double rangeInMeter = soundSpeed * rangeInSample / (2 * sampleRate);
			return new FFloat((float) rangeInMeter);
		}));

		valuesSwathBeam.add(new ValueD2U1(beamGrp.getTransmit_beam_index(),
				(buffer, offset) -> WC.getTransmitSectorNumber(buffer.byteBufferData, offset)));

		// declare backscatter variable
		beamGrp.getBackscatter_r();
		beamGrp.getBackscatter_r().addAttribute("scale_factor", 0.5f);// scale factor of 0.5f allowing to convert raw
		// amplitude to float values

		// declare sample_count variable
		beamGrp.getSample_count();

	}

	@Override
	public void fillData(AllFile allFile) throws NCException, IOException {
		long origin[] = { 0, 0 };
		// allocate buffer pool
		TreeMap<Long, BaseDatagramBuffer> runtimes = stats.getRunTime().getSortedDatagrams();

		ObjectBufferPool<DatagramBuffer> pool = new ObjectBufferPool<>(() -> new DatagramBuffer(stats.getByteOrder()));
		/** Rx Beam related data */

		boolean firstPing = true;
		int[] received_transducer_index = null;
		if (firstPing) {
			received_transducer_index = new int[stats.getWc().getMaxBeamCount()];
			Arrays.fill(received_transducer_index, 0); // fill default values
		}

		int[] sampleCountVariable = new int[stats.getWcBeamCount()]; // number of sample per beam

		for (Entry<SwathIdentifier, WCCompletePingMetadata> pingEntry : stats.getWc().getPings()) {
			values2DRxAntenna.forEach(ValueD2::clear);
			valuesTx.forEach(ValueD2::clear);
			valuesTxComposite.forEach(ValueD2Composite::clear);
			valuesSwathBeam.forEach(ValueD2::clear);
			swathBeamRuntime.forEach(ValueD2Composite::clear);
			Arrays.fill(sampleCountVariable, 0); // fill default values

			final WCCompletePingMetadata ping = pingEntry.getValue();
			// If we skip WC or bathymetry we could have invalid time value remaining in datasets

			if (!allFile.getGlobalMetadata().getSwathIds().contains(pingEntry.getKey())) {
				continue;
			}

			// first read datagrams
			List<Pair<DatagramBuffer, WCCompleteAntennaPingMetadata>> data = DatagramReader.read(pool, ping.locate());

			// get origin from swath identifier (ignore if matching index not found)
			var swathId = pingEntry.getKey();
			var optIndex = stats.getGlobalMetadata().getSwathIds().getIndex(swathId);
			if (optIndex.isEmpty()) {
				DefaultLogger.get()
						.warn("Datagram MRZ associated with a missing ping is ignored (number = {}, sequence = {}).",
								swathId.getRawPingNumber(), swathId.getSwathPosition());
				continue;
			}
			origin[0] = optIndex.get();

			final int beamCount = ping.getBeamCount();
			final int ntx = ping.getNumTxSector();
			int beamIndex;
			Vlen_t[] tab = (Vlen_t[]) new Vlen_t().toArray(stats.getWc().getMaxBeamCount());

			for (Pair<DatagramBuffer, WCCompleteAntennaPingMetadata> p : data) {
				DatagramBuffer buf = p.getFirst();
				long currentTime = BaseDatagram.getEpochTime(buf.byteBufferData);
				Entry<Long, BaseDatagramBuffer> found = runtimes.floorEntry(currentTime);
				// cannot find runtime datagram, as it could be a time stamp bug or missing datagram we just try to use
				// the next one, even if it is not the right one
				if (found == null) {
					found = runtimes.ceilingEntry(currentTime);
				}
				Optional<BaseDatagramBuffer> runtimeParameter = (found != null)
						? Optional.of(found.getValue())
						: Optional.empty();

				for (ValueD1 v : valuesSwath) {
					v.fill(buf);
				}

				for (ValueD2 v : values2DRxAntenna) {
					DatagramBuffer buffer = data.get(0).getFirst();
					v.fill(buffer, 0,
							stats.getInstallation().getRxAntennaIndex(WC.getSerialNumber(buf.byteBufferData)));
				}
				for (int i = 0; i < ntx; i++) {
					int txSector = WC.getTxSectorIndex(buf.byteBufferData, i).getU();
					for (ValueD2 v : valuesTx) {
						v.fill(buf, i, txSector);
					}
					for (ValueD2Composite v : valuesTxComposite) {
						v.fill(buf, txSector, runtimeParameter, txSector);
					}
				}
				int transducerIndex = 0;
				if (firstPing) {
					// fill rxbeam info buffer
					transducerIndex = stats.getInstallation().getRxAntennaIndex(WC.getSerialNumber(buf.byteBufferData));
				}

				final int beamOffset = ping.getBeamOffset(WC.getSerialNumber(buf.byteBufferData).getU()) + p.getSecond()
						.getBeamOffset(WC.getDatagramNumber(buf.byteBufferData).getU() - 1);
				int offset = WC.getRXOffset(buf.byteBufferData);
				final int nRx = WC.getReceivedBeamCount(buf.byteBufferData).getU();
				for (int i = 0; i < nRx; i++) {
					beamIndex = beamOffset + i;
					if (firstPing) {
						received_transducer_index[beamIndex] = transducerIndex;

					}
					for (ValueD2 v : valuesSwathBeam) {
						v.fill(buf, offset, beamIndex);
					}
					for (ValueD2Composite v : swathBeamRuntime) {
						// find latest runtime parameter
						v.fill(buf, offset, runtimeParameter, beamIndex);
					}
					int sampleCount = WC.getSampleCount(buf.byteBufferData, offset).getU();
					// TODO avoid double memory allocation
					/**
					 *
					 * byte[] samples = WC.getSamples(buf.byteBufferData, offset); Memory m=new Memory(sampleCount);
					 * m.write(0, samples, 0, sampleCount); tab[beamIndex].p=m; tab[beamIndex].len=sampleCount;
					 */
					int bufferlength = sampleCount;
					Memory m = new Memory(bufferlength);
					ByteBuffer buffer = m.getByteBuffer(0, bufferlength);
					WC.getSamples(buf.byteBufferData, offset, buffer);
					tab[beamIndex].p = m;
					tab[beamIndex].len = sampleCount;
					offset = WC.getNextRxOffset(buf.byteBufferData, offset);
					sampleCountVariable[beamIndex] = sampleCount;
				}

			}
			beamGrp.getBackscatter_r()
					.put(new long[] { origin[0], 0, 0 }, new long[] { 1, stats.getWc().getMaxBeamCount(), 1 }, tab);

			// write indicative variable beam sample count
			beamGrp.getSample_count()
					.put(new long[] { origin[0], 0, 0 }, new long[] { 1, stats.getWc().getMaxBeamCount(), 1 },
							sampleCountVariable);

			// write rxbeam constant values
			if (firstPing) {
				NCVariable variable = beamGrp.getReceive_transducer_index();
				try {
					variable.put(new long[] { 0 }, new long[] { received_transducer_index.length },
							received_transducer_index);
				} catch (Exception e) {
					DefaultLogger.get().error(String.format("Error while writing variable %s (%s)", variable.getName(),
							e.getMessage()));
					throw e;
				}
			}

			/*
			 * Effectively write datasets
			 */
			long[] originD1 = new long[] { origin[0] };

			for (ValueD1 v : valuesSwath) {
				v.write(originD1);
			}
			for (ValueD2 v : values2DRxAntenna) {
				v.write(origin, new long[] { 1, stats.getGlobalMetadata().getSerialNumbers().size() });
			}

			for (ValueD2 v : valuesTx) {
				v.write(origin, new long[] { 1, ntx });
			}
			for (ValueD2Composite v : valuesTxComposite) {
				v.write(origin, new long[] { 1, ntx });
			}
			for (ValueD2 v : valuesSwathBeam) {
				v.write(origin, new long[] { 1, beamCount });
			}
			for (ValueD2Composite v : swathBeamRuntime) {
				v.write(origin, new long[] { 1, beamCount });
			}

			// release the datagram buffers for the next ping
			for (Pair<DatagramBuffer, WCCompleteAntennaPingMetadata> p : data) {
				pool.release(p.getFirst());
			}
			firstPing = false;
		}
	}

}
