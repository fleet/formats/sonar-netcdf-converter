package fr.ifremer.globe.api.xsf.converter.kmall.xsf;

import java.io.IOException;
import java.util.ArrayList;

import fr.ifremer.globe.api.xsf.converter.all.datagram.installation.InstallationMetadata.AntennaType;
import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.utils.Angles;
import fr.ifremer.globe.api.xsf.converter.common.utils.Namer;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfConstants;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfFromKongsberg;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.BeamGroup1Grp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.PlatformGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.SonarGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.types.transducer_type_t;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.IIPMetadata;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.IIPMetadata.InternalLever;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.IIPMetadata.MRU;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.IIPMetadata.Positions;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.IIPMetadata.Tray;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.KmallFile;
import fr.ifremer.globe.netcdf.ucar.NCException;

public class IIPConverter implements IDatagramConverter<KmallFile, XsfFromKongsberg> {

	private PlatformGrp platformGrp;
	private BeamGroup1Grp beamGrp;
	private SonarGrp sonarGrp;

	@Override
	public void declare(KmallFile kmAllFile, XsfFromKongsberg xsf) throws NCException {
		platformGrp = xsf.getPlatformGrp();
		sonarGrp = xsf.getSonarGrp();
		beamGrp = xsf.getBeamGroup();
		// this is kmall specific
		xsf.getPlatformVendorGrp().addAttribute("installation_raw_data_kmall",
				kmAllFile.getIIPMetadata().getInstallTxt());
	}

	@Override
	public void fillData(KmallFile kmAllFile) throws IOException, NCException {
		IIPMetadata metadata = kmAllFile.getIIPMetadata();
		fillTransducerData(metadata);
		fillMRUData(metadata);
		fillPositionData(metadata);
		platformGrp.getWater_level().putScalar(metadata.getWaterlineVerticalLocation());
		fillSonarData(metadata);
	}

	private void fillPositionData(IIPMetadata metadata) throws NCException {
		int i = 0;
		for (Positions p : metadata.getPositions()) {
			long[] origin = new long[] { i };
			long[] count = new long[] { 1 };
			int positionId = p.index; // kmall positionId == index

			platformGrp.getPosition_offset_x().put(origin, count, new float[] { p.x });
			platformGrp.getPosition_offset_y().put(origin, count, new float[] { p.y });
			platformGrp.getPosition_offset_z().put(origin, count, new float[] { p.z });
			platformGrp.getPosition_ids().put(origin, count,
					new String[] { Namer.getPositionSubGroupName(positionId) });

			// for loop increment
			i++;
		}
	}

	private void fillMRUData(IIPMetadata metadata) throws NCException {
		int i = 0;
		int activeMruIndex = 0;
		for (MRU mru : metadata.getMrus()) {
			long[] origin = new long[] { i };
			long[] count = new long[] { 1 };
			int mruId = mru.index;

			platformGrp.getMRU_offset_x().put(origin, count, new float[] { mru.x });
			platformGrp.getMRU_offset_y().put(origin, count, new float[] { mru.y });
			platformGrp.getMRU_offset_z().put(origin, count, new float[] { mru.z });
			platformGrp.getMRU_rotation_z().put(origin, count, new float[] { mru.h });
			platformGrp.getMRU_rotation_x().put(origin, count, new float[] { mru.r });
			platformGrp.getMRU_rotation_y().put(origin, count, new float[] { mru.p });
			platformGrp.getMRU_ids().put(origin, count, new String[] { Namer.getAttitudeSubGroupName(mruId) });
			if (mru.u.compareTo("ACTIVE") == 0)
				activeMruIndex = i;

			// for loop increment
			i++;
		}
		beamGrp.addAttribute(BeamGroup1Grp.ATT_PREFERRED_MRU, activeMruIndex);
	}

	private void fillSonarData(IIPMetadata data) throws NCException {
		short serialId = data.getSerialId();
		sonarGrp.addAttribute(SonarGrp.ATT_SONAR_MANUFACTURER, "Kongsberg");
		sonarGrp.addAttribute(SonarGrp.ATT_SONAR_MODEL, data.getModel());
		sonarGrp.addAttribute(SonarGrp.ATT_SONAR_SERIAL_NUMBER, (int) serialId);
		sonarGrp.addAttribute(SonarGrp.ATT_SONAR_SOFTWARE_NAME, "SIS");
		sonarGrp.addAttribute(SonarGrp.ATT_SONAR_SOFTWARE_VERSION, data.getCPUVersion());
		sonarGrp.addAttribute(SonarGrp.ATT_SONAR_TYPE, XsfConstants.SONAR_TYPE_NAME);

	}

	private void fillTransducerData(IIPMetadata metadata) throws NCException {
		int i = 0;
		short serialId = metadata.getSerialId();

		ArrayList<Tray> trays = new ArrayList<>(metadata.getTrays());

		for (Tray tray : trays) {
			long[] origin = new long[] { i };
			i++;
			long[] count = new long[] { 1 };

			InternalLever il;
			if (tray.it != null) {
				il = tray.it;
			} else if (tray.ir != null) {
				il = tray.ir;
			} else if (tray.i != null) {
				il = tray.i;
			} else {
				il = metadata.new InternalLever();
			}

			// TODO index and serial number are missing ?
			platformGrp.getTransducer_offset_x().put(origin, count, new float[] { tray.x + il.x });
			platformGrp.getTransducer_offset_y().put(origin, count, new float[] { tray.y + il.y });
			platformGrp.getTransducer_offset_z().put(origin, count, new float[] { tray.z + il.z });
			platformGrp.getTransducer_rotation_z().put(origin, count, new float[] { Angles.trim180f(tray.h) });
			platformGrp.getTransducer_rotation_x().put(origin, count, new float[] { Angles.trim180f(tray.r) });
			platformGrp.getTransducer_rotation_y().put(origin, count, new float[] { Angles.trim180f(tray.p) });
			platformGrp.getTransducer_function().put(origin, count,
					new byte[] { (byte) (tray.type == AntennaType.RX ? transducer_type_t.Rx_Transducer.getValue()
							: transducer_type_t.Tx_Transducer.getValue()) });
			// compute serial id to be compatible of .all we add the system serial id to rx
			// antenna
			int serial = tray.type == AntennaType.RX ? tray.sn + serialId : tray.sn;
			platformGrp.getTransducer_ids().put(origin, count, new String[] { Integer.toString(serial) });
		}
	}

}
