package fr.ifremer.globe.api.xsf.converter.all;

import java.io.IOException;

import org.slf4j.Logger;

import fr.ifremer.globe.api.xsf.converter.MbgConverterParameters;
import fr.ifremer.globe.api.xsf.converter.XsfConverterParameters;
import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.all.mbg.AllMbgWriter;
import fr.ifremer.globe.api.xsf.converter.all.xsf.AllXsfWriter;
import fr.ifremer.globe.api.xsf.converter.common.IConverter;
import fr.ifremer.globe.api.xsf.converter.common.INcFileWriter;
import fr.ifremer.globe.api.xsf.converter.common.exceptions.UnsupportedSounderException;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgBase;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfFromKongsberg;
import fr.ifremer.globe.api.xsf.util.DefaultLogger;

public class KongsbergAllConverter implements IConverter<AllFile> {

	/** Logger **/
	private Logger logger = DefaultLogger.get();

	@Override
	public Logger getLogger() {
		return logger;
	}

	@Override
	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	@Override
	public boolean accept(String inputFilePath) {
		return inputFilePath.endsWith(AllFile.EXTENSION_ALL);
	}

	@Override
	public AllFile indexFile(String inputFilePath, boolean parseWaterColumData) throws IOException,UnsupportedSounderException {
		return new AllFile(inputFilePath, parseWaterColumData);
	}

	@Override
	public INcFileWriter<AllFile, XsfFromKongsberg, XsfConverterParameters> getXsfWriter() {
		return new AllXsfWriter();
	}

	@Override
	public INcFileWriter<AllFile, MbgBase, MbgConverterParameters> getMbgWriter() {
		return new AllMbgWriter();
	}

}
