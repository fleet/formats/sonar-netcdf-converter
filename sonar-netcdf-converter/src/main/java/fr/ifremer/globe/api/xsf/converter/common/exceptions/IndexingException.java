package fr.ifremer.globe.api.xsf.converter.common.exceptions;

import fr.ifremer.globe.utils.exception.GException;

@SuppressWarnings("serial")
public class IndexingException extends GException {

    public IndexingException(String message) {
        super(message);
    }

}
