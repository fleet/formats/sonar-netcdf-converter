package fr.ifremer.globe.api.xsf.converter.common.mbg;

import java.io.IOException;

import org.slf4j.Logger;

import fr.ifremer.globe.api.xsf.converter.MbgConverterParameters;
import fr.ifremer.globe.api.xsf.converter.common.INcFileWriter;
import fr.ifremer.globe.api.xsf.converter.common.sounder.ISounderFile;
import fr.ifremer.globe.netcdf.ucar.NCException;

public abstract class MbgWriter<T extends ISounderFile> implements INcFileWriter<T, MbgBase, MbgConverterParameters> {

	/**
	 * Creates the MBG file.
	 */
	@Override
	public MbgBase createOutputNcFile(T inputSounderFile, MbgConverterParameters params) throws NCException, IOException {
		MbgBase mbgBase = new MbgBase(inputSounderFile);
		mbgBase.initialize(params.getOutFilePath());
		mbgBase.fillVariableWithDefaultValues(inputSounderFile);
		return mbgBase;
	}

	@Override
	public void postProcess(T inputSounderFile, MbgBase mbgFile, MbgConverterParameters params, Logger logger) throws NCException {
		mbgFile.writeGlobalAttributes(inputSounderFile, params);
	}

}
