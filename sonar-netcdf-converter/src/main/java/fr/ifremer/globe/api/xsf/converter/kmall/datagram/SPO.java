package fr.ifremer.globe.api.xsf.converter.kmall.datagram;

import java.nio.ByteBuffer;

import fr.ifremer.globe.api.xsf.converter.all.datagram.IndividualSensorMetadata;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.utils.NmeaParser;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.KmallFile;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.SPOMetadata;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.DDouble;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.api.xsf.util.types.UByte;
import fr.ifremer.globe.api.xsf.util.types.UInt;
import fr.ifremer.globe.api.xsf.util.types.UShort;

public class SPO extends SCommon {

	@Override
	public void computeSpecificMetadata(KmallFile metadata, ByteBuffer datagram, String datagramType,
			DatagramPosition position) {
		SPOMetadata md = getSpecificMetadata(metadata);

		UShort sensorId = getSensorId(datagram);
		long epochTime = KmAllBaseDatagram.getEpochTimeNano(datagram);
		IndividualSensorMetadata sensorMetadata = md.getOrCreateSensor(sensorId);
		sensorMetadata.addDatagram(position, epochTime, 1);

		// compute statistics
		md.computeSOG(getSpeedOverGround_mPerSec(datagram).get());
	}

	@Override
	public SPOMetadata getSpecificMetadata(KmallFile metadata) {
		return metadata.getSPOMetadata();
	}

	public static UShort getSensorId(ByteBuffer datagram) {
		return new UShort((short) (getSensorSystem(datagram).getU() + 1));
	}

	public static UInt getTimeFromSensor_sec(ByteBuffer buffer) {
		return TypeDecoder.read4U(buffer, 24);
	}

	public static UInt getTimeFromSensor_nanosec(ByteBuffer buffer) {
		return TypeDecoder.read4U(buffer, 28);
	}

	public static FFloat getPosFixQuality_m(ByteBuffer buffer) {
		return TypeDecoder.read4F(buffer, 32);
	}

	public static DDouble getCorrectedLat_deg(ByteBuffer buffer) {
		return TypeDecoder.read8F(buffer, 36);
	}

	public static DDouble getCorrectedLong_deg(ByteBuffer buffer) {
		return TypeDecoder.read8F(buffer, 44);
	}

	public static FFloat getSpeedOverGround_mPerSec(ByteBuffer buffer) {
		return TypeDecoder.read4F(buffer, 52);
	}

	public static FFloat getCourseOverGround_deg(ByteBuffer buffer) {
		return TypeDecoder.read4F(buffer, 56);
	}

	public static FFloat getEllipsoidHeightReRefPoint_m(ByteBuffer buffer) {
		return TypeDecoder.read4F(buffer, 60);
	}

	public static String getDataFromSensor(ByteBuffer datagram) {
		final long fullDgSize = TypeDecoder.read4U(datagram, datagram.limit() - 4).getU();
		final int dataFromSensorOffset = 64;
		final int rawDataLen = (int) (fullDgSize - 8 - dataFromSensorOffset);

		final int pos = datagram.position();
		byte[] ret = new byte[rawDataLen];
		datagram.position(dataFromSensorOffset);
		datagram.get(ret, 0, rawDataLen);
		datagram.position(pos);

		return new String(ret);
	}

	public static UByte getSensorQualityIndicator(ByteBuffer datagram) {
		return new UByte(NmeaParser.getQualityIndicator(getDataFromSensor(datagram)));
	}

}
