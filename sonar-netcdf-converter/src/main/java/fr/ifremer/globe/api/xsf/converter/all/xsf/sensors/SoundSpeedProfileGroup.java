package fr.ifremer.globe.api.xsf.converter.all.xsf.sensors;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import com.sun.jna.Memory;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.all.datagram.IndividualSensorMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.all.datagram.soundspeedprofile.SoundSpeedProfile;
import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1S4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U8;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfFromKongsberg;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.SoundSpeedProfileGrp;
import fr.ifremer.globe.api.xsf.util.SimpleIdentifier;
import fr.ifremer.globe.api.xsf.util.types.SInt;
import fr.ifremer.globe.api.xsf.util.types.ULong;
import fr.ifremer.globe.netcdf.jna.Nc4prototypes.Vlen_t;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.date.DateUtils;

public class SoundSpeedProfileGroup implements IDatagramConverter<AllFile, XsfFromKongsberg> {

	private IndividualSensorMetadata sensorMetadata;
	int maxSampleCount;

	private AllFile metadata;
	protected List<ValueD1> profileValues = new LinkedList<>();
	private SoundSpeedProfileGrp sspGroup;

	public SoundSpeedProfileGroup(AllFile metadata) {
		this.metadata = metadata;
		this.sensorMetadata = metadata.getSoundSpeedProfile();
		this.maxSampleCount = sensorMetadata.getMaxEntriesPerDatagram();
	}

	@Override
	public void declare(AllFile allFile, XsfFromKongsberg xsf) throws NCException {
		sspGroup = xsf.getSoundSpeedProfileGrp();

		// declare all the variables

		profileValues.add(new ValueD1U8(sspGroup.getProfile_time(),
				buffer -> new ULong((SoundSpeedProfile.getXSFEpochTime(buffer.byteBufferData) ))));

		profileValues.add(new ValueD1U8(sspGroup.getMeasure_time(), 
				(buffer) -> new ULong(DateUtils.milliSecondToNano(SoundSpeedProfile.getProfilEpochTime(allFile, buffer.byteBufferData)))));


		profileValues.add(new ValueD1S4(sspGroup.getSample_count(), (buffer) -> {
			return new SInt(SoundSpeedProfile.getNumberEntries(buffer.byteBufferData).getU());
		}));

		
		
		// declare vlen variables
		sspGroup.getSound_speed();
		
		sspGroup.getSample_depth();
	}

	@Override
	public void fillData(AllFile allFile) throws IOException, NCException {
		long[] origin = new long[] { 0 ,0};

		DatagramBuffer buffer = new DatagramBuffer(metadata.getByteOrder());

		for (Entry<SimpleIdentifier, DatagramPosition> posentry : sensorMetadata.getDatagrams()) {
			final DatagramPosition pos = posentry.getValue();
			DatagramReader.readDatagramAt(pos.getFile(), buffer, pos.getSeek());

			// read the data from the source file
			for (ValueD1 value : this.profileValues) {
				value.fill(buffer);
			}
			
			int sample_count=SoundSpeedProfile.getNumberEntries(buffer.byteBufferData).getU();
			int bufferlength = sample_count * Float.BYTES;
			
			Memory depthMem = new Memory(bufferlength);
			ByteBuffer local_buffer_depth = depthMem.getByteBuffer(0, bufferlength);
			Memory speedMem = new Memory(bufferlength);
			ByteBuffer local_buffer_speed = speedMem.getByteBuffer(0, bufferlength);

			for (int i=0;i<sample_count;i++)
			{
				float value=SoundSpeedProfile.getDepth(buffer.byteBufferData, i).getU()*0.01f*SoundSpeedProfile.getDepthResolution(buffer.byteBufferData).getU();
				local_buffer_depth.putFloat(value);
			
				value=SoundSpeedProfile.getSoundSpeedProfile(buffer.byteBufferData, i).getU()*0.1f;
				local_buffer_speed.putFloat(value);

			}
			
			Vlen_t[] vlen_struct = (Vlen_t[]) new Vlen_t().toArray(1);
			vlen_struct[0].p = depthMem;
			vlen_struct[0].len = sample_count;
			sspGroup.getSample_depth().put(new long[] { origin[0] },
					new long[] { 1,}, vlen_struct);
			vlen_struct[0].p = speedMem;
			vlen_struct[0].len = sample_count;
			sspGroup.getSound_speed().put(new long[] { origin[0] },
					new long[] { 1,}, vlen_struct);



			long[] originD1 = new long[] { origin[0] };

			// flush into the output file
			for (ValueD1 value : this.profileValues) {
				value.write(originD1);
			}

			origin[0] += 1;
		}
	}

}
