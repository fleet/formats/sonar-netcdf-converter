package fr.ifremer.globe.api.xsf.converter.common.xsf;

import java.io.IOException;
import java.util.HashMap;
import java.util.Optional;

import fr.ifremer.globe.api.xsf.converter.XsfConverterParameters;
import fr.ifremer.globe.api.xsf.converter.common.sounder.IKongsbergFile;
import fr.ifremer.globe.api.xsf.converter.common.sounder.ISounderFile;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.BeamGroup1Grp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.BathymetryVendorSpecificGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.BeamGroup1VendorSpecificGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.ClockGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.HeightGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.RuntimeGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.SoundSpeedProfileVendorSpecificGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.SurfaceSoundSpeedVendorSpecificGrp;
import fr.ifremer.globe.netcdf.jna.Nc4prototypes;
import fr.ifremer.globe.netcdf.ucar.NCException;

/**
 * declaration for all specific group attributes
 */
public class XsfFromKongsberg extends XsfBase {
	private final BeamGroup1VendorSpecificGrp beamGroup;
	private final BathymetryVendorSpecificGrp bathyGroup;
	private final RuntimeGrp runtime;
	private final SurfaceSoundSpeedVendorSpecificGrp surface;
	private final SoundSpeedProfileVendorSpecificGrp ssp;
	private final HeightGrp height;
	private final ClockGrp clock;

	public XsfFromKongsberg(IKongsbergFile kongsbergFile, XsfConverterParameters params)
			throws NCException, IOException {
		super(kongsbergFile, params);

		beamGroup = new BeamGroup1VendorSpecificGrp(getBeamGroup(), kongsbergFile,new HashMap<String, Integer>());
		bathyGroup = new BathymetryVendorSpecificGrp(getBathyGrp(), kongsbergFile,new HashMap<String, Integer>());
		clock = new ClockGrp(getEnvironmentVendor(), kongsbergFile,new HashMap<String, Integer>());
		height = new HeightGrp(getEnvironmentVendor(), kongsbergFile,new HashMap<String, Integer>());
		ssp = new SoundSpeedProfileVendorSpecificGrp(getSoundSpeedProfileGrp(), kongsbergFile,new HashMap<String, Integer>());

		if (kongsbergFile.getRuntimeDgCount() > 0) {
			runtime = new RuntimeGrp(getPlatformVendorGrp(), kongsbergFile,new HashMap<String, Integer>());
		} else {
			runtime = null;
		}
		if (getSurfaceSoundSpeedGrp().isPresent()) {
			surface = new SurfaceSoundSpeedVendorSpecificGrp(getSurfaceSoundSpeedGrp().get(), kongsbergFile,new HashMap<String, Integer>());
		} else {
			surface = null;
		}
	}

	public ClockGrp getClock() {
		return clock;
	}

	public SoundSpeedProfileVendorSpecificGrp getSvp() {
		return ssp;
	}

	public Optional<SurfaceSoundSpeedVendorSpecificGrp> getSurface() {
		return Optional.of(surface);
	}

	public Optional<RuntimeGrp> getRuntime() {
		return Optional.of(runtime);
	}
	
	/**Create BeamGrp, override this function if needed to override type definition*/
	protected BeamGroup1Grp makeBeamGrp(ISounderFile sounderFile) throws NCException {
		HashMap<String,Integer> typeDict= new HashMap<>();
		typeDict.put("sample_t",Nc4prototypes.NC_BYTE);
		if (sounderFile.hasPhaseValue())
		{
			typeDict.put("angle_t",Nc4prototypes.NC_FLOAT); //we force the type to float, phase is almost never recorded so file size expansion is acceptable
		}
		return new BeamGroup1Grp(this.getSonarGrp(), sounderFile, DEFAULT_BEAM_GROUP_NAME,typeDict);
	}
	

	public BeamGroup1VendorSpecificGrp getBeamGroupVendorSpecific() {
		return beamGroup;
	}

	public HeightGrp getHeight() {
		return height;
	}

	public BathymetryVendorSpecificGrp getBathy() {
		return bathyGroup;
	}
}
