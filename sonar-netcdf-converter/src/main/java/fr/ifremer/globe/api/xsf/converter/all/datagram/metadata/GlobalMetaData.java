package fr.ifremer.globe.api.xsf.converter.all.datagram.metadata;

import java.util.Set;
import java.util.TreeSet;
import java.util.function.Predicate;

import fr.ifremer.globe.api.xsf.converter.common.utils.DateUtil;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.SwathIndexContainer;
import fr.ifremer.globe.api.xsf.util.SwathIndexGenerator;
import fr.ifremer.globe.api.xsf.util.types.UShort;

public class GlobalMetaData {
	public int datagramCount = 0;
	private long minDate = Long.MAX_VALUE;
	private long maxDate = Long.MIN_VALUE;

	private int modelNumber;
	private Set<Integer> serialNumbers = new TreeSet<>();

	// utility responsible for computing swath and swath indexes
	private SwathIndexGenerator generator;
	private SwathIndexContainer retainedIds;

	// public SwathIndexGenerator getGenerator() {
	// return generator;
	// }

	public GlobalMetaData() {
		this.generator = new SwathIndexGenerator();
		this.retainedIds = new SwathIndexContainer();
	}

	public void updateRetainedIndex(Predicate<SwathIdentifier> filter) {
		this.retainedIds.generateIndex(generator, filter);
	}

	public SwathIdentifier addSwath(int swathNumber, long swathTime) {
		return generator.addId(swathNumber, 1, 1, swathTime);
	}

	public void addTime(long epochTime) {
		minDate = Math.min(epochTime, minDate);
		maxDate = Math.max(epochTime, maxDate);
	}

	public void addSerial(UShort serialNumber) {
		this.serialNumbers.add(serialNumber.getU());
	}

	/**
	 * return the number of swath
	 */
	public int getSwathCount() {
		return retainedIds.getIndexSize();
	}

	@Override
	public String toString() {
		return "-->" + this.getClass().getSimpleName() + System.lineSeparator() + "datagram count " + datagramCount
				+ System.lineSeparator() + "model " + modelNumber + System.lineSeparator() + System.lineSeparator()
				+ "date times" + "[" + DateUtil.fromLong(minDate) + "," + DateUtil.fromLong(maxDate) + "]"
				+ System.lineSeparator() + "serials " + serialNumbers.toString();

	}

	public SwathIndexContainer getSwathIds() {
		return retainedIds;
	}

	public void setModelNumber(int modelNumber) {
		this.modelNumber = modelNumber;
	}

	/**
	 * @return the datagramCount
	 */
	public int getDatagramCount() {
		return datagramCount;
	}

	/**
	 * @return the minDate
	 */
	public long getMinDate() {
		return minDate;
	}

	/**
	 * @return the maxDate
	 */
	public long getMaxDate() {
		return maxDate;
	}

	/**
	 * @return the modelNumber
	 */
	public int getModelNumber() {
		return modelNumber;
	}

	/**
	 * @return the serialNumbers
	 */
	public Set<Integer> getSerialNumbers() {
		return serialNumbers;
	}

}
