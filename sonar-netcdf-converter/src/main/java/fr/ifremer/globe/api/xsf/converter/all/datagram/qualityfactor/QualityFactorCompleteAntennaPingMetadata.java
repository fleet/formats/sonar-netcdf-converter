package fr.ifremer.globe.api.xsf.converter.all.datagram.qualityfactor;

import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;

/**
 * all datagram metadata for a ping/antenna couple
 */
public class QualityFactorCompleteAntennaPingMetadata {
	private int datagramCount;

	DatagramPosition position; // offset in file for each datagram

	/**
	 * map offset between each NrxEntries starting points and beam number in
	 * datagram
	 */
	private int beamCount;
	private int paramCount;

	QualityFactorCompleteAntennaPingMetadata(DatagramPosition position, int totalBeamCount, int paramCount) {
		this.position = position;
		this.beamCount = totalBeamCount;
		this.paramCount = paramCount;
	}

	public int getBeamCount() {
		return beamCount;
	}

	/**
	 * @return the datagramCount
	 */
	public int getDatagramCount() {
		return datagramCount;
	}
	
	public int getParamCount() {
	    return paramCount;
	}
}
