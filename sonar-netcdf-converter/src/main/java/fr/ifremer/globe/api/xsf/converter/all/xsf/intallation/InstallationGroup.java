package fr.ifremer.globe.api.xsf.converter.all.xsf.intallation;

import java.io.IOException;
import java.util.List;

import fr.ifremer.globe.api.xsf.converter.all.datagram.installation.InstallationMetadata.Antenna;
import fr.ifremer.globe.api.xsf.converter.all.datagram.installation.InstallationMetadata.AntennaType;
import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.utils.Angles;
import fr.ifremer.globe.api.xsf.converter.common.utils.Namer;
import fr.ifremer.globe.api.xsf.converter.common.utils.XLog;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfConstants;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfFromKongsberg;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.BeamGroup1Grp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.PlatformGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.SonarGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.types.transducer_type_t;
import fr.ifremer.globe.netcdf.ucar.NCException;

public class InstallationGroup implements IDatagramConverter<AllFile, XsfFromKongsberg> {

	private AllFile metadata;

	public InstallationGroup(AllFile metadata) {
		this.metadata = metadata;
	}

	@Override
	public void fillData(AllFile allFile) throws IOException, NCException {
		// Nothing here. The group is simple enough to be constructed at declaration time.
	}

	@Override
	public void declare(AllFile allFile, XsfFromKongsberg xsf) throws NCException {
		PlatformGrp platformGrp = xsf.getPlatformGrp();

		xsf.getPlatformVendorGrp().addAttribute("rawInstallation", metadata.getInstallation().getRawConf());
		xsf.getPlatformVendorGrp().addAttribute("kongsbergModelNumber", metadata.getGlobalMetadata().getModelNumber());
		xsf.getSonarGrp().addAttribute(SonarGrp.ATT_SONAR_MANUFACTURER, "Kongsberg");
		xsf.getSonarGrp().addAttribute(SonarGrp.ATT_SONAR_MODEL, metadata.getInstallation().getSounderDesc().name);
		xsf.getSonarGrp().addAttribute(SonarGrp.ATT_SONAR_SERIAL_NUMBER,
				metadata.getInstallation().getSerialNumber().getU());
		xsf.getSonarGrp().addAttribute(SonarGrp.ATT_SONAR_SOFTWARE_NAME, "SIS");
		xsf.getSonarGrp().addAttribute(SonarGrp.ATT_SONAR_SOFTWARE_VERSION, metadata.getInstallation().getSISVersion());
		xsf.getSonarGrp().addAttribute(SonarGrp.ATT_SONAR_TYPE, XsfConstants.SONAR_TYPE_NAME);

		List<Antenna> antennas = metadata.getInstallation().getAntennas();

		// fill them
		for (int i = 0; i < antennas.size(); i++) {
			Antenna antenna = antennas.get(i);
			long[] offset = new long[] { i };
			long[] count = new long[] { 1 };

			// Ajout des offset Rx/TX (en particulier pour le 2040).
			platformGrp.getTransducer_offset_x().put(offset, count, new float[] { (float) antenna.getX() });
			platformGrp.getTransducer_offset_y().put(offset, count, new float[] { (float) antenna.getY() });
			platformGrp.getTransducer_offset_z().put(offset, count, new float[] { (float) antenna.getZ() });
			platformGrp.getTransducer_rotation_z().put(offset, count,
					new float[] { Angles.trim180f((float) antenna.getH()) });
			platformGrp.getTransducer_rotation_x().put(offset, count,
					new float[] { Angles.trim180f((float) antenna.getR()) });
			platformGrp.getTransducer_rotation_y().put(offset, count,
					new float[] { Angles.trim180f((float) antenna.getP()) });
			platformGrp.getTransducer_function()
					.put(offset, count,
							new byte[] { (byte) (antenna.getType() == AntennaType.RX
									? transducer_type_t.Rx_Transducer.getValue()
									: transducer_type_t.Tx_Transducer.getValue()) });
			platformGrp.getTransducer_ids().put(offset, count,
					new String[] { Integer.toString(antenna.getId().getU()) });
		}
		// position captor information
		{
			int maxId = metadata.getPosition().getSensors().size();
			// update ids
			long[] offset = new long[] { 0 };
			long[] count = new long[] { maxId };
			String[] ids = new String[maxId];
			float[] x = new float[maxId];
			float[] y = new float[maxId];
			float[] z = new float[maxId];

			for (int i = 0; i < maxId; i++) {
				ids[i] = Namer.getPositionSubGroupName(i + 1); // les sensor KM sont numérotés de 1 à 3, on
																// conserve leurs identifiants
				x[i] = parseFloat("P" + (i + 1) + "X", Float.NaN, metadata);
				y[i] = parseFloat("P" + (i + 1) + "Y", Float.NaN, metadata);
				z[i] = parseFloat("P" + (i + 1) + "Z", Float.NaN, metadata);
			}
			platformGrp.getPosition_ids().put(offset, count, ids);
			int activeIndex = metadata.getPosition().getActiveId() - 1;
			// in .all active id is index +1
			xsf.getBeamGroup().addAttribute(BeamGroup1Grp.ATT_PREFERRED_POSITION, activeIndex); 
			platformGrp.getPosition_offset_x().put(offset, count, x);
			platformGrp.getPosition_offset_y().put(offset, count, y);
			platformGrp.getPosition_offset_z().put(offset, count, z);
		}
		// MRU captor information
		{
			int maxId = metadata.getAttitude().getSensors().size();
			/*
			 * String active_MRU=metadata.getInstallation().get("VSN"); int active_id=0; if(active_MRU!=null) { try {
			 * active_id = Integer.parseInt(active_MRU); } catch(NumberFormatException e) {
			 * XLog.warn("Error while parsing VSN attribute in Installation Parameter datagram : "+ "VSN:"+active_MRU);
			 * } }
			 */
			// update ids
			long[] offset = new long[] { 0 };
			long[] count = new long[] { maxId };
			String[] ids = new String[maxId];
			float[] x = new float[maxId];
			float[] y = new float[maxId];
			float[] z = new float[maxId];
			float[] roll = new float[maxId];
			float[] pitch = new float[maxId];
			float[] heading = new float[maxId];
			String InstallationIds[] = { "M", "N" }; // table used to retrieve encoding installation parameters for
														// attitude sensors
			for (int i = 0; i < maxId; i++) {
				ids[i] = Namer.getAttitudeSubGroupName(i + 1); // les sensor KM sont numérotés de 1 à 2, on conserve
																// leurs identifiants
				x[i] = parseFloat(InstallationIds[i] + "SX", Float.NaN, metadata);
				y[i] = parseFloat(InstallationIds[i] + "SY", Float.NaN, metadata);
				z[i] = parseFloat(InstallationIds[i] + "SZ", Float.NaN, metadata);
				roll[i] = parseFloat(InstallationIds[i] + "SR", Float.NaN, metadata);
				pitch[i] = parseFloat(InstallationIds[i] + "SP", Float.NaN, metadata);
				heading[i] = parseFloat(InstallationIds[i] + "SG", Float.NaN, metadata);
			}
			platformGrp.getMRU_ids().put(offset, count, ids);
			int activeIndex = metadata.getAttitude().getActiveId() - 1;
			// in .all active id is index +1
			xsf.getBeamGroup().addAttribute(BeamGroup1Grp.ATT_PREFERRED_MRU, activeIndex); // ici on indique l'index du
																							// MRU principal
			platformGrp.getMRU_offset_x().put(offset, count, x);
			platformGrp.getMRU_offset_y().put(offset, count, y);
			platformGrp.getMRU_offset_z().put(offset, count, z);
			platformGrp.getMRU_rotation_x().put(offset, count, roll);
			platformGrp.getMRU_rotation_y().put(offset, count, pitch);
			platformGrp.getMRU_rotation_z().put(offset, count, heading);
		}
		{
			platformGrp.getWater_level().putScalar(parseFloat("WLZ", 0f, metadata));
		}
	}

	/**
	 * Try to parse a float parameter from metadata, if failed return the given default value
	 * 
	 * @param parameterName the parameter to parse
	 * @param defaultValue default returned value if failed
	 * @param metadata the {@link AllFile}
	 */
	private static float parseFloat(String parameterName, float defaultValue, AllFile metadata) {
		String vx = metadata.getInstallation().get(parameterName);
		if (vx != null) {
			try {
				return Float.parseFloat(vx);
			} catch (Exception e) {
				XLog.warn("error while parsing installation message for parameter " + parameterName);
			}
		}
		return defaultValue;
	}
}
