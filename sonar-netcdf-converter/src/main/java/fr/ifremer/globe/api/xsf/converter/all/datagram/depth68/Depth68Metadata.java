package fr.ifremer.globe.api.xsf.converter.all.datagram.depth68;

import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.AbstractBeamDatagramMetadata;

public class Depth68Metadata extends AbstractBeamDatagramMetadata<Depth68CompletePingMetadata>{

    private float depthSum;
    private long numberOfDepth;
    public double depthMin, depthMax;
    
    private long minDate = Long.MAX_VALUE;
    private long maxDate = Long.MIN_VALUE;
    
    
    public Depth68Metadata() {
        depthSum        = 0;
        numberOfDepth   = 0;
        depthMin        = Float.MAX_VALUE;
        depthMax        = -Float.MAX_VALUE;
    }
    
    public void addDepth(double depth, long epoch) {
        this.depthSum += depth;
		this.numberOfDepth++;
		depthMin = Double.min(depth, depthMin);
		depthMax = Double.max(depth, depthMax);
        
        minDate = Math.min(minDate, epoch);
        maxDate = Math.max(maxDate, epoch);
    }
    
    public double getDepthMin()
    {
        return depthMin;
    }
    public double getDepthMax()
    {
        return depthMax;
    }
    
    public float getDepthMean() {
        return this.depthSum / this.numberOfDepth;
    }
    
    public long getMinDate() {
    	return minDate;
    }
    
    public long getMaxDate() {
    	return maxDate;
    }
}
