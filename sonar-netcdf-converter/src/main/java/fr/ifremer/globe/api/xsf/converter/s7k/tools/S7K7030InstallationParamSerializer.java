package fr.ifremer.globe.api.xsf.converter.s7k.tools;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.mapper.CannotResolveClassException;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7K7030InstallationParameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.stream.Stream;

public class S7K7030InstallationParamSerializer {

    /**
     * {@link XStream} writer
     **/
    private static final XStream xstreamWriter = new XStream();

    /**
     * {@link XStream} reader
     **/
    private static final XStream xstreamReader = new XStream();

    static {
        // configure XStream reader to handle old XML files (from previous converter)
        xstreamReader.allowTypes(new Class[]{S7K7030InstallationParameters.class});
        xstreamReader.autodetectAnnotations(true);
        xstreamReader.ignoreUnknownElements();
        xstreamReader.processAnnotations(S7K7030InstallationParameters.class);
        xstreamReader.setClassLoader(S7K7030InstallationParameters.class.getClassLoader());
        xstreamReader.alias("fr.ifremer.globe.s7kfile.parser.S7K7030InstallationDatagramParser",
                S7K7030InstallationParameters.class);
        xstreamReader.aliasField("recordProtocalInfo", S7K7030InstallationParameters.class, "protocolVersionInfo");
        xstreamReader.aliasField("software7kVersionInfo", S7K7030InstallationParameters.class, "s7KSoftwareVersion");
        xstreamReader.aliasField("softwareVersionInfo", S7K7030InstallationParameters.class, "softwareVersion");
        xstreamReader.aliasField("firmwareVersionInfo", S7K7030InstallationParameters.class, "firmwareVersion");
        xstreamReader.aliasField("ReceiveArrayX", S7K7030InstallationParameters.class, "receiveArrayX");
        xstreamReader.aliasField("ReceiveArrayY", S7K7030InstallationParameters.class, "receiveArrayY");
        xstreamReader.aliasField("ReceiveArrayZ", S7K7030InstallationParameters.class, "receiveArrayZ");
        xstreamReader.aliasField("ReceiveArrayRoll", S7K7030InstallationParameters.class, "receiveArrayRoll");
        xstreamReader.aliasField("ReceiveArrayPitch", S7K7030InstallationParameters.class, "receiveArrayPitch");
        xstreamReader.aliasField("ReceiveArrayHeading", S7K7030InstallationParameters.class, "receiveArrayHeading");
        xstreamReader.aliasField("MotionSensorX", S7K7030InstallationParameters.class, "motionSensorX");
        xstreamReader.aliasField("MotionSensorY", S7K7030InstallationParameters.class, "motionSensorY");
        xstreamReader.aliasField("MotionSensorZ", S7K7030InstallationParameters.class, "motionSensorZ");
        xstreamReader.aliasField("MotionSensorRollCalibration", S7K7030InstallationParameters.class,
                "motionSensorRollCalibration");
        xstreamReader.aliasField("MotionSensorPitchCalibration", S7K7030InstallationParameters.class,
                "motionSensorPitchCalibration");
        xstreamReader.aliasField("MotionSensorHeadingCalibration", S7K7030InstallationParameters.class,
                "motionSensorHeadingCalibration");
        xstreamReader.aliasField("MotionSensorTimeDelay", S7K7030InstallationParameters.class, "motionSensorTimeDelay");
        xstreamReader.aliasField("PositionSensorX", S7K7030InstallationParameters.class, "positionSensorX");
        xstreamReader.aliasField("PositionSensorY", S7K7030InstallationParameters.class, "positionSensorY");
        xstreamReader.aliasField("PositionSensorZ", S7K7030InstallationParameters.class, "positionSensorZ");
        xstreamReader.aliasField("PositionSensorTimeDelay", S7K7030InstallationParameters.class,
                "positionSensorTimeDelay");
        xstreamReader.aliasField("WaterLineVerticalOffset", S7K7030InstallationParameters.class,
                "waterLineVerticalOffset");
    }

    /**
     * logger
     */
    protected static Logger logger = LoggerFactory.getLogger(S7K7030InstallationParamSerializer.class);

    /**
     * Constructor
     **/
    private S7K7030InstallationParamSerializer() {
        // private constructor : utility class
    }

    /**
     * Computes setting file name (does not include frequcency if =< 0)
     */
    private static String computeXMLFileName(int deviceId, Optional<Float> optFrequency) {
        String frequencyLabel = optFrequency.isPresent()
                ? "_" + (int) (optFrequency.get() / 1000) + "kHz"
                : "";
        return "s7kDefaultSettings_" + deviceId + frequencyLabel;
    }

    /**
     * Serializes the given {@link S7K7030InstallationParameters} with default naming value in the destination
     * directory.
     *
     * @param outputdir the destination directory
     */
    public static void serialize(String outputdir, S7K7030InstallationParameters installParameters) {
        String xmlFileName = computeXMLFileName(installParameters.getDeviceId(),
                Optional.of(installParameters.getFrequency())) + ".xml";
        File xmlFile = Paths.get(outputdir, xmlFileName).toFile();
        if (!xmlFile.exists()) {
            try (Writer writer = new OutputStreamWriter(new FileOutputStream(xmlFile), StandardCharsets.UTF_8)) {
                xstreamWriter.toXML(installParameters, writer);
            } catch (IOException e) {
                logger.error("Error while serializing S7kInstallationDgr :" + e.getMessage());
            }
        }
    }

    /**
     * Tries to get a {@link S7K7030InstallationParameters} from XML files of the given directory.
     */
    public static Optional<S7K7030InstallationParameters> deserialize(int deviceId, Optional<Float> frequency,
                                                                      String directoryPath) {
        Optional<S7K7030InstallationParameters> deserializedParameters = Optional.empty();

        // compute expected XML file name
        String xmlFileName = computeXMLFileName(deviceId, frequency);

        // search file matching with the computed name
        Optional<File> xmlFile = Stream.of(new File(directoryPath).listFiles())
                .filter(file -> file.getName().contains(xmlFileName)).findFirst();

        if (xmlFile.isPresent()) {
            try (InputStream fis = new FileInputStream(xmlFile.get())) {
                // parse file
                deserializedParameters = Optional.of((S7K7030InstallationParameters) xstreamReader.fromXML(fis));
                deserializedParameters.get().setOrigin(S7K7030InstallationParameters.Origin.FROM_XML_FILE);
                deserializedParameters.get().setOriginFile(xmlFile.get().getAbsolutePath());
                deserializedParameters.get().setDeviceId(deviceId);
            } catch (CannotResolveClassException | IOException e) {
                logger.debug("Failed to extract installation parameters from : {}", xmlFile.get().getName());
            }
        }
        return deserializedParameters;
    }

    /**
     * Tries to get a {@link S7K7030InstallationParameters} from XML files of the given directory.
     */
    public static Optional<S7K7030InstallationParameters> deserializeDefault(int deviceId, Optional<Float> frequency) {
        Optional<S7K7030InstallationParameters> deserializedParameters = Optional.empty();

        // compute expected XML file name
        String xmlFileName = "/resources/" + computeXMLFileName(deviceId, frequency) + ".xml";

        // search file matching with the computed name
        try (InputStream fis = S7K7030InstallationParamSerializer.class.getResourceAsStream(xmlFileName)) {
            // parse file
            deserializedParameters = Optional.of((S7K7030InstallationParameters) xstreamReader.fromXML(fis));
            deserializedParameters.get().setOrigin(S7K7030InstallationParameters.Origin.FROM_XML_FILE);
            deserializedParameters.get().setOriginFile(xmlFileName);
            deserializedParameters.get().setDeviceId(deviceId);
        } catch (CannotResolveClassException | IOException e) {
            logger.debug("Failed to extract installation parameters from : {}", xmlFileName);
        }
        return deserializedParameters;
    }
}
