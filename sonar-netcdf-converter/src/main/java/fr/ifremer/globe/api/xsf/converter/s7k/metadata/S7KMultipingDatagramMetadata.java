package fr.ifremer.globe.api.xsf.converter.s7k.metadata;

import java.util.Map.Entry;

import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.DatagramMetadata;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;

public abstract class S7KMultipingDatagramMetadata extends DatagramMetadata<Long, S7KMultipingMetadata> {

	/**
	 * Check if the given id is datagram map
	 */
	public boolean contains(SwathIdentifier swathId) {
		long ping_id = swathId.getRawPingNumber();
		int multipingsequence = swathId.getSwathPosition();
		// we parse all device id and check if at least one contains the ping
		// Note that, parser seems coded as we could have several deviceid, if this happens, data will probably be
		for (long deviceId : index.keySet()) {
			S7KMultipingMetadata m = index.get(deviceId);
			if (m == null)
				continue;

			S7KMultipingSequenceMetadata met = m.getEntry(ping_id);
			if (met == null)
				continue;
			if (met.contains(multipingsequence))
				return true;
		}
		return false;
	}

	public S7KMultipingMetadata getOrCreateMetadataForDevice(long deviceId) {
		S7KMultipingMetadata md = index.get(deviceId);
		if (md == null) {
			md = new S7KMultipingMetadata();
			index.put(deviceId, md);
		}
		return md;
	}

	public String toString() {
		StringBuilder bld = new StringBuilder();
		bld.append(this.getClass().getSimpleName());

		for (Entry<Long, S7KMultipingMetadata> e : getEntries()) {
			bld.append(System.lineSeparator() + "\t[" + Long.toString(e.getKey()) + "] " + e.getValue().datagramCount
					+ " datagrams");
		}
		return bld.toString();
	}
}
