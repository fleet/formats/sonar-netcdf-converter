package fr.ifremer.globe.api.xsf.converter.all.datagram.extradetections;

import java.util.Set;

import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.DatagramMetadata;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;

/**
 * */
public class ExtraDetectionsMetadata extends DatagramMetadata<SwathIdentifier,ExtraDetectionsCompletePingMetadata> {
	
    public ExtraDetectionsMetadata() {
        super();
    }
    
    public ExtraDetectionsCompletePingMetadata getPing(SwathIdentifier pingNumber) {
        return index.get(pingNumber);
    }
    
    public void addPing(SwathIdentifier swathId, ExtraDetectionsCompletePingMetadata ping) {
        index.put(swathId, ping);
    }
    
	public int getTotalSampleCount() {
        return totalSampleCount;
    }


    public int getMaxExtraDections() {
        return maxExtraDections;
    }


    public int getDetectionClasses() {
        return detectionClasses;
    }

    /** total count of raw samples */
	private int totalSampleCount = 0;
	/** max ExtraDetections per datagram */
	private int maxExtraDections = 0;
	/** Number of detection classes */
	private int detectionClasses = 0;

	
	/**
	 * called after a full read is finished.
	 */
	public void computeIndex(Set<Integer> allAntennas) {
		totalSampleCount = 0;
		maxExtraDections = 0;
		detectionClasses = 0;
		for (ExtraDetectionsCompletePingMetadata ping : index.values()) {
		    if (ping.isComplete(allAntennas)) {
		        ping.computeIndex();
		        totalSampleCount += ping.getSampleCount();
		        detectionClasses = Integer.max(this.detectionClasses, ping.getDetectionClassesCount());
		        maxExtraDections = Integer.max(this.maxExtraDections, ping.getExtradetectionsCount());
		    }
		}
	}
}
