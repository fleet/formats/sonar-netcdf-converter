package fr.ifremer.globe.api.xsf.converter.all.datagram.unknown;

import java.nio.ByteBuffer;

import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagram;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;

public class Unknown extends BaseDatagram {

	@Override
	public UnknownMetadata getSpecificMetadata(AllFile metadata) {
		return metadata.getUnknown();
	}

	@Override
	protected void computeSpecificMetadata(AllFile metadata, ByteBuffer datagram, byte datagramType,
			long epochTime, DatagramPosition position) {
		getSpecificMetadata(metadata).pushUnknownType(datagramType);
	}

}
