package fr.ifremer.globe.api.xsf.converter.s7k.datagram;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.LinkedList;
import java.util.List;

import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.exceptions.UnexpectedEndOfFile;
import fr.ifremer.globe.api.xsf.converter.s7k.exceptions.FragmentedDatagramException;
import fr.ifremer.globe.api.xsf.converter.s7k.exceptions.InvalidChecksumException;
import fr.ifremer.globe.api.xsf.util.ObjectBufferPool;
import fr.ifremer.globe.netcdf.util.Pair;

public class DatagramReader {

	public static int readDatagram(RandomAccessFile raf, DatagramBuffer buffer) throws IOException {
		int byteRead = 0;
		
		byteRead = raf.read(buffer.rawBufferHeader);
		if (byteRead < 0) {
			return byteRead;
		}
		buffer.byteBufferSizeHeader.rewind();
		
		final long recordSize = Integer.toUnsignedLong(buffer.byteBufferSizeHeader.getInt(8));
		final int dataSize = (int) recordSize - buffer.rawBufferHeader.length;
		buffer.allocate(dataSize);
		int read = raf.read(buffer.rawBufferData, 0, dataSize);
		if (read != dataSize) {
			throw new UnexpectedEndOfFile();
		}
		byteRead += read;
		buffer.byteBufferData.limit(dataSize);
		buffer.byteBufferData.rewind();
		
		if (S7KBaseDatagram.getRecordsInFragmentedSet(buffer).getU() != 0) {
			throw new FragmentedDatagramException(String.format("Fragmented datagram at offset %d", raf.getFilePointer() - byteRead));
		}
		
		// perform validation
		if (!S7KBaseDatagram.validChecksum(buffer)) {
			throw new InvalidChecksumException(String.format("Invalid checksum for datagram at offset %d", raf.getFilePointer() - byteRead));
		}
		
		return byteRead;
	}
	
	public static int readDatagram(RandomAccessFile raf, DatagramBuffer buffer, long offset) throws IOException {
		raf.seek(offset);
		return readDatagram(raf, buffer);
	}
	
	public static <T> List<Pair<DatagramBuffer, T>> read(ObjectBufferPool<DatagramBuffer> pool, List<Pair<DatagramPosition, T>> locations) throws IOException {
        List<Pair<DatagramBuffer, T>> ret = new LinkedList<Pair<DatagramBuffer, T>>();
        for (Pair<DatagramPosition, T> loc : locations) {
            DatagramBuffer buffer = pool.borrow();
            readDatagram(loc.getFirst().getFile(), buffer, loc.getFirst().getSeek());
            ret.add(new Pair<DatagramBuffer, T>(buffer, loc.getSecond()));
        }
        return ret;
	}
}
