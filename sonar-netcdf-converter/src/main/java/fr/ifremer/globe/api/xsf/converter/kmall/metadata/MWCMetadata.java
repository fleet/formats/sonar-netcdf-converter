package fr.ifremer.globe.api.xsf.converter.kmall.metadata;

import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.AbstractBeamDatagramMetadata;

public class MWCMetadata extends AbstractBeamDatagramMetadata<MWCCompletePingMetadata> {
	boolean hasPhaseValues=false;
	
    public boolean hasPhaseValues() {
		return hasPhaseValues;
	}

	public void setHasPhaseValues(boolean hasPhaseValues) {
		this.hasPhaseValues = hasPhaseValues;
	}
}
