package fr.ifremer.globe.api.xsf.converter.common.utils;

public class NmeaParser {
	/**
	 * Retrives Quality indicator from a GGA NMEA GGA - Global Positioning System Fix Data Time, Position and fix related
	 * data for a GPS receiver.
	 * 
	 * @descr
	 * 1 2 3 4 5 6 7 8 9 10 | 12 13 14 15 | | | | | | | | | | | | | | |
	 * $--GGA,hhmmss.ss,llll.ll,a,yyyyy.yy,a,x,xx,x.x,x.x,M,x.x,M,x.x,xxxx*hh<CR><LF>
	 * 
	 * Field Number: 1) Universal Time Coordinated (UTC) // 
	 * 				 2) Latitude 
	 * 				 3) N or S (North or South) 
	 * 				 4) Longitude 
	 * 				 5) E or W (East or West) 
	 * 				 6) GPS Quality Indicator, 
	 * 					0 - fix not available, 
	 * 					1 - GPS fix, 
	 * 					2 - Differential GPS fix (values above 2 are 2.3 features) 
	 * 					3 = PPS fix 
	 * 					4 = Real Time Kinematic 
	 * 					5 = Float RTK 
	 * 					6 = estimated (dead reckoning) 
	 * 					7 = Manual input mode 
	 * 					8 = Simulation mode 
	 * 				 7) Number of satellites in view, 00 - 12 
	 * 				 8) Horizontal Dilution of precision (meters) 
	 * 				 9) Antenna Altitude above/below mean-sea-level (geoid) (in meters) 
	 * 				10) Units of antenna altitude, meters 
	 * 				11) Geoidal separation, the difference between the WGS-84 earth ellipsoid and mean-sea-level
	 * (geoid), "-" means mean-sea-level below ellipsoid 
	 * 				12) Units of geoidal separation, meters 
	 * 				13) Age of differential
	 * GPS data, time in seconds since last SC104 type 1 or 9 update, null field when DGPS is not used 
	 * 				14) Differential
	 * reference station ID, 0000-1023 
	 * 				15) Checksum
	 **/
	
	public static byte getQualityIndicator(String nmea) {
		String[] dataElements = nmea.split(",");
		if (dataElements.length > 6 && dataElements[0].endsWith("GGA")) {
			// input datagram contains NMEA GGA data.
			// Parse quality indicator field
			return Byte.parseByte(dataElements[6]);
		}
		return 0;
	}
}
