package fr.ifremer.globe.api.xsf.converter.kmall.metadata;

import fr.ifremer.globe.api.xsf.converter.all.datagram.installation.InstallationMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.installation.InstallationMetadata.AntennaType;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.DatagramMetadata;
import fr.ifremer.globe.api.xsf.converter.common.utils.XLog;
import fr.ifremer.globe.api.xsf.util.DefaultLogger;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IIPMetadata extends DatagramMetadata<Integer, DatagramPosition> {

    public class InternalLever implements Cloneable {
        public float x;
        public float y;
        public float z;

        public InternalLever() {
            this.x = 0;
            this.y = 0;
            this.z = 0;
        }

        public InternalLever(InternalLever source) {
            super();
            this.x = source.x;
            this.y = source.y;
            this.z = source.z;
        }
    }

    public class Positions {
        public int index;
        public float x = Float.NaN;// forwards,inmeter.
        public float y = Float.NaN;// athwart starboard,inmeter.
        public float z = Float.NaN;// vertical down,inmeter.
        public float d = Float.NaN;// timedelay ,in sec.
        public String g = "";// datum
        public double t = Double.NaN;// time stamp fromPU/POS
        public boolean c = false;// compensation for motion On/Off.
        public String f = "";// dataformat ,with quality settings from operator(optional).
        public String q = "";// QualitycheckOn/Off.(Will be off when operator has set quality settings).
        public String i = "";// inputsource
        public String u = "";// ACTIVE/PASSIVE/NOT_SET

        public boolean isMotionCorrected() {
            return c || (x == 0.f && y == 0.f && z == 0.f);
        }
    }

    public class MRU {
        public int index;
        public float x = Float.NaN; // forwards, inmeter.
        public float y = Float.NaN; // athwart starboard,inmeter.
        public float z = Float.NaN;// vertical down,inmeter.
        public float r = Float.NaN;// roll offset,indegrees.
        public float p = Float.NaN;// pitch offset,indegrees.
        public float h = Float.NaN;// heading offset,indegrees.
        public double d = Double.NaN;// timedelay,in sec.
        public String m = ""; // motion ref.plan RP/HO
        public String f = "";// dataformat
        public String i = "";// inputsource
        public String u = "NOT_SET";// use ACTIVE/PASSIVE/NOT_SET
    }

    public class Tray {
        public int index;
        public AntennaType type;

        public Tray(Tray source) {
            super();
            this.index = source.index;
            this.type = source.type;
            this.sn = source.sn;
            this.x = source.x;
            this.y = source.y;
            this.z = source.z;
            this.r = source.r;
            this.p = source.p;
            this.h = source.h;
            this.g = source.g;
            this.s = source.s;
            if (source.ip != null)
                this.ip = new InternalLever(source.ip);
            if (source.ic != null)
                this.ic = new InternalLever(source.ic);
            if (source.is != null)
                this.is = new InternalLever(source.is);
            if (source.i != null)
                this.i = new InternalLever(source.i);
            if (source.it != null)
                this.it = new InternalLever(source.it);
            if (source.it != null)
                this.ir = new InternalLever(source.ir);
        }

        public int sn;
        public float x;
        public float y;
        public float z;
        public float r;
        public float p;
        public float h;
        public float g;
        public float s;

        // port side of sonar head
        public InternalLever ip;

        // center of sonar head
        public InternalLever ic;

        // starboard side of sonar head
        public InternalLever is;

        // array offset
        public InternalLever i;

        // tx array offset
        public InternalLever it;

        // rx array offset
        public InternalLever ir;

        public Tray() {
            this.index = 0;
            this.sn = 0;
            this.x = 0;
            this.y = 0;
            this.z = 0;
            this.r = 0;
            this.p = 0;
            this.h = 0;
            this.g = 0;
            this.s = 0;
        }
    }

    private String installTxt;
    private String model_type;
    private String cpuVersion;
    private int s_no_tx;
    private int s_no_rx;
    private List<Tray> trays;
    private short serialId;
    private List<MRU> mrus;
    private List<Positions> positions;
    float waterlineVerticalLocation = Float.NaN;

    public void setInstallationTxt(String installTxt, short serialId) {
        this.installTxt = installTxt;
        parseInstallationTxt();
        this.serialId = serialId;
    }

    public String getInstallTxt() {
        return this.installTxt;
    }

    /**
     * Parses the installationTXT field to extract relevant information
     */
    protected void parseInstallationTxt() {
        try {
            Pattern emxvPatt = Pattern.compile("EMXV:\\s*(?<emxv>\\w+)");
            Matcher emxvM = emxvPatt.matcher(this.installTxt);
            emxvM.find();
            this.model_type = emxvM.group("emxv");
        } catch (Exception e) {
            XLog.warn("Error while parsing model type in IIP datagram", e);
        }

        try {

            Pattern snPat_v = Pattern.compile("(?sm)VERSIONS:(.*)VERSIONS-END");
            Matcher snMatch_v = snPat_v.matcher(installTxt);
            snMatch_v.find();

            Matcher vers = Pattern.compile("CPU:(?<sn>.*) (?<date>.*),\\n").matcher(snMatch_v.group());
            vers.find();
            this.cpuVersion = vers.group("sn");

        } catch (Exception e) {
            XLog.warn("Error while parsing software versions in IIP datagram", e);
        }

        try {
            Pattern snPat = Pattern.compile("(?sm)SERIALno:(.*)SERIALno-END");
            Matcher snMatch = snPat.matcher(installTxt);
            snMatch.find();
            Matcher txsnM = Pattern.compile("TX:\\s*(?<sn>\\d+)").matcher(snMatch.group());
            txsnM.find();
            this.s_no_tx = Integer.parseInt(txsnM.group("sn"));

            Matcher rxsnM = Pattern.compile("RX:\\s*(?<sn>\\d+)").matcher(snMatch.group());
            rxsnM.find();
            this.s_no_rx = Integer.parseInt(rxsnM.group("sn"));
        } catch (Exception e) {
            XLog.warn("Error while parsing serial ids versions in IIP datagram", e);
        }

        this.serialId = 0;

        try {
            Pattern emxiPatt = Pattern.compile("EMXI:SWLZ=\\s*(?<swlz>[^,]+)");
            Matcher emxiM = emxiPatt.matcher(this.installTxt);
            boolean find = emxiM.find();
            if (find) {
                float waterline = Float.parseFloat(emxiM.group("swlz"));
                waterlineVerticalLocation = waterline;
            }
        } catch (Exception e) {
            waterlineVerticalLocation = Float.NaN;
        }

        trays = parseTray();
        mrus = parseMRU();
        positions = parsePositions();

    }

    private List<Positions> parsePositions() {
        LinkedList<Positions> positions = new LinkedList<>();

        Matcher trayMatcher = Pattern.compile("POSI_(?<n>[0-9]):(?<descr>[^,]+),", Pattern.MULTILINE)
                .matcher(this.installTxt);
        while (trayMatcher.find()) {
            // parse the key-val structure
            Map<String, String> dat = new HashMap<>();
            final String descr = trayMatcher.group("descr");
            for (String entry : descr.split(";")) {
                String[] kv = entry.split("=");
                dat.put(kv[0], kv[1]);
            }
            Positions t = new Positions();
            t.index = Integer.parseInt(trayMatcher.group("n"));
            String u = dat.get("U");
            if (u != null && u.compareTo("NOT_SET") != 0) {
                t.x = Float.parseFloat(dat.get("X"));
                t.y = Float.parseFloat(dat.get("Y"));
                t.z = Float.parseFloat(dat.get("Z"));
                String c = dat.get("C");
                t.c = c.compareToIgnoreCase("On") == 0;
                positions.add(t);
            }
        }
        return positions;
    }

    private List<MRU> parseMRU() {
        LinkedList<MRU> mrus = new LinkedList<>();

        Matcher trayMatcher = Pattern.compile("ATTI_(?<n>[0-9]):(?<descr>[^,]+),", Pattern.MULTILINE)
                .matcher(this.installTxt);
        while (trayMatcher.find()) {
            // parse the key-val structure
            Map<String, String> dat = new HashMap<>();
            final String descr = trayMatcher.group("descr");
            for (String entry : descr.split(";")) {
                String[] kv = entry.split("=");
                dat.put(kv[0], kv[1]);
            }
            MRU t = new MRU();
            t.index = Integer.parseInt(trayMatcher.group("n"));
            String u = dat.get("U");
            if (u != null && u.compareTo("NOT_SET") != 0) {
                t.x = Float.parseFloat(dat.get("X"));
                t.y = Float.parseFloat(dat.get("Y"));
                t.z = Float.parseFloat(dat.get("Z"));
                t.r = Float.parseFloat(dat.get("R"));
                t.p = Float.parseFloat(dat.get("P"));
                t.h = Float.parseFloat(dat.get("H"));
                t.u = dat.get("U");
                mrus.add(t);
            }

        }
        return mrus;
    }

    private List<Tray> parseTray() {
        LinkedList<Tray> returnTrays = new LinkedList<>();

        try {

            Matcher trayMatcher = Pattern
                    .compile("TRAI_(?<type>TX|RX|HD)(?<n>[0-9]):(?<descr>[^,]+),", Pattern.MULTILINE)
                    .matcher(this.installTxt);
            while (trayMatcher.find()) {
                // parse the key-val structure
                Map<String, String> dat = new HashMap<>();
                final String descr = trayMatcher.group("descr");
                for (String entry : descr.split(";")) {
                    String[] kv = entry.split("=");
                    if (kv.length > 1) {

                        // TODO remove this, but this workaround.
                        if (kv[1].equals(".0.000000")) {
                            dat.put(kv[0], "0.000000");
                        } else {
                            dat.put(kv[0], kv[1]);
                        }
                    }
                }
                Tray t = new Tray();
                t.index = Integer.parseInt(trayMatcher.group("n"));
                t.sn = Integer.parseInt(dat.get("N"));
                t.x = Float.parseFloat(dat.get("X"));
                t.y = Float.parseFloat(dat.get("Y"));
                t.z = Float.parseFloat(dat.get("Z"));
                t.r = Float.parseFloat(dat.get("R"));
                t.p = Float.parseFloat(dat.get("P"));
                t.h = Float.parseFloat(dat.get("H"));
                t.g = Float.parseFloat(dat.getOrDefault("G", "0.0"));
                t.s = Float.parseFloat(dat.getOrDefault("S", "0.0"));

                // parse internal levers
                if (descr.contains("IPX")) {
                    t.ip = parseInternalLever(dat, "IP");
                }
                if (descr.contains("ICX")) {
                    t.ic = parseInternalLever(dat, "IC");
                }
                if (descr.contains("ISX")) {
                    t.is = parseInternalLever(dat, "IS");
                }
                if (descr.contains("IX")) {
                    t.i = parseInternalLever(dat, "I");
                }
                if (descr.contains("ITX")) {
                    t.it = parseInternalLever(dat, "IT");
                }
                if (descr.contains("IRX")) {
                    t.ir = parseInternalLever(dat, "IR");
                }

                String tpe = trayMatcher.group("type");
                if (tpe.equals("TX")) {
                    t.type = AntennaType.TX;
                    returnTrays.add(t);
                } else if (tpe.equals("RX")) {
                    t.type = AntennaType.RX;
                    returnTrays.add(t);
                } else if (tpe.equals("HD")) { // HEAD
                    // TODO : apply offsets in the good referential. Offset are given in transducer
                    // referencial, not vessel referential like installation parameters.
                    // We should also consider adding TX trays for other EM2040 transducers or no
                    // one at all (even if TRAI_TX is define).
                    // If tx sub-trays (sub-array) are added, we should use the MRZ.getTxSubArray
                    // instead of the MRZ.getTxArrNumber to retrieve the effective tx transducer index

                    if (getModel().equals("EM2040C")) {
                        // EM2040C 1 RX, 1 TX array per head
                        //
                        Tray rx = new Tray(t);
                        rx.type = AntennaType.RX;
                        rx.x += InstallationMetadata.RxOffset2040c[0][0];
                        rx.y += InstallationMetadata.RxOffset2040c[0][1];
                        rx.z += InstallationMetadata.RxOffset2040c[0][2];
                        returnTrays.add(rx);

                        Tray tx = new Tray(t);
                        tx.type = AntennaType.TX;
                        tx.x += InstallationMetadata.TxOffset2040c[0][0];
                        tx.y += InstallationMetadata.TxOffset2040c[0][1];
                        tx.z += InstallationMetadata.TxOffset2040c[0][2];
                        returnTrays.add(tx);
                    } else {
                        // EM2040P 1 RX, 3 TX arrays
                        //
                        Tray rx = new Tray(t);
                        rx.type = AntennaType.RX;
                        rx.x += InstallationMetadata.RxOffset2040p[0][0];
                        rx.y += InstallationMetadata.RxOffset2040p[0][1];
                        rx.z += InstallationMetadata.RxOffset2040p[0][2];
                        returnTrays.add(rx);

                        Tray tx1 = new Tray(t);
                        Tray tx2 = new Tray(t);
                        Tray tx3 = new Tray(t);
                        Tray[] txs = {tx1, tx2, tx3};
                        for (int i = 0; i < 3; i++) {
                            txs[i].type = AntennaType.TX;
                            txs[i].x += InstallationMetadata.TxOffset2040p[i][0];
                            txs[i].y += InstallationMetadata.TxOffset2040p[i][1];
                            txs[i].z += InstallationMetadata.TxOffset2040p[i][2];
                            returnTrays.add(txs[i]);
                        }
                    }
                }
            }
        } catch (Exception e) {
            DefaultLogger.get().warn("Error while parsing IIP : " + e.getMessage(), e);
        }

        // by convention we expect to have Rx antenna first stored then Tx antenna
        // sort Trays with tx last
        returnTrays.sort((tray0, tray1) -> {
            if (tray0.type == tray1.type) {
                // compare on the starboard, port value
                return Double.compare(tray0.y, tray1.y);
            } else {
                // set RX before TX
                if (tray0.type == AntennaType.RX)
                    return -1;
                return 1;
            }
        });
        return returnTrays;
    }

    protected InternalLever parseInternalLever(Map<String, String> dat, String prefix) {
        InternalLever il = new InternalLever();
        il.x = Float.parseFloat(dat.get(prefix + "X"));
        il.y = Float.parseFloat(dat.get(prefix + "Y"));
        il.z = Float.parseFloat(dat.get(prefix + "Z"));
        return il;
    }

    public String getModel() {
        return model_type;
    }

    /**
     * Return the storage index in xsf for this kongsberg transducer index
     */
    public int getFirstTxTransducerIndex() {
        int index = 0;
        for (Tray t : trays) {
            if (t.type == AntennaType.TX) {
                return index;
            }
            index++;
        }
        return 0;
    }

    /**
     * Return the storage index in xsf for this kongsberg transducer index
     */
    public int getTxIndex(int kmIndex) {
        return getFirstTxTransducerIndex() + kmIndex;
    }

    /**
     * Return the storage index in xsf for this kongsberg transducer index
     */
    public int getRxIndex(int kmIndex) {
        int index = 0;
        for (Tray t : trays) {
            // for kongsberg transducer in IIP index start at 1, not 0
            if (t.type == AntennaType.RX && t.index == kmIndex + 1) {
                return index;
            }
            index++;
        }
        return 0;
    }

    public int getS_no_tx() {
        return s_no_tx;
    }

    public int getS_no_rx() {
        return s_no_rx;
    }

    public List<Tray> getTrays() {
        return trays;
    }

    public short getSerialId() {
        return serialId;
    }

    public List<MRU> getMrus() {
        return mrus;
    }

    public List<Positions> getPositions() {
        return positions;
    }

    public float getWaterlineVerticalLocation() {
        return waterlineVerticalLocation;
    }

    public String getCPUVersion() {
        return cpuVersion;
    }
}
