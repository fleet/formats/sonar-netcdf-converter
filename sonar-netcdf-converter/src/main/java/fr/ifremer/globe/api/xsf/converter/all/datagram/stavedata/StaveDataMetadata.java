package fr.ifremer.globe.api.xsf.converter.all.datagram.stavedata;

import java.util.Set;

import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.DatagramMetadata;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;

/**
 * */
public class StaveDataMetadata extends DatagramMetadata<SwathIdentifier,StaveDataCompletePingMetadata> {
    
    public int numTxSector = 0;

    /**
     * max wc beamCount per datagram
     */
    int maxSampleCount = 0;

    /**
     * The total number of beams across all the swath
     */
    long totalStaveCount = 0;

    /**
     * @return the totalSampleCount
     */
    public int getMaxSampleCount() {
        return maxSampleCount;
    }

    /**
     * @return the total stave count
     */
    public long getTotalStaveCount() {
        return totalStaveCount;
    }

    /**
     * perform the index
     * @param allAntennas
     */
    public void computeIndex(Set<Integer> allAntennas) {
        totalStaveCount = 0;
        maxSampleCount = 0;
        for (StaveDataCompletePingMetadata ping : index.values()) {
            ping.computeIndex();
            if (ping.isComplete(allAntennas)) {
                totalStaveCount += ping.getStaveCount();
                maxSampleCount = Integer.max(maxSampleCount, ping.getSampleCount());
            }
        }
    }
}
