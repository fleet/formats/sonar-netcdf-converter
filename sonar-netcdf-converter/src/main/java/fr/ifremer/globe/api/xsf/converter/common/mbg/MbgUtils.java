package fr.ifremer.globe.api.xsf.converter.common.mbg;

import java.util.NavigableMap;

import fr.ifremer.globe.utils.mbes.Ellipsoid;
import fr.ifremer.globe.utils.mbes.MbesUtils;

public class MbgUtils {

	/**
	 * compute horizontal distance scale for one ping this scale has to be recorded in mbDistanceScale later
	 *
	 * @param maxDistance maximum horizontal distance
	 * @return horizontal distance scale for the ping
	 */
	public static double getHorizontalScale(double maxDistance) {
		double distRes;
		if (maxDistance <= 327) {
			distRes = 0.01;
		} else if (maxDistance <= 655) {
			distRes = 0.02;
		} else if (maxDistance <= 1638) {
			distRes = 0.05;
		} else if (maxDistance <= 3276) {
			distRes = 0.1;
		} else if (maxDistance <= 6553) {
			distRes = 0.2;
		} else if (maxDistance <= 16383) {
			distRes = 0.5;
		} else if (maxDistance <= 32767) {
			distRes = 1.;
		} else if (maxDistance <= 65534) {
			distRes = 2.;
		} else if (maxDistance <= 163835) {
			distRes = 5.;
		} else {
			distRes = 10.;
		}

		return distRes;
	}

	/**
	 * @return position (lat/lon) for a a sounding detection.
	 */
	public static double[] getDetectionPosition(double latNav, double lonNav, double along, double across,
			double heading) {
		double norm = MbesUtils.calcNormRayon(latNav, Ellipsoid.WGS84)[0];
		double rayon = MbesUtils.calcNormRayon(latNav, Ellipsoid.WGS84)[1];
		double sinHeading = Math.sin(Math.toRadians(heading));
		double cosHeading = Math.cos(Math.toRadians(heading));

		double lat = latNav + Math.toDegrees(along * cosHeading - across * sinHeading) / rayon;

		double lon = lonNav
				+ Math.toDegrees(along * sinHeading + across * cosHeading) / norm / Math.cos(Math.toRadians(latNav));

		return new double[] { lat, lon };
	}

	/**
	 * Get interpolated attitude values for a specific timestamp.
	 */
	public static double[] getAttitudeByInterpolation(long currentTime, NavigableMap<Long, short[]> attitudeValues) {

		// Get attitude values before and after the specified time stamp
		Long timeBefore = attitudeValues.floorKey(currentTime);
		Long timeAfter = attitudeValues.ceilingKey(currentTime);

		// If no attitude values have been found before; use the 2 first values
		if (timeBefore == null) {
			timeBefore = attitudeValues.firstKey();
			timeAfter = attitudeValues.higherKey(timeBefore);
		}
		// If no attitude values have been found after; use the 2 last values
		if (timeAfter == null) {
			timeAfter = attitudeValues.lastKey();
			timeBefore = attitudeValues.lowerKey(timeAfter);
		}

		short[] attitudeBefore = attitudeValues.get(timeBefore);
		short[] attitudeAfter = attitudeValues.get(timeAfter);
		double rollBefore = attitudeBefore[0] / 100d;
		double pitchBefore = attitudeBefore[1] / 100d;
		double heaveBefore = attitudeBefore[2] / 100d;
		double rollAfter = attitudeAfter[0] / 100d;
		double pitchAfter = attitudeAfter[1] / 100d;
		double heaveAfter = attitudeAfter[2] / 100d;

		// Linear interpolation
		double coeff = !timeAfter.equals(timeBefore) ? ((double) (currentTime - timeBefore)) / (timeAfter - timeBefore)
				: 0;

		double roll = rollBefore + coeff * (rollAfter - rollBefore);
		double pitch = pitchBefore + coeff * (pitchAfter - pitchBefore);
		double heave = heaveBefore + coeff * (heaveAfter - heaveBefore);

		return new double[] { roll, pitch, heave };
	}

	/**
	 * @return string value from flag byte.
	 */
	public static String flagToString(byte flagByte) {
		switch (flagByte) {
		case MbgConstants.MB_TRT_FLAG_ON:
			return "Done";
		case MbgConstants.MB_TRT_FLAG_OFF:
			return "No";
		default:
			return "Invalid (" + flagByte + ")";
		}
	}

	/**
	 * @return string value for tide type integer.
	 */
	public static String getTideType(int intTide) {
		switch (intTide) {
		case MbgConstants.MB_FLAG_NO_TIDE:
			return "unknown";
		case MbgConstants.MB_FLAG_PREDICTED_TIDE:
			return "predicted";
		case MbgConstants.MB_FLAG_MESURED_TIDE:
			return "mesured";
		case MbgConstants.MB_FLAG_GPS_TIDE:
			return "GPS";
		default:
			return "Unknown";
		}
	}

}
