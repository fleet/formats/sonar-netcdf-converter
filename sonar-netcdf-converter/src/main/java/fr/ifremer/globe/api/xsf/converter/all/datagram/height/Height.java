package fr.ifremer.globe.api.xsf.converter.all.datagram.height;

import java.nio.ByteBuffer;

import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagram;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.SInt;
import fr.ifremer.globe.api.xsf.util.types.UByte;
import fr.ifremer.globe.api.xsf.util.types.UShort;

/**
 * Stateless Clock datagram
 */
public class Height extends BaseDatagram {

	@Override
	public HeightMetadata getSpecificMetadata(AllFile metadata) {
		return metadata.getHeight();
	}

	@Override
	protected void computeSpecificMetadata(AllFile metadata, ByteBuffer datagram, byte datagramType,
			long epochTime, DatagramPosition position) {
		final HeightMetadata index = getSpecificMetadata(metadata);
		index.addDatagram(position, getEpochTime(datagram), 1);
		index.setSerialNumber(getSerialNumber(datagram));

		// Calcul de stats
		index.addHeight(getHeight(datagram).get());
		index.computeMinMaxHeight(getHeight(datagram).get());
	}
	
	public static UShort getCounter(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 12);
	}

	public static UShort getSerialNumber(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 14);
	}

	public static SInt getHeight(ByteBuffer datagram) {
		return TypeDecoder.read4S(datagram, 16);
	}

	public static UByte getHeightType(ByteBuffer datagram) {
		return TypeDecoder.read1U(datagram, 18);
	}


	public static void print(ByteBuffer datagram) {
		System.out.println(getCounter(datagram));
		System.out.println(getSerialNumber(datagram));

	}

}
