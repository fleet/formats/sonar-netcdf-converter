package fr.ifremer.globe.api.xsf.converter.common.xsf;

import org.slf4j.Logger;

import fr.ifremer.globe.api.xsf.converter.XsfConverterParameters;
import fr.ifremer.globe.api.xsf.converter.common.INcFileWriter;
import fr.ifremer.globe.api.xsf.converter.common.sounder.ISounderFile;
import fr.ifremer.globe.netcdf.ucar.NCException;

public abstract class XsfWriter<T extends ISounderFile, U extends XsfBase>
		implements INcFileWriter<T, U, XsfConverterParameters> {

	@Override
	public void postProcess(T inputSounderFile, U xsfBase, XsfConverterParameters params, Logger logger) throws NCException {
		xsfBase.computeSummary();
	}
}
