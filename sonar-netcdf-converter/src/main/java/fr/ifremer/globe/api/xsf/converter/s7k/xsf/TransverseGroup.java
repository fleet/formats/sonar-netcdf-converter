package fr.ifremer.globe.api.xsf.converter.s7k.xsf;

import java.io.IOException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Map.Entry;

import org.apache.commons.io.FilenameUtils;

import fr.ifremer.globe.api.xsf.converter.SounderFileConverter;
import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfConstants;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.BathymetryGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.BeamGroup1Grp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.ProvenanceGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.RootGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.SonarGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.BeamGroup1VendorSpecificGrp;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KFile;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.SwathIndexContainer;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCVariable;

/**
 * Handle data transverse to all or several groups
 * 
 */
public class TransverseGroup implements IDatagramConverter<S7KFile, XsfFromS7k> {

	private SwathIndexContainer indexes;
	private BeamGroup1Grp beamGrp;
	private S7KFile vendor;
	private BathymetryGrp bathyGrp;
	private SonarGrp sonarGrp;
	private XsfFromS7k xsf;

	public TransverseGroup(SwathIndexContainer indexes) {
		this.indexes = indexes;
	}

	@Override
	public void declare(S7KFile s7kFile, XsfFromS7k xsf) throws NCException {
		this.xsf = xsf;
		vendor = s7kFile;
		beamGrp = xsf.getBeamGroup();
		bathyGrp = xsf.getBathyGrp();
		sonarGrp = xsf.getSonarGrp();
	}

	@Override
	public void fillData(S7KFile s7kFile) throws IOException, NCException {
		fillSonarData();
		fillPingCounter();
		fillSampleTimeOffset();
		fillStatusDetail();
		fillProvenance();
		fillProcessingStatus();
		fillWC();
		fillTxTranducerIndex();
		fillTxBeamIndex();
	}

	private void fillSonarData() throws NCException {
		sonarGrp.addAttribute(SonarGrp.ATT_SONAR_MANUFACTURER, "Reson");
		sonarGrp.addAttribute(SonarGrp.ATT_SONAR_MODEL, vendor.getSounderDescription().getName());
		sonarGrp.addAttribute(SonarGrp.ATT_SONAR_SERIAL_NUMBER, vendor.getStats().getUniqueDeviceId());
		sonarGrp.addAttribute(SonarGrp.ATT_SONAR_TYPE, XsfConstants.SONAR_TYPE_NAME);
	}
	
	private void fillStatusDetail() throws NCException {

		{// Fill for WC
			if (vendor.getSwathCount() > 0) {
				long[] origin = { 0, };
				long[] count = { vendor.getSwathCount() };
				byte[] values = new byte[vendor.getSwathCount()];
				Arrays.fill(values, (byte) 1); // fill with invalid values
				xsf.getBeamVendor().getPing_validity().put(origin, count, values);
			}
		}
		{ // Fill for bathymetry
			if (vendor.getDetectionCount() > 0) {
				long[] origin = { 0, 0 };
				long[] count = { 1, vendor.getDetectionCount() };
				byte[] values = new byte[vendor.getDetectionCount()];
				Arrays.fill(values, (byte) 0);
				// for .all files, sector ids are the same as sector indexes
				for (int swath = 0; swath < vendor.getSwathCount(); swath++) {
					origin[0] = swath;
					bathyGrp.getStatus_detail().put(origin, count, values);
				}
			}
		}
	}

	private void fillProvenance() throws NCException {
		String software = SounderFileConverter.NAME + " " + SounderFileConverter.VERSION + "/Ifremer";
		String version = SounderFileConverter.VERSION;
		String time = Instant.now().truncatedTo(ChronoUnit.SECONDS).toString();
		String filename = FilenameUtils.getName(vendor.getFilePath());
		
		//software attributes
		xsf.getProvenanceGrp().addAttribute(ProvenanceGrp.ATT_CONVERSION_SOFTWARE_NAME, software);
		xsf.getProvenanceGrp().addAttribute(ProvenanceGrp.ATT_CONVERSION_SOFTWARE_VERSION, version);
		xsf.getProvenanceGrp().addAttribute(ProvenanceGrp.ATT_CONVERSION_TIME, time);
		
		//filenames
		long[] origin = { 0, };
		long[] count = { 1 };
		String[] filenames = {filename};
		xsf.getProvenanceGrp().getSource_filenames().put(origin, count, filenames);
	
		// history
		String[] history = { time + //
				" Converted from " + filename + //
				" with " + software };
		xsf.getProvenanceGrp().addAttributeTextArray(ProvenanceGrp.ATT_HISTORY, history);
	}

	private void fillProcessingStatus() throws NCException {
		// FILL PROCESSING STATUS HERE
		xsf.getRootGrp().addAttribute(RootGrp.ATT_PROCESSING_STATUS, "");
	}

	private void fillWC() throws NCException {

		// check some values extracted from datagram viewer

		if (vendor.getWcBeamCount() == 0)
			return;
		for (int s = 0; s < vendor.getSwathCount(); s++) {
			long[] start1 = { 0 };
			long[] count1 = { vendor.getSwathCount() };

			float samplingTab[] = beamGrp.getSample_interval().get_float(start1, count1);
			float sampling = 1 / samplingTab[0];

			// fill default values
			{
				long[] start2 = { s, 0 };
				long[] count2 = { 1, vendor.getWcBeamCount() };

				float defaultValue[] = new float[vendor.getWcBeamCount()];
				Arrays.fill(defaultValue, 0);
				beamGrp.getBlanking_interval().put(start2, count2, defaultValue);
			}

		}
	}

	private void fillTxTranducerIndex() throws NCException {
		int txbeam = vendor.getTxSectorCount();
		long[] origin = { 0, 0 };
		long[] count = { vendor.getSwathCount(), txbeam };
		int[] value = new int[vendor.getSwathCount() * txbeam];
		Arrays.fill(value, 0);
		beamGrp.getTransmit_transducer_index().put(origin, count, value);

	}

	private void fillTxBeamIndex() throws NCException {
		int beam = vendor.getWcBeamCount();
		if (beam == 0)
			return;
		
		long[] origin = { 0, 0 };
		long[] count = { vendor.getSwathCount(), beam };
		int[] value = new int[vendor.getSwathCount() * beam];
		Arrays.fill(value, 0);
		beamGrp.getTransmit_beam_index().put(origin, count, value);

	}

	private void fillSampleTimeOffset() throws NCException {

		for (int index = 0; index < vendor.getSwathCount(); index++) {

			long[] s = { index, 0 };
			long[] c = { 1, vendor.getTxSectorCount() };
			float[] defaultValues = new float[vendor.getTxSectorCount()];
			Arrays.fill(defaultValues, 0);
			beamGrp.getSample_time_offset().put(s, c, defaultValues);

		}
	}

	private void fillPingCounter() throws NCException {
		long[] origin = { 0 };
		long[] count = { 1 };
		int[] values = new int[1];
		NCVariable v = xsf.getBeamVendor().getVariable(BeamGroup1VendorSpecificGrp.PING_RAW_COUNT);
		for (Entry<SwathIdentifier, Integer> entry : indexes.getEntries()) {
			int index = entry.getValue();
			SwathIdentifier id = entry.getKey();
			origin[0] = index;
			values[0] = (int) id.getRawPingNumber();
			v.put(origin, count, values);
		}
	}
}
