package fr.ifremer.globe.api.xsf.converter.all.datagram.rawrange70;

import java.nio.ByteBuffer;

import fr.ifremer.globe.api.xsf.converter.all.datagram.PingDatagram;
import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.SByte;
import fr.ifremer.globe.api.xsf.util.types.SShort;
import fr.ifremer.globe.api.xsf.util.types.UByte;
import fr.ifremer.globe.api.xsf.util.types.UShort;

public class RawRange70 extends PingDatagram {
	/** {@inheritDoc} */
	@Override
	public RawRange70Metadata getSpecificMetadata(AllFile metadata) {
		return metadata.getRawRange70();
	}

	@Override
	protected void computeSpecificMetadata(AllFile metadata, ByteBuffer datagram, long epochTime, SwathIdentifier swathId,
			int serialNumber, DatagramPosition position) {
		RawRange70Metadata rawRange70Metadata = getSpecificMetadata(metadata);
		UByte nBeams = getMaxBeamsNumber(datagram);
		RawRange70CompletePingMetadata completeping = rawRange70Metadata.getPing(swathId);
		if (completeping == null) {
			completeping = new RawRange70CompletePingMetadata(metadata.getInstallation().getSerialNumbers());
			rawRange70Metadata.addPing(swathId, completeping);
		}
		completeping.addAntenna(serialNumber, new RawRange70CompleteAntennaPingMetadata(position, nBeams.getU()));
	}

	public static UShort getCounter(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 12);
	}

	public static UShort getSerialNumber(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 14);
	}

	public static UByte getMaxBeamsNumber(ByteBuffer datagram) {
		return TypeDecoder.read1U(datagram, 16);
	}

	public static UByte getValidBeamsCounter(ByteBuffer datagram) {
		return TypeDecoder.read1U(datagram, 17);
	}

	public static UShort getSoundSpeed(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 18);
	}

	/**
	 * START Cycle Entries
	 */
	/*
	 * START Entries Block (Samples)
	 */
	
	public static int getBeamOffset(int startRxOffset, int idx) {
	    return startRxOffset + 8 * idx;
	}
	/**
	 * return the given value
	 * 
	 * @param datagram
	 *            the datagram buffer
	 * @param index
	 *            the starting offset of the considered rxBeam
	 */
	public static SShort getBeamPointingAngle(ByteBuffer datagram, int index) {
		return TypeDecoder.read2S(datagram,  20 + 8 * index);
	}

	/**
	 * return the given value
	 * 
	 * @param datagram
	 *            the datagram buffer
	 * @param index
	 *            the starting offset of the considered rxBeam
	 */
	public static SShort getTxTilt(ByteBuffer datagram, int index) {
		return TypeDecoder.read2S(datagram, 20 +  8 * index + 2);
	}

	/**
	 * return the given value
	 * 
	 * @param datagram
	 *            the datagram buffer
	 * @param index
	 *            the starting offset of the considered rxBeam
	 */
	public static UShort getRange(ByteBuffer datagram, int index) {
		return TypeDecoder.read2U(datagram, 20 +  8 * index + 4);
	}


	/**
	 * return the given value
	 * 
	 * @param datagram
	 *            the datagram buffer
	 * @param index
	 *            the starting offset of the considered rxBeam
	 */
	public static SByte getReflectivity(ByteBuffer datagram, int index) {
		return TypeDecoder.read1S(datagram, 20 +  8 * index + 6);
	}

	/**
	 * return the given value
	 * 
	 * @param datagram
	 *            the datagram buffer
	 * @param index
	 *            the starting offset of the considered rxBeam
	 */
	public static UByte getBeamNumber(ByteBuffer datagram, int index) {
		return TypeDecoder.read1U(datagram, 20 +  8 * index + 7);
	}

	
}
