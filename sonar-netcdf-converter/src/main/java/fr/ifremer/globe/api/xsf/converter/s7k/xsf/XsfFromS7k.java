package fr.ifremer.globe.api.xsf.converter.s7k.xsf;

import java.io.IOException;
import java.util.HashMap;

import fr.ifremer.globe.api.xsf.converter.XsfConverterParameters;
import fr.ifremer.globe.api.xsf.converter.common.sounder.ISounderFile;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfBase;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.BeamGroup1Grp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.BathymetryVendorSpecificGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.BeamGroup1VendorSpecificGrp;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KFile;
import fr.ifremer.globe.netcdf.jna.Nc4prototypes;
import fr.ifremer.globe.netcdf.ucar.NCException;

public class XsfFromS7k extends XsfBase {

	private BathymetryVendorSpecificGrp bathyVendorGrp;
	private BeamGroup1VendorSpecificGrp beamVendorGrp;

	public XsfFromS7k(S7KFile s7kFile, XsfConverterParameters params) throws NCException, IOException {
		super(s7kFile, params);
		bathyVendorGrp = new BathymetryVendorSpecificGrp(getBathyGrp(), s7kFile,new HashMap<String, Integer>());
		beamVendorGrp = new BeamGroup1VendorSpecificGrp(getBeamGroup(), s7kFile,new HashMap<String, Integer>());
	}

	public BathymetryVendorSpecificGrp getBathyVendor() {
		return bathyVendorGrp;
	}

	public BeamGroup1VendorSpecificGrp getBeamVendor() {
		return beamVendorGrp;
	}
	
	/**Create BeamGrp, override this function if needed to override type definition*/
	protected BeamGroup1Grp makeBeamGrp(ISounderFile sounderFile) throws NCException {
		HashMap<String,Integer> typeDict= new HashMap<>();
		typeDict.put("sample_t",Nc4prototypes.NC_SHORT);
		return new BeamGroup1Grp(this.getSonarGrp(), sounderFile, DEFAULT_BEAM_GROUP_NAME,typeDict);
	}
}
