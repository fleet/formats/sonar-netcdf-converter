package fr.ifremer.globe.api.xsf.converter.all.xsf.bathy;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.all.datagram.seabed89.Seabed89;
import fr.ifremer.globe.api.xsf.converter.all.datagram.seabed89.Seabed89CompleteAntennaPingMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.seabed89.Seabed89CompletePingMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.seabed89.Seabed89Metadata;
import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2F4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2S4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2U2;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfFromKongsberg;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.BathymetryVendorSpecificGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.BeamGroup1VendorSpecificGrp;
import fr.ifremer.globe.api.xsf.util.DefaultLogger;
import fr.ifremer.globe.api.xsf.util.ObjectBufferPool;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCVariable;
import fr.ifremer.globe.netcdf.util.Pair;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

public class Seabed89Group implements IDatagramConverter<AllFile, XsfFromKongsberg> {

    private AllFile stats;
    private Seabed89Metadata metadata;

    private List<ValueD2> swathAntennaValues;
    private List<ValueD2> swathBeamValues;
    private NCVariable samples;

    public Seabed89Group(AllFile md) {
        stats = md;
        metadata = md.getSeabed89();
        swathAntennaValues = new LinkedList<>();
        swathBeamValues = new LinkedList<>();
    }

    @Override
    public void declare(AllFile allFile, XsfFromKongsberg xsf) throws NCException {
        // declare the groups that will be used
        BathymetryVendorSpecificGrp bathy_group_vendor = xsf.getBathy();
        BeamGroup1VendorSpecificGrp beam_group_vendor = xsf.getBeamGroupVendorSpecific();

        swathAntennaValues.add(new ValueD2F4(beam_group_vendor.getSeabed_image_sample_rate(),
                (buffer, a) -> Seabed89.getSamplingFn(buffer.byteBufferData)));

        swathAntennaValues.add(new ValueD2U2(bathy_group_vendor.getRange_to_normal_incidence(),
                (buffer, a) -> Seabed89.getRangeToNormalIncidence(buffer.byteBufferData)));

        swathAntennaValues.add(new ValueD2F4(bathy_group_vendor.getBackscatter_normal_incidence_level(),
                (buffer, a) -> new FFloat(Seabed89.getNormalIncidenceBS(buffer.byteBufferData).get() * 0.1f)));

        swathAntennaValues.add(new ValueD2F4(bathy_group_vendor.getBackscatter_oblique_incidence_level(),
                (buffer, a) -> new FFloat(Seabed89.getObliqueBS(buffer.byteBufferData).get() * 0.1f)));

        swathAntennaValues.add(new ValueD2F4(bathy_group_vendor.getTx_beam_width_along(),
                (buffer, a) -> new FFloat(Seabed89.getTxBeamWidth(buffer.byteBufferData).getU() * 0.1f)));

        swathAntennaValues.add(new ValueD2F4(bathy_group_vendor.getTvg_law_cross_over_angle(),
                (buffer, a) -> new FFloat(Seabed89.getTVGLawCrossOver(buffer.byteBufferData).getU() * 0.1f)));

        // Create the swath-beam variables

        // detection info is given by detection values

        swathBeamValues.add(new ValueD2S4(xsf.getBathyGrp().getSeabed_image_center(),
                (buffer, i) -> Seabed89.getCenterSample(buffer.byteBufferData, i)));
        samples = xsf.getBathyGrp().getSeabed_image_samples_r();
    }

    @Override
    public void fillData(AllFile allFile) throws IOException, NCException {
        long[] origin2D = {0, 0};
        ObjectBufferPool<DatagramBuffer> pool = new ObjectBufferPool<>(() -> new DatagramBuffer(stats.getByteOrder()));
        int detectionCount = stats.getDetectionCount();
        int maxSampleCount = stats.getSeabed89().getMaxAbstractSampleCount();

        for (Entry<SwathIdentifier, Seabed89CompletePingMetadata> pingEntry : metadata.getPings()) {
            swathAntennaValues.forEach(ValueD2::clear);
            swathBeamValues.forEach(ValueD2::clear);
            final Seabed89CompletePingMetadata ping = pingEntry.getValue();
            if (!ping.isComplete(stats.getGlobalMetadata().getSerialNumbers())
                    || !allFile.getGlobalMetadata().getSwathIds().contains(pingEntry.getKey())) {
                continue;
            }

            List<Pair<DatagramBuffer, Seabed89CompleteAntennaPingMetadata>> data = DatagramReader.read(pool,
                    ping.locate());

            // get origin from swath identifier (ignore if matching index not found)
            var swathId = pingEntry.getKey();
            var optSwathIndex = stats.getGlobalMetadata().getSwathIds().getIndex(swathId);
            if (optSwathIndex.isEmpty()) {
                DefaultLogger.get().warn(
                        "Datagram Seabed89 associated with a missing ping is ignored (number = {}, sequence = {}).",
                        swathId.getRawPingNumber(), swathId.getSwathPosition());
                continue;
            }
            origin2D[0] = optSwathIndex.get();
            short[] sampleBuffer = new short[maxSampleCount * detectionCount];
            Arrays.fill(sampleBuffer, samples.getShortFillValue());

            // iterate over the antenna
            for (Pair<DatagramBuffer, Seabed89CompleteAntennaPingMetadata> antenna : data) {
                DatagramBuffer buffer = antenna.getFirst();

                // fill the swath x antenna related variables
                for (ValueD2 v : swathAntennaValues) {
                    v.fill(buffer, 0,
                            stats.getInstallation().getRxAntennaIndex(Seabed89.getSerialNumber(buffer.byteBufferData)));
                }

                // fill all the data based on their ping number
                final int nBeam = Seabed89.getReceivedBeamsCounter(buffer.byteBufferData).getU();
                final int beamOffset = ping.getBeamOffset(Seabed89.getSerialNumber(buffer.byteBufferData).getU());
                short[] allSamples = Seabed89.getAllSamples(buffer.byteBufferData);
                int sampleLocalOffset = 0;
                for (int i = 0; i < nBeam; i++) {
                    int globalBeamNumber = beamOffset + i;
                    int globalBeamOffset = globalBeamNumber * maxSampleCount;
                    int beamSampleCount = Seabed89.getSampleCount(buffer.byteBufferData, i).getU();
                    if (beamSampleCount > 0) {
                        System.arraycopy(allSamples, sampleLocalOffset, sampleBuffer, globalBeamOffset, beamSampleCount);
                        sampleLocalOffset += beamSampleCount;
                        for (ValueD2 value : swathBeamValues) {
                            value.fill(buffer, i, globalBeamNumber);
                        }
                    }
                }
            }
            // fill samples
            if (maxSampleCount > 0)
                samples.put(new long[]{origin2D[0], 0, 0}, new long[]{1, detectionCount, maxSampleCount}, sampleBuffer);

            // fill the data
            for (ValueD2 v : swathAntennaValues) {
                v.write(origin2D, new long[]{1, stats.getGlobalMetadata().getSerialNumbers().size()});
            }
            for (ValueD2 v : swathBeamValues) {
                v.write(origin2D, new long[]{1, ping.getBeamCount()});
            }

            // release the datagram buffers for the next ping
            for (Pair<DatagramBuffer, Seabed89CompleteAntennaPingMetadata> antenna : data) {
                pool.release(antenna.getFirst());
            }
        }
    }
}
