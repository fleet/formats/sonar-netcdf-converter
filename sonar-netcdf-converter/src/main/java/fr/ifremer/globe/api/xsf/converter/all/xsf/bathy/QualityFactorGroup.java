package fr.ifremer.globe.api.xsf.converter.all.xsf.bathy;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.all.datagram.qualityfactor.QualityFactor;
import fr.ifremer.globe.api.xsf.converter.all.datagram.qualityfactor.QualityFactorCompleteAntennaPingMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.qualityfactor.QualityFactorCompletePingMetadata;
import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2F4;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfFromKongsberg;
import fr.ifremer.globe.api.xsf.util.DefaultLogger;
import fr.ifremer.globe.api.xsf.util.ObjectBufferPool;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.util.Pair;

public class QualityFactorGroup implements IDatagramConverter<AllFile, XsfFromKongsberg> {

	private List<ValueD2> swathDetectionValue = new LinkedList<>();

	@Override
	public void declare(AllFile allFile, XsfFromKongsberg xsf) throws NCException {
		swathDetectionValue
				.add(new ValueD2F4(xsf.getBathyGrp().getDetection_quality_factor(), (buffer, beamIndexSource) -> {
					// retrieve Ifremer quality factor for the corresponding beam
					return new FFloat(QualityFactor.getSample(buffer.byteBufferData, beamIndexSource));
				}));
	}

	@Override
	public void fillData(AllFile allFile) throws IOException, NCException {
		long[] origin = { 0, 0 };
		ObjectBufferPool<DatagramBuffer> pool = new ObjectBufferPool<>(
				() -> new DatagramBuffer(allFile.getByteOrder()));

		for (Entry<SwathIdentifier, QualityFactorCompletePingMetadata> pingEntry : allFile.getQualityFactor()
				.getEntries()) {
			swathDetectionValue.forEach(ValueD2::clear);
			final QualityFactorCompletePingMetadata ping = pingEntry.getValue();
			// we do not process incomplete ping
			if (!ping.isComplete(allFile.getGlobalMetadata().getSerialNumbers())
					|| !allFile.getGlobalMetadata().getSwathIds().contains(pingEntry.getKey())) {
				continue;
			}

			List<Pair<DatagramBuffer, QualityFactorCompleteAntennaPingMetadata>> data = ping.read(pool);

			// get origin from swath identifier (ignore if matching index not found)
			var swathId = pingEntry.getKey();
			var optIndex = allFile.getGlobalMetadata().getSwathIds().getIndex(swathId);
			if (optIndex.isEmpty()) {
				DefaultLogger.get().warn(
						"Datagram QualityFactor associated with a missing ping is ignored (number = {}, sequence = {}).",
						swathId.getRawPingNumber(), swathId.getSwathPosition());
				continue;
			}
			origin[0] = optIndex.get();

			final int beamCount = ping.getBeamCount();

			// iterate over the antenna
			for (Pair<DatagramBuffer, QualityFactorCompleteAntennaPingMetadata> antenna : data) {
				DatagramBuffer buffer = antenna.getFirst();

				// fill all the data based on their ping number
				int nBeam = QualityFactor.getBeamCount(buffer.byteBufferData).getU();
				for (int i = 0; i < nBeam; i++) {
					int globalBeamNumber = ping
							.getBeamNumber(QualityFactor.getSerialNumber(buffer.byteBufferData).getU(), i);
					for (ValueD2 value : swathDetectionValue) {
						value.fill(antenna.getFirst(), i, globalBeamNumber);
					}
				}

			}

			for (ValueD2 v : swathDetectionValue) {
				v.write(origin, new long[] { 1, beamCount });
			}

			// release the datagram buffers for the next ping
			for (Pair<DatagramBuffer, QualityFactorCompleteAntennaPingMetadata> antenna : data) {
				pool.release(antenna.getFirst());
			}
		}
	}

}
