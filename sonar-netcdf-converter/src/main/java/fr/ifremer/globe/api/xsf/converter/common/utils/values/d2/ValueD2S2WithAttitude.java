package fr.ifremer.globe.api.xsf.converter.common.utils.values.d2;

import java.util.Arrays;

import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCVariable;

public class ValueD2S2WithAttitude extends ValueD2WithAttitude {
	protected short[] dataOut;
	private ValueProvider valueProvider;

	public ValueD2S2WithAttitude(NCVariable variable, ValueProvider filler) {
		super(variable);
		this.dataOut = new short[dim];
		this.valueProvider = filler;
	}
	
	@Override
	public void clear() {
		Arrays.fill(this.dataOut, variable.getShortFillValue());
	}
	
	/**
	 * Here the lambda expression will expect roll/pitch/heave data in addition of the input buffer
	 */
	public interface ValueProvider {
		public short get(BaseDatagramBuffer buffer, int beamIndexSource, double roll, double pitch, double heave);
	}

	@Override
	public void fill(BaseDatagramBuffer buffer, int beamIndexSource, int beamIndexDest, double roll, double pitch,
			double heave) {
		dataOut[beamIndexDest] = valueProvider.get(buffer, beamIndexSource, roll, pitch, heave);
	}

	@Override
	public void write(long[] origin, long[] count) throws NCException {
		try {
			variable.put(origin, count, dataOut);
		} catch (Exception e) {
			logger.error(String.format("Error while writing variable %s (%s)", variable.getName(), e.getMessage()));
			throw e;
		}
	}

}
