package fr.ifremer.globe.api.xsf.converter.common.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * XSF converter internal logger
 */
public class XLog {
	// utility class only, private constructor
	private XLog() {
	}

	protected static Logger logger = LoggerFactory.getLogger(XLog.class);
	/**
	 * see {@link Logger#debug(String)}
	 * */
	public static void debug(String message) {
		logger.debug(message);
	}

	/**
	 * see {@link Logger#info(String)}
	 * */
	public static void info(String message) {
		logger.info(message);
	}

	/**
	 * see {@link  Logger#warn(String,Throwable)}
	 * */

	public static void warn(String message, Throwable e) {
		logger.warn(message, e);
	}

	/**
	 * see {@link Logger#warn(String)}
	 * */
	public static void warn(String message) {
		logger.warn(message);
	}

	/**
	 * see {@link Logger#error(String)}
	 * */
	public static void error(String message) {
		logger.error(message);
	}

	/**
	 * see {@link Logger#error(String, Throwable)}
	 * */
	public static void error(String message, Throwable e) {
		logger.error(message, e);
	}
}
