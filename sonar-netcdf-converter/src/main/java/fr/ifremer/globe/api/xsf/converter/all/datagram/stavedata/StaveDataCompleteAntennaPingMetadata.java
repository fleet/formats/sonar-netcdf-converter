package fr.ifremer.globe.api.xsf.converter.all.datagram.stavedata;

import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;

/**
 * all datagram metadata for a ping/antenna couple
 */
public class StaveDataCompleteAntennaPingMetadata {
	
    /**
     * number of datagram for the current antenna
     */
    int datagramCount;

    /**
     * Where the the datagrams are located in files
     */
	DatagramPosition positions[];
	
	/**
	 * number of samples for each DG
	 */
	int[] sampleCountPerDG;
	
	/**
	 * nember of stave for each sample for a given DG
	 */
	int[] stavePerSamplePerDG;
	
	/**
	 * indicates if a givan datagram have been seen
	 */
	private boolean valid[]; // indicate if datagram was filled or not

	StaveDataCompleteAntennaPingMetadata(int datagramCount) {
		allocate(datagramCount);
	}

	private void allocate(int datagramCount) {
		this.datagramCount = datagramCount;
		positions = new DatagramPosition[datagramCount];
		sampleCountPerDG = new int[datagramCount];
		stavePerSamplePerDG = new int[datagramCount];
		valid = new boolean[datagramCount];
		
		for (int i = 0; i < datagramCount; i++) {
		    valid[i] = false;
		}
	}

	/**
	 * add a new bathymetry datagram for the given ping
	 * 
	 * @param datagramIndex
	 *            the datagramIndex (datagramNumber -1)
	 */
	protected void pushDatagram(DatagramPosition position, int datagramIndex, int sampleCount, int stavePerSample) {
		positions[datagramIndex] = position;
		sampleCountPerDG[datagramIndex] = sampleCount;
		stavePerSamplePerDG[datagramIndex] = stavePerSample;
		valid[datagramIndex] = true;
	}

	/**
	 * check if all datagrmas have been seen
	 */
	public boolean isComplete() {
		for (boolean v : valid) {
			if (v == false) {
				return false;
			}
		}
		return true;
	}

	/**
	 * total number of samples for the current antenna
	 */
	public int getSampleCount() {
		int result = 0;
		for (int i : sampleCountPerDG) {
			result += i;
		}
		return result;
	}

	/**
	 * number of datagrams for the current antenna
	 */
	public int getDatagramCount() {
		return datagramCount;
	}

	/**
	 * total number of stave for the current antenna
	 */
	public int getStaveCount() {
	    int sum = 0;
	    for (int i = 0; i < datagramCount; i++) {
	        sum += sampleCountPerDG[i] * stavePerSamplePerDG[i];
	    }
	    return sum;
	}
}
