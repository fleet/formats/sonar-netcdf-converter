package fr.ifremer.globe.api.xsf.converter.kmall.datagram;

import java.nio.ByteBuffer;

import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.KmallFile;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.SDEMetadata;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.DDouble;
import fr.ifremer.globe.api.xsf.util.types.FFloat;

public class SDE extends SCommon {
    
    @Override
    public void computeSpecificMetadata(KmallFile metadata, ByteBuffer datagram, String datagramType,
            DatagramPosition position) {
        getSpecificMetadata(metadata).addDatagram(position);        
    }
    
    @Override
    public SDEMetadata getSpecificMetadata(KmallFile metadata) {
        return metadata.getSDEMetadata();
    }
    
    
    public static FFloat getDepthUsed_m(ByteBuffer buffer) {
        return TypeDecoder.read4F(buffer, 24);
    }
    public static FFloat getOffset(ByteBuffer buffer) {
    	return TypeDecoder.read4F(buffer, 28);
    }
    public static FFloat getScale(ByteBuffer buffer) {
    	return TypeDecoder.read4F(buffer, 32);
    }
    public static DDouble getLatitude(ByteBuffer buffer) {
    	return TypeDecoder.read8F(buffer, 36);
    }
    public static DDouble getLongitude(ByteBuffer buffer) {
    	return TypeDecoder.read8F(buffer, 44);
    }
    public static String getDataFromSensor(ByteBuffer datagram) {
        final long fullDgSize = TypeDecoder.read4U(datagram, datagram.limit() - 4).getU();
        final int dataFromSensorOffset = 52;
        final int rawDataLen = (int) (fullDgSize - 8 - dataFromSensorOffset);
        
		final int pos = datagram.position();
        byte[] ret = new byte[rawDataLen];
        datagram.position(dataFromSensorOffset);
        datagram.get(ret, 0, rawDataLen);
        datagram.position(pos);
        
        return new String(ret);
    }    
}
