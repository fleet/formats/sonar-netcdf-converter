package fr.ifremer.globe.api.xsf.converter.s7k.xsf;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1F4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1S1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U8;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2F4;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.BathymetryGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.BeamGroup1Grp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.BeamGroup1VendorSpecificGrp;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K7000;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7K7000Metadata;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KFile;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KMultipingMetadata;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KMultipingSequenceMetadata;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KSwathMetadata;
import fr.ifremer.globe.api.xsf.util.DefaultLogger;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.api.xsf.util.types.SByte;
import fr.ifremer.globe.api.xsf.util.types.UByte;
import fr.ifremer.globe.api.xsf.util.types.UInt;
import fr.ifremer.globe.api.xsf.util.types.ULong;
import fr.ifremer.globe.netcdf.ucar.NCException;

public class S7K7000Converter implements IDatagramConverter<S7KFile, XsfFromS7k> {

	private List<ValueD1> valuesD1;
	// fake D2 value, indexed on TxSector (=1 on reson)
	private List<ValueD2> valuesTxSectorD2;

	/**
	 * Compute and return an array containing the start and stop frequency
	 * 
	 */
	public FFloat[] computeFrequenciesStartAndStop(BaseDatagramBuffer buffer) {
		float fq = S7K7000.getFrequency(buffer).get();
		int waveForm = (int) S7K7000.getTxPulseTypeIdentifier(buffer).getU(); // 0:CW, 1:FM
		float bandwidth = S7K7000.getTxPulseWidth(buffer).get();
		if (waveForm == 0) // CW
		{
			FFloat[] ret = { new FFloat(fq), new FFloat(fq) };
			return ret;
		} else {
			FFloat[] ret = { new FFloat(fq - bandwidth), new FFloat(fq + bandwidth) };
			return ret;
		}
	}

	@Override
	public void declare(S7KFile s7kFile, XsfFromS7k xsf) throws NCException {
		valuesD1 = new LinkedList<>();
		valuesTxSectorD2 = new LinkedList<>();
		BeamGroup1Grp beamGrp = xsf.getBeamGroup();
		BathymetryGrp bathyGrp = xsf.getBathyGrp();
		BeamGroup1VendorSpecificGrp beamVendor = xsf.getBeamVendor();

		valuesD1.add(new ValueD1U8(beamGrp.getPing_time(), (buffer) -> new ULong(S7K7000.getTimestampNano(buffer))));
		valuesD1.add(new ValueD1U4(beamVendor.getPing_raw_count(),
				(buffer) -> S7K7000.getPingNumber(buffer)));
		valuesD1.add(new ValueD1U1(bathyGrp.getMultiping_sequence(),
				(buffer) -> new UByte((byte) S7K7000.getMultiPingSequence(buffer).getU())));
        valuesD1.add(new ValueD1F4(beamGrp.getSound_speed_at_transducer(), S7K7000::getSoundVelocity));
		valuesTxSectorD2.add(new ValueD2F4(beamGrp.getTransmit_frequency_start(),
				(buffer, i) -> computeFrequenciesStartAndStop(buffer)[0]));
		valuesTxSectorD2.add(new ValueD2F4(beamGrp.getTransmit_frequency_stop(),
				(buffer, i) -> computeFrequenciesStartAndStop(buffer)[1]));
		valuesD1.add(new ValueD1F4(beamVendor.getSample_rate(), (buffer) -> S7K7000.getSampleRate(buffer)));
		valuesD1.add(new ValueD1F4(beamVendor.getRx_bandwidth(), (buffer) -> S7K7000.getReceiverBandwidth(buffer)));
		valuesD1.add(new ValueD1F4(beamVendor.getTx_pulse_width(), (buffer) -> S7K7000.getTxPulseWidth(buffer)));
		valuesD1.add(new ValueD1S1(beamVendor.getTx_pulse_type_identifier(),
				(buffer) -> new SByte((byte) S7K7000.getTxPulseTypeIdentifier(buffer).getU())));
		valuesD1.add(new ValueD1S1(beamVendor.getTx_pulse_envelope_identifier(),
				(buffer) -> new SByte((byte) S7K7000.getTxPulseEnvelopeIdentifier(buffer).getU())));
		valuesD1.add(new ValueD1F4(beamVendor.getTx_pulse_envelope_parameter(),
				(buffer) -> S7K7000.getTxPulseEnvelopeParameter(buffer)));
		valuesD1.add(new ValueD1U4(beamVendor.getTx_pulse_reserved(), (buffer) -> S7K7000.getTxPulseReserved(buffer)));
		valuesD1.add(new ValueD1F4(beamVendor.getMax_ping_rate(), (buffer) -> S7K7000.getMaxPingRate(buffer)));
		valuesD1.add(new ValueD1F4(beamVendor.getPing_period(), (buffer) -> S7K7000.getPingPeriod(buffer)));
		valuesD1.add(new ValueD1F4(beamVendor.getRange_selection(), (buffer) -> S7K7000.getRangeSelection(buffer)));
		valuesD1.add(new ValueD1F4(beamVendor.getPower_selection(), (buffer) -> S7K7000.getPowerSelection(buffer)));
		valuesD1.add(new ValueD1F4(beamVendor.getGain_selection(), (buffer) -> S7K7000.getGainSelection(buffer)));
		valuesD1.add(new ValueD1U4(beamVendor.getControl_flags(), (buffer) -> S7K7000.getControlFlag(buffer)));
		valuesD1.add(new ValueD1U4(beamVendor.getProjector_identifier(),
				(buffer) -> S7K7000.getProjectorIdentifier(buffer)));
		valuesD1.add(new ValueD1F4(beamVendor.getProjector_beam_steering_angle_horizontal(), (buffer) -> new FFloat(
				(float) Math.toDegrees(S7K7000.getProjectorBeamSteeringAngleHorizontal(buffer).get()))));
		valuesD1.add(new ValueD1F4(beamVendor.getProjector_beam_steering_angle_vertical(), (buffer) -> new FFloat(
				(float) Math.toDegrees(S7K7000.getProjectorBeamSteeringAngleVertical(buffer).get()))));
		valuesD1.add(new ValueD1F4(beamVendor.getProjector_beam_minus3dB_beam_width_vertical(), (buffer) -> new FFloat(
				(float) Math.toDegrees(S7K7000.getProjectorBeamMinus3dBBeamWidthVertical(buffer).get()))));
		valuesD1.add(
				new ValueD1F4(beamVendor.getProjector_beam_minus3dB_beam_width_horizontal(), (buffer) -> new FFloat(
						(float) Math.toDegrees(S7K7000.getProjectorBeamMinus3dBBeamWidthHorizontal(buffer).get()))));
		valuesD1.add(new ValueD1F4(beamVendor.getProjector_beam_focal_point(),
				(buffer) -> S7K7000.getProjectorBeamFocalPoint(buffer)));
		valuesD1.add(new ValueD1S1(beamVendor.getProjector_beam_weighting_window_type(),
				(buffer) -> new SByte((byte) S7K7000.getProjectorBeamWeightingWindowType(buffer).getU())));
		valuesD1.add(new ValueD1F4(beamVendor.getProjector_beam_weighting_window_parameter(),
				(buffer) -> S7K7000.getProjectorBeamWeightingWindowParameter(buffer)));
		valuesD1.add(new ValueD1U4(beamVendor.getTransmit_flags(), (buffer) -> S7K7000.getTransmitFlags(buffer)));
		valuesD1.add(new ValueD1U4(beamVendor.getHydrophone_identifier(),
				(buffer) -> S7K7000.getHydrophoneIdentifier(buffer)));
		valuesD1.add(new ValueD1S1(beamVendor.getReceive_beam_weighting_window(),
				(buffer) -> new SByte((byte) S7K7000.getReceiveBeamWeightingWindow(buffer).getU())));
		valuesD1.add(new ValueD1F4(beamVendor.getReceive_beam_weighting_parameter(),
				(buffer) -> S7K7000.getReceiveBeamWeightingParameter(buffer)));
		valuesD1.add(new ValueD1U4(beamVendor.getReceive_flags(), (buffer) -> S7K7000.getReceiveFlags(buffer)));
		valuesD1.add(new ValueD1F4(beamVendor.getReceive_beam_width(),
				(buffer) -> new FFloat((float) Math.toDegrees(S7K7000.getReceiveBeamWidth(buffer).get()))));
		valuesD1.add(new ValueD1F4(beamVendor.getBottom_detection_filter_info_min_range(),
				(buffer) -> S7K7000.getBottomDetectionFilterInfoMinRange(buffer)));
		valuesD1.add(new ValueD1F4(beamVendor.getBottom_detection_filter_info_max_range(),
				(buffer) -> S7K7000.getBottomDetectionFilterInfoMaxRange(buffer)));
		valuesD1.add(new ValueD1F4(beamVendor.getBottom_detection_filter_info_min_depth(),
				(buffer) -> S7K7000.getBottomDetectionFilterInfoMinDepth(buffer)));
		valuesD1.add(new ValueD1F4(beamVendor.getBottom_detection_filter_info_max_depth(),
				(buffer) -> S7K7000.getBottomDetectionFilterInfoMaxDepth(buffer)));
		valuesD1.add(new ValueD1F4(beamVendor.getAbsorption(), (buffer) -> S7K7000.getAbsorption(buffer)));
		valuesD1.add(new ValueD1F4(beamVendor.getSound_velocity(), (buffer) -> S7K7000.getSoundVelocity(buffer)));
		valuesD1.add(new ValueD1F4(beamVendor.getSpreading(), (buffer) -> S7K7000.getSpreading(buffer)));
	}

	@Override
	public void fillData(S7KFile s7kFile) throws IOException, NCException {
		S7K7000Metadata md = s7kFile.get7000Metadata();
		if (md.getEntries().size() != 1) {
			throw new RuntimeException("This converter only supports one sonar");
		}
		S7KMultipingMetadata mpmd = md.getEntries().iterator().next().getValue();
		DatagramBuffer buffer = new DatagramBuffer();
		long origin;

		// iterate over pings (by date)
		for (Entry<Long, S7KMultipingSequenceMetadata> posentry : mpmd.getEntries()) {
			// iterate over ping sequence
			for (Entry<Integer, S7KSwathMetadata> e : posentry.getValue().getEntries()) {

				for(var valueD1 : valuesD1)
					valueD1.clear();
				valuesTxSectorD2.forEach(ValueD2::clear);
				DatagramReader.readDatagram(e.getValue().getPosition().getFile(), buffer,
						e.getValue().getPosition().getSeek());
				SwathIdentifier swathId = new SwathIdentifier(S7K7000.getPingNumber(buffer).getU(),
						S7K7000.getMultiPingSequence(buffer).getU());

				// get origin from swath identifier (ignore if matching index not found)
				var optIndex = s7kFile.getSwathIndexer().getIndex(swathId);
				if (optIndex.isEmpty()) {
					DefaultLogger.get().warn(
							"Datagram S7K7000 associated with a missing ping is ignored (number = {}, sequence = {}).",
							swathId.getRawPingNumber(), swathId.getSwathPosition());
					continue;
				}
				origin = optIndex.get();

				for (ValueD1 v : this.valuesD1) {
					v.fill(buffer);
					v.write(new long[] { origin });
				}
				// fake D2 value, indexed on TxSector (=1 on reson)
				for (ValueD2 v : valuesTxSectorD2) {
					v.fill(buffer, 0, 0);
					v.write(new long[] { origin, 0 }, new long[] { 1, 1 });
				}
			}
		}
	}
}
