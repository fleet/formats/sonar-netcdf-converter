package fr.ifremer.globe.api.xsf.converter.common.datagram.metadata;

import java.util.LinkedList;
import java.util.List;

import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;

public abstract class AbstractCompleteAntennaPingSingleMetadata extends AbstractCompleteAntennaPingMetadata {

    protected DatagramPosition position;
    protected int beamCount;
    
    /** max number of abstract samples for one beam. Contains WC or Seabed sample count*/
	protected int maxSampleCount;
    
    public AbstractCompleteAntennaPingSingleMetadata(DatagramPosition position, int beamCount, int maxSampleCount) {
        this.position = position;
        this.beamCount = beamCount;
        this.maxSampleCount = maxSampleCount;
    }

    @Override
    public boolean isComplete() {
        return true;
    }

    @Override
    public int getBeamCount() {
        return beamCount;
    }

	@Override
	public int getMaxAbstractSampleCount() {
		return maxSampleCount;
	}
	
    @Override
    public List<DatagramPosition> locate() {
        List<DatagramPosition> ret = new LinkedList<DatagramPosition>();
        ret.add(position);
        return ret;
    }
}
