package fr.ifremer.globe.api.xsf.converter.common.utils;

import java.time.DateTimeException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import fr.ifremer.globe.utils.date.DateUtils;

public class DateUtil {
	static String TimeZone = "GMT+0";
	static ZoneId zoneId = ZoneId.of(TimeZone);

	/**
	 * Convert a date time from kongsberg date time format
	 * 
	 * @param dateKongsbergFormat the date in format 10000*year(4 digits)+100*month + day
	 * @param timeSinceMidnight the time elapsed in milliseconds from midnight
	 * @return the time since Epoch in milliseconds
	 */
	public static long convertKongsbergTime(long dateKongsbergFormat, long timeSinceMidnight) {
		int year = (int) (dateKongsbergFormat / 10000);
		int month = (int) ((dateKongsbergFormat - year * 10000) / 100);
		int day = (int) (dateKongsbergFormat - year * 10000 - month * 100);
		long timeConverted = 0;
		if (month < 1 || month > 12)
			month = 1;
		if (day < 1 || day > 31)
			day = 1;
		try {
			ZonedDateTime time = ZonedDateTime.of(year, month, day, 0, 0, 0, 0, zoneId);
			timeConverted = time.toInstant().plusMillis(timeSinceMidnight).toEpochMilli();
		} catch (DateTimeException e) {
			XLog.warn("Error while converting date, switch to default value " + e.getMessage(), e);
		}
		return timeConverted;
	}

	/**
	 * Convert a date time from Reson date time format
	 * 
	 * @return the time in millisecond from epochTime
	 */
	public static long convertResonTime(int year, long days, long hours, long minutes, float seconds) {
		ZonedDateTime time = ZonedDateTime.of(year, 1, 1, 0, 0, 0, 0, zoneId);
		time = time.plusDays(days - 1);
		time = time.plusHours(hours);
		time = time.plusMinutes(minutes);
		long nanos = DateUtils.secondToNano(seconds);
		time = time.plusNanos(nanos);
		long epochMilli = time.toInstant().toEpochMilli();
		// round the date with nanosecond (toEpochMilli() returns floor milliseconds)
		epochMilli += Math.round((nanos % 1_000_000) / 1_000_000f);
		return epochMilli;
	}
	
	/**
	 * @return the time in nanoseconds from epochTime
	 */
	public static long convertResonTimeInNano(int year, long days, long hours, long minutes, float seconds) {
		ZonedDateTime time = ZonedDateTime.of(year, 1, 1, 0, 0, 0, 0, zoneId);
		time = time.plusDays(days - 1);
		time = time.plusHours(hours);
		time = time.plusMinutes(minutes);
		long nanos = DateUtils.secondToNano(seconds);
		time = time.plusNanos(nanos);
		return time.toInstant().getEpochSecond() * 1_000_000_000 + time.getNano();
	}

	/**
	 * convert a date from a long value
	 * 
	 * @param time the time from Epoch (see {@link Instant#ofEpochMilli(long)} )
	 * @return a {@link ZonedDateTime}
	 */
	public static ZonedDateTime fromLong(long time) {
		return Instant.ofEpochMilli(time).atZone(zoneId);
	}
}
