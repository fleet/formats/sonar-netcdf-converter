package fr.ifremer.globe.api.xsf.converter.common.utils.values.d2;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCVariable;

/***
 * class allocating storage for 2D matrix, and giving a interface allowing to fill the data with attitude values
 */
public abstract class ValueD2WithAttitude {
	protected static final Logger logger = LoggerFactory.getLogger(ValueD2WithAttitude.class);
	protected NCVariable variable;
	protected int dim;

	/***
	 * Constructor, allocate netcdf buffer for one variable and one ping
	 */
	public ValueD2WithAttitude(NCVariable variable) {
		this.variable = variable;
		this.dim = (int) variable.getShape().get(1).getLength();
	}

	/**
	 * Call the lambda to fill in the internal buffer
	 */
	public abstract void fill(BaseDatagramBuffer buffer, int beamIndexSource, int beamIndexDest, double roll, double pitch, double heave);

	/**
	 * Write the internal buffer into the netCDF file.
	 * 
	 * @param dataFile
	 * @param origin
	 * @throws IOException
	 * @throws InvalidRangeException
	 */
	public abstract void write(long[] origin, long[] count) throws NCException;
	
	/**
	 * Resets data array.
	 */
	public abstract void clear();
}
