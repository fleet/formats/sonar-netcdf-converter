package fr.ifremer.globe.api.xsf.converter.s7k.xsf;

import java.io.IOException;

import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.VendorSpecificGrp;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K7001;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K7001.DeviceInformation;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KFile;
import fr.ifremer.globe.netcdf.ucar.NCException;

public class S7K7001Converter implements IDatagramConverter<S7KFile, XsfFromS7k> {

	private VendorSpecificGrp platformVendorGrp;

	@Override
	public void declare(S7KFile s7kFile, XsfFromS7k xsf) throws NCException {
		platformVendorGrp = xsf.getPlatformVendorGrp();
	}

	@Override
	public void fillData(S7KFile s7kFile) throws IOException, NCException {
		DatagramBuffer buffer = new DatagramBuffer();
		DatagramPosition pos = s7kFile.get7001Metadata().getEntries().iterator().next().getValue();
		DatagramReader.readDatagram(pos.getFile(), buffer, pos.getSeek());

		DeviceInformation[] dis = S7K7001.getDeviceInformations(buffer);
		for (int i = 0; i < dis.length; i++) {
			platformVendorGrp.addAttribute(String.format("device_%d_description", i), dis[i].description);
			platformVendorGrp.addAttribute(String.format("device_%d_infos", i), dis[i].info);
		}
	}

}
