package fr.ifremer.globe.api.xsf.converter.kmall.datagram;

import java.nio.ByteBuffer;

import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.IIPMetadata;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.KmallFile;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;

public class IIP extends KmAllBaseDatagram {


    @Override
    public IIPMetadata getSpecificMetadata(KmallFile metadata) {
        return metadata.getIIPMetadata();
    }

    
    @Override
    public void computeSpecificMetadata(KmallFile metadata, ByteBuffer datagram, String datagramType,
            DatagramPosition position) {
        getSpecificMetadata(metadata).setInstallationTxt(getInstallTxt(datagram), getSystemId(datagram).getU());
    }

    public static String getInstallTxt(ByteBuffer buffer) {
        final long fullDgSize = TypeDecoder.read4U(buffer, buffer.limit() - 4).getU();
        final int rawDataLen = (int) (fullDgSize - 30);
        
        final int pos = buffer.position();
        byte[] ret = new byte[rawDataLen];
        buffer.position(22);
        buffer.get(ret, 0, rawDataLen);
        buffer.position(pos);
        
        return new String(ret).trim();
    }
}
