package fr.ifremer.globe.api.xsf.converter.kmall.datagram;

import java.nio.ByteBuffer;

import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.KmallFile;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.SCLMetadata;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.api.xsf.util.types.SInt;

public class SCL extends SCommon {

    @Override
    public SCLMetadata getSpecificMetadata(KmallFile metadata) {
        return metadata.getSCLMetadata();
    }
    
    @Override
    public void computeSpecificMetadata(KmallFile metadata, ByteBuffer datagram, String datagramType,
            DatagramPosition position) {
        getSpecificMetadata(metadata).addDatagram(position);
    }    

    public static FFloat getOffset_sec(ByteBuffer buffer) {
        return TypeDecoder.read4F(buffer, 24);
	}
	public static SInt getClockDevPU_nanosec(ByteBuffer buffer) {
	        return TypeDecoder.read4S(buffer, 28);
	}

    public static String getDataFromSensor(ByteBuffer datagram) {
        final long fullDgSize = TypeDecoder.read4U(datagram, datagram.limit() - 4).getU();
        final int dataFromSensorOffset = 32;
        final int rawDataLen = (int) (fullDgSize - 8 - dataFromSensorOffset);
        
		final int pos = datagram.position();
        byte[] ret = new byte[rawDataLen];
        datagram.position(dataFromSensorOffset);
        datagram.get(ret, 0, rawDataLen);
        datagram.position(pos);
        
        return new String(ret);
    }
}
