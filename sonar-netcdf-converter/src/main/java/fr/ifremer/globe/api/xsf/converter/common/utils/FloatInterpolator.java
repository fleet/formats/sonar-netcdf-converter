package fr.ifremer.globe.api.xsf.converter.common.utils;

import java.nio.channels.NoConnectionPendingException;
import java.util.*;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCVariable;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.exception.GIOException;

public class FloatInterpolator {
	/**
	 * Map : key = time (long) , value = reference value used to compute interpolation.
	 */
	private final TreeMap<Long, Float> refMap = new TreeMap<>(Long::compareUnsigned);

	/**
	 * Creates and initializes an interpolator.
	 */
	public FloatInterpolator(Map<Long, Float> initValues) {
		if (initValues.isEmpty()) {
			throw new IllegalArgumentException("FloatInterpolator can not be initialized with empty map.");
		}
		refMap.putAll(initValues);
	}

	/**
	 * Tries to build a {@link FloatInterpolator} from NetCDF variables.
	 */
	public static Optional<FloatInterpolator> build(NCVariable refVariable, NCVariable refTime) throws NCException {
		var validValuesByTime = new HashMap<Long, Float>();
		NCDimension refDims = refVariable.getShape().get(0);
		long[] refTimeArray = refTime.get_ulong(new long[] { 0 }, new long[] { refDims.getLength() });
		float[] refValueArray = refVariable.get_float(new long[] { 0 }, new long[] { refDims.getLength() });
		// Keep only valid values.
		for (int i = 0; i < refTimeArray.length; i++) {
			if (Float.isFinite(refValueArray[i]))
				validValuesByTime.put(refTimeArray[i], refValueArray[i]);
		}
		return !validValuesByTime.isEmpty() ? Optional.of(new FloatInterpolator(validValuesByTime)) : Optional.empty();
	}

	/**
	 * Fills a variable with the interpolation of a reference variable.
	 */
	public static void interpolate(NCVariable refVariable, NCVariable refTime, NCVariable outVariable,
			NCVariable outTime) throws NCException {

		FloatInterpolator it = build(refVariable, refTime).orElseThrow(() -> new NCException(
				"Invalid reference NetCDF variable to compute interpolation : " + refVariable.getName()));

		// Get interpolated values
		NCDimension outDims = outVariable.getShape().get(0);
		long[] outTimeArray = outTime.get_ulong(new long[] { 0 }, new long[] { outDims.getLength() });

		float[] outValues = new float[outTimeArray.length];
		for (int i = 0; i < outTimeArray.length; i++) {
			long currentOutTime = outTimeArray[i];
			outValues[i] = it.floatInterpolate(currentOutTime);
		}

		// Write result in output variable
		outVariable.put(new long[] { 0 }, new long[] { outDims.getLength() }, outValues);
	}

	/**
	 * @return elements number of internal ref map
	 */
	public int getSize() {
		return refMap.size();
	}

	/**
	 * Interpolate the values at the given time
	 */
	public float floatInterpolate(long time) {
		// Check if refMap is valid
		if (refMap.isEmpty())
			return Float.NaN;

		Long timeBefore = refMap.floorKey(time);
		Long timeAfter = refMap.ceilingKey(time);

		double coeff = (timeBefore != null && timeAfter != null && !timeAfter.equals(timeBefore)) ?
				((double) (time - timeBefore)) / (timeAfter - timeBefore) : 0;
		float valueBefore = timeBefore != null ? refMap.get(timeBefore) : refMap.get(timeAfter);
		float valueAfter = timeAfter != null ? refMap.get(timeAfter) : valueBefore;

		return (float) (valueBefore + coeff * (valueAfter - valueBefore));
	}

	/**
	 * Interpolate the values at the given time
	 */
	public float angleInterpolate(long time) {
		Long timeBefore = refMap.floorKey(time);
		Long timeAfter = refMap.ceilingKey(time);

		double coeff = (timeBefore != null && timeAfter != null && !timeAfter.equals(timeBefore)) ?
				((double) (time - timeBefore)) / (timeAfter - timeBefore) : 0;
		float valueBefore = timeBefore != null ? refMap.get(timeBefore) : refMap.get(timeAfter);
		float valueAfter = timeAfter != null ? refMap.get(timeAfter) : valueBefore;
		if (valueBefore < valueAfter && (valueAfter - valueBefore) > 180.f) {
			valueBefore += 360.f;
		} else if (valueBefore > valueAfter && (valueBefore - valueAfter) > 180.f) {
			valueAfter += 360.f;
		}

		return (float) (valueBefore + coeff * (valueAfter - valueBefore));
	}

}
