package fr.ifremer.globe.api.xsf.converter.s7k.datagram;

import static java.lang.Math.toIntExact;

import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7K7001Metadata;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KFile;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.UInt;
import fr.ifremer.globe.api.xsf.util.types.ULong;

public class S7K7001 extends S7KBaseDatagram {

	@Override
	public void computeSpecificMetadata(S7KFile metadata, DatagramBuffer buffer, long type,
			DatagramPosition position, long time) {
		getSpecificMetadata(metadata).setEntry(getDeviceIdentifier(buffer).getU(), position);
	}

	@Override
	public S7K7001Metadata getSpecificMetadata(S7KFile metadata) {
		return metadata.get7001Metadata();
	}

	public static class DeviceInformation {
		public UInt uniqueIdentifier;
		public String description;
		public ULong serialNumber;
		public String info;
	}
	
	public static ULong getSonarSerialnumber(DatagramBuffer buffer) {
		return TypeDecoder.read8U(buffer.byteBufferData, 0);
	}
	
	public static UInt getNumberOfDevices(DatagramBuffer buffer) {
		return TypeDecoder.read4U(buffer.byteBufferData, 8);
	}
	
	public static DeviceInformation[] getDeviceInformations(DatagramBuffer buffer) {
		final int numOfDevices = toIntExact(getNumberOfDevices(buffer).getU());
		DeviceInformation[] ret = new DeviceInformation[numOfDevices];
		int deviceOffset = 12;
		for (int i = 0; i < numOfDevices; i++) {
			DeviceInformation di = new DeviceInformation();
			
			di.uniqueIdentifier = TypeDecoder.read4U(buffer.byteBufferData, deviceOffset);
			byte[] raw_descr = new byte[64];
			buffer.byteBufferData.position(deviceOffset + 4);
			buffer.byteBufferData.get(raw_descr);
			buffer.byteBufferData.rewind();
			int validBytes = 0;
			while (validBytes < 64 && raw_descr[validBytes] != 0) {
				validBytes++;
			}
			di.description = new String(raw_descr, 0, validBytes);
			
			di.serialNumber = TypeDecoder.read8U(buffer.byteBufferData, deviceOffset + 68);
			final int infoSize = toIntExact(TypeDecoder.read4U(buffer.byteBufferData, deviceOffset + 76).getU());
			byte[] raw_info = new byte[infoSize];
			buffer.byteBufferData.position(deviceOffset + 80);
			buffer.byteBufferData.get(raw_info);
			buffer.byteBufferData.rewind();
			di.info = new String(raw_info, 0, infoSize - 1 /* terminates with a \0 */);
			
			ret[i] = di;
			deviceOffset += 80 + infoSize;
		}
		return ret;
	}
}
