package fr.ifremer.globe.api.xsf.converter.all.xsf.sensors;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.all.datagram.heading.Heading;
import fr.ifremer.globe.api.xsf.converter.all.datagram.heading.HeadingMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2F4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2U1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2U2;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2U8;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfFromKongsberg;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.HeadingGrp;
import fr.ifremer.globe.api.xsf.util.SimpleIdentifier;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.api.xsf.util.types.ULong;
import fr.ifremer.globe.api.xsf.util.types.UShort;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.date.DateUtils;

public class HeadingGroup implements IDatagramConverter<AllFile, XsfFromKongsberg> {

	private AllFile metadata;
	private HeadingMetadata sensorMetadata;
	private List<ValueD2> values;

	public HeadingGroup(AllFile metadata) {
		this.metadata = metadata;
		this.sensorMetadata = metadata.getHeading();
		this.values = new LinkedList<>();
	}

	@Override
	public void declare(AllFile allFile, XsfFromKongsberg xsf) throws NCException {

		HeadingGrp headingGroup = new HeadingGrp(xsf.getEnvironmentVendor(), allFile,new HashMap<String, Integer>());

		// declare all the variables
		values.add(
				new ValueD2U2(headingGroup.getSystem_serial_number(), sensorMetadata.getMaxEntriesPerDatagram(), (buffer, i) -> {
					return Heading.getSerialNumber(buffer.byteBufferData);
				}));

		values.add(new ValueD2U8(headingGroup.getDatagram_time(), sensorMetadata.getMaxEntriesPerDatagram(),
				(buffer, i) -> new ULong(Heading.getXSFEpochTime(buffer.byteBufferData))));

		values.add(new ValueD2U2(headingGroup.getRaw_count(), sensorMetadata.getMaxEntriesPerDatagram(), (buffer, i) -> {
			return Heading.getCounter(buffer.byteBufferData);
		}));

		values.add(new ValueD2U8(headingGroup.getTime(), sensorMetadata.getMaxEntriesPerDatagram(), (buffer, i) -> {
			return new ULong(DateUtils.milliSecondToNano(Heading.getEpochTime(buffer.byteBufferData)
					+ Heading.getTimeSinceRecordStart(buffer.byteBufferData, i).getU()));
		}));

		values.add(new ValueD2F4(headingGroup.getHeading(), sensorMetadata.getMaxEntriesPerDatagram(), (buffer, i) -> {
			return new FFloat(Heading.getHeading(buffer.byteBufferData, i).getU() * 0.01f);
		}));

		values.add(new ValueD2U1(headingGroup.getHeading_indicator(), sensorMetadata.getMaxEntriesPerDatagram(), (buffer, i) -> {
			return Heading.getHeadingIndicator(buffer.byteBufferData);
		}));
	}

	@Override
	public void fillData(AllFile allFile) throws IOException, NCException {
		long[] count = new long[] { 0 };
		long[] origin = new long[] { 0 };

		DatagramBuffer buffer = new DatagramBuffer(metadata.getByteOrder());
		for (Entry<SimpleIdentifier, DatagramPosition> posentry : sensorMetadata.getDatagrams()) {
			values.forEach(ValueD2::clear);
			final DatagramPosition pos = posentry.getValue();
			DatagramReader.readDatagramAt(pos.getFile(), buffer, pos.getSeek());
			long nEntries = Heading.getNumberEntries(buffer.byteBufferData).getU();

			// read the data frem the source file
			for (int i = 0; i < nEntries; i++) {
				for (ValueD2 value : this.values) {
					value.fill(buffer, i, i);
				}
			}

			// flush into the output file
			count[0] = nEntries;
			for (ValueD2 value : this.values) {
				value.write(origin, count);
			}
			origin[0] += nEntries;
		}
	}

}
