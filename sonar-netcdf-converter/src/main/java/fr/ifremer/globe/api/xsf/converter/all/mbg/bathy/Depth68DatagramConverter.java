package fr.ifremer.globe.api.xsf.converter.all.mbg.bathy;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.depth68.Depth68;
import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.AbstractCompleteAntennaPingSingleMetadata;
import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.AbstractCompletePingMetadata;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgBase;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgConstants;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgConstants.SoundingValidity;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgUtils;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2S1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2S1WithHScale;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2S2;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2S4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2S4WithPosition;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2U2;
import fr.ifremer.globe.api.xsf.util.types.SByte;
import fr.ifremer.globe.api.xsf.util.types.SInt;
import fr.ifremer.globe.api.xsf.util.types.SShort;
import fr.ifremer.globe.api.xsf.util.types.UShort;
import fr.ifremer.globe.netcdf.api.convention.CFStandardNames;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCVariable;

public class Depth68DatagramConverter extends DepthDatagramConverter {

	private static final double SAMPLING_RATE_MIN = 300;
	private static final double SAMPLING_RATE_MAX = 30000;

	/** Constructor **/
	public Depth68DatagramConverter(AllFile stats) {
		super(stats.getDepth68());
	}

	@Override
	public void declare(AllFile allFile, MbgBase mbg) throws NCException {
		super.declare(allFile, mbg);

		/** Across angle **/
		NCVariable acrossBeamAngleVariable = mbg.getVariable(MbgConstants.AcrossAngle);
		double acrossBeamAngleFactor = acrossBeamAngleVariable.getAttributeDouble(CFStandardNames.CF_SCALE_FACTOR);
		double acrossBeamAngleOffset = acrossBeamAngleVariable.getAttributeDouble(CFStandardNames.CF_ADD_OFFSET);
		swathBeamValues.add(new ValueD2S2(acrossBeamAngleVariable, (buffer, iBeam) -> {
			float acrossDistance = Depth68.getAcrossDistance(buffer.byteBufferData, iBeam).get();
			double angle = Depth68.getBeamDepressionAngle(buffer.byteBufferData, iBeam).get() / 100.0;

			if (acrossDistance > 0) {
				angle = 90. - angle;
			} else if (acrossDistance < 0) {
				angle = angle - 90;
			} else {
				angle = 0;
			}

			return new SShort((short) Math.round((angle - acrossBeamAngleOffset) / acrossBeamAngleFactor));
		}));

		/** Along angle **/
		NCVariable azimuthBeamAngleVariable = mbg.getVariable(MbgConstants.AlongAngle);
		double azimuthBeamAngleFactor = azimuthBeamAngleVariable.getAttributeDouble(CFStandardNames.CF_SCALE_FACTOR);
		double azimuthBeamAngleOffset = azimuthBeamAngleVariable.getAttributeDouble(CFStandardNames.CF_ADD_OFFSET);
		swathBeamValues.add(new ValueD2S2(azimuthBeamAngleVariable, (buffer, iBeam) -> {
			float angle = Depth68.getBeamAzimuthAngle(buffer.byteBufferData, iBeam).getU() / 100.0f;
			return new SShort((short) Math.round((angle - azimuthBeamAngleOffset) / azimuthBeamAngleFactor));
		}));

		/** Depth **/
		NCVariable depthVariable = mbg.getVariable(MbgConstants.Depth);
		double depthFactor = depthVariable.getAttributeDouble(CFStandardNames.CF_SCALE_FACTOR);
		double depthOffset = depthVariable.getAttributeDouble(CFStandardNames.CF_ADD_OFFSET);
		swathBeamValuesWithPosition
				.add(new ValueD2S4WithPosition(depthVariable, (buffer, iBeam, lat, lon, speed, positionMetadata) -> {
					int modelNumber = Depth68.getModelNumber(buffer.byteBufferData);
					boolean unsignedDepth = modelNumber == 120 || modelNumber == 300;
					float zResolution = Depth68.getZResolution(buffer.byteBufferData).getInt() / 100.0f;
					float depth = unsignedDepth ? Depth68.getDepth2U(buffer.byteBufferData, iBeam).getU()
							: Depth68.getDepth2S(buffer.byteBufferData, iBeam).get();
					depth *= zResolution;

					if (depth > 0) // getDectectionValidity(buffer, iBeam) != SoundingValidity.MISSING)
						depth += Depth68.getTransmitTransducerDepth(buffer.byteBufferData).getU() / 100.0f;

					// computes geographic bounding box with valid detection
					if (depth > 0 && depth < Integer.MAX_VALUE) {
						double[] detectionLatLon = getDetectionPosition(buffer, iBeam, lat, lon);
						allFile.getDetectionBB().computeBoundingBoxLat(detectionLatLon[0]);
						allFile.getDetectionBB().computeBoundingBoxLon(detectionLatLon[1]);
					}

					return (int) Math.round(((depth) - depthOffset) / depthFactor);
				}));

		/** Heading **/
		NCVariable headingVariable = mbg.getVariable(MbgConstants.Heading);
		double headingScaleFactor = headingVariable.getAttributeDouble(CFStandardNames.CF_SCALE_FACTOR);
		double headingOffset = headingVariable.getAttributeDouble(CFStandardNames.CF_ADD_OFFSET);
		swathAntennaValues.add(new ValueD2S2(headingVariable, (buffer, beamIndexSource) -> //
		new SShort((short) ((Depth68.getHeadingOfVessel(buffer.byteBufferData).getU() / 100. - headingOffset)
				/ headingScaleFactor))));

		/** Immersion **/
		NCVariable refDepthVariable = mbg.getVariable(MbgConstants.Immersion);
		double refDepthFactor = refDepthVariable.getAttributeDouble(CFStandardNames.CF_SCALE_FACTOR);
		double refDepthOffset = refDepthVariable.getAttributeDouble(CFStandardNames.CF_ADD_OFFSET);
		swathAntennaValues.add(new ValueD2S4(refDepthVariable,
				(buffer, iBeam) -> new SInt((int) Math.round(
						(Depth68.getTransmitTransducerDepth(buffer.byteBufferData).getU() / 100.0 - refDepthOffset)
								/ refDepthFactor))));

		/** Quality **/
		NCVariable qualityVariable = mbg.getVariable(MbgConstants.SQuality);
		int qualityFactor = qualityVariable.getAttributeInt(CFStandardNames.CF_SCALE_FACTOR);
		int qualityOffset = qualityVariable.getAttributeInt(CFStandardNames.CF_ADD_OFFSET);
		swathBeamValues.add(new ValueD2S1(qualityVariable, (buffer, iBeam) -> //
		new SByte((byte) ((Depth68.getQualityFactor(buffer.byteBufferData, iBeam).getU() - qualityOffset)
				/ qualityFactor))));

		/** Range **/
		NCVariable rangeVariable = mbg.getVariable(MbgConstants.Range);
		double rangeFactor = rangeVariable.getAttributeDouble(CFStandardNames.CF_SCALE_FACTOR);
		swathBeamValues.add(new ValueD2S4(rangeVariable, (buffer, i) -> {
			float samplingFreq = Depth68.getSamplingFrequency(buffer.byteBufferData).getU();
			float rangeScale = SAMPLING_RATE_MIN <= samplingFreq && samplingFreq <= SAMPLING_RATE_MAX
					? 0.25f / samplingFreq
					: 0;
			float range = Depth68.getRange(buffer.byteBufferData, i).getU() * rangeScale;
			return new SInt((int) (range * (1 / rangeFactor)));
		}));

		/** Reflectivity **/
		NCVariable reflectivityVariable = mbg.getVariable(MbgConstants.Reflectivity);
		double reflectivityFactor = reflectivityVariable.getAttributeDouble(CFStandardNames.CF_SCALE_FACTOR);
		double reflectivityOffset = reflectivityVariable.getAttributeDouble(CFStandardNames.CF_ADD_OFFSET);
		swathBeamValues.add(new ValueD2S1(reflectivityVariable, (buffer, iBeam) -> {
			double reflectivity = Depth68.getReflectivity(buffer.byteBufferData, iBeam).get() / 2.0;
			return new SByte((byte) ((reflectivity - reflectivityOffset) / reflectivityFactor));
		}));

		/** Sampling rate **/
		NCVariable sampRateVariable = mbg.getVariable(MbgConstants.SamplingRate);
		int sampRateFactor = sampRateVariable.getAttributeInt(CFStandardNames.CF_SCALE_FACTOR);
		int sampRateOffset = sampRateVariable.getAttributeInt(CFStandardNames.CF_ADD_OFFSET);
		swathAntennaValues.add(new ValueD2U2(sampRateVariable, (buffer, iBeam) -> //
		new UShort((short) ((Depth68.getSamplingFrequency(buffer.byteBufferData).getU() - sampRateOffset)
				/ sampRateFactor))));

		/** SLengthOfDetection **/
		NCVariable windowLengthVariable = mbg.getVariable(MbgConstants.SLengthOfDetection);
		int windowLengthFactor = windowLengthVariable.getAttributeInt(CFStandardNames.CF_SCALE_FACTOR);
		int windowLengthOffset = windowLengthVariable.getAttributeInt(CFStandardNames.CF_ADD_OFFSET);
		swathBeamValues.add(new ValueD2S1(windowLengthVariable, (buffer, iBeam) -> new SByte(
				(byte) ((Depth68.getLengthOfDetectionWindow(buffer.byteBufferData, iBeam).getU() - windowLengthOffset)
						/ windowLengthFactor))));

		/** Sound velocity **/
		NCVariable soundVelVariable = mbg.getVariable(MbgConstants.SoundVelocity);
		double soundVelFactor = soundVelVariable.getAttributeDouble(CFStandardNames.CF_SCALE_FACTOR);
		double soundVelOffset = soundVelVariable.getAttributeDouble(CFStandardNames.CF_ADD_OFFSET);
		swathAntennaValues.add(new ValueD2U2(soundVelVariable, (buffer, iBeam) -> {
			int soudSpeed = Depth68.getSoundSpeedAtTx(buffer.byteBufferData).getU();
			return new UShort((short) Math.round((soudSpeed / 10.0 - soundVelOffset) / soundVelFactor));
		}));

		/** Distance scale **/
		NCVariable distanceScaleVariable = mbg.getVariable(MbgConstants.DistanceScale);
		double scaleFactor = distanceScaleVariable.getAttributeDouble(CFStandardNames.CF_SCALE_FACTOR);
		swathAntennaValuesWithHScale.add(new ValueD2S1WithHScale(distanceScaleVariable,
				(buffer, iBeam, hScale) -> new SByte((byte) (hScale / scaleFactor))));

		/** Vertical depth **/
		NCVariable verticalDepthVariable = mbg.getVariable(MbgConstants.VerticalDepth);
		double vertDepthScaleFactor = verticalDepthVariable.getAttributeDouble(CFStandardNames.CF_SCALE_FACTOR);
		swathAntennaValues.add(new ValueD2S4(verticalDepthVariable, (buffer, antennaIndex) -> {
			final int receivedBeamCount = Depth68.getValidDetectionCount(buffer.byteBufferData).getU();
			if (receivedBeamCount == 0)
				return new SInt(0);// if no valid beam -> return 0

			// get the beam with the minimum across distance
			float minAcrossDistance = Float.MAX_VALUE;
			int verticalBeamIndex = 0;
			for (int i = 0; i < receivedBeamCount; i++) {
				float acrossDistance = Math.abs(Depth68.getAcrossDistance(buffer.byteBufferData, i).get());
				if (acrossDistance < minAcrossDistance) {
					verticalBeamIndex = i;
					minAcrossDistance = acrossDistance;
				}
			}

			// get depth
			int modelNumber = Depth68.getModelNumber(buffer.byteBufferData);
			boolean unsignedDepth = modelNumber == 120 || modelNumber == 300;
			float verticalDepth = unsignedDepth ? Depth68.getDepth2U(buffer.byteBufferData, verticalBeamIndex).getU()
					: Depth68.getDepth2S(buffer.byteBufferData, verticalBeamIndex).get();
			verticalDepth *= Depth68.getZResolution(buffer.byteBufferData).getInt() / 100.0f;
			verticalDepth += Depth68.getTransmitTransducerDepth(buffer.byteBufferData).getU() / 100.0f;
			return new SInt((int) Math.round(verticalDepth * (1 / vertDepthScaleFactor)));
		}));

	}

	@Override
	protected SoundingValidity getDectectionValidity(BaseDatagramBuffer buffer, int iBeam) {
		int modelNumber = Depth68.getModelNumber(buffer.byteBufferData);
		boolean unsignedDepth = modelNumber == 120 || modelNumber == 300;
		int depth = unsignedDepth ? Depth68.getDepth2U(buffer.byteBufferData, iBeam).getU()
				: Depth68.getDepth2S(buffer.byteBufferData, iBeam).get();
		depth += Depth68.getTransmitTransducerDepth(buffer.byteBufferData).getU();
		return (depth > 0 && depth < Integer.MAX_VALUE) ? SoundingValidity.VALID : SoundingValidity.MISSING;
	}

	@Override
	protected int getBeamCount(DatagramBuffer buffer) {
		return Depth68.getValidDetectionCount(buffer.byteBufferData).getU();
	}

	/**
	 * For {@link Depth68}, beam index source and destination are different.
	 */
	@Override
	protected int computeBeamIndexDestination(AllFile allFile,
			AbstractCompletePingMetadata<AbstractCompleteAntennaPingSingleMetadata> ping, DatagramBuffer buffer,
			int beamIndexSource) {
		return Depth68.getBeamNumber(buffer.byteBufferData, beamIndexSource).getU() - 1;
	}

	@Override
	protected double computeHorizontaleScale(DatagramBuffer buffer) {
		int validBeams = Depth68.getValidDetectionCount(buffer.byteBufferData).getU();
		float max = Float.MIN_VALUE;
		float resolution = Depth68.getXYResolution(buffer.byteBufferData).getInt() / 100f;
		for (int i = 0; i < validBeams; i++) {
			if (getDectectionValidity(buffer, i) != SoundingValidity.MISSING) {
				float acrossDistance = Math.abs(Depth68.getAcrossDistance(buffer.byteBufferData, i).get() * resolution);
				float alongDistance = Math.abs(Depth68.getAlongDistance(buffer.byteBufferData, i).get() * resolution);
				if (acrossDistance > max)
					max = acrossDistance;
				if (alongDistance > max)
					max = alongDistance;
			}
		}
		return MbgUtils.getHorizontalScale(max);
	}

	@Override
	protected double getAlongDistance(BaseDatagramBuffer buffer, int iBeam) {
		float resolution = Depth68.getXYResolution(buffer.byteBufferData).getU() / 100f;
		return Depth68.getAlongDistance(buffer.byteBufferData, iBeam).get() * resolution;
	}

	@Override
	protected double getAcrossDistance(BaseDatagramBuffer buffer, int ibeam) {
		float resolution = Depth68.getXYResolution(buffer.byteBufferData).getU() / 100f;
		return Depth68.getAcrossDistance(buffer.byteBufferData, ibeam).get() * resolution;
	}

	@Override
	protected double getVesselHeading(BaseDatagramBuffer buffer) {
		return Depth68.getHeadingOfVessel(buffer.byteBufferData).getU() / 100.;
	}

}
