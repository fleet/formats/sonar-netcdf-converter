package fr.ifremer.globe.api.xsf.converter.common.utils;

public class Namer {
	public static String getAttitudeSubGroupName(int sensorId)
	{
		return String.format("%03d", sensorId);
	}
	public static String getAttitudeSubGroupName(long sensorId)
	{
		return String.format("%03d", sensorId);
	}

	public static String getPositionSubGroupName(int sensorId)
	{
		return String.format("%03d", sensorId);
	}
}
