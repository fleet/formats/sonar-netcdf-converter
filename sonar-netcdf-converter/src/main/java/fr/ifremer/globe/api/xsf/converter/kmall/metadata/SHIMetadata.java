package fr.ifremer.globe.api.xsf.converter.kmall.metadata;


import fr.ifremer.globe.api.xsf.converter.all.datagram.IndividualSensorMetadata;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;

public class SHIMetadata extends IndividualSensorMetadata {
    
    /**
     * Register a new datagrams. Since datagrams do not have sequencial numbers,
     * they are assumed to come in sequencial order.
     */
    public void addDatagram(DatagramPosition pos) {
        super.addDatagram(pos, this.datagramCount - 1, 1);
    }

}
