package fr.ifremer.globe.api.xsf.converter.all.tools;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramParser;
import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramReader;

/**
 * Simple tool used to completry decompose a file. This machinery is
 * used to generate unit tests.
 *
 */
public class ALLDecomposer {
    
    static String file = "E:\\Temp\\testKmall2Hsf\\ALLFiles\\0008_20150611_095724_SE_ext_det.all";
    static String dest = "E:\\Temp\\testKmall2Hsf\\ShortnerFiles";
    
    public static void main(String[] args) throws FileNotFoundException, IOException{
        try (RandomAccessFile raf = new RandomAccessFile(file, "r")) {
            
            final DatagramBuffer buffer = new DatagramBuffer(DatagramReader.getByteOrder(raf));
            final DatagramParser parser = new DatagramParser();
            final File outFile = new File(dest);
            int dgmCount = 0;
            if (outFile.exists()) {
                outFile.delete();
            }
            outFile.mkdirs();
            
            while (DatagramReader.readDatagram(raf, buffer) > 0) {
                final File byTypeFolder = new File(outFile, String.format("0x%2x", parser.getDatagramType(buffer.byteBufferData)));
                if (!byTypeFolder.exists()) {
                    byTypeFolder.mkdir();
                }
                final File dgmFile = new File(byTypeFolder, String.format("%05d.dgm", dgmCount++));
                FileOutputStream binOut = new FileOutputStream(dgmFile);
                binOut.write(buffer.rawBufferHeader);
                binOut.write(buffer.rawBufferData, 0, buffer.byteBufferData.limit());
                binOut.close();
            }
        }
    }

}
