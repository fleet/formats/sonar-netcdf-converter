package fr.ifremer.globe.api.xsf.converter.kmall.tools;

enum KmallMode {
	Very_shallow(0), Shallow(1), Medium(2), Deep(3), Deeper(4), Very_deep(5), Extra_deep(6), Extreme_deep(7);

	private final int mode;

	KmallMode(int i) {
		mode = i;
	}

	public int getMode() {
		return mode;
	}
}

/***
 * Some magical ugly code to deduce the effective pulse length values
 * 
 * @author Cyrille
 *
 */
public class KMPulseLenghtEffectiveComputer {

	public static float compute(int sounderModelNumber, float totalPulseLength) {

		if (sounderModelNumber == 2040) {
			return getEM2040EffectivePulseLength(totalPulseLength);
		}
		if (sounderModelNumber == 850) {
			return getME70EffectivePulseLength(totalPulseLength);
		}
		if (sounderModelNumber == 304) {
			return Float.NaN; //need to be reworked and validated with SIS software version numbers and so on
			//return getDefaultEffectivePulseLength(totalPulseLength);
		}
		return getDefaultEffectivePulseLength(totalPulseLength);

	}

	public static float getDefaultEffectivePulseLength(float totalPulseLength) {
		return totalPulseLength * 0.375f; // magic extracted from sonarscope

		// SignalLengthEffective = SignalLengthTotal * 0.375; % Mail kjell.echholt.nilsen@km.kongsberg.com du 28/09/2017
		//
		// % Temporaire pour test Naig et Laurent
		// % SignalLengthEffective = SignalLengthTotal * 0.477; % Mail kjell.echholt.nilsen@km.kongsberg.com du
		// 28/09/2017
		// % Temporaire pour test Naig et Laurent

	}

	public static float getEM2040EffectivePulseLength(float totalPulseLength) {
		return totalPulseLength * 0.477f; // magic extracted from sonarscope
	}

	public static float getME70EffectivePulseLength(float totalPulseLength) {
		// extrait sonarscope
		// SignalLengthEffective = SignalLengthTotal * (0.7^2);
		// if DataSonarInstallationParameters.EmModel == 850 % ME70
		// PulseLength_UsedByKMinIA = SignalLengthTotal * 0.7;
		// SignalLengthEffective = SignalLengthTotal * (0.7^2);
		// else
		// % if isfield(DataSonarInstallationParameters, 'OSV') && (versionSIS(DataSonarInstallationParameters.OSV) <
		// 4.32)
		// if isfield(DataSonarInstallationParameters, 'PSV') && (versionSIS(DataSonarInstallationParameters.PSV(1:5)) <
		// 2.22) % Modif JMA le 08/01/2019 sous couvert Ridha et Laurent
		// PulseLength_UsedByKMinIA = SignalLengthNominal;
		//
		// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		// % PulseLength_UsedByKMinIA(:) = 0.2; % Test EM3002
		// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		//
		// else
		// PulseLength_UsedByKMinIA = SignalLengthEffective;
		// end
		// end
		return (float) (totalPulseLength * (0.7 * 0.7));
	}

	public static float getEM304EffectivePulseLength(KmallMode mode, boolean cw, boolean singleSwath,
			float totalPulseLength) {
		switch (mode) {
		case Shallow:

		case Very_shallow:
			if (singleSwath)
				return 0.6f;
			else
				return 1.1f;
		case Medium:
			return 1.4f;

		case Deep:
			if (singleSwath)
				return 3.3f;
			else
				return 3.6f;

		case Deeper:
			if (singleSwath)

			{
				if (cw)
					return 3.8f;
				return 3.9f;
			} else {
				if (cw)
					return 4f;
				return 4.6f;

			}

		case Very_deep:
			if (singleSwath && !cw)
				return 5.3f;
			return 3.4f;

		case Extra_deep:
			if (singleSwath && !cw)
				return 5.3f;
			return 8.6f;

		case Extreme_deep:
			if (singleSwath && !cw)
				return 5.3f;
			return 8.6f;
		default:
			break;

		}
		return getDefaultEffectivePulseLength(totalPulseLength);
	}
}
