package fr.ifremer.globe.api.xsf.converter.common.utils;

/**
 * Specialized calculator ignoring NaN values
 * */
public class FloatStatCalculator extends MinMaxCalculator<Float>{
	public FloatStatCalculator() {
		super(Float.NaN);
	}
	/**
	 * 
	 * */
	@Override
	public void register(Float value)
	{
		if(!value.isNaN())
			super.register(value);
	}
}
