package fr.ifremer.globe.api.xsf.converter.kmall.xsf;

import fr.ifremer.globe.api.xsf.converter.XsfConverterParameters;
import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.utils.processing.VariableComputer;
import fr.ifremer.globe.api.xsf.converter.common.xsf.TransverseGroup;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfFromKongsberg;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfWriter;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.BeamGroup1VendorSpecificGrp;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.KmallFile;
import fr.ifremer.globe.api.xsf.converter.kmall.tools.KMPulseLenghtEffectiveComputer;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCVariable;
import org.slf4j.Logger;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * {@link XsfWriter} for .kmall files
 */
public class KmallXsfWriter extends XsfWriter<KmallFile, XsfFromKongsberg> {

    @Override
    public XsfFromKongsberg createOutputNcFile(KmallFile allFile, XsfConverterParameters params)
            throws NCException, IOException {
        return new XsfFromKongsberg(allFile, params);
    }

    @Override
    public List<IDatagramConverter<KmallFile, XsfFromKongsberg>> getGroupConverters(KmallFile kmallFile) {
        List<IDatagramConverter<KmallFile, XsfFromKongsberg>> dataGroups = new LinkedList<>();

        // Common groups
        dataGroups.add(new TransverseGroup<KmallFile>(kmallFile.getSwathIds()));

        if (kmallFile.getIIPMetadata().datagramCount > 0) {
            dataGroups.add(new IIPConverter());
        }

        if (kmallFile.getIOPMetadata().datagramCount > 0) {
            dataGroups.add(new IOPConverter(kmallFile));
        }

        if (kmallFile.getMWCMetadata().datagramCount > 0) {
            dataGroups.add(new MWCConverter(kmallFile));
        }

        if (kmallFile.getMRZMetadata().datagramCount > 0) {
            dataGroups.add(new MRZConverter(kmallFile));
        }

        if (kmallFile.getSCLMetadata().datagramCount > 0) {
            dataGroups.add(new SCLConverter(kmallFile));
        }

        if (kmallFile.getSPOMetadata().datagramCount > 0) {
            dataGroups.add(new SPOConverter(kmallFile));
        }

        if (kmallFile.getSVPMetadata().datagramCount > 0) {
            dataGroups.add(new SVPConverter(kmallFile));
        }

        if (kmallFile.getSKMMetadata().datagramCount > 0) {
            dataGroups.add(new SKMConverter(kmallFile));
        }

        if (kmallFile.getSHIMetadata().datagramCount > 0) {
            dataGroups.add(new SHIConverter(kmallFile));
        }

        if (kmallFile.getSDEMetadata().datagramCount > 0) {
            dataGroups.add(new SDEConverter(kmallFile));
        }

        return dataGroups;
    }

    @Override
    public void postProcess(KmallFile kmallFile, XsfFromKongsberg xsf, XsfConverterParameters params, Logger logger)
            throws NCException {
        super.postProcess(kmallFile, xsf, params, logger);

        VariableComputer.fillPlatformAttitudeAndPosition(kmallFile, xsf);
        VariableComputer.computeBeamAngle(xsf);
        computeBeamWidths(xsf);
        computeTransmitDurationEquivalent(kmallFile, xsf);
        computeReceiveTransducerIndex(kmallFile, xsf);
        VariableComputer.fillSwathTideDraught(kmallFile, xsf);

        // declare missing variable when WC is missing
        xsf.getBeamGroup().getSample_count();
    }

    /**
     * Computes the transmit duration equivalent for kmall files.
     */
    private void computeReceiveTransducerIndex(KmallFile kmallFile, XsfFromKongsberg xsfBase) throws NCException {
        if (xsfBase.getBeamGroup().getDim_beam() == null) {
            return;
        }
        long swathCount = xsfBase.getBeamGroup().getDim_ping_time().getLength();
        if (swathCount < 1) {
            return;
        }
        BeamGroup1VendorSpecificGrp beamGrpVendor = xsfBase.getBeamGroupVendorSpecific();

        long rxBeamCount = xsfBase.getBeamGroup().getDim_beam().getLength();

        // We need to ensure that reference obtained is filled with valid values

        // find a fully valid reference

        int[] reference = {};
        int first_valid_swath = 0;
        for (int starting_ = 0; starting_ < swathCount; starting_++) {
            {
                long[] start = {starting_, 0};
                long[] count = {1, rxBeamCount};
                reference = beamGrpVendor.getRaw_receive_transducer_index().get_int(start, count);
            }
            boolean allValid = true;
            for (int v : reference) {
                if (v < 0) // we consider invalid values as negatives values
                {
                    allValid = false;
                    break;
                }
            }
            if (allValid) {
                first_valid_swath = starting_;
            }
        }

        if (first_valid_swath < swathCount - 1) {
            boolean does_rx_transducer_index_change = false;
            for (long swath = first_valid_swath + 1; swath < swathCount; swath++) {
                long[] start = {swath, 0};
                long[] count = {1, rxBeamCount};
                int[] index = beamGrpVendor.getRaw_receive_transducer_index().get_int(start, count);

                // compare each index with reference to ensure they are always equals
                for (int l = 0; l < rxBeamCount; l++) {
                    if (reference[l] != index[l] && reference[l] > 0 && index[l] > 0/** ignore invalid values */
                    ) {
                        does_rx_transducer_index_change = true;
                        break;
                    }
                }
                if (does_rx_transducer_index_change) {
                    break;
                }
            }
            if (does_rx_transducer_index_change) {
                throw new AssertionError(
                        "Beam associated Rx transducer index changed over ping time but it is expected to be constant. Unsupported case in data format");
            }

        } // else we retain the last reference read
        // then copy reference
        long[] start = {0};
        long[] count = {rxBeamCount};
        xsfBase.getBeamGroup().getReceive_transducer_index().put(start, count, reference);
    }

    /**
     * Computes the transmit duration equivalent for kmall files.
     */
    private void computeTransmitDurationEquivalent(KmallFile kmallFile, XsfFromKongsberg xsfBase) throws NCException {
        long swathCount = xsfBase.getBeamGroup().getDim_ping_time().getLength();
        NCVariable signalLength = xsfBase.getBeamGroupVendorSpecific().getSignal_length();

        long txBeamCount = xsfBase.getBeamGroup().getDim_tx_beam().getLength();
        float[] value = {Float.NaN};
        for (long swath = 0; swath < swathCount; swath++) {
            for (int beam = 0; beam < txBeamCount; beam++) {
                long[] start = {swath, beam};
                long[] count = {1, 1};
                float[] totalPulseLength = signalLength.get_float(start, count);

                value[0] = KMPulseLenghtEffectiveComputer.compute(kmallFile.getModelNumberCode(), totalPulseLength[0]);
                xsfBase.getBeamGroup().getReceive_duration_effective().put(start, count, value);
            }
        }
    }

    private void computeBeamWidths(XsfFromKongsberg xsfBase) throws NCException {
        if (xsfBase.getBeamGroup().getDim_beam() == null) {
            return;
        }
        BeamGroup1VendorSpecificGrp vendorBeamGrp = xsfBase.getBeamGroupVendorSpecific();
        long swathCount = xsfBase.getBeamGroup().getDim_ping_time().getLength();
        long beamCount = xsfBase.getBeamGroup().getDim_beam().getLength();
        float[] value = {Float.NaN};
        for (long swath = 0; swath < swathCount; swath++) {
            // retrieve beamWidth (per rx antenna value)
            long[] s = {swath, 0L};
            long[] c = {1, vendorBeamGrp.getReceive_array_size_used().getShape().get(1).getLength()};
            float[] rxBeamWidht = vendorBeamGrp.getReceive_array_size_used().get_float(s, c);
            for (int beam = 0; beam < beamCount; beam++) {
                long[] start = {swath, beam};
                long[] count = {1, 1};
                // we only care about the cos of the angle, it is store in direction_z() vector
                float[] diry = xsfBase.getBeamGroup().getRx_beam_rotation_phi().get_float(start, count);
                double cosAngle = Math.cos(Math.toRadians(diry[0]));
                long[] start_s = {beam};
                long[] count_s = {1};
                int[] rxAntenna = xsfBase.getBeamGroup().getReceive_transducer_index().get_int(start_s, count_s);
                if (rxAntenna[0] >= 0 && rxAntenna[0] < rxBeamWidht.length) {
                    value[0] = (float) (rxBeamWidht[rxAntenna[0]] / cosAngle);
                } else {
                    value[0] = Float.NaN;
                }
                xsfBase.getBeamGroup().getBeamwidth_receive_major().put(start, count, value);
            }
        }
    }
}
