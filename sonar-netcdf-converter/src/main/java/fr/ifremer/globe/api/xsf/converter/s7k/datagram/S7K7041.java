package fr.ifremer.globe.api.xsf.converter.s7k.datagram;

import java.util.Arrays;

import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7K7041Metadata;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KFile;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.api.xsf.util.types.UInt;
import fr.ifremer.globe.api.xsf.util.types.ULong;
import fr.ifremer.globe.api.xsf.util.types.UShort;

public class S7K7041 extends S7KMultipingDatagram {

	@Override
	public SwathIdentifier genSwathIdentifier(S7KFile stats, DatagramBuffer buffer, long time) {
		return stats.getSwathId(getPingNumber(buffer).getU(), getMultipingSequence(buffer).getU(), getTimestampNano(buffer));
	}

	@Override
	public void computeSpecificMetadata(S7KFile metadata, DatagramBuffer buffer, long type,
			DatagramPosition position, long time, SwathIdentifier swathId) {
		getSpecificMetadata(metadata).getOrCreateMetadataForDevice(getDeviceIdentifier(buffer).getU()).registerSwath(position, getPingNumber(buffer).getU(), getMultipingSequence(buffer).getU(), getNumberOfBeams(buffer).getU(),  getOptionalDataOffset(buffer).getU() != 0);
	}

	@Override
	public S7K7041Metadata getSpecificMetadata(S7KFile metadata) {
		return metadata.get7041Metadata();
	}

	public static ULong getSonarId(BaseDatagramBuffer buffer) {
		return TypeDecoder.read8U(buffer.byteBufferData, 0);
	}
	
	public static UInt getPingNumber(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4U(buffer.byteBufferData, 8);
	}
	
	public static UShort getMultipingSequence(BaseDatagramBuffer buffer) {
		return TypeDecoder.read2U(buffer.byteBufferData, 12);
	}
	
	public static UShort getNumberOfBeams(BaseDatagramBuffer buffer) {
		return TypeDecoder.read2U(buffer.byteBufferData, 14);
	}
	
	public static UShort getFlags(BaseDatagramBuffer buffer) {
		return TypeDecoder.read2U(buffer.byteBufferData, 16);
	}
	
	public static FFloat getSampleRate(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 18);
	}
	
	
	
	public static class BeamIndex implements Comparable<BeamIndex> {
		public int beamNumber;
		public float beamAngle;
		public int numberOfSample;
		public short[] samples;
		@Override
		public int compareTo(BeamIndex o) {
			if (beamNumber > 0) {
				return Integer.compare(this.beamNumber, o.beamNumber);
			} else {
				return Float.compare(this.beamAngle, o.beamAngle);
			}
		}
		public void allocateSample(int ns) {
			samples=new short[ns];
			this.numberOfSample=ns;
			
		}
	}
	
	public static class WCData {
		public BeamIndex[] beams;
	}
	
	public static WCData getData(BaseDatagramBuffer buffer) {
		WCData ret = new WCData();
		final int numBeams = Math.toIntExact(getNumberOfBeams(buffer).getU());
		final short flags = getFlags(buffer).getShort();
		final int datasize = (((flags & 0x01) == 0) ? 1 : 2);
		final int idSize = (((flags & 0x10) == 0) ? 2 : 4);
		int offset = 38;
		ret.beams = new BeamIndex[numBeams];
		for (int i = 0; i < numBeams; i++) {
			BeamIndex bi = new BeamIndex();
			if (idSize == 2) {
				bi.beamNumber = Short.toUnsignedInt(buffer.byteBufferData.getShort(offset));
				bi.beamAngle = Float.NaN;
			} else {
				bi.beamNumber = -1;
				bi.beamAngle = buffer.byteBufferData.getFloat(offset);
			}
			final int ns = Math.toIntExact(Integer.toUnsignedLong(buffer.byteBufferData.getInt(offset + idSize)));
			bi.allocateSample(ns);
			offset += idSize + 4;
			if (datasize == 1) {
				for (int j = 0; j < ns; j++) {
					bi.samples[j] = (short) (buffer.byteBufferData.get(offset) * 256);
					offset++;
				}
			} else {
				for (int j = 0; j < ns; j++) {
					bi.samples[j] = buffer.byteBufferData.getShort(offset);
					offset += 2;
				}				
			}
			ret.beams[i] = bi;
		}
		Arrays.sort(ret.beams);
		return ret;
	}
}
