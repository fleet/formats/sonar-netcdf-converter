package fr.ifremer.globe.api.xsf.converter.kmall.xsf;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1F4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1String;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U2;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U8;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfFromKongsberg;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.HeightGrp;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.KmAllBaseDatagram;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.SHI;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.KmallFile;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.SHIMetadata;
import fr.ifremer.globe.api.xsf.util.SimpleIdentifier;
import fr.ifremer.globe.api.xsf.util.types.ULong;
import fr.ifremer.globe.netcdf.ucar.NCException;

public class SHIConverter implements IDatagramConverter<KmallFile, XsfFromKongsberg> {

	private List<ValueD1> valuesD1;
	private KmallFile stats;
	private SHIMetadata metadata;
	long nEntries;

	public SHIConverter(KmallFile stats) {
		this.stats = stats;
		this.metadata = this.stats.getSHIMetadata();
		this.nEntries = this.metadata.getTotalNumberOfEntries();
		this.valuesD1 = new LinkedList<>();
	}

	@Override
	public void declare(KmallFile allFile, XsfFromKongsberg xsf) throws NCException {
		HeightGrp heighGrp = xsf.getHeight();

		// declare all the variables

		valuesD1.add(new ValueD1U8(heighGrp.getDatagram_time(),
				buffer -> new ULong(KmAllBaseDatagram.getEpochTimeNano(buffer.byteBufferData))));

		valuesD1.add(
				new ValueD1U2(heighGrp.getSensor_system(), (buffer) -> SHI.getSensorSystem(buffer.byteBufferData)));

		valuesD1.add(
				new ValueD1U2(heighGrp.getSensor_status(), (buffer) -> SHI.getSensorStatus(buffer.byteBufferData)));

		valuesD1.add(new ValueD1U2(heighGrp.getSensor_type(), (buffer) -> SHI.getSensorType(buffer.byteBufferData)));

		valuesD1.add(new ValueD1F4(heighGrp.getHeight(), (buffer) -> SHI.getHeigthUsed(buffer.byteBufferData)));

		valuesD1.add(new ValueD1String(heighGrp.getData_received_from_sensor(),
				(buffer) -> SHI.getDataFromSensor(buffer.byteBufferData)));
	}

	@Override
	public void fillData(KmallFile kmAllFile) throws IOException, NCException {
		long[] originD1 = new long[] { 0 };

		DatagramBuffer buffer = new DatagramBuffer();
		for (Entry<SimpleIdentifier, DatagramPosition> posentry : metadata.getDatagrams()) {
			final DatagramPosition pos = posentry.getValue();
			DatagramReader.readDatagram(pos.getFile(), buffer, pos.getSeek());

			// read the data from the source file
			for (int i = 0; i < nEntries; i++) {
				for (ValueD1 value : this.valuesD1) {
					value.fill(buffer);
				}
			}
			// flush into the output file
			for (ValueD1 value : this.valuesD1) {
				value.write(originD1);
			}
			originD1[0] += 1;

		}

	}

}
