package fr.ifremer.globe.api.xsf.converter.kmall.tools;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;

import fr.ifremer.globe.api.xsf.converter.kmall.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.KmAllBaseDatagram;

public class KmAllShortener {
    static String file = "";
    static String dest = "";

    public static void main(String[] args) throws FileNotFoundException, IOException {
        try (RandomAccessFile raf = new RandomAccessFile(file, "r")) {
            final File outFile = new File(dest);
            if (outFile.exists()) {
                outFile.delete();
            }

            try (FileOutputStream output = new FileOutputStream(dest)) {                
                final DatagramBuffer buffer = new DatagramBuffer();
                long ref_time = -1;
                
                while (DatagramReader.readDatagram(raf, buffer) > 0) {
                    if (ref_time < 0) {
                        ref_time = KmAllBaseDatagram.getTimeSec(buffer.byteBufferData).getU();
                    } else {
                        if (KmAllBaseDatagram.getTimeSec(buffer.byteBufferData).getU() - ref_time > 1) {
                            break;
                        }
                    }
                    
                    output.write(buffer.rawBufferHeader);
                    output.write(buffer.rawBufferData, 0, buffer.byteBufferData.limit());
                }
            }
        }
    }
}
