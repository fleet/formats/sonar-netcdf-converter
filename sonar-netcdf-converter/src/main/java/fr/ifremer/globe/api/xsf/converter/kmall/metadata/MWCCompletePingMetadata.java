package fr.ifremer.globe.api.xsf.converter.kmall.metadata;

import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.AbstractCompletePingMetadata;

public class MWCCompletePingMetadata extends AbstractCompletePingMetadata<MWCCompletePingAntennaMetadata> {

	public MWCCompletePingMetadata(int numTxSectors, int[] serialNumbers) {
        super(numTxSectors, serialNumbers);
    }
    
    
}
