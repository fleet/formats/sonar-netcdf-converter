package fr.ifremer.globe.api.xsf.util.types;

public class ULong implements Comparable<ULong> {
    
    private long v;
    
    public ULong(long v) {
        this.v = v;
    }

    public long getLong() {
        return v;
    }
    
    public boolean isSigned() {
        return false;
    }

	@Override
	public int compareTo(ULong o) {
		return Long.compareUnsigned(v, o.v);
	}
}
