package fr.ifremer.globe.api.xsf.converter.common.utils.values.d2;

import java.util.Arrays;

import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCVariable;

/***
 * class allocating storage for 2D matrix, and giving a interface allowing to fill the data with attitude
 */
public class ValueD2F4WithAttitude extends ValueD2WithAttitude {
	protected float[] dataOut;
	private ValueProvider valueProvider;

	/** Constructor **/
	public ValueD2F4WithAttitude(NCVariable variable, ValueProvider filler) {
		super(variable);
		// allocate storage
		this.dataOut = new float[dim];
		this.valueProvider = filler;
	}
	
	@Override
	public void clear() {
		Arrays.fill(this.dataOut, variable.getFloatFillValue());
	}

	/**
	 * Here the lambda expression will expect roll/pitch/heave data in addition of the input buffer
	 */
	public interface ValueProvider {
		public FFloat get(BaseDatagramBuffer buffer, int beamIndexSource, double roll, double pitch, double heave);
	}

	/**
	 * call the lambda to retrieve the value and set it in the allocated netcdf buffer dataOut
	 */
	@Override
	public void fill(BaseDatagramBuffer buffer, int beamIndexSource, int beamIndexDest, double roll, double pitch, double heave) {
		dataOut[beamIndexDest] = valueProvider.get(buffer, beamIndexSource, roll, pitch, heave).get();
	}

	@Override
	public void write(long[] origin, long[] count) throws NCException {
		try {
			variable.put(origin, count, dataOut);
		} catch (Exception e) {
			logger.error(String.format("Error while writing variable %s (%s)", variable.getName(), e.getMessage()));
			throw e;
		}
	}
}