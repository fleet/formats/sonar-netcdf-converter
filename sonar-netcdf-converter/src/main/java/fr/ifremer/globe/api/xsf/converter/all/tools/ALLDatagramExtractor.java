package fr.ifremer.globe.api.xsf.converter.all.tools;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.nio.file.Paths;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramParser;
import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramReader;

/**
 * This is a utility that is used to extract datagrams form a .all file
 * in order to create unit tests. It will produce a raw dump of one datagram
 * of each type, and the same datagram as an Base64 encoded String.
 * 
 */
public class ALLDatagramExtractor {

    public static void main(String[] args) throws Exception {
        String input = "E:\\SonarScopeAnnexes\\SonarScopeDataArchive\\Tests_Format_Kongsberg\\WithBSCorr\\0014_20121105_103947_SimradEcho.all"; 
        		
        String output = "E:\\temp\\datagrams";

        // make sure output exists
        final File outFile = new File(output);
        outFile.mkdirs();

        Files.walk(Paths.get(input))
        .filter(Files::isRegularFile)
        .filter((p) -> {
            return p.toString().endsWith(".all");
        })
        .forEach((f) -> {
            RandomAccessFile raf = null;
            try {
                raf = new RandomAccessFile(f.toFile(), "r");

                final DatagramBuffer buffer = new DatagramBuffer(DatagramReader.getByteOrder(raf));
                final DatagramParser parser = new DatagramParser();

                while ((DatagramReader.readDatagram(raf, buffer) > 0)) {
                    final byte datagramType = parser.getDatagramType(buffer.byteBufferData);
                    final String datagramName = String.format("0x%2x", datagramType);
                    final File dgmFolder = new File(outFile, datagramName);

                    if (!dgmFolder.exists()) {
                        dgmFolder.mkdir();
                        // generate a micro file
                        FileOutputStream binRawFile = new FileOutputStream(new File(dgmFolder, "datagram.all"));
                        binRawFile.write(buffer.rawBufferHeader);
                        binRawFile.write(buffer.rawBufferData, 0, buffer.byteBufferData.limit());
                        binRawFile.close();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (raf != null) {
                    try {
                        raf.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

    }
}
