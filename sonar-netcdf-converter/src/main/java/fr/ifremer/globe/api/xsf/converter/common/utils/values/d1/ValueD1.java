package fr.ifremer.globe.api.xsf.converter.common.utils.values.d1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCVariable;

/***
 * class allocating storage for 1D matrix, and giving a interface allowing to fill the data *
 */
public abstract class ValueD1 {
	protected static final Logger logger = LoggerFactory.getLogger(ValueD1.class);

	public final NCVariable variable;
	/** Buffer capacity */
	public final int dataOutCapacity;
	/** Index of insertion in buffer */
	protected int dataOutPosition = 0;

	/***
	 * Constructor, allocate netcdf buffer for one variable. <br>
	 * Capacity of the buffer is set to 1. So, each value added with {@link #fill(BaseDatagramBuffer)} be followed by a
	 * call of {@link #write(long[])}
	 */
	protected ValueD1(NCVariable variable) {
		this(variable, false);
	}

	/***
	 * Constructor, allocate netcdf buffer for one variable. <br>
	 * Creates a buffer of capacity equals to the variable dimension. So, {@link #write(long[])} method must be invoked
	 * once, after all invocations of {@link #fill(BaseDatagramBuffer)}
	 */
	protected ValueD1(NCVariable variable, boolean bufferCapacityIsDimension) {
		this.variable = variable;
		dataOutCapacity = bufferCapacityIsDimension ? (int) variable.getShape().get(0).getLength() : 1;
	}

	/**
	 * Call the lambda to get one value and add it to the internal buffer
	 */
	public void fill(BaseDatagramBuffer buffer) throws NCException {
		fill(buffer, dataOutPosition);
	}

	/**
	 * Call the lambda to get one value and set it to the internal buffer at the specified position
	 */
	public abstract void fill(BaseDatagramBuffer buffer, int position) throws NCException;

	/**
	 * Write the internal buffer into the netCDF file.
	 */
	public abstract void write(long[] origin) throws NCException;

	/** Reset the netCDF variable to its FillValue */
	public abstract void clear() throws NCException;
}
