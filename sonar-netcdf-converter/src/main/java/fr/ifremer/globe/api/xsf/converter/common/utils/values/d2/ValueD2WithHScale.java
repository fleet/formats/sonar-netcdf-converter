package fr.ifremer.globe.api.xsf.converter.common.utils.values.d2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCVariable;

/***
 * class allocating storage for 2D matrix, and giving a interface allowing to fill the data *
 */
public abstract class ValueD2WithHScale {
	protected static final Logger logger = LoggerFactory.getLogger(ValueD2WithHScale.class);
	protected NCVariable variable;
	protected int dim;

	/***
	 * Constructor
	 */
	public ValueD2WithHScale(NCVariable variable) {
		this.variable = variable;
		this.dim = (int) variable.getShape().get(1).getLength();
	}
	
	/**
	 * Resets data array.
	 */
	public abstract void clear();

	/**
	 * call the lambda to retrieve the value and set it in the allocated netcdf buffer dataOut
	 */
	public abstract void fill(BaseDatagramBuffer buffer, int beamIndexSource, int beamIndexDest, double hScale);

	/**
	 * Flush the data into the underlying netcdf variable.
	 * 
	 * @param origin
	 * @param count
	 * @throws NCException
	 */
	public abstract void write(long[] origin, long[] count) throws NCException;

}
