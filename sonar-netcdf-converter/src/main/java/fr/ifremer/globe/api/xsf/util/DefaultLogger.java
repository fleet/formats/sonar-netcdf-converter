package fr.ifremer.globe.api.xsf.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultLogger {

	static Logger logger = LoggerFactory.getLogger(DefaultLogger.class);

	public static Logger get() {
		return logger;
	}

	public static void setLogger(Logger logger) {
		DefaultLogger.logger = logger;
	}

}
