package fr.ifremer.globe.api.xsf.converter.common.xsf.types;

public enum transmit_t {
	CW((byte)0), LFM((byte)1), HFM((byte)2);
	private byte value;

	public byte getValue() {
		return value;
	}

	transmit_t(byte value)
	{
		this.value=value;
	}
}
