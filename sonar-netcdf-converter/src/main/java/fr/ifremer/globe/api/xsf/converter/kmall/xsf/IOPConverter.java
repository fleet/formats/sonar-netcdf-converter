package fr.ifremer.globe.api.xsf.converter.kmall.xsf;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.exceptions.IndexingException;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1F4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1String;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U8;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfFromKongsberg;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.RuntimeGrp;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.IOP;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.KmAllBaseDatagram;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.IOPMetadata;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.KmallFile;
import fr.ifremer.globe.api.xsf.util.DefaultLogger;
import fr.ifremer.globe.api.xsf.util.SimpleIdentifier;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.api.xsf.util.types.ULong;
import fr.ifremer.globe.netcdf.ucar.NCException;

public class IOPConverter implements IDatagramConverter<KmallFile, XsfFromKongsberg> {

	private KmallFile stats;
	private IOPMetadata metadata;
	private List<ValueD1> values;

	public IOPConverter(KmallFile metadata) {
		this.stats = metadata;
		this.metadata = this.stats.getIOPMetadata();
		values = new LinkedList<>();
	}

	@Override
	public void declare(KmallFile allFile, XsfFromKongsberg xsf) throws NCException {
		RuntimeGrp runtimeGroup = xsf.getRuntime().get();

		values.add(new ValueD1U8(runtimeGroup.getTime(),
				buffer -> new ULong(KmAllBaseDatagram.getEpochTimeNano(buffer.byteBufferData))));

		values.add(new ValueD1String(runtimeGroup.getRuntime_txt(), (buffer) -> IOP.getRuntimeTxt(buffer.byteBufferData)));
		values.add(new ValueD1F4(runtimeGroup.getTVG_law_crossover_angle(),
				(buffer) -> new FFloat(IOP.getTvgLawCrossOverAngle(buffer.byteBufferData))));
	}

	@Override
	public void fillData(KmallFile kmAllFile) throws IOException, NCException {
		DatagramBuffer buffer = new DatagramBuffer();

		for (Entry<SimpleIdentifier, DatagramPosition> posentry : metadata.getDatagrams()) {
			DatagramReader.readDatagram(posentry.getValue().getFile(), buffer, posentry.getValue().getSeek());
			for (ValueD1 v : this.values) {
				v.fill(buffer);
				try {
					v.write(new long[] { metadata.getGenerator().getIndex(posentry.getKey()) });
				} catch (IndexingException e) {
					DefaultLogger.get()
							.error(String.format("Error decoding %s cannot retrieve storage index for ping %s",
									this.getClass().getSimpleName(), posentry.getKey()));
				}
			}
		}
	}

}
