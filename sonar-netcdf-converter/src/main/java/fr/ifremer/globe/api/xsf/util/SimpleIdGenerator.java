package fr.ifremer.globe.api.xsf.util;

import fr.ifremer.globe.api.xsf.converter.common.exceptions.IndexingException;

/**
 * This class helps to handle sequential ids
 * Ids will be pushed onto a list of raw ids,
 * 
 * afterward the list will be sorted then each index can be retrieved from the raw id
 */
public class SimpleIdGenerator implements IndexGenerator<SimpleIdentifier> {
	
	final static long INVALID=-1;
	private long minValue=INVALID;
	private long maxValue=INVALID;
	
	/** 
	 * Get an identifier given an id for a raw ping number 
	 * */
	public SimpleIdentifier getId(long raw) {
		if(minValue==INVALID)
		{
			maxValue=minValue=raw;
		}
		minValue=Math.min(minValue, raw);
		maxValue=Math.max(maxValue, raw);
		return new SimpleIdentifier(raw);
	}
	/* (non-Javadoc)
	 * @see fr.ifremer.globe.api.xsf.util.IndexGenerator#generateIndex()
	 */
	@Override
	public void generateIndex()
	{
		//empty method, the index is given by the offset from a current value to the min value added
	}

	
	/* (non-Javadoc)
	 * @see fr.ifremer.globe.api.xsf.util.IndexGenerator#getIndexSize()
	 */
	@Override
	public	long getIndexSize()
	{
		if(maxValue==-1)
			return 0;
		return maxValue-minValue+1;
	}
	
	/* (non-Javadoc)
	 * @see fr.ifremer.globe.api.xsf.util.IndexGenerator#getIndex(fr.ifremer.globe.api.xsf.util.DgIdentifier)
	 */
	@Override
	public long getIndex(SimpleIdentifier identifier) throws IndexingException
	{
		if(minValue==INVALID)
			throw new IndexingException(String.format("Bug while retrieving datagram index , %s index was not generated",this.getClass().getSimpleName()));
		return identifier.getRawId()-minValue;
	}
}
