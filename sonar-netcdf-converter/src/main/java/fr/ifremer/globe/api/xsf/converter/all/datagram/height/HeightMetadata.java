package fr.ifremer.globe.api.xsf.converter.all.datagram.height;


import fr.ifremer.globe.api.xsf.converter.all.datagram.IndividualSensorMetadata;
import fr.ifremer.globe.api.xsf.util.types.UShort;

/**
 * */
public class HeightMetadata extends IndividualSensorMetadata {

	private UShort serialNumber;

	public void setSerialNumber(UShort serialNumber) {
		this.serialNumber = serialNumber;
	}
	
	public UShort getSerialNumber() {
		return serialNumber;
	}
	
	private float heightSum;
	private long heightNumber;
	public float heightMin, heightMax;	
	
	public HeightMetadata() {
        // Tells the parent class that individual sensor class
        // IndividualSensorMetadata. If for some other datagram,
        // this class is not enough, it can be extended and
        // given here as well as in the class declaration.
		heightSum 		= 0;
		heightNumber 	= 0;
		heightMin 		= 999999f;
		heightMax 		= -99999f;
    }
    
	// Methodes pour les statistiques de Summary.
	public void addHeight(int heightValue) {
		this.heightSum += heightValue;
		this.heightNumber++;
	}
	
	public float getHeightMean() {
		return this.heightSum / this.heightNumber;
	}
	
	public void computeMinMaxHeight(int heightValue) {
    	heightMin = Float.min((float)(heightValue*0.01), (float) heightMin);
    	heightMax = Float.max((float)(heightValue*0.01), (float) heightMax);		
	}
	
    public float getHeightMin()
    {
    	return heightMin;
    }

    public float getHeightMax()
    {
    	return heightMax;
    }
    
}