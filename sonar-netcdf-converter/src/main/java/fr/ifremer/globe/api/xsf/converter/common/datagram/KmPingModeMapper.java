package fr.ifremer.globe.api.xsf.converter.common.datagram;

import java.util.stream.Stream;

import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Utility mapper from .all or .kmall files to xsf runtime mode
 */
public class KmPingModeMapper {

	/**
	 * .all ping mode enum and values for older system (eg EM3000 and EM3002 we're
	 * gonna assume : ) Nearfield (4�) => VERY_SHALLOW Normal (1.5�)=> SHALLOW
	 * Target detect => MEDIUM Ping mode (EM 3002) Wide Tx beamwidth (4�) ->
	 * VERY_SHALLOW Normal Tx beamwidth (1.5� -> SHALLOW
	 */
	public enum AllPingMode {
		VERY_SHALLOW(0), SHALLOW(1), MEDIUM(2), DEEP(3), VERY_DEEP(4), EXTRA_DEEP(5);

		public final int value;

		private AllPingMode(int value) {
			this.value = value;
		}

		public static AllPingMode fromValue(int v) throws GIOException {
			return Stream.of(values()).//
					filter(x -> x.value == v).//
					findFirst().//
					orElseThrow(() -> new GIOException("Trying to match invalid pingMode value :" + v));
		}

	}

	/**
	 * .kmall ping mode enum and values
	 */
	public enum KmallPingMode {
		VERY_SHALLOW(0), SHALLOW(1), MEDIUM(2), DEEP(3), DEEPER(4), VERY_DEEP(5), EXTRA_DEEP(6), EXTREME_DEEP(7);

		public final int value;

		private KmallPingMode(int value) {
			this.value = value;
		}

		public static KmallPingMode fromValue(int v) throws GIOException {
			return Stream.of(values()).//
					filter(x -> x.value == v || x.value == v - 100).// Kmall special case, values can be x or 100+x to
																	// indicate manual mode
					findFirst().//
					orElseThrow(() -> new GIOException("Trying to match invalid pingMode value :" + v));
		}
	}

	/**
	 * .kmall ping mode enum and values
	 */
	public enum XsfPingMode {
		VERY_SHALLOW(0), SHALLOW(1), MEDIUM(2), DEEP(3), DEEPER(4), VERY_DEEP(5), EXTRA_DEEP(6), EXTREME_DEEP(7),
		UNKNOWN(-128);

		public final int value;

		private XsfPingMode(int value) {
			this.value = value;
		}

		public static XsfPingMode fromValue(int v) throws GIOException {
			return Stream.of(values()).//
					filter(xsfPulseLengthMode -> xsfPulseLengthMode.value == v).//
					findFirst().//
					orElseThrow(() -> new GIOException("Trying to match invalid pingMode value :" + v));
		}
	}

	public static XsfPingMode fromAll(AllPingMode srcMode) {
		switch (srcMode) {
		case VERY_SHALLOW:
			return XsfPingMode.VERY_SHALLOW;
		case SHALLOW:
			return XsfPingMode.SHALLOW;
		case MEDIUM:
			return XsfPingMode.MEDIUM;
		case DEEP:
			return XsfPingMode.DEEP;
		case VERY_DEEP:
			return XsfPingMode.VERY_DEEP;
		case EXTRA_DEEP:
			return XsfPingMode.EXTRA_DEEP;
		default:
			return XsfPingMode.UNKNOWN;
		}
	}

	public static XsfPingMode fromkmall(KmallPingMode srcMode) throws GIOException {
		return XsfPingMode.fromValue(srcMode.value);
	}

}
