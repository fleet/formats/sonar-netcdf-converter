package fr.ifremer.globe.api.xsf.converter.kmall.metadata;

import java.util.Set;

import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.AbstractBeamDatagramMetadata;

public class MRZMetadata extends AbstractBeamDatagramMetadata<MRZCompletePingMetadata> {

	private int numExtraDetectionClasses;
	private int maxExtraDetectionsCount;
	private int maxExtraDetectionSeabedSampleCount;

	private float depthSum, BSSum;
	private long numberOfDepth, numberOfBS;
	public float depthMin, depthMax, BSMin, BSMax;
	private double latMin = 9999;
	private double latMax = -9999;
	private double lonMin = 9999;
	private double lonMax = -9999;

	private int mrzVersion = -1;
	
	public MRZMetadata() {
		depthSum = 0;
		numberOfDepth = 0;
		depthMin = Float.MAX_VALUE;
		depthMax = -Float.MAX_VALUE;
		BSSum = 0;
		numberOfBS = 0;
		BSMin = Float.MAX_VALUE;
		BSMax = -Float.MAX_VALUE;
	}

	@Override
	public void computeIndex(Set<Integer> allAntennas) {
		super.computeIndex(allAntennas);
		numExtraDetectionClasses = 0;
		maxExtraDetectionsCount = 0;
		maxExtraDetectionSeabedSampleCount = 0;

		for (MRZCompletePingMetadata ping : index.values()) {
			if (ping.isComplete(allAntennas)) {
				numExtraDetectionClasses = Math.max(numExtraDetectionClasses, ping.getNumExtraDetectionClasses());
				maxExtraDetectionsCount = Math.max(maxExtraDetectionsCount, ping.getExtraDetectionsCount());
				maxExtraDetectionSeabedSampleCount = Math.max(maxExtraDetectionSeabedSampleCount, ping.getMaxExtraDetectionSeabedSampleCount());
			}
		}
	}

	public void computeBoundingBoxLat(double lat) {
		if (!Double.isNaN(lat) && lat <= 90 && lat >= -90) {
			latMin = Double.min(lat, latMin);
			latMax = Double.max(lat, latMax);
		}
	}

	public void computeBoundingBoxLon(double lon) {
		if (!Double.isNaN(lon) && lon <= 180 && lon >= -180) {
			lonMin = Double.min(lon, lonMin);
			lonMax = Double.max(lon, lonMax);
		}
	}

	
	public int getNumExtraDetectionClasses() {
		return numExtraDetectionClasses;
	}

	public int getMaxExtraDetectionsCount() {
		return maxExtraDetectionsCount;
	}

	public int getMaxExtraDetectionSeabedSampleCount() {
		return maxExtraDetectionSeabedSampleCount;
	}

	public void setVersion(int datagramVersion)
	{
		if(mrzVersion==-1)
			mrzVersion=datagramVersion;
		if(mrzVersion!=datagramVersion)
		{
			//in the same file we got incompatible datagram version numbers
			throw new RuntimeException("Error while parsing MRZ datagram, inconsistent mrz datagram version");
		}
		mrzVersion=datagramVersion;
	}
	
	public void addDepth(float depth) {
		this.depthSum += depth;
		this.numberOfDepth++;
		depthMin = Float.min(depth, (float) depthMin);
		depthMax = Float.max(depth, (float) depthMax);
	}

	public float getDepthMean() {
		return this.depthSum / this.numberOfDepth;
	}

	public float getDepthMin() {
		return depthMin;
	}

	public float getDepthMax() {
		return depthMax;
	}

	public float getBSMean() {
		return this.BSSum / this.numberOfBS;
	}

	public float getBSMin() {
		return BSMin;
	}

	public float getBSMax() {
		return BSMax;
	}

	public double getLonMin() {
		return lonMin;
	}

	public double getLonMax() {
		return lonMax;
	}

	public double getLatMin() {
		return latMin;
	}

	public double getLatMax() {
		return latMax;
	}

	public int getMrzVersion() {
		return mrzVersion;
	}

	public void addReflectivity(float BS) {
		this.BSSum += BS;
		this.numberOfBS++;
		BSMin = Float.min(BS, (float) BSMin);
		BSMax = Float.max(BS, (float) BSMax);
	}

}
