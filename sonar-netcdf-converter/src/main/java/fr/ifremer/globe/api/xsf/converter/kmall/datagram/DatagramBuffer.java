package fr.ifremer.globe.api.xsf.converter.kmall.datagram;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;

public class DatagramBuffer extends BaseDatagramBuffer {
    
    /**
     * Initialize the buffer
     */
    public DatagramBuffer() {
        this(0);
    }
    
    /**
     * Initialize a buffer to old capacity bytes in its data section
     * @param capacity
     */
    public DatagramBuffer(int capacity) {
        super(ByteOrder.LITTLE_ENDIAN);
        this.rawBufferHeader = new byte[4];
        this.byteBufferSizeHeader = ByteBuffer.wrap(this.rawBufferHeader);
        this.byteBufferSizeHeader.order(byteOrder);

        this.allocate(capacity);        
    }
    
    /**
     * Make sure the buffer can hold a body of desiredCapacity, grow the buffer if necessary
     * 
     * @param desiredCapacity
     */
    public void allocate(int desiredCapacity) {
        if (this.rawBufferData == null || this.rawBufferData.length < desiredCapacity) {
            this.rawBufferData = new byte[desiredCapacity];
            this.byteBufferData = ByteBuffer.wrap(this.rawBufferData);
            this.byteBufferData.order(byteOrder);
        }
    }
    
    /**
     * validate the buffert
     * @throws IOException
     */
    public void validate() throws IOException {
        long size = Integer.toUnsignedLong(byteBufferSizeHeader.getInt(0));
        
        int sizeRepeatOffset = (int) size - 8; // 2 uint32
        long sizeRepeated = Integer.toUnsignedLong(byteBufferData.getInt(sizeRepeatOffset));
        
        if (size != sizeRepeated) {
            throw new IOException("Invalid size for kmall datagram");
        }
    }
}
