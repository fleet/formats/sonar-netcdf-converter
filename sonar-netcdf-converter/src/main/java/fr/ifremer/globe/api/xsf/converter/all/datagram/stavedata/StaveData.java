package fr.ifremer.globe.api.xsf.converter.all.datagram.stavedata;

import java.nio.ByteBuffer;

import fr.ifremer.globe.api.xsf.converter.all.datagram.PingDatagram;
import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.SByte;
import fr.ifremer.globe.api.xsf.util.types.SShort;
import fr.ifremer.globe.api.xsf.util.types.UInt;
import fr.ifremer.globe.api.xsf.util.types.UShort;

public class StaveData extends PingDatagram {
    /** {@inheritDoc} */
    @Override
    public StaveDataMetadata getSpecificMetadata(AllFile metadata) {
        return metadata.getStaveData();
    }

    @Override
    protected void computeSpecificMetadata(AllFile metadata, ByteBuffer datagram, long epochTime, SwathIdentifier swathId,
            int serialNumber, DatagramPosition position) {
        StaveDataMetadata staveDataMetadata = getSpecificMetadata(metadata);
        
        StaveDataCompletePingMetadata completeping = staveDataMetadata.getEntry(swathId);
        if (completeping == null) {
            completeping = new StaveDataCompletePingMetadata();
            staveDataMetadata.setEntry(swathId, completeping);
        }
        
        StaveDataCompleteAntennaPingMetadata ping = completeping.getOrCreate(serialNumber, getDatagramCount(datagram).getU());
        ping.pushDatagram(position, getDatagramNumber(datagram).getU() - 1, getSamplesCountInDgm(datagram).getU(), getStavesCountPerSample(datagram).getU());
    }

    public static UShort getDatagramCount(ByteBuffer datagram) {
        return TypeDecoder.read2U(datagram, 16);
    }

    public static UShort getDatagramNumber(ByteBuffer datagram) {
        return TypeDecoder.read2U(datagram, 18);
    }

    public static UInt getSamplingFreq(ByteBuffer datagram) {
        return TypeDecoder.read4U(datagram, 20);
    }

    public static UShort getSoundSpeed(ByteBuffer datagram) {
        return TypeDecoder.read2U(datagram, 24);
    }

    public static UShort getStartRange(ByteBuffer datagram) {
        return TypeDecoder.read2U(datagram, 26);
    }

    public static UShort getTotalSamplesPerStave(ByteBuffer datagram) {
        return TypeDecoder.read2U(datagram, 28);
    }

    public static UShort getSamplesCountInDgm(ByteBuffer datagram) {
        return TypeDecoder.read2U(datagram, 30);
    }

    public static UShort getStaveIndex(ByteBuffer datagram) {
        return TypeDecoder.read2U(datagram, 32);
    }

    public static UShort getStavesCountPerSample(ByteBuffer datagram) {
        return TypeDecoder.read2U(datagram, 34);
    }

    public static UShort getRangeToNormaIncidence(ByteBuffer datagram) {
        return TypeDecoder.read2U(datagram, 36);
    }

    // Spare : 2U

    /**
     * START Stave Data - Ns Entries
     */
    // utility functions to iterate over the Ns Samples
    public static int getStaveDataEntriesOffset(ByteBuffer datagram) {
        return 40;
    }
    public static int getNextStaveDataEntriesOffset(ByteBuffer datagram, int stavePerSample, int currentSDEntriesOffset) {
        return currentSDEntriesOffset + 4 + stavePerSample;
    }
    
    public static UShort getSampleNumber(ByteBuffer datagram, int offset) {
        return TypeDecoder.read2U(datagram, offset);
    }
    
    public static SShort getTVGGain(ByteBuffer datagram, int offset) {
        return TypeDecoder.read2S(datagram, offset + 2);
    }
    
    public static SByte getStaveBackscatter(ByteBuffer datagram, int offset, int bsNumber) {
        return TypeDecoder.read1S(datagram, offset + 4 + bsNumber);
    }
}
