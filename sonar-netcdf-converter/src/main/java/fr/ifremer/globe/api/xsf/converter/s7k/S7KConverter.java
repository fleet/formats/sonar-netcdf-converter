package fr.ifremer.globe.api.xsf.converter.s7k;

import java.io.IOException;

import org.slf4j.Logger;

import fr.ifremer.globe.api.xsf.converter.MbgConverterParameters;
import fr.ifremer.globe.api.xsf.converter.XsfConverterParameters;
import fr.ifremer.globe.api.xsf.converter.common.IConverter;
import fr.ifremer.globe.api.xsf.converter.common.INcFileWriter;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgBase;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfBase;
import fr.ifremer.globe.api.xsf.converter.s7k.mbg.S7KMbgWriter;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KFile;
import fr.ifremer.globe.api.xsf.converter.s7k.xsf.S7KXsfWriter;
import fr.ifremer.globe.api.xsf.util.DefaultLogger;

public class S7KConverter implements IConverter<S7KFile> {

	/** Logger **/
	private Logger logger = DefaultLogger.get();

	@Override
	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	@Override
	public Logger getLogger() {
		return logger;
	}

	@Override
	public boolean accept(String inputFilePath) {
		return inputFilePath.endsWith(S7KFile.EXTENSION_S7K);
	}

	@Override
	public S7KFile indexFile(String inputFilePath, boolean parseWaterColumData) throws IOException {
		return new S7KFile(inputFilePath, parseWaterColumData,true);
	}

	@Override
	public INcFileWriter<S7KFile, MbgBase, MbgConverterParameters> getMbgWriter() {
		return new S7KMbgWriter();
	}

	@Override
	public INcFileWriter<S7KFile, ? extends XsfBase, XsfConverterParameters> getXsfWriter() {
		return new S7KXsfWriter();
	}

}
