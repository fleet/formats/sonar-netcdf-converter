package fr.ifremer.globe.api.xsf.converter.common.datagram;

import java.nio.ByteBuffer;

import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.common.utils.DateUtil;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.utils.date.DateUtils;

/**
 * base class for all datagrams,
 * <p>
 * <b> warning </b>, this class is <b>stateless</b> to prevent memory allocation during reading
 * <p>
 * <p>
 */
public abstract class BaseDatagram implements Datagram {
	/**
	 * compute the meta data, buffer is supposed to start and be positionned after the STX identifier
	 */
	@Override
	public final void computeMetaData(AllFile metadata, ByteBuffer datagram, byte datagramType,
			DatagramPosition position) {
		int modelNumber = getModelNumber(datagram);
		long epochTime = getEpochTime(datagram);

		metadata.getGlobalMetadata().datagramCount++;
		metadata.getGlobalMetadata().setModelNumber(modelNumber);

		getSpecificMetadata(metadata).addDatagram();
		computeSpecificMetadata(metadata, datagram, datagramType, epochTime, position);

	}

	/**
	 * compute and add specific metadata per datagram type
	 */
	protected abstract void computeSpecificMetadata(AllFile metadata, ByteBuffer datagram, byte datagramType,
			long epochTime, DatagramPosition position);

	public static long getEpochTime(ByteBuffer datagram) {
		long dateKongsbergFormat = TypeDecoder.read4U(datagram, 4).getU();
		// time since midnight in milliseconds
		long timeSinceMidnight = TypeDecoder.read4U(datagram, 8).getU();
		long epochTime = DateUtil.convertKongsbergTime(dateKongsbergFormat, timeSinceMidnight);
		return epochTime;
	}

	public static long milliSecondToNano(long value) {
		return DateUtils.milliSecondToNano(value);
	}

	/**
	 * Return time in nano second since epoch time
	 */
	public static long getXSFEpochTime(ByteBuffer datagram) {
		return milliSecondToNano(getEpochTime(datagram));
	}

	public static int getModelNumber(ByteBuffer datagram) {
		return Short.toUnsignedInt(datagram.getShort(2));

	}

}
