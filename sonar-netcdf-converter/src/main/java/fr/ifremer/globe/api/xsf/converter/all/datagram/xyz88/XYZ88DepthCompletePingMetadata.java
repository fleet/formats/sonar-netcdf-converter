package fr.ifremer.globe.api.xsf.converter.all.datagram.xyz88;

import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.AbstractCompletePingMetadata;

public class XYZ88DepthCompletePingMetadata extends AbstractCompletePingMetadata<XYZ88DepthCompleteAntennaPingMetadata> {
	public XYZ88DepthCompletePingMetadata(int[] serialNumbers) {
		super(0, serialNumbers);
	}
}
