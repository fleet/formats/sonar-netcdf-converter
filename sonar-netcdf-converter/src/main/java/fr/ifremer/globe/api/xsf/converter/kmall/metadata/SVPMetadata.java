package fr.ifremer.globe.api.xsf.converter.kmall.metadata;


import fr.ifremer.globe.api.xsf.converter.all.datagram.IndividualSensorMetadata;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;

public class SVPMetadata extends IndividualSensorMetadata {
    
    private float 	sspSum,  tempSum, salSum;
    private long 	sspCount,  tempCount, salCount;
    public float 	sspMin, 
    				sspMax, 
    				sspMean,
    				tempMin,
    				tempMax,
    				tempMean,
    				salMin,
    				salMax,
    				salMean;
    
    public SVPMetadata() {
    	sspSum 		= 0;
    	sspCount 	= 0;
        sspMin 		= Float.MAX_VALUE;
        sspMax 		= -Float.MAX_VALUE;
    }

    /**
     * Register a new datagrams. Since datagrams do not have sequencial numbers,
     * they are assumed to come in sequencial order.
     */
    public void addDatagram(DatagramPosition pos, int nSamples) {
        super.addDatagram(pos, this.datagramCount - 1, nSamples);
    }

    public float getSSPMean() {
        return this.sspSum / this.sspCount;
    }    
    public float getSSPMin()
    {
        return sspMin;
    }
    public float getSSPMax()
    {
        return sspMax;
    }    

    public void addSSP(float ssp) {
        this.sspSum += ssp;
        this.sspCount++;
        sspMin = Float.min(ssp, (float) sspMin);
        sspMax = Float.max(ssp, (float) sspMax);      
    }
    
  
    public float getTempMean() {
        return this.tempSum / this.tempCount;
    }    
    public float getTempMin()
    {
        return tempMin;
    }
    public float getTempMax()
    {
        return tempMax;
    }    
    public void addTemp(float temp) {
        this.tempSum += temp;
        this.tempCount++;
        tempMin = Float.min(temp, (float) tempMin);
        tempMax = Float.max(temp, (float) tempMax);      
    }

    public float getSalMean() {
        return this.salSum / this.salCount;
    }    
    public float getSalMin()
    {
        return salMin;
    }
    public float getSalMax()
    {
        return salMax;
    }    
    public void addSal(float sal) {
        this.salSum += sal;
        this.salCount++;
        salMin = Float.min(sal, (float) salMin);
        salMax = Float.max(sal, (float) salMax);      
    }
}
