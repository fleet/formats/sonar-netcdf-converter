package fr.ifremer.globe.api.xsf.converter.all.mbg.bathy;

import java.io.IOException;

import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.all.datagram.rawrange70.RawRange70;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgBase;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgConstants;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2S4;
import fr.ifremer.globe.api.xsf.util.types.SInt;
import fr.ifremer.globe.netcdf.api.convention.CFStandardNames;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCVariable;

/**
 * Converter for RawRange datagram
 */
public class RawRange70DatagramConverter extends AbstractRawRangeDatagramConverter {

	@Override
	public void declare(AllFile allFile, MbgBase mbg) throws NCException {
		NCVariable rangeVariable = mbg.getVariable(MbgConstants.Range);
		double rangeFactor = rangeVariable.getAttributeDouble(CFStandardNames.CF_SCALE_FACTOR);
		double rangeOffset = rangeVariable.getAttributeDouble(CFStandardNames.CF_ADD_OFFSET);
		swathBeamValues.add(new ValueD2S4(rangeVariable, (buffer, i) -> new SInt(
				(int) ((RawRange70.getRange(buffer.byteBufferData, i).getU() - rangeOffset) / 2 / rangeFactor))));
	}

	@Override
	public void fillData(AllFile md) throws IOException, NCException {
		// TODO ??
	}
}
