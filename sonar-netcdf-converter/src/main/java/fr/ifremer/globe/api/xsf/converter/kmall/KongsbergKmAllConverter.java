package fr.ifremer.globe.api.xsf.converter.kmall;

import java.io.IOException;

import org.slf4j.Logger;

import fr.ifremer.globe.api.xsf.converter.MbgConverterParameters;
import fr.ifremer.globe.api.xsf.converter.XsfConverterParameters;
import fr.ifremer.globe.api.xsf.converter.common.IConverter;
import fr.ifremer.globe.api.xsf.converter.common.INcFileWriter;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgBase;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfBase;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.KmallFile;
import fr.ifremer.globe.api.xsf.converter.kmall.xsf.KmallXsfWriter;
import fr.ifremer.globe.api.xsf.util.DefaultLogger;
import fr.ifremer.globe.netcdf.ucar.NCException;

public class KongsbergKmAllConverter implements IConverter<KmallFile> {

	/** Logger **/
	private Logger logger = DefaultLogger.get();

	@Override
	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	@Override
	public Logger getLogger() {
		return logger;
	}

	@Override
	public boolean accept(String inputFilePath) {
		return inputFilePath.endsWith(KmallFile.EXTENSION_KMALL);
	}

	@Override
	public KmallFile indexFile(String inputFilePath, boolean parseWaterColumData) throws IOException {
		return new KmallFile(inputFilePath,parseWaterColumData);
	}

	@Override
	public INcFileWriter<KmallFile, ? extends XsfBase, XsfConverterParameters> getXsfWriter() {
		return new KmallXsfWriter();
	}

	/**
	 * Converts to MBG: not yet implemented for KMALL.
	 */
	@Override
	public INcFileWriter<KmallFile, MbgBase, MbgConverterParameters> getMbgWriter() {
		return null; // not yet implemented for KMALL.
	}

	@Override
	public void convertToMbg(MbgConverterParameters params) throws IOException, NCException {
		// not yet implemented for KMALL.
		throw new IOException("This converter does not handle MBG files.");
	}

}
