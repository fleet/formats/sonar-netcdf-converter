package fr.ifremer.globe.api.xsf.converter.common.mbg;

import fr.ifremer.globe.api.xsf.converter.MbgConverterParameters;
import fr.ifremer.globe.api.xsf.converter.SounderFileConverter;
import fr.ifremer.globe.api.xsf.converter.common.sounder.ISounderFile;
import fr.ifremer.globe.netcdf.ucar.*;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.mbes.Ellipsoid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MbgBase implements AutoCloseable {
    protected static Logger logger = LoggerFactory.getLogger(MbgBase.class);

    private static final int COMMENT_ATT_LENGTH = 256;
    private static final int NAME_ATT_LENGTH = 20;
    private static final int INSTALL_PARAMERTERS_ATT_LENGTH = 1024;

    private final ISounderFile dataProxy;

    private NCFile file;

    public MbgBase(ISounderFile sumDataProxy) {
        this.dataProxy = sumDataProxy;
    }

    public void initialize(String outputfilePath) throws NCException {
        file = NCFile.createNew(NCFile.Version.netcdf4, outputfilePath).withCompressionDisabled();
        try {
            MbgBase.addCDL(file, dataProxy.getSwathCountFromDepthDatagrams(), dataProxy.getDetectionCount(),
                    dataProxy.getRxAntennaNb(), (int) dataProxy.getSVPProfileCount());
        } catch (GIOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void close() {
        if (file != null) {
            file.close();
        }
    }

    public NCVariable getVariable(String variableName) throws NCException {
        return file.getVariable(variableName);
    }

    /**
     * Writes global attributes.
     *
     * @param params
     */
    public void writeGlobalAttributes(ISounderFile dataProxy, MbgConverterParameters params) throws NCException {
        // global attributes
        file.addAttribute(MbgConstants.Name, new File(file.getName()).getName());
        file.addAttribute(MbgConstants.Version, MbgConstants.ActualVersion);
        file.addAttribute(MbgConstants.Classe, MbgConstants.MBGClasse);
        file.addAttribute(MbgConstants.Level, MbgConstants.LevelValue);
        file.addAttribute(MbgConstants.TimeReference, MbgConstants.TimeReferenceValue);
        file.addAttribute(MbgConstants.NbrHistoryRec, (short) 1);
        file.addAttribute(MbgConstants.CycleCounter, dataProxy.getSwathCountFromDepthDatagrams());

        // depth
        file.addAttribute(MbgConstants.MinDepth, dataProxy.getDepthMin());
        file.addAttribute(MbgConstants.MaxDepth, dataProxy.getDepthMax());

        // geographic bounds
        file.addAttribute(MbgConstants.NorthLatitude, dataProxy.getLatMax());
        file.addAttribute(MbgConstants.SouthLatitude, dataProxy.getLatMin());
        file.addAttribute(MbgConstants.EastLongitude, dataProxy.getLonMax());
        file.addAttribute(MbgConstants.WestLongitude, dataProxy.getLonMin());

        // date (compute from depth datagrams)
        int[] minDateTime = DateUtils.getDateTime(dataProxy.getDepthDatagramMinDate());
        file.addAttribute(MbgConstants.StartDate, (int) (minDateTime[0] + MbgConstants.TimeReferenceInt));
        file.addAttribute(MbgConstants.StartTime, minDateTime[1]);
        int[] maxDateTime = DateUtils.getDateTime(dataProxy.getDepthDatagramMaxDate());
        file.addAttribute(MbgConstants.EndDate, (int) (maxDateTime[0] + MbgConstants.TimeReferenceInt));
        file.addAttribute(MbgConstants.EndTime, maxDateTime[1]);

        // geographic representation & projection
        file.addAttribute(MbgConstants.Meridian180, "");
        file.addAttribute(MbgConstants.GeodesicSystem, createString("", NAME_ATT_LENGTH));
        file.addAttribute(MbgConstants.EllipsoidName,
                createString(Ellipsoid.WGS84.getEllipsName(), COMMENT_ATT_LENGTH));
        file.addAttribute(MbgConstants.EllipsoidA, Ellipsoid.WGS84.getHalf_A()); // 6378137
        file.addAttribute(MbgConstants.EllipsoidInvF, Ellipsoid.WGS84.flatness());
        file.addAttribute(MbgConstants.EllipsoidE2, Ellipsoid.WGS84.eccentricity2());// 0.00669437999014144
        file.addAttribute(MbgConstants.ProjType, (short) 0);
        double[] projParameterValue = {0., 0., 0., 0., 0., 0., 0., 0., 0., 0.};
        file.addAttribute(MbgConstants.ProjParameterValue, projParameterValue);
        file.addAttribute(MbgConstants.ProjParameterCode, createString("", COMMENT_ATT_LENGTH));

        // ship & sounder
        file.addAttribute(MbgConstants.Ship, createString(params.getShipName(), COMMENT_ATT_LENGTH));
        file.addAttribute(MbgConstants.Survey, createString(params.getSurveyName(), COMMENT_ATT_LENGTH));
        file.addAttribute(MbgConstants.Reference, createString(params.getReference(), COMMENT_ATT_LENGTH));
        file.addAttribute(MbgConstants.Sounder, (short) dataProxy.getMbgModelNumber());

        file.addAttribute(MbgConstants.SerialNumber, (short) dataProxy.getMbgSerialNumber());
        double[] sounderOffset = {0., 0., 0.};
        file.addAttribute(MbgConstants.SounderOffset, sounderOffset);
        file.addAttribute(MbgConstants.SounderDelay, 0d);
        double[] vruOffset = {0., 0., 0.};
        file.addAttribute(MbgConstants.VRUOffset, vruOffset);
        file.addAttribute(MbgConstants.VRUDelay, 0d);

        // antenna parameters
        file.addAttribute(MbgConstants.InstallParameters,
                createString(dataProxy.getRawInstallationParameters(), INSTALL_PARAMERTERS_ATT_LENGTH));
        double[] antennaOffset = {0., 0., 0.};
        file.addAttribute(MbgConstants.AntennaOffset, antennaOffset);
        file.addAttribute(MbgConstants.mbTxAntennaLeverArm, dataProxy.getTxAntennaLevelArm());
        file.addAttribute(MbgConstants.AntennaDelay, 0d);

        // correction flags & values
        file.addAttribute(MbgConstants.AutomaticCleaning, (char) 0);
        file.addAttribute(MbgConstants.ManualCleaning, (char) 0);
        file.addAttribute(MbgConstants.PositionCorrection, (char) 0);
        file.addAttribute(MbgConstants.BiasCorrection, (char) 0);
        file.addAttribute(MbgConstants.DraughtCorrection, (char) 0);
        file.addAttribute(MbgConstants.VelocityCorrection, (char) 0);
        file.addAttribute(MbgConstants.HeadingBias, 0d);
        file.addAttribute(MbgConstants.RollBias, 0d);
        file.addAttribute(MbgConstants.PitchBias, 0d);
        file.addAttribute(MbgConstants.HeaveBias, 0d);
        file.addAttribute(MbgConstants.AcrossAngleCorrect, dataProxy.getAcrossAngleCorrectFlag());
        file.addAttribute(MbgConstants.TideCorrection, "");
        file.addAttribute(MbgConstants.TideType, (short) 0);
        file.addAttribute(MbgConstants.TideRef, createString("", COMMENT_ATT_LENGTH));
        file.addAttribute(MbgConstants.Draught, 0d);
        file.addAttribute(MbgConstants.NavType, (short) 0);
        file.addAttribute(MbgConstants.NavRef, createString("", COMMENT_ATT_LENGTH));

        // CDI
        file.addAttribute(MbgConstants.CDI, createString(params.getCdi(), 100));
    }

    public static String createString(char value, int length) {
        StringBuilder sbString = new StringBuilder(length);
        sbString.append(value);
        for (int i = 0; i < (length - 1); i++)
            sbString.append(' ');
        return sbString.toString();
    }

    public static String createString(String value, int length) {
        StringBuilder sbString = new StringBuilder(length);
        sbString.append(value);
        for (int i = 0; i < (length - value.length()); i++)
            sbString.append(' ');
        return sbString.toString();
    }

    /**
     * Constructs the MBG file with attributes and variables
     *
     * @param _ncfile
     * @param aNumberOfPings
     * @param aNumberOfBeams
     * @param aNumberOfAntenna
     * @param aNumberOfVelocityProfiles
     * @throws GIOException
     */
    public static void addCDL(NCFile _ncfile, int aNumberOfPings, int aNumberOfBeams, int aNumberOfAntenna,
                              int aNumberOfVelocityProfiles) throws GIOException {

        InputStream cdl = MbgBase.class.getResourceAsStream("/resources/MBG.cdl");
        InputStreamReader ipsr = new InputStreamReader(cdl);
        BufferedReader br = new BufferedReader(ipsr);
        String ligne;
        boolean skipline = true;
        String name;
        String datatype;
        String attribut;
        String value;
        double dblValue = 0;
        short shortValue = 0;
        int intValue = 0;
        boolean bshort = false;
        double[] doubleArray = null;
        String[] doubleArraySplit = null;
        int valueInt;
        Map<String, NCDimension> dimensionMap = new HashMap<>();// to
        // collect
        // the
        // dimension
        // for
        // the
        // variables
        try {
            while ((ligne = br.readLine()) != null) {
                // move to ligne where dimension is
                if ((ligne.indexOf("dimensions:") > -1) && (skipline)) {
                    skipline = false;
                }
                if (!skipline) {
                    // analyser line
                    // Les lignes utiles finissent par ;
                    if (ligne.indexOf(";") > -1) {
                        // cas dimensions ont un = et pas de :
                        if (ligne.indexOf("=") > -1 && ligne.indexOf(":") == -1) {
                            name = ligne.substring(0, ligne.indexOf("=")).trim();
                            value = ligne.substring(ligne.indexOf("=") + 1, ligne.indexOf(";")).trim();
                            valueInt = 0;
                            // change value of mbCycleNbr, mbBeamNbr and
                            // mbAntennaNbr.
                            if (name.indexOf("mbHistoryRecNbr") > -1) {
                                value = String.valueOf(MbgConstants.HistoryRecNbrValue);
                            } else if (name.indexOf("mbNameLength") > -1) {
                                value = String.valueOf(MbgConstants.NameLengthValue);
                            } else if (name.indexOf("mbCommentLength") > -1) {
                                value = String.valueOf(MbgConstants.CommentLengthValue);
                            } else if (name.indexOf("mbCycleNbr") > -1) {
                                value = String.valueOf(aNumberOfPings);
                            } else if (name.indexOf("mbBeamNbr") > -1) {
                                value = String.valueOf(aNumberOfBeams);
                            } else if (name.indexOf("mbAntennaNbr") > -1) {
                                value = String.valueOf(aNumberOfAntenna);
                            } else if (name.indexOf("mbVelocityProfilNbr") > -1) {
                                value = String.valueOf(aNumberOfVelocityProfiles);
                            } else if (name.indexOf("mbLevelMaxNbr") > -1) {
                                value = value;
                            }

                            try {
                                valueInt = Integer.parseInt(value);
                                valueInt = Math.max(1, valueInt);
                            } catch (Exception e) {
                                logger.error("MBGFile dimension : " + ligne + " ( name : " + name + " value : " + value
                                        + ")");
                                throw (new GIOException("MBGFile addCDL dimension error : " + e.getMessage()));
                            }
                            if (name.length() > 0) {
                                NCDimension dim;
                                try {
                                    dim = _ncfile.addDimension(name, valueInt);
                                } catch (NCException e) {
                                    logger.error("MBGFile dimension : " + ligne + " ( name : " + name + " value : "
                                            + value + ")");
                                    throw (new GIOException("MBGFile addCDL dimension error : " + e.getMessage()));
                                }
                                // create dim
                                dimensionMap.put(name, dim);
                            }
                        }
                        // cas variables ont un ( et pas de =
                        // Variable
                        else if (ligne.indexOf("(") > -1 && ligne.indexOf("=") == -1) {
                            datatype = ligne.substring(0, ligne.indexOf(" ")).trim();
                            name = ligne.substring(ligne.indexOf(" ") + 1, ligne.indexOf("(")).trim();
                            attribut = value = ligne.substring(ligne.indexOf("(") + 1, ligne.indexOf(")")).trim();
                            // create list of dimensions
                            List<NCDimension> dims = new ArrayList<NCDimension>();
                            String[] dimItem = attribut.split(",");
                            if (dimensionMap.size() > 0) {
                                for (int i = 0; i < dimItem.length; i++) {
                                    dims.add(dimensionMap.get(dimItem[i].trim()));
                                }
                                try {
                                    _ncfile.addVariable(name, DataType.getType(datatype), dims);
                                } catch (Exception e) {
                                    logger.error("variable : " + ligne + "( datatype : " + datatype + " name : " + name
                                            + " attribut : " + attribut);
                                    throw (new GIOException("MBGFile addCDL variable error : " + e.getMessage()));

                                }
                            }
                        }
                        // cas attributs de variable ont un = et un : pas au
                        // debut
                        // Attribut de variable
                        else if (ligne.indexOf("=") > -1 && ligne.indexOf(":") > 0) {
                            // ncfile.addVariableAttribute("mbHistAutor",
                            // "type", "string");
                            name = ligne.substring(0, ligne.indexOf(":")).trim();
                            attribut = value = ligne.substring(ligne.indexOf(":") + 1, ligne.indexOf("=")).trim();
                            value = ligne.substring(ligne.indexOf("=") + 1, ligne.indexOf(";")).trim();

                            if (value.indexOf("\"") > -1) {
                                value = value.replace("\"", "");
                            } else {
                                dblValue = 0;
                                intValue = 0;
                                if (value.indexOf(".") > -1) {
                                    try {
                                        dblValue = Double.valueOf(value);
                                        value = "-double";
                                    } catch (Exception e) {
                                    }
                                } else {

                                    try {
                                        intValue = Integer.valueOf(value);
                                        value = "-int";
                                    } catch (Exception e) {
                                    }
                                }
                            }

                            try {
                                NCVariable var = _ncfile.getVariable(name);
                                if (value.indexOf("-double") == 0) {
                                    var.addAttribute(attribut, dblValue);
                                } else if (value.indexOf("-int") == 0) {
                                    var.addAttribute(attribut, intValue);
                                } else {
                                    var.addAttribute(attribut, value.replace("\"", ""));
                                }
                            } catch (Exception e) {
                                throw (new GIOException("MBGFile addCDL variable attribut error : " + e.getMessage()));
                            }
                        }
                        // cas attributs ont un = et un : au debut
                        // Attributs globaux
                        else if (ligne.indexOf("=") > -1 && ligne.indexOf(":") == 0) {
                            // ncfile.addGlobalAttribute("mbClasse", " ");
                            // ncfile.addGlobalAttribute("mbVersion", 200);
                            name = ligne.substring(1, ligne.indexOf("=")).trim();
                            attribut = ligne.substring(ligne.indexOf("=") + 1, ligne.indexOf(";")).trim();

                            if (attribut.indexOf("\"") > -1) {
                                // attribut=attribut.replace("\"", "");
                            } else if (attribut.indexOf(",") == -1) {
                                if (attribut.indexOf("s") > -1) {
                                    bshort = true;
                                    attribut = attribut.replace("s", "");
                                }
                                dblValue = 0;
                                intValue = 0;
                                if (attribut.indexOf(".") > -1) {
                                    try {
                                        dblValue = Double.valueOf(attribut);
                                        attribut = "-double";
                                    } catch (Exception e) {
                                    }
                                } else {

                                    try {
                                        intValue = Integer.valueOf(attribut);
                                        if (bshort) {
                                            attribut = "-short";
                                        } else {
                                            attribut = "-int";
                                        }
                                    } catch (Exception e) {
                                    }
                                }
                            } else {
                                // array
                                doubleArraySplit = attribut.split(",");
                                doubleArray = new double[doubleArraySplit.length];
                                attribut = "-array";
                                for (int i = 0; i < doubleArraySplit.length; i++) {
                                    dblValue = Double.valueOf(doubleArraySplit[i]);
                                    doubleArray[i] = dblValue;
                                }
                            }
                            try {
                                if (attribut.indexOf("-double") == 0) {
                                    _ncfile.addAttribute(name, dblValue);
                                } else if (attribut.indexOf("-short") == 0) {
                                    shortValue = (short) intValue;
                                    _ncfile.addAttribute(name, shortValue);
                                } else if (attribut.indexOf("-int") == 0) {
                                    _ncfile.addAttribute(name, intValue);
                                } else if (attribut.indexOf("-array") == 0) {
                                    _ncfile.addAttribute(name, doubleArray);
                                } else {
                                    // _ncfile.addGlobalAttribute(name,
                                    // attribut);
                                }

                            } catch (Exception e) {
                                throw (new GIOException("MBGFile addCDL variable attribut error : " + e.getMessage()));
                            }
                        }
                    }
                }
            } // end while
            br.close();
        } catch (GIOException | IOException e) {
            throw (new GIOException("MBGFile addCDL error : " + e.getMessage()));
        }
    }

    /**
     * Initializes some MBG variables
     */
    public void fillVariableWithDefaultValues(ISounderFile dataProxy) throws NCException {

        // variables initialized at 0
        file.getVariable(MbgConstants.TransmitBeamwidth).setShortFillValue((short) 0);
        file.getVariable(MbgConstants.TransmitPulseLength).setShortFillValue((short) 0);
        file.getVariable(MbgConstants.ParamMinimumDepth).setShortFillValue((short) 0);
        file.getVariable(MbgConstants.ParamMaximumDepth).setShortFillValue((short) 0);
        file.getVariable(MbgConstants.AbsorptionCoefficient).setShortFillValue((short) 0);
        file.getVariable(MbgConstants.MaxPortWidth).setShortFillValue((short) 0);
        file.getVariable(MbgConstants.MaxStarboardWidth).setShortFillValue((short) 0);
        file.getVariable(MbgConstants.DurotongSpeed).setShortFillValue((short) 0);
        file.getVariable(MbgConstants.AlongSlope).setShortFillValue((short) 0);
        file.getVariable(MbgConstants.AcrossSlope).setShortFillValue((short) 0);
        file.getVariable(MbgConstants.SoundingBias).setShortFillValue((short) 0);
        file.getVariable(MbgConstants.SonarFrequency).setIntFillValue(0);
        file.getVariable(MbgConstants.Quality).setIntFillValue(0);
        file.getVariable(MbgConstants.Tide).setShortFillValue((short) 0);
        file.getVariable(MbgConstants.DyDraught).setShortFillValue((short) 0);
        file.getVariable(MbgConstants.VelProfilIdx).setShortFillValue((short) 0);
        file.getVariable(MbgConstants.VelProfilDate).setIntFillValue(0);
        file.getVariable(MbgConstants.VelProfilTime).setIntFillValue(0);

        file.getVariable(MbgConstants.Cycle).setIntFillValue(0);
        file.getVariable(MbgConstants.Date).setIntFillValue(2147483647);
        file.getVariable(MbgConstants.Time).setIntFillValue(-2147483648);

        file.getVariable(MbgConstants.Depth).setIntFillValue(-2147483648);
        file.getVariable(MbgConstants.VerticalDepth).setIntFillValue(0);
        file.getVariable(MbgConstants.AcrossDistance).setIntFillValue(0);
        file.getVariable(MbgConstants.AlongDistance).setIntFillValue(0);
        file.getVariable(MbgConstants.AcrossAngle).setIntFillValue(0);
        file.getVariable(MbgConstants.AlongAngle).setIntFillValue(0);
        file.getVariable(MbgConstants.Immersion).setIntFillValue(0);
        file.getVariable(MbgConstants.Range).setIntFillValue(0);
        file.getVariable(MbgConstants.SamplingRate).setIntFillValue(0);
        file.getVariable(MbgConstants.Heave).setIntFillValue(0);

        file.getVariable(MbgConstants.Heading).setIntFillValue(0);
        file.getVariable(MbgConstants.Abscissa).setDoubleFillValue(0);
        file.getVariable(MbgConstants.Ordinate).setDoubleFillValue(0);
        file.getVariable(MbgConstants.Pitch).setIntFillValue(0);
        file.getVariable(MbgConstants.Roll).setIntFillValue(0);

        file.getVariable(MbgConstants.DistanceScale).setIntFillValue(1);

        file.getVariable(MbgConstants.SoundVelocity).setShortFillValue((short) -8928);

        // flags
        file.getVariable(MbgConstants.AFlag).setShortFillValue((short) 1);
        file.getVariable(MbgConstants.BFlag).setShortFillValue((short) 2);
        file.getVariable(MbgConstants.CFlag).setShortFillValue((short) 0);

        file.getVariable(MbgConstants.CompensationLayerMode)
                .setByteFillValue(MbgConstants.COMPENSATION_LAYER_MODE_UNKNOWN);

        NCVariable mbBeam = file.getVariable(MbgConstants.Beam);
        short value = (short) (dataProxy.getDetectionCount() / dataProxy.getRxAntennaNb());
        for (int i = 0; i < dataProxy.getRxAntennaNb(); i++) {
            mbBeam.put(new long[]{i}, new long[]{1}, new short[]{value});
        }

        NCVariable mbAntenna = file.getVariable(MbgConstants.Antenna);
        for (int i = 0; i < dataProxy.getDetectionCount(); i++) {
            byte b = (byte) (i * dataProxy.getRxAntennaNb() / dataProxy.getDetectionCount());
            mbAntenna.put(new long[]{i}, new long[]{1}, new byte[]{b});
        }

        NCVariable mbHistAuthor = file.getVariable(MbgConstants.HistAutor);
        String author = SounderFileConverter.NAME + " " + SounderFileConverter.VERSION;
        for (int i = 0; i < author.length(); i++) {
            byte b = (byte) author.charAt(i);
            mbHistAuthor.put(new long[]{0, i}, new long[]{1, 1}, new byte[]{b});
        }

        int[] currentDateTime = DateUtils.getDateTime(Instant.now());

        NCVariable mbHistDate = file.getVariable(MbgConstants.HistDate);
        mbHistDate.put(new long[]{0}, new long[]{1}, new int[]{currentDateTime[0]});

        NCVariable mbHistTime = file.getVariable(MbgConstants.HistTime);
        mbHistTime.put(new long[]{0}, new long[]{1}, new int[]{currentDateTime[1]});
    }

}
