package fr.ifremer.globe.api.xsf.converter.s7k.xsf;

import static java.lang.Math.toIntExact;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.function.Function;

import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1F4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1F8;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U8;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2F4;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.BathymetryGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.BeamGroup1Grp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.BathymetryVendorSpecificGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.BeamGroup1VendorSpecificGrp;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K7058;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K7058.BeamSnippets;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KFile;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KMultipingMetadata;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KMultipingSequenceMetadata;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KSwathMetadata;
import fr.ifremer.globe.api.xsf.util.DefaultLogger;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.types.DDouble;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.api.xsf.util.types.UByte;
import fr.ifremer.globe.api.xsf.util.types.UInt;
import fr.ifremer.globe.api.xsf.util.types.ULong;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCVariable;
import fr.ifremer.globe.utils.signal.SignalUtils;

public class S7K7058Converter implements IDatagramConverter<S7KFile, XsfFromS7k> {

	private List<ValueD1> valuesD1;
	private List<ValueD1> optionalD1;
	// private ValueD2 freqV;
	private NCVariable snippetFirstNumber;
	private NCVariable snippetLastNumber;
	private NCVariable snippetDetectNumber;
	private NCVariable snippetBeamNumber;
	private NCVariable seabedImageSamples;
	private NCVariable seabedImageCenter;

	class ValueD2F4_BS extends ValueD2F4 {

		public ValueD2F4_BS(NCVariable variable) {
			super(variable, null);
		}

		public void setValue(int index, float value) {
			dataOut[index] = value;
		}
	}

	private ValueD2F4_BS detectionBackscatter;

	@Override
	public void declare(S7KFile s7kFile, XsfFromS7k xsf) throws NCException {
		this.valuesD1 = new LinkedList<>();
		this.optionalD1 = new LinkedList<>();
		BeamGroup1Grp beamGrp = xsf.getBeamGroup();
		BeamGroup1VendorSpecificGrp beamGroupVendor = xsf.getBeamVendor();
		BathymetryGrp bathyGrp = xsf.getBathyGrp();
		BathymetryVendorSpecificGrp bathyVendorGrp = xsf.getBathyVendor();

		valuesD1.add(
				new ValueD1U8(beamGrp.getPing_time(), (buffer) -> new ULong(S7K7058.getTimestampNano(buffer)), true));
		valuesD1.add(new ValueD1U4(beamGroupVendor.getPing_raw_count(),
				(buffer) -> S7K7058.getPingNumber(buffer), true));
		valuesD1.add(new ValueD1U1(xsf.getBathyGrp().getMultiping_sequence(),
				(buffer) -> new UByte((byte) S7K7058.getMultiPingSequence(buffer).getU()), true));
		valuesD1.add(
				new ValueD1U1(beamGroupVendor.getSnippet_error_flag(), (buffer) -> S7K7058.getErrorFlag(buffer), true));
		valuesD1.add(new ValueD1U4(beamGroupVendor.getSnippet_control_flag(),
				(buffer) -> S7K7058.getControlFlags(buffer), true));

		snippetFirstNumber = bathyVendorGrp.getSnippet_first_sample_number();
		snippetLastNumber = bathyVendorGrp.getSnippet_last_sample_number();
		snippetDetectNumber = bathyVendorGrp.getSnippet_bottom_detection_sample_number();
		snippetBeamNumber = bathyVendorGrp.getSnippet_beam_number();

		seabedImageCenter = bathyGrp.getSeabed_image_center();
		seabedImageSamples = bathyGrp.getSeabed_image_samples_r();

		Function<BaseDatagramBuffer, Integer> optOff = (buffer) -> toIntExact(
				S7K7058.getOptionalDataOffset(buffer).getU());
		optionalD1.add(new ValueD1F8(beamGrp.getPlatform_latitude(),
				(buffer) -> new DDouble(Math.toDegrees(S7K7058.getLatitude(buffer, optOff.apply(buffer)).get()))));
		optionalD1.add(new ValueD1F8(beamGrp.getPlatform_longitude(),
				(buffer) -> new DDouble(Math.toDegrees(S7K7058.getLongitude(buffer, optOff.apply(buffer)).get()))));
		optionalD1.add(new ValueD1F4(beamGrp.getPlatform_heading(), (buffer) -> new FFloat(
				(float) Math.toDegrees(S7K7058.getHeading(buffer, optOff.apply(buffer)).get()))));
		// done through 7000 dg
		// freqV = new ValueD2F4(grp.getTx_center_frequency(),
		// (buffer, i) -> S7K7058.getFrequency(buffer, optOff.apply(buffer)));

		// declare a backscatter variable, filled by hand
		detectionBackscatter = new ValueD2F4_BS(xsf.getBathyGrp().getDetection_backscatter_r());

	}

	@Override
	public void fillData(S7KFile s7kFile) throws IOException, NCException {
		DatagramBuffer buffer = new DatagramBuffer();
		long[] origin2D = { 0, 0 };

		for (var valueD1 : valuesD1)
			valueD1.clear();
		for (var valueD1 : optionalD1)
			valueD1.clear();

		if (s7kFile.get7058Metadata().getEntries().size() != 1) {
			throw new RuntimeException("This converter only supports one sonar");
		}
		final int detectionCount = s7kFile.getDetectionCount();
		final int maxSampleCount = s7kFile.getMaxSeabedSampleCount();

		S7KMultipingMetadata mpmd = s7kFile.get7058Metadata().getEntries().iterator().next().getValue();

		for (Entry<Long, S7KMultipingSequenceMetadata> posentry : mpmd.getEntries()) {
			for (Entry<Integer, S7KSwathMetadata> e : posentry.getValue().getEntries()) {
				DatagramReader.readDatagram(e.getValue().getPosition().getFile(), buffer,
						e.getValue().getPosition().getSeek());
				SwathIdentifier swathId = new SwathIdentifier(S7K7058.getPingNumber(buffer).getU(),
						S7K7058.getMultiPingSequence(buffer).getU());

				// get origin from swath identifier (ignore if matching index not found)
				var optSwathIndex = s7kFile.getSwathIndexer().getIndex(swathId);
				if (optSwathIndex.isEmpty()) {
					DefaultLogger.get().warn(
							"Datagram S7K7058 associated with a missing ping is ignored (number = {}, sequence = {}).",
							swathId.getRawPingNumber(), swathId.getSwathPosition());
					continue;
				}
				origin2D[0] = optSwathIndex.get();

				// allocate backscatter buffer =
				for (ValueD1 v : valuesD1) {
					v.fill(buffer, (int) origin2D[0]);
				}

				BeamSnippets[] s = S7K7058.getSnippets(buffer);

				snippetFirstNumber.putu(origin2D, new long[] { 1, s.length },
						Arrays.stream(s).mapToInt(bs -> bs.beginSample.getInt()).toArray());
				snippetDetectNumber.putu(origin2D, new long[] { 1, s.length },
						Arrays.stream(s).mapToInt(bs -> bs.bottomDetection.getInt()).toArray());
				snippetLastNumber.putu(origin2D, new long[] { 1, s.length },
						Arrays.stream(s).mapToInt(bs -> bs.endSample.getInt()).toArray());

				short[] beamNumbers = new short[s.length];
				short[] sampleBuffer = new short[maxSampleCount * detectionCount];
				short[] centerBuffer = new short[detectionCount];
				Arrays.fill(sampleBuffer, seabedImageSamples.getShortFillValue());
				Arrays.fill(centerBuffer, seabedImageCenter.getShortFillValue());

				for (int i = 0; i < s.length; i++) {
					BeamSnippets elem = s[i];
					short beamIndex = elem.beamNumber.getShort();
					beamNumbers[i] = beamIndex;
					centerBuffer[beamIndex] = (short) elem.bottomDetectionIndex;
					for (int j = 0; j < elem.snippets.length; j++) {
						// convert float sample to short with scalefactor 0.1
						sampleBuffer[beamIndex * maxSampleCount + j] = (short) (elem.snippets[j] * 10);
					}
				}
				snippetBeamNumber.putu(origin2D, new long[] { 1, s.length }, beamNumbers);
				seabedImageCenter.putu(origin2D, new long[] { 1, detectionCount }, centerBuffer);
				seabedImageSamples.put(new long[] { origin2D[0], 0, 0 },
						new long[] { 1, detectionCount, maxSampleCount }, sampleBuffer);

				float[] mean_values = new float[s.length];
				// compute backscatter_ values for each detection
				this.detectionBackscatter.clear();

				for (int i = 0; i < s.length; i++) {
					BeamSnippets values = s[i];
					double mean_value = Double.NaN;
					double sum_natural = 0;
					for (int j = 0; j < values.snippets.length; j++) {
						float vdb = values.snippets[j];
						sum_natural += SignalUtils.dBToAmplitude(vdb);

					}
					if (values.snippets.length > 0)
						mean_value = sum_natural / values.snippets.length;

					mean_values[i] = (float) SignalUtils.amplitudeTodB(mean_value);
					this.detectionBackscatter.setValue(s[i].beamNumber.getU(), mean_values[i]);

				}

				this.detectionBackscatter.write(origin2D, new long[] { 1, detectionCount });
				// optional data
				if (S7K7058.getOptionalDataOffset(buffer).getU() != 0) {
					for (ValueD1 v : optionalD1) {
						v.fill(buffer);
						v.write(new long[] { origin2D[0] });
					}
					/*
					 * freqV.fill(buffer, 0, 0); freqV.write(new long[] { origin, 0 }, new long [] {
					 * 1, 1 });
					 * 
					 */
				}
			}
		}

		for (ValueD1 v : valuesD1) {
			v.write(new long[] { 0l });
		}
	}
}
