package fr.ifremer.globe.api.xsf.converter.all.datagram.mechanicaltransducertilt;

import fr.ifremer.globe.api.xsf.converter.all.datagram.IndividualSensorMetadata;
import fr.ifremer.globe.api.xsf.util.types.UShort;


/**
 * */
public class MechanicalTransducerTiltMetadata extends IndividualSensorMetadata {

	private UShort serialNumber;

	public void setSerialNumber(UShort serialNumber) {
		this.serialNumber = serialNumber;
	}
	
	public UShort getSerialNumber() {
		return serialNumber;
	}
}