package fr.ifremer.globe.api.xsf.util.types;

public class SByte implements Comparable<SByte> {
    
    private byte v;
    
    public SByte(byte v) {
        this.v = v;
    }
    
    public byte get() {
        return v;
    }
    
    public boolean isSigned() {
        return true;
    }

    @Override
    public int compareTo(SByte o) {
    	return Byte.compare(v, o.v);
    }
}
