package fr.ifremer.globe.api.xsf.converter.s7k.datagram;

import static java.lang.Math.toIntExact;

import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7K7057Metadata;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KFile;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.DDouble;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.api.xsf.util.types.UByte;
import fr.ifremer.globe.api.xsf.util.types.UInt;
import fr.ifremer.globe.api.xsf.util.types.ULong;
import fr.ifremer.globe.api.xsf.util.types.UShort;

public class S7K7057 extends S7KMultipingDatagram {
	
	public static abstract class Samples {
		public UShort[] portBeamNumbers;
		public UShort[] starboardsBeamNumbers;
	}
	
	public static class FloatSamples extends Samples {
		public float[] portBeams;
		public float[] starboardBeams;
	}
	
	public static class DoubleSamples extends Samples {
		public double[] portBeams;
		public double[] starboardBeams;
	}
	
	@Override
	public SwathIdentifier genSwathIdentifier(S7KFile stats, DatagramBuffer buffer, long time) {
		return stats.getSwathId(getPingNumber(buffer).getU(), getMultiPingSequence(buffer).getU(), time);
	}

	@Override
	public void computeSpecificMetadata(S7KFile metadata, DatagramBuffer buffer, long type,
			DatagramPosition position, long time, SwathIdentifier swathId) {
		if (getNumberOfBeamsPerSide(buffer).getU() > 1) {
			throw new RuntimeException();
		}
		getSpecificMetadata(metadata).getOrCreateMetadataForDevice(getDeviceIdentifier(buffer).getU()).registerSwath(position, getPingNumber(buffer).getU(), getMultiPingSequence(buffer).getU(), toIntExact(getNumOfSamplesPerSide(buffer).getU()), getOptionalDataOffset(buffer).getU() != 0);
		
	}

	@Override
	public S7K7057Metadata getSpecificMetadata(S7KFile metadata) {
		return metadata.get7057Metadata();
	}

	public static ULong getSonarId(BaseDatagramBuffer buffer) {
		return TypeDecoder.read8U(buffer.byteBufferData, 0);
	}
	
	public static UInt getPingNumber(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4U(buffer.byteBufferData, 8);
	}
	
	public static UShort getMultiPingSequence(BaseDatagramBuffer buffer) {
		return TypeDecoder.read2U(buffer.byteBufferData, 12);
	}
	
	public static FFloat getBeamPosition(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 14);
	}
	
	public static UInt getControlFlags(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4U(buffer.byteBufferData, 18);
	}
	
	public static UInt getNumOfSamplesPerSide(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4U(buffer.byteBufferData, 22);
	}
	
	public static UShort getNumberOfBeamsPerSide(BaseDatagramBuffer buffer) {
		return TypeDecoder.read2U(buffer.byteBufferData, 58);
	}
	
	public static UShort getCurrentBeamNumber(BaseDatagramBuffer buffer) {
		return TypeDecoder.read2U(buffer.byteBufferData, 60);
	}
	
	public static UByte getNumberOfBytesPerSample(BaseDatagramBuffer buffer) {
		return TypeDecoder.read1U(buffer.byteBufferData, 62);
	}
	
	public static UByte getDataType(BaseDatagramBuffer buffer) {
		return TypeDecoder.read1U(buffer.byteBufferData, 63);
	}
	
	public static UByte getErrorFlags(BaseDatagramBuffer buffer) {
		return TypeDecoder.read1U(buffer.byteBufferData, 64);
	}
	
	public static FloatSamples getSamples4(BaseDatagramBuffer buffer) {
		final int numOfSamples = toIntExact(getNumOfSamplesPerSide(buffer).getU());
		FloatSamples ret = new FloatSamples(); 
		ret.portBeams = new float[numOfSamples];
		ret.starboardBeams = new float[numOfSamples];
		ret.portBeamNumbers = new UShort[numOfSamples];
		ret.starboardsBeamNumbers = new UShort[numOfSamples];
		
		for (int i = 0; i < numOfSamples; i++) {
			ret.portBeams[i] = buffer.byteBufferData.getFloat(65 + i * 4);
			ret.starboardBeams[i] = buffer.byteBufferData.getFloat(65 + numOfSamples * 4 + i * 4);
			ret.portBeamNumbers[i] = TypeDecoder.read2U(buffer.byteBufferData, 65 + numOfSamples * 8 + i * 2);
			ret.starboardsBeamNumbers[i] = TypeDecoder.read2U(buffer.byteBufferData, 65 + numOfSamples * 10 + i * 2);
		}
			
		return ret;
	}
	
	public static DoubleSamples getSamples8(BaseDatagramBuffer buffer) {
		final int numOfSamples = toIntExact(getNumOfSamplesPerSide(buffer).getU());
		DoubleSamples ret = new DoubleSamples(); 
		ret.portBeams = new double[numOfSamples];
		ret.starboardBeams = new double[numOfSamples];
		ret.portBeamNumbers = new UShort[numOfSamples];
		ret.starboardsBeamNumbers = new UShort[numOfSamples];
		
		for (int i = 0; i < numOfSamples; i++) {
			ret.portBeams[i] = buffer.byteBufferData.getDouble(65 + i * 8);
			ret.starboardBeams[i] = buffer.byteBufferData.getDouble(65 + numOfSamples * 8 + i * 8);
			ret.portBeamNumbers[i] = TypeDecoder.read2U(buffer.byteBufferData, 65 + numOfSamples * 16 + i * 2);
			ret.starboardsBeamNumbers[i] = TypeDecoder.read2U(buffer.byteBufferData, 65 + numOfSamples * 18 + i * 2);
		}
			
		return ret;
	}
	
	// optional data
	public static FFloat getFrequency(BaseDatagramBuffer buffer, int optionalDataOffset) {
		return TypeDecoder.read4F(buffer.byteBufferData, optionalDataOffset - 64);
	}
	
	public static DDouble getLatitude(BaseDatagramBuffer buffer, int optionalDataOffset) {
		return TypeDecoder.read8F(buffer.byteBufferData, optionalDataOffset - 60);
	}
	
	public static DDouble getLongitude(BaseDatagramBuffer buffer, int optionalDataOffset) {
		return TypeDecoder.read8F(buffer.byteBufferData, optionalDataOffset - 52);
	}
	
	public static FFloat getHeading(BaseDatagramBuffer buffer, int optionalDataOffset) {
		return TypeDecoder.read4F(buffer.byteBufferData, optionalDataOffset - 44);
	}
	
	public static FFloat getDepth(BaseDatagramBuffer buffer, int optionalDataOffset) {
		return TypeDecoder.read4F(buffer.byteBufferData, optionalDataOffset - 40);
	}
}
