package fr.ifremer.globe.api.xsf.converter.all.datagram.networkattitudevelocity;

import fr.ifremer.globe.api.xsf.converter.all.datagram.SensorMetadata;
import fr.ifremer.globe.api.xsf.util.ComparablePair;
import fr.ifremer.globe.api.xsf.util.types.UShort;

/**
 * */
public class NetworkAttitudeVelocityMetadata extends SensorMetadata<ComparablePair<UShort, Short>, IndividualNetworkAttitudeVelocity> {

    public NetworkAttitudeVelocityMetadata() {
        // Tells the parent class that individual sensor class
        // IndividualSensorMetadata. If for some other datagram,
        // this class is not enough, it can be extended and
        // given here as well as in the class declaration.
        super(new IndividualNetworkAttitudeVelocity());
    }

}
