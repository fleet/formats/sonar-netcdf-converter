package fr.ifremer.globe.api.xsf.converter.common.utils.values.d1;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCVariable;

/***
 * class allocating storage for 1D matrix, and giving a interface allowing to fill the data with latitude and longitude
 * values
 */
public abstract class ValueD1WithPosition {
	protected static final Logger logger = LoggerFactory.getLogger(ValueD1WithPosition.class);

	public NCVariable variable;
	/** Buffer capacity */
	public final int dataOutCapacity;
	/** Index of insertion in buffer */
	protected int dataOutPosition = 0;

	/***
	 * Constructor, allocate netcdf buffer for one variable and one ping
	 */
	protected ValueD1WithPosition(NCVariable variable) {
		this(variable, false);
		// allocate storage
	}

	/***
	 * Constructor, allocate netcdf buffer for one variable. <br>
	 * Creates a buffer of capacity equals to the variable dimension. So, {@link #write(long[])} method must be invoked
	 * once, after all invocations of {@link #fill(BaseDatagramBuffer)}
	 */
	protected ValueD1WithPosition(NCVariable variable, boolean bufferCapacityIsDimension) {
		this.variable = variable;
		dataOutCapacity = bufferCapacityIsDimension ? (int) variable.getShape().get(0).getLength() : 1;
	}

	/**
	 * Call the lambda to get one value and add it to the internal buffer
	 */
	public void fill(BaseDatagramBuffer buffer, double lat, double lon, double speed) throws NCException {
		fill(buffer, lat, lon, speed, dataOutPosition);
	}

	/**
	 * Call the lambda to get one value and set it to the internal buffer at the specified position
	 */
	public abstract void fill(BaseDatagramBuffer buffer, double lat, double lon, double speed, int position)
			throws NCException;

	/**
	 * Write the internal buffer into the netCDF file.
	 * 
	 * @param dataFile
	 * @param origin
	 * @throws IOException
	 * @throws InvalidRangeException
	 */
	public abstract void write(long[] origin) throws NCException;

	/** Reset the netCDF variable to its FillValue */
	public abstract void clear() throws NCException;

}
