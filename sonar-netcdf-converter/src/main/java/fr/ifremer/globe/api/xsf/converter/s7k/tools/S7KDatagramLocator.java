package fr.ifremer.globe.api.xsf.converter.s7k.tools;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramParser;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramReader;

public class S7KDatagramLocator {

	public static String scanDir = "/media/exchange/lsix/S7K2XSF/exampleFiles";
	
	public static void main(String[] args) throws IOException {
		Map<Long, Set<String>> mapping = new TreeMap<Long, Set<String>>();
		
		Files.walkFileTree(new File(scanDir).toPath(), new SimpleFileVisitor<Path>() {
				@Override
				public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
					if (attrs.isRegularFile() && file.toString().endsWith(".s7k")) {
						String fp = file.toString();
						try (RandomAccessFile raf = new RandomAccessFile(fp, "r")) {
							DatagramBuffer buffer = new DatagramBuffer();
							while (DatagramReader.readDatagram(raf, buffer) > 0) {
								final long dgt = DatagramParser.getDatagramType(buffer);
								if (!mapping.containsKey(dgt)) {
									mapping.put(dgt, new TreeSet<String>());
								}
								mapping.get(dgt).add(fp);
							}
						} catch (IOException e) {
							
						}
					}

					if (attrs.isDirectory() && file.toString().endsWith(".s7k")) {
						return FileVisitResult.SKIP_SUBTREE;
					}
					return FileVisitResult.CONTINUE;
				}
		});
		
		for (Map.Entry<Long, Set<String>> ent : mapping.entrySet()) {
			System.out.println(ent.getKey());
			for (String v : ent.getValue()) {
				System.out.println("\t" + v);
			}
		}
	}
}
