package fr.ifremer.globe.api.xsf.converter.all.xsf.sensors;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.all.datagram.IndividualSensorMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.all.datagram.position.Position;
import fr.ifremer.globe.api.xsf.converter.all.xsf.PlatformInterpolator;
import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagram;
import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.utils.Namer;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.*;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfFromKongsberg;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.PositionSubGroup;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.PositionSubGroupVendorSpecificGrp;
import fr.ifremer.globe.api.xsf.util.SimpleIdentifier;
import fr.ifremer.globe.api.xsf.util.types.DDouble;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.api.xsf.util.types.ULong;
import fr.ifremer.globe.api.xsf.util.types.UShort;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.maths.CoordinatesSystem;
import fr.ifremer.globe.utils.maths.Vector3D;
import fr.ifremer.globe.utils.mbes.MbesUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;

public class PositionGroup implements IDatagramConverter<AllFile, XsfFromKongsberg> {

    private final AllFile metadata;
    private final List<PositionSensorGroup> subgroups = new LinkedList<>();

    public PositionGroup(AllFile metadata) {
        this.metadata = metadata;
    }

    @Override
    public void declare(AllFile allFile, XsfFromKongsberg xsf) throws NCException {
        metadata.getPosition().forEachSensor((key, sensorMetadata) -> {
            if (sensorMetadata.getTotalNumberOfEntries() > 0) {
                subgroups.add(new PositionSensorGroup(key, sensorMetadata));
            }
        });

        for (PositionSensorGroup sensorGr : subgroups)
            sensorGr.declare(allFile, xsf);
    }

    @Override
    public void fillData(AllFile allFile) throws IOException, NCException {
        for (PositionSensorGroup sensorGr : subgroups) {
            sensorGr.fillData(allFile);
        }
    }

    private class PositionSensorGroup implements IDatagramConverter<AllFile, XsfFromKongsberg> {
        int nEntries;
        private XsfFromKongsberg xsf;
        private final List<ValueD1> values = new LinkedList<>();
        private final List<ValueD1F8WithPosition> valuesWithPosition = new LinkedList<>();
        private final IndividualSensorMetadata sensorMetadata;
        private final Integer sensorId;

        public PositionSensorGroup(Integer sensorDescriptor, IndividualSensorMetadata sensorMetadata) {
            sensorId = sensorDescriptor;
            this.sensorMetadata = sensorMetadata;
            nEntries = (int) sensorMetadata.getTotalNumberOfEntries();
        }

        @Override
        public void declare(AllFile allFile, XsfFromKongsberg xsf) throws NCException {
            this.xsf = xsf;
            PositionSubGroup positionGrp = xsf.addPositionGrp(Namer.getPositionSubGroupName(sensorId), nEntries);

            valuesWithPosition.add(new ValueD1F8WithPosition(positionGrp.getLatitude(),
                    (buffer, lat, lon, speed) -> new DDouble(lat), true));

            valuesWithPosition.add(new ValueD1F8WithPosition(positionGrp.getLongitude(),
                    (buffer, lat, lon, speed) -> new DDouble(lon), true));

            values.add(new ValueD1F4(positionGrp.getCourse_over_ground(),
                    buffer -> new FFloat(Position.getCourseOverGround(buffer.byteBufferData).getU() * 0.01f), true));

            values.add(new ValueD1F4(positionGrp.getHeading(), buffer -> new FFloat(getHeading(buffer)), true));

            values.add(new ValueD1U8(positionGrp.getTime(),
                    buffer -> new ULong(Position.getXSFEpochTime(buffer.byteBufferData)), true));

            values.add(new ValueD1F4(positionGrp.getSpeed_over_ground(),
                    buffer -> new FFloat(Position.getSpeedOverGround(buffer.byteBufferData).getU() * 0.01f), true));

            PositionSubGroupVendorSpecificGrp positionVendorSubgroup = new PositionSubGroupVendorSpecificGrp(
                    positionGrp, allFile, new HashMap<>());

            values.add(new ValueD1U2(positionVendorSubgroup.getSystem_serial_number(),
                    buffer -> Position.getSerialNumber(buffer.byteBufferData), true));

            values.add(new ValueD1U2(positionVendorSubgroup.getPosition_raw_count(),
                    buffer -> Position.getCounter(buffer.byteBufferData), true));

            values.add(new ValueD1U2(positionVendorSubgroup.getMeasure_in_fix_quality(),
                    buffer -> Position.getMeasureInFixQuality(buffer.byteBufferData), true));

            values.add(new ValueD1U2(positionVendorSubgroup.getPosition_sensor_descriptor(),
                    buffer -> Position.getSensorDescriptor(buffer.byteBufferData), true));

            values.add(new ValueD1String(positionVendorSubgroup.getData_received_from_sensor(),
                    buffer -> Position.getDataFromSensor(buffer.byteBufferData), true));

            values.add(new ValueD1S1(positionVendorSubgroup.getSensor_quality_indicator(),
                    buffer -> Position.getSensorQualityIndicator(buffer.byteBufferData), true));
        }

        @Override
        public void fillData(AllFile allFile) throws IOException, NCException {

            TreeMap<Long, short[]> attitudeDatagrams = allFile.getAttitude().getSortedActiveAttitudeDatagrams();
            Vector3D positionSensorOffset = xsf.getPositionOffset(sensorId - 1);

            for (var valueD1F8WithPosition : valuesWithPosition)
                valueD1F8WithPosition.clear();
            for (var valueD1 : values)
                valueD1.clear();

            DatagramBuffer buffer = new DatagramBuffer(metadata.getByteOrder());
            for (Entry<SimpleIdentifier, DatagramPosition> posentry : sensorMetadata.getDatagrams()) {
                final DatagramPosition pos = posentry.getValue();
                DatagramReader.readDatagramAt(pos.getFile(), buffer, pos.getSeek());

                // Get interpolated position
                long currentTime = BaseDatagram.getEpochTime(buffer.byteBufferData);
                double[] attitude = PlatformInterpolator.getAttitudeByInterpolation(currentTime, attitudeDatagrams);

                double latitude = getLatitude(buffer);
                double longitude = getLongitude(buffer);
                float heading = getHeading(buffer);
                double[] refPointLatLong = {latitude, longitude};
                if (attitude != null) {
                    double roll = attitude[0];
                    double pitch = attitude[1];
                    // Set position offset in SCS coordinates
                    Vector3D sensorPositionSCS = CoordinatesSystem.fromVCStoSCS(positionSensorOffset, roll, pitch);
                    double along = -sensorPositionSCS.getX();
                    double across = -sensorPositionSCS.getY();
                    refPointLatLong = MbesUtils.acrossAlongToWGS84LatLong(latitude, longitude, heading, across, along);
                }

                // read the data from the source file
                for (ValueD1F8WithPosition value : valuesWithPosition) {
                    value.fill(buffer, refPointLatLong[0], refPointLatLong[1], 0);
                }
                for (ValueD1 value : values) {
                    value.fill(buffer);
                }
            }

            // flush into the output file
            long[] origin = new long[]{0};
            for (ValueD1F8WithPosition value : valuesWithPosition) {
                value.write(origin);
            }
            for (ValueD1 value : values) {
                value.write(origin);
            }
        }

        private double getLatitude(BaseDatagramBuffer buffer) {
            return Position.getLatitude(buffer.byteBufferData).get() / 20000000.;
        }

        private double getLongitude(BaseDatagramBuffer buffer) {
            return Position.getLongitude(buffer.byteBufferData).get() / 10000000.;
        }

        private float getHeading(BaseDatagramBuffer buffer) {
            return Position.getHeading(buffer.byteBufferData).getU() * 0.01f;
        }
    }

}
