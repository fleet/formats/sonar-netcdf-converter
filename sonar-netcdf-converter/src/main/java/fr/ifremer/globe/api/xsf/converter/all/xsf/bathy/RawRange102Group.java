package fr.ifremer.globe.api.xsf.converter.all.xsf.bathy;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.all.datagram.qualityfactor.QualityFactor;
import fr.ifremer.globe.api.xsf.converter.all.datagram.rawrange102.RawRange102;
import fr.ifremer.globe.api.xsf.converter.all.datagram.rawrange102.RawRange102CompleteAntennaPingMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.rawrange102.RawRange102CompletePingMetadata;
import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1F4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U8;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2F4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2S1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2S2;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2U1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2U2;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfFromKongsberg;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.BathymetryGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.BeamGroup1Grp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.BathymetryVendorSpecificGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.types.transmit_t;
import fr.ifremer.globe.api.xsf.util.DefaultLogger;
import fr.ifremer.globe.api.xsf.util.ObjectBufferPool;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.api.xsf.util.types.SByte;
import fr.ifremer.globe.api.xsf.util.types.SShort;
import fr.ifremer.globe.api.xsf.util.types.UInt;
import fr.ifremer.globe.api.xsf.util.types.ULong;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.util.Pair;

public class RawRange102Group implements IDatagramConverter<AllFile, XsfFromKongsberg> {

	private List<ValueD2> swathAntennaValues = new LinkedList<>();
	private List<ValueD2> swathTxValues = new LinkedList<>();
	private List<ValueD2> swathBeamValues = new LinkedList<>();
	protected List<ValueD1> swathValues = new LinkedList<>();

	/**
	 * Compute and return an array containing the start and stop frequency
	 * 
	 */
	public FFloat[] computeFrequenciesStartAndStop(BaseDatagramBuffer buffer, int i) {
		float fq = RawRange102.getTxCenterFrequency(buffer.byteBufferData, i).getU();
		int waveForm = RawRange102.getWaveFormIdentifier(buffer.byteBufferData, i).getU(); // 0:CW, 1:FM
		float bandwidth = RawRange102.getSignalBandwidth(buffer.byteBufferData, i).getU() * 10.f;
		if (waveForm == 0) // CW
		{
			FFloat[] ret = { new FFloat(fq), new FFloat(fq) };
			return ret;
		} else {
			FFloat[] ret = { new FFloat(fq - bandwidth), new FFloat(fq + bandwidth) };
			return ret;

		}

	}

	private SByte toTransmitType(BaseDatagramBuffer buffer, int i) {
		short type = RawRange102.getWaveFormIdentifier(buffer.byteBufferData, i).getU();
		if (type == 0) {
			return new SByte(transmit_t.CW.getValue());
		} else {
			return new SByte(transmit_t.LFM.getValue());
		}
	}

	@Override
	public void declare(AllFile allFile, XsfFromKongsberg xsf) throws NCException {
		BeamGroup1Grp beamGrp = xsf.getBeamGroup();
		BathymetryGrp bathyGrp = xsf.getBathyGrp();
		BathymetryVendorSpecificGrp bathyVendorGrp = xsf.getBathy();

		// Create the swath only variables
		swathValues.add(new ValueD1U4(xsf.getBeamGroupVendorSpecific().getPing_raw_count(),
				buffer -> new UInt(RawRange102.getPingNumber(buffer.byteBufferData).getU())));

		swathValues.add(new ValueD1U8(beamGrp.getPing_time(),
				buffer -> new ULong((RawRange102.getXSFEpochTime(buffer.byteBufferData)))));

		swathAntennaValues.add(new ValueD2F4(bathyVendorGrp.getDetection_sampling_freq(),
				(buffer, a) -> new FFloat(RawRange102.getSamplingFn(buffer.byteBufferData).getU() * 0.01f)));

		swathAntennaValues.add(new ValueD2F4(xsf.getBeamGroupVendorSpecific().getRov_depth(), (buffer, a) -> {
			return new FFloat(RawRange102.getROVDepth(buffer.byteBufferData).get() * 0.01f);
		}));
		swathValues.add(new ValueD1F4(beamGrp.getSound_speed_at_transducer(),
				buffer -> new FFloat(RawRange102.getSoundSpeed(buffer.byteBufferData).getU() * 0.1f)));

		swathAntennaValues.add(new ValueD2U2(xsf.getBeamGroupVendorSpecific().getTx_sector_count(),
				(buffer, a) -> RawRange102.getNTxTransmitSectorCounter(buffer.byteBufferData)));

		swathAntennaValues.add(new ValueD2U2(xsf.getBeamGroupVendorSpecific().getBeam_count(),
				(buffer, a) -> RawRange102.getReceivedBeamCounter(buffer.byteBufferData)));

		// Declare the txSector related data

		swathTxValues.add(new ValueD2F4(xsf.getBeamGroupVendorSpecific().getRaw_tx_beam_tilt_angle(), (buffer, i) -> {
			double angleDeg = RawRange102.getTxTilt(buffer.byteBufferData, i).get() * 0.01f;
			return new FFloat((float) angleDeg);
		}));

		swathTxValues.add(new ValueD2U2(xsf.getBeamGroupVendorSpecific().getFocus_range(),
				(buffer, i) -> RawRange102.getFocusRange(buffer.byteBufferData, i)));

		swathTxValues.add(new ValueD2F4(beamGrp.getTransmit_duration_nominal(), (buffer, i) -> {
			float bdw = RawRange102.getSignalBandwidth(buffer.byteBufferData, i).getU() * 10f;
			float signal_length_nominal = 0;
			if (bdw != 0)
				signal_length_nominal = 1f / bdw;
			else
				signal_length_nominal = RawRange102.getSignalLength(buffer.byteBufferData, i).getU() * 10e-6f;
			return new FFloat(signal_length_nominal);
		}));

		swathTxValues.add(new ValueD2F4(xsf.getBeamGroupVendorSpecific().getTransmit_time_delay(),
				(buffer, i) -> new FFloat(RawRange102.getTransmitTimeOffset(buffer.byteBufferData, i).getU() * 10e-6f)));

		swathTxValues.add(new ValueD2F4(xsf.getBeamGroupVendorSpecific().getCenter_frequency(),
				(buffer, i) -> new FFloat(RawRange102.getTxCenterFrequency(buffer.byteBufferData, i).getU() * 1f)));
		swathTxValues.add(new ValueD2F4(beamGrp.getTransmit_frequency_start(),
				(buffer, i) -> computeFrequenciesStartAndStop(buffer, i)[0]));
		swathTxValues.add(new ValueD2F4(beamGrp.getTransmit_frequency_stop(),
				(buffer, i) -> computeFrequenciesStartAndStop(buffer, i)[1]));

		swathTxValues.add(new ValueD2F4(beamGrp.getTransmit_bandwidth(),
				(buffer, i) -> new FFloat(RawRange102.getSignalBandwidth(buffer.byteBufferData, i).getU() * 10f)));

		swathTxValues.add(new ValueD2U1(xsf.getBeamGroupVendorSpecific().getSignal_waveform(),
				(buffer, i) -> RawRange102.getWaveFormIdentifier(buffer.byteBufferData, i)));
		swathTxValues.add(new ValueD2S1(beamGrp.getTransmit_type(), (buffer, i) -> toTransmitType(buffer, i)));

		// declare the swath-beam variables
		/** Add variables with a ping/beam dimension */

		swathBeamValues.add(new ValueD2S2(bathyGrp.getDetection_rx_transducer_index(),
				(buffer, offset) -> new SShort((short) allFile.getInstallation()
						.getRxAntennaIndex(QualityFactor.getSerialNumber(buffer.byteBufferData)))));

		swathBeamValues.add(new ValueD2F4(bathyGrp.getDetection_beam_pointing_angle(),
				(buffer, i) -> new FFloat(RawRange102.getBeamPointingAngle(buffer.byteBufferData, i).get() * 0.01f)));

		swathBeamValues.add(new ValueD2F4(bathyGrp.getDetection_two_way_travel_time(), (buffer, i) -> {
			int r = RawRange102.getRange(buffer.byteBufferData, i).getU();
			long F = RawRange102.getSamplingFn(buffer.byteBufferData).getU();
			float twoW = r / (4.f * F * 0.01f);
			return new FFloat(twoW);
		}));

		swathBeamValues.add(new ValueD2U2(bathyVendorGrp.getDetection_range(),
				(buffer, i) -> RawRange102.getRange(buffer.byteBufferData, i)));

		swathBeamValues.add(new ValueD2U1(bathyGrp.getDetection_tx_beam(),
				(buffer, i) -> RawRange102.getTxSectorIndex(buffer.byteBufferData, i)));

		swathBeamValues.add(new ValueD2U1(bathyVendorGrp.getRawrange_quality_factor(),
				(buffer, i) -> RawRange102.getQualityFactor(buffer.byteBufferData, i)));

		swathBeamValues.add(new ValueD2F4(bathyVendorGrp.getDetection_window_length(),
				(buffer, i) -> new FFloat(RawRange102.getDetectionLength(buffer.byteBufferData, i).getU()
						/ (RawRange102.getSamplingFn(buffer.byteBufferData).getU() * 0.01f))));

	}

	@Override
	public void fillData(AllFile allFile) throws IOException, NCException {
		long[] origin = { 0, 0 };
		ObjectBufferPool<DatagramBuffer> pool = new ObjectBufferPool<>(
				() -> new DatagramBuffer(allFile.getByteOrder()));

		for (Entry<SwathIdentifier, RawRange102CompletePingMetadata> pingEntry : allFile.getRawRange102().getPings()) {
			final RawRange102CompletePingMetadata ping = pingEntry.getValue();
			// test for ping rejection
			if (!ping.isComplete(allFile.getGlobalMetadata().getSerialNumbers())
					|| !allFile.getGlobalMetadata().getSwathIds().contains(pingEntry.getKey())) {
				continue;
			}

			List<Pair<DatagramBuffer, RawRange102CompleteAntennaPingMetadata>> data = DatagramReader.read(pool,
					ping.locate());

			// get origin from swath identifier (ignore if matching index not found)
			var swathId = pingEntry.getKey();
			var optIndex = allFile.getGlobalMetadata().getSwathIds().getIndex(swathId);
			if (optIndex.isEmpty()) {
				DefaultLogger.get().warn(
						"Datagram RawRange102 associated with a missing ping is ignored (number = {}, sequence = {}).",
						swathId.getRawPingNumber(), swathId.getSwathPosition());
				continue;
			}
			origin[0] = optIndex.get();

			final int beamCount = ping.getBeamCount();
			final int ntx = ping.getNumTxSector();

			swathAntennaValues.forEach(ValueD2::clear);
			swathTxValues.forEach(ValueD2::clear);
			swathBeamValues.forEach(ValueD2::clear);
			for (Pair<DatagramBuffer, RawRange102CompleteAntennaPingMetadata> antenna : data) {

				DatagramBuffer buffer = antenna.getFirst();

				// Swath values
				for (ValueD1 v : swathValues)
					v.fill(buffer);

				// fill the rx antenna related data
				for (ValueD2 v : swathAntennaValues) {
					v.fill(buffer, 0, allFile.getInstallation()
							.getRxAntennaIndex(RawRange102.getSerialNumber(buffer.byteBufferData)));
				}

				int nTx = RawRange102.getNTxTransmitSectorCounter(buffer.byteBufferData).getU();
				// fill the tx sector related data
				for (ValueD2 v : swathTxValues) {
					for (int i = 0; i < nTx; i++) {
						v.fill(buffer, i, i);
					}
				}

				// fill beam related data
				final int beamOffset = ping.getBeamOffset(RawRange102.getSerialNumber(buffer.byteBufferData).getU());
				final int offsetInDg = RawRange102.getStartingRxOffset(buffer.byteBufferData);

				for (int i = 0; i < RawRange102.getReceivedBeamCounter(buffer.byteBufferData).getU(); i++) {
					final int currBeamOffset = RawRange102.getBeamOffset(offsetInDg, i);
					final int beamIndex = beamOffset
							+ RawRange102.getBeamIndex(buffer.byteBufferData, currBeamOffset).get();
					for (ValueD2 v : swathBeamValues) {
						v.fill(antenna.getFirst(), currBeamOffset, beamIndex);
					}
				}
			}
			long[] originD1 = new long[] { origin[0] };
			// Write in netcdf variable
			for (ValueD1 v : swathValues)
				v.write(originD1);

			for (ValueD2 v : swathAntennaValues) {
				v.write(origin, new long[] { 1, allFile.getGlobalMetadata().getSerialNumbers().size() });
			}
			for (ValueD2 v : swathTxValues) {
				v.write(origin, new long[] { 1, ntx });
			}
			for (ValueD2 v : swathBeamValues) {
				v.write(origin, new long[] { 1, beamCount });
			}

			for (Pair<DatagramBuffer, RawRange102CompleteAntennaPingMetadata> antenna : data) {
				pool.release(antenna.getFirst());
			}
		}
	}
}
