package fr.ifremer.globe.api.xsf.converter.kmall.xsf;

import fr.ifremer.globe.api.xsf.converter.all.datagram.IndividualSensorMetadata;
import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.utils.Namer;
import fr.ifremer.globe.api.xsf.converter.common.utils.PlatformInterpolator;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.*;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfFromKongsberg;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.PositionSubGroup;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.PositionSubGroupVendorSpecificGrp;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.*;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.KmallFile;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.SPOMetadata;
import fr.ifremer.globe.api.xsf.util.SimpleIdentifier;
import fr.ifremer.globe.api.xsf.util.types.DDouble;
import fr.ifremer.globe.api.xsf.util.types.ULong;
import fr.ifremer.globe.api.xsf.util.types.UShort;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.maths.CoordinatesSystem;
import fr.ifremer.globe.utils.maths.Vector3D;
import fr.ifremer.globe.utils.mbes.MbesUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;

public class SPOConverter implements IDatagramConverter<KmallFile, XsfFromKongsberg> {

    private final SPOMetadata metadata;
    private final List<PositionSensorGroup> subgroups = new LinkedList<>();

    /**
     * Constructor
     **/
    public SPOConverter(KmallFile stats) {
        this.metadata = stats.getSPOMetadata();
    }

    @Override
    public void declare(KmallFile kmAllFile, XsfFromKongsberg xsf) throws NCException, IOException {
        metadata.forEachSensor((key, sensorMetadata) -> subgroups.add(new PositionSensorGroup(key, sensorMetadata)));
        for (PositionSensorGroup sensorGr : subgroups)
            sensorGr.declare(kmAllFile, xsf);
    }

    @Override
    public void fillData(KmallFile kmAllFile) throws IOException, NCException {
        for (PositionSensorGroup sensorGr : subgroups)
            sensorGr.fillData(kmAllFile);
    }

    /**
     * Position sub group (one per sensor)
     */
    private class PositionSensorGroup implements IDatagramConverter<KmallFile, XsfFromKongsberg> {
        private final IndividualSensorMetadata sensorMetadata;
        private final UShort sensorSystemId;
        private final int nEntries;
        private final List<ValueD1> values = new LinkedList<>();
        private final List<ValueD1F8WithPosition> valuesWithPosition = new LinkedList<>();
        private XsfFromKongsberg xsf;


        public PositionSensorGroup(UShort sensorSystemId, IndividualSensorMetadata sensorMetadata) {
            this.sensorSystemId = sensorSystemId;
            this.sensorMetadata = sensorMetadata;
            this.nEntries = (int) sensorMetadata.getTotalNumberOfEntries();
        }

        @Override
        public void declare(KmallFile kmallFile, XsfFromKongsberg xsf) throws NCException, IOException {

            this.xsf = xsf;
            PositionSubGroup positionSubGrp = xsf.addPositionGrp(Namer.getPositionSubGroupName(sensorSystemId.getU()),
                    nEntries);

            // time
            values.add(new ValueD1U8(positionSubGrp.getTime(),
                    buffer -> new ULong(KmAllBaseDatagram.getEpochTimeNano(buffer.byteBufferData))));

            // latitude
            valuesWithPosition.add(new ValueD1F8WithPosition(positionSubGrp.getLatitude(),
                    (buffer, lat, lon, speed) -> new DDouble(lat)));

            // longitude
            valuesWithPosition.add(new ValueD1F8WithPosition(positionSubGrp.getLongitude(),
                    (buffer, lat, lon, speed) -> new DDouble(lon)));

            // height
            values.add(new ValueD1F4(positionSubGrp.getHeight_above_reference_ellipsoid(),
                    buffer -> SPO.getEllipsoidHeightReRefPoint_m(buffer.byteBufferData)));

            // SOG
            values.add(new ValueD1F4(positionSubGrp.getSpeed_over_ground(),
                    buffer -> SPO.getSpeedOverGround_mPerSec(buffer.byteBufferData)));

            // COG
            values.add(new ValueD1F4(positionSubGrp.getCourse_over_ground(),
                    buffer -> SPO.getCourseOverGround_deg(buffer.byteBufferData)));

            PositionSubGroupVendorSpecificGrp positionVendorSubGrp = new PositionSubGroupVendorSpecificGrp(positionSubGrp, kmallFile, new HashMap<>());

            /********************************************************************************************************
             * ** .kmall specific variables
             **********************************************************************************************************/

            values.add(new ValueD1U2(positionVendorSubGrp.getSystem_serial_number(), buffer -> SCommon.getSensorSystem(buffer.byteBufferData)));

            values.add(new ValueD1U2(positionVendorSubGrp.getSensor_status(), buffer -> SCommon.getSensorStatus(buffer.byteBufferData)));

            values.add(
                    new ValueD1F4(positionVendorSubGrp.getMeasure_in_fix_quality(), buffer -> SPO.getPosFixQuality_m(buffer.byteBufferData)));

            values.add(new ValueD1String(positionVendorSubGrp.getData_received_from_sensor(), buffer -> SPO.getDataFromSensor(buffer.byteBufferData)));

            values.add(new ValueD1U1(positionVendorSubGrp.getSensor_quality_indicator(),
                    buffer -> SPO.getSensorQualityIndicator(buffer.byteBufferData)));
        }

        @Override
        public void fillData(KmallFile kmAllFile) throws IOException, NCException {
            long[] origin = new long[]{0};

            int positionIndex = sensorSystemId.getU() - 1;
            // check if motion correction is already applied
            boolean motionCorrected = kmAllFile.getIIPMetadata().getPositions().get(positionIndex).isMotionCorrected();

            Vector3D positionSensorOffset = xsf.getPositionOffset(positionIndex);
            TreeMap<Long, float[]> attitudeDatagrams = motionCorrected ? null : kmAllFile.getSKMMetadata().getSortedActiveAttitudeDatagrams();

            DatagramBuffer buffer = new DatagramBuffer();
            for (Entry<SimpleIdentifier, DatagramPosition> posentry : sensorMetadata.getDatagrams()) {
                final DatagramPosition pos = posentry.getValue();
                DatagramReader.readDatagram(pos.getFile(), buffer, pos.getSeek());

                // Get motion corrected position
                double latitude = SPO.getCorrectedLat_deg(buffer.byteBufferData).get();
                double longitude = SPO.getCorrectedLong_deg(buffer.byteBufferData).get();
                double[] refPointLatLong = {latitude, longitude};
                if (!motionCorrected) {
                    final long currentTime = DateUtils.secondToNano(SPO.getTimeFromSensor_sec(buffer.byteBufferData).getU())
                            + SPO.getTimeFromSensor_nanosec(buffer.byteBufferData).getU();

                    double[] attitude = PlatformInterpolator.getAttitudeByInterpolation(currentTime, attitudeDatagrams);
                    double roll = attitude[0];
                    double pitch = attitude[1];
                    double heading = attitude[3];
                    // Set position offset in SCS coordinates
                    Vector3D sensorPositionSCS = CoordinatesSystem.fromVCStoSCS(positionSensorOffset, roll, pitch);
                    double along = -sensorPositionSCS.getX();
                    double across = -sensorPositionSCS.getY();
                    refPointLatLong = MbesUtils.acrossAlongToWGS84LatLong(latitude, longitude, heading, across, along);
                }
                // read the data from the source file
                for (ValueD1F8WithPosition value : valuesWithPosition) {
                    value.fill(buffer, refPointLatLong[0], refPointLatLong[1], 0);
                }
                // read the data from the source file
                for (ValueD1 value : this.values)
                    value.fill(buffer);

                // flush into the output file
                for (ValueD1 value : this.values)
                    value.write(origin);
                for (ValueD1F8WithPosition value : this.valuesWithPosition)
                    value.write(origin);

                origin[0] += 1;
            }
        }

    }

}
