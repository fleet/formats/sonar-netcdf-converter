package fr.ifremer.globe.api.xsf.converter.s7k.xsf;

import java.io.IOException;

import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.SonarGrp;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KFile;
import fr.ifremer.globe.netcdf.ucar.NCException;

public class S7K7200Converter implements IDatagramConverter<S7KFile, XsfFromS7k> {

	private SonarGrp grp;

	@Override
	public void declare(S7KFile s7kFile, XsfFromS7k xsf) throws NCException {
		this.grp = xsf.getSonarGrp();
	}

	@Override
	public void fillData(S7KFile s7kFile) throws IOException, NCException {
		DatagramBuffer buffer = new DatagramBuffer();
		DatagramPosition pos = s7kFile.get7200Metadata().getEntries().iterator().next().getValue();
		DatagramReader.readDatagram(pos.getFile(), buffer, pos.getSeek());
		grp.addAttribute("sonar_manufacturer", "Reson");
	}

}
