package fr.ifremer.globe.api.xsf.converter.s7k.datagram;

import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7K7058Metadata;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KFile;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.DDouble;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.api.xsf.util.types.UByte;
import fr.ifremer.globe.api.xsf.util.types.UInt;
import fr.ifremer.globe.api.xsf.util.types.ULong;
import fr.ifremer.globe.api.xsf.util.types.UShort;

public class S7K7058 extends S7KMultipingDatagram {
	
	public static class BeamSnippets {
		public UShort beamNumber;
		
		public UInt beginSample;
		public UInt endSample;
		public UInt bottomDetection;
		
		public float[] snippets;
		public int bottomDetectionIndex;
	}

	@Override
	public SwathIdentifier genSwathIdentifier(S7KFile stats, DatagramBuffer buffer, long time) {
		return stats.getSwathId(getPingNumber(buffer).getU(), getMultiPingSequence(buffer).getU(), time);
	}
	
	@Override
	public void computeSpecificMetadata(S7KFile metadata, DatagramBuffer buffer, long type,
			DatagramPosition position, long time, SwathIdentifier swathId) {
		getSpecificMetadata(metadata).getOrCreateMetadataForDevice(getDeviceIdentifier(buffer).getU()).registerSwath(position, getPingNumber(buffer).getU(), getMultiPingSequence(buffer).getU(), getNumberOfBeams(buffer).getU(),  getOptionalDataOffset(buffer).getU() != 0);
		metadata.get7058Metadata().addSnippets(getNumOfSnippets(buffer), getMaxSampleCount(buffer));
	}

	@Override
	public S7K7058Metadata getSpecificMetadata(S7KFile metadata) {
		return metadata.get7058Metadata();
	}

	public static ULong getSonarId(BaseDatagramBuffer buffer) {
		return TypeDecoder.read8U(buffer.byteBufferData, 0);
	}
	
	public static UInt getPingNumber(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4U(buffer.byteBufferData, 8);
	}
	
	public static UShort getMultiPingSequence(BaseDatagramBuffer buffer) {
		return TypeDecoder.read2U(buffer.byteBufferData, 12);
	}
	
	public static UShort getNumberOfBeams(BaseDatagramBuffer buffer) {
		return TypeDecoder.read2U(buffer.byteBufferData, 14);
	}
	
	public static UByte getErrorFlag(BaseDatagramBuffer buffer) {
		return TypeDecoder.read1U(buffer.byteBufferData, 16);
	}
	
	public static UInt getControlFlags(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4U(buffer.byteBufferData, 17);
	}
	
	public static BeamSnippets[] getSnippets(BaseDatagramBuffer buffer) {
		int numOfBeams = (int) getNumberOfBeams(buffer).getU();
		BeamSnippets[] ret = new BeamSnippets[numOfBeams];

		int snippetOffset = 49 + numOfBeams * 14;
		for (int i = 0; i < numOfBeams; i++) {
			BeamSnippets c = new BeamSnippets();

			c.beamNumber = TypeDecoder.read2U(buffer.byteBufferData, 49 + i * 14);
			c.beginSample = TypeDecoder.read4U(buffer.byteBufferData, 51 + i * 14);
			c.bottomDetection = TypeDecoder.read4U(buffer.byteBufferData, 55 + i * 14);
			c.endSample = TypeDecoder.read4U(buffer.byteBufferData, 59 + i * 14);
			int numOfSnipps = (int) (c.endSample.getU() - c.beginSample.getU() + 1);
			c.snippets = new float[numOfSnipps];		

			for (int j = 0; j < numOfSnipps; j++) {
				c.snippets[j] = buffer.byteBufferData.getFloat(snippetOffset);
				snippetOffset = snippetOffset + 4;
			}

			c.bottomDetectionIndex = (int) (c.bottomDetection.getU() - c.beginSample.getU());

			ret[i] = c;
		}
		
		return ret;
	}
	
	/**
	 * Computes the number of snippets in the datagram, without decoding everything.
	 * 
	 * This method is meant to be more efficient than using the full getSnuppets during
	 * indexation phase.
	 * 
	 * @param buffer
	 * @return
	 */
	public static int getNumOfSnippets(BaseDatagramBuffer buffer) {
		final int numOfBeams = (int) getNumberOfBeams(buffer).getU();
		final int snippetOffset = 49 + numOfBeams * 14;
		final int optionalOffset = (int) getOptionalDataOffset(buffer).getU();
		if (optionalOffset == 0) {
			
			//TODO fix it, this is wrong because of checksum
			return (buffer.byteBufferData.capacity() - snippetOffset - 4 /* offset at the end */) / 4;
		} else {
			return (optionalOffset - 64 - snippetOffset) / 4;
		}
	}

	/**
	 * Computes the max number of samples by beam in the datagram.
	 * 
	 * @param buffer
	 * @return
	 */
	public static int getMaxSampleCount(BaseDatagramBuffer buffer) {
		int maxSampleCount = 0;
		int numOfBeams = getNumberOfBeams(buffer).getU();
		for (int i = 0; i < numOfBeams; i++) {
			UInt beginSample = TypeDecoder.read4U(buffer.byteBufferData, 51 + i * 14);
			UInt endSample = TypeDecoder.read4U(buffer.byteBufferData, 59 + i * 14);
			int numOfSnipps = (int) (endSample.getU() - beginSample.getU() + 1);
			maxSampleCount = Math.max(maxSampleCount, numOfSnipps);			
		}
		return maxSampleCount;
	}

	//
	// Optional data
	//
	
	public static class BeamOptionalData {
		public float alongTrackDistance;
		public float acrossTrackDistance;
		public UInt centerCenterNumber;
	}
	
	public static FFloat getFrequency(BaseDatagramBuffer buffer, int optionnalOffset) {
		return TypeDecoder.read4F(buffer.byteBufferData, optionnalOffset - 64);
	}
	
	public static DDouble getLatitude(BaseDatagramBuffer buffer, int optionalOffset) {
		return TypeDecoder.read8F(buffer.byteBufferData, optionalOffset - 60);
	}
	
	public static DDouble getLongitude(BaseDatagramBuffer buffer, int optionalOffset) {
		return TypeDecoder.read8F(buffer.byteBufferData, optionalOffset - 52);
	}
	
	public static FFloat getHeading(BaseDatagramBuffer buffer, int optionalOffset) {
		return TypeDecoder.read4F(buffer.byteBufferData, optionalOffset - 44);
	}
	
	public static BeamOptionalData[] getBeamOptionalData(BaseDatagramBuffer buffer, int optionalOffset) {
		final int numOfBeams = (int) getNumberOfBeams(buffer).getU();
		BeamOptionalData[] ret = new BeamOptionalData[numOfBeams];
		
		for (int i = 0; i < numOfBeams; i++) {
			BeamOptionalData v = new BeamOptionalData();
			v.alongTrackDistance = buffer.byteBufferData.getFloat(optionalOffset - 40 + 12 * i);
			v.acrossTrackDistance = buffer.byteBufferData.getFloat(optionalOffset - 36 + 12 * i);
			v.centerCenterNumber = TypeDecoder.read4U(buffer.byteBufferData, optionalOffset - 32 + 12 * i);
			ret[i] = v;
		}
		
		return ret;
	}
}
