package fr.ifremer.globe.api.xsf.converter.common.xsf;

import java.io.IOException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Map.Entry;

import org.apache.commons.io.FilenameUtils;

import fr.ifremer.globe.api.xsf.converter.SounderFileConverter;
import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.sounder.IKongsbergFile;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.BathymetryGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.ProvenanceGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.RootGrp;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.SwathIndexContainer;
import fr.ifremer.globe.netcdf.ucar.NCException;

/**
 * Handle data transverse to all or several groups
 * 
 */
public class TransverseGroup<T extends IKongsbergFile> implements IDatagramConverter<T, XsfFromKongsberg> {

	private T sounderFile;
	private XsfFromKongsberg xsf;
	private SwathIndexContainer indexes;
	private BathymetryGrp bathyGrp;

	public TransverseGroup(SwathIndexContainer indexes) {
		this.indexes = indexes;
	}

	@Override
	public void declare(T sounderFile, XsfFromKongsberg xsf) throws NCException {
		this.sounderFile = sounderFile;
		this.xsf = xsf;
		bathyGrp = xsf.getBathyGrp();
		xsf.getTideGrp();
	}

	@Override
	public void fillData(T sounderFile) throws IOException, NCException {
		fillPingCounter();
		fillStatusDetail();
		fillProvenance();
		fillSampleTimeOffset();
		fillProcessingStatus();
	}

	private void fillProcessingStatus() throws NCException {
		xsf.getRootGrp().addAttribute(RootGrp.ATT_PROCESSING_STATUS, "");
	}

	private void fillProvenance() throws NCException {
		String software = SounderFileConverter.NAME + " " + SounderFileConverter.VERSION + "/Ifremer";
		String version = SounderFileConverter.VERSION;
		String time = Instant.now().truncatedTo(ChronoUnit.SECONDS).toString();
		String filename = FilenameUtils.getName(sounderFile.getFilePath());
		
		//software attributes
		xsf.getProvenanceGrp().addAttribute(ProvenanceGrp.ATT_CONVERSION_SOFTWARE_NAME, software);
		xsf.getProvenanceGrp().addAttribute(ProvenanceGrp.ATT_CONVERSION_SOFTWARE_VERSION, version);
		xsf.getProvenanceGrp().addAttribute(ProvenanceGrp.ATT_CONVERSION_TIME, time);
		
		//filenames
		long[] origin = { 0, };
		long[] count = { 1 };
		String[] filenames = {filename};
		xsf.getProvenanceGrp().getSource_filenames().put(origin, count, filenames);
	
		// history
		String[] history = { time + //
				" Converted from " + filename + //
				" with " + software };
		xsf.getProvenanceGrp().addAttributeTextArray(ProvenanceGrp.ATT_HISTORY, history);
	}

	private void fillStatusDetail() throws NCException {
		{
			// fill for WC
			if (sounderFile.getSwathCount() > 0) {
				long[] origin = { 0, };
				long[] count = { sounderFile.getSwathCount() };
				byte[] values = new byte[sounderFile.getSwathCount()];
				Arrays.fill(values, (byte) 1); // fill with invalid values
				xsf.getBeamGroupVendorSpecific().getPing_validity().put(origin, count, values);
			}
		}
		{
			// fill for bathymetry
			if (sounderFile.getDetectionCount() > 0) {

				long[] origin = { 0, 0 };
				long[] count = { 1, sounderFile.getDetectionCount() };
				byte[] values = new byte[sounderFile.getDetectionCount()];
				byte[] values_status = new byte[sounderFile.getDetectionCount()];
				Arrays.fill(values, (byte) 0);
				Arrays.fill(values_status, (byte) 4); // fill with invalid values
				// for .all files, sector ids are the same as sector indexes
				for (int swath = 0; swath < sounderFile.getSwathCount(); swath++) {
					origin[0] = swath;
					bathyGrp.getStatus_detail().put(origin, count, values);
					bathyGrp.getStatus().put(origin, count, values_status);
				}
			}
		}

	}

	private void fillSampleTimeOffset() throws NCException {

		for (int index = 0; index < sounderFile.getSwathCount(); index++) {

			long[] s = { index, 0 };
			long[] c = { 1, sounderFile.getTxSectorCount() };
			float[] defaultValues = new float[sounderFile.getTxSectorCount()];
			Arrays.fill(defaultValues, 0);
			xsf.getBeamGroup().getSample_time_offset().put(s, c, defaultValues);

		}
	}

	private void fillPingCounter() throws NCException {
		long[] origin = { 0 };
		long[] count = { 1 };
		long[] values = new long[1];
		for (Entry<SwathIdentifier, Integer> entry : indexes.getEntries()) {
			int index = entry.getValue();
			SwathIdentifier id = entry.getKey();
			origin[0] = index;
			values[0] = id.getRawPingNumber();
			xsf.getBeamGroupVendorSpecific().getPing_raw_count().put(origin, count, values);
		}
	}
}
