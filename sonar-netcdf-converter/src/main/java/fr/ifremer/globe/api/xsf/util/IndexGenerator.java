package fr.ifremer.globe.api.xsf.util;

import fr.ifremer.globe.api.xsf.converter.common.exceptions.IndexingException;

/**
 * Interface for index generator
 * */
public interface IndexGenerator<T extends DgIdentifier> {

	/**
	 * generate indexes for each identifier registered
	 * */
	void generateIndex();

	/**
	 * return the size of the generated index 
	 * */
	long getIndexSize();

	/***
	 * retrieve the storage index for the given identifier
	 * @throws IndexingException 
	 * */
	long getIndex(T identifier) throws IndexingException;

}