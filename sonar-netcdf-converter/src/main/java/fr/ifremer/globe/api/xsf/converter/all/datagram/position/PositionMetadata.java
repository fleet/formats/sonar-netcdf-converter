package fr.ifremer.globe.api.xsf.converter.all.datagram.position;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Map.Entry;
import java.util.TreeMap;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.all.datagram.IndividualSensorMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.SensorMetadata;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.util.SimpleIdentifier;

public class PositionMetadata extends SensorMetadata<Integer, IndividualSensorMetadata> {

	private double latMin = 99999;
	private double latMax = -99999;
	private double lonMin = 99999;
	private double lonMax = -99999;

	private double SOGMin = 99999f;
	private double SOGMax = -99999f;

	private int activeId=1; //by default sensor 1 is considered as active

	public PositionMetadata() {
		// Tells the parent class that individual sensor class
		// IndividualSensorMetadata. If for some other datagram,
		// this class is not enough, it can be extended and
		// given here as well as in the class declaration.
		super(new IndividualSensorMetadata());
	}

	public void computeBoundingBoxLat(double lat) {
		latMin = Double.min(lat, latMin);
		latMax = Double.max(lat, latMax);
	}

	public void computeBoundingBoxLon(double lon) {
		lonMin = Double.min(lon, lonMin);
		lonMax = Double.max(lon, lonMax);
	}

	public double getLonMin() {
		return lonMin;
	}

	public double getLonMax() {
		return lonMax;
	}

	public double getLatMin() {
		return latMin;
	}

	public double getLatMax() {
		return latMax;
	}

	public void computeSOG(float SOG) {
		SOGMin = Double.min(SOG * 0.01, SOGMin);
		SOGMax = Double.max(SOG * 0.01, SOGMax);
	}

	public double getSOGMin() {
		return SOGMin;
	}

	public double getSOGMax() {
		return SOGMax;
	}

	public int getActiveId() {
		return activeId;
	}

	public void setActiveId(int activeId) {
		this.activeId = activeId;
	}
	
	/**
	 * Reads and sorts position datagrams from each sensor
	 */
	public TreeMap<Long, ByteBuffer> getSortedPositionDatagrams() throws IOException {
		TreeMap<Long, ByteBuffer> positionDatagrams = new TreeMap<>();

		for (IndividualSensorMetadata sensorMetadata : getSensors().values()) {
			for (Entry<SimpleIdentifier, DatagramPosition> posentry : sensorMetadata.getDatagrams()) {
				final DatagramPosition pos = posentry.getValue();

				DatagramBuffer bufferPosition = new DatagramBuffer(DatagramReader.getByteOrder(pos.getFile()));
				DatagramReader.readDatagramAt(pos.getFile(), bufferPosition, pos.getSeek());

				long positionTime = Position.getEpochTime(bufferPosition.byteBufferData);

				positionDatagrams.put(positionTime, bufferPosition.byteBufferData);
			}
		}
		return positionDatagrams;
	}

	/**
	 * Reads and sorts position datagrams from each sensor
	 */
	public TreeMap<Long, ByteBuffer> getSortedActivePositionDatagrams() throws IOException {
		TreeMap<Long, ByteBuffer> activePositionDatagrams = new TreeMap<>();
		TreeMap<Long, ByteBuffer> allPositionDatagrams = new TreeMap<>();

		for (IndividualSensorMetadata sensorMetadata : getSensors().values()) {
			for (Entry<SimpleIdentifier, DatagramPosition> posentry : sensorMetadata.getDatagrams()) {
				final DatagramPosition pos = posentry.getValue();

				DatagramBuffer bufferPosition = new DatagramBuffer(DatagramReader.getByteOrder(pos.getFile()));
				DatagramReader.readDatagramAt(pos.getFile(), bufferPosition, pos.getSeek());

				long positionTime = Position.getEpochTime(bufferPosition.byteBufferData);
				boolean isActive = Position.isActive(bufferPosition.byteBufferData);

				if (isActive) {
					activePositionDatagrams.put(positionTime, bufferPosition.byteBufferData);
				} else {
					allPositionDatagrams.put(positionTime, bufferPosition.byteBufferData);
				}
			}
		}

		// if no active position datagrams, returns all datagrams
		return activePositionDatagrams.isEmpty() ? allPositionDatagrams : activePositionDatagrams;
	}
}
