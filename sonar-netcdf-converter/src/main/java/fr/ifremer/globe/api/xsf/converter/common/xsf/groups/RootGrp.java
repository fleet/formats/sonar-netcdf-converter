package fr.ifremer.globe.api.xsf.converter.common.xsf.groups;

import java.util.HashMap;

import fr.ifremer.globe.api.xsf.converter.common.sounder.ISounderFile;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCGroup;

/** Class generated by SCGroupAdapter **/
@SuppressWarnings("unused")
public class RootGrp extends NCGroup {

	public static final String GROUP_NAME = "";

	/**group attributes names declaration*/
	public static final String ATT_CONVENTIONS="Conventions";
	public static final String ATT_DATE_CREATED="date_created";
	public static final String ATT_KEYWORDS="keywords";
	public static final String ATT_LICENSE="license";
	public static final String ATT_RIGHTS="rights";
	public static final String ATT_SONAR_CONVENTION_AUTHORITY="sonar_convention_authority";
	public static final String ATT_SONAR_CONVENTION_NAME="sonar_convention_name";
	public static final String ATT_SONAR_CONVENTION_VERSION="sonar_convention_version";
	public static final String ATT_SUMMARY="summary";
	public static final String ATT_TITLE="title";
	public static final String ATT_PROCESSING_STATUS="processing_status";
	public static final String ATT_XSF_CONVENTION_VERSION="xsf_convention_version";

	/**
	* Constructor for RootGrp 
	* @param 
	* typeDictionnary a dictonnary of type definition, if not found the default ones will be used
	* */
	public RootGrp(NCGroup parent, ISounderFile dataProxy,HashMap<String,Integer> typeDictionnary) throws NCException {
		super(GROUP_NAME,parent);

		/** Sub groups **/
		// Annotation
		// Environment
		// Platform
		// Provenance
		// Sonar

		/** Group attributes **/
		addAttribute(ATT_CONVENTIONS, "CF-1.7, SONAR-netCDF4-2.0, ACDD-1.3");
		addAttribute(ATT_DATE_CREATED, "");
		addAttribute(ATT_KEYWORDS, "");
		addAttribute(ATT_LICENSE, "");
		addAttribute(ATT_RIGHTS, "");
		addAttribute(ATT_SONAR_CONVENTION_AUTHORITY, "ICES");
		addAttribute(ATT_SONAR_CONVENTION_NAME, "SONAR-netCDF4");
		addAttribute(ATT_SONAR_CONVENTION_VERSION, "2.0");
		addAttribute(ATT_SUMMARY, "");
		addAttribute(ATT_TITLE, "");
		addAttribute(ATT_PROCESSING_STATUS, "");
		addAttribute(ATT_XSF_CONVENTION_VERSION, "0.6");
	}
}
