package fr.ifremer.globe.api.xsf.converter.all.xsf.sensors;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.all.datagram.IndividualSensorMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.height.Height;
import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1F4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U2;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U8;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfFromKongsberg;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.HeightGrp;
import fr.ifremer.globe.api.xsf.util.SimpleIdentifier;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.api.xsf.util.types.ULong;
import fr.ifremer.globe.netcdf.ucar.NCException;

public class HeightGroup implements IDatagramConverter<AllFile, XsfFromKongsberg> {

	private List<ValueD1> values;
	private IndividualSensorMetadata sensorMetadata;
	long nEntries;

	private AllFile metadata;

	public HeightGroup(AllFile metadata) {
		this.metadata = metadata;
		this.sensorMetadata = metadata.getHeight();
		this.nEntries = sensorMetadata.getTotalNumberOfEntries();
		this.values = new LinkedList<>();
	}

	@Override
	public void declare(AllFile allFile, XsfFromKongsberg xsf) throws NCException {
		HeightGrp heightGroup = xsf.getHeight();

		values.add(new ValueD1U2(heightGroup.getSensor_system(), (buffer) -> {
			return Height.getSerialNumber(buffer.byteBufferData);
		}));

		values.add(new ValueD1U8(heightGroup.getDatagram_time(),
				buffer -> new ULong(Height.getXSFEpochTime(buffer.byteBufferData) )));

		values.add(new ValueD1U2(heightGroup.getHeight_raw_count(), (buffer) -> {
			return Height.getCounter(buffer.byteBufferData);
		}));

		values.add(new ValueD1F4(heightGroup.getHeight(), (buffer) -> {
			return new FFloat(Height.getHeight(buffer.byteBufferData).get() * 0.01f);
		}));

		values.add(new ValueD1U1(heightGroup.getHeight_type(), (buffer) -> {
			return Height.getHeightType(buffer.byteBufferData);
		}));

	}

	@Override
	public void fillData(AllFile allFile) throws IOException, NCException {
		long[] origin = new long[] { 0 };

		DatagramBuffer buffer = new DatagramBuffer(metadata.getByteOrder());
		for (Entry<SimpleIdentifier, DatagramPosition> posentry : sensorMetadata.getDatagrams()) {
			final DatagramPosition pos = posentry.getValue();
			DatagramReader.readDatagramAt(pos.getFile(), buffer, pos.getSeek());

			// read the data from the source file
			for (ValueD1 value : this.values) {
				value.fill(buffer);
			}

			// flush into the output file
			for (ValueD1 value : this.values) {
				value.write(origin);
			}
			origin[0] += 1;
		}
	}

}
