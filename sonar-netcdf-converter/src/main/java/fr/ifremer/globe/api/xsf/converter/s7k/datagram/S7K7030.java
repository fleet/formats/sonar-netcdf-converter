package fr.ifremer.globe.api.xsf.converter.s7k.datagram;

import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7K7030IndividualDeviceMetadata;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7K7030Metadata;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KFile;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7K7030InstallationParameters;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.api.xsf.util.types.UShort;

public class S7K7030 extends S7KBaseDatagram {
	
	@Override
	public void computeSpecificMetadata(S7KFile metadata, DatagramBuffer buffer, long type,
			DatagramPosition position, long time) {
		S7K7030IndividualDeviceMetadata md = getSpecificMetadata(metadata).getOrCreateSensor(getDeviceIdentifier(buffer).getU(), metadata);
		md.registerSonarInstallationParameters(position, time, getInstallParameters(metadata, buffer));
	}

	@Override
	public S7K7030Metadata getSpecificMetadata(S7KFile metadata) {
		return metadata.get7030Metadata();
	}

	public static FFloat getFrequency(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 0);
	}
	
	public static String getFirmwareVersion(BaseDatagramBuffer buffer) {
		final int buffSize = TypeDecoder.read2U(buffer.byteBufferData, 4).getU();
		byte[] buff = new byte[buffSize];
		buffer.byteBufferData.position(6);
		buffer.byteBufferData.get(buff);
		buffer.byteBufferData.rewind();
		return new String(buff);
	}
	
	public static String getSoftwareVersionInfo(BaseDatagramBuffer buffer) {
		final int buffsize = TypeDecoder.read2U(buffer.byteBufferData, 134).getU();
		byte[] buff = new byte[buffsize];
		buffer.byteBufferData.position(136);
		buffer.byteBufferData.get(buff);
		buffer.byteBufferData.rewind();
		return new String(buff);
	}
	
	public static String get7KSoftwareVersionInfo(BaseDatagramBuffer buffer) {
		final int buffsize = TypeDecoder.read2U(buffer.byteBufferData, 264).getU();
		byte[] buff = new byte[buffsize];
		buffer.byteBufferData.position(266);
		buffer.byteBufferData.get(buff);
		buffer.byteBufferData.rewind();
		return new String(buff);
	}
	
	public static String getRecordProtocolVersionInfo(BaseDatagramBuffer buffer) {
		final int buffsize = TypeDecoder.read2U(buffer.byteBufferData, 394).getU();
		byte[] buff = new byte[buffsize];
		buffer.byteBufferData.position(396);
		buffer.byteBufferData.get(buff);
		buffer.byteBufferData.rewind();
		return new String(buff);
	}
	
	public static FFloat getTransmitArrayX(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 524);
	}
	
	public static FFloat getTransmitArrayY(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 528);
	}
	
	public static FFloat getTransmitArrayZ(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 532);
	}
	
	public static FFloat getTransmitArrayRoll(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 536);
	}
	
	public static FFloat getTransmitArrayPitch(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 540);
	}
	
	public static FFloat getTransmitArrayHeading(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 544);
	}
	
	public static FFloat getReceiveArrayX(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 548);
	}
	
	public static FFloat getReceiveArrayY(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 552);
	}
	
	public static FFloat getReceiveArrayZ(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 556);
	}
	
	public static FFloat getReceiveArrayRoll(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 560);
	}
	
	public static FFloat getReceiveArrayPitch(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 564);
	}
	
	public static FFloat getReceiveArrayHeading(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 568);
	}
	
	public static FFloat getMotionSensorX(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 572);
	}
	
	public static FFloat getMotionSensorY(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 576);
	}
	
	public static FFloat getMotionSensorZ(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 580);
	}
	
	public static FFloat getMotionSensorRollCalibration(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 584);
	}
	
	public static FFloat getMotionSensorPitchCalibration(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 588);
	}
	
	public static FFloat getMotionSensorHeadingCalibration(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 592);
	}
	
	public static UShort getMotionSensorTimeDelay(BaseDatagramBuffer buffer) {
		return TypeDecoder.read2U(buffer.byteBufferData, 596);
	}
	
	public static FFloat getPositionSensorX(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 598);
	}
	
	public static FFloat getPositionSensorY(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 602);
	}
	
	public static FFloat getPositionSensorZ(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 606);
	}
	
	public static UShort getPositionSensorTimeDelay(BaseDatagramBuffer buffer) {
		return TypeDecoder.read2U(buffer.byteBufferData, 610);
	}
	
	public static FFloat getWaterLineVerticalOffset(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 612);
	}
	
	public static S7K7030InstallationParameters getInstallParameters(S7KFile metadata, BaseDatagramBuffer buffer) {
		return new S7K7030InstallationParameters(metadata, buffer);
	}
}
