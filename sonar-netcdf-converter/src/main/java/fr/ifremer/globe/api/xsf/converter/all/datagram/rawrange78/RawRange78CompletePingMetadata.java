package fr.ifremer.globe.api.xsf.converter.all.datagram.rawrange78;

import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.AbstractCompletePingMetadata;

public class RawRange78CompletePingMetadata extends AbstractCompletePingMetadata<RawRange78CompleteAntennaPingMetadata> {

    public RawRange78CompletePingMetadata(int ntx, int[] serialNumbers) {
        super(ntx, serialNumbers);
    }
}
