package fr.ifremer.globe.api.xsf.converter.all.datagram;


public class DetectionBoundingBox  {

	private double latMin = 99999;
	private double latMax = -99999;
	private double lonMin = 99999;
	private double lonMax = -99999;


	public DetectionBoundingBox() {
	}

	public void computeBoundingBoxLat(double lat) {
		latMin = Double.min(lat, latMin);
		latMax = Double.max(lat, latMax);
	}

	public void computeBoundingBoxLon(double lon) {
		lonMin = Double.min(lon, lonMin);
		lonMax = Double.max(lon, lonMax);
	}

	public double getLonMin() {
		return lonMin;
	}

	public double getLonMax() {
		return lonMax;
	}

	public double getLatMin() {
		return latMin;
	}

	public double getLatMax() {
		return latMax;
	}


}
