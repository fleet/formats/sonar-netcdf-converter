package fr.ifremer.globe.api.xsf.util;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * Pool of object, not intended to be thread safe
 */
public class ObjectBufferPool<T> {

	/***
	 * object builder
	 */
	private maker<T> builder;
	/**
	 * object stack
	 */
	Deque<T> objects = new ArrayDeque<>();

	/**
	 * contructor
	 * 
	 * @param builder
	 *            an object builder}
	 */
	public ObjectBufferPool(maker<T> builder) {
		this.builder = builder;
	}

	/**
	 * interface delegating object builder
	 */
	public interface maker<T> {
		public T makeObject();
	}

	/**
	 * borrow an object from the pool
	 */
	public T borrow() {
		if (objects.isEmpty()) {
			return builder.makeObject();
		}
		return objects.pop();
	}

	/**
	 * put back an object in the pool
	 */
	public void release(T t) {
		objects.push(t);
	}

	/**
	 * empty the collection
	 */
	public void clear() {
		objects.clear();
	}
}
