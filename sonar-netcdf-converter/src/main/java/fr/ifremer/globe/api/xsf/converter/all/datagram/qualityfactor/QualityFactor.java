package fr.ifremer.globe.api.xsf.converter.all.datagram.qualityfactor;

import java.nio.ByteBuffer;

import fr.ifremer.globe.api.xsf.converter.all.datagram.PingDatagram;
import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.UByte;
import fr.ifremer.globe.api.xsf.util.types.UShort;

/**
 * Stateless QualityFactor datagram
 */
public class QualityFactor extends PingDatagram {

	@Override
	public QualityFactorMetadata getSpecificMetadata(AllFile metadata) {
		return metadata.getQualityFactor();
	}

	@Override
	protected void computeSpecificMetadata(AllFile metadata, ByteBuffer datagram, long epochTime, SwathIdentifier swathId,
			int serialNumber, DatagramPosition position) {
		QualityFactorMetadata qualityFactorMetadata = metadata.getQualityFactor();

		QualityFactorCompletePingMetadata completeping = qualityFactorMetadata.getEntry(swathId);
		if (completeping == null) {
			completeping = new QualityFactorCompletePingMetadata();
			qualityFactorMetadata.setEntry(swathId, completeping);
		}
		// compute the total number of parameters
		int beamCount = getBeamCount(datagram).getU();
		int paramsCount = beamCount * getNumberParamsPerBeam(datagram).getU();
		completeping.addAntenna(serialNumber, position, beamCount, paramsCount);

		qualityFactorMetadata.pushMaxBeamCount(completeping.getBeamCount());
		
	}

	public static UShort getBeamCount(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 16);
	}

	public static UByte getNumberParamsPerBeam(ByteBuffer datagram) {
		return TypeDecoder.read1U(datagram, 18);
	}

	public static float getSample(ByteBuffer datagram, int sampleIndex) {
		int nbParamPerBeam=getNumberParamsPerBeam(datagram).getU();
        if(sampleIndex>getBeamCount(datagram).getU())
        {
        	return Float.NaN;
        }
        return datagram.getFloat(20 + 4 * sampleIndex*nbParamPerBeam);
    }

	
    public static float[] getAllSamples(ByteBuffer datagram) {
        int nParams = getBeamCount(datagram).getU() * getNumberParamsPerBeam(datagram).getU();
        
        
        float[] ret = new float[nParams];
        for (int i = 0; i < nParams; i++) {
            ret[i] = datagram.getFloat(20 + 4 * i);
        }
        return ret;
    }

}
