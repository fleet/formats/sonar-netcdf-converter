package fr.ifremer.globe.api.xsf.converter.common.xsf.types;

public enum invalid_detection_estimation_method_t {
	normal((byte)0), interpolated((byte)1), estimated((byte)2),rejected((byte)3),no_detection_data((byte)4),unknown((byte)5);
	
	private byte value;

	public byte get() {
		return value;
	}

	invalid_detection_estimation_method_t(byte value)
	{
		this.value=value;
	}
}
