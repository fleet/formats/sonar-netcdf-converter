package fr.ifremer.globe.api.xsf.converter.all.datagram.wc;

import java.nio.ByteBuffer;

import fr.ifremer.globe.api.xsf.converter.all.datagram.PingDatagram;
import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.SByte;
import fr.ifremer.globe.api.xsf.util.types.SShort;
import fr.ifremer.globe.api.xsf.util.types.UByte;
import fr.ifremer.globe.api.xsf.util.types.UInt;
import fr.ifremer.globe.api.xsf.util.types.UShort;

public class WC extends PingDatagram {
	/** {@inheritDoc} */
	@Override
	public WCMetadata getSpecificMetadata(AllFile metadata) {
		return metadata.getWc();
	}

	@Override
	protected void computeSpecificMetadata(AllFile metadata, ByteBuffer datagram, long epochTime, SwathIdentifier swathId,
			int serialNumber, DatagramPosition position) {
		WCMetadata wcMetadata = metadata.getWc();

		UShort datagramCount = getDatagramCount(datagram);
		UShort datagramNumber = getDatagramNumber(datagram);
		UShort receivedBeamCount = getReceivedBeamCount(datagram);

		WCCompletePingMetadata completing = wcMetadata.getPing(swathId);
		if (completing == null) {
			completing = new WCCompletePingMetadata(
				getNTxTransmitSectorCount(datagram).getU(),
				metadata.getInstallation().getSerialNumbers()	
			);
			wcMetadata.addPing(swathId, completing);
		}
		WCCompleteAntennaPingMetadata antennaMD = completing.getAntenna(serialNumber);
		if (antennaMD == null) {
			antennaMD = new WCCompleteAntennaPingMetadata(datagramCount.getU());
			completing.addAntenna(serialNumber, antennaMD);
		}

		int totalSamples = 0;
		int offset = getRXOffset(datagram);
		final int nRx = getReceivedBeamCount(datagram).getU();
		for (int i = 0; i < nRx; i++) {
			totalSamples += getSampleCount(datagram, offset).getU();
			offset = getNextRxOffset(datagram, offset);
		}

		antennaMD.registerDatagram(position, datagramNumber.getU() - 1, receivedBeamCount.getU(), totalSamples);
	}

	public static UShort getDatagramCount(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 16);
	}

	public static UShort getDatagramNumber(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 18);
	}

	public static UShort getNTxTransmitSectorCount(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 20);
	}

	public static UShort getTotalBeamCount(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 22);
	}

	public static UShort getReceivedBeamCount(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 24);
	}

	public static UShort getSoundSpeed(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 26);
	}

	public static UInt getSamplingFreq(ByteBuffer datagram) {
		return TypeDecoder.read4U(datagram, 28);
	}

	public static SShort getTxTimeHeave(ByteBuffer datagram) {
		return TypeDecoder.read2S(datagram, 32);
	}

	public static UByte getTVGFunction(ByteBuffer datagram) {
		return TypeDecoder.read1U(datagram, 34);
	}

	public static SByte getTVGOffset(ByteBuffer datagram) {
		return TypeDecoder.read1S(datagram, 35);
	}

	public static UByte getScanningInfo(ByteBuffer datagram) {
		return TypeDecoder.read1U(datagram, 36);
	}

	public static SShort getTxTilt(ByteBuffer datagram, int txIndex) {
		return TypeDecoder.read2S(datagram, 40 + 6 * txIndex);
	}

	public static UShort getTxCenterFq(ByteBuffer datagram, int txIndex) {
		return TypeDecoder.read2U(datagram, 42 + 6 * txIndex);
	}

	public static UByte getTxSectorIndex(ByteBuffer datagram, int txIndex) {
		return TypeDecoder.read1U(datagram, 44 + 6 * txIndex);
	}

	/**
	 * START WC DATACOUNT
	 */

	/**
	 * return the given value
	 * 
	 * @param datagram
	 *            the datagram buffer
	 * @param startingOffset
	 *            the starting offset of the considered rxBeam
	 */
	public static SShort getBeamPointingAngle(ByteBuffer datagram, int startingOffset) {
		return TypeDecoder.read2S(datagram, startingOffset);
	}

	/**
	 * return the given value
	 * 
	 * @param datagram
	 *            the datagram buffer
	 * @param startingOffset
	 *            the starting offset of the considered rxBeam
	 */
	public static UShort getStartRange(ByteBuffer datagram, int startingOffset) {
		return TypeDecoder.read2U(datagram, startingOffset + 2);
	}

	/**
	 * return the given value
	 * 
	 * @param datagram
	 *            the datagram buffer
	 * @param startingOffset
	 *            the starting offset of the considered rxBeam
	 */
	public static UShort getSampleCount(ByteBuffer datagram, int startingOffset) {
		return TypeDecoder.read2U(datagram, startingOffset + 4);
	}

	/**
	 * return the given value
	 * 
	 * @param datagram
	 *            the datagram buffer
	 * @param startingOffset
	 *            the starting offset of the considered rxBeam
	 */
	public static UShort getDetectedRangeInSample(ByteBuffer datagram, int startingOffset) {
		return TypeDecoder.read2U(datagram, startingOffset + 6);
	}

	/**
	 * return the given value
	 * 
	 * @param datagram
	 *            the datagram buffer
	 * @param startingOffset
	 *            the starting offset of the considered rxBeam
	 */
	public static UByte getTransmitSectorNumber(ByteBuffer datagram, int startingOffset) {
		return TypeDecoder.read1U(datagram, startingOffset + 8);
	}

	/**
	 * return the given value
	 * 
	 * @param datagram
	 *            the datagram buffer
	 * @param startingOffset
	 *            the starting offset of the considered rxBeam
	 */
	public static UByte getBeamNumber(ByteBuffer datagram, int startingOffset) {
		return TypeDecoder.read1U(datagram, startingOffset + 9);
	}

	// utility functions to iterate over the rx beams
	public static int getRXOffset(ByteBuffer datagram) {
		return 40 + 6 * getNTxTransmitSectorCount(datagram).getU();
	}
	public static int getNextRxOffset(ByteBuffer datagram, int currentRXOffset) {
		return currentRXOffset + 10 + getSampleCount(datagram, currentRXOffset).getU();
	}
	public static void getSamples(ByteBuffer datagram, int startingOffset,ByteBuffer output) {
		final int nSamples = getSampleCount(datagram, startingOffset).getU();
		int currPos = datagram.position(); //retain old position
		int currLim=datagram.limit();//retain old limit
		datagram.position(startingOffset + 10); //set position to start of wc
		datagram.limit(datagram.position()+nSamples); //set limit to end of wc
		output.put(datagram);  //copy data to output
		datagram.limit(currLim); //reset limit
		datagram.position(currPos); //reset position
	}
	public static byte[] getSamples(ByteBuffer datagram, int startingOffset) {
		final int nSamples = getSampleCount(datagram, startingOffset).getU();
		byte[] buffer = new byte[nSamples];

		if (datagram.hasArray()) {
			System.arraycopy(datagram.array(), startingOffset + 10, buffer, 0, nSamples);
		} else {
			int currPos = datagram.position();
			datagram.position(startingOffset + 10);
			datagram.get(buffer, 0, nSamples);
			datagram.position(currPos);
		}

		return buffer;
	}
}
