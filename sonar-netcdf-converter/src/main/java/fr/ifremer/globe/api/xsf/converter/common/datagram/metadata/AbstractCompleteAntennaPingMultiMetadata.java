package fr.ifremer.globe.api.xsf.converter.common.datagram.metadata;

import java.util.LinkedList;
import java.util.List;

import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;

public abstract class AbstractCompleteAntennaPingMultiMetadata extends AbstractCompleteAntennaPingMetadata {

	protected int datagramCount;
	protected DatagramPosition[] positions;
	protected boolean[] valid;
	protected int[] beamCount;
	
	/** max number of abstract samples for each ping. Contains WC or Seabed sample count*/
	protected int[] maxSampleCount;

	public AbstractCompleteAntennaPingMultiMetadata(int datagramCount) {
		this.datagramCount = datagramCount;
		this.positions = new DatagramPosition[datagramCount];
		this.valid = new boolean[datagramCount];
		this.beamCount = new int[datagramCount];
		this.maxSampleCount = new int[datagramCount];

		for (int i = 0; i < datagramCount; i++) {
			this.valid[i] = false;
		}
	}

	public void registerDatagram(DatagramPosition position, int dgIndex, int beamCount, int sampleCount) {
		this.positions[dgIndex] = position;
		this.valid[dgIndex] = true;
		this.beamCount[dgIndex] = beamCount;
		this.maxSampleCount[dgIndex] = sampleCount;
	}

	@Override
	public boolean isComplete() {
		boolean res = true;
		for (boolean dg : valid) {
			res &= dg;

		}
		return res;
	}

	@Override
	public int getMaxAbstractSampleCount() {
		int maxAbstractSampleCount = 0;
		for (int i = 0; i < maxSampleCount.length; i++) {
			//sample count for missing datagrams is 0 
        	//therefore we do not need to take into account for validity field
			maxAbstractSampleCount = Math.max(maxAbstractSampleCount, maxSampleCount[i]);
		}
		return maxAbstractSampleCount;
	}
	
	@Override
	public int getBeamCount() {
		int sum = 0;
		for (int i = 0; i < beamCount.length; i++) {
			//beam count for missing datagrams is 0 
        	//therefore we do not need to take into account for validity field
			sum += beamCount[i];
		}
		return sum;
	}

	@Override
	public List<DatagramPosition> locate() {
		List<DatagramPosition> ret = new LinkedList<DatagramPosition>();
		for (int i = 0; i < positions.length; i++) {
			if(valid[i])
        	{
        		//we remove missing datagrams
        		ret.add(positions[i]);
        	}
		}
		return ret;
	}

	public int getBeamOffset(int dgIndex) {
		int off = 0;
		for (int i = 0; i < dgIndex; i++) {
			//offset for missing datagrams is 0 
        	//therefore we do not need to take into account for validity field
			off = off + beamCount[i];
		}
		return off;
	}

}
