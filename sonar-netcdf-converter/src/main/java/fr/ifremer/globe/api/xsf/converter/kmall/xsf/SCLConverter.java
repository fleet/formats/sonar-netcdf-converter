package fr.ifremer.globe.api.xsf.converter.kmall.xsf;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1F4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1S4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1String;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U2;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U8;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfFromKongsberg;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.ClockGrp;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.KmAllBaseDatagram;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.SCL;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.KmallFile;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.SCLMetadata;
import fr.ifremer.globe.api.xsf.util.SimpleIdentifier;
import fr.ifremer.globe.api.xsf.util.types.ULong;
import fr.ifremer.globe.netcdf.ucar.NCException;

public class SCLConverter implements IDatagramConverter<KmallFile, XsfFromKongsberg> {

	private List<ValueD1> valuesD1;
    private KmallFile stats;
    private SCLMetadata metadata;
	long nEntries;
   
    public SCLConverter(KmallFile stats) {
        this.stats = stats;
        this.metadata = this.stats.getSCLMetadata(); 
        this.nEntries = this.metadata.getTotalNumberOfEntries();
        this.valuesD1  = new LinkedList<>();
    }

    @Override
    public void declare(KmallFile allFile, XsfFromKongsberg xsf) throws NCException {
		ClockGrp clockGrp = xsf.getClock();

        // declare all the variables
        
        valuesD1.add(new ValueD1U8(clockGrp.getDatagram_time(), buffer -> new ULong(KmAllBaseDatagram.getEpochTimeNano(buffer.byteBufferData))));
        
		valuesD1.add(new ValueD1U2(clockGrp.getSensor_system(), buffer -> SCL.getSensorSystem(buffer.byteBufferData)));
        
		valuesD1.add(new ValueD1U2(clockGrp.getSensor_status(), buffer -> SCL.getSensorStatus(buffer.byteBufferData)));

		valuesD1.add(new ValueD1F4(clockGrp.getOffset(), buffer -> SCL.getOffset_sec(buffer.byteBufferData)));

		valuesD1.add(new ValueD1S4(clockGrp.getClock_dev_pu(), buffer -> SCL.getClockDevPU_nanosec(buffer.byteBufferData)));
		
		valuesD1.add(new ValueD1String(clockGrp.getData_received_from_sensor(), buffer -> SCL.getDataFromSensor(buffer.byteBufferData)));
    }
    
    @Override
    public void fillData(KmallFile kmAllFile) throws IOException, NCException {
        long[] origin = new long[] { 0 };

        DatagramBuffer buffer = new DatagramBuffer();
        for (Entry<SimpleIdentifier, DatagramPosition> posentry : metadata.getDatagrams()) {
            final DatagramPosition pos = posentry.getValue();
            DatagramReader.readDatagram(pos.getFile(), buffer, pos.getSeek());

            // read the data from the source file
            for (ValueD1 value: this.valuesD1) {
                value.fill(buffer);
            }

            // flush into the output file
            for (ValueD1 value : this.valuesD1) {
                value.write(origin);
            }
            origin[0] += 1;
        }
        
    }


}
