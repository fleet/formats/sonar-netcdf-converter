package fr.ifremer.globe.api.xsf.converter.common.xsf.groups;

import java.util.Arrays;

import java.util.HashMap;

import fr.ifremer.globe.api.xsf.converter.common.sounder.ISounderFile;
import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCGroup;
import fr.ifremer.globe.netcdf.ucar.NCVariable;

/** Class generated by SCGroupAdapter **/
@SuppressWarnings("unused")
public class ProvenanceGrp extends NCGroup {

	public static final String GROUP_NAME = "Provenance";

	/**group attributes names declaration*/
	public static final String ATT_CONVERSION_SOFTWARE_NAME="conversion_software_name";
	public static final String ATT_CONVERSION_SOFTWARE_VERSION="conversion_software_version";
	public static final String ATT_CONVERSION_TIME="conversion_time";
	public static final String ATT_HISTORY="history";

	/**dimensions names declaration*/
	public static final String FILENAMES_DIM_NAME="filenames";


	public static final String SOURCE_FILENAMES="source_filenames";
	private NCVariable source_filenames;
	private NCDimension dim_filenames;

	/**
	* Constructor for ProvenanceGrp 
	* @param 
	* typeDictionnary a dictonnary of type definition, if not found the default ones will be used
	* */
	public ProvenanceGrp(NCGroup parent, ISounderFile dataProxy,HashMap<String,Integer> typeDictionnary) throws NCException {
		super(GROUP_NAME,parent);

		/** Dimensions **/
		dim_filenames = declareDimension(FILENAMES_DIM_NAME, 1); //Can be of fixed or unlimited length, as appropriate.

		/** Group attributes **/
		addAttribute(ATT_CONVERSION_SOFTWARE_NAME, "");
		addAttribute(ATT_CONVERSION_SOFTWARE_VERSION, "");
		addAttribute(ATT_CONVERSION_TIME, "");
		addAttribute(ATT_HISTORY, "");
	}
	/**
	* declare a dimension is its lenght is strictly positive
	**/
	private NCDimension declareDimension(String dimName, long len) throws NCException {
		 if(len < 0) return null; //dimension is considered as not defined (optional) in this file
		 return addDimension(dimName, len);
	}

	/**
	 * @return the source_filenames
	 */
	public NCVariable getSource_filenames() throws NCException {
		if (source_filenames == null) declare_Source_filenames();
		return source_filenames;
	}


	/** Variable: source_filenames declarator**/

	private void declare_Source_filenames() throws NCException {
	
		source_filenames = addVariable(SOURCE_FILENAMES, DataType.STRING, Arrays.asList(dim_filenames));
		source_filenames.addAttribute("long_name", "Source filenames");
		source_filenames.addAttribute("comment", "Vector of datafile names that were used to generate the data in this SONAR-netCDF4 file.");
		source_filenames.addAttribute("obligation", "MA");
	}
	/**
	 * @return the dim_filenames
	 */
	public NCDimension getDim_filenames() {
		return dim_filenames;
	}

}
