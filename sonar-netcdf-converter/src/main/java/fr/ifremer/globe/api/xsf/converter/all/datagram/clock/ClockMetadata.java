package fr.ifremer.globe.api.xsf.converter.all.datagram.clock;


import fr.ifremer.globe.api.xsf.converter.all.datagram.IndividualSensorMetadata;
import fr.ifremer.globe.api.xsf.util.types.UShort;

/**
 * */
public class ClockMetadata extends IndividualSensorMetadata {

	private UShort serialNumber;

	public void setSerialNumber(UShort serialNumber) {
		this.serialNumber = serialNumber;
	}
	
	public UShort getSerialNumber() {
		return serialNumber;
	}
}