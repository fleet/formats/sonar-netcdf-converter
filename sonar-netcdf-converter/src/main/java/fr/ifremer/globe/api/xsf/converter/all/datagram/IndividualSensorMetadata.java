package fr.ifremer.globe.api.xsf.converter.all.datagram;

import java.util.Map.Entry;
import java.util.Set;

import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.DatagramMetadata;
import fr.ifremer.globe.api.xsf.util.SimpleIdGenerator;
import fr.ifremer.globe.api.xsf.util.SimpleIdentifier;

public class IndividualSensorMetadata extends DatagramMetadata<SimpleIdentifier,DatagramPosition> {

    public SimpleIdGenerator getGenerator() {
		return generator;
	}

	protected SimpleIdGenerator generator;
    protected long totalEntriesCount;
    protected int maxEntriesPerDatagram;
    
    public IndividualSensorMetadata() {
        super();
        totalEntriesCount = 0;
        maxEntriesPerDatagram = 0;
        this.generator = new SimpleIdGenerator();
    }
    
    public void addDatagram(DatagramPosition position, long rawSeqNumber, int numEntries) {
        if(this.index.put(generator.getId(rawSeqNumber), position) == null) {
        	this.totalEntriesCount += numEntries;
        	this.maxEntriesPerDatagram = Integer.max(numEntries, this.maxEntriesPerDatagram);
        }
    }
    
    public long getTotalNumberOfEntries() {
        return this.totalEntriesCount;
    }
    
    public int getMaxEntriesPerDatagram() {
        return this.maxEntriesPerDatagram;
    }
    
    public Set<Entry<SimpleIdentifier, DatagramPosition>> getDatagrams() {
        return getEntries();
    }

	
}
