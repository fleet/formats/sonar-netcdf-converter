package fr.ifremer.globe.api.xsf.converter.common.exceptions;

import java.io.IOException;

@SuppressWarnings("serial")
/**
 * Exception thrown when STX record is not good in file
 */
public class BadSTXRecord extends IOException {
	public BadSTXRecord() {
		super("BadSTXRecord, invalid datagram size detected, possible file corruption");
	}
}
