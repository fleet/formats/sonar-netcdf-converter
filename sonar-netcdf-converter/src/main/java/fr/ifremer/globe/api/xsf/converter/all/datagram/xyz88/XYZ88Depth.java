package fr.ifremer.globe.api.xsf.converter.all.datagram.xyz88;

import java.nio.ByteBuffer;

import fr.ifremer.globe.api.xsf.converter.all.datagram.PingDatagram;
import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.api.xsf.util.types.SByte;
import fr.ifremer.globe.api.xsf.util.types.SShort;
import fr.ifremer.globe.api.xsf.util.types.UByte;
import fr.ifremer.globe.api.xsf.util.types.UShort;

/**
 * Stateless XYZ88 datagram
 */
public class XYZ88Depth extends PingDatagram {

	@Override
	public XYZ88DepthMetadata getSpecificMetadata(AllFile metadata) {
		return metadata.getXyzdepth();
	}

	@Override
	protected void computeSpecificMetadata(AllFile metadata, ByteBuffer datagram, long epochTime,
			SwathIdentifier swathId, int serialNumber, DatagramPosition position) {
		XYZ88DepthMetadata xyz88Metadata = metadata.getXyzdepth();

		XYZ88DepthCompletePingMetadata completing = xyz88Metadata.getPing(swathId);
		if (completing == null) {
			completing = new XYZ88DepthCompletePingMetadata(metadata.getInstallation().getSerialNumbers());
			xyz88Metadata.addPing(swathId, completing);
		}
		completing.addAntenna(serialNumber,
				new XYZ88DepthCompleteAntennaPingMetadata(position, getBeamCount(datagram).getU(), 0));

		// Calcul de stats
		for (int i = 0; i < getBeamCount(datagram).getU(); i++) {
			int detectionInfo = XYZ88Depth.getDetectionInformation(datagram, i).getInt();
			if ((detectionInfo & 0X80) == 0) {
				xyz88Metadata.addDepth(
						(double) getDepth(datagram, i).get() + getTransmitTransducerDepth(datagram).get(), epochTime);
			}
		}
	}

	public static UShort getVesselHeading(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 16);
	}

	public static UShort getSoundSpeedAtTx(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 18);
	}

	public static FFloat getTransmitTransducerDepth(ByteBuffer datagram) {
		return TypeDecoder.read4F(datagram, 20);
	}

	public static UShort getBeamCount(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 24);
	}

	public static UShort getValidDetectionCount(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 26);

	}

	public static FFloat getSamplingFrequency(ByteBuffer datagram) {
		return TypeDecoder.read4F(datagram, 28);
	}

	public static UByte getScanningInfo(ByteBuffer datagram) {
		return TypeDecoder.read1U(datagram, 32);
	}

	// Spare : 3 Bytes
	/**
	 * depth in meter
	 */
	public static FFloat getDepth(ByteBuffer datagram, int beamIndex) {
		return TypeDecoder.read4F(datagram, 36 + beamIndex * 20);
	}

	/**
	 * accross distance in meter
	 */
	public static FFloat getAcrossDistance(ByteBuffer datagram, int beamIndex) {
		return TypeDecoder.read4F(datagram, 40 + beamIndex * 20);
	}

	/**
	 * accross distance in meter
	 */
	public static FFloat getAlongTrackDistance(ByteBuffer datagram, int beamIndex) {
		return TypeDecoder.read4F(datagram, 44 + beamIndex * 20);
	}

	public static UShort getWindowsLengthInSample(ByteBuffer datagram, int beamIndex) {
		return TypeDecoder.read2U(datagram, 48 + beamIndex * 20);
	}

	public static UByte getQF(ByteBuffer datagram, int beamIndex) {
		return TypeDecoder.read1U(datagram, 50 + beamIndex * 20);
	}

	public static SByte getBeamIncidenceAngleAdj(ByteBuffer datagram, int beamIndex) {
		return TypeDecoder.read1S(datagram, 51 + beamIndex * 20);
	}

	public static UByte getDetectionInformation(ByteBuffer datagram, int beamIndex) {
		return TypeDecoder.read1U(datagram, 52 + beamIndex * 20);
	}

	public static SByte getRTCleaningInformation(ByteBuffer datagram, int beamIndex) {
		return TypeDecoder.read1S(datagram, 53 + beamIndex * 20);
	}

	public static SShort getReflectivity(ByteBuffer datagram, int beamIndex) {
		return TypeDecoder.read2S(datagram, 54 + beamIndex * 20);
	}

}
