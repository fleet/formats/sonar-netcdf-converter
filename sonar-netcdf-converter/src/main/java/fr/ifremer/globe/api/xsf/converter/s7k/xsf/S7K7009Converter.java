package fr.ifremer.globe.api.xsf.converter.s7k.xsf;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1F4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1F8;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U8;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.BeamGroup1Grp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.BeamGroup1VendorSpecificGrp;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K7009;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KFile;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KMultipingMetadata;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KMultipingSequenceMetadata;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KSwathMetadata;
import fr.ifremer.globe.api.xsf.util.DefaultLogger;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.types.DDouble;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.api.xsf.util.types.UByte;
import fr.ifremer.globe.api.xsf.util.types.UInt;
import fr.ifremer.globe.api.xsf.util.types.ULong;
import fr.ifremer.globe.netcdf.ucar.NCException;

public class S7K7009Converter implements IDatagramConverter<S7KFile, XsfFromS7k> {

	private List<ValueD1> valuesD1;
	// private ValueD2 freqV;

	@Override
	public void declare(S7KFile s7kFile, XsfFromS7k xsf) throws NCException {
		this.valuesD1 = new LinkedList<>();
		BeamGroup1Grp beamGrp = xsf.getBeamGroup();
		BeamGroup1VendorSpecificGrp beamVendorGrp = xsf.getBeamVendor();

		valuesD1.add(new ValueD1U8(beamGrp.getPing_time(), (buffer) -> new ULong(S7K7009.getTimestampNano(buffer))));
		/*
		 * freqV = new ValueD2F4(grp.getTx_center_frequency(), (buffer, i) -> S7K7009.getFrequency(buffer));
		 */
		valuesD1.add(new ValueD1U4(beamVendorGrp.getPing_raw_count(),
				(buffer) -> S7K7009.getPingNumber(buffer)));
		valuesD1.add(new ValueD1U1(xsf.getBathyGrp().getMultiping_sequence(),
				(buffer) -> new UByte((byte) S7K7009.getMultiPingSequence(buffer).getU())));
		valuesD1.add(new ValueD1F8(beamGrp.getPlatform_latitude(),
				(buffer) -> new DDouble(Math.toDegrees(S7K7009.getLatitude(buffer).get()))));
		valuesD1.add(new ValueD1F8(beamGrp.getPlatform_longitude(),
				(buffer) -> new DDouble(Math.toDegrees(S7K7009.getLongitude(buffer).get()))));
		valuesD1.add(new ValueD1F4(beamGrp.getPlatform_heading(),
				(buffer) -> new FFloat((float) Math.toDegrees(S7K7009.getHeading(buffer).get()))));
		valuesD1.add(new ValueD1F4(beamVendorGrp.getVertical_depth_along_track_distance(),
				(buffer) -> S7K7009.getAlongTrackDistance(buffer)));
		valuesD1.add(new ValueD1F4(beamVendorGrp.getVertical_depth_across_track_distance(),
				(buffer) -> S7K7009.getAcrossTrackDistance(buffer)));
		valuesD1.add(new ValueD1F4(beamVendorGrp.getVertical_depth(), (buffer) -> S7K7009.getDepth(buffer)));

	}

	@Override
	public void fillData(S7KFile s7kFile) throws IOException, NCException {
		DatagramBuffer buffer = new DatagramBuffer();

		if (s7kFile.get7009Metadata().getEntries().size() != 1) {
			throw new RuntimeException("This converter only supports one sonar");
		}
		S7KMultipingMetadata mpmd = s7kFile.get7009Metadata().getEntries().iterator().next().getValue();

		// iterate over pings (by date)
		for (Entry<Long, S7KMultipingSequenceMetadata> posentry : mpmd.getEntries()) {
			// iterate over ping sequence
			for (Entry<Integer, S7KSwathMetadata> e : posentry.getValue().getEntries()) {
				DatagramReader.readDatagram(e.getValue().getPosition().getFile(), buffer,
						e.getValue().getPosition().getSeek());
				SwathIdentifier swathId = new SwathIdentifier(S7K7009.getPingNumber(buffer).getU(),
						S7K7009.getMultiPingSequence(buffer).getU());

				// get origin from swath identifier (ignore if matching index not found)
				var optIndex = s7kFile.getSwathIndexer().getIndex(swathId);
				if (optIndex.isEmpty()) {
					DefaultLogger.get().warn(
							"Datagram S7K7009 associated with a missing ping is ignored (number = {}, sequence = {}).",
							swathId.getRawPingNumber(), swathId.getSwathPosition());
					continue;
				}
				int origin = optIndex.get();

				for (ValueD1 v : this.valuesD1) {
					v.fill(buffer);
					v.write(new long[] { origin });
				}

				// freqV.fill(buffer, 0, 0);
				// freqV.write(new long[] { origin, 0 }, new long [] { 1, 1 });
			}
		}
	}

}
