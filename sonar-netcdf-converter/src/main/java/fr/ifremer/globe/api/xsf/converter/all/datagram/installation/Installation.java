package fr.ifremer.globe.api.xsf.converter.all.datagram.installation;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagram;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.UByte;
import fr.ifremer.globe.api.xsf.util.types.UShort;

public class Installation extends BaseDatagram {

	@Override
	public InstallationMetadata getSpecificMetadata(AllFile metadata) {
		return metadata.getInstallation();
	}

	@Override
	protected void computeSpecificMetadata(AllFile metadata, ByteBuffer datagram, byte datagramType, long epochTime,
			DatagramPosition position) {
		InstallationMetadata inst = getSpecificMetadata(metadata);
		inst.addDatagram(position);
		inst.setInstallation(getBody(datagram));
		inst.setSerialNumber(getSystemSerialNumber(datagram));
		inst.setSecondarySerialNumber(getSecondarySystemSerialNumber(datagram));
		inst.setModelNumber(getModelNumber(datagram));

		// we need to retain sensor and mru ids, because in some cases, the sensor id 2 is the only one in file but
		// sensor id 1 is defined
		if (inst.has("NSX"))
			metadata.getAttitude().getOrCreateSensor(2);
		if (inst.has("MSX"))
			metadata.getAttitude().getOrCreateSensor(1);
		if (inst.has("P1X"))
			metadata.getPosition().getOrCreateSensor(1);
		if (inst.has("P2X"))
			metadata.getPosition().getOrCreateSensor(2);
	}

	public static UByte getType(ByteBuffer datagram) {
		return TypeDecoder.read1U(datagram, 1);
	}

	public static UShort getInstallationCounter(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 12);
	}

	public static UShort getSystemSerialNumber(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 14);
	}

	public static UShort getSecondarySystemSerialNumber(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 16);
	}

	public static String getBody(ByteBuffer datagram) {
		return new String(datagram.array(), 18, datagram.limit() - 21, Charset.forName("UTF-8"));
	}
}
