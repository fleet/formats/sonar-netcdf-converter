package fr.ifremer.globe.api.xsf.converter.all.datagram.soundspeedprofile;

import java.nio.ByteBuffer;

import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagram;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.utils.DateUtil;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.UInt;
import fr.ifremer.globe.api.xsf.util.types.UShort;

/**
 * Stateless Position datagram
 */
public class SoundSpeedProfile extends BaseDatagram {

	@Override
	public SoundSpeedProfileMetadata getSpecificMetadata(AllFile metadata) {
		return metadata.getSoundSpeedProfile();
	}

	@Override
	protected void computeSpecificMetadata(AllFile metadata, ByteBuffer datagram, byte datagramType,
			long epochTime, DatagramPosition position) {
		
        final SoundSpeedProfileMetadata index = getSpecificMetadata(metadata);

        index.addDatagram(position, getEpochTime(datagram), getNumberEntries(datagram).getU());
        index.setSerialNumber(getSerialNumber(datagram));
	}

	public static UShort getCounter(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 12);
	}
	
	public static UShort getSerialNumber(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 14);
	}

	public static UInt getDateProfile(ByteBuffer datagram) {
		return TypeDecoder.read4U(datagram, 16);
	}

	public static UInt getTimeProfile(ByteBuffer datagram) {
		return TypeDecoder.read4U(datagram, 20);
	}
	public static long getProfilEpochTime(AllFile allFile, ByteBuffer datagram) {
		int sISVersion = allFile.getInstallation().getSISVersion();		
		long dateKongsbergFormat = TypeDecoder.read4U(datagram, 16).getU();
		// time since midnight in milliseconds
		long timeSinceMidnight = TypeDecoder.read4U(datagram, 20).getU();
		if (sISVersion <= 432) // SIS version Ifremer: SIS 4.3.2
			timeSinceMidnight *= 1000;// x1000 : patch SIS error
		long epochTime = DateUtil.convertKongsbergTime(dateKongsbergFormat, timeSinceMidnight);
		return epochTime;
	}

	public static UShort getNumberEntries(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 24);
	}

	public static UShort getDepthResolution(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 26);
	}

	// Read Repeat Cycle - N Entries
	public static UInt getDepth(ByteBuffer datagram, int sspIndex) {
		return TypeDecoder.read4U(datagram, 28 + sspIndex * 8);
	}
	
	public static UInt getSoundSpeedProfile(ByteBuffer datagram, int sspIndex) {
		return TypeDecoder.read4U(datagram, 32 + sspIndex * 8);
	}
	
	public static void print(ByteBuffer datagram) {
		System.out.println(getCounter(datagram));
		System.out.println(getSerialNumber(datagram));

	}

}
