package fr.ifremer.globe.api.xsf.converter.kmall.metadata;

import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.AbstractCompletePingMetadata;

public class MRZCompletePingMetadata extends AbstractCompletePingMetadata<MRZCompletePingAntennaMetadata> {

	private int numExtraDetectionClasses;
	private int extraDetectionsCount;
	private int maxExtraDetectionSeabedSampleCount;

	public MRZCompletePingMetadata(int numTxSectors, int nED, int[] serialNumbers) {
		super(numTxSectors, serialNumbers);
		this.numExtraDetectionClasses = nED;
		extraDetectionsCount = 0;
		maxExtraDetectionSeabedSampleCount = 0;
	}

	@Override
	public void computeIndex() {
		super.computeIndex();

		extraDetectionsCount = 0;
		maxExtraDetectionSeabedSampleCount = 0;
		for (MRZCompletePingAntennaMetadata antenna : antennas.values()) {
			if (antenna.isComplete()) {
				extraDetectionsCount += antenna.getExtraDetectionCount();
				maxExtraDetectionSeabedSampleCount = Math.max(maxExtraDetectionSeabedSampleCount,
						antenna.getMaxExtraDetectionSeabedSampleCount());
			}
		}
	}

	public int getNumExtraDetectionClasses() {
		return numExtraDetectionClasses;
	}

	public int getExtraDetectionsCount() {
		return extraDetectionsCount;
	}

	public int getMaxExtraDetectionSeabedSampleCount() {
		return maxExtraDetectionSeabedSampleCount;
	}

}
