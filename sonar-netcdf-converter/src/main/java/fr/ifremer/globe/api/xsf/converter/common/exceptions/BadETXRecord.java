package fr.ifremer.globe.api.xsf.converter.common.exceptions;

import java.io.IOException;

@SuppressWarnings("serial")
/**
 * Exception thrown when ETX record is not good in file
 */
public class BadETXRecord extends IOException {
	public BadETXRecord() {
		super("BadETXRecord, invalid datagram size detected, possible file corruption");
	}
}
