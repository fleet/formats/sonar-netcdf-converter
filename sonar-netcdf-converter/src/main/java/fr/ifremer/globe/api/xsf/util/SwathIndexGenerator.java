package fr.ifremer.globe.api.xsf.util;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.function.Consumer;
import java.util.stream.Stream;

/**
 * Class used to generate HSF swath_number from KmAll ping/swath numbering
 * 
 * @author lsix
 *
 */
public class SwathIndexGenerator {

	private class SwathNumberMapping {
		long time;
		private SwathIdentifier swathId;

		/**
		 * Associated time stamp
		 */
		public long getTime() {
			return time;
		}

		/**
		 * Create a new {@link SwathNumberMapping}
		 * 
		 * @param rawPingNumber the ping number given by constructor, could be rewinded to 0 if it exceed 65535
		 * @param swathPerPing number of swath per ping
		 * @param swathAlongPosition the swath identifier per ping (typically between 0 and 4 in multiswath mode)
		 * @param time the time of the mid tx pulse
		 * 
		 */
		public SwathNumberMapping(long rawPingNumber, int swathPerPing, int swathAlongPosition, long time) {
			this.swathId = new SwathIdentifier(rawPingNumber, swathAlongPosition);
			this.time = time;
		}

		/**
		 * 
		 * */
		public SwathIdentifier getSwathId() {
			return swathId;
		}
	}

	private LinkedList<SwathNumberMapping> buffer;

	/**
	 * Initialize a KmAllSwathNumberGenerator
	 */
	public SwathIndexGenerator() {
		buffer = new LinkedList<>();
	}

	/**
	 * add the swathNumber given the ping - swath information
	 * 
	 * @param rawPingNumber
	 * @param swathPerPing
	 * @param swathAlongPosition
	 * @return {@link SwathIdentifier} an id identifying the swath
	 */
	public SwathIdentifier addId(long rawPingNumber, int swathPerPing, int swathAlongPosition, long swathTime) {
		SwathNumberMapping n = new SwathNumberMapping(rawPingNumber, swathPerPing, swathAlongPosition, swathTime);
		buffer.add(n);
		return n.getSwathId();
	}

	/**
	 * Sort all {@link SwathIdentifier} inputs, sort is based on ping time
	 */
	public void sort() {
		// sort datagram on time stamp
		buffer.sort(Comparator.comparingLong(SwathNumberMapping::getTime));
	}

	public Stream<SwathIdentifier> stream() {
		return buffer.stream().map(t -> t.getSwathId());
	}

	public void forEach(Consumer<SwathIdentifier> action) {
		buffer.forEach(t -> action.accept(t.getSwathId()));
	}

}
