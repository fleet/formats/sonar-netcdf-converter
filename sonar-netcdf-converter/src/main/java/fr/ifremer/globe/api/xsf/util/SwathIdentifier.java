package fr.ifremer.globe.api.xsf.util;


/**
 * class identifying uniquely a swath, a swath is identified by a ping number and a swath per ping identifier.
 **/
public class SwathIdentifier implements DgIdentifier, Comparable<SwathIdentifier>
{

	public long getRawPingNumber() {
		return rawPingNumber;
	}
	public int getSwathPosition() {
		return swathPosition;
	}
	long rawPingNumber;
	int swathPosition;
	
	public SwathIdentifier(long rawPingNumber, int swathPosition) {
		super();
		this.rawPingNumber = rawPingNumber;
		this.swathPosition = swathPosition;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((int) rawPingNumber >> 32);
		result = prime * result + ((int) rawPingNumber & 0x0000FFFF);
		result = prime * result + swathPosition;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SwathIdentifier other = (SwathIdentifier) obj;
		if (rawPingNumber != other.rawPingNumber)
			return false;
		if (swathPosition != other.swathPosition)
			return false;
		return true;
	}
	@Override
	public int compareTo(SwathIdentifier o) {
		int result = Long.compare(this.rawPingNumber, o.rawPingNumber);
		if(result==0)
		{
			//same ping id, we compare swath position in this id (~swath number)
			result=Integer.compare(this.swathPosition, o.swathPosition);
		}
		
		return result;
	}
	
	
};