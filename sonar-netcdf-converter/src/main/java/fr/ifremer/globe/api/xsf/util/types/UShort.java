package fr.ifremer.globe.api.xsf.util.types;

public class UShort implements Comparable<UShort> {
    
    private short v;
    
    public UShort(short v) {
        this.v = v;
    }

    public short getShort() {
        return v;
    }
    
    public int getU() {
        return Short.toUnsignedInt(v);
    }
    
    public boolean isSigned() {
        return false;
    }

	@Override
	public int compareTo(UShort o) {
		return Integer.compare(getU(), o.getU());
	}
}
