package fr.ifremer.globe.api.xsf.converter.all.datagram.rawrange102;

import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.AbstractCompleteAntennaPingSingleMetadata;

public class RawRange102CompleteAntennaPingMetadata extends AbstractCompleteAntennaPingSingleMetadata {

    public RawRange102CompleteAntennaPingMetadata(DatagramPosition position, int beamCount) {
        super(position, beamCount, 0);
    }

}
