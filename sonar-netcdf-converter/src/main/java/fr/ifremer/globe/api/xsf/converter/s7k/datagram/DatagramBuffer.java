package fr.ifremer.globe.api.xsf.converter.s7k.datagram;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;

public class DatagramBuffer extends BaseDatagramBuffer {
	
	public DatagramBuffer() {
		super(ByteOrder.LITTLE_ENDIAN);
		// See spec, p12
		this.rawBufferHeader = new byte[64];
		this.byteBufferSizeHeader = ByteBuffer.wrap(rawBufferHeader);
		this.byteBufferSizeHeader.order(ByteOrder.LITTLE_ENDIAN);
	}
	
	public void allocate(int desiredCapacity) {
		if (this.rawBufferData == null || this.rawBufferData.length < desiredCapacity) {
			this.rawBufferData = new byte[desiredCapacity];
			this.byteBufferData = ByteBuffer.wrap(this.rawBufferData);
			this.byteBufferData.order(ByteOrder.LITTLE_ENDIAN);
		}
	}
}
