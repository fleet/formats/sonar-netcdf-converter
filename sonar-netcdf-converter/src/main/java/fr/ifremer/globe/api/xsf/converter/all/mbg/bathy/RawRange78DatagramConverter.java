package fr.ifremer.globe.api.xsf.converter.all.mbg.bathy;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.all.datagram.PingDatagram;
import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.all.datagram.rawrange78.RawRange78;
import fr.ifremer.globe.api.xsf.converter.all.datagram.rawrange78.RawRange78CompleteAntennaPingMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.rawrange78.RawRange78CompletePingMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.xyz88.XYZ88DepthCompleteAntennaPingMetadata;
import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagram;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgBase;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgConstants;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgUtils;
import fr.ifremer.globe.api.xsf.converter.common.utils.XLog;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2S2;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2S2WithAttitude;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2S4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2WithAttitude;
import fr.ifremer.globe.api.xsf.util.DefaultLogger;
import fr.ifremer.globe.api.xsf.util.ObjectBufferPool;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.types.SInt;
import fr.ifremer.globe.api.xsf.util.types.SShort;
import fr.ifremer.globe.api.xsf.util.types.UShort;
import fr.ifremer.globe.netcdf.api.convention.CFStandardNames;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCVariable;
import fr.ifremer.globe.netcdf.util.Pair;

/**
 * Converter for RawRange datagram
 */
public class RawRange78DatagramConverter extends AbstractRawRangeDatagramConverter {

	@Override
	public void declare(AllFile allFile, MbgBase mbg) throws NCException {

		/** range **/
		NCVariable rangeVariable = mbg.getVariable(MbgConstants.Range);
		float rangeFactor = (float) rangeVariable.getAttributeDouble(CFStandardNames.CF_SCALE_FACTOR);
		double rangeOffset = rangeVariable.getAttributeDouble(CFStandardNames.CF_ADD_OFFSET);
		swathBeamValues.add(new ValueD2S4(rangeVariable, (buffer, i) -> new SInt(
				(int) ((RawRange78.getRange(buffer.byteBufferData, i).get() / 2f - rangeOffset) / rangeFactor))));

		/** across angle **/
		NCVariable acrossBeamVariable = mbg.getVariable(MbgConstants.AcrossAngle);
		double acrossBeamFactor = acrossBeamVariable.getAttributeDouble(CFStandardNames.CF_SCALE_FACTOR);
		swathBeamWithAttitudeValues
				.add(new ValueD2S2WithAttitude(acrossBeamVariable, (buffer, beam, roll, pitch, heave) -> {
					double pointingAngle = RawRange78.getBeamPointingAngle(buffer.byteBufferData, beam).get() * 0.01d;

					if (allFile.getGlobalMetadata().getModelNumber() == 850) {
						// ME70 case, beam are roll compensated
						return (short) Math.round(pointingAngle * (1 / acrossBeamFactor));
					}

					UShort serialNumber = RawRange78.getSerialNumber(buffer.byteBufferData);
					int index = allFile.getInstallation().getRxAntennaIndex(serialNumber);
					double antennaRollOffset = allFile.getInstallation().getAntennas().get(index).getR();

					float headingOffset = (float) allFile.getInstallation().getAntennas().get(index).getH();
					int signOfAngles = headingOffset > 170 && headingOffset < 190 ? 1 : -1;

					float angle = (float) (signOfAngles * pointingAngle - antennaRollOffset - roll);
					return (short) Math.round(angle * (1 / acrossBeamFactor));
				}));

		/** along angle **/
		NCVariable azimuthBeamVariable = mbg.getVariable(MbgConstants.AlongAngle);
		swathBeamValues.add(new ValueD2S2(azimuthBeamVariable, (buffer, iBeam) -> new SShort((short) 0)));
	}

	@Override
	public void fillData(AllFile allFile) throws IOException, NCException {
		long[] origin = { 0, 0 };
		long[] count = { 1, allFile.getDetectionCount() };

		ObjectBufferPool<DatagramBuffer> pool = new ObjectBufferPool<>(
				() -> new DatagramBuffer(allFile.getByteOrder()));

		TreeMap<Long, short[]> attitudeDatagrams = allFile.getAttitude().getSortedActiveAttitudeDatagrams();

		for (Entry<SwathIdentifier, RawRange78CompletePingMetadata> pingEntry : allFile.getRawRange78().getPings()) {
			if (!allFile.getGlobalMetadata().getSwathIds().contains(pingEntry.getKey())) {
				continue;
			}
			final RawRange78CompletePingMetadata ping = pingEntry.getValue();

			List<Pair<DatagramBuffer, RawRange78CompleteAntennaPingMetadata>> data = DatagramReader.read(pool,
					ping.locate());

			// clear buffers
			swathBeamValues.forEach(ValueD2::clear);
			swathBeamWithAttitudeValues.forEach(ValueD2WithAttitude::clear);

			// get origin from swath identifier (ignore if matching index not found)
			var swathId = pingEntry.getKey();
			var optIndex = allFile.getGlobalMetadata().getSwathIds().getIndex(swathId);
			if (optIndex.isEmpty()) {
				DefaultLogger.get().warn(
						"Datagram RawRange78 associated with a missing ping is ignored (number = {}, sequence = {}).",
						swathId.getRawPingNumber(), swathId.getSwathPosition());
				continue;
			}
			origin[0] = optIndex.get();

			Map<Integer, XYZ88DepthCompleteAntennaPingMetadata> depthAntenna = new HashMap<>();
			if (allFile.getXyzdepth().getPing(pingEntry.getKey()) != null) {
				depthAntenna = allFile.getXyzdepth().getPing(pingEntry.getKey()).getAntennas();
			} else {
				// missing XYZ datagram; skipping
				XLog.warn("RawRange78 decoder : missing XYZ88 datagram for ping "
						+ pingEntry.getKey().getRawPingNumber() + " skipping it");
			}

			for (Pair<DatagramBuffer, RawRange78CompleteAntennaPingMetadata> antenna : data) {
				DatagramBuffer buffer = antenna.getFirst();
				int antennaSerialNumber = PingDatagram.getSerialNumber(buffer.byteBufferData).getU();
				final int offsetInDg = RawRange78.getStartingRxOffset(buffer.byteBufferData);

				// compute beam offset from the current antenna
				// Previous method: errors when the ping is not complete!
				// final int beamOffset = ping.getBeamOffset(RawRange78.getSerialNumber(buffer.byteBufferData).getU());
				Set<Integer> serialNumbers = allFile.getGlobalMetadata().getSerialNumbers();
				int beamOffset = (!serialNumbers.isEmpty() && antennaSerialNumber != serialNumbers.iterator().next())
						? antenna.getSecond().getBeamCount()
						: 0;

				for (int i = 0; i < antenna.getSecond().getBeamCount(); i++) {

					// if the current datagram is not present for depth, do not read it
					if (!depthAntenna.containsKey(antennaSerialNumber))
						continue;

					int beamIndexSrc = RawRange78.getBeamOffset(offsetInDg, i);
					int beamIndexDest = beamOffset + i;

					// fill [swath * beam] variables
					for (ValueD2 v : swathBeamValues)
						v.fill(antenna.getFirst(), beamIndexSrc, beamIndexDest);

					// fill [swath * beam + attitude] variables
					double[] attitude = getAttitude(attitudeDatagrams, buffer, beamIndexSrc);
					for (ValueD2WithAttitude v : swathBeamWithAttitudeValues)
						v.fill(antenna.getFirst(), beamIndexSrc, beamIndexDest, attitude[0], attitude[1], attitude[2]);
				}
			}

			for (ValueD2 v : swathBeamValues)
				v.write(origin, count);

			for (ValueD2WithAttitude v : swathBeamWithAttitudeValues)
				v.write(origin, count);

			for (Pair<DatagramBuffer, RawRange78CompleteAntennaPingMetadata> antenna : data)
				pool.release(antenna.getFirst());
		}
	}

	/**
	 * Computes attitude values.
	 */
	private double[] getAttitude(TreeMap<Long, short[]> attitudeDatagrams, DatagramBuffer buffer, int beamIndexSrc) {
		int txSectorIndex = RawRange78.getTxSectorIndex(buffer.byteBufferData, beamIndexSrc).getInt();

		double beamDuration = RawRange78.getSectorTransmitDelay(buffer.byteBufferData, txSectorIndex).get()
				+ RawRange78.getRange(buffer.byteBufferData, beamIndexSrc).get();

		long currentTimeRx = BaseDatagram.getEpochTime(buffer.byteBufferData) + ((long) (beamDuration * 1000));

		return MbgUtils.getAttitudeByInterpolation(currentTimeRx, attitudeDatagrams);
	}

}
