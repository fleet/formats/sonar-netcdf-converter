package fr.ifremer.globe.api.xsf.converter.kmall.datagram;

import java.nio.ByteBuffer;

import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.KmallFile;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.UByte;

/**
 * Multi
 */
public abstract class KmAllMDatagram extends KmAllBaseDatagram {
	
    public static void dump4debug(ByteBuffer datagram)
	{
    	KmAllBaseDatagram.dump4debug(datagram);

    	System.err.println("EMdgmMpartition_def ");
    	System.err.println("NumOfDgms:"+getNumOfDgms(datagram));
        System.err.println("DgmNum:"+getDgmNum(datagram));
        
    	
    	System.err.println("EMdgmMbody_def");

    	
		System.err.println("NumByteCmnPart:"+getNumByteCmnPart(datagram));
        System.err.println("PingCnt:"+getPingCnt(datagram));
        System.err.println("RxFansPerPing:"+getRxFansPerPing(datagram).getInt());
        System.err.println("FanIndex:"+getRxFanIndex(datagram));
        System.err.println("SwathPerPing:"+getSwathPerPing(datagram).getU());
        System.err.println("SwathAlongPosition:"+getSwathAlongPosition(datagram).getU());
        System.err.println("TxTransducerInd:"+getTxTransducerInd(datagram).getU());
        System.err.println("RxTransducerInd:"+getRxTransducerInd(datagram).getU());
        System.err.println("NumRxTransducers:"+getNumRxTransducers(datagram).getU());
        System.err.println("AlgorithmType:"+getAlgorithmType(datagram).getU());

	}

    @Override
    public final void computeSpecificMetadata(KmallFile metadata, ByteBuffer datagram, String datagramType,
            DatagramPosition position) {
        final SwathIdentifier swathNum = metadata.getSwathId(getPingCnt(datagram), getSwathPerPing(datagram).getU(),getSwathAlongPosition(datagram).getU(),getEpochTimeMilli(datagram));
        metadata.addRxFan(getRxTransducerInd(datagram).getU());
        computeSpecificMMetadata(metadata, datagram, datagramType, swathNum, position);
    }

    /**
     * Should be implemented by all multibeam datagram handlers.
     * @param metadata
     * @param datagram
     * @param datagramType
     * @param swathNum
     * @param position
     */
    public abstract void computeSpecificMMetadata(KmallFile metadata, ByteBuffer datagram, String datagramType, SwathIdentifier swathId, DatagramPosition position);

    ///////////////////////////
    // EMdgmMpartition_def part
    ///////////////////////////
    private static final int PARTITION_START = HEADER_SIZE-DATAGRAMM_OFFSET;
    /**
     * Number of datagrmas composing the partition. Always 1 in kmall files
     * @return
     */
    public static int getNumOfDgms(ByteBuffer buffer) {
        return TypeDecoder.read2U(buffer, PARTITION_START).getU();
    }
    
    /**
     * Current datagram number in the partition. Always 1 in kmall files
     * @param buffer
     * @return
     */
    public static int getDgmNum(ByteBuffer buffer) {
        return TypeDecoder.read2U(buffer, PARTITION_START+2).getU();
    }
    
    //////////////////////
    // EMdgmMbody_def part
    //////////////////////
    private static final int BODY_START = PARTITION_START+4;

    /**
     * get num bytes in EMdgmMbody_def struct, constant value
     * 
     * @param buffer
     * @return
     */
    public static int getNumByteCmnPart(ByteBuffer buffer) {
        return TypeDecoder.read2U(buffer, BODY_START).getU();
    }
    
    /**
     * get ping number
     * @param buffer
     * @return
     */
    public static int getPingCnt(ByteBuffer buffer) {
        return TypeDecoder.read2U(buffer, BODY_START+2).getU();
    }

    public static UByte getRxFansPerPing(ByteBuffer buffer) {
        return TypeDecoder.read1U(buffer, BODY_START+4);
    }
    public static UByte getFanIndexU(ByteBuffer buffer) {
        return TypeDecoder.read1U(buffer, BODY_START+5);
    }
    public static int getRxFanIndex(ByteBuffer buffer) {
        return getFanIndexU(buffer).getU();
    }
    
    public static UByte getSwathPerPing(ByteBuffer buffer) {
        return TypeDecoder.read1U(buffer, BODY_START+6);
    }
    
    public static UByte getSwathAlongPosition(ByteBuffer buffer) {
        return TypeDecoder.read1U(buffer, BODY_START+7);
    }
    
    public static UByte getTxTransducerInd(ByteBuffer buffer) {
        return TypeDecoder.read1U(buffer, BODY_START+8);
    }
    
    public static UByte getRxTransducerInd(ByteBuffer buffer) {
        return TypeDecoder.read1U(buffer, BODY_START+9);
    }
    
    public static UByte getNumRxTransducers(ByteBuffer buffer) {
        return TypeDecoder.read1U(buffer, BODY_START+10);
    }
    
    public static UByte getAlgorithmType(ByteBuffer buffer) {
        return TypeDecoder.read1U(buffer, BODY_START+11);
    }
}
