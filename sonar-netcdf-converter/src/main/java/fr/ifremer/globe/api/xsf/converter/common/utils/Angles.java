package fr.ifremer.globe.api.xsf.converter.common.utils;

public class Angles {
	/**
	 * Trim an angle value to ]-180 180]
	 * */
	public static double trim180(double value)
	{
		while(value>180.)
			value-=360.;
		while(value<=-180.)
		{
			value+=360;
		}
		return value;
	}
	/**
	 * Trim an angle value to ]-180 180]
	 * */
	public static float trim180f(float value)
	{
		return (float)trim180(value);
	}
}
