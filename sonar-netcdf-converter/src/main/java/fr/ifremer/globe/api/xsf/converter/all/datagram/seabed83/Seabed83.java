package fr.ifremer.globe.api.xsf.converter.all.datagram.seabed83;

import fr.ifremer.globe.api.xsf.converter.all.datagram.PingDatagram;
import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.SByte;
import fr.ifremer.globe.api.xsf.util.types.SInt;
import fr.ifremer.globe.api.xsf.util.types.UByte;
import fr.ifremer.globe.api.xsf.util.types.UShort;

import java.nio.ByteBuffer;

public class Seabed83 extends PingDatagram {
    /**
     * {@inheritDoc}
     */
    @Override
    public Seabed83Metadata getSpecificMetadata(AllFile metadata) {
        return metadata.getSeabed83();
    }

    @Override
    protected void computeSpecificMetadata(AllFile metadata, ByteBuffer datagram, long epochTime,
                                           SwathIdentifier swathId,
                                           int serialNumber, DatagramPosition position) {
        Seabed83Metadata seabed83Metadata = metadata.getSeabed83();

        Seabed83CompletePingMetadata completeping = seabed83Metadata.getPing(swathId);
        if (completeping == null) {
            completeping = new Seabed83CompletePingMetadata(metadata.getInstallation().getSerialNumbers());
            seabed83Metadata.addPing(swathId, completeping);
        }
        // compute the total number of samples
        int beamCount = getReceivedBeamsCounter(datagram).getU();
        int maxSampleCount = 0;
        for (int i = 0; i < beamCount; i++) {
            maxSampleCount = Math.max(maxSampleCount, getSampleCount(datagram, i).getU());
        }
        completeping.addAntenna(serialNumber,
                new Seabed83CompleteAntennaPingMetadata(position, getMaxBeamCount(datagram), maxSampleCount));
    }

    public static UShort getCounter(ByteBuffer datagram) {
        return TypeDecoder.read2U(datagram, 12);
    }

    public static UShort getSerialNumber(ByteBuffer datagram) {
        return TypeDecoder.read2U(datagram, 14);
    }

    public static UShort getMeanAbsorptionCoefficient(ByteBuffer datagram) {
        return TypeDecoder.read2U(datagram, 16);
    }

    public static UShort getPulseLength(ByteBuffer datagram) {
        return TypeDecoder.read2U(datagram, 18);
    }

    public static UShort getRangeToNormalIncidence(ByteBuffer datagram) {
        return TypeDecoder.read2U(datagram, 20);
    }

    public static UShort getStartRangeSampleTVGRamp(ByteBuffer datagram) {
        return TypeDecoder.read2U(datagram, 22);
    }

    public static UShort getStopRangeSampleTVGRamp(ByteBuffer datagram) {
        return TypeDecoder.read2U(datagram, 24);
    }

    public static SByte getNormalIncidenceBS(ByteBuffer datagram) {
        return TypeDecoder.read1S(datagram, 26);
    }

    public static SByte getObliqueBS(ByteBuffer datagram) {
        return TypeDecoder.read1S(datagram, 27);
    }

    public static UShort getTxBeamWidth(ByteBuffer datagram) {
        return TypeDecoder.read2U(datagram, 28);
    }

    public static UByte getTVGLawCrossOver(ByteBuffer datagram) {
        return TypeDecoder.read1U(datagram, 30);
    }

    /**
     * compute the total number of beams possible, including invalid ones
     *
     * @param datagram
     * @return
     */
    public static int getMaxBeamCount(ByteBuffer datagram) {
        int nmax = 0;
        final int beamCount = getReceivedBeamsCounter(datagram).getU();

        for (int i = 0; i < beamCount; i++) {
            // + 1 because indexes are 0 based.
            nmax = Integer.max(getBeamIndex(datagram, i).getU() + 1, nmax);
        }
        return nmax;
    }

    public static UByte getReceivedBeamsCounter(ByteBuffer datagram) {
        return TypeDecoder.read1U(datagram, 31);
    }

    /**
     * START N entries Block
     */


    /**
     * return the given value
     *
     * @param datagram  the datagram buffer
     * @param beamIndex the index of valid beam index
     */
    public static UByte getBeamIndex(ByteBuffer datagram, int beamIndex) {
        return TypeDecoder.read1U(datagram, 32 + beamIndex * 6);
    }

    public static SByte getSortingDirection(ByteBuffer datagram, int beamIndex) {
        return TypeDecoder.read1S(datagram, 33 + beamIndex * 6);
    }

    // Ns - Number of samples
    public static UShort getSampleCount(ByteBuffer datagram, int beamIndex) {
        return TypeDecoder.read2U(datagram, 34 + beamIndex * 6);
    }

    /**
     * return the given value
     *
     * @param datagram  the datagram buffer
     * @param beamIndex the index of valid beam index
     */
    public static SInt getCenterSample(ByteBuffer datagram, int beamIndex) {
        // center number is the index of sample
        int center = TypeDecoder.read2U(datagram, 36 + beamIndex * 6).getU();
        if (getSortingDirection(datagram, beamIndex).get() > 0) {
            return new SInt(center);
        } else {
            return new SInt(getSampleCount(datagram, beamIndex).getU() - center);
        }
    }

    /**
     * START Sum(N entries) Block
     */

    /**
     * return the given value
     *
     * @param datagram       the datagram buffer
     * @param offsets the starting offset of the considered rxBeam
     */
    public static byte[] getAllSamples(ByteBuffer datagram, int[] offsets, int[] counts) {
        int totalSampleCount = 0;
        final int maxBeamCount = getMaxBeamCount(datagram);
        final int realBeamCount = getReceivedBeamsCounter(datagram).getU();

        int[] directions = new int[maxBeamCount];

        for (int i = 0; i < maxBeamCount; i++) {
            counts[i] = 0;
            directions[i] = 0;
            offsets[i] = 0;
        }

        for (int i = 0; i < realBeamCount; i++) {
            final int idx = getBeamIndex(datagram, i).getU();
            final int sampleCount = getSampleCount(datagram, i).getU();
            counts[idx] = sampleCount;
            offsets[idx] = totalSampleCount;
            directions[idx] = getSortingDirection(datagram, i).get();
            totalSampleCount += sampleCount;
        }

        byte[] samples = new byte[totalSampleCount];

        int sampleIndex = 0;
        final int datagramOffset = 32 + 6 * realBeamCount;
        for (int i = 0; i < realBeamCount; i++) {
            final int idx = getBeamIndex(datagram, i).getU();
            if (directions[idx] > 0) {
                for (int j = 0; j < counts[idx]; j++) {
                    samples[sampleIndex++] = datagram.get(datagramOffset + offsets[idx] + j);
                }
            } else {
                for (int j = counts[idx] - 1; j >= 0; j--) {
                    samples[sampleIndex++] = datagram.get(datagramOffset + offsets[idx] + j);
                }
            }
        }
        return samples;
    }
}
