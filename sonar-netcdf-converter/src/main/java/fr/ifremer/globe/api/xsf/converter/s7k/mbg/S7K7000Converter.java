package fr.ifremer.globe.api.xsf.converter.s7k.mbg;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgBase;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgConstants;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2S4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2U2;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K7000;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KFile;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KMultipingMetadata;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KMultipingSequenceMetadata;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KSwathMetadata;
import fr.ifremer.globe.api.xsf.util.DefaultLogger;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.types.SInt;
import fr.ifremer.globe.api.xsf.util.types.UShort;
import fr.ifremer.globe.netcdf.api.convention.CFStandardNames;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCVariable;

public class S7K7000Converter implements IDatagramConverter<S7KFile, MbgBase> {

	private final List<ValueD2> swathAntennaValues = new LinkedList<>();

	@Override
	public void declare(S7KFile s7kFile, MbgBase mbg) throws NCException {

		/** Sampling rate (not declare here if 7006 datagram exist) **/
		if (s7kFile.get7006Metadata().datagramCount > 0) {
			NCVariable samplingRateVariable = mbg.getVariable(MbgConstants.SamplingRate);
			swathAntennaValues.add(new ValueD2S4(samplingRateVariable, (buffer, antennaIndex) -> {
				return new SInt((int) (S7K7000.getSampleRate(buffer).get()));
			}));
		}

		/** Sound velocity (not declare here if 7027 datagram exist) **/
		if (s7kFile.get7027Metadata().datagramCount > 0) {
			NCVariable soundVelVariable = mbg.getVariable(MbgConstants.SoundVelocity);
			double soundVelFactor = soundVelVariable.getAttributeDouble(CFStandardNames.CF_SCALE_FACTOR);
			double soundVelOffset = soundVelVariable.getAttributeDouble(CFStandardNames.CF_ADD_OFFSET);
			swathAntennaValues.add(new ValueD2U2(soundVelVariable, (buffer, iBeam) -> new UShort((short) Math
					.round((S7K7000.getSoundVelocity(buffer).get() - soundVelOffset) * (1 / soundVelFactor)))));
		}
	}

	@Override
	public void fillData(S7KFile s7kFile) throws IOException, NCException {
		S7KMultipingMetadata mpmd = s7kFile.get7000Metadata().getEntries().iterator().next().getValue();
		DatagramBuffer buffer = new DatagramBuffer();
		long origin;

		// iterate over pings (by date)
		for (Entry<Long, S7KMultipingSequenceMetadata> posentry : mpmd.getEntries()) {
			// iterate over ping sequence
			for (Entry<Integer, S7KSwathMetadata> e : posentry.getValue().getEntries()) {
				DatagramReader.readDatagram(e.getValue().getPosition().getFile(), buffer,
						e.getValue().getPosition().getSeek());
				swathAntennaValues.forEach(ValueD2::clear);
				SwathIdentifier swathId = new SwathIdentifier(S7K7000.getPingNumber(buffer).getU(),
						S7K7000.getMultiPingSequence(buffer).getU());

				// get origin from swath identifier (ignore if matching index not found)
				var optIndex = s7kFile.getSwathIndexer().getIndex(swathId);
				if (optIndex.isEmpty()) {
					DefaultLogger.get().warn(
							"Datagram S7K7000 associated with a missing ping is ignored (number = {}, sequence = {}).",
							swathId.getRawPingNumber(), swathId.getSwathPosition());
					continue;
				}
				origin = optIndex.get();

				// Mbg swath (= cycle) count is only defined by depth datagrams: avoid to get an overflow exception
				if (origin >= s7kFile.getSwathCountFromDepthDatagrams())
					return;

				// Swath x Antenna values
				for (ValueD2 v : swathAntennaValues) {
					v.fill(buffer, 0, 0);// antennaIdx);
					v.write(new long[] { origin, 0 }, new long[] { 1, 1 });
				}
			}
		}
	}
}
