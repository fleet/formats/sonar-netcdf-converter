package fr.ifremer.globe.api.xsf.converter.all.datagram.attitude;

import java.io.IOException;
import java.util.Map.Entry;
import java.util.TreeMap;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.all.datagram.IndividualSensorMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.SensorMetadata;
import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagram;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.util.SimpleIdentifier;

/**
 * */
public class AttitudeMetadata extends SensorMetadata<Integer, IndividualSensorMetadata> {


	private float headingSum, rollSum, pitchSum, heaveSum;
	private long attNumber;
	public float headingMin, headingMax;	
	public float rollMin, rollMax;	
	public float pitchMin, pitchMax;	
	public float heaveMin, heaveMax;	

	private int activeId=1; //by default sensor 1 is considered as active
	


	public AttitudeMetadata() {
        // Tells the parent class that individual sensor class
        // IndividualSensorMetadata. If for some other datagram,
        // this class is not enough, it can be extended and
        // given here as well as in the class declaration.
        super(new IndividualSensorMetadata());

		headingSum 		= 0;
		rollSum 		= 0;
		pitchSum 		= 0;
		heaveSum 		= 0;
		attNumber 		= 0;
		headingMin 		= 999999f;
		headingMax 		= -99999f;
		rollMax 		= -99999f;
		rollMin 		= 999999f;
		pitchMax 		= -99999f;
		pitchMin 		= 999999f;
		heaveMax 		= -99999f;
		heaveMin 		= 999999f;
	}

	// Methodes pour les statistiques de Summary.
	public void addAttValue(int headingVal, int rollVal, int pitchVal, int heaveVal) {
		this.headingSum += headingVal*0.01;
		this.rollSum 	+= rollVal*0.01;
		this.pitchSum 	+= pitchVal*0.01;
		this.heaveSum 	+= heaveVal*0.01;
		this.attNumber++;		
	}
	
	public float getHeadingMean() {
		return this.headingSum / this.attNumber;
	}
	public float getRollMean() {
		return this.rollSum / this.attNumber;
	}	
	public float getPitchMean() {
		return this.pitchSum / this.attNumber;
	}
	public float getHeaveMean() {
		return this.heaveSum / this.attNumber;
	}
	
	public void computeMinMaxValue(int headingVal, int rollVal, int pitchVal, int heaveVal) {
    	headingMin = Float.min((float)(headingVal*0.01), (float) headingMin);
    	headingMax = Float.max((float)(headingVal*0.01), (float) headingMax);		
    	rollMin 	= Float.min((float)(rollVal*0.01), (float) rollMin);
    	rollMax 	= Float.max((float)(rollVal*0.01), (float) rollMax);		
    	pitchMin = Float.min((float)(pitchVal*0.01), (float) pitchMin);
    	pitchMax = Float.max((float)(pitchVal*0.01), (float) pitchMax);		
    	heaveMin = Float.min((float)(heaveVal*0.01), (float) heaveMin);
    	heaveMax = Float.max((float)(heaveVal*0.01), (float) heaveMax);		
	}
	
    public float getHeadingMin()
    {
    	return headingMin;
    }

    public float getHeadingMax()
    {
    	return headingMax;
    }
    public float getRollMin()
    {
    	return rollMin;
    }

    public float getRollMax()
    {
    	return rollMax;
    }        
    public float getPitchMin()
    {
    	return pitchMin;
    }

    public float getPitchMax()
    {
    	return pitchMax;
    }    
    public float getHeaveMin()
    {
    	return heaveMin;
    }

    public float getHeaveMax()
    {
    	return heaveMax;
    }
    
    
	/**
	 * 
	 * */
	public int getActiveId() {
		return activeId;
	}

	public void setActiveId(int activeId) {
		this.activeId = activeId;
	}
	
	/**
	 * Reads and sorts attitude values from each sensor
	 */
	public TreeMap<Long, short[]> getSortedAttitudeDatagrams() throws IOException {
		TreeMap<Long, short[]> attitudeValues = new TreeMap<>();

		for (IndividualSensorMetadata sensorMetadata : getSensors().values()) {
			for (Entry<SimpleIdentifier, DatagramPosition> entry : sensorMetadata.getDatagrams()) {
				final DatagramPosition attDatagram = entry.getValue();

				DatagramBuffer buffer = new DatagramBuffer(DatagramReader.getByteOrder(attDatagram.getFile()));
				DatagramReader.readDatagramAt(attDatagram.getFile(), buffer, attDatagram.getSeek());

				long startTimeDatagram= BaseDatagram.getEpochTime(buffer.byteBufferData);
				int valueCount = Attitude.getNumberEntries(buffer.byteBufferData).getU();
				
				long time;
				for(int i =0; i < valueCount; i++) {
					time = startTimeDatagram + Attitude.getTimeSinceRecordStart(buffer.byteBufferData, i).getU();
					
					attitudeValues.put(time, new short[] {
							Attitude.getRoll(buffer.byteBufferData, i).get(),
							Attitude.getPitch(buffer.byteBufferData, i).get(),
							Attitude.getHeave(buffer.byteBufferData, i).get()
					});
				}
			}
		}
		return attitudeValues;
	}
	
	/**
	 * Reads and sorts attitude values from each sensor
	 */
	public TreeMap<Long, short[]> getSortedActiveAttitudeDatagrams() throws IOException {
		TreeMap<Long, short[]> attitudeValues = new TreeMap<>();

		for (IndividualSensorMetadata sensorMetadata : getSensors().values()) {
			for (Entry<SimpleIdentifier, DatagramPosition> entry : sensorMetadata.getDatagrams()) {
				final DatagramPosition attDatagram = entry.getValue();

				DatagramBuffer buffer = new DatagramBuffer(DatagramReader.getByteOrder(attDatagram.getFile()));
				DatagramReader.readDatagramAt(attDatagram.getFile(), buffer, attDatagram.getSeek());
				
				boolean active = Attitude.isActive(buffer.byteBufferData);
				if(!active) {
					continue;
				}
				
				long startTimeDatagram= BaseDatagram.getEpochTime(buffer.byteBufferData);
				int valueCount = Attitude.getNumberEntries(buffer.byteBufferData).getU();
				long time;
				
				for(int i =0; i < valueCount; i++) {
					time = startTimeDatagram + Attitude.getTimeSinceRecordStart(buffer.byteBufferData, i).getU();
					
					attitudeValues.put(time, new short[] {
							Attitude.getRoll(buffer.byteBufferData, i).get(),
							Attitude.getPitch(buffer.byteBufferData, i).get(),
							Attitude.getHeave(buffer.byteBufferData, i).get()
					});
				}
			}
		}
		return attitudeValues;
	}
}
