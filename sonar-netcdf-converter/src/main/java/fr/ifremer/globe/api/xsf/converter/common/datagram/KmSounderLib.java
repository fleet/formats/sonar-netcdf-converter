package fr.ifremer.globe.api.xsf.converter.common.datagram;

public class KmSounderLib {
	public static final int EM_1002 = 1002;
	public static final int EM_2040 = 2040;
	public static final int EM_2040C = 2045;
	public static final int ME_70 = 850;
	public static final int EM_710 = 710;
	public static int EM_302 = 302;
	public static int EM_122 = 122;
}
