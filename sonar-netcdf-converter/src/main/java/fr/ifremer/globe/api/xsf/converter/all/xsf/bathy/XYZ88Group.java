package fr.ifremer.globe.api.xsf.converter.all.xsf.bathy;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.installation.InstallationMetadata.Antenna;
import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.all.datagram.xyz88.XYZ88Depth;
import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.AbstractCompleteAntennaPingSingleMetadata;
import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.AbstractCompletePingMetadata;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1F4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1F4WithAttitude;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2F4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2F4WithAttitude;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2F8WithPosition;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2S1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2S2;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2U1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2U2;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2WithAttitude;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2WithPosition;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfFromKongsberg;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.BathymetryVendorSpecificGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.types.DetectionTypeHelper;
import fr.ifremer.globe.api.xsf.converter.common.xsf.types.detection_backscatter_compensation_t;
import fr.ifremer.globe.api.xsf.converter.common.xsf.types.invalid_detection_estimation_method_t;
import fr.ifremer.globe.api.xsf.util.types.DDouble;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.api.xsf.util.types.SByte;
import fr.ifremer.globe.api.xsf.util.types.SShort;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.maths.MatrixBuilder;
import fr.ifremer.globe.utils.maths.Ops;
import fr.ifremer.globe.utils.maths.Vector3D;

public class XYZ88Group extends DepthGroup {

	public XYZ88Group(AllFile md) {
		super(md, md.getXyzdepth());
	}

	@Override
	public void declare(AllFile allFile, XsfFromKongsberg xsf) throws NCException {
		super.declare(allFile, xsf);

		BathymetryVendorSpecificGrp bathyVendorGrp = xsf.getBathy();

		/**********************************************************************************************************
		 * xsf common variables
		 **********************************************************************************************************/

		/** tx_transducer_zdepth **/
		swathValues.add(new ValueD1F4(beamGrp.getTx_transducer_depth(),
				buffer -> XYZ88Depth.getTransmitTransducerDepth(buffer.byteBufferData)));

		/** ship_vertical_offset **/
		swathValuesWithAttitude.add(new ValueD1F4WithAttitude(beamGrp.getPlatform_vertical_offset(),
				/** Get interpolated attitude */
				(buffer, roll, pitch, heave) -> {
					MatrixBuilder.setDegRotationMatrix(0, pitch, roll, attitudeMatrix);
					Antenna txAnt = stats.getInstallation().getFirstTxAntenna(); // TODO case with several tx antenna
					Ops.mult(attitudeMatrix, new Vector3D(txAnt.getX(), txAnt.getY(), txAnt.getZ()), txAntennaSCS);

					return new FFloat((float) txAntennaSCS.getZ()
							- XYZ88Depth.getTransmitTransducerDepth(buffer.byteBufferData).get());
				}));

		/** detection_reflectivity **/
		swathBeamValues
				.add(new ValueD2F4(bathyGrp.getDetection_backscatter_r(), (buffer, beamIndexSource) -> new FFloat(
						(float) XYZ88Depth.getReflectivity(buffer.byteBufferData, beamIndexSource).get() / 10)));

		/** detection_x **/
		swathBeamValues.add(new ValueD2F4(bathyGrp.getDetection_x(),
				(buffer, iBeam) -> new FFloat((float)getAlongDistance(buffer, iBeam))));

		/** detection_y **/
		swathBeamValues.add(new ValueD2F4(bathyGrp.getDetection_y(),
				(buffer, iBeam) -> new FFloat((float)getAcrossDistance(buffer, iBeam))));

		/** status **/
		swathBeamValues.add(new ValueD2S1(bathyGrp.getStatus(), (buffer, i) -> {
			byte detectionInfoByte = XYZ88Depth.getDetectionInformation(buffer.byteBufferData, i).getByte();
			return (detectionInfoByte & 0x80) == 0x80 ? new SByte((byte) (0x01 << 1)) : new SByte((byte) 0);
		}));

		/** type (phase/amp) **/
		swathBeamValues.add(new ValueD2S1(bathyGrp.getDetection_type(), (buffer, i) -> {
			byte detectionInfoByte = XYZ88Depth.getDetectionInformation(buffer.byteBufferData, i).getByte();
			if ((detectionInfoByte & 0x80) == 0x00) { // valid detection
				if ((detectionInfoByte & 0x01) == 0x00) {
					return new SByte(DetectionTypeHelper.AMPLITUDE.get());
				} else {
					return new SByte(DetectionTypeHelper.PHASE.get());
				}
			} else { // invalid detection
				return new SByte(DetectionTypeHelper.INVALID.get());
			}
		}));

		/** Ship heading **/
		swathValues.add(new ValueD1F4(xsf.getBeamGroup().getPlatform_heading(),
				buffer -> new FFloat(XYZ88Depth.getVesselHeading(buffer.byteBufferData).getU() / 100f)));

		/** detection_z : depth_from_antenna + zAntenna_in_SCS **/
		swathBeamValuesWithAttitude
				.add(new ValueD2F4WithAttitude(bathyGrp.getDetection_z(), (buffer, iBeam, roll, pitch, heave) -> {

					MatrixBuilder.setDegRotationMatrix(0, pitch, roll, attitudeMatrix);
					Antenna txAnt = stats.getInstallation().getFirstTxAntenna(); // TODO case with several tx antenna
					Ops.mult(attitudeMatrix, new Vector3D(txAnt.getX(), txAnt.getY(), txAnt.getZ()), txAntennaSCS);

					return new FFloat(
							(float) (XYZ88Depth.getDepth(buffer.byteBufferData, iBeam).get() + txAntennaSCS.getZ()));
				}));

		/** detection_latitude **/
		swathBeamValuesWithPosition.add(new ValueD2F8WithPosition(bathyGrp.getDetection_latitude(),
				(buffer, beamIndexSource, latNav, lonNav, speed, positionMetadata) -> {
					double lat = getDetectionLatLong(buffer, beamIndexSource, latNav, lonNav)[0];

					// Update geobox
					positionMetadata.computeBoundingBoxLat(lat);

					return new DDouble(lat);

				}));

		/** detection_longitude **/
		swathBeamValuesWithPosition.add(new ValueD2F8WithPosition(bathyGrp.getDetection_longitude(),
				(buffer, beamIndexSource, latNav, lonNav, speed, positionMetadata) -> {
					double lon = getDetectionLatLong(buffer, beamIndexSource, latNav, lonNav)[1];

					// Update geobox
					positionMetadata.computeBoundingBoxLon(lon);

					return new DDouble(lon);
				}));

		/**********************************************************************************************************
		 * .all specific variables
		 **********************************************************************************************************/

		swathValues.add(new ValueD1F4(beamGrp.getPlatform_heading(),
				(buffer) -> new FFloat(XYZ88Depth.getVesselHeading(buffer.byteBufferData).getU() * 0.01f)));

		swathAntennaValues.add(new ValueD2U2(bathyVendorGrp.getDetection_beam_count(),
				(buffer, i) -> XYZ88Depth.getValidDetectionCount(buffer.byteBufferData)));

		swathAntennaValues.add(new ValueD2F4(bathyVendorGrp.getDetection_sampling_freq(),
				(buffer, a) -> XYZ88Depth.getSamplingFrequency(buffer.byteBufferData)));

		swathAntennaValues.add(new ValueD2U1(xsf.getBeamGroupVendorSpecific().getScanning_info(),
				(buffer, a) -> XYZ88Depth.getScanningInfo(buffer.byteBufferData)));

		/** Add variables with a ping/beam dimension */
		swathBeamValues
				.add(new ValueD2S2(bathyGrp.getDetection_rx_transducer_index(), (buffer, beamIndexSource) -> new SShort(
						(short) stats.getInstallation().getRxAntennaIndex(XYZ88Depth.getSerialNumber(buffer.byteBufferData)))));

		swathBeamValues.add(new ValueD2F4(bathyVendorGrp.getDetection_window_length(),
				(buffer, beamIndexSource) -> new FFloat(
						XYZ88Depth.getWindowsLengthInSample(buffer.byteBufferData, beamIndexSource).getU()
								/ XYZ88Depth.getSamplingFrequency(buffer.byteBufferData).get())));

		swathBeamValues.add(new ValueD2F4(bathyVendorGrp.getDetection_constructor_quality_factor(),
				(buffer, beamIndexSource) -> new FFloat(
						XYZ88Depth.getQF(buffer.byteBufferData, beamIndexSource).getInt() * 1f)));

		/** for .all file, no backscatter calibration info is available */
		swathBeamValues
				.add(new ValueD2F4(bathyGrp.getDetection_backscatter_calibration(), (a, b) -> new FFloat(Float.NaN)));

		swathBeamValues.add(new ValueD2F4(bathyVendorGrp.getBeam_incidence_angle_adjustment(),
				(buffer, beamIndexSource) -> new FFloat(
						XYZ88Depth.getBeamIncidenceAngleAdj(buffer.byteBufferData, beamIndexSource).get() * 0.1f)));

		swathBeamValues.add(new ValueD2U1(bathyVendorGrp.getDetection_information(), (buffer,
				beamIndexSource) -> XYZ88Depth.getDetectionInformation(buffer.byteBufferData, beamIndexSource)));

		/** invalid_detection_estimation_method **/
		swathBeamValues.add(new ValueD2S1(bathyVendorGrp.getInvalid_detection_estimation_method(), (buffer, i) -> {
			byte detectionInfoByte = XYZ88Depth.getDetectionInformation(buffer.byteBufferData, i).getByte();
			if ((detectionInfoByte & 0x80) == 0x00) { // valid detection
				return new SByte(invalid_detection_estimation_method_t.normal.get());

			} else {
				// invalid detection
				byte value = (byte) (detectionInfoByte & 0x0F);
				switch (value) {
				case (byte) 0x00:
					return new SByte(invalid_detection_estimation_method_t.normal.get());
				case (byte) 0x01:
					return new SByte(invalid_detection_estimation_method_t.interpolated.get());
				case (byte) 0x02:
					return new SByte(invalid_detection_estimation_method_t.estimated.get());
				case (byte) 0x03:
					return new SByte(invalid_detection_estimation_method_t.rejected.get());
				case (byte) 0x04:
					return new SByte(invalid_detection_estimation_method_t.no_detection_data.get());
				default:
					return new SByte(invalid_detection_estimation_method_t.unknown.get());
				}
			}
		}));

		swathBeamValues.add(new ValueD2S1(bathyVendorGrp.getDetection_backscatter_compensation(), (buffer, i) -> {
			byte detectionInfoByte = XYZ88Depth.getDetectionInformation(buffer.byteBufferData, i).getByte();
			if ((detectionInfoByte & 0x10) == 0x00) {
				return new SByte(detection_backscatter_compensation_t.not_compensated.get());
			} else {
				return new SByte(detection_backscatter_compensation_t.compensated.get());
			}

		}));

		swathBeamValues.add(new ValueD2S1(bathyVendorGrp.getReal_time_cleaning_info(), (buffer,
				beamIndexSource) -> XYZ88Depth.getRTCleaningInformation(buffer.byteBufferData, beamIndexSource)));
	}

	@Override
	protected void fillSwathBeamValues(AbstractCompletePingMetadata<AbstractCompleteAntennaPingSingleMetadata> ping,
			DatagramBuffer buffer) {
		int beamOffset = ping.getBeamOffset(XYZ88Depth.getSerialNumber(buffer.byteBufferData).getU());
		int beamCount = XYZ88Depth.getBeamCount(buffer.byteBufferData).getU();

		for (int i = 0; i < beamCount; i++) {
			for (ValueD2 v : swathBeamValues) {
				v.fill(buffer, i, beamOffset + i);
			}
		}
	}

	@Override
	protected void fillSwathBeamValuesWithPosition(
			AbstractCompletePingMetadata<AbstractCompleteAntennaPingSingleMetadata> ping, DatagramBuffer buffer,
			double lat, double lon, double speed) {
		int beamOffset = ping.getBeamOffset(XYZ88Depth.getSerialNumber(buffer.byteBufferData).getU());
		int beamCount = XYZ88Depth.getBeamCount(buffer.byteBufferData).getU();

		for (int i = 0; i < beamCount; i++) {
			for (ValueD2WithPosition v : swathBeamValuesWithPosition)
				v.fill(buffer, i, beamOffset + i, lat, lon, speed, positionMetadata);
		}
	}

	@Override
	protected void fillSwathBeamValuesWithAttitude(
			AbstractCompletePingMetadata<AbstractCompleteAntennaPingSingleMetadata> ping, DatagramBuffer buffer,
			double roll, double pitch, double heave) {
		int beamOffset = ping.getBeamOffset(XYZ88Depth.getSerialNumber(buffer.byteBufferData).getU());
		int beamCount = XYZ88Depth.getBeamCount(buffer.byteBufferData).getU();

		for (int i = 0; i < beamCount; i++) {
			for (ValueD2WithAttitude v : swathBeamValuesWithAttitude)
				v.fill(buffer, i, beamOffset + i, roll, pitch, heave);
		}
	}

	@Override
	protected double getAlongDistance(BaseDatagramBuffer buffer, int iBeam) {
		return XYZ88Depth.getAlongTrackDistance(buffer.byteBufferData, iBeam).get() + getActivePositionSCS().getX();
	}

	@Override
	protected double getAcrossDistance(BaseDatagramBuffer buffer, int iBeam) {
		return  XYZ88Depth.getAcrossDistance(buffer.byteBufferData, iBeam).get() + getActivePositionSCS().getY();
	}

	@Override
	protected double getVesselHeading(BaseDatagramBuffer buffer) {
		return XYZ88Depth.getVesselHeading(buffer.byteBufferData).getU() / 100f;
	}

}
