package fr.ifremer.globe.api.xsf.converter.all.datagram.networkattitudevelocity;

import java.nio.ByteBuffer;

import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagram;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.util.ComparablePair;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.SShort;
import fr.ifremer.globe.api.xsf.util.types.UByte;
import fr.ifremer.globe.api.xsf.util.types.UShort;

/**
 * Stateless Position datagram
 */
public class NetworkAttitudeVelocity extends BaseDatagram {

	@Override
	public NetworkAttitudeVelocityMetadata getSpecificMetadata(AllFile metadata) {
		return metadata.getNetworkAttitudeVelocity();
	}

	@Override
	protected void computeSpecificMetadata(AllFile metadata, ByteBuffer datagram, byte datagramType,
			long epochTime, DatagramPosition position) {
		final NetworkAttitudeVelocityMetadata index = getSpecificMetadata(metadata);

		IndividualNetworkAttitudeVelocity sensorMetadata = index.getOrCreateSensor(
				new ComparablePair<UShort, Short>(getSerialNumber(datagram), getSensorId(datagram)));
		sensorMetadata.addDatagram(position, getEpochTime(datagram),
				NetworkAttitudeVelocity.getNumberEntries(datagram).getU());

		int offset = getFirstInputOffset();
		for (int i = 0; i < getNumberEntries(datagram).getU(); i++) {
			sensorMetadata.addRawSensorInputLen(getNumberOfBytesInInputDgm(datagram, offset).getU());
			offset = getNextInputOffset(datagram, offset);
		}
	}

	public static UShort getCounter(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 12);
	}

	public static UShort getSerialNumber(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 14);
	}

	public static UShort getNumberEntries(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 16);
	}

	public static short getSensorId(ByteBuffer datagram) {
		return (short) (((getSensorDescriptor(datagram).getU()>>4)& 0x1)+1); // 0 is motion sensor number 1, xx01 is motion sensor number 2;
	}

	
	public static UByte getSensorDescriptor(ByteBuffer datagram) {
		return TypeDecoder.read1U(datagram, 18);
	}

	public static int getFirstInputOffset() {
		return 20;
	}

	public static int getNextInputOffset(ByteBuffer datagram, int currentOffset) {
		return currentOffset + 11 + getNumberOfBytesInInputDgm(datagram, currentOffset).getU();
	}
	// Read Spare after Sensor Descriptor

	// Read Repeat Cycle - N Entries
	public static UShort getTimeSinceRecordStart(ByteBuffer datagram, int startingOffset) {
		return TypeDecoder.read2U(datagram, startingOffset);
	}

	public static SShort getRoll(ByteBuffer datagram, int startingOffset) {
		return TypeDecoder.read2S(datagram, startingOffset + 2);
	}

	public static SShort getPitch(ByteBuffer datagram, int startingOffset) {
		return TypeDecoder.read2S(datagram, startingOffset + 4);
	}

	public static SShort getHeave(ByteBuffer datagram, int startingOffset) {
		return TypeDecoder.read2S(datagram, startingOffset + 6);
	}

	public static UShort getHeading(ByteBuffer datagram, int startingOffset) {
		return TypeDecoder.read2U(datagram, startingOffset + 8);
	}

	public static UByte getNumberOfBytesInInputDgm(ByteBuffer datagram, int startingOffset) {
		return TypeDecoder.read1U(datagram, startingOffset + 10);
	}

	public static byte[] getInputDatagram(ByteBuffer datagram, int startingOffset) {
		int numOfBytes = getNumberOfBytesInInputDgm(datagram, startingOffset).getU();
		byte[] ret = new byte[numOfBytes];
		for (int i = 0; i < numOfBytes; i++) {
			ret[i] = datagram.get(startingOffset + 11 + i);

		}
		return ret;
	}

	public static String getDataFromSensor(ByteBuffer datagram, int startingOffset) {
		return new String(getInputDatagram(datagram, startingOffset));
	}

}
