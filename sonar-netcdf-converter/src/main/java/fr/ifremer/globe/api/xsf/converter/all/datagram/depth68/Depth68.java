package fr.ifremer.globe.api.xsf.converter.all.datagram.depth68;

import fr.ifremer.globe.api.xsf.converter.all.datagram.PingDatagram;
import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.SByte;
import fr.ifremer.globe.api.xsf.util.types.SShort;
import fr.ifremer.globe.api.xsf.util.types.UByte;
import fr.ifremer.globe.api.xsf.util.types.UShort;

import java.nio.ByteBuffer;

public class Depth68 extends PingDatagram {

    @Override
    public Depth68Metadata getSpecificMetadata(AllFile metadata) {
        return metadata.getDepth68();
    }

    @Override
    protected void computeSpecificMetadata(AllFile metadata, ByteBuffer datagram, long epochTime,
                                           SwathIdentifier swathId, int serialNumber, DatagramPosition position) {
        Depth68Metadata specificMetadata = getSpecificMetadata(metadata);
        Depth68CompletePingMetadata completing = specificMetadata.getPing(swathId);
        if (completing == null) {
            completing = new Depth68CompletePingMetadata(metadata.getInstallation().getSerialNumbers());
            specificMetadata.addPing(swathId, completing);
        }

        completing.addAntenna(serialNumber,
                new Depth68CompleteAntennaPingMetadata(position, getBeamCount(datagram).getU()));

        // Calcul de stats
        float zResolution = getZResolution(datagram).getInt() / 100.0f;
        float transducerDepth = getTransmitTransducerDepth(datagram).getU() / 100.0f;
        if ((metadata.getInstallation().getModelNumber() == 120)
                || (metadata.getInstallation().getModelNumber() == 300)) {
            for (int i = 0; i < getValidDetectionCount(datagram).getU(); i++) {
                specificMetadata.addDepth(getDepth2U(datagram, i).getU() * zResolution + transducerDepth, epochTime);
            }
        } else {
            for (int i = 0; i < getValidDetectionCount(datagram).getU(); i++) {
                specificMetadata.addDepth(getDepth2S(datagram, i).get() * zResolution + transducerDepth, epochTime);
            }
        }
    }

    public static UShort getHeadingOfVessel(ByteBuffer datagram) {
        return TypeDecoder.read2U(datagram, 16);
    }

    public static UShort getSoundSpeedAtTx(ByteBuffer datagram) {
        return TypeDecoder.read2U(datagram, 18);
    }

    public static UShort getTransmitTransducerDepth(ByteBuffer datagram) {
        return TypeDecoder.read2U(datagram, 20);
    }

    public static UByte getBeamCount(ByteBuffer datagram) {
        return TypeDecoder.read1U(datagram, 22);
    }

    public static UByte getValidDetectionCount(ByteBuffer datagram) {
        return TypeDecoder.read1U(datagram, 23);
    }

    public static UByte getZResolution(ByteBuffer datagram) {
        return TypeDecoder.read1U(datagram, 24);
    }

    public static UByte getXYResolution(ByteBuffer datagram) {
        return TypeDecoder.read1U(datagram, 25);
    }

    public static UShort getSamplingFrequency(ByteBuffer datagram) {
        int modelNum = getModelNumber(datagram);
        if ((modelNum >= 3002) && (modelNum <= 3008)) {
            throw new RuntimeException("Invalid model");
        } else {
            return TypeDecoder.read2U(datagram, 26);
        }
    }

    public static SShort getDepthDiffBetweenSonarHeads(ByteBuffer datagram) {
        int modelNum = getModelNumber(datagram);
        if ((modelNum < 3002) || (modelNum > 3008)) {
            throw new RuntimeException("Invalid model");
        } else {
            return TypeDecoder.read2S(datagram, 26);
        }
    }

    public static SShort getDepth2S(ByteBuffer datagram, int beamCounter) {
        // Signed value if Sounder not EM 120 and EM300
        return TypeDecoder.read2S(datagram, 28 + 16 * beamCounter);
    }

    public static UShort getDepth2U(ByteBuffer datagram, int beamCounter) {
        // Unsigned value for EM 120 and EM300
        return TypeDecoder.read2U(datagram, 28 + 16 * beamCounter);
    }

    public static SShort getAcrossDistance(ByteBuffer datagram, int beamCounter) {
        return TypeDecoder.read2S(datagram, 30 + 16 * beamCounter);
    }

    public static SShort getAlongDistance(ByteBuffer datagram, int beamCounter) {
        return TypeDecoder.read2S(datagram, 32 + 16 * beamCounter);
    }

    public static SShort getBeamDepressionAngle(ByteBuffer datagram, int beamCounter) {
        return TypeDecoder.read2S(datagram, 34 + 16 * beamCounter);
    }

    public static UShort getBeamAzimuthAngle(ByteBuffer datagram, int beamCounter) {
        return TypeDecoder.read2U(datagram, 36 + 16 * beamCounter);
    }

    public static UShort getRange(ByteBuffer datagram, int beamCounter) {
        return TypeDecoder.read2U(datagram, 38 + 16 * beamCounter);
    }

    public static UByte getQualityFactor(ByteBuffer datagram, int beamCounter) {
        return TypeDecoder.read1U(datagram, 40 + 16 * beamCounter);
    }

    public static UByte getLengthOfDetectionWindow(ByteBuffer datagram, int beamCounter) {
        return TypeDecoder.read1U(datagram, 41 + 16 * beamCounter);
    }

    public static SByte getReflectivity(ByteBuffer datagram, int beamCounter) {
        return TypeDecoder.read1S(datagram, 42 + 16 * beamCounter);
    }

    public static UByte getBeamNumber(ByteBuffer datagram, int beamCounter) {
        return TypeDecoder.read1U(datagram, 43 + 16 * beamCounter);
    }

    public static SByte getTransducerDepthOffsetMultiplier(ByteBuffer datagram) {
        return TypeDecoder.read1S(datagram, 44 + getBeamCount(datagram).getU() * 16);
    }
}
