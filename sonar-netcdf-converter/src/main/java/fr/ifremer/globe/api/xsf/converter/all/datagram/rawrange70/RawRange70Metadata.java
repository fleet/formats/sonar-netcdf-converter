package fr.ifremer.globe.api.xsf.converter.all.datagram.rawrange70;

import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.AbstractBeamDatagramMetadata;

public class RawRange70Metadata extends AbstractBeamDatagramMetadata<RawRange70CompletePingMetadata> {

	@Override
	public int getTxSectorCount() {
		return 1;
	}
}
