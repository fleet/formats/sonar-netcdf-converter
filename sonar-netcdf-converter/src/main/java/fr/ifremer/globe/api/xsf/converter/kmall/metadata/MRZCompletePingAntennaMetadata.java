package fr.ifremer.globe.api.xsf.converter.kmall.metadata;

import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.AbstractCompleteAntennaPingSingleMetadata;

public class MRZCompletePingAntennaMetadata extends AbstractCompleteAntennaPingSingleMetadata{

    private int extraDetectionCount;
    private int maxExtraDetectionSeabedSampleCount;
    
    public MRZCompletePingAntennaMetadata(DatagramPosition position, int detectionCount, int extraDetectionCount, int maxSeabedSampleCount, int maxExtraDetectionSeabedSampleCount) {
        super(position, detectionCount, maxSeabedSampleCount);
        this.extraDetectionCount = extraDetectionCount;
        this.maxExtraDetectionSeabedSampleCount = maxExtraDetectionSeabedSampleCount;
    }


    public int getExtraDetectionCount() {
        return extraDetectionCount;
    }

    public int getMaxExtraDetectionSeabedSampleCount() {
        return maxExtraDetectionSeabedSampleCount;
    }
}
