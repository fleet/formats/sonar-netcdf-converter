package fr.ifremer.globe.api.xsf.converter.s7k.datagram;

import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7K7009Metadata;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KFile;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.DDouble;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.api.xsf.util.types.UInt;
import fr.ifremer.globe.api.xsf.util.types.UShort;

public class S7K7009 extends S7KMultipingDatagram {

	@Override
	public SwathIdentifier genSwathIdentifier(S7KFile stats, DatagramBuffer buffer, long time) {
		return stats.getSwathId(getPingNumber(buffer).getU(), getMultiPingSequence(buffer).getU(), time);
	}
	
	@Override
	public void computeSpecificMetadata(S7KFile metadata, DatagramBuffer buffer, long type,
			DatagramPosition position, long time, SwathIdentifier swathId) {
		getSpecificMetadata(metadata).getOrCreateMetadataForDevice(getDeviceIdentifier(buffer).getU()).registerSwath(position, getPingNumber(buffer).getU(), getMultiPingSequence(buffer).getU(), 1,  getOptionalDataOffset(buffer).getU() != 0);
	}

	@Override
	public S7K7009Metadata getSpecificMetadata(S7KFile metadata) {
		return metadata.get7009Metadata();
	}
	
	public static FFloat getFrequency(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 0);
	}
	
	public static UInt getPingNumber(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4U(buffer.byteBufferData, 4);
	}
	
	public static UShort getMultiPingSequence(BaseDatagramBuffer buffer) {
		return TypeDecoder.read2U(buffer.byteBufferData, 8);
	}
	
	public static DDouble getLatitude(BaseDatagramBuffer buffer) {
		return TypeDecoder.read8F(buffer.byteBufferData, 10);
	}
	
	public static DDouble getLongitude(BaseDatagramBuffer buffer) {
		return TypeDecoder.read8F(buffer.byteBufferData, 18);
	}
	
	public static FFloat getHeading(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 26);
	}
	
	public static FFloat getAlongTrackDistance(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 30);
	}

	public static FFloat getAcrossTrackDistance(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 34);
	}
	
	public static FFloat getDepth(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 38);
	}
}
