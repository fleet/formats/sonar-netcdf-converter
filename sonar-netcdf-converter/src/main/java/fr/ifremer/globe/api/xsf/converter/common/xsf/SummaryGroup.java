package fr.ifremer.globe.api.xsf.converter.common.xsf;

import java.time.Instant;

import fr.ifremer.globe.api.xsf.converter.common.sounder.ISounderFile;
import fr.ifremer.globe.api.xsf.converter.common.utils.DoubleStatCalculator;
import fr.ifremer.globe.api.xsf.converter.common.utils.FloatStatCalculator;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCFile;
import fr.ifremer.globe.netcdf.ucar.NCGroup;

public class SummaryGroup extends NCGroup {

	protected SummaryGroup(NCFile parent, ISounderFile proxy, XsfBase file) throws NCException {
		super("summary",parent);
		
		/** Global **/
        addAttribute("model", proxy.getModelNumber());
        addAttribute("swath_count", proxy.getSwathCount());
        addAttribute("bathy_beams_count",proxy.getDetectionCount() );
        addAttribute("wc_beams_count", proxy.getWcBeamCount() );
        addAttribute("seabed_samples_count", proxy.getMaxSeabedSampleCount());
        addAttribute("rx_antennas_count", proxy.getRxAntennaNb());
        addAttribute("tx_antennas_count", proxy.getTxAntennaNb());
        
        /** Dates (ISO-8601 representation) **/
        addAttribute("swath_date_min", Instant.ofEpochMilli(proxy.getMinDate()).toString());
        addAttribute("swath_date_max", Instant.ofEpochMilli(proxy.getMaxDate()).toString());
		
		/** Attitude **/
		NCGroup attGrp = addGroup("attitude");
		attGrp.addAttribute("min_heading_deg", proxy.getHeadingMin());
		attGrp.addAttribute("max_heading_deg", proxy.getHeadingMax());
		attGrp.addAttribute("mean_heading_deg", proxy.getHeadingMean());
		attGrp.addAttribute("min_roll_deg", proxy.getRollMin());
		attGrp.addAttribute("max_roll_deg", proxy.getRollMax());
		attGrp.addAttribute("mean_roll_deg", proxy.getRollMean());
		attGrp.addAttribute("min_pitch_deg", proxy.getPitchMin());
		attGrp.addAttribute("max_pitch_deg", proxy.getPitchMax());
		attGrp.addAttribute("mean_pitch_deg", proxy.getPitchMean());
		attGrp.addAttribute("min_heave_m", proxy.getHeaveMin());
		attGrp.addAttribute("max_heave_m", proxy.getHeaveMax());
		attGrp.addAttribute("mean_heave_m", proxy.getHeaveMean());
		
		/** Seabed **/
		NCGroup bsGrp = addGroup("seabed");
		bsGrp.addAttribute("bs_min_dB", proxy.getBsMin());
		bsGrp.addAttribute("bs_max_dB", proxy.getBsMax());
		bsGrp.addAttribute("bs_mean_dB", proxy.getBsMean());

		/** Surface sound speed */
		NCGroup sssGrp = addGroup("surface_sound_speed");
		sssGrp.addAttribute("surface_sound_speed_min_m_sec", proxy.getSspMin());
		sssGrp.addAttribute("surface_sound_speed_max_m_sec", proxy.getSspMax());
		sssGrp.addAttribute("surface_sound_speed_mean_m_sec", proxy.getSspMean());
		
	
        
        addAttribute("SOG_min_meters_secs", proxy.getSOGMin());
        addAttribute("SOG_max_meters_sec", proxy.getSOGMax());
        
        computeStats(file);
       	
       /**.kmall specific data not yet handled:  
         		absCoefMin=0,
				absCoefMax=0, 
				absCoefMean=0,
				tempMin=0,
				tempMax=0,
				tempMean=0,
				salMin=0,
				salMax=0,
				salMean=0;
        
       .all specific data not yet handled: 
			heightMin = stats.getHeight().getHeightMin();
			heightMax = stats.getHeight().getHeightMax();
			heightMean = stats.getHeight().getHeightMean();**/
	}
	
	private void computeStats(XsfBase file) throws NCException
	{
		//get position Group
		DoubleStatCalculator latStat=new DoubleStatCalculator();
		FloatStatCalculator depthStat=new FloatStatCalculator();
		DoubleStatCalculator longStat=new DoubleStatCalculator();
		for(long swath=0;swath<file.getBeamGroup().getDim_ping_time().getLength();swath++)
		{
			if (file.getBathyGrp()!=null && file.getBathyGrp().getDim_detection()!=null)
			{
				int detectionDim=(int) file.getBathyGrp().getDim_detection().getLength();
				//read depth
				float depth[]=file.getBathyGrp().getDetection_z().get_float(new long[]{swath,0}, new long[] {1,detectionDim});
				double latitude[]=file.getBathyGrp().getDetection_latitude().get_double(new long[]{swath,0}, new long[] {1,detectionDim});
				double longitude[]=file.getBathyGrp().getDetection_longitude().get_double(new long[]{swath,0}, new long[] {1,detectionDim});
				byte status[]=file.getBathyGrp().getStatus().get_byte(new long[]{swath,0}, new long[] {1,detectionDim});
				for(int detection=0;detection<detectionDim;detection++)
				{
					if(status[detection]==0) //valid sounding
					{
						latStat.register(latitude[detection]);
						depthStat.register(depth[detection]);
						longStat.register(longitude[detection]);
					}
				}
				
			}else 
				
			{
				
			}
		}
		 /** Bathy **/
		NCGroup bathyGrp = addGroup("bathy");
		bathyGrp.addAttribute("min_depth_m", depthStat.getMin());
		bathyGrp.addAttribute("max_depth_m", depthStat.getMax());
		bathyGrp.addAttribute("mean_depth_m", depthStat.getMean());
		/** Bounding box **/
		NCGroup bboxGrp = addGroup("bounding_box");
        bboxGrp.addAttribute("latitude_min_deg", latStat.getMin());
        bboxGrp.addAttribute("latitude_max_deg", latStat.getMax());
        bboxGrp.addAttribute("longitude_min_deg", longStat.getMin());
        bboxGrp.addAttribute("longitude_max_deg", longStat.getMax());
	}
}
