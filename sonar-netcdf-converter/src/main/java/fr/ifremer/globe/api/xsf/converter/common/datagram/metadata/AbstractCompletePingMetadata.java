package fr.ifremer.globe.api.xsf.converter.common.datagram.metadata;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.netcdf.util.Pair;

/**
 * Base for a complete ping, across multiple antennas.
 *
 * @param <T>
 */
public abstract class AbstractCompletePingMetadata<T extends AbstractCompleteAntennaPingMetadata> {

	/** map one antenna serial number to the corresponding metadata */
	protected Map<Integer, T> antennas;

	/** total number of beams across all the antennas */
	protected int beamCount;

	/** max number of abstract samples for one beam. Contains WC or Seabed sample count*/
	protected int maxSampleCount;

	/** total number of tx sectors. Not used for every datagrams. */
	protected int txSectorCount;

	/** Array of antenna's serial numbers. Used to keep order **/
	protected int[] serialNumbers;

	/** default constructor */
	public AbstractCompletePingMetadata() {
		this(0, new int[] {});
	}

	/**
	 * constructor that sets the number of tx sectors detected
	 * 
	 * @param txSectorCount
	 */
	public AbstractCompletePingMetadata(int txSectorCount, int[] serialNumbers) {
		antennas = new TreeMap<>();
		this.txSectorCount = txSectorCount;
		this.serialNumbers = serialNumbers;
	}

	/**
	 * Apply new serial numbers. Used to change antenna order.
	 * 
	 * @param Array of antenna's serial numbers.
	 */
	public void setSerialNumbers(int[] serialNumbers) {
		this.serialNumbers = serialNumbers;
	}

	public int getBeamCount() {
		return beamCount;
	}

	public int getMaxAbstractSampleCount() {
		return maxSampleCount;
	}

	public Map<Integer, T> getAntennas() {
		return antennas;
	}

	/**
	 * retrieve antenna specific metadata if exists, null if no metadata is known
	 * 
	 * @param serial
	 * @return the antenna specific metadata, or null if missing
	 */
	public T getAntenna(int serial) {
		return antennas.get(serial);
	}

	/**
	 * register metadata for a missing antenna
	 * 
	 * @param serial
	 * @param antenna
	 */
	public void addAntenna(int serial, T antenna) {
		this.antennas.put(serial, antenna);
		// check if serial is recorded
		boolean found = false;
		for (int value : this.serialNumbers) {
			if (value == serial) {
				found = true;
			}
		}
		// if not recorded we add it to the list of known serial numbers
		if (!found) {
			serialNumbers = Arrays.copyOf(serialNumbers, serialNumbers.length + 1);
			serialNumbers[serialNumbers.length - 1] = serial;
		}
	}

	/**
	 * Tells if the ping is complete (all antennas have all the data)
	 * 
	 * @param allAntennas
	 * @return
	 */
	public boolean isComplete(Set<Integer> allAntennas) {
		boolean valid = true;
		// Check All required fans are present in the current ping
		for (int antenna : allAntennas) {
			valid &= antennas.containsKey(antenna);
		}
		// Check All required datagrams are present for each fans (datagram partition)
		for (T antenna : antennas.values()) {
			valid &= antenna.isComplete();
		}
		return valid;
	}

	/**
	 * called after the first pass of indexing is finished, it aggregates data
	 */
	public void computeIndex() {
		beamCount = 0;
		maxSampleCount = 0;
		
		for (T antenna : antennas.values()) {
			if (antenna.isComplete()) {
				beamCount += antenna.getBeamCount();
				maxSampleCount = Math.max(maxSampleCount, antenna.getMaxAbstractSampleCount());
			}
		}
	}

	public int getBeamOffset(int serialNumber) {
		// No beam offset if only one antenna...
		if (antennas.size() == 1)
			return 0;
		int ret = 0;
		for (int sN : serialNumbers) {
			if (sN == serialNumber) {
				break;
			}
			T a = antennas.get(sN);

			if (a == null) {
				// Case of file
				a = antennas.get(antennas.keySet().iterator().next());
			}

			ret += a.getBeamCount();
		}
		return ret;
	}

	public List<Pair<DatagramPosition, T>> locate() throws IOException {
		List<Pair<DatagramPosition, T>> ret = new LinkedList<Pair<DatagramPosition, T>>();

		for (T antenna : antennas.values()) {
			for (DatagramPosition p : antenna.locate()) {
				ret.add(new Pair<DatagramPosition, T>(p, antenna));
			}
		}
		return ret;
	}

	public int getNumTxSector() {
		return this.txSectorCount;
	}
}
