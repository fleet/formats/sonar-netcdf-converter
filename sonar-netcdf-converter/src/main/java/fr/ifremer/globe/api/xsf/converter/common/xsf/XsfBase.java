package fr.ifremer.globe.api.xsf.converter.common.xsf;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Optional;

import fr.ifremer.globe.api.xsf.converter.XsfConverterParameters;
import fr.ifremer.globe.api.xsf.converter.common.sounder.ISounderFile;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.AttitudeGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.AttitudeSubGroup;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.BathymetryGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.BeamGroup1Grp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.DynamicDraughtGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.EnvironmentGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.PlatformGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.PositionGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.PositionSubGroup;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.ProvenanceGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.RootGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.SonarGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.SoundSpeedProfileGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.SurfaceSoundSpeedGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.TideGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.VendorSpecificGrp;
import fr.ifremer.globe.netcdf.jna.Nc4prototypes;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCFile;
import fr.ifremer.globe.netcdf.ucar.NCGroup;
import fr.ifremer.globe.utils.maths.Vector3D;

public class XsfBase implements AutoCloseable {

	public static final String SUFFIX_XSF = "xsf.nc";
	public static final String EXTENSION_XSF = "." + SUFFIX_XSF;

	public static final String DEFAULT_BEAM_GROUP_NAME = "Beam_group1";

	protected NCFile file;
	private NCGroup summaryGrp;
	private SonarGrp sonarGrp;
	private BeamGroup1Grp beamGrp;
	private NCGroup environment;
	private SoundSpeedProfileGrp soundSpeedProfileGrp;
	private Optional<SurfaceSoundSpeedGrp> surfaceSoundSpeedGrp;
	private TideGrp tideGrp;
	private DynamicDraughtGrp dynamicDraughtGrp;
	private PlatformGrp platformGrp;
	private PositionGrp positionGrp;
	private AttitudeGrp attitudeGrp;
	private VendorSpecificGrp platformVendorGrp;
	private VendorSpecificGrp soundSpeedProfileVendor;
	private VendorSpecificGrp environmentVendor;
	private BathymetryGrp bathyGrp;

	private ISounderFile sounderFile;
	private RootGrp rootGrp;
	private ProvenanceGrp provenanceGrp;

	/**
	 * Constructor
	 */
	public XsfBase(ISounderFile sounderFile, XsfConverterParameters params) throws NCException, IOException {
		this.sounderFile = sounderFile;
		file = NCFile.createNew(NCFile.Version.netcdf4, params.getOutFilePath(), true);

		// initialize root group and its metadata
		rootGrp = new RootGrp(file, sounderFile, new HashMap<String, Integer>());

		// set external parameters
		String sounderModel = sounderFile.getSounderDescription().getName();
		rootGrp.addAttribute(RootGrp.ATT_TITLE, params.getTitle(sounderModel));
		rootGrp.addAttribute(RootGrp.ATT_SUMMARY, params.getSummary(sounderModel));
		rootGrp.addAttribute(RootGrp.ATT_KEYWORDS, params.getKeywords(sounderModel));
		rootGrp.addAttribute(RootGrp.ATT_LICENSE, params.getLicense(sounderModel));
		rootGrp.addAttribute(RootGrp.ATT_RIGHTS, params.getRights(sounderModel));

		// creation date, see documentation :
		// "Timestamp of file creation in ISO8601:2004 extended format, including the time zone (e.g.
		// 2017-05-06T20:21:35Z)."
		rootGrp.addAttribute(RootGrp.ATT_DATE_CREATED, ZonedDateTime.now().toString());

		// Environnement
		environment = new EnvironmentGrp(file, sounderFile, new HashMap<String, Integer>());
		environmentVendor = new VendorSpecificGrp(environment, sounderFile);
		soundSpeedProfileGrp = new SoundSpeedProfileGrp(environment, sounderFile, new HashMap<String, Integer>());
		surfaceSoundSpeedGrp = Optional.empty();
		if (sounderFile.getSurfaceSoundSpeedCount() > 0)
			surfaceSoundSpeedGrp = Optional
					.of(new SurfaceSoundSpeedGrp(environment, sounderFile, new HashMap<String, Integer>()));
		tideGrp = new TideGrp(environment, sounderFile, new HashMap<String, Integer>());

		// Platform
		platformGrp = new PlatformGrp(file, sounderFile, new HashMap<String, Integer>());
		positionGrp = new PositionGrp(platformGrp, sounderFile, new HashMap<String, Integer>());
		attitudeGrp = new AttitudeGrp(platformGrp, sounderFile, new HashMap<String, Integer>());
		dynamicDraughtGrp = new DynamicDraughtGrp(platformGrp, sounderFile, new HashMap<String, Integer>());

		// Provenance
		provenanceGrp = new ProvenanceGrp(file, sounderFile, new HashMap<String, Integer>());

		// Sonar
		sonarGrp = makeSonarGrp(sounderFile);
		beamGrp = makeBeamGrp(sounderFile);
		bathyGrp = new BathymetryGrp(beamGrp, sounderFile, new HashMap<String, Integer>());

		platformVendorGrp = new VendorSpecificGrp(platformGrp, sounderFile);
	}

	public ProvenanceGrp getProvenanceGrp() {
		return provenanceGrp;
	}

	/** Create SonarGrp, override this function if needed to override type definition */
	protected SonarGrp makeSonarGrp(ISounderFile sounderFile) throws NCException {
		return new SonarGrp(file, sounderFile, new HashMap<String, Integer>());
	}

	/** Create BeamGrp, override this function if needed to override type definition */
	protected BeamGroup1Grp makeBeamGrp(ISounderFile sounderFile) throws NCException {
		HashMap<String, Integer> typeDict = new HashMap<>();
		typeDict.put("sample_t", Nc4prototypes.NC_BYTE);
		return new BeamGroup1Grp(sonarGrp, sounderFile, DEFAULT_BEAM_GROUP_NAME, typeDict);
	}

	@Override
	public void close() {
		if (file != null) {
			file.close();
		}
	}

	/**
	 * Compute summary group
	 */
	public void computeSummary() throws NCException {
		new SummaryGroup(file, sounderFile, this);
	}

	/** Getters **/

	public NCGroup getSummaryGrp() {
		return summaryGrp;
	}

	public BeamGroup1Grp getBeamGroup() {
		return beamGrp;
	}

	public SoundSpeedProfileGrp getSoundSpeedProfileGrp() {
		return soundSpeedProfileGrp;
	}

	public VendorSpecificGrp getSoundSpeedProfileVendor() throws NCException {
		if (soundSpeedProfileVendor == null) {
			soundSpeedProfileVendor = new VendorSpecificGrp(soundSpeedProfileGrp, sounderFile);
		}
		return soundSpeedProfileVendor;
	}

	public VendorSpecificGrp getPlatformVendorGrp() {
		return platformVendorGrp;
	}

	public PositionSubGroup addPositionGrp(String id, int nEntries) throws NCException {
		return new PositionSubGroup(positionGrp, sounderFile, id, new HashMap<String, Integer>(), nEntries);
	}

	public AttitudeSubGroup addAttitudeGrp(String id, long nEntries) throws NCException {
		return new AttitudeSubGroup(attitudeGrp, sounderFile, id, new HashMap<String, Integer>(), nEntries);
	}

	public Optional<SurfaceSoundSpeedGrp> getSurfaceSoundSpeedGrp() {
		return surfaceSoundSpeedGrp;
	}

	public VendorSpecificGrp getEnvironmentVendor() {
		return environmentVendor;
	}

	public AttitudeGrp getAttitudeGrp() {
		return attitudeGrp;
	}

	public PlatformGrp getPlatformGrp() {
		return platformGrp;
	}

	public SonarGrp getSonarGrp() {
		return sonarGrp;
	}

	public NCGroup getEnvironment() {
		return environment;
	}

	public TideGrp getTideGrp() {
		return tideGrp;
	}

	public DynamicDraughtGrp getDynamicDraughtGrp() {
		return dynamicDraughtGrp;
	}

	public PositionGrp getPositionGrp() {
		return positionGrp;
	}

	public BathymetryGrp getBathyGrp() {
		return bathyGrp;
	}

	public RootGrp getRootGrp() {
		return rootGrp;
	}

	public AttitudeSubGroup getPreferredAttitudeSubGroup() throws NCException {
		long[] attitudeIndexStart = { beamGrp.getAttributeInt(BeamGroup1Grp.ATT_PREFERRED_MRU) };
		long[] attitudeIndexCount = { 1 };

		String activeAttitudeId = platformGrp.getMRU_ids().get_string(attitudeIndexStart, attitudeIndexCount)[0];
		return (AttitudeSubGroup) getAttitudeGrp().getGroup(activeAttitudeId);
	}

	public PositionSubGroup getPreferredPositionSubGroup() throws NCException {
		long[] positionIndexStart = { beamGrp.getAttributeInt(BeamGroup1Grp.ATT_PREFERRED_POSITION) };
		long[] positionIndexCount = { 1 };

		String activePositionId = platformGrp.getPosition_ids().get_string(positionIndexStart, positionIndexCount)[0];
		return (PositionSubGroup) getPositionGrp().getGroup(activePositionId);
	}

	public Vector3D getPreferredPositionOffset() throws NCException {
		return getPositionOffset(beamGrp.getAttributeInt(BeamGroup1Grp.ATT_PREFERRED_POSITION));
	}

	public Vector3D getPositionOffset(int index) throws NCException {
		long[] positionIndexStart = { index };
		long[] positionIndexCount = { 1 };

		return new Vector3D(platformGrp.getPosition_offset_x().get_float(positionIndexStart, positionIndexCount)[0],
				platformGrp.getPosition_offset_y().get_float(positionIndexStart, positionIndexCount)[0],
				platformGrp.getPosition_offset_z().get_float(positionIndexStart, positionIndexCount)[0]);
	}

}
