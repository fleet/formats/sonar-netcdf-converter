package fr.ifremer.globe.api.xsf.converter.s7k.datagram;

import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KFile;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KUnknownDatagramMetadata;

public class S7KUnknownDatagram extends S7KBaseDatagram {

	@Override
	public void computeSpecificMetadata(S7KFile metadata, DatagramBuffer buffer, long type,
			DatagramPosition position, long time) {
		getSpecificMetadata(metadata).pushUnknownType(type);
	}

	@Override
	public S7KUnknownDatagramMetadata getSpecificMetadata(S7KFile metadata) {
		return metadata.getUnknownMetadata();
	}

}
