package fr.ifremer.globe.api.xsf.converter.kmall.xsf;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import fr.ifremer.globe.api.xsf.converter.all.datagram.IndividualSensorMetadata;
import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.utils.Namer;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2F4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2F8;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2U1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2U2;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2U4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2U8;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfFromKongsberg;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.AttitudeSubGroup;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.AttitudeSubGroupVendorSpecificGrp;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.KmAllBaseDatagram;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.SKM;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.KmallFile;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.SKMMetadata;
import fr.ifremer.globe.api.xsf.util.SimpleIdentifier;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.api.xsf.util.types.UByte;
import fr.ifremer.globe.api.xsf.util.types.ULong;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.date.DateUtils;

public class SKMConverter implements IDatagramConverter<KmallFile, XsfFromKongsberg> {

	private SKMMetadata metadata;
	private List<AttitudeSensorGroup> subgroups = new LinkedList<>();

	/** Constructor **/
	public SKMConverter(KmallFile stats) {
		this.metadata = stats.getSKMMetadata();
	}

	@Override
	public void declare(KmallFile allFile, XsfFromKongsberg xsf) throws NCException {
		metadata.forEachSensor((key, sensorMetadata) -> subgroups.add(new AttitudeSensorGroup(key, sensorMetadata)));
		for (AttitudeSensorGroup sensorGr : subgroups)
			sensorGr.declare(allFile, xsf);
	}

	@Override
	public void fillData(KmallFile kmAllFile) throws IOException, NCException {
		for (AttitudeSensorGroup sensorGr : subgroups)
			sensorGr.fillData(kmAllFile);
	}

	/**
	 * Attitude sub group (one per sensor)
	 */
	private class AttitudeSensorGroup implements IDatagramConverter<KmallFile, XsfFromKongsberg> {
		private List<ValueD2> values;
		private final IndividualSensorMetadata sensorMetadata;
		private final UByte sensorSystemId;

		public AttitudeSensorGroup(UByte sensorSystemId, IndividualSensorMetadata sensorMetadata) {
			this.sensorSystemId = sensorSystemId;
			this.sensorMetadata = sensorMetadata;
			this.values = new LinkedList<>();
		}

		@Override
		public void declare(KmallFile kmAllFile, XsfFromKongsberg xsf) throws NCException {

			/**********************************************************************************************************
			 * xsf common variables
			 **********************************************************************************************************/

			AttitudeSubGroup attitudeGrp = xsf.addAttitudeGrp(Namer.getAttitudeSubGroupName(sensorSystemId.getU()),
					(int) sensorMetadata.getTotalNumberOfEntries());
			// WE use Value2D to store data that will be effectively store in a 1D variable, values for some values will
			// be duplicated (like serial number and so on)

			// time
			values.add(new ValueD2U8(attitudeGrp.getTime(), sensorMetadata.getMaxEntriesPerDatagram(),
					(buffer, i) -> new ULong(
							DateUtils.secondToNano(SKM.getTimeFromKMBinary_sec(buffer.byteBufferData, i).getU())
									+ SKM.getTimeFromKMBinary_nanosec(buffer.byteBufferData, i).getU())));

			// attitude
			values.add(new ValueD2F4(attitudeGrp.getRoll(), sensorMetadata.getMaxEntriesPerDatagram(),
					(buffer, i) -> SKM.getRoll_deg(buffer.byteBufferData, i)));

			values.add(new ValueD2F4(attitudeGrp.getPitch(), sensorMetadata.getMaxEntriesPerDatagram(),
					(buffer, i) -> SKM.getPitch_deg(buffer.byteBufferData, i)));

			values.add(new ValueD2F4(attitudeGrp.getHeading(), sensorMetadata.getMaxEntriesPerDatagram(),
					(buffer, i) -> SKM.getHeading_deg(buffer.byteBufferData, i)));

			// rates
			values.add(new ValueD2F4(attitudeGrp.getRoll_rate(), sensorMetadata.getMaxEntriesPerDatagram(),
					(buffer, i) -> SKM.getRollRate(buffer.byteBufferData, i)));

			values.add(new ValueD2F4(attitudeGrp.getPitch_rate(), sensorMetadata.getMaxEntriesPerDatagram(),
					(buffer, i) -> SKM.getPitchRate(buffer.byteBufferData, i)));

			values.add(new ValueD2F4(attitudeGrp.getHeading_rate(), sensorMetadata.getMaxEntriesPerDatagram(),
					(buffer, i) -> SKM.getYawRate(buffer.byteBufferData, i)));

			// heave
			values.add(new ValueD2F4(attitudeGrp.getVertical_offset(), sensorMetadata.getMaxEntriesPerDatagram(),
					(buffer, i) -> new FFloat(SKM.getHeave_m(buffer.byteBufferData, i).get() * -1.f)));

			/**********************************************************************************************************
			 * .kmall specific variables
			 **********************************************************************************************************/
			AttitudeSubGroupVendorSpecificGrp attitudeVendorSubGroup = new AttitudeSubGroupVendorSpecificGrp(
					attitudeGrp, kmAllFile, new HashMap<String, Integer>());

			values.add(
					new ValueD2U8(attitudeVendorSubGroup.getDatagram_time(), sensorMetadata.getMaxEntriesPerDatagram(),
							(buffer, i) -> new ULong(KmAllBaseDatagram.getEpochTimeNano(buffer.byteBufferData))));

			values.add(new ValueD2U1(attitudeVendorSubGroup.getSystem_serial_number(),
					sensorMetadata.getMaxEntriesPerDatagram(),
					(buffer, i) -> SKM.getSensorSystem(buffer.byteBufferData)));

			values.add(new ValueD2U1(attitudeVendorSubGroup.getKmall_sensor_status(),
					sensorMetadata.getMaxEntriesPerDatagram(),
					(buffer, i) -> SKM.getSensorStatus(buffer.byteBufferData)));

			values.add(new ValueD2U2(attitudeVendorSubGroup.getSensor_input_format(),
					sensorMetadata.getMaxEntriesPerDatagram(),
					(buffer, i) -> SKM.getSensorInputFormat(buffer.byteBufferData)));

			values.add(new ValueD2U2(attitudeVendorSubGroup.getDgm_version(), sensorMetadata.getMaxEntriesPerDatagram(),
					(buffer, i) -> SKM.getDgmVersionFromKMBinary(buffer.byteBufferData, i)));

			values.add(new ValueD2U4(attitudeVendorSubGroup.getStatus_from_km_binary(),
					sensorMetadata.getMaxEntriesPerDatagram(),
					(buffer, i) -> SKM.getStatusFromKMBinary(buffer.byteBufferData, i)));

			// Position
			values.add(new ValueD2F8(attitudeVendorSubGroup.getLat(), sensorMetadata.getMaxEntriesPerDatagram(),
					(buffer, i) -> SKM.getLatitude_deg(buffer.byteBufferData, i)));

			values.add(new ValueD2F8(attitudeVendorSubGroup.getLongitude(), sensorMetadata.getMaxEntriesPerDatagram(),
					(buffer, i) -> SKM.getLongitude_deg(buffer.byteBufferData, i)));

			values.add(new ValueD2F4(attitudeVendorSubGroup.getEllipsoid_height(),
					sensorMetadata.getMaxEntriesPerDatagram(),
					(buffer, i) -> SKM.getEllipsoidHeight_m(buffer.byteBufferData, i)));

			// Velocities.
			values.add(new ValueD2F4(attitudeVendorSubGroup.getVel_north(), sensorMetadata.getMaxEntriesPerDatagram(),
					(buffer, i) -> SKM.getVelNorth(buffer.byteBufferData, i)));

			values.add(new ValueD2F4(attitudeVendorSubGroup.getVel_east(), sensorMetadata.getMaxEntriesPerDatagram(),
					(buffer, i) -> SKM.getVelEast(buffer.byteBufferData, i)));

			values.add(new ValueD2F4(attitudeVendorSubGroup.getVel_down(), sensorMetadata.getMaxEntriesPerDatagram(),
					(buffer, i) -> SKM.getVelDown(buffer.byteBufferData, i)));

			// Errors in data. Sensor data quality, as standard deviations.
			values.add(
					new ValueD2F4(attitudeVendorSubGroup.getLatitude_error(), sensorMetadata.getMaxEntriesPerDatagram(),
							(buffer, i) -> SKM.getLatitudeError_m(buffer.byteBufferData, i)));

			values.add(new ValueD2F4(attitudeVendorSubGroup.getLongitude_error(),
					sensorMetadata.getMaxEntriesPerDatagram(),
					(buffer, i) -> SKM.getLongitudeError_m(buffer.byteBufferData, i)));

			values.add(new ValueD2F4(attitudeVendorSubGroup.getEllipsoid_height_error(),
					sensorMetadata.getMaxEntriesPerDatagram(),
					(buffer, i) -> SKM.getEllipsoidHeightError_m(buffer.byteBufferData, i)));

			values.add(new ValueD2F4(attitudeVendorSubGroup.getRoll_error(), sensorMetadata.getMaxEntriesPerDatagram(),
					(buffer, i) -> SKM.getRollError_deg(buffer.byteBufferData, i)));

			values.add(new ValueD2F4(attitudeVendorSubGroup.getPitch_error(), sensorMetadata.getMaxEntriesPerDatagram(),
					(buffer, i) -> SKM.getPitchError_deg(buffer.byteBufferData, i)));

			values.add(
					new ValueD2F4(attitudeVendorSubGroup.getHeading_error(), sensorMetadata.getMaxEntriesPerDatagram(),
							(buffer, i) -> SKM.getHeadingError_deg(buffer.byteBufferData, i)));

			values.add(new ValueD2F4(attitudeVendorSubGroup.getHeave_error(), sensorMetadata.getMaxEntriesPerDatagram(),
					(buffer, i) -> SKM.getHeaveError_m(buffer.byteBufferData, i)));

			// Acceleration.
			values.add(new ValueD2F4(attitudeVendorSubGroup.getNorth_acceleration(),
					sensorMetadata.getMaxEntriesPerDatagram(),
					(buffer, i) -> SKM.getNorthAcceleration(buffer.byteBufferData, i)));

			values.add(new ValueD2F4(attitudeVendorSubGroup.getEast_acceleration(),
					sensorMetadata.getMaxEntriesPerDatagram(),
					(buffer, i) -> SKM.getEastAcceleration(buffer.byteBufferData, i)));

			values.add(new ValueD2F4(attitudeVendorSubGroup.getDown_acceleration(),
					sensorMetadata.getMaxEntriesPerDatagram(),
					(buffer, i) -> SKM.getDownAcceleration(buffer.byteBufferData, i)));

			// Structure KM Delayed heave
			values.add(new ValueD2U8(attitudeVendorSubGroup.getTime_from_kmdelayed_heave(),
					sensorMetadata.getMaxEntriesPerDatagram(),
					(buffer, i) -> new ULong(
							DateUtils.secondToNano(SKM.getTimeFromKMDelayedHeave_sec(buffer.byteBufferData, i).getU())
									+ SKM.getTimeFromKMDelayedHeave_nanosec(buffer.byteBufferData, i).getU())));

			values.add(
					new ValueD2F4(attitudeVendorSubGroup.getDelayed_heave(), sensorMetadata.getMaxEntriesPerDatagram(),
							(buffer, i) -> SKM.getDelayedHeave(buffer.byteBufferData, i)));

		}

		@Override
		public void fillData(KmallFile kmAllFile) throws IOException, NCException {
			long[] origin = new long[] { 0 };
			long[] count = new long[] { 0 };

			DatagramBuffer buffer = new DatagramBuffer();
			for (Entry<SimpleIdentifier, DatagramPosition> posentry : sensorMetadata.getDatagrams()) {
				values.forEach(ValueD2::clear);
				final DatagramPosition pos = posentry.getValue();
				DatagramReader.readDatagram(pos.getFile(), buffer, pos.getSeek());
				long nEntries = SKM.getNumSamplesArray(buffer.byteBufferData).getU();

				// read the data from the source file
				for (int i = 0; i < nEntries; i++) {
					for (ValueD2 value : this.values)
						value.fill(buffer, i, i);
				}

				// flush into the output file
				count[0] = nEntries;
				for (ValueD2 value : this.values) {
					try {
						value.write(origin, count);
					} catch (Exception e) {
						e.printStackTrace();
					}

				}

				origin[0] += nEntries;

			}

		}
	}

}
