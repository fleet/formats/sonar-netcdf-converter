package fr.ifremer.globe.api.xsf.converter.all.mbg.bathy;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.PingDatagram;
import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.all.datagram.qualityfactor.QualityFactor;
import fr.ifremer.globe.api.xsf.converter.all.datagram.qualityfactor.QualityFactorCompleteAntennaPingMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.qualityfactor.QualityFactorCompletePingMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.xyz88.XYZ88DepthCompleteAntennaPingMetadata;
import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgBase;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgConstants;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2S4;
import fr.ifremer.globe.api.xsf.util.DefaultLogger;
import fr.ifremer.globe.api.xsf.util.ObjectBufferPool;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.types.SInt;
import fr.ifremer.globe.netcdf.api.convention.CFStandardNames;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCVariable;
import fr.ifremer.globe.netcdf.util.Pair;

/**
 * Converter for quality factor datagram
 */
public class QualityFactorDatagramConverter implements IDatagramConverter<AllFile, MbgBase> {

	private List<ValueD2> swathDetectionValue = new LinkedList<>();

	@Override
	public void declare(AllFile allFile, MbgBase mbg) throws NCException {
		/** Quality **/
		NCVariable qualityVariable = mbg.getVariable(MbgConstants.Quality);
		double qualityFactor = qualityVariable.getAttributeDouble(CFStandardNames.CF_SCALE_FACTOR);
		double qualityOffset = qualityVariable.getAttributeDouble(CFStandardNames.CF_ADD_OFFSET);
		swathDetectionValue.add(new ValueD2S4(qualityVariable, (buffer, beamIndexSource) ->
		// retrieve Ifremer quality factor for the corresponding beam
		new SInt((int) Math.round(
				(QualityFactor.getSample(buffer.byteBufferData, beamIndexSource) - qualityOffset) / qualityFactor))));
	}

	@Override
	public void fillData(AllFile allFile) throws IOException, NCException {
		long[] origin = { 0, 0 };
		ObjectBufferPool<DatagramBuffer> pool = new ObjectBufferPool<>(
				() -> new DatagramBuffer(allFile.getByteOrder()));

		for (Entry<SwathIdentifier, QualityFactorCompletePingMetadata> pingEntry : allFile.getQualityFactor()
				.getEntries()) {
			final QualityFactorCompletePingMetadata ping = pingEntry.getValue();
			List<Pair<DatagramBuffer, QualityFactorCompleteAntennaPingMetadata>> data = ping.read(pool);

			// test for ping rejection
			if (!allFile.getGlobalMetadata().getSwathIds().contains(pingEntry.getKey())) {
				continue;
			}

			// clear buffer
			swathDetectionValue.forEach(ValueD2::clear);

			// get origin from swath identifier (ignore if matching index not found)
			var swathId = pingEntry.getKey();
			var optIndex = allFile.getGlobalMetadata().getSwathIds().getIndex(swathId);
			if (optIndex.isEmpty()) {
				DefaultLogger.get().warn(
						"Datagram QualityFactor associated with a missing ping is ignored (number = {}, sequence = {}).",
						swathId.getRawPingNumber(), swathId.getSwathPosition());
				continue;
			}
			origin[0] = optIndex.get();

			// if depth datagram is mission, we skip it
			if (allFile.getXyzdepth().getPing(pingEntry.getKey()) == null)
				continue;

			Map<Integer, XYZ88DepthCompleteAntennaPingMetadata> depthAntenna = allFile.getXyzdepth()
					.getPing(pingEntry.getKey()).getAntennas();

			// iterate over the antenna
			for (Pair<DatagramBuffer, QualityFactorCompleteAntennaPingMetadata> antenna : data) {
				DatagramBuffer buffer = antenna.getFirst();
				int antennaSerialNumber = PingDatagram.getSerialNumber(buffer.byteBufferData).getU();

				// if the current datagram is not present for depth, do not read it
				if (!depthAntenna.containsKey(antennaSerialNumber))
					continue;

				// compute beam offset from the current antenna
				Set<Integer> serialNumbers = allFile.getGlobalMetadata().getSerialNumbers();
				int beamOffset = (!serialNumbers.isEmpty() && antennaSerialNumber != serialNumbers.iterator().next())
						? QualityFactor.getBeamCount(buffer.byteBufferData).getU()
						: 0;

				// fill all the data based on their ping number
				int nBeam = QualityFactor.getBeamCount(buffer.byteBufferData).getU();
				for (int i = 0; i < nBeam; i++) {
					for (ValueD2 value : swathDetectionValue)
						value.fill(antenna.getFirst(), i, i + beamOffset);
				}
			}

			// write data in netcdf variable
			for (ValueD2 v : swathDetectionValue)
				v.write(origin, new long[] { 1, allFile.getDetectionCount() });

			// release the datagram buffers for the next ping
			for (Pair<DatagramBuffer, QualityFactorCompleteAntennaPingMetadata> antenna : data)
				pool.release(antenna.getFirst());
		}
	}

}
