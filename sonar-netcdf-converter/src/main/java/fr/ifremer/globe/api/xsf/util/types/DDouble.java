package fr.ifremer.globe.api.xsf.util.types;

public class DDouble implements Comparable<DDouble> {

	private double d;

	public static DDouble NAN = new DDouble(Double.NaN);

	public DDouble(double d) {
		this.d = d;
	}

	public double get() {
		return this.d;
	}

	@Override
	public int compareTo(DDouble o) {
		return Double.compare(d, o.d);
	}
}
