package fr.ifremer.globe.api.xsf.converter.kmall.datagram;

import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.DatagramMetadata;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.KmallFile;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.UByte;
import fr.ifremer.globe.api.xsf.util.types.UInt;
import fr.ifremer.globe.api.xsf.util.types.UShort;

import java.nio.ByteBuffer;

public abstract class KmAllBaseDatagram implements KmAllDatagram {
    public static final int HEADER_SIZE = 20; // size in octet of header definition struct EMdgmHeader_def
    public static final int DATAGRAMM_OFFSET = 4; // Buffer read and passed has an offset of DATAGRAMM_OFFSET (matching
    // the numBytesDgm variable read to allocate memory)
    public static final int PARTITION_SIZE = 4; // size in octet of partition definition struct EMdgmMpartition_def

    /**
     * Check if datagram version is higher or equal to the expected datagram Version if the datagram has a version
     * number less that
     *
     * @param expectedVersion : the minimum required datagram version
     */
    public static void checkMinimuVersion(int expectedVersion, ByteBuffer datagram) {
        int versionRead = getDgmVersion(datagram).getU();

        if (versionRead < expectedVersion)
            throw new AssertionError("Ready a datagram not avalaible in datagram MRZ (expected:"
                    + expectedVersion + " vs real version:" + versionRead + ")");
    }

    public static void dump4debug(ByteBuffer datagram) {
        System.err.println("EMdgmHeader_def");
        System.err.println("DgmType:" + getDgmType(datagram));
        System.err.println("DgmVersion:" + getDgmVersion(datagram).getU());
        System.err.println("SystemId:" + getSystemId(datagram).getU());
        System.err.println("EchoSounderId:" + getEchoSounderId(datagram).getU());
        System.err.println("TimeSec:" + getTimeSec(datagram).getU());
        System.err.println("TimeNano:" + getTimeNano(datagram).getU());
    }

    public void computeMetaData(KmallFile metadata, ByteBuffer datagram, String datagramType,
                                DatagramPosition position) {
        int modelNumber = getEchoSounderId(datagram).getU();
        // long dateKongsbergFormat=Integer.toUnsignedLong(datagram.getInt());
        // //time since midnight in milliseconds
        // long timeSinceMidnight=Integer.toUnsignedLong(datagram.getInt());
        // long epochTime=DateUtil.convertTime(dateKongsbergFormat,
        // timeSinceMidnight);

        long epochTime = getEpochTimeMilli(datagram);

        metadata.getGlobalMetadata().incDatagram();
        metadata.getGlobalMetadata().addTime(epochTime);
        metadata.getGlobalMetadata().setModelNumber(modelNumber);

        getSpecificMetadata(metadata).addDatagram();
        computeSpecificMetadata(metadata, datagram, datagramType, position);
    }

    public abstract void computeSpecificMetadata(KmallFile metadata, ByteBuffer datagram, String datagramType,
                                                 DatagramPosition position);

    @SuppressWarnings("rawtypes")
    public abstract DatagramMetadata getSpecificMetadata(KmallFile metadata);

    /////////////////////
    // EMdgmHeader struct
    /////////////////////

    /**
     * read datagram type
     *
     * @param buffer
     * @return
     */
    public static String getDgmType(ByteBuffer buffer) {
        final int position = buffer.position();
        buffer.position(0);
        byte[] dgmType = new byte[4];
        buffer.get(dgmType, 0, 4);
        buffer.position(position);
        return new String(dgmType);
    }

    /**
     * read datagram version
     *
     * @param buffer
     * @return
     */
    public static UByte getDgmVersion(ByteBuffer buffer) {
        return TypeDecoder.read1U(buffer, 4);
    }

    /**
     * Get system ID
     *
     * @param buffer
     * @return
     */
    public static UByte getSystemId(ByteBuffer buffer) {
        return TypeDecoder.read1U(buffer, 5);
    }

    /**
     * get echo sounder ID
     *
     * @param buffer
     * @return
     */
    public static UShort getEchoSounderId(ByteBuffer buffer) {
        return TypeDecoder.read2U(buffer, 6);
    }

    /**
     * @return epoch time in nanoseconds (from 1970-01-01 00:00:00.000+0).
     */
    public static long getEpochTimeNano(ByteBuffer buffer) {
        long secondsInNano = getTimeSec(buffer).getU() * 1_000_000_000;
        long nanoPart = getTimeNano(buffer).getU();
        return secondsInNano + nanoPart;
    }

    /**
     * Get Epoch compatible time (milli secs from 1970-01-01 00:00:00.000+0)
     *
     * @param buffer
     * @return
     */
    public static long getEpochTimeMilli(ByteBuffer buffer) {
        long ret = getTimeSec(buffer).getU() * 1000;
        long milli_from_nano = (long) (getTimeNano(buffer).getU() / 1e6);
        return ret + milli_from_nano;
    }

    /**
     * Get remainder of nano second (values to add to millisecond epoch date to get nano epoch date)
     *
     * @param buffer
     * @return
     */
    public static long getRemainderTimeNano(ByteBuffer buffer) {
        return (long) (getTimeNano(buffer).getU() % 1e6);
    }

    /**
     * get timestamp (sec since 1970-01-01 00:00:00.000+0)
     *
     * @param buffer
     * @return
     */
    public static UInt getTimeSec(ByteBuffer buffer) {
        return TypeDecoder.read4U(buffer, 8);
    }

    /**
     * return nano seconds deviation from the timestamp
     *
     * @param buffer
     * @return
     */
    public static UInt getTimeNano(ByteBuffer buffer) {
        return TypeDecoder.read4U(buffer, 12);
    }
}
