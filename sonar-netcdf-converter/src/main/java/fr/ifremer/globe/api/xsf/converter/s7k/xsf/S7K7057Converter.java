package fr.ifremer.globe.api.xsf.converter.s7k.xsf;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.function.Function;

import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1F4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1F8;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U8;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.BeamGroup1Grp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.BeamGroup1VendorSpecificGrp;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K7057;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K7057.DoubleSamples;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K7057.FloatSamples;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K7057.Samples;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KFile;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KMultipingMetadata;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KMultipingSequenceMetadata;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KSwathMetadata;
import fr.ifremer.globe.api.xsf.util.DefaultLogger;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.types.DDouble;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.api.xsf.util.types.UByte;
import fr.ifremer.globe.api.xsf.util.types.UInt;
import fr.ifremer.globe.api.xsf.util.types.ULong;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCVariable;

public class S7K7057Converter implements IDatagramConverter<S7KFile, XsfFromS7k> {

	private List<ValueD1> valuesD1;
	private List<ValueD1> optionalD1;
	// private ValueD2 freqV;
	NCVariable samplesPort;
	NCVariable samplesStarport;
	NCVariable samplesPortNumbers;
	NCVariable samplesStarportNumbers;

	@Override
	public void declare(S7KFile s7kFile, XsfFromS7k xsf) throws NCException {
		valuesD1 = new LinkedList<>();
		optionalD1 = new LinkedList<>();
		BeamGroup1Grp beamGrp = xsf.getBeamGroup();
		BeamGroup1VendorSpecificGrp beamVendor = xsf.getBeamVendor();

		valuesD1.add(new ValueD1U8(beamGrp.getPing_time(), (buffer) -> new ULong(S7K7057.getTimestampNano(buffer))));
		valuesD1.add(new ValueD1U4(beamVendor.getPing_raw_count(),
				(buffer) -> S7K7057.getPingNumber(buffer)));
		valuesD1.add(new ValueD1U1(xsf.getBathyGrp().getMultiping_sequence(),
				(buffer) -> new UByte((byte) S7K7057.getMultiPingSequence(buffer).getU())));
		valuesD1.add(new ValueD1F4(beamVendor.getBeam_position(), (buffer) -> S7K7057.getBeamPosition(buffer)));
		valuesD1.add(new ValueD1U1(beamVendor.getSidescan_error_flag(), (buffer) -> S7K7057.getErrorFlags(buffer)));

		samplesPort = beamVendor.getSidescan_port_beams();
		samplesStarport = beamVendor.getSidescan_starboard_beams();
		samplesPortNumbers = beamVendor.getSidescan_port_beams_numbers();
		samplesStarportNumbers = beamVendor.getSidescan_starboard_beams_number();

		Function<BaseDatagramBuffer, Integer> optOff = (buffer) -> Math
				.toIntExact(S7K7057.getOptionalDataOffset(buffer).getU());
		// done through datagram 7000

		// freqV = new ValueD2F4(beamVendor.getTx_center_frequency(), (buffer, i) -> S7K7057.getFrequency(buffer,
		// optOff.apply(buffer)));

		optionalD1.add(new ValueD1F8(beamGrp.getPlatform_latitude(),
				(buffer) -> new DDouble(Math.toDegrees(S7K7057.getLatitude(buffer, optOff.apply(buffer)).get()))));
		optionalD1.add(new ValueD1F8(beamGrp.getPlatform_longitude(),
				(buffer) -> new DDouble(Math.toDegrees(S7K7057.getLongitude(buffer, optOff.apply(buffer)).get()))));
		optionalD1.add(new ValueD1F4(beamGrp.getPlatform_heading(), (buffer) -> new FFloat(
				(float) Math.toDegrees(S7K7057.getHeading(buffer, optOff.apply(buffer)).get()))));
		optionalD1.add(new ValueD1F4(beamVendor.getDepth_side_scan(),
				(buffer) -> S7K7057.getDepth(buffer, optOff.apply(buffer))));
	}

	@Override
	public void fillData(S7KFile s7kFile) throws IOException, NCException {
		DatagramBuffer buffer = new DatagramBuffer();

		if (s7kFile.get7057Metadata().getEntries().size() != 1) {
			throw new RuntimeException("This converter only supports one sonar");
		}
		S7KMultipingMetadata mpmd = s7kFile.get7057Metadata().getEntries().iterator().next().getValue();

		for (Entry<Long, S7KMultipingSequenceMetadata> posentry : mpmd.getEntries()) {
			for (Entry<Integer, S7KSwathMetadata> e : posentry.getValue().getEntries()) {
				DatagramReader.readDatagram(e.getValue().getPosition().getFile(), buffer,
						e.getValue().getPosition().getSeek());
				SwathIdentifier swathId = new SwathIdentifier(S7K7057.getPingNumber(buffer).getU(),
						S7K7057.getMultiPingSequence(buffer).getU());

				// get origin from swath identifier (ignore if matching index not found)
				var optIndex = s7kFile.getSwathIndexer().getIndex(swathId);
				if (optIndex.isEmpty()) {
					DefaultLogger.get().warn(
							"Datagram S7K7057 associated with a missing ping is ignored (number = {}, sequence = {}).",
							swathId.getRawPingNumber(), swathId.getSwathPosition());
					continue;
				}
				int origin = optIndex.get();

				for (ValueD1 v : this.valuesD1) {
					v.fill(buffer);
					v.write(new long[] { origin });
				}

				Samples samples;
				switch (S7K7057.getNumberOfBytesPerSample(buffer).getU()) {
				case 4:
					FloatSamples samples4 = S7K7057.getSamples4(buffer);
					// upcast values to double;
					double[] portBeams = new double[samples4.portBeams.length];
					double[] starboardBeams = new double[samples4.starboardBeams.length];
					for (int i = 0; i < samples4.portBeams.length; i++) {
						// samples4.portBeams.length == samples4.starboardBeams.length
						portBeams[i] = samples4.portBeams[i];
						starboardBeams[i] = samples4.starboardBeams[i];
					}
					samplesPort.put(new long[] { origin, 0 }, new long[] { 1, portBeams.length }, portBeams);
					samplesStarport.put(new long[] { origin, 0 }, new long[] { 1, starboardBeams.length },
							starboardBeams);

					samples = samples4;
					break;

				case 8:
					DoubleSamples samples8 = S7K7057.getSamples8(buffer);
					samples = samples8;
					samplesPort.put(new long[] { origin, 0 }, new long[] { 1, samples8.portBeamNumbers.length },
							samples8.portBeams);
					samplesStarport.put(new long[] { origin, 0 }, new long[] { 1, samples8.starboardBeams.length },
							samples8.starboardBeams);
					break;
				default:
					throw new RuntimeException("Unsupported bytes per sample size");
				}

				short[] portBeamNumbers = new short[samples.portBeamNumbers.length];
				short[] starbordBeamNumbers = new short[samples.starboardsBeamNumbers.length];
				for (int i = 0; i < portBeamNumbers.length; i++) {
					portBeamNumbers[i] = samples.portBeamNumbers[i].getShort();
					starbordBeamNumbers[i] = samples.starboardsBeamNumbers[i].getShort();
				}
				samplesPortNumbers.putu(new long[] { origin, 0 }, new long[] { 1, portBeamNumbers.length },
						portBeamNumbers);
				samplesStarportNumbers.putu(new long[] { origin, 0 }, new long[] { 1, starbordBeamNumbers.length },
						starbordBeamNumbers);

				// optional data
				if (S7K7057.getOptionalDataOffset(buffer).getU() != 0) {
					for (ValueD1 v : optionalD1) {
						v.fill(buffer);
						v.write(new long[] { origin });
					}
					// freqV.fill(buffer, 0, 0);
					// freqV.write(new long[] { origin, 0 }, new long[] { 1, 1 });
				}
			}

		}

	}

}
