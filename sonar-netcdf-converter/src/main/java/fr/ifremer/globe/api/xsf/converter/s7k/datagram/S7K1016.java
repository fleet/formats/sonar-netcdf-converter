package fr.ifremer.globe.api.xsf.converter.s7k.datagram;

import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7K1016Metadata;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KFile;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KIndividualSensorMetadata;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.api.xsf.util.types.UByte;
import fr.ifremer.globe.api.xsf.util.types.UShort;
import fr.ifremer.globe.utils.date.DateUtils;

public class S7K1016 extends S7KBaseDatagram {
	
	public static class AttitudeData {
		public long time;
		public UShort timeDifferenceFromRecord;
		public float roll;
		public float pitch;
		public float heave;
		public float heading;
	}

	@Override
	public void computeSpecificMetadata(S7KFile metadata, DatagramBuffer buffer, long type,
			DatagramPosition position, long time) {
		S7K1016Metadata md = getSpecificMetadata(metadata);
		S7KIndividualSensorMetadata sensorMd = md.getEntry(getDeviceIdentifier(buffer).getU());
		if (sensorMd == null) {
			sensorMd = new S7KIndividualSensorMetadata();
			md.addSensor(getDeviceIdentifier(buffer).getU(), sensorMd);
		}
		sensorMd.addDatagram(position, time, getNumberOfAttitudeData(buffer).getU());
		
		for (AttitudeData d : getAttitudeDataRecords(buffer)) {
			metadata.getStats().getRoll().register((float) Math.toDegrees(d.roll));
			metadata.getStats().getPitch().register((float) Math.toDegrees(d.pitch));
			metadata.getStats().getHeave().register((float) Math.toDegrees(d.heave));
		}
	}

	@Override
	public S7K1016Metadata getSpecificMetadata(S7KFile metadata) {
		return metadata.get1016Metadata();
	}

	public static UByte getNumberOfAttitudeData(BaseDatagramBuffer buffer) {
		return TypeDecoder.read1U(buffer.byteBufferData, 0);
	}
	
	public static long getRecordTimestamp(BaseDatagramBuffer buffer, int i) {
		final long nanosecondsTime= getTimestampNano(buffer);
		return nanosecondsTime+ DateUtils.milliSecondToNano((TypeDecoder.read2U(buffer.byteBufferData, 1 + i * 18).getU()));
	}
	
	public static FFloat getRoll(BaseDatagramBuffer buffer, int i) {
		return TypeDecoder.read4F(buffer.byteBufferData, 3 + i * 18);
	}
	
	public static FFloat getPitch(BaseDatagramBuffer buffer, int i) {
		return TypeDecoder.read4F(buffer.byteBufferData, 7 + i * 18);
	}
	
	public static FFloat getHeave(BaseDatagramBuffer buffer, int i) {
		return TypeDecoder.read4F(buffer.byteBufferData, 11 + i * 18);
	}
	
	public static FFloat getHeading(BaseDatagramBuffer buffer, int i) {
		return  TypeDecoder.read4F(buffer.byteBufferData, 15 + i * 18);
	}
	
	public static AttitudeData[] getAttitudeDataRecords(DatagramBuffer buffer) {
		int numberOfRecords = getNumberOfAttitudeData(buffer).getU();
		AttitudeData[] ret = new AttitudeData[numberOfRecords];
		
		for (int i = 0; i < numberOfRecords; i++) {
			AttitudeData rec = new AttitudeData();
			rec.timeDifferenceFromRecord = TypeDecoder.read2U(buffer.byteBufferData, 1 + i * 18);
			rec.time = getRecordTimestamp(buffer, i);
			rec.roll = getRoll(buffer, i).get();
			rec.pitch = getPitch(buffer, i).get();
			rec.heave = getHeave(buffer, i).get();
			rec.heading = getHeading(buffer, i).get();
			ret[i] = rec;
		}
		
		return ret;
	}
}
