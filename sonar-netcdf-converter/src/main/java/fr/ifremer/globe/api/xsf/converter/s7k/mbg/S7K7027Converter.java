package fr.ifremer.globe.api.xsf.converter.s7k.mbg;

import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgBase;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgConstants;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgConstants.SoundingValidity;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2S2;
import fr.ifremer.globe.api.xsf.converter.common.xsf.types.DetectionTypeHelper;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K7027;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KFile;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KMultipingMetadata;
import fr.ifremer.globe.api.xsf.util.types.SShort;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCVariable;

public class S7K7027Converter extends AbstractDepthDatagramConverter {

	@Override
	protected S7KMultipingMetadata getMultipingMetada(S7KFile s7kFile) {
		return s7kFile.get7027Metadata().getEntries().iterator().next().getValue();
	}

	@Override
	public void declare(S7KFile s7kFile, MbgBase mbg) throws NCException {
		super.declare(s7kFile, mbg);

		// declare specific variables of S7K7027
		/** Sampling rate **/
		NCVariable samplingRateVariable = mbg.getVariable(MbgConstants.SamplingRate);
		swathAntennaValues.add(new ValueD2S2(samplingRateVariable, (buffer, antennaIndex) -> {
			return new SShort((short) (S7K7027.getSamplingRate(buffer).get()));
		}));
	}

	@Override
	protected int getOptionalDataOffset(BaseDatagramBuffer buffer) {
		return (int) S7K7027.getOptionalDataOffset(buffer).getU();
	}

	@Override
	protected int getDeviceIdentifier(BaseDatagramBuffer buffer) {
		return (int) S7K7027.getDeviceIdentifier(buffer).getU();
	}

	@Override
	protected int getMultiPingSequence(BaseDatagramBuffer buffer) {
		return S7K7027.getMultiPingSequence(buffer).getU();
	}

	@Override
	protected long getPingNumber(BaseDatagramBuffer buffer) {
		return S7K7027.getPingNumber(buffer).getU();
	}

	@Override
	protected long getTimestamp(BaseDatagramBuffer buffer) {
		return S7K7027.getTimestampNano(buffer);
	}

	@Override
	protected long getTime(BaseDatagramBuffer buffer) {
		return S7K7027.getTimeMilli(buffer);
	}

	@Override
	protected double getLatitude(BaseDatagramBuffer buffer) {
		return S7K7027.getLatitude(buffer).get();
	}

	@Override
	protected double getLongitude(BaseDatagramBuffer buffer) {
		return S7K7027.getLongitude(buffer).get();
	}

	@Override
	protected float getHeading(BaseDatagramBuffer buffer) {
		return S7K7027.getHeading(buffer).get();
	}

	@Override
	protected float getRoll(BaseDatagramBuffer buffer) {
		return S7K7027.getRoll(buffer).get();
	}

	@Override
	protected float getPitch(BaseDatagramBuffer buffer) {
		return S7K7027.getPitch(buffer).get();
	}

	@Override
	protected float getHeave(BaseDatagramBuffer buffer) {
		return S7K7027.getHeave(buffer).get();
	}

	@Override
	protected float getVehiculeDepth(BaseDatagramBuffer buffer) {
		return S7K7027.getVehiculeDepth(buffer).get();
	}

	@Override
	protected int getNumberOfDetectionPoints(BaseDatagramBuffer buffer) {
		return S7K7027.getNumberOfDetectionPoints(buffer).getInt();
	}

	@Override
	protected DetectionTypeHelper getDetectionType(BaseDatagramBuffer buffer, int iBeam) {
		int detectionFlags = getDetectionFlags(buffer, iBeam);
		if ((detectionFlags & 0x01) > 0)
			return DetectionTypeHelper.AMPLITUDE;
		if ((detectionFlags & 0x02) > 0)
			return DetectionTypeHelper.PHASE;
		return DetectionTypeHelper.INVALID;
	}

	@Override
	protected SoundingValidity getDetectionValidity(BaseDatagramBuffer buffer, int iBeam) {
		if (getDetectionType(buffer, iBeam) == DetectionTypeHelper.INVALID)
			return SoundingValidity.MISSING;
		if ((getQuality(buffer, iBeam) & 0x1) == 0) // no quality or Brightness = 0
			return SoundingValidity.UNVALID_ACQUISITION;
		return SoundingValidity.VALID;
	}

	protected float getDetectionPoint(BaseDatagramBuffer buffer, int iBeam) {
		return S7K7027.getDetectionPoint(buffer, iBeam).get();
	}

	@Override
	protected float getDetectionDepth(BaseDatagramBuffer buffer, int iBeam) {
		return S7K7027.getDetectionDepth(buffer, iBeam).get();
	}

	@Override
	protected float getBeamAzimuthAngle(BaseDatagramBuffer buffer, int iBeam) {
		return S7K7027.getBeamAzimuthAngle(buffer, iBeam).get();
	}

	@Override
	protected float getBeamPointingAngle(BaseDatagramBuffer buffer, int iBeam) {
		return S7K7027.getBeamPointingAngle(buffer, iBeam).get();
	}

	@Override
	protected float getDetectionAlongTrackDistance(BaseDatagramBuffer buffer, int iBeam) {
		return S7K7027.getDetectionAlongTrackDistance(buffer, iBeam).get();
	}

	@Override
	protected float getDetectionAcrossTrackDistance(BaseDatagramBuffer buffer, int iBeam) {
		return S7K7027.getDetectionAcrossTrackDistance(buffer, iBeam).get();
	}

	protected int getDetectionFlags(BaseDatagramBuffer buffer, int iBeam) {
		return S7K7027.getDetectionFlags(buffer, iBeam).getInt();
	}

	@Override
	protected int getQuality(BaseDatagramBuffer buffer, int iBeam) {
		return S7K7027.getQuality(buffer, iBeam).getInt();
	}

	protected float getSamplingRate(BaseDatagramBuffer buffer) {
		return S7K7027.getSamplingRate(buffer).get();
	}

	@Override
	protected float getFrequency(BaseDatagramBuffer buffer) {
		return S7K7027.getFrequency(buffer).get() / 1000; // frequency in kHz;
	}

	@Override
	protected float getRange(BaseDatagramBuffer buffer, int iBeam) {
		return S7K7027.getDetectionPoint(buffer, iBeam).get() / S7K7027.getSamplingRate(buffer).get();
	}
}
