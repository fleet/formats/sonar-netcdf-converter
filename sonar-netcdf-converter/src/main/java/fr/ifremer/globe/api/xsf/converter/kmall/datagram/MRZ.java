package fr.ifremer.globe.api.xsf.converter.kmall.datagram;

import java.nio.ByteBuffer;

import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.KmallFile;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.MRZCompletePingAntennaMetadata;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.MRZCompletePingMetadata;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.MRZMetadata;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.DDouble;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.api.xsf.util.types.SShort;
import fr.ifremer.globe.api.xsf.util.types.UByte;
import fr.ifremer.globe.api.xsf.util.types.UInt;
import fr.ifremer.globe.api.xsf.util.types.UShort;

public class MRZ extends KmAllMDatagram {
	
    @Override
    public MRZMetadata getSpecificMetadata(KmallFile metadata) {
        return metadata.getMRZMetadata();
    }
    
    public static void dump4debug(ByteBuffer datagram)
    {

    	KmAllMDatagram.dump4debug(datagram);
        
        System.err.println("NumBytesInfoData:"+getNumBytesInfoData(datagram));
        System.err.println("NumTxSectors:"+getNumTxSectors(datagram).getU());
        System.err.println("NumBytesPerTxSector:"+getNumBytesPerTxSector(datagram).getU());
        System.err.println("Latitude_deg:"+getLatitude_deg(datagram).get());
        System.err.println("Longitude_deg:"+getLongitude_deg(datagram).get());
        System.err.println("EllipsoidHeightReRefPoint_m:"+getEllipsoidHeightReRefPoint_m(datagram).get());

        
        // 
        for(int tx =0;tx<getNumTxSectors(datagram).getU();tx++)
        {
        	System.err.println("TxSectorNumb ["+tx+"]:"+getTxSectorNumb(datagram,tx).getU());
        	System.err.println("TxArrNumber ["+tx+"]:"+getTxArrNumber(datagram,tx).getU());
        	System.err.println("TxSubArray ["+tx+"]:"+getTxSubArray(datagram,tx).getU());
        	System.err.println("SectorTransmitDelay_sec ["+tx+"]:"+getSectorTransmitDelay_sec(datagram,tx).get());
        	System.err.println("TiltAngleReTx_deg ["+tx+"]:"+getTiltAngleReTx_deg(datagram,tx).get());
        }
        
    }
    
    @Override
    public void computeSpecificMMetadata(KmallFile metadata, ByteBuffer datagram, String datagramType,
    		SwathIdentifier swathId, DatagramPosition position) {
        MRZMetadata md = getSpecificMetadata(metadata);
        md.setVersion(getDgmVersion(datagram).getU());
      
        final int rxOffset = getRxInfoOffset(datagram);
        
        MRZCompletePingMetadata completing = md.getPing(swathId);
        if (completing == null) {
            completing = new MRZCompletePingMetadata(
            		getNumTxSectors(datagram).getU(),
            		getNumExtraDetectionClasses(datagram, rxOffset).getU(),
            		new int[] {}
            );
            md.addPing(swathId, completing);
        }
        int detectionCount = getNumSoundingsMaxMain(datagram, rxOffset).getU();
        int extraDetectCount = getNumExtraDetections(datagram, rxOffset).getU();
        
        int maxExtraDetectionSeabedSampleCount = 0;
        int maxSeabedSampleCount = 0;
        
        int offset = getSoundingOffset(datagram, rxOffset);
        double lat = MRZ.getLatitude_deg(datagram).get();
        double lon = MRZ.getLongitude_deg(datagram).get();
        
        for (int i = 0; i < detectionCount + extraDetectCount; i++) {
            if (getDetectionType(datagram, offset).getU() == 1) {
            	maxExtraDetectionSeabedSampleCount = Math.max(maxExtraDetectionSeabedSampleCount, getSInumSamples(datagram, offset).getU());
            } else {
                maxSeabedSampleCount = Math.max(maxSeabedSampleCount, getSInumSamples(datagram, offset).getU());
            }
    		// compute statistics
            md.addDepth(getTwoWayTravelTime_sec(datagram, offset).get()*getSoundSpeedAtTxDepth_mPerSec(datagram).get());
            md.addReflectivity(getReflectivity1_dB(datagram, offset).get());
        	md.computeBoundingBoxLat(lat + MRZ.getDeltaLatitude_deg(datagram, offset).get());
			md.computeBoundingBoxLon(lon + MRZ.getDeltaLongitude_deg(datagram, offset).get());

            offset = getNextSoundingOffset(datagram, offset);
        }
        
        completing.addAntenna(getRxTransducerInd(datagram).getU(),
                new MRZCompletePingAntennaMetadata(position, detectionCount, extraDetectCount, maxSeabedSampleCount, maxExtraDetectionSeabedSampleCount));
        
    }

    /**
     * return the offset of ping info part in byte buffer
     * */
    private static int getPingInfoDatagramOffset(ByteBuffer buffer)
    {
    	return HEADER_SIZE-DATAGRAMM_OFFSET+PARTITION_SIZE+getNumByteCmnPart(buffer);
    }
    
    // pingInfo - offset 30
    public static int getNumBytesInfoData(ByteBuffer buffer) {
        return TypeDecoder.read2U(buffer, getPingInfoDatagramOffset(buffer)+0).getU();   
    }
    public static FFloat getPingRate_Hz(ByteBuffer buffer) {
        return TypeDecoder.read4F(buffer, getPingInfoDatagramOffset(buffer)+4);  
    }
    public static UByte getBeamSpacing(ByteBuffer buffer) {
        return TypeDecoder.read1U(buffer, getPingInfoDatagramOffset(buffer)+8);  
    }
    public static UByte getDepthMode(ByteBuffer buffer) {
        return TypeDecoder.read1U(buffer, getPingInfoDatagramOffset(buffer)+9);   
    }
    public static UByte getSubDepthMode(ByteBuffer buffer) {
        return TypeDecoder.read1U(buffer, getPingInfoDatagramOffset(buffer)+10);  
    }
    public static UByte getDistanceBtwSwath(ByteBuffer buffer) {
        return TypeDecoder.read1U(buffer, getPingInfoDatagramOffset(buffer)+11); 
    }
    public static UByte getDetectionMode(ByteBuffer buffer) {
        return TypeDecoder.read1U(buffer, getPingInfoDatagramOffset(buffer)+12);   
    }
    public static UByte getPulseForm(ByteBuffer buffer) {
        return TypeDecoder.read1U(buffer, getPingInfoDatagramOffset(buffer)+13); 
    }
    public static FFloat getFrequencyMode_Hz(ByteBuffer buffer) {
        return TypeDecoder.read4F(buffer, getPingInfoDatagramOffset(buffer)+16); 
    }
    public static FFloat getFreqRangeLowLim_Hz(ByteBuffer buffer) {
        return TypeDecoder.read4F(buffer, getPingInfoDatagramOffset(buffer)+20);   
    }
    public static FFloat getFreqRangeHighLim_Hz(ByteBuffer buffer) {
        return TypeDecoder.read4F(buffer, getPingInfoDatagramOffset(buffer)+24); 
    }
    public static FFloat getMaxTotalTxPulseLength_sec(ByteBuffer buffer) {
        return TypeDecoder.read4F(buffer, getPingInfoDatagramOffset(buffer)+28); 
    }
    public static FFloat getMaxEffTxPulseLength_sec(ByteBuffer buffer) {
        return TypeDecoder.read4F(buffer, getPingInfoDatagramOffset(buffer)+32); 
    }
    public static FFloat getMaxEffTxBandWidth_Hz(ByteBuffer buffer) {
        return TypeDecoder.read4F(buffer, getPingInfoDatagramOffset(buffer)+36); 
    }
    public static FFloat getAbsCoeff_dBPerkm(ByteBuffer buffer) {
        return TypeDecoder.read4F(buffer, getPingInfoDatagramOffset(buffer)+40);  
    }
    public static FFloat getPortSectorEdge_deg(ByteBuffer buffer) {
        return TypeDecoder.read4F(buffer, getPingInfoDatagramOffset(buffer)+44);  
    }
    public static FFloat getStarbSectorEdge_deg(ByteBuffer buffer) {
        return TypeDecoder.read4F(buffer, getPingInfoDatagramOffset(buffer)+48); 
    }
    public static FFloat getPortMeanCov_deg(ByteBuffer buffer) {
        return TypeDecoder.read4F(buffer, getPingInfoDatagramOffset(buffer)+52);  
    }
    public static FFloat getStarbMeanCov_deg(ByteBuffer buffer) {
        return TypeDecoder.read4F(buffer, getPingInfoDatagramOffset(buffer)+56); 
    }
    public static SShort getPortMeanCov_m(ByteBuffer buffer) {
        return TypeDecoder.read2S(buffer, getPingInfoDatagramOffset(buffer)+60);  
    }
    public static SShort getStarbMeanCov_m(ByteBuffer buffer) {
        return TypeDecoder.read2S(buffer, getPingInfoDatagramOffset(buffer)+62); 
    }
    public static UByte getModeAndStabilisation(ByteBuffer buffer) {
        return TypeDecoder.read1U(buffer, getPingInfoDatagramOffset(buffer)+64); 
    }
    public static UByte getRuntimeFilter1(ByteBuffer buffer) {
        return TypeDecoder.read1U(buffer, getPingInfoDatagramOffset(buffer)+65);  
    }
    public static UShort getRuntimeFilter2(ByteBuffer buffer) {
        return TypeDecoder.read2U(buffer, getPingInfoDatagramOffset(buffer)+66); 
    }
    public static UInt getPipeTrackingStatus(ByteBuffer buffer) {
        return TypeDecoder.read4U(buffer, getPingInfoDatagramOffset(buffer)+68);  
    }
    public static FFloat getTransmitArraySizeUsed_deg(ByteBuffer buffer) {
        return TypeDecoder.read4F(buffer, getPingInfoDatagramOffset(buffer)+72); 
    }
    public static FFloat getReceiveArraySizeUsed_deg(ByteBuffer buffer) {
        return TypeDecoder.read4F(buffer, getPingInfoDatagramOffset(buffer)+76);  
    }
    public static FFloat getTransmitPower_dB(ByteBuffer buffer) {
        return TypeDecoder.read4F(buffer, getPingInfoDatagramOffset(buffer)+80); 
    }
    public static UShort getSLrampUpTimeRemaining(ByteBuffer buffer) {
        return TypeDecoder.read2U(buffer, getPingInfoDatagramOffset(buffer)+84); 
    }
    public static FFloat getYawAngle_deg(ByteBuffer buffer) {
        return TypeDecoder.read4F(buffer, getPingInfoDatagramOffset(buffer)+88); 
    }
    public static UShort getNumTxSectors(ByteBuffer buffer) {
        return TypeDecoder.read2U(buffer, getPingInfoDatagramOffset(buffer)+92);  
    }
    public static UShort getNumBytesPerTxSector(ByteBuffer buffer) {
        return TypeDecoder.read2U(buffer, getPingInfoDatagramOffset(buffer)+94);  
    }
    public static FFloat getHeadingVessel_deg(ByteBuffer buffer) {
        return TypeDecoder.read4F(buffer, getPingInfoDatagramOffset(buffer)+96);  
    }
    public static FFloat getSoundSpeedAtTxDepth_mPerSec(ByteBuffer buffer) {
        return TypeDecoder.read4F(buffer, getPingInfoDatagramOffset(buffer)+100); 
    }
    public static FFloat getTxTransducerDepth_m(ByteBuffer buffer) {
        return TypeDecoder.read4F(buffer, getPingInfoDatagramOffset(buffer)+104); 
    }
    public static FFloat getZ_waterLevelReRefPoint_m(ByteBuffer buffer) {
        return TypeDecoder.read4F(buffer, getPingInfoDatagramOffset(buffer)+108); 
    }
    public static FFloat getX_kmallToall_m(ByteBuffer buffer) {
        return TypeDecoder.read4F(buffer, getPingInfoDatagramOffset(buffer)+112);   
    }
    public static FFloat getY_kmallToall_m(ByteBuffer buffer) {
        return TypeDecoder.read4F(buffer, getPingInfoDatagramOffset(buffer)+116);   
    }
    public static UByte getLatLongInfo(ByteBuffer buffer) {
        return TypeDecoder.read1U(buffer, getPingInfoDatagramOffset(buffer)+120); 
    }
    public static UByte getPosSensorStatus(ByteBuffer buffer) {
        return TypeDecoder.read1U(buffer, getPingInfoDatagramOffset(buffer)+121); 
    }
    public static UByte getAttitudeSensorStatus(ByteBuffer buffer) {
        return TypeDecoder.read1U(buffer, getPingInfoDatagramOffset(buffer)+122); 
    }
    public static DDouble getLatitude_deg(ByteBuffer buffer) {
        return TypeDecoder.read8F(buffer, getPingInfoDatagramOffset(buffer)+124); 
    }
    public static DDouble getLongitude_deg(ByteBuffer buffer) {
        return TypeDecoder.read8F(buffer, getPingInfoDatagramOffset(buffer)+132); 
    }
    public static FFloat getEllipsoidHeightReRefPoint_m(ByteBuffer buffer) {
        return TypeDecoder.read4F(buffer, getPingInfoDatagramOffset(buffer)+140); 
    }
    
    
    
    //VARIABLE ADDED FOR REV 1 of MRZ datagram 
    //float 	bsCorrectionOffset_dB
    public static FFloat getBsCorrectionOffset_dB(ByteBuffer buffer) {
    	checkMinimuVersion(1, buffer);
        return TypeDecoder.read4F(buffer, getPingInfoDatagramOffset(buffer)+144); 
    }
    //uint8_t 	lambertsLawApplied
    public static UByte getLambertsLawApplied(ByteBuffer buffer) {
    	checkMinimuVersion(1, buffer);
        return TypeDecoder.read1U(buffer, getPingInfoDatagramOffset(buffer)+148); 
    }
    //uint8_t 	iceWindow
    public static UByte getIceWindow(ByteBuffer buffer) {
    	checkMinimuVersion(1, buffer);
        return TypeDecoder.read1U(buffer, getPingInfoDatagramOffset(buffer)+149); 
    }  

    //VARIABLE ADDED FOR REV 2 of MRZ datagram
    //uint16_t 	activeModes
    public static UShort getActiveModes(ByteBuffer buffer) {
    	checkMinimuVersion(2, buffer);
        return TypeDecoder.read2U(buffer, getPingInfoDatagramOffset(buffer)+150);
    }
 
    // txSectorInfo
    public static int getTxSectorOffset(ByteBuffer buffer, int txSectorNumber) {
        return getPingInfoDatagramOffset(buffer) + getNumBytesInfoData(buffer) + txSectorNumber * getNumBytesPerTxSector(buffer).getU();
    }

    public static UByte getTxSectorNumb(ByteBuffer buffer, int txSectorNumber) {
        return TypeDecoder.read1U(buffer, getTxSectorOffset(buffer, txSectorNumber) + 0);
    }
    public static UByte getTxArrNumber(ByteBuffer buffer, int txSectorNumber) {
        return TypeDecoder.read1U(buffer, getTxSectorOffset(buffer, txSectorNumber) + 1);
    }
    public static UByte getTxSubArray(ByteBuffer buffer, int txSectorNumber) {
        return TypeDecoder.read1U(buffer, getTxSectorOffset(buffer, txSectorNumber) + 2);
    }
    public static FFloat getSectorTransmitDelay_sec(ByteBuffer buffer, int txSectorNumber) {
        return TypeDecoder.read4F(buffer, getTxSectorOffset(buffer, txSectorNumber) + 4);
    }
    public static FFloat getTiltAngleReTx_deg(ByteBuffer buffer, int txSectorNumber) {
        return TypeDecoder.read4F(buffer, getTxSectorOffset(buffer, txSectorNumber) + 8);
    }
    public static FFloat getTxNominalSourceLevel_dB(ByteBuffer buffer, int txSectorNumber) {
        return TypeDecoder.read4F(buffer, getTxSectorOffset(buffer, txSectorNumber) + 12);
    }
    public static FFloat getTxFocusRange_m(ByteBuffer buffer, int txSectorNumber) {
        return TypeDecoder.read4F(buffer, getTxSectorOffset(buffer, txSectorNumber) + 16);
    }
    public static FFloat getCentreFreq_Hz(ByteBuffer buffer, int txSectorNumber) {
        return TypeDecoder.read4F(buffer, getTxSectorOffset(buffer, txSectorNumber) + 20);
    }
    public static FFloat getSignalBandWidth_Hz(ByteBuffer buffer, int txSectorNumber) {
        return TypeDecoder.read4F(buffer, getTxSectorOffset(buffer, txSectorNumber) + 24);
    }
    public static FFloat getTotalSignalLength_sec(ByteBuffer buffer, int txSectorNumber) {
        return TypeDecoder.read4F(buffer, getTxSectorOffset(buffer, txSectorNumber) + 28);
    }
    public static UByte getPulseShading(ByteBuffer buffer, int txSectorNumber) {
        return TypeDecoder.read1U(buffer, getTxSectorOffset(buffer, txSectorNumber) + 32);
    }
    public static UByte getSignalWaveForm(ByteBuffer buffer, int txSectorNumber) {
        return TypeDecoder.read1U(buffer, getTxSectorOffset(buffer, txSectorNumber) + 33);
    }
    //padding short (34,35)
    
    //float highVoltageLevel_dB; 
    public static FFloat gethighVoltageLevel_dB(ByteBuffer buffer, int txSectorNumber) {
    	checkMinimuVersion(1, buffer);
    	return TypeDecoder.read4F(buffer, getTxSectorOffset(buffer, txSectorNumber) + 36);
    } 
    
    //float sectorTrackingCorr_dB; 
    public static FFloat getSectorTrackingCorr_dB(ByteBuffer buffer, int txSectorNumber) {
    	checkMinimuVersion(1, buffer);
    	return TypeDecoder.read4F(buffer, getTxSectorOffset(buffer, txSectorNumber) + 40);
    }
    //float effectiveSignalLength_sec; 
    public static FFloat getEffectiveSignalLength_sec(ByteBuffer buffer, int txSectorNumber) {
    	checkMinimuVersion(1, buffer);
    	return TypeDecoder.read4F(buffer, getTxSectorOffset(buffer, txSectorNumber) + 44);
    }
    
    
    // rxInfo
    public static int getRxInfoOffset(ByteBuffer buffer) {
        return getTxSectorOffset(buffer, getNumTxSectors(buffer).getU());
    }
    public static int getNumBytesRxInfo(ByteBuffer buffer, int rxInfoOffset) {
        return TypeDecoder.read2U(buffer, rxInfoOffset).getU();
    }
    public static UShort getNumSoundingsMaxMain(ByteBuffer buffer, int rxInfoOffset) {
        return TypeDecoder.read2U(buffer, rxInfoOffset + 2);     	
    }
    public static UShort getNumSoundingsValidMain(ByteBuffer buffer, int rxInfoOffset) {
        return TypeDecoder.read2U(buffer, rxInfoOffset + 4);
    }
    public static int getNumBytesPerSounding(ByteBuffer buffer, int rxInfoOffset) {
        return TypeDecoder.read2U(buffer, rxInfoOffset + 6).getU();
    }
    public static FFloat getWCSampleRate(ByteBuffer buffer, int rxInfoOffset) {
        return TypeDecoder.read4F(buffer, rxInfoOffset + 8);
    }
    public static FFloat getSeabedImageSampleRate(ByteBuffer buffer, int rxInfoOffset) {
        return TypeDecoder.read4F(buffer, rxInfoOffset + 12);
    }
    public static FFloat getBSnormal_dB(ByteBuffer buffer, int rxInfoOffset) {
        return TypeDecoder.read4F(buffer, rxInfoOffset + 16);
    }
    public static FFloat getBSoblique_dB(ByteBuffer buffer, int rxInfoOffset) {
        return TypeDecoder.read4F(buffer, rxInfoOffset + 20);
    }
    public static UShort getExtraDetectionAlarmFlag(ByteBuffer buffer, int rxInfoOffset) {
        return TypeDecoder.read2U(buffer, rxInfoOffset + 24);
    }
    public static UShort getNumExtraDetections(ByteBuffer buffer, int rxInfoOffset) {
        return TypeDecoder.read2U(buffer, rxInfoOffset + 26);
    }
    public static UShort getNumExtraDetectionClasses(ByteBuffer buffer, int rxInfoOffset) {
        return TypeDecoder.read2U(buffer, rxInfoOffset + 28);
    }
    public static UShort getNumBytesPerClass(ByteBuffer buffer, int rxInfoOffset) {
        return TypeDecoder.read2U(buffer, rxInfoOffset + 30);
    }

    // extraDetClassInfo
    public static int getExtraDetClassInfoOffset(ByteBuffer buffer, int classIdx) {
        final int rxOffset = getRxInfoOffset(buffer);
        return rxOffset + getNumBytesRxInfo(buffer, rxOffset) + classIdx * getNumBytesPerClass(buffer, rxOffset).getU();
    }
    public static UShort getNumExtraDetInClass(ByteBuffer buffer, int extraDetClassInfoOffset) {
        return TypeDecoder.read2U(buffer, extraDetClassInfoOffset);
    }
    public static UByte getAlarmFlag(ByteBuffer buffer, int extraDetClassInfoOffset) {
        return TypeDecoder.read1U(buffer, extraDetClassInfoOffset + 3);
    }

    // sounding
    public static int getSoundingOffset(ByteBuffer buffer, int getRxInfoOffset) {
        return getSoundingOffset(buffer, getRxInfoOffset, 0);
    }
    public static int getSoundingOffset(ByteBuffer buffer, int getRxInfoOffset, int soundingIdx) {
        return getExtraDetClassInfoOffset(buffer, 0) + getNumExtraDetectionClasses(buffer, getRxInfoOffset).getU() * getNumBytesPerClass(buffer, getRxInfoOffset).getU() + soundingIdx * getNumBytesPerSounding(buffer, getRxInfoOffset);
    }
    public static int getNextSoundingOffset(ByteBuffer buffer, int offset) {
        return offset + 120;
    }
    public static UShort getSoundingIndex(ByteBuffer buffer, int soundingOffset) {
        return TypeDecoder.read2U(buffer, soundingOffset);
    }
    public static UByte getCorrTxSectorNumb(ByteBuffer buffer, int soundingOffset) {
        return TypeDecoder.read1U(buffer, soundingOffset + 2);
    }
    public static UByte getDetectionType(ByteBuffer buffer, int soundingOffset) {
        return TypeDecoder.read1U(buffer, soundingOffset + 3);
    }
    public static UByte getDetectionMethod(ByteBuffer buffer, int soundingOffset) {
        return TypeDecoder.read1U(buffer, soundingOffset + 4);
    }
    public static UByte getRejectionInfo1(ByteBuffer buffer, int soundingOffset) {
        return TypeDecoder.read1U(buffer, soundingOffset + 5);
    }
    public static UByte getRejectionInfo2(ByteBuffer buffer, int soundingOffset) {
        return TypeDecoder.read1U(buffer, soundingOffset + 6);
    }
    public static UByte getPostProcessingInfo(ByteBuffer buffer, int soundingOffset) {
        return TypeDecoder.read1U(buffer, soundingOffset + 7);
    }
    public static UByte getDetectionClass(ByteBuffer buffer, int soundingOffset) {
        return TypeDecoder.read1U(buffer, soundingOffset + 8);
    }
    public static UByte getDetectionConfidenceLevel(ByteBuffer buffer, int soundingOffset) {
        return TypeDecoder.read1U(buffer, soundingOffset + 9);
    }
    public static FFloat getRangeFactor(ByteBuffer buffer, int soundingOffset) {
        return TypeDecoder.read4F(buffer, soundingOffset + 12);
    }
    public static FFloat getQualityFactor(ByteBuffer buffer, int soundingOffset) {
        return TypeDecoder.read4F(buffer, soundingOffset + 16);
    }
    public static FFloat getDetectionUncertaintyVer_m(ByteBuffer buffer, int soundingOffset) {
        return TypeDecoder.read4F(buffer, soundingOffset + 20);
    }
    public static FFloat getDetectionUncertaintyHor_m(ByteBuffer buffer, int soundingOffset) {
        return TypeDecoder.read4F(buffer, soundingOffset + 24);
    }
    public static FFloat getDetectionWindowLength_sec(ByteBuffer buffer, int soundingOffset) {
        return TypeDecoder.read4F(buffer, soundingOffset + 28);
    }
    public static FFloat getEchoLength_sec(ByteBuffer buffer, int soundingOffset) {
        return TypeDecoder.read4F(buffer, soundingOffset + 32);
    }
    public static UShort getWCBeamNumb(ByteBuffer buffer, int soundingOffset) {
        return TypeDecoder.read2U(buffer, soundingOffset + 36);
    }
    public static UShort getWCrange_samples(ByteBuffer buffer, int soundingOffset) {
        return TypeDecoder.read2U(buffer, soundingOffset + 38);
    }
    public static FFloat getWCNomBeamAngleAcross_deg(ByteBuffer buffer, int soundingOffset) {
        return TypeDecoder.read4F(buffer, soundingOffset + 40);
    }
    public static FFloat getMeanAbsCoeff_dBPerkm(ByteBuffer buffer, int soundingOffset) {
        return TypeDecoder.read4F(buffer, soundingOffset + 44);
    }
    public static FFloat getReflectivity1_dB(ByteBuffer buffer, int soundingOffset) {
        return TypeDecoder.read4F(buffer, soundingOffset + 48);
    }
    public static FFloat getReflectivity2_dB(ByteBuffer buffer, int soundingOffset) {
        return TypeDecoder.read4F(buffer, soundingOffset + 52);
    }
    public static FFloat getReceiverSensitivityApplied_dB(ByteBuffer buffer, int soundingOffset) {
        return TypeDecoder.read4F(buffer, soundingOffset + 56);
    }
    public static FFloat getSourceLevelApplied_dB(ByteBuffer buffer, int soundingOffset) {
        return TypeDecoder.read4F(buffer, soundingOffset + 60);
    }
    public static FFloat getBScalibration_dB(ByteBuffer buffer, int soundingOffset) {
        return TypeDecoder.read4F(buffer, soundingOffset + 64);
    }
    public static FFloat getTVG_dB(ByteBuffer buffer, int soundingOffset) {
        return TypeDecoder.read4F(buffer, soundingOffset + 68);
    }
    public static FFloat getBeamAngleReRx_deg(ByteBuffer buffer, int soundingOffset) {
        return TypeDecoder.read4F(buffer, soundingOffset + 72);
    }
    public static FFloat getBeamAngleCorrection_deg(ByteBuffer buffer, int soundingOffset) {
        return TypeDecoder.read4F(buffer, soundingOffset + 76);
    }
    public static FFloat getTwoWayTravelTime_sec(ByteBuffer buffer, int soundingOffset) {
        return TypeDecoder.read4F(buffer, soundingOffset + 80);
    }
    public static FFloat getTwoWayTravelTimeCorrection_sec(ByteBuffer buffer, int soundingOffset) {
        return TypeDecoder.read4F(buffer, soundingOffset + 84);
    }
    public static FFloat getDeltaLatitude_deg(ByteBuffer buffer, int soundingOffset) {
        return TypeDecoder.read4F(buffer, soundingOffset + 88);
    }
    public static FFloat getDeltaLongitude_deg(ByteBuffer buffer, int soundingOffset) {
        return TypeDecoder.read4F(buffer, soundingOffset + 92);
    }
    public static FFloat getZ_reRefPoint_m(ByteBuffer buffer, int soundingOffset) {
        return TypeDecoder.read4F(buffer, soundingOffset + 96);
    }
    public static FFloat getY_reRefPoint_m(ByteBuffer buffer, int soundingOffset) {
        return TypeDecoder.read4F(buffer, soundingOffset + 100);
    }
    public static FFloat getX_reRefPoint_m(ByteBuffer buffer, int soundingOffset) {
        return TypeDecoder.read4F(buffer, soundingOffset + 104);
    }
    public static FFloat getBeamIncAngleAdj_deg(ByteBuffer buffer, int soundingOffset) {
        return TypeDecoder.read4F(buffer, soundingOffset + 108);
    }
    public static UShort getRealTimeCleanInfo(ByteBuffer buffer, int soundingOffset) {
        return TypeDecoder.read2U(buffer, soundingOffset + 112);
    }
    public static UShort getSIstartRange_samples(ByteBuffer buffer, int soundingOffset) {
        return TypeDecoder.read2U(buffer, soundingOffset + 114);
    }
    public static UShort getSIcentreSample(ByteBuffer buffer, int soundingOffset) {
        return TypeDecoder.read2U(buffer, soundingOffset + 116);
    }
    public static UShort getSInumSamples(ByteBuffer buffer, int soundingOffset) {
        return TypeDecoder.read2U(buffer, soundingOffset + 118);
    }

    // SIsample
    public static short[] getSIsample_desidB(ByteBuffer buffer) {
        final int rxInfoOffset = getRxInfoOffset(buffer);
        int soundingOffset = getSoundingOffset(buffer, rxInfoOffset);
        int nSamples = 0;
        int nSoundins = getNumSoundingsMaxMain(buffer, rxInfoOffset).getU() + MRZ.getNumExtraDetections(buffer, rxInfoOffset).getU();
        for (int i = 0; i <  nSoundins; i++) {
            nSamples += MRZ.getSInumSamples(buffer, soundingOffset).getU();
            soundingOffset = MRZ.getNextSoundingOffset(buffer, soundingOffset);
        }
        
        short[] ret = new short[nSamples];
        int currPos = buffer.position();
        buffer.position(soundingOffset);
        for (int i = 0; i < nSamples; i++) {
            ret[i] = buffer.getShort();
        }
        buffer.position(currPos);
        return ret;
    }
}
