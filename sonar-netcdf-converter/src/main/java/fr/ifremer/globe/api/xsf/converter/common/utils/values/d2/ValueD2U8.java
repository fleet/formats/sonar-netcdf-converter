package fr.ifremer.globe.api.xsf.converter.common.utils.values.d2;

import java.util.Arrays;

import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.api.xsf.util.types.ULong;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCVariable;

/***
 * class allocating storage for 2D matrix, and giving a interface allowing to fill the data *
 */
public class ValueD2U8 extends ValueD2 {
	protected long[] dataOut;
	private ValueProvider valueProvider;
	private ULong fillValue;

	/***
	 * Constructor, allocate netcdf buffer for one variable and one ping
	 */
	public ValueD2U8(NCVariable variable, ValueProvider filler) {
		super(variable);
		// allocate storage
		this.dataOut = new long[dim];
		this.fillValue=new ULong(variable.getLongFillValue());
		this.valueProvider = filler;

	}

	public ValueD2U8(NCVariable variable, ValueProvider filler, ULong fillValue) {
		this(variable, filler);
		this.fillValue=fillValue;
		setDefault(fillValue);
	}

	public ValueD2U8(NCVariable variable, int dim, ValueProvider filler) {
		super(variable, dim);
		// allocate storage
		this.dataOut = new long[dim];
		this.fillValue=new ULong(variable.getLongFillValue());
		clear();
		this.valueProvider = filler;

	}
	
	@Override
	public void clear() {
		Arrays.fill(this.dataOut,fillValue.getLong());
	}

	public void setDefault(ULong dft) {
		for (int i = 0; i < dataOut.length; i++) {
			dataOut[i] = dft.getLong();
		}
	}

	public interface ValueProvider {
		public ULong get(BaseDatagramBuffer buffer, int beamIndexSource);
	}

	/**
	 * call the lambda to retrieve the value and set it in the allocated netcdf buffer dataOut
	 */
	@Override
	public void fill(BaseDatagramBuffer buffer, int beamIndexSource, int beamIndexDest) {
		dataOut[beamIndexDest] = valueProvider.get(buffer, beamIndexSource).getLong();
	}

	@Override
	public void write(long[] origin, long[] count) throws NCException {
		try {
			variable.putu(origin, count, dataOut);
		} catch (Exception e) {
			logger.error(String.format("Error while writing variable %s (%s)", variable.getName(), e.getMessage()));
			throw e;
		}
	}
}
