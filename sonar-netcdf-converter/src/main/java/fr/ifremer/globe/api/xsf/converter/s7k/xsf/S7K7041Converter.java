package fr.ifremer.globe.api.xsf.converter.s7k.xsf;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import com.sun.jna.Memory;

import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1F4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1S1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U2;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U8;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2F4;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.BeamGroup1Grp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.BeamGroup1VendorSpecificGrp;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K7004;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K7041;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K7041.WCData;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7K7041Metadata;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KFile;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KIndividualSensorMetadata;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KMultipingMetadata;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KMultipingSequenceMetadata;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KSwathMetadata;
import fr.ifremer.globe.api.xsf.util.DefaultLogger;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.api.xsf.util.types.SByte;
import fr.ifremer.globe.api.xsf.util.types.UByte;
import fr.ifremer.globe.api.xsf.util.types.UInt;
import fr.ifremer.globe.api.xsf.util.types.ULong;
import fr.ifremer.globe.netcdf.jna.Nc4prototypes.Vlen_t;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCVariable;

public class S7K7041Converter implements IDatagramConverter<S7KFile, XsfFromS7k> {

	private S7K7041Metadata md;
	private S7KMultipingMetadata mpmd;
	private S7KIndividualSensorMetadata md7004;

	private List<ValueD1> valuesD1;
	private List<ValueD2> valuesD2;
	private NCVariable wcBeamNumber;
	private NCVariable wcBeamAngle;

	// the beam realted values are provided by the 7004 beam configuration datagram
	private List<ValueD2> valuesD27004;

	private BeamGroup1Grp beamGrp;

	public S7K7041Converter(S7KFile stats) {
		this.md = stats.get7041Metadata();
		if (this.md.getEntries().size() != 1) {
			throw new RuntimeException("This converter only supports one sonar");
		}
		this.mpmd = this.md.getEntries().iterator().next().getValue();
		this.md7004 = stats.get7004Metadata().getEntry(this.md.getEntries().iterator().next().getKey());

		valuesD1 = new LinkedList<>();
		valuesD2 = new LinkedList<>();
		valuesD27004 = new LinkedList<>();
	}

	@Override
	public void declare(S7KFile s7kFile, XsfFromS7k xsf) throws NCException {
		beamGrp = xsf.getBeamGroup();
		BeamGroup1VendorSpecificGrp beamVendor = xsf.getBeamVendor();

		// main data from the 7041 datagram
		valuesD1.add(
				new ValueD1U8(beamGrp.getPing_time(), (buffer) -> new ULong(S7K7041.getTimestampNano(buffer)), true));
		valuesD1.add(new ValueD1U4(beamVendor.getPing_raw_count(),
				(buffer) -> S7K7041.getPingNumber(buffer), true));
		valuesD1.add(new ValueD1U1(xsf.getBathyGrp().getMultiping_sequence(),
				(buffer) -> new UByte((byte) S7K7041.getMultipingSequence(buffer).getU()), true));

		valuesD1.add(new ValueD1S1(beamVendor.getPing_validity(), (buffer) -> new SByte((byte) 0), true));

		valuesD1.add(new ValueD1F4(beamGrp.getSample_interval(),
				(buffer) -> new FFloat(1 / S7K7041.getSampleRate(buffer).get()), true));
		valuesD1.add(new ValueD1U2(beamVendor.getWatercolumn_flags(), (buffer) -> S7K7041.getFlags(buffer), true));

		wcBeamNumber = beamVendor.getWatercolumn_beamnumber();
		wcBeamAngle = beamVendor.getWatercolumn_beamangle();

		valuesD27004.add(new ValueD2F4(beamVendor.getBeam_horizontal_direction_angle(), (buffer,
				i) -> new FFloat((float) Math.toDegrees(S7K7004.getBeamHorizontalDirection(buffer, i).get()))));
		valuesD27004.add(new ValueD2F4(beamVendor.getBeam_vertical_direction_angle(),
				(buffer, i) -> new FFloat((float) Math.toDegrees(S7K7004.getBeamVerticalDirection(buffer, i).get()))));

		valuesD27004.add(new ValueD2F4(beamGrp.getBeamwidth_receive_minor(), (buffer, i) -> {
			return new FFloat((float) Math.toDegrees(S7K7004.getMinus3dBBeamWidthY(buffer, i).get()));
		}));

		valuesD27004.add(new ValueD2F4(beamGrp.getBeamwidth_receive_major(), (buffer, i) -> {
			return new FFloat((float) Math.toDegrees(S7K7004.getMinus3dBBeamWidthX(buffer, i).get()));
		}));

		// declare scale factor, add offset and comment for backscatter variable
		beamGrp.getBackscatter_r().addAttribute("scale_factor", 1f); // scale factor of 1 allowing to convert raw
																		// amplitude to float values
		beamGrp.getBackscatter_r().addAttribute("comment",
				"Amplitude of backscatter measurements. Each element in the 3D matrix is a variable length vector (of type sample_t) that contains the samples for that beam, ping time");
		beamGrp.getBackscatter_r().addAttribute("units", "Unknown : Raw Amplitude");
		
		// declare sample_count variable
		beamGrp.getSample_count();

	}

	@Override
	public void fillData(S7KFile s7kFile) throws IOException, NCException {
		DatagramBuffer buffer = new DatagramBuffer();
		DatagramBuffer buffer7004 = new DatagramBuffer();
		
		for (var valueD1 : valuesD1)
			valueD1.clear();

		// The 7004 datagrams are indexed over time. Therefore, the following iterator will
		// yield the information in chronological order
		Iterator<Entry<Long, DatagramPosition>> it7004 = this.md7004.getEntries().iterator();
		Entry<Long, DatagramPosition> curr7004 = it7004.next();
		DatagramReader.readDatagram(curr7004.getValue().getFile(), buffer7004, curr7004.getValue().getSeek());
		Entry<Long, DatagramPosition> next7004 = curr7004;
		if (it7004.hasNext())
			next7004 = it7004.next();
		Vlen_t[] vlen_backscatter_amp = (Vlen_t[]) new Vlen_t().toArray(s7kFile.getWcBeamCount());

		int[] sampleCountVariable = new int[s7kFile.getWcBeamCount()]; // number of sample per beam

		boolean firstPing = true;

		// iterate over the pings for the 7041 datagram (one ping can contain multiple swaths).
		for (Entry<Long, S7KMultipingSequenceMetadata> posentry : this.mpmd.getEntries()) {

			// find the 7004 datagram associated with the current ping (the last one received before the current ping)
			while (next7004.getKey() < posentry.getKey() && it7004.hasNext()) {
				curr7004 = next7004;
				next7004 = it7004.next();
				DatagramReader.readDatagram(curr7004.getValue().getFile(), buffer7004, curr7004.getValue().getSeek());
			}

			// iterate over all the swaths of the current ping
			for (Entry<Integer, S7KSwathMetadata> e : posentry.getValue().getEntries()) {
				valuesD2.forEach(ValueD2::clear);
				valuesD27004.forEach(ValueD2::clear);

				Arrays.fill(sampleCountVariable, 0); // fill default values

				DatagramReader.readDatagram(e.getValue().getPosition().getFile(), buffer,
						e.getValue().getPosition().getSeek());
				SwathIdentifier swathId = new SwathIdentifier(S7K7041.getPingNumber(buffer).getU(),
						S7K7041.getMultipingSequence(buffer).getU());

				// get origin from swath identifier (ignore if matching index not found)
				var optIndex = s7kFile.getSwathIndexer().getIndex(swathId);
				if (optIndex.isEmpty()) {
					DefaultLogger.get().warn(
							"Datagram S7K7041 associated with a missing ping is ignored (number = {}, sequence = {}).",
							swathId.getRawPingNumber(), swathId.getSwathPosition());
					continue;
				}
				int origin = optIndex.get();

				for (ValueD1 v : this.valuesD1) {
					v.fill(buffer, origin);
				}
				for (ValueD2 v : this.valuesD2) {
					for (int i = 0; i < S7K7041.getNumberOfBeams(buffer).getU(); i++) {
						v.fill(buffer, i, i);
					}
					v.write(new long[] { origin, 0 }, new long[] { 1, S7K7041.getNumberOfBeams(buffer).getU() });
				}

				WCData wcd = S7K7041.getData(buffer);

				final short flag = S7K7041.getFlags(buffer).getShort();
				int[] wcBeamNumbers = (flag & 0x10) != 0x10 ? new int[wcd.beams.length] : null;

				final long[] cnt = new long[] { 1, 1 };
				for (int beam = 0; beam < wcd.beams.length; beam++) {
					final long[] or = new long[] { origin, beam };
					if ((flag & 0x10) == 0x10) {
						wcBeamAngle.put(or, cnt, new float[] { (float) Math.toDegrees(wcd.beams[beam].beamAngle) });

					} else {
						wcBeamNumbers[beam] = wcd.beams[beam].beamNumber;
					}

					int sampleCount = wcd.beams[beam].numberOfSample;

					int bufferlength = wcd.beams[beam].numberOfSample * Short.SIZE;

					// WE SAVE DATA AS SHORT AND AS AMPLITUDE FOR S7K

					Memory ampMem = new Memory(bufferlength);
					ByteBuffer localBuffer = ampMem.getByteBuffer(0, bufferlength);
					// copy the values
					for (int s = 0; s < sampleCount; s++) {

						// samples are stored as byte, and a scale factor of 0.5 should be applied when reading the
						// data.
						// byte v = (byte) (wcd.beams[beam].samples[s]*0.5
						// * 1 /* when applying the 0.5 scale_factor, the reader will have the original value */);
						//
						short v = wcd.beams[beam].samples[s];
						localBuffer.putShort(v);

						// float v= (float) (20.0 * Math.log10(wcd.beams[beam].samples[s] +32+1)); //Fake to make
						// comparaison with SSc
						// localBuffer.putFloat(v);
					}
					vlen_backscatter_amp[beam].p = ampMem;
					vlen_backscatter_amp[beam].len = sampleCount;
					sampleCountVariable[beam] = sampleCount;

				}

				if ((flag & 0x10) != 0x10) {
					wcBeamNumber.put(new long[] { origin, 0l }, new long[] { 1l, wcBeamNumbers.length }, wcBeamNumbers);
				}

				beamGrp.getBackscatter_r().put(new long[] { origin, 0, 0 },
						new long[] { 1, s7kFile.getWcBeamCount(), 1 }, vlen_backscatter_amp);

				// write indicative variable beam sample count
				beamGrp.getSample_count().put(new long[] { origin, 0, 0 },
						new long[] { 1, s7kFile.getWcBeamCount(), 1 }, sampleCountVariable);

				if (firstPing) {
					firstPing = false;
					int[] receivedTransducerIndex = null;
					receivedTransducerIndex = new int[wcd.beams.length];
					Arrays.fill(receivedTransducerIndex, 1); // fill default values
					NCVariable variable = beamGrp.getReceive_transducer_index();
					try {
						variable.put(new long[] { 0 }, new long[] { receivedTransducerIndex.length },
								receivedTransducerIndex);
					} catch (Exception ex) {
						DefaultLogger.get().error(String.format("Error while writing variable %s (%s)",
								variable.getName(), ex.getMessage()));
						throw ex;
					}
					firstPing = false;
				}
				// handle the beam geometry related variables
				final int nBeams = Math.toIntExact(S7K7004.getNumberOfReceivedBeams(buffer7004).getU());
				for (ValueD2 v : this.valuesD27004) {
					for (int i = 0; i < nBeams; i++) {
						v.fill(buffer7004, i, i);
					}
					v.write(new long[] { origin, 0 }, new long[] { 1, nBeams });
				}
			}
		}

		for (ValueD1 v : valuesD1) {
			v.write(new long[] { 0l });
		}
	}
}
