package fr.ifremer.globe.api.xsf.converter.s7k.xsf;

import static java.lang.Math.toIntExact;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.function.Function;

import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1F4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1F8;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1S1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U8;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2F4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2F8;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2S1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2S2;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2U1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2U2;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.BathymetryGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.BeamGroup1Grp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.BathymetryVendorSpecificGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.BeamGroup1VendorSpecificGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.types.DetectionTypeHelper;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K7006;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7K7030InstallationParameters;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KFile;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KMultipingMetadata;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KMultipingSequenceMetadata;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KSwathMetadata;
import fr.ifremer.globe.api.xsf.util.DefaultLogger;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.types.DDouble;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.api.xsf.util.types.SByte;
import fr.ifremer.globe.api.xsf.util.types.SShort;
import fr.ifremer.globe.api.xsf.util.types.UByte;
import fr.ifremer.globe.api.xsf.util.types.UInt;
import fr.ifremer.globe.api.xsf.util.types.ULong;
import fr.ifremer.globe.api.xsf.util.types.UShort;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.maths.MatrixBuilder;
import fr.ifremer.globe.utils.maths.Ops;
import fr.ifremer.globe.utils.maths.RotationMatrix3x3D;
import fr.ifremer.globe.utils.maths.Vector3D;
import fr.ifremer.globe.utils.mbes.Ellipsoid;
import fr.ifremer.globe.utils.mbes.MbesUtils;

public class S7K7006Converter implements IDatagramConverter<S7KFile, XsfFromS7k> {

	protected final RotationMatrix3x3D attitudeMatrix = new RotationMatrix3x3D();
	protected final Vector3D txAntennaSCS = new Vector3D();

	private List<ValueD1> valuesD1;
	private List<ValueD1> optionalsD1;
	private List<ValueD2> valuesD2;
	private List<ValueD2> optionalsD2;

	@Override
	public void declare(S7KFile s7kFile, XsfFromS7k xsf) throws NCException {
		this.valuesD1 = new LinkedList<>();
		this.optionalsD1 = new LinkedList<>();
		this.valuesD2 = new LinkedList<>();
		this.optionalsD2 = new LinkedList<>();

		BeamGroup1Grp beamGrp = xsf.getBeamGroup();
		BathymetryGrp bathymetryGrp = xsf.getBathyGrp();
		BathymetryVendorSpecificGrp bathymetryVendorGrp = xsf.getBathyVendor();
		BeamGroup1VendorSpecificGrp beamVendorGrp = xsf.getBeamVendor();

		valuesD1.add(new ValueD1U8(beamGrp.getPing_time(), (buffer) -> new ULong(S7K7006.getTimestampNano(buffer))));
		valuesD1.add(new ValueD1U4(beamVendorGrp.getPing_raw_count(),
				(buffer) -> S7K7006.getPingNumber(buffer)));
		valuesD1.add(
				new ValueD1F4(beamGrp.getSound_speed_at_transducer(), (buffer) -> S7K7006.getSoundVelocity(buffer)));
		valuesD1.add(new ValueD1U1(bathymetryGrp.getMultiping_sequence(),
				(buffer) -> new UByte((byte) S7K7006.getMultiPingSequence(buffer).getU())));
		valuesD1.add(new ValueD1S1(beamVendorGrp.getLayer_compensation_flag(),
				(buffer) -> new SByte(S7K7006.getLayerCompensationFlag(buffer).getByte())));
		valuesD1.add(new ValueD1S1(beamVendorGrp.getSound_velocity_flag(),
				(buffer) -> new SByte(S7K7006.getSoundVelocityFlag(buffer).getByte())));

		Function<BaseDatagramBuffer, Integer> optOff = (buffer) -> toIntExact(
				S7K7006.getOptionalDataOffset(buffer).getU());

		optionalsD1.add(new ValueD1F4(bathymetryVendorGrp.getDetection_ping_frequency(),
				(buffer) -> S7K7006.getFrequency(buffer, optOff.apply(buffer))));

		optionalsD1.add(new ValueD1F8(beamGrp.getPlatform_latitude(),
				(buffer) -> new DDouble(Math.toDegrees(S7K7006.getLatitude(buffer, optOff.apply(buffer)).get()))));

		optionalsD1.add(new ValueD1F8(beamGrp.getPlatform_longitude(),
				(buffer) -> new DDouble(Math.toDegrees(S7K7006.getLongitude(buffer, optOff.apply(buffer)).get()))));

		optionalsD1.add(new ValueD1F4(beamGrp.getPlatform_heading(), (buffer) -> new FFloat(
				(float) Math.toDegrees(Math.toDegrees(S7K7006.getHeading(buffer, optOff.apply(buffer)).get())))));
		optionalsD1
				.add(new ValueD1F4(beamVendorGrp.getTide(), (buffer) -> S7K7006.getTide(buffer, optOff.apply(buffer))));

		optionalsD1.add(new ValueD1F4(beamGrp.getPlatform_pitch(),
				(buffer) -> new FFloat((float) Math.toDegrees(S7K7006.getPitch(buffer, optOff.apply(buffer)).get()))));

		optionalsD1.add(new ValueD1F4(beamGrp.getPlatform_roll(),
				(buffer) -> new FFloat((float) Math.toDegrees(S7K7006.getRoll(buffer, optOff.apply(buffer)).get()))));

		optionalsD1.add(new ValueD1F4(beamGrp.getPlatform_vertical_offset(),
				(buffer) -> new FFloat(-1 * S7K7006.getVehicleDepth(buffer, optOff.apply(buffer)).get())));

		optionalsD1.add(new ValueD1F4(beamVendorGrp.getSounder_heave(),
				(buffer) -> new FFloat(S7K7006.getHeave(buffer, optOff.apply(buffer)).get())));

		optionalsD1.add(new ValueD1F4(beamGrp.getTx_transducer_depth(), (buffer) -> {
			// 1) get antenna Z in Surface Coordinate System (ref point)
			final S7K7030InstallationParameters si = s7kFile.getInstallationParameters(s7kFile.getMaxDate());

			float pitch = (float) Math.toDegrees(S7K7006.getPitch(buffer, optOff.apply(buffer)).get());
			float roll = (float) Math.toDegrees(S7K7006.getRoll(buffer, optOff.apply(buffer)).get());

			MatrixBuilder.setDegRotationMatrix(0, pitch, roll, attitudeMatrix);

			Ops.mult(attitudeMatrix,
					new Vector3D(si.getTransmitArrayY(), si.getTransmitArrayX(), -si.getTransmitArrayZ()),
					txAntennaSCS);

			// 2) get reference_point_depth
			double vehicleDepth = S7K7006.getVehicleDepth(buffer, optOff.apply(buffer)).get();

			// transducer_depth = reference_point_depth + antenna_z_SCS
			return new FFloat((float) (vehicleDepth + txAntennaSCS.getZ()));

		}));

		// take into account in 7000 dg
		// freqV = new ValueD2F4(grp.getTx_center_frequency(),
		// (buffer, i) -> S7K7006.getFrequency(buffer, optOff.apply(buffer)));
		optionalsD1.add(new ValueD1S1(beamVendorGrp.getHeight_source(),
				(buffer) -> new SByte(S7K7006.getHeightSource(buffer, optOff.apply(buffer)).getByte())));

		valuesD2.add(new ValueD2F4(bathymetryGrp.getDetection_two_way_travel_time(),
				(buffer, i) -> S7K7006.getRange(buffer, i)));

		valuesD2.add(
				new ValueD2U2(bathymetryGrp.getDetection_tx_transducer_index(), (buffer, i) -> new UShort((short) 0)));

		valuesD2.add(
				new ValueD2S2(bathymetryGrp.getDetection_rx_transducer_index(), (buffer, i) -> new SShort((short) 1)));

		valuesD2.add(new ValueD2U1(bathymetryVendorGrp.getReson_detection_quality_flag(),
				(buffer, i) -> S7K7006.getQuality(buffer, i)));

		// Direct Reson flag extraction
		valuesD2.add(new ValueD2U1(bathymetryVendorGrp.getReson_detection_brightness_flag(), (buffer, i) -> {
			byte detectionInfoByte = S7K7006.getQuality(buffer, i).getByte();
			return new UByte((byte) (detectionInfoByte & 0x01)); // Bit0
		}));
		valuesD2.add(new ValueD2U1(bathymetryVendorGrp.getReson_detection_colinearity_flag(), (buffer, i) -> {
			byte detectionInfoByte = S7K7006.getQuality(buffer, i).getByte();
			return new UByte((byte) ((detectionInfoByte & 0x02) >> 1)); // Bit1
		}));
		valuesD2.add(new ValueD2U1(bathymetryVendorGrp.getReson_detection_nadir_filter_flag(), (buffer, i) -> {
			byte detectionInfoByte = S7K7006.getQuality(buffer, i).getByte();
			return new UByte((byte) ((detectionInfoByte & 0x20) >> 5)); // Bit5
		}));

		// we force here the Rx sector index for detection
		valuesD2.add(new ValueD2S2(bathymetryGrp.getDetection_tx_beam(), (buffer, i) -> new SShort((short) 0)));

		valuesD2.add(new ValueD2S1(bathymetryGrp.getDetection_type(), (buffer, i) -> {
			byte detectionInfoByte = S7K7006.getQuality(buffer, i).getByte();
			if ((detectionInfoByte & 0x08) > 0) {
				return new SByte(DetectionTypeHelper.PHASE.get());
			} else if ((detectionInfoByte & 0x04) > 0) {
				return new SByte(DetectionTypeHelper.AMPLITUDE.get());
			} else {
				// invalid value
				return new SByte(DetectionTypeHelper.INVALID.get());
			}
		}));
		valuesD2.add(new ValueD2S1(bathymetryGrp.getStatus(), (buffer, i) -> {
			byte detectionInfoByte = S7K7006.getQuality(buffer, i).getByte();
			// first check if detection has a valid phase of amplitude flag
			if ((detectionInfoByte & 0x04) == 0 && (detectionInfoByte & 0x08) == 0)
				return new SByte((byte) 2);
			// // check if colinearity fail or brightness fail
			// if ((detectionInfoByte & 0x01) > 0 || (detectionInfoByte & 0x02) > 0) {
			// // valid sounding
			// return new SByte((byte)0);
			// }
			return new SByte((byte) 0);
		}));

		valuesD2.add(new ValueD2F4(bathymetryVendorGrp.getIntensity_or_backscatter_strenth(),
				(buffer, i) -> S7K7006.getIntensity(buffer, i)));
		// retrieve quality factor, which is quite a mess since it is specially done for Ifremer by reusing backscatter
		// data field

		optionalsD2.add(new ValueD2F4(bathymetryGrp.getDetection_quality_factor(), (buffer, i) -> {
			long id = S7K7006.getDeviceIdentifier(buffer).getU();
			if ((id != S7kConstants.SEABAT_7150 && id != S7kConstants.SEABAT_7111)
					|| S7K7006.getTimestampNano(buffer) < S7kConstants.startingDateUseQF) {
				// quality factor is not set
				return new FFloat(Float.NaN);
			}
			return S7K7006.getIntensity(buffer, i);
		}));

		// retrieve back scatter if needed, which is quite a mess
		valuesD2.add(new ValueD2F4(bathymetryGrp.getDetection_backscatter_r(), (buffer, i) -> {
			long id = S7K7006.getDeviceIdentifier(buffer).getU();
			if ((id != S7kConstants.SEABAT_7150 && id != S7kConstants.SEABAT_7111)
					|| S7K7006.getTimestampNano(buffer) < S7kConstants.startingDateUseQF) {
				// quality factor is not set
				return S7K7006.getIntensity(buffer, i);
			}
			return new FFloat(Float.NaN);
		}));
		// fill backscatter calibration if we have data
		valuesD2.add(new ValueD2F4(bathymetryGrp.getDetection_backscatter_calibration(),
				(buffer, i) -> new FFloat(Float.NaN)));

		valuesD2.add(new ValueD2F4(bathymetryVendorGrp.getMin_filter_info(),
				(buffer, i) -> S7K7006.getMinFilterInfo(buffer, i)));
		valuesD2.add(new ValueD2F4(bathymetryVendorGrp.getMax_filter_info(),
				(buffer, i) -> S7K7006.getMaxFilterInfo(buffer, i)));

		optionalsD2.add(new ValueD2F4(bathymetryGrp.getDetection_z(), (buffer, i) -> new FFloat(
				S7K7006.getDepth(buffer, i).get() - S7K7006.getVehicleDepth(buffer, optOff.apply(buffer)).get())));

		optionalsD2.add(
				new ValueD2F4(bathymetryGrp.getDetection_x(), (buffer, i) -> S7K7006.getAlongTrackDistance(buffer, i)));
		optionalsD2.add(new ValueD2F4(bathymetryGrp.getDetection_y(),
				(buffer, i) -> S7K7006.getAcrossTrackDistance(buffer, i)));

		optionalsD2.add(new ValueD2F8(bathymetryGrp.getDetection_latitude(), (buffer, i) -> {
			double heading = Math.toDegrees(S7K7006.getHeading(buffer, optOff.apply(buffer)).get());
			double along = S7K7006.getAlongTrackDistance(buffer, i).get();
			double accross = S7K7006.getAcrossTrackDistance(buffer, i).get();

			double latNav = Math.toDegrees(S7K7006.getLatitude(buffer, optOff.apply(buffer)).get());
			double rayon = MbesUtils.calcNormRayon(latNav, Ellipsoid.WGS84)[1];
			double sinHeading = Math.sin(Math.toRadians(heading));
			double cosHeading = Math.cos(Math.toRadians(heading));

			double lat = latNav + Math.toDegrees(along * cosHeading - accross * sinHeading) / rayon;
			return new DDouble(lat);

		}

		));
		optionalsD2.add(new ValueD2F8(bathymetryGrp.getDetection_longitude(), (buffer, i) -> {
			double heading = Math.toDegrees(S7K7006.getHeading(buffer, optOff.apply(buffer)).get());
			double along = S7K7006.getAlongTrackDistance(buffer, i).get();
			double accross = S7K7006.getAcrossTrackDistance(buffer, i).get();

			double latNav = Math.toDegrees(S7K7006.getLatitude(buffer, optOff.apply(buffer)).get());
			double lonNav = Math.toDegrees(S7K7006.getLongitude(buffer, optOff.apply(buffer)).get());
			double norm = MbesUtils.calcNormRayon(latNav, Ellipsoid.WGS84)[0];

			double sinHeading = Math.sin(Math.toRadians(heading));
			double cosHeading = Math.cos(Math.toRadians(heading));

			double lon = lonNav + Math.toDegrees(along * sinHeading + accross * cosHeading) / norm
					/ Math.cos(Math.toRadians(latNav));
			return new DDouble(lon);
		}));

		// store pointing angle in depression angle variable. detection_beam_pointing_angle computed in post processing.   
		optionalsD2.add(new ValueD2F4(bathymetryVendorGrp.getBeam_depression_angle(),
				(buffer, i) -> new FFloat((float) Math.toDegrees(S7K7006.getPointingAngle(buffer, i).get()))));
		optionalsD2.add(new ValueD2F4(bathymetryVendorGrp.getBeam_azimuth_angle(),
				(buffer, i) -> new FFloat((float) Math.toDegrees(S7K7006.getAzimuthAngle(buffer, i).get()))));


	}

	@Override
	public void fillData(S7KFile s7kFile) throws IOException, NCException {
		DatagramBuffer buffer = new DatagramBuffer();
		long origin;
		if (s7kFile.get7006Metadata().getEntries().size() != 1) {
			throw new RuntimeException("This converter only supports one sonar");
		}
		S7KMultipingMetadata mpmd = s7kFile.get7006Metadata().getEntries().iterator().next().getValue();

		// iterate over pings (by date)
		for (Entry<Long, S7KMultipingSequenceMetadata> posentry : mpmd.getEntries()) {
			// iterate over ping sequence
			for (Entry<Integer, S7KSwathMetadata> e : posentry.getValue().getEntries()) {
				DatagramReader.readDatagram(e.getValue().getPosition().getFile(), buffer,
						e.getValue().getPosition().getSeek());
				SwathIdentifier swathId = new SwathIdentifier(S7K7006.getPingNumber(buffer).getU(),
						S7K7006.getMultiPingSequence(buffer).getU());

				for (var valueD1 : valuesD1)
					valueD1.clear();
				valuesD2.forEach(ValueD2::clear);
				for (var valueD1 : optionalsD1)
					valueD1.clear();
				optionalsD2.forEach(ValueD2::clear);

				// get origin from swath identifier (ignore if matching index not found)
				var optIndex = s7kFile.getSwathIndexer().getIndex(swathId);
				if (optIndex.isEmpty()) {
					DefaultLogger.get().warn(
							"Datagram S7K7006 associated with a missing ping is ignored (number = {}, sequence = {}).",
							swathId.getRawPingNumber(), swathId.getSwathPosition());
					continue;
				}
				origin = optIndex.get();

				for (ValueD1 v : this.valuesD1) {
					v.fill(buffer);
					v.write(new long[] { origin });
				}
				for (ValueD2 v : this.valuesD2) {
					final int nEntries = toIntExact(S7K7006.getNumberOfReceivedBeams(buffer).getU());
					for (int i = 0; i < nEntries; i++) {
						v.fill(buffer, i, i);
					}
					v.write(new long[] { origin, 0 }, new long[] { 1, nEntries });
				}

				if (S7K7006.getOptionalDataOffset(buffer).getU() != 0) {
					for (ValueD1 v : this.optionalsD1) {
						v.fill(buffer);
						v.write(new long[] { origin });
					}
					for (ValueD2 v : this.optionalsD2) {
						final int nEntries = toIntExact(S7K7006.getNumberOfReceivedBeams(buffer).getU());
						for (int i = 0; i < nEntries; i++) {
							v.fill(buffer, i, i);
						}
						v.write(new long[] { origin, 0 }, new long[] { 1, nEntries });
					}
				}
			}
		}
	}
}
