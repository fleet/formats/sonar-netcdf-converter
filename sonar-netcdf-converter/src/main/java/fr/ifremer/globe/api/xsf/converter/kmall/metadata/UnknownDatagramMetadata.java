package fr.ifremer.globe.api.xsf.converter.kmall.metadata;

import java.util.Set;
import java.util.TreeSet;

import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.DatagramMetadata;

public class UnknownDatagramMetadata extends DatagramMetadata<Integer,DatagramPosition> {

    private Set<String> unknownDatagrams;
    
    public UnknownDatagramMetadata() {
        unknownDatagrams = new TreeSet<String>();
    }
    
    public void pushUnknownType(String type) {
        unknownDatagrams.add(type);
    }
}
