package fr.ifremer.globe.api.xsf.converter.s7k.xsf;

import static java.lang.Math.toIntExact;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import com.sun.jna.Memory;

import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1F4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1F8;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1S1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1S4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U8;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.SoundSpeedProfileGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.SoundSpeedProfileVendorSpecificGrp;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K1010;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KFile;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KIndividualSensorMetadata;
import fr.ifremer.globe.api.xsf.util.DefaultLogger;
import fr.ifremer.globe.api.xsf.util.types.SByte;
import fr.ifremer.globe.api.xsf.util.types.SInt;
import fr.ifremer.globe.api.xsf.util.types.ULong;
import fr.ifremer.globe.netcdf.jna.Nc4prototypes.Vlen_t;
import fr.ifremer.globe.netcdf.ucar.NCException;

public class S7K1010Converter implements IDatagramConverter<S7KFile, XsfFromS7k> {

	// general time-related values
	private List<ValueD1> valuesD1;

	// general time-depth related values

	private SoundSpeedProfileGrp soundSpeedProfileGrp;
	private SoundSpeedProfileVendorSpecificGrp soundSpeedProfileVendorGrp;

	@Override
	public void declare(S7KFile s7kFile, XsfFromS7k xsf) throws NCException {
		valuesD1 = new LinkedList<>();

		soundSpeedProfileGrp = xsf.getSoundSpeedProfileGrp();
		soundSpeedProfileVendorGrp = new SoundSpeedProfileVendorSpecificGrp(soundSpeedProfileGrp, s7kFile,
				new HashMap<String, Integer>());

		// 1D data
		// generic data
		valuesD1.add(new ValueD1U8(soundSpeedProfileGrp.getProfile_time(), (buffer) -> {
			return new ULong(S7K1010.getTimestampNano(buffer));
		}));
		valuesD1.add(new ValueD1F8(soundSpeedProfileGrp.getLat(), (buffer) -> S7K1010.getLatitude(buffer)));
		valuesD1.add(new ValueD1F8(soundSpeedProfileGrp.getLon(), (buffer) -> S7K1010.getLongitude(buffer)));
		valuesD1.add(new ValueD1S4(soundSpeedProfileGrp.getSample_count(),
				(buffer) -> new SInt(toIntExact(S7K1010.getNumberOfSamples(buffer).getU()))));

		// specific data
		valuesD1.add(
				new ValueD1F4(soundSpeedProfileVendorGrp.getFrequency(), (buffer) -> S7K1010.getFrequency(buffer)));
		valuesD1.add(
				new ValueD1F4(soundSpeedProfileVendorGrp.getSample_rate(), (buffer) -> S7K1010.getSampleRate(buffer)));
		valuesD1.add(new ValueD1S1(soundSpeedProfileVendorGrp.getSound_velodicy_source_flag(),
				(buffer) -> new SByte(S7K1010.getSoundVelocitySourceFlag(buffer).getByte())));
		valuesD1.add(new ValueD1S1(soundSpeedProfileVendorGrp.getSound_velocity_algorithm(),
				(buffer) -> new SByte(S7K1010.getSoundVelocityAlgorithm(buffer).getByte())));
		valuesD1.add(new ValueD1S1(soundSpeedProfileVendorGrp.getConductivity_flag(),
				(buffer) -> new SByte(S7K1010.getConductivityFlag(buffer).getByte())));
		valuesD1.add(new ValueD1S1(soundSpeedProfileVendorGrp.getPressure_flag(),
				(buffer) -> new SByte(S7K1010.getPressureFlag(buffer).getByte())));
		valuesD1.add(new ValueD1S1(soundSpeedProfileVendorGrp.getPosition_flag(),
				(buffer) -> new SByte(S7K1010.getPositionFlag(buffer).getByte())));
		valuesD1.add(new ValueD1U1(soundSpeedProfileVendorGrp.getSample_content_validity(),
				(buffer) -> S7K1010.getSampleContentValidity(buffer)));

		// TODO compute depth from pressure if only pressure is available
		// TODO compute salinity from conductivity if only pressure is available

		// Declare vlen variables
		soundSpeedProfileGrp.getTemperature();
		soundSpeedProfileGrp.getSound_speed();
		soundSpeedProfileGrp.getAbsorption();

		soundSpeedProfileVendorGrp.getConductivity();
		soundSpeedProfileGrp.getSalinity();
		soundSpeedProfileVendorGrp.getPressure();
		soundSpeedProfileGrp.getSample_depth();
	}

	@Override
	public void fillData(S7KFile s7kFile) throws IOException, NCException {
		DatagramBuffer buffer = new DatagramBuffer();
		if (s7kFile.get1010Metadata().getEntries().size() != 1) {
			throw new RuntimeException("This converter only supports one sound speed profile group");
		}
		S7KIndividualSensorMetadata sensorMd = s7kFile.get1010Metadata().getEntries().iterator().next().getValue();

		long i = 0;
		for (Entry<Long, DatagramPosition> posentry : sensorMd.getDatagrams()) {
			DatagramReader.readDatagram(posentry.getValue().getFile(), buffer, posentry.getValue().getSeek());

			// handle the data that is always present

			for (ValueD1 v : this.valuesD1) {
				v.fill(buffer);
				v.write(new long[] { i });
			}

			final int sample_count = toIntExact(S7K1010.getNumberOfSamples(buffer).getU());
			int bufferlength = sample_count * Float.BYTES;

			if (bufferlength > 0) {
				Memory tempMem = new Memory(bufferlength);
				ByteBuffer local_buffer_temp = tempMem.getByteBuffer(0, bufferlength);
				Memory speedMem = new Memory(bufferlength);
				ByteBuffer local_buffer_speed = speedMem.getByteBuffer(0, bufferlength);
				Memory absMem = new Memory(bufferlength);
				ByteBuffer local_buffer_abs = absMem.getByteBuffer(0, bufferlength);

				// handle optional data
				float[] salinityOrConductivity = new float[sample_count];
				float[] depthOrPressure = new float[sample_count];
				for (int k = 0; k < sample_count; k++) {
					local_buffer_temp.putFloat(S7K1010.getWaterTemperature(buffer, k).get());
					local_buffer_speed.putFloat(S7K1010.getSoundVelocity(buffer, k).get());
					local_buffer_abs.putFloat(S7K1010.getAbsorption(buffer, k).get());
					salinityOrConductivity[k] = S7K1010.getConductivityOrSalinity(buffer, k).get();
					depthOrPressure[k] = S7K1010.getPressureOrDepth(buffer, k).get();

				}

				Vlen_t[] vlen_struct = (Vlen_t[]) new Vlen_t().toArray(1);
				vlen_struct[0].p = tempMem;
				vlen_struct[0].len = sample_count;
				soundSpeedProfileGrp.getTemperature().put(new long[] { i }, new long[] { 1 }, vlen_struct);

				vlen_struct[0].p = speedMem;
				vlen_struct[0].len = sample_count;
				soundSpeedProfileGrp.getSound_speed().put(new long[] { i }, new long[] { 1 }, vlen_struct);

				vlen_struct[0].p = absMem;
				vlen_struct[0].len = sample_count;
				soundSpeedProfileGrp.getAbsorption().put(new long[] { i }, new long[] { 1 }, vlen_struct);

				Memory mem = new Memory(bufferlength);
				ByteBuffer local_buff = mem.getByteBuffer(0, bufferlength);
				for (int index = 0; index < sample_count; index++)
					local_buff.putFloat(salinityOrConductivity[index]);
				vlen_struct[0].p = mem;
				vlen_struct[0].len = sample_count;
				if (S7K1010.getConductivityFlag(buffer).getU() == 0) {

					soundSpeedProfileVendorGrp.getConductivity().put(new long[] { i }, new long[] { 1, }, vlen_struct);
				} else {
					soundSpeedProfileGrp.getSalinity().put(new long[] { i }, new long[] { 1, }, vlen_struct);
				}
				mem = new Memory(bufferlength);
				local_buff = mem.getByteBuffer(0, bufferlength);
				for (int index = 0; index < sample_count; index++)
					local_buff.putFloat(depthOrPressure[index]);
				vlen_struct[0].p = mem;
				vlen_struct[0].len = sample_count;
				if (S7K1010.getPressureFlag(buffer).getU() == 0) {
					soundSpeedProfileVendorGrp.getPressure().put(new long[] { i }, new long[] { 1, }, vlen_struct);
				} else {
					soundSpeedProfileGrp.getSample_depth().put(new long[] { i }, new long[] { 1, }, vlen_struct);
				}
			} else {
				DefaultLogger.get().warn("Datagram S7K1010 : empty SoundSpeedProfile");
			}
			i++;
		}
	}
}
