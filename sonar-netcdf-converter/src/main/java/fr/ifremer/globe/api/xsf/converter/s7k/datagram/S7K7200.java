package fr.ifremer.globe.api.xsf.converter.s7k.datagram;

import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7K7200Metadata;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KFile;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.UInt;
import fr.ifremer.globe.api.xsf.util.types.UShort;

public class S7K7200 extends S7KBaseDatagram {
	
	public static class Device {
		public UInt deviceIdentifier;
		public UShort systemEnumerator;
	}

	@Override
	public void computeSpecificMetadata(S7KFile metadata, DatagramBuffer buffer, long type,
			DatagramPosition position, long time) {
		getSpecificMetadata(metadata).setEntry(getDeviceIdentifier(buffer).getU(), position);
	}

	@Override
	public S7K7200Metadata getSpecificMetadata(S7KFile metadata) {
		return metadata.get7200Metadata();
	}

	public static byte[] getFileIdentifier(DatagramBuffer buffer) {
		byte[] ret = new byte[16];
		buffer.byteBufferData.rewind();
		buffer.byteBufferData.get(ret);
		buffer.byteBufferData.rewind();
		return ret;
	}
	
	public static UShort getVersionNumber(DatagramBuffer buffer) {
		return TypeDecoder.read2U(buffer.byteBufferData, 16);
	}
	
	public static byte[] getSessionIdentifier(DatagramBuffer buffer) {
		byte[] ret = new byte[16];
		buffer.byteBufferData.position(20);
		buffer.byteBufferData.get(ret);
		buffer.byteBufferData.rewind();
		return ret;
	}
	
	public static UInt getRecordDataSize(DatagramBuffer buffer) {
		return TypeDecoder.read4U(buffer.byteBufferData, 36);
	}
	
	public static UInt getNumberOfDevices(DatagramBuffer buffer) {
		return TypeDecoder.read4U(buffer.byteBufferData, 40);
	}
	
	public static String getRecordingName(DatagramBuffer buffer) {
		byte[] buff = new byte[64];
		buffer.byteBufferData.position(44);
		buffer.byteBufferData.get(buff);
		buffer.byteBufferData.rewind();
		int i = 0;
		while (i < buff.length && buff[i] != 0) {
			i++;
		}
		return new String(buff, 0, i);
	}
	
	public static String getRecordingProgramVersionNumber(DatagramBuffer buffer) {
		byte[] buff = new byte[16];
		buffer.byteBufferData.position(108);
		buffer.byteBufferData.get(buff);
		buffer.byteBufferData.rewind();
		int i = 0;
		while (i < buff.length && buff[i] != 0) {
			i++;
		}
		return new String(buff, 0, i);
	}
	
	public static String getUserDefinedName(DatagramBuffer buffer) {
		byte[] buff = new byte[64];
		buffer.byteBufferData.position(124);
		buffer.byteBufferData.get(buff);
		buffer.byteBufferData.rewind();
		int i = 0;
		while (i < buff.length && buff[i] != 0) {
			i++;
		}
		return new String(buff, 0, i);
	}
	
	public static String getNotes(DatagramBuffer buffer) {
		byte[] buff = new byte[128];
		buffer.byteBufferData.position(188);
		buffer.byteBufferData.get(buff);
		buffer.byteBufferData.rewind();
		int i = 0;
		while (i < buff.length && buff[i] != 0) {
			i++;
		}
		return new String(buff, 0, i);
	}
	
	public static Device[] getDevices(DatagramBuffer buffer) {
		final int numOfDevices = (int) getNumberOfDevices(buffer).getU();
		Device[] ret = new Device[numOfDevices];
		for (int i = 0; i < numOfDevices; i++) {
			Device d = new Device();
			d.deviceIdentifier = TypeDecoder.read4U(buffer.byteBufferData, 316 + 6 * i);
			d.systemEnumerator = TypeDecoder.read2U(buffer.byteBufferData, 320 + 6 * i);
			ret[i] = d;
		}
		return ret;
	}
}
