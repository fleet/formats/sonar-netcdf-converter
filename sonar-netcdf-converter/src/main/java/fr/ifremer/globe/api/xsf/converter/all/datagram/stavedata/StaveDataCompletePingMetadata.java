package fr.ifremer.globe.api.xsf.converter.all.datagram.stavedata;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.all.datagram.PingDatagram;
import fr.ifremer.globe.api.xsf.util.ObjectBufferPool;
import fr.ifremer.globe.api.xsf.util.types.UShort;
import fr.ifremer.globe.netcdf.util.Pair;

/**
 * Represent a complete steave data ping, across all antennas
 */
public class StaveDataCompletePingMetadata {
	TreeMap<Integer, StaveDataCompleteAntennaPingMetadata> map = new TreeMap<>();

	private int samplesCount;
	private int staveCount;

	public StaveDataCompletePingMetadata() {
	    samplesCount = 0;
	}

	StaveDataCompleteAntennaPingMetadata getOrCreate(int serial, int datagramCount) {
		StaveDataCompleteAntennaPingMetadata ret = map.get(serial);
		if (ret == null) {
			ret = new StaveDataCompleteAntennaPingMetadata(datagramCount);
			map.put(serial, ret);
		}
		return ret;
	}
    
	/**
	 * get sample count for this complete ping.
	 */
	public int getSampleCount() {
		return samplesCount;
	}

	/**
	 * get the number of stave points
	 * @return
	 */
	public int getStaveCount() {
	    return staveCount;
	}

	/**
	 * Read all the datagrams for the given ping
	 * @param pool
	 * @return
	 * @throws IOException
	 */
	public List<Pair<DatagramBuffer, StaveDataCompleteAntennaPingMetadata>> read(
			ObjectBufferPool<DatagramBuffer> pool) throws IOException {
		ArrayList<Pair<DatagramBuffer, StaveDataCompleteAntennaPingMetadata>> result = new ArrayList<>();
		// for each antenna
		for (StaveDataCompleteAntennaPingMetadata antennaPing : map.values()) {
			// for each datagram
			for (int dgIdx = 0; dgIdx < antennaPing.datagramCount; dgIdx++) {
				DatagramBuffer buffer = pool.borrow();
				DatagramReader.readDatagramAt(antennaPing.positions[dgIdx].getFile(), buffer, antennaPing.positions[dgIdx].getSeek());

				result.add(new Pair<DatagramBuffer, StaveDataCompleteAntennaPingMetadata>(buffer, antennaPing));
			}
		}
		return result;
	}

	/** Helper to get ping number */
	public UShort getPingNumber(List<Pair<DatagramBuffer, StaveDataCompleteAntennaPingMetadata>> data) {
		DatagramBuffer datagram = data.get(0).getFirst();
		return PingDatagram.getPingNumber(datagram.byteBufferData);
	}

	/**
	 * compute completeness for this ping, ie all datagram have been received
	 */
	public boolean isComplete(Set<Integer> allSNs) {
		boolean result = true;
		
		for (int sn : allSNs) {
		    result &= map.containsKey(sn);
		}
		
		for (StaveDataCompleteAntennaPingMetadata antennaPing : map.values()) {
			result &= antennaPing.isComplete();
		}
		return result;
	}

	/**
	 * Called after a first full reading of all datagram is done compute
	 * indexation and links between global values (like beamIndex) and datagrams
	 */
	public void computeIndex() {
	    staveCount = 0;
	    samplesCount = 0;
	    for (StaveDataCompleteAntennaPingMetadata antenna : map.values()) {
	        staveCount += antenna.getStaveCount();
	        samplesCount += antenna.getSampleCount();
	    }
	};
	
	/**
	 * Compute the index of a sample in the entire ping (across all antennas)
	 * 
	 * @param serialNumber
	 * @param sampleIndexForAntenna
	 * @return
	 */
	public int getSampleIndexOffset(int serialNumber) {
	    int ret = 0;
	    for (int antenna : map.keySet()) {
	        if (antenna == serialNumber) {
	            return ret;
	        }
	        ret += map.get(antenna).getSampleCount();
	    }
	    return ret;
	}
	
	/** Get the number of the first stave point for the ping */
	public int getStaveIndexOffset(int serialNumber) {
	    int ret = 0;
	    for (int antenna : map.keySet()) {
	        if (antenna == serialNumber) {
	            return ret;
	        }
	        ret += map.get(antenna).getStaveCount();
	    }
	    return ret;
	}
}
