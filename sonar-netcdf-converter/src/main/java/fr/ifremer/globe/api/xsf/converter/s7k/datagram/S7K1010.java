package fr.ifremer.globe.api.xsf.converter.s7k.datagram;

import static java.lang.Math.toIntExact;

import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7K1010Metadata;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KFile;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KIndividualSensorMetadata;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.DDouble;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.api.xsf.util.types.UByte;
import fr.ifremer.globe.api.xsf.util.types.UInt;

public class S7K1010 extends S7KBaseDatagram {
	
	public static class CTDSample {
		public float condictivity_salinity;
		public float water_temperature;
		public float pressure_depth;
		public float sound_velocity;
		public float absorption;
	}

	@Override
	public void computeSpecificMetadata(S7KFile metadata, DatagramBuffer buffer, long type,
			DatagramPosition position, long time) {
		S7K1010Metadata md = getSpecificMetadata(metadata);
		S7KIndividualSensorMetadata sensorMd = md.getEntry(getDeviceIdentifier(buffer).getU());
		if (sensorMd == null) {
			sensorMd = new S7KIndividualSensorMetadata();
			md.addSensor(getDeviceIdentifier(buffer).getU(), sensorMd);
		}
		sensorMd.addDatagram(position, time, toIntExact(getNumberOfSamples(buffer).getU()));
		
		for (CTDSample s : getSmples(buffer)) {
			metadata.getStats().getSsp().register(s.sound_velocity);
		}
	}

	@Override
	public S7K1010Metadata getSpecificMetadata(S7KFile metadata) {
		return metadata.get1010Metadata();
	}

	public static FFloat getFrequency(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 0);
	}
	
	public static UByte getSoundVelocitySourceFlag(BaseDatagramBuffer buffer) {
		return TypeDecoder.read1U(buffer.byteBufferData, 4);
	}
	
	public static UByte getSoundVelocityAlgorithm(BaseDatagramBuffer buffer) {
		return TypeDecoder.read1U(buffer.byteBufferData, 5);
	}
	
	public static UByte getConductivityFlag(BaseDatagramBuffer buffer) {
		return TypeDecoder.read1U(buffer.byteBufferData, 6);
	}
	
	public static UByte getPressureFlag(BaseDatagramBuffer buffer) {
		return TypeDecoder.read1U(buffer.byteBufferData, 7);
	}
	
	public static UByte getPositionFlag(BaseDatagramBuffer buffer) {
		return TypeDecoder.read1U(buffer.byteBufferData, 8);
	}
	
	public static UByte getSampleContentValidity(BaseDatagramBuffer buffer) {
		return TypeDecoder.read1U(buffer.byteBufferData, 9);
	}
	
	public static DDouble getLatitude(BaseDatagramBuffer buffer) {
		return TypeDecoder.read8F(buffer.byteBufferData, 12);
	}
	
	public static DDouble getLongitude(BaseDatagramBuffer buffer) {
		return TypeDecoder.read8F(buffer.byteBufferData, 20);
	}
	
	public static FFloat getSampleRate(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 28);
	}
	
	public static UInt getNumberOfSamples(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4U(buffer.byteBufferData, 32);
	}
	
	public static FFloat getConductivityOrSalinity(BaseDatagramBuffer buffer, int i) {
		return TypeDecoder.read4F(buffer.byteBufferData, 36 + i * 20);
	}
	
	public static FFloat getWaterTemperature(BaseDatagramBuffer buffer, int i) {
		return TypeDecoder.read4F(buffer.byteBufferData, 40 + i * 20);
	}
	
	public static FFloat getPressureOrDepth(BaseDatagramBuffer buffer, int i) {
		return TypeDecoder.read4F(buffer.byteBufferData, 44 + i * 20);
	}
	
	public static FFloat getSoundVelocity(BaseDatagramBuffer buffer, int i) {
		return TypeDecoder.read4F(buffer.byteBufferData, 48 + i * 20);
	}
	
	public static FFloat getAbsorption(BaseDatagramBuffer buffer, int i) {
		return TypeDecoder.read4F(buffer.byteBufferData, 52 + i * 20);
	}
	
	public static CTDSample[] getSmples(BaseDatagramBuffer buffer) {
		int n = toIntExact(getNumberOfSamples(buffer).getU());
		CTDSample[] ret = new CTDSample[n];
		
		for (int i = 0; i < n; i++) {
			CTDSample v = new CTDSample();
			v.condictivity_salinity = getConductivityOrSalinity(buffer, i).get();
			v.water_temperature = getWaterTemperature(buffer, i).get();
			v.pressure_depth = getPressureOrDepth(buffer, i).get();
			v.sound_velocity = getSoundVelocity(buffer, i).get();
			v.absorption = getAbsorption(buffer, i).get();
			ret[i] = v;
		}
		
		return ret;
	}
}
