package fr.ifremer.globe.api.xsf.converter.all.datagram.rawrange78;

import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.AbstractCompleteAntennaPingSingleMetadata;

public class RawRange78CompleteAntennaPingMetadata extends AbstractCompleteAntennaPingSingleMetadata {

    public RawRange78CompleteAntennaPingMetadata(DatagramPosition position, int beamCount) {
        super(position, beamCount, 0);
    }

}
