package fr.ifremer.globe.api.xsf.converter.all.datagram.extraparameters;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagram;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.UShort;

/**
 * Stateless Clock datagram
 */
public class ExtraParameters extends BaseDatagram {

	@Override
	public ExtraParametersMetadata getSpecificMetadata(AllFile metadata) {
		return metadata.getExtraParameters();
	}

	@Override
	protected void computeSpecificMetadata(AllFile metadata, ByteBuffer datagram, byte datagramType,
			long epochTime, DatagramPosition position) {
	    getSpecificMetadata(metadata).addDatagram(position, getCounter(datagram).getU(), 1);
	}
	
	public static UShort getCounter(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 12);
	}

	public static UShort getSerialNumber(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 14);
	}

	public static UShort getContentIdentifier(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 16);
	}

	public static String getInformationDatagram(ByteBuffer datagram) {
        int lenDgm = datagram.limit() - 18;
        byte[] ret = new byte[lenDgm];
		for (int i = 18; i < lenDgm; i++) {
			ret[i] = datagram.get(i);

		}
		return new String(datagram.array(), 18, lenDgm, Charset.forName("UTF-8"));
	}
	
	public static void print(ByteBuffer datagram) {
		System.out.println(getCounter(datagram));
		System.out.println(getSerialNumber(datagram));

	}

}
