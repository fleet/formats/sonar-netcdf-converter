package fr.ifremer.globe.api.xsf.converter.s7k.datagram;

import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7K1015Metadata;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KFile;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KIndividualSensorMetadata;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.DDouble;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.api.xsf.util.types.UByte;

public class S7K1015 extends S7KBaseDatagram {

	@Override
	public void computeSpecificMetadata(S7KFile metadata, DatagramBuffer buffer, long type,
			DatagramPosition position, long time) {
		S7K1015Metadata md = getSpecificMetadata(metadata);
		S7KIndividualSensorMetadata sensorMd = md.getEntry(getDeviceIdentifier(buffer).getU());
		if (sensorMd == null) {
			sensorMd = new S7KIndividualSensorMetadata();
			md.addSensor(getDeviceIdentifier(buffer).getU(), sensorMd);
		}
		sensorMd.addDatagram(position, time, 1);
		
		metadata.getStats().getLat().register(Math.toDegrees(getLatitude(buffer).get()));
		metadata.getStats().getLon().register(Math.toDegrees(getLongitude(buffer).get()));
		metadata.getStats().getSog().register((double) getSpeedOverGround(buffer).get());
		metadata.getStats().getCog().register(Math.toDegrees(getCourseOverGround(buffer).get()));
		metadata.getStats().getHeading().register((float) Math.toDegrees(getHeading(buffer).get()));
	}

	@Override
	public S7K1015Metadata getSpecificMetadata(S7KFile metadata) {
		return metadata.get1015Metadata();
	}

	public static UByte getVerticalReference(BaseDatagramBuffer buffer) {
		return TypeDecoder.read1U(buffer.byteBufferData, 0);
	}
	
	public static DDouble getLatitude(BaseDatagramBuffer buffer) {
		return TypeDecoder.read8F(buffer.byteBufferData, 1);
	}
	
	public static DDouble getLongitude(BaseDatagramBuffer buffer) {
		return TypeDecoder.read8F(buffer.byteBufferData, 9);
	}
	
	public static FFloat getHorizontalPositionAccuracy(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 17);
	}
	
	public static FFloat getVesselHeight(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 21);
	}
	
	public static FFloat getHeightAccuracy(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 25);
	}
	
	public static FFloat getSpeedOverGround(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 29);
	}
	
	public static FFloat getCourseOverGround(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 33);
	}
	
	public static FFloat getHeading(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 37);
	}
	
	
}
