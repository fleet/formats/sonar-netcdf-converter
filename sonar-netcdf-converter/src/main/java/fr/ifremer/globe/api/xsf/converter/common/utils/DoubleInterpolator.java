package fr.ifremer.globe.api.xsf.converter.common.utils;

import java.util.TreeMap;

import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCVariable;

public class DoubleInterpolator {
	private TreeMap<Long, Double> refMap;

	/**
	 * Create and initialize an interpolator
	 * 
	 * @throws NCException
	 */
	public DoubleInterpolator(NCVariable refVariable, NCVariable refTime) throws NCException {
		// Get reference tree map
		NCDimension refDims = refVariable.getShape().get(0);
		long[] refTimeArray = refTime.get_ulong(new long[] { 0 }, new long[] { refDims.getLength() });
		double[] refValueArray = refVariable.get_double(new long[] { 0 }, new long[] { refDims.getLength() });
		refMap = new TreeMap<>(Long::compareUnsigned);
		for (int i = 0; i < refTimeArray.length; i++)
			if (Double.isFinite(refValueArray[i])) // use only valid values
				refMap.put(refTimeArray[i], refValueArray[i]);

	}

	/**
	 * Interpolate the values at the given time
	 */
	public double doubleInterpolate(long time) {
		Long timeBefore = refMap.floorKey(time);
		Long timeAfter = refMap.ceilingKey(time);

		double coeff = (timeBefore != null && timeAfter != null && timeAfter != timeBefore)
				? ((double) (time - timeBefore)) / (timeAfter - timeBefore)
				: 0;
		double valueBefore = timeBefore != null ? refMap.get(timeBefore) : refMap.get(timeAfter);
		double valueAfter = timeAfter != null ? refMap.get(timeAfter) : valueBefore;

		return (valueBefore + coeff * (valueAfter - valueBefore));
	}

	/**
	 * Interpolate the values at the given time
	 */
	public double angleInterpolate(long time) {
		Long timeBefore = refMap.floorKey(time);
		Long timeAfter = refMap.ceilingKey(time);

		double coeff = (timeBefore != null && timeAfter != null && timeAfter != timeBefore)
				? ((double) (time - timeBefore)) / (timeAfter - timeBefore)
				: 0;
		double valueBefore = timeBefore != null ? refMap.get(timeBefore) : refMap.get(timeAfter);
		double valueAfter = timeAfter != null ? refMap.get(timeAfter) : valueBefore;
		if (valueBefore < valueAfter && (valueAfter - valueBefore) > 180.f) {
			valueBefore += 360.f;
		} else if (valueBefore > valueAfter && (valueBefore - valueAfter) > 180.f) {
			valueAfter += 360.f;
		}
		return (valueBefore + coeff * (valueAfter - valueBefore));
	}

	private DoubleInterpolator() {
	}

	/**
	 * Fills a variable with the interpolation of a reference variable.
	 * 
	 * @param refVariable
	 * @param refTime
	 * @param outVariable
	 * @param outTime
	 * @throws NCException
	 */
	public static void interpolate(NCVariable refVariable, NCVariable refTime, NCVariable outVariable,
			NCVariable outTime) throws NCException {

		DoubleInterpolator it = new DoubleInterpolator(refVariable, refTime);

		// Get interpolated values
		NCDimension outDims = outVariable.getShape().get(0);
		long[] outTimeArray = outTime.get_ulong(new long[] { 0 }, new long[] { outDims.getLength() });

		double[] outValues = new double[outTimeArray.length];
		for (int i = 0; i < outTimeArray.length; i++) {
			long currentOutTime = outTimeArray[i];
			outValues[i] = it.doubleInterpolate(currentOutTime);
		}

		// Write result in output variable
		outVariable.put(new long[] { 0 }, new long[] { outDims.getLength() }, outValues);
	}

}
