package fr.ifremer.globe.api.xsf.converter;

import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;

import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.KmallFile;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KFile;

public abstract class SounderFileConverterParameters {

	private final String outputFileExtension;
	private String inputFilePath;
	private String outFilePath;
	private boolean overwrite = false;
	private boolean ignoreWaterColumn = false; // tell if we need to ignore WC datagrams and WC specific files

	/**
	 * Constructor
	 */
	public SounderFileConverterParameters(String inputFilePath, String outFilePath, String outputFileExtension) {
		this.outputFileExtension = outputFileExtension;
		this.inputFilePath = inputFilePath;
		this.outFilePath = outFilePath;

		// if output path is a directory : compute output file name with input file
		File outputFile = new File(outFilePath);
		if (outputFile.isDirectory()) {
			String inputFileName = new File(inputFilePath).getName();
			String tmp = null;
			if (inputFileName.endsWith(AllFile.EXTENSION_ALL))
				tmp = inputFileName.replace(AllFile.EXTENSION_ALL, outputFileExtension);
			if (inputFileName.endsWith(KmallFile.EXTENSION_KMALL))
				tmp = inputFileName.replace(KmallFile.EXTENSION_KMALL, outputFileExtension);
			if (inputFileName.endsWith(S7KFile.EXTENSION_S7K))
				tmp = inputFileName.replace(S7KFile.EXTENSION_S7K, outputFileExtension);
			this.outFilePath = new File(outputFile.getAbsolutePath() + File.separator + tmp).getAbsolutePath();
		}
	}

	public String getInputFilePath() {
		return inputFilePath;
	}

	public String getOutFilePath() {
		return outFilePath;
	}

	public String getOutputFileExtension() {
		return outputFileExtension;
	}

	public void setOverwrite(boolean value) {
		this.overwrite = value;
	}

	public boolean isOverwriteAllowed() {
		return overwrite;
	}

	public boolean isIgnoreWaterColumn() {
		return ignoreWaterColumn;
	}

	public void setIgnoreWaterColumn(boolean ignoreWaterColumn) {
		this.ignoreWaterColumn = ignoreWaterColumn;
	}

	public void log(Logger logger) throws IOException {
		logger.info("Input file : {} ", getInputFilePath());
		logger.info("Output file : {} ", getOutFilePath());
	}

}
