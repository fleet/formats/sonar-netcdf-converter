package fr.ifremer.globe.api.xsf.converter.all.datagram;

import fr.ifremer.globe.api.xsf.converter.common.datagram.KmSounderLib;

/**
 * Class holding Km sounder type and description for .all files
 * */
public class KmSounder {
	public final SounderType type;
	public final String name;
	public final Configuration configuration; //configuration code
	public KmSounder(int modelNumber, int stc)
	{
		String typename="EM"+String.valueOf(modelNumber);
		String lname="";
		SounderType ltype=SounderType.UNKNOWN;
		try {
			ltype=SounderType.valueOf(typename);
			lname=getModelDesc(modelNumber);
		} catch(Exception e)
		{
			//unknown sounder of a more complicated
			lname=getModelDesc(modelNumber) + " ";
			switch(stc)
			{
			case 0:
				lname+="Single Rx Single Tx";
				break;
			case 1:
				lname+="Single Head";
				break;				
			case 2:
				lname+="Dual Head";
				break;
				
			case 3:
				lname+="Single Tx + Dual Rx";
				break;
			case 4:
				lname+="Dual Tx + Dual Rx";
				break;
			case 5:
				lname +="Portable Single Head";
				break;
			case 6:
				lname+="Modular";
				break;
			default:
				break;
			}
			ltype=SounderType.UNKNOWN;
		}
		name=lname;
		type=ltype;
		this.configuration=Configuration.fromValue(stc);
	}
	
	private String getModelDesc(int modelNumber)
	{
		if(modelNumber==KmSounderLib.ME_70)
			return "ME70";
		if(modelNumber==KmSounderLib.EM_2040C)
			return "EM2040C";
		else return "EM"+String.valueOf(modelNumber);
	}
}

enum Configuration{
	SINGLE_TX_SINGLE_RX(0),
	SINGLE_HEAD(1),
	DUAL_HEAD(2),
	SINGLE_TX_DUAL_RX(3),
	DUAL_TX_DUAL_RX(4),
	PORTABLE_SINGLE_HEAD(5),
	MODULAR(6);
	@SuppressWarnings("unused")
	private final int value;

	Configuration(int value)
	{
		this.value=value;
	}
	static Configuration fromValue(int value)
	{
		return Configuration.values()[value];
	}
}

enum SounderType
{
	EM12,
	EM122,
	EM120,
	EM300,
	EM302,
	EM304,
	EM710,
	EM712,
	EM1000,
	EM1002,
	EM2000,
	EM2040_SINGLE,
	EM2040_DUAL_RX,
	EM2040_DUAL_TX,
	EM2040C,
	EM2040P,
	EM2040M,
	ME70,
	EM3000_SINGLE,
	EM3002_DUAL,
	EM3002_DUAL_3003,
	EM3002_DUAL_3004,
	EM3002_DUAL_3005,
	EM3002_DUAL_3006,
	EM3002_DUAL_3007,
	EM3002_DUAL_3008,
	UNKNOWN;
}