package fr.ifremer.globe.api.xsf.converter.all.datagram.position;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

import fr.ifremer.globe.api.xsf.converter.all.datagram.IndividualSensorMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagram;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.utils.NmeaParser;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.SByte;
import fr.ifremer.globe.api.xsf.util.types.SInt;
import fr.ifremer.globe.api.xsf.util.types.UShort;

/**
 * Stateless Position datagram
 */
public class Position extends BaseDatagram {

	@Override
	public PositionMetadata getSpecificMetadata(AllFile metadata) {
		return metadata.getPosition();
	}

	@Override
	protected void computeSpecificMetadata(AllFile metadata, ByteBuffer datagram, byte datagramType, long epochTime,
			DatagramPosition position) {

		final PositionMetadata index = getSpecificMetadata(metadata);
		int id = getSensorId(datagram);

		IndividualSensorMetadata sensorMetadata = index.getOrCreateSensor(id);

		sensorMetadata.addDatagram(position, getEpochTime(datagram), 1);
		if (isActive(datagram)) {
			index.setActiveId(id);
		}
		// Calcul de stats : pour tous les capteurs
		index.computeSOG(getSpeedOverGround(datagram).getU());
		index.computeBoundingBoxLat(Position.getLatitude(datagram).get() / 2e7);
		index.computeBoundingBoxLon(Position.getLongitude(datagram).get() / 1e7);
	}

	public static UShort getCounter(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 12);
	}

	public static UShort getSerialNumber(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 14);
	}

	public static SInt getLatitude(ByteBuffer datagram) {
		return TypeDecoder.read4S(datagram, 16);
	}

	public static SInt getLongitude(ByteBuffer datagram) {
		return TypeDecoder.read4S(datagram, 20);
	}

	public static UShort getMeasureInFixQuality(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 24);
	}

	public static UShort getSpeedOverGround(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 26);
	}

	public static UShort getCourseOverGround(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 28);
	}

	public static UShort getHeading(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 30);
	}

	public static UShort getSensorDescriptor(ByteBuffer datagram) {
		return new UShort(datagram.get(32));
	}

	public static String getDataFromSensor(ByteBuffer datagram) {
		int lenInputDgm = TypeDecoder.read1U(datagram, 33).getInt();
		return new String(datagram.array(), 34, lenInputDgm, StandardCharsets.UTF_8);
	}

	public static SByte getSensorQualityIndicator(ByteBuffer datagram) {
		String inputData = Position.getDataFromSensor(datagram);
		return new SByte(NmeaParser.getQualityIndicator(inputData));
	}

	public static int getSensorId(ByteBuffer datagram) {
		// xxxx xx01 - position system no 1
		// xxxx xx10 – position system no 2
		// xxxx xx11 – position system no 3
		return (getSensorDescriptor(datagram).getU() & 0x3);
	}

	public static boolean isActive(ByteBuffer datagram) {
		/*
		 * 
		 * 10xx xxxx – the position system is active, system time has been used
		 * 11xx xxxx - the position system is active, input datagram time has been used		 
		 */
		return ((getSensorDescriptor(datagram).getU() & 0x80) != 0);
	}

	public static void print(ByteBuffer datagram) {
		System.out.println(getCounter(datagram));
		System.out.println(getSerialNumber(datagram));

	}

}
