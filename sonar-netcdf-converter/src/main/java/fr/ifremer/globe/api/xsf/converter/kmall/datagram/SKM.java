package fr.ifremer.globe.api.xsf.converter.kmall.datagram;

import java.nio.ByteBuffer;

import fr.ifremer.globe.api.xsf.converter.all.datagram.IndividualSensorMetadata;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.KmallFile;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.SKMMetadata;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.DDouble;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.api.xsf.util.types.UByte;
import fr.ifremer.globe.api.xsf.util.types.UInt;
import fr.ifremer.globe.api.xsf.util.types.UShort;

public class SKM extends KmAllBaseDatagram {

    @Override
    public void computeSpecificMetadata(KmallFile metadata, ByteBuffer datagram, String datagramType,
            DatagramPosition position) {
    	SKMMetadata md = getSpecificMetadata(metadata);
    	
		UByte sensorId = getSensorId(datagram);
		IndividualSensorMetadata sensorMetadata = md.getOrCreateSensor(sensorId);
		long epochTime = KmAllBaseDatagram.getEpochTimeNano(datagram);
        sensorMetadata.addDatagram(position,  epochTime, SKM.getNumSamplesArray(datagram).getU());

        for (int i = 0; i < getNumSamplesArray(datagram).getU(); i++) {
                // Compute Stats
                md.addHeading(getHeading_deg(datagram, i).get());
                md.addRoll(getRoll_deg(datagram, i).get());
                md.addPitch(getPitch_deg(datagram, i).get());
                md.addHeave(getHeave_m(datagram, i).get());
        }
    }
    
    @Override
    public SKMMetadata getSpecificMetadata(KmallFile metadata) {
        return metadata.getSKMMetadata();
    }

	public static UByte getSensorId(ByteBuffer datagram) {
		return new UByte((byte) (getSensorSystem(datagram).getU() + 1));
	}

    public static boolean isActive(ByteBuffer datagram) {
        /*
         * xxxx xxx1 – Sensor is chosen as active
         */
        return (getSensorStatus(datagram).getU() & 0x01) == 1;
    }

    // Size Dgm KMBinary + KMDelayedHeave = sizeDgmKMBinary
    static final int sizeDgmKMBinary = 132;

    public static UShort getNumBytesInfoPart(ByteBuffer buffer) {
        return TypeDecoder.read2U(buffer, 16);
	}
	public static UByte getSensorSystem(ByteBuffer buffer) {
	        return TypeDecoder.read1U(buffer, 18);
	}
	public static UByte getSensorStatus(ByteBuffer buffer) {
	        return TypeDecoder.read1U(buffer, 19);
	}
	public static UShort getSensorInputFormat(ByteBuffer buffer) {
	        return TypeDecoder.read2U(buffer, 20);
	}
	public static UShort getNumSamplesArray(ByteBuffer buffer) {
	        return TypeDecoder.read2U(buffer, 22);
	}
	public static UShort getNumBytesPerSample(ByteBuffer buffer) {
	        return TypeDecoder.read2U(buffer, 24);
	}
	// + Padding sur Uint 16.

	// KM Binary 
	// ---------
	public static String getDgmTypeFromKMBinary(ByteBuffer buffer, int indexSample) {
		final int pos = buffer.position();
        byte[] dgmType = new byte[4];
        buffer.position(28 + indexSample*sizeDgmKMBinary);
        buffer.get(dgmType, 0, 4);
        buffer.position(pos);
        return new String(dgmType);
    }
    public static UShort getNumBytesDgmFromKMBinary(ByteBuffer buffer, int indexSample) {
    	return TypeDecoder.read2U(buffer,indexSample * sizeDgmKMBinary + 32);
    }
    public static UShort getDgmVersionFromKMBinary(ByteBuffer buffer, int indexSample) {
    	return TypeDecoder.read2U(buffer, indexSample * sizeDgmKMBinary + 34);
    }
    public static UInt getTimeFromKMBinary_sec(ByteBuffer buffer, int indexSample) {
    	return TypeDecoder.read4U(buffer, indexSample * sizeDgmKMBinary + 36);
    }
    public static UInt getTimeFromKMBinary_nanosec(ByteBuffer buffer, int indexSample) {
    	return TypeDecoder.read4U(buffer, indexSample * sizeDgmKMBinary + 40);
    }
    public static UInt getStatusFromKMBinary(ByteBuffer buffer, int indexSample) {
    	return TypeDecoder.read4U(buffer, indexSample * sizeDgmKMBinary + 44);
    }	
    // KM Binary - Position
    public static DDouble getLatitude_deg(ByteBuffer buffer, int indexSample) {
    	return TypeDecoder.read8F(buffer, indexSample * sizeDgmKMBinary + 48);
    }
    public static DDouble getLongitude_deg(ByteBuffer buffer, int indexSample) {
    	return TypeDecoder.read8F(buffer, indexSample * sizeDgmKMBinary + 56);
    }
    public static FFloat getEllipsoidHeight_m(ByteBuffer buffer, int indexSample)
    {
    	return TypeDecoder.read4F(buffer, indexSample * sizeDgmKMBinary + 64);
    }
    // KM Binary - Attitude
    public static FFloat getRoll_deg(ByteBuffer buffer, int indexSample) {
    	return TypeDecoder.read4F(buffer, indexSample * sizeDgmKMBinary + 68);
    }
    public static FFloat getPitch_deg(ByteBuffer buffer, int indexSample) {
    	return TypeDecoder.read4F(buffer, indexSample * sizeDgmKMBinary + 72);
    }
    public static FFloat getHeading_deg(ByteBuffer buffer, int indexSample) {
    	return TypeDecoder.read4F(buffer, indexSample * sizeDgmKMBinary + 76);
    }
    public static FFloat getHeave_m(ByteBuffer buffer, int indexSample) {
    	return TypeDecoder.read4F(buffer, indexSample * sizeDgmKMBinary + 80);
    }
    // KM Binary - Rates
    public static FFloat getRollRate(ByteBuffer buffer, int indexSample) {
    	return TypeDecoder.read4F(buffer, indexSample * sizeDgmKMBinary + 84);
    }
    public static FFloat getPitchRate(ByteBuffer buffer, int indexSample) {
    	return TypeDecoder.read4F(buffer, indexSample * sizeDgmKMBinary + 88);
    }
    public static FFloat getYawRate(ByteBuffer buffer, int indexSample) {
    	return TypeDecoder.read4F(buffer, indexSample * sizeDgmKMBinary + 92);
    }
    // KM Binary - Velocities
    public static FFloat getVelNorth(ByteBuffer buffer, int indexSample) {
    	return TypeDecoder.read4F(buffer, indexSample * sizeDgmKMBinary + 96);
    }
    public static FFloat getVelEast(ByteBuffer buffer, int indexSample) {
    	return TypeDecoder.read4F(buffer, indexSample * sizeDgmKMBinary + 100);
    }
    public static FFloat getVelDown(ByteBuffer buffer, int indexSample) {
    	return TypeDecoder.read4F(buffer, indexSample * sizeDgmKMBinary + 104);
    }
    // KM Binary - Errors
    public static FFloat getLatitudeError_m(ByteBuffer buffer, int indexSample) {

    	return TypeDecoder.read4F(buffer, indexSample * sizeDgmKMBinary + 108);
    }
    public static FFloat getLongitudeError_m(ByteBuffer buffer, int indexSample)
    {
    	return TypeDecoder.read4F(buffer, indexSample * sizeDgmKMBinary + 112);
    }
    public static FFloat getEllipsoidHeightError_m(ByteBuffer buffer, int indexSample) {
    	return TypeDecoder.read4F(buffer, indexSample * sizeDgmKMBinary + 116);
    }
    public static FFloat getRollError_deg(ByteBuffer buffer, int indexSample) {
    	return TypeDecoder.read4F(buffer, indexSample * sizeDgmKMBinary + 120);
    }
    public static FFloat getPitchError_deg(ByteBuffer buffer, int indexSample) {
    	return TypeDecoder.read4F(buffer, indexSample * sizeDgmKMBinary + 124);
    }
    public static FFloat getHeadingError_deg(ByteBuffer buffer, int indexSample)
    {
    	return TypeDecoder.read4F(buffer, indexSample * sizeDgmKMBinary + 128);
    }
    public static FFloat getHeaveError_m(ByteBuffer buffer, int indexSample) {
    	return TypeDecoder.read4F(buffer, indexSample * sizeDgmKMBinary + 132);
    }
    // KM Binary - Acceleration
    public static FFloat getNorthAcceleration(ByteBuffer buffer, int indexSample)
    {
    	return TypeDecoder.read4F(buffer, indexSample * sizeDgmKMBinary + 136);
    }
    public static FFloat getEastAcceleration(ByteBuffer buffer, int indexSample)
    {
    	return TypeDecoder.read4F(buffer, indexSample * sizeDgmKMBinary + 140);
    }
    public static FFloat getDownAcceleration(ByteBuffer buffer, int indexSample)
    {
    	return TypeDecoder.read4F(buffer, indexSample * sizeDgmKMBinary + 144);
    }
    
    
    // KM Delayed Heave
    public static UInt getTimeFromKMDelayedHeave_sec(ByteBuffer buffer, int indexSample) {
    	return TypeDecoder.read4U(buffer, 148 + indexSample * sizeDgmKMBinary);
    }
    public static UInt getTimeFromKMDelayedHeave_nanosec(ByteBuffer buffer, int indexSample) {
    	return TypeDecoder.read4U(buffer, 152 + indexSample * sizeDgmKMBinary);
    }
    public static FFloat getDelayedHeave(ByteBuffer buffer, int indexSample) {
    	return TypeDecoder.read4F(buffer, 156 + indexSample * sizeDgmKMBinary);
    }
}
