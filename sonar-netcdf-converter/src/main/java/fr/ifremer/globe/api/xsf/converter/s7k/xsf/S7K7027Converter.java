package fr.ifremer.globe.api.xsf.converter.s7k.xsf;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1F4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1F8;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1S1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U8;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2F4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2F8;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2S1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2U1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2U2;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2U4;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.BathymetryGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.BeamGroup1Grp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.BathymetryVendorSpecificGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.BeamGroup1VendorSpecificGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.types.DetectionTypeHelper;
import fr.ifremer.globe.api.xsf.converter.common.xsf.types.SonarDetectionStatus;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K7027;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7K7030InstallationParameters;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KFile;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KMultipingMetadata;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KMultipingSequenceMetadata;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KSwathMetadata;
import fr.ifremer.globe.api.xsf.util.DefaultLogger;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.types.DDouble;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.api.xsf.util.types.SByte;
import fr.ifremer.globe.api.xsf.util.types.UByte;
import fr.ifremer.globe.api.xsf.util.types.UInt;
import fr.ifremer.globe.api.xsf.util.types.ULong;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCVariable;
import fr.ifremer.globe.utils.maths.MatrixBuilder;
import fr.ifremer.globe.utils.maths.Ops;
import fr.ifremer.globe.utils.maths.RotationMatrix3x3D;
import fr.ifremer.globe.utils.maths.Vector3D;
import fr.ifremer.globe.utils.mbes.Ellipsoid;
import fr.ifremer.globe.utils.mbes.MbesUtils;

public class S7K7027Converter implements IDatagramConverter<S7KFile, XsfFromS7k> {

	protected final RotationMatrix3x3D attitudeMatrix = new RotationMatrix3x3D();
	protected final Vector3D txAntennaSCS = new Vector3D();

	private NCVariable status;
	private List<ValueD1> valuesD1;
	private List<ValueD2> valuesD2;
	private List<ValueD1> optionalsD1;
	private List<ValueD2> optionalsD2;

	@Override
	public void declare(S7KFile s7kFile, XsfFromS7k xsf) throws NCException {
		this.valuesD1 = new LinkedList<>();
		this.valuesD2 = new LinkedList<>();
		this.optionalsD1 = new LinkedList<>();
		this.optionalsD2 = new LinkedList<>();
		BeamGroup1Grp beamGrp = xsf.getBeamGroup();
		BathymetryGrp bathyGrp = xsf.getBathyGrp();
		BathymetryVendorSpecificGrp bathymetryVendor = xsf.getBathyVendor();
		BeamGroup1VendorSpecificGrp beamVendor = xsf.getBeamVendor();

		status = bathyGrp.getStatus();
		valuesD1.add(new ValueD1U8(beamGrp.getPing_time(), (buffer) -> new ULong(S7K7027.getTimestampNano(buffer))));
		valuesD1.add(new ValueD1U4(beamVendor.getPing_raw_count(),
				(buffer) -> S7K7027.getPingNumber(buffer)));
		valuesD1.add(new ValueD1U1(bathyGrp.getMultiping_sequence(),
				(buffer) -> new UByte((byte) S7K7027.getMultiPingSequence(buffer).getU())));
		valuesD1.add(
				new ValueD1U1(beamVendor.getDetection_algorithm(), (buffer) -> S7K7027.getDetectionAlgorithm(buffer)));
		valuesD1.add(new ValueD1U4(beamVendor.getDetection_flags(), (buffer) -> S7K7027.get7027Flags(buffer)));

		valuesD1.add(new ValueD1F4(beamVendor.getSample_rate(), (buffer) -> S7K7027.getSamplingRate(buffer)));
		valuesD1.add(new ValueD1F4(beamVendor.getDetection_tx_steering_angle(),
				(buffer) -> new FFloat((float) Math.toDegrees(S7K7027.getTxAngle(buffer).get()))));

		valuesD2.add(new ValueD2U2(bathymetryVendor.getDetection_beam_descriptor(),
				(buffer, i) -> S7K7027.getBeamDescriptor(buffer, i)));
		valuesD2.add(new ValueD2F4(bathymetryVendor.getDetection_point(),
				(buffer, i) -> S7K7027.getDetectionPoint(buffer, i)));
		valuesD2.add(
				new ValueD2F4(bathymetryVendor.getDetection_rx_angle(), (buffer, i) -> S7K7027.getRxAngle(buffer, i)));
		valuesD2.add(new ValueD2S1(bathyGrp.getDetection_type(), (buffer, i) -> {
			byte value = DetectionTypeHelper.INVALID.get();
			final int flags = S7K7027.getDetectionFlags(buffer, i).getInt();
			if ((flags & 0x0001) == 0x0001) {
				value = DetectionTypeHelper.AMPLITUDE.get();
			}
			if ((flags & 0x0002) == 0x0002) {
				value = DetectionTypeHelper.PHASE.get();
			}
			return new SByte(value);
		}));
		valuesD2.add(new ValueD2U1(bathymetryVendor.getDetection_uncertainty_availability(), (buffer, i) -> {
			final int flags = S7K7027.getDetectionFlags(buffer, i).getInt();
			return new UByte((byte) ((flags & 0x0100) > 0 ? 1 : 0));
		}));
		valuesD2.add(new ValueD2U1(bathymetryVendor.getDetection_quality_availability(), (buffer, i) -> {
			final int flags = S7K7027.getDetectionFlags(buffer, i).getInt();
			return new UByte((byte) ((flags & 0x000F) >> 2));
		}));

		valuesD2.add(
				new ValueD2U4(bathymetryVendor.getDetection_quality(), (buffer, i) -> S7K7027.getQuality(buffer, i)));
		valuesD2.add(new ValueD2S1(bathyGrp.getStatus(), (buffer, i) -> {
			int quality = S7K7027.getQuality(buffer, i).getInt();
			return new SByte(((quality & 0x01) > 0 && (quality & 0x02) > 0
					? SonarDetectionStatus.VALID
					: SonarDetectionStatus.INVALID_ACQUISITION).getValue());
		}));
		valuesD2.add(new ValueD2F4(bathymetryVendor.getDetection_uncertainty(),
				(buffer, i) -> S7K7027.getUncertainty(buffer, i)));

		if (s7kFile.get7027Metadata().getEntries().size() != 1) {
			throw new RuntimeException("This converter only supports one sonar");
		}
		S7KMultipingMetadata mpmd = s7kFile.get7027Metadata().getEntries().iterator().next().getValue();
		// show the optional data
		if (mpmd.hasOptionalData()) {
			this.optionalsD1.add(new ValueD1F4(beamGrp.getSample_interval(),
					(buffer) -> new FFloat(1 / S7K7027.getFrequency(buffer).get())));
			this.optionalsD1.add(new ValueD1F8(beamGrp.getPlatform_latitude(),
					(buffer) -> new DDouble(Math.toDegrees(S7K7027.getLatitude(buffer).get()))));
			this.optionalsD1.add(new ValueD1F8(beamGrp.getPlatform_longitude(),
					(buffer) -> new DDouble(Math.toDegrees(S7K7027.getLongitude(buffer).get()))));
			this.optionalsD1.add(new ValueD1F4(beamGrp.getPlatform_heading(),
					(buffer) -> new FFloat((float) Math.toDegrees(S7K7027.getHeading(buffer).get()))));
			this.optionalsD1.add(new ValueD1S1(beamVendor.getHeight_source(),
					(buffer) -> new SByte(S7K7027.getHeightSource(buffer).getByte())));
			this.optionalsD1.add(new ValueD1F4(beamGrp.getPlatform_roll(),
					(buffer) -> new FFloat((float) Math.toDegrees(S7K7027.getRoll(buffer).get()))));
			this.optionalsD1.add(new ValueD1F4(beamGrp.getPlatform_pitch(),
					(buffer) -> new FFloat((float) Math.toDegrees(S7K7027.getPitch(buffer).get()))));
			this.optionalsD1.add(new ValueD1F4(beamVendor.getTide(),
					(buffer) -> new FFloat((float) Math.toDegrees(S7K7027.getTide(buffer).get()))));
			this.optionalsD1.add(new ValueD1F4(beamVendor.getSounder_heave(),
					(buffer) -> new FFloat((float) Math.toDegrees(S7K7027.getHeave(buffer).get()))));
			this.optionalsD1.add(new ValueD1F4(beamGrp.getPlatform_vertical_offset(),
					(buffer) -> new FFloat(-1 * S7K7027.getVehiculeDepth(buffer).get())));
			this.optionalsD2.add(new ValueD2F4(bathyGrp.getDetection_x(),
					(buffer, i) -> S7K7027.getDetectionAlongTrackDistance(buffer, i)));
			this.optionalsD2.add(new ValueD2F4(bathyGrp.getDetection_y(),
					(buffer, i) -> S7K7027.getDetectionAcrossTrackDistance(buffer, i)));

			optionalsD1.add(new ValueD1F4(beamGrp.getTx_transducer_depth(), (buffer) -> {
				// 1) get antenna Z in Surface Coordinate System (ref point)
				final S7K7030InstallationParameters si = s7kFile.getInstallationParameters(s7kFile.getMaxDate());

				float pitch = S7K7027.getPitch(buffer).get();
				float roll = S7K7027.getRoll(buffer).get();

				MatrixBuilder.setDegRotationMatrix(0, pitch, roll, attitudeMatrix);

				Ops.mult(attitudeMatrix,
						new Vector3D(si.getTransmitArrayY(), si.getTransmitArrayX(), -si.getTransmitArrayZ()),
						txAntennaSCS);

				// 2) get reference_point_depth
				double vehicleDepth = S7K7027.getVehiculeDepth(buffer).get();

				// transducer_depth = reference_point_depth + antenna_z_SCS
				return new FFloat((float) (vehicleDepth + txAntennaSCS.getZ()));
			}));

			this.optionalsD2.add(new ValueD2F8(bathyGrp.getDetection_latitude(), (buffer, i) -> {
				double heading = Math.toDegrees(S7K7027.getHeading(buffer).get());
				double along = S7K7027.getDetectionAlongTrackDistance(buffer, i).get();
				double accross = S7K7027.getDetectionAcrossTrackDistance(buffer, i).get();

				double latNav = Math.toDegrees(S7K7027.getLatitude(buffer).get());
				double rayon = MbesUtils.calcNormRayon(latNav, Ellipsoid.WGS84)[1];
				double sinHeading = Math.sin(Math.toRadians(heading));
				double cosHeading = Math.cos(Math.toRadians(heading));

				double lat = latNav + Math.toDegrees(along * cosHeading - accross * sinHeading) / rayon;
				return new DDouble(lat);

			}

			));
			this.optionalsD2.add(new ValueD2F8(bathyGrp.getDetection_longitude(), (buffer, i) -> {
				double heading = Math.toDegrees(S7K7027.getHeading(buffer).get());
				double along = S7K7027.getDetectionAlongTrackDistance(buffer, i).get();
				double accross = S7K7027.getDetectionAcrossTrackDistance(buffer, i).get();
				double latNav = Math.toDegrees(S7K7027.getLatitude(buffer).get());
				double lonNav = Math.toDegrees(S7K7027.getLongitude(buffer).get());
				double norm = MbesUtils.calcNormRayon(latNav, Ellipsoid.WGS84)[0];

				double sinHeading = Math.sin(Math.toRadians(heading));
				double cosHeading = Math.cos(Math.toRadians(heading));

				double lon = lonNav + Math.toDegrees(along * sinHeading + accross * cosHeading) / norm
						/ Math.cos(Math.toRadians(latNav));
				return new DDouble(lon);
			}));

			this.optionalsD2.add(new ValueD2F4(bathyGrp.getDetection_z(), (buffer, i) -> new FFloat(
					S7K7027.getDetectionDepth(buffer, i).get() - S7K7027.getVehiculeDepth(buffer).get())));

			// store pointing angle in depression angle variable. detection_beam_pointing_angle computed in post processing.   
			this.optionalsD2.add(new ValueD2F4(bathymetryVendor.getBeam_depression_angle(),
					(buffer, i) -> new FFloat((float) Math.toDegrees(S7K7027.getBeamPointingAngle(buffer, i).get()))));
			this.optionalsD2.add(new ValueD2F4(bathymetryVendor.getBeam_azimuth_angle(),
					(buffer, i) -> new FFloat((float) Math.toDegrees(S7K7027.getBeamAzimuthAngle(buffer, i).get()))));

		}
	}

	@Override
	public void fillData(S7KFile s7kFile) throws IOException, NCException {
		DatagramBuffer buffer = new DatagramBuffer();

		if (s7kFile.get7027Metadata().getEntries().size() != 1) {
			throw new RuntimeException("This converter only supports one sonar");
		}
		S7KMultipingMetadata mpmd = s7kFile.get7027Metadata().getEntries().iterator().next().getValue();

		for (Entry<Long, S7KMultipingSequenceMetadata> posentry : mpmd.getEntries()) {
			for (Entry<Integer, S7KSwathMetadata> e : posentry.getValue().getEntries()) {
				valuesD2.forEach(ValueD2::clear);
				optionalsD2.forEach(ValueD2::clear);
				DatagramReader.readDatagram(e.getValue().getPosition().getFile(), buffer,
						e.getValue().getPosition().getSeek());
				SwathIdentifier swathId = new SwathIdentifier(S7K7027.getPingNumber(buffer).getU(),
						S7K7027.getMultiPingSequence(buffer).getU());

				// get origin from swath identifier (ignore if matching index not found)
				var optIndex = s7kFile.getSwathIndexer().getIndex(swathId);
				if (optIndex.isEmpty()) {
					DefaultLogger.get().warn(
							"Datagram S7K7027 associated with a missing ping is ignored (number = {}, sequence = {}).",
							swathId.getRawPingNumber(), swathId.getSwathPosition());
					continue;
				}
				int origin = optIndex.get();

				for (ValueD1 v : this.valuesD1) {
					v.fill(buffer);
					v.write(new long[] { origin });
				}

				if (e.getValue().hasOptionalData()) {
					for (ValueD1 v : this.optionalsD1) {
						v.fill(buffer);
						v.write(new long[] { origin });
					}
				}

				byte st = 0x0;
				if (posentry.getValue().isComplete(mpmd.getSequenceLength())) {
					st &= 0x1;
				}

				final int numOfDetections = Math.toIntExact(S7K7027.getNumberOfDetectionPoints(buffer).getU());
				for (int i = 0; i < numOfDetections; i++) {
					status.put(new long[] { origin, i }, new long[] { 1, 1 }, new byte[] { st });
				}
				for (ValueD2 v : this.valuesD2) {
					for (int i = 0; i < numOfDetections; i++) {
						v.fill(buffer, i, i);
					}
					v.write(new long[] { origin, 0 }, new long[] { 1, numOfDetections });
				}
				if (e.getValue().hasOptionalData()) {
					for (ValueD2 v : this.optionalsD2) {
						for (int i = 0; i < numOfDetections; i++) {
							v.fill(buffer, i, i);
						}
						v.write(new long[] { origin, 0 }, new long[] { 1, numOfDetections });
					}
				}
			}
		}
	}
}
