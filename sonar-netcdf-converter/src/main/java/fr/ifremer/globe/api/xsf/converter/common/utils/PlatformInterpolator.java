package fr.ifremer.globe.api.xsf.converter.common.utils;

import java.util.TreeMap;


/**
 * Tools for platform position and attitude interpolation
 */
public class PlatformInterpolator {

    private PlatformInterpolator() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Get interpolated roll, pitch, heave, heading values for a specific timestamp.
     */
    public static double[] getAttitudeByInterpolation(long currentTime, TreeMap<Long, float[]> attitudeValues) {

        // Get attitude values before and after the specified time stamp
        Long timeBefore = attitudeValues.floorKey(currentTime);
        Long timeAfter = attitudeValues.ceilingKey(currentTime);

        // If no attitude values have been found before; use the 2 first values
        if (timeBefore == null) {
            timeBefore = attitudeValues.firstKey();
            timeAfter = attitudeValues.higherKey(timeBefore);
        }
        // If no attitude values have been found after; use the 2 last values
        if (timeAfter == null) {
            timeAfter = attitudeValues.lastKey();
            timeBefore = attitudeValues.lowerKey(timeAfter);
        }

        float[] attitudeBefore = attitudeValues.get(timeBefore);
        float[] attitudeAfter = attitudeValues.get(timeAfter);
        double rollBefore = attitudeBefore[0];
        double pitchBefore = attitudeBefore[1];
        double heaveBefore = attitudeBefore[2];
        double headingBefore = attitudeBefore[3];
        double rollAfter = attitudeAfter[0];
        double pitchAfter = attitudeAfter[1];
        double heaveAfter = attitudeAfter[2];
        double headingAfter = attitudeAfter[3];

        // Linear interpolation
        double coeff = !timeAfter.equals(timeBefore) ? ((double) (currentTime - timeBefore)) / (timeAfter - timeBefore)
                : 0;

        double roll = rollBefore + coeff * (rollAfter - rollBefore);
        double pitch = pitchBefore + coeff * (pitchAfter - pitchBefore);
        double heave = heaveBefore + coeff * (heaveAfter - heaveBefore);
        if (headingAfter - headingBefore > 180.0) {
            headingBefore += 360.0;
        } else if (headingBefore - headingAfter > 180.0) {
            headingAfter += 360.0;
        }
        double heading = headingBefore + coeff * (headingAfter - headingBefore);
        if (heading >= 360.0) {
            heading -= 360.0;
        }

        return new double[]{roll, pitch, heave, heading};
    }
}
