package fr.ifremer.globe.api.xsf.converter.all.datagram.unknown;

import java.util.Set;
import java.util.TreeSet;

import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.DatagramMetadata;

public class UnknownMetadata extends DatagramMetadata<Integer,DatagramPosition> {
	/**
	 * the list of unknown datagram types
	 */
	private Set<Integer> unknownDatagram = new TreeSet<Integer>();

	public void pushUnknownType(int type) {
		unknownDatagram.add(type);
	}
}
