package fr.ifremer.globe.api.xsf.converter.common.utils.values.d2;

import java.util.Arrays;

import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.api.xsf.util.types.UInt;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCVariable;

/***
 * class allocating storage for 2D matrix, and giving a interface allowing to fill the data *
 */
public class ValueD2U4 extends ValueD2 {
	protected int[] dataOut;
	private ValueProvider valueProvider;
	private UInt fillValue;

	/***
	 * Constructor, allocate netcdf buffer for one variable and one ping
	 */
	public ValueD2U4(NCVariable variable, ValueProvider filler) {
		super(variable);
		// allocate storage
		this.dataOut = new int[dim];
		this.fillValue=new UInt(variable.getIntFillValue());
		clear();
		this.valueProvider = filler;

	}
	
	@Override
	public void clear() {
		Arrays.fill(this.dataOut, fillValue.getInt());
	}

	public ValueD2U4(NCVariable variable, ValueProvider filler, UInt fillValue) {
		this(variable, filler);
		this.fillValue=new UInt(variable.getIntFillValue());
		setDefault(fillValue);
	}

	public ValueD2U4(NCVariable variable, int dim, ValueProvider filler) {
		super(variable, dim);
		this.dataOut = new int[dim];
		this.fillValue=new UInt(variable.getIntFillValue());
		this.valueProvider = filler;
	}

	public ValueD2U4(NCVariable variable, int dim, ValueProvider filler, UInt fillValue) {
		this(variable, dim, filler);
		this.fillValue=new UInt(variable.getIntFillValue());
		setDefault(fillValue);
	}

	public void setDefault(UInt dft) {
		for (int i = 0; i < dataOut.length; i++) {
			dataOut[i] = dft.getInt();
		}
	}

	public interface ValueProvider {
		public UInt get(BaseDatagramBuffer buffer, int beamIndexSource);
	}

	/**
	 * call the lambda to retrieve the value and set it in the allocated netcdf buffer dataOut
	 */
	@Override
	public void fill(BaseDatagramBuffer buffer, int beamIndexSource, int beamIndexDest) {
		dataOut[beamIndexDest] = valueProvider.get(buffer, beamIndexSource).getInt();
	}
	
	public void directFill(int beamIndexDest, int value) {
		dataOut[beamIndexDest] = value;
	}

	@Override
	public void write(long[] origin, long[] count) throws NCException {
		try {
			variable.putu(origin, count, dataOut);
		} catch (Exception e) {
			logger.error(String.format("Error while writing variable %s (%s)", variable.getName(), e.getMessage()));
			throw e;
		}
	}
}
