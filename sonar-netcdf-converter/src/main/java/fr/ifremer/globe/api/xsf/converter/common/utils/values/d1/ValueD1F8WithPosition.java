package fr.ifremer.globe.api.xsf.converter.common.utils.values.d1;

import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.api.xsf.util.types.DDouble;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCVariable;

/***
 * class allocating storage for 1D matrix, and giving a interface allowing to fill the data with latitude and longitude
 * values
 */
public class ValueD1F8WithPosition extends ValueD1WithPosition {

	protected double[] dataOut;
	ValueProvider valueProvider;

	/**
	 * Constructor, allocate netcdf buffer for one variable and one ping
	 * 
	 * @throws NCException
	 */
	public ValueD1F8WithPosition(NCVariable variable, ValueProvider filler) throws NCException {
		this(variable, filler, false);
	}

	/**
	 * Constructor, allocate netcdf buffer for one variable and all pings
	 */
	public ValueD1F8WithPosition(NCVariable variable, ValueProvider filler, boolean bufferCapacityIsDimension) {
		super(variable, bufferCapacityIsDimension);
		dataOut = new double[dataOutCapacity];
		valueProvider = filler;
	}

	public interface ValueProvider {
		public DDouble get(BaseDatagramBuffer buffer, double lat, double lon, double speed);
	}

	/** {@inheritDoc} */
	@Override
	public void fill(BaseDatagramBuffer buffer, double lat, double lon, double speed, int position) throws NCException {
		if (dataOutCapacity == 1) {
			dataOut[0] = valueProvider.get(buffer, lat, lon, speed).get();
			dataOutPosition = 1;
		} else if (position < dataOutCapacity) {
			dataOut[position] = valueProvider.get(buffer, lat, lon, speed).get();
			dataOutPosition = position + 1;
		} else {
			// For debugging purpose
			throw new NCException("Try to add a value over the buffer capacity");
		}
	}

	@Override
	public void write(long[] origin) throws NCException {
		try {
			variable.put(origin, new long[] { dataOutCapacity }, dataOut);
			dataOutPosition = 0;
		} catch (Exception e) {
			logger.error(String.format("Error while writing variable %s (%s)", variable.getName(), e.getMessage()));
			throw e;
		}
	}

	@Override
	public void clear() throws NCException {
		if (dataOutCapacity == 1) {
			dataOut[0] = variable.getDoubleFillValue();
		} else {
			// Fill the buffer with the variable (prevent overwriting previous values)
			var variableData = variable.get_double(new long[] { 0l }, new long[] { dataOutCapacity });
			System.arraycopy(variableData, 0, dataOut, 0, dataOutCapacity);
		}
		dataOutPosition = 0;
	}

}
