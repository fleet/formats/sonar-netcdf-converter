package fr.ifremer.globe.api.xsf.converter.kmall.xsf;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import com.sun.jna.Memory;

import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1F8;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1S4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1String;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U8;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfFromKongsberg;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.SoundSpeedProfileGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.SoundSpeedProfileVendorSpecificGrp;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.KmAllBaseDatagram;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.SVP;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.KmallFile;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.SVPMetadata;
import fr.ifremer.globe.api.xsf.util.SimpleIdentifier;
import fr.ifremer.globe.api.xsf.util.types.SInt;
import fr.ifremer.globe.api.xsf.util.types.ULong;
import fr.ifremer.globe.netcdf.jna.Nc4prototypes.Vlen_t;
import fr.ifremer.globe.netcdf.ucar.NCException;

public class SVPConverter implements IDatagramConverter<KmallFile, XsfFromKongsberg> {

	private List<ValueD1> profileValues = new LinkedList<>();;
	private KmallFile stats;
	private SVPMetadata metadata;
	private SoundSpeedProfileGrp soundSpeedProfileGrp;
	private SoundSpeedProfileVendorSpecificGrp soundSpeedProfileVendorGrp;

	public SVPConverter(KmallFile stats) {
		this.stats = stats;
		this.metadata = this.stats.getSVPMetadata();
	}

	@Override
	public void declare(KmallFile allFile, XsfFromKongsberg xsf) throws NCException {

		soundSpeedProfileGrp = xsf.getSoundSpeedProfileGrp();
		soundSpeedProfileVendorGrp = xsf.getSvp();

		// declare all the variables
		profileValues.add(new ValueD1U8(soundSpeedProfileGrp.getProfile_time(),
				buffer -> new ULong(KmAllBaseDatagram.getEpochTimeNano(buffer.byteBufferData))));

		profileValues.add(new ValueD1U8(soundSpeedProfileGrp.getMeasure_time(),
				buffer -> new ULong(SVP.getTime_sec(buffer.byteBufferData).getU() * 1_000_000_000)));

		profileValues.add(new ValueD1S4(soundSpeedProfileGrp.getSample_count(),
				buffer -> new SInt(SVP.getSamplesCount(buffer.byteBufferData).getU())));

		profileValues.add(new ValueD1String(soundSpeedProfileVendorGrp.getSensor_format(),
				buffer -> SVP.getSensorFormat(buffer.byteBufferData)));

		profileValues.add(
				new ValueD1F8(soundSpeedProfileGrp.getLat(), buffer -> SVP.getLatitude_deg(buffer.byteBufferData)));

		profileValues.add(
				new ValueD1F8(soundSpeedProfileGrp.getLon(), buffer -> SVP.getLongitude_deg(buffer.byteBufferData)));

		soundSpeedProfileGrp.getSample_depth();
		soundSpeedProfileGrp.getSound_speed();
		soundSpeedProfileGrp.getTemperature();
		soundSpeedProfileGrp.getSalinity();
	}

	@Override
	public void fillData(KmallFile kmAllFile) throws IOException, NCException {
		long[] origin = new long[] { 0, 0 };

		DatagramBuffer buffer = new DatagramBuffer();
		for (Entry<SimpleIdentifier, DatagramPosition> posentry : metadata.getDatagrams()) {
			final DatagramPosition pos = posentry.getValue();
			DatagramReader.readDatagram(pos.getFile(), buffer, pos.getSeek());
			// read the data from the source file
			for (ValueD1 value : this.profileValues) {
				value.fill(buffer);
			}
			int sample_count = SVP.getSamplesCount(buffer.byteBufferData).getU();
			int bufferlength = sample_count * Float.BYTES;

			Memory depthMem = new Memory(bufferlength);
			ByteBuffer local_buffer_depth = depthMem.getByteBuffer(0, bufferlength);
			Memory speedMem = new Memory(bufferlength);
			ByteBuffer local_buffer_speed = speedMem.getByteBuffer(0, bufferlength);
			Memory tempMem = new Memory(bufferlength);
			ByteBuffer local_buffer_temp = tempMem.getByteBuffer(0, bufferlength);
			Memory salinityMem = new Memory(bufferlength);
			ByteBuffer local_buff_salinity = salinityMem.getByteBuffer(0, bufferlength);

			for (int k = 0; k < sample_count; k++) {
				local_buffer_depth.putFloat(SVP.getDepth_m(buffer.byteBufferData, k).get());
				local_buffer_speed.putFloat(SVP.getSoundVelocity_mPerSec(buffer.byteBufferData, k).get());
				local_buffer_temp.putFloat(SVP.getTemp_C(buffer.byteBufferData, k).get());
				local_buff_salinity.putFloat(SVP.getSalinity(buffer.byteBufferData, k).get());
			}

			long[] originD1 = new long[] { origin[0] };

			// flush into the output file
			for (ValueD1 value : this.profileValues) {
				value.write(originD1);
			}

			Vlen_t[] vlen_struct = (Vlen_t[]) new Vlen_t().toArray(1);
			vlen_struct[0].p = depthMem;
			vlen_struct[0].len = sample_count;
			soundSpeedProfileGrp.getSample_depth().put(new long[] { origin[0] }, new long[] { 1 }, vlen_struct);
			vlen_struct[0].p = speedMem;
			soundSpeedProfileGrp.getSound_speed().put(new long[] { origin[0] }, new long[] { 1 }, vlen_struct);
			vlen_struct[0].p = tempMem;
			soundSpeedProfileGrp.getTemperature().put(new long[] { origin[0] }, new long[] { 1 }, vlen_struct);
			vlen_struct[0].p = salinityMem;
			soundSpeedProfileGrp.getSalinity().put(new long[] { origin[0] }, new long[] { 1 }, vlen_struct);

			origin[0] += 1;
		}

	}

}
