package fr.ifremer.globe.api.xsf.converter.common.xsf.types;

public enum DetectionTypeHelper {
	INVALID((byte)0),
	AMPLITUDE((byte)1),
	PHASE((byte)2),
	UNKNOWN((byte) -127);
	
	byte value;
	DetectionTypeHelper(byte v){
		value=v;
	};
	
	public byte get() {return value;}
	
}
