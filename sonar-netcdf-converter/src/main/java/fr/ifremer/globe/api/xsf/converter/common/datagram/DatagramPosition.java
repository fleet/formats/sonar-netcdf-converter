package fr.ifremer.globe.api.xsf.converter.common.datagram;

import java.io.RandomAccessFile;

/**
 * Repretents the position of a datagram within a file.
 *
 */
public class DatagramPosition {

    protected long seek;
    protected RandomAccessFile file;
    
    public DatagramPosition(RandomAccessFile file, long seek) {
        this.file = file;
        this.seek = seek;
    }
    
    public RandomAccessFile getFile() {
        return this.file;
    }
    
    public long getSeek() {
        return this.seek;
    }
}
