package fr.ifremer.globe.api.xsf.converter.s7k.xsf;

import java.util.Calendar;
import java.util.TimeZone;


public class S7kConstants {
	 static class LimitDateForIfremerQualityFactor
	    {

			public static long startingDateUseQF()
	        {
	        	Calendar calendar = Calendar.getInstance();

	    		calendar.set(Calendar.YEAR, 2011);
	    		calendar.set(Calendar.MONTH, 03);
	    		calendar.set(Calendar.DAY_OF_MONTH, 19);
	    		calendar.setTimeZone(TimeZone.getTimeZone("GMT"));

	    		return calendar.getTime().getTime();
	    	
	        }
	    }
	//utility class, hide constructor
	private S7kConstants(){}

	
	// starting date  of the use of quality factors:19/03/2011
	public static  long startingDateUseQF = LimitDateForIfremerQualityFactor.startingDateUseQF();
	
	
	/**
	 * SeaBat 7125
	 */
	public static final int SEABAT_7111 = 7111;
	/**
	 * SeaBat 7125
	 */
	public static final int SEABAT_7125 = 7125;
	/**
	 * SeaBat 7125
	 */
	public static final int SEABAT_7150 = 7150;
}
