package fr.ifremer.globe.api.xsf.converter.all.datagram.echosounderdepth;

import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.DatagramMetadata;

/**
 * */
public class EchoSounderDepthMetadata extends DatagramMetadata {

	public void addDatagram() {
		datagramCount++;
	}

	@Override
	public String toString() {

		return super.toString() + System.lineSeparator();
	}

}
