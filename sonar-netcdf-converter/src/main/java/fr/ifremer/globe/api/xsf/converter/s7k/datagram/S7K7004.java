package fr.ifremer.globe.api.xsf.converter.s7k.datagram;

import static java.lang.Math.toIntExact;

import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7K7004Metadata;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KFile;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KIndividualSensorMetadata;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.api.xsf.util.types.UInt;
import fr.ifremer.globe.api.xsf.util.types.ULong;

public class S7K7004 extends S7KBaseDatagram {

	@Override
	public void computeSpecificMetadata(S7KFile metadata, DatagramBuffer buffer, long type,
			DatagramPosition position, long time) {
		S7K7004Metadata md = getSpecificMetadata(metadata);
		S7KIndividualSensorMetadata sensorMd = md.getEntry(getDeviceIdentifier(buffer).getU());
		if (sensorMd == null) {
			sensorMd = new S7KIndividualSensorMetadata();
			md.addSensor(getDeviceIdentifier(buffer).getU(), sensorMd);
		}
		sensorMd.addDatagram(position, time, toIntExact(getNumberOfReceivedBeams(buffer).getU()));
	}

	@Override
	public S7K7004Metadata getSpecificMetadata(S7KFile metadata) {
		return metadata.get7004Metadata();
	}

	public static ULong getSonarSerialNumber(BaseDatagramBuffer buffer) {
		return TypeDecoder.read8U(buffer.byteBufferData, 0);
	}

	public static UInt getNumberOfReceivedBeams(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4U(buffer.byteBufferData, 8);
	}
	
	public static FFloat getBeamVerticalDirection(BaseDatagramBuffer buffer, int i) {
		return TypeDecoder.read4F(buffer.byteBufferData, 12 + i * 4);
	}
	
	public static FFloat getBeamHorizontalDirection(BaseDatagramBuffer buffer, int i) {
		final int numOfBeams = toIntExact(getNumberOfReceivedBeams(buffer).getU());
		return TypeDecoder.read4F(buffer.byteBufferData, 12 + numOfBeams * 4 + i * 4);
	}
	
	public static FFloat getMinus3dBBeamWidthY(BaseDatagramBuffer buffer, int i) {
		final int numOfBeams = toIntExact(getNumberOfReceivedBeams(buffer).getU());
		return TypeDecoder.read4F(buffer.byteBufferData, 12 + numOfBeams * 8 + i * 4);
	}
	
	public static FFloat getMinus3dBBeamWidthX(BaseDatagramBuffer buffer, int i) {
		final int numOfBeams = toIntExact(getNumberOfReceivedBeams(buffer).getU());
		return TypeDecoder.read4F(buffer.byteBufferData, 12 + numOfBeams * 12 + i * 4);
	}
	
	public static class ReceivedBeamWidthAndSteering {
		public float beamVerticalDirectionAngle;
		public float beamHorizontalDirectionAngle;
		public float minus3dBBeamWidthY;
		public float minux3dBBeamWidthX;
	}

	public static ReceivedBeamWidthAndSteering[] getReceivedBeamWidthAndSteering(DatagramBuffer buffer) {
		final int numOfBeams = toIntExact(getNumberOfReceivedBeams(buffer).getU());
		final ReceivedBeamWidthAndSteering[] ret = new ReceivedBeamWidthAndSteering[numOfBeams];

		for (int i = 0; i < numOfBeams; i++) {
			final ReceivedBeamWidthAndSteering b = new ReceivedBeamWidthAndSteering();
			b.beamVerticalDirectionAngle = getBeamVerticalDirection(buffer, i).get();
			b.beamHorizontalDirectionAngle = getBeamHorizontalDirection(buffer, i).get();
			b.minus3dBBeamWidthY = getMinus3dBBeamWidthY(buffer, i).get();
			b.minux3dBBeamWidthX = getMinus3dBBeamWidthX(buffer, i).get();
			ret[i] = b;
		}

		return ret;
	}
}
