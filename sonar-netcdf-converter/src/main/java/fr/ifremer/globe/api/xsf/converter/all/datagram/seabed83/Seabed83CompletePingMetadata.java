package fr.ifremer.globe.api.xsf.converter.all.datagram.seabed83;

import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.AbstractCompletePingMetadata;

public class Seabed83CompletePingMetadata extends AbstractCompletePingMetadata<Seabed83CompleteAntennaPingMetadata> {
	public Seabed83CompletePingMetadata(int[] serialNumbers) {
		super(0, serialNumbers);
	}
}
