package fr.ifremer.globe.api.xsf.converter.s7k.datagram;

import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7K7027Metadata;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KFile;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KMultipingMetadata;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.DDouble;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.api.xsf.util.types.UByte;
import fr.ifremer.globe.api.xsf.util.types.UInt;
import fr.ifremer.globe.api.xsf.util.types.ULong;
import fr.ifremer.globe.api.xsf.util.types.UShort;

public class S7K7027 extends S7KMultipingDatagram {

	@Override
	public SwathIdentifier genSwathIdentifier(S7KFile stats, DatagramBuffer buffer, long time) {
		return stats.getSwathId(getPingNumber(buffer).getU(), getMultiPingSequence(buffer).getU(), time);
	}

	@Override
	public void computeSpecificMetadata(S7KFile metadata, DatagramBuffer buffer, long type,
			DatagramPosition position, long time, SwathIdentifier swathId) {
		S7KMultipingMetadata deviceMd = getSpecificMetadata(metadata)
				.getOrCreateMetadataForDevice(getDeviceIdentifier(buffer).getU());
		deviceMd.registerSwath(position, getPingNumber(buffer).getU(), getMultiPingSequence(buffer).getU(),
				Math.toIntExact(getNumberOfDetectionPoints(buffer).getU()), getOptionalDataOffset(buffer).getU() != 0);
		metadata.getStats().getDate().register(getTimeMilli(buffer));
	}

	@Override
	public S7K7027Metadata getSpecificMetadata(S7KFile metadata) {
		return metadata.get7027Metadata();
	}

	public static ULong getSonarId(BaseDatagramBuffer buffer) {
		return TypeDecoder.read8U(buffer.byteBufferData, 0);
	}

	public static UInt getPingNumber(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4U(buffer.byteBufferData, 8);
	}

	public static UShort getMultiPingSequence(BaseDatagramBuffer buffer) {
		return TypeDecoder.read2U(buffer.byteBufferData, 12);
	}

	public static UInt getNumberOfDetectionPoints(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4U(buffer.byteBufferData, 14);
	}

	public static UInt getDataFieldSize(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4U(buffer.byteBufferData, 18);
	}

	public static UByte getDetectionAlgorithm(BaseDatagramBuffer buffer) {
		return TypeDecoder.read1U(buffer.byteBufferData, 22);
	}

	public static UInt get7027Flags(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4U(buffer.byteBufferData, 23);
	}

	public static FFloat getSamplingRate(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 27);
	}

	public static FFloat getTxAngle(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 31);
	}

	// per-detection data

	public static UShort getBeamDescriptor(BaseDatagramBuffer buffer, int i) {
		return TypeDecoder.read2U(buffer.byteBufferData, Math.toIntExact(99 + i * getDataFieldSize(buffer).getU()));
	}

	// Detection point f32 Non corrected fractional sample number
	public static FFloat getDetectionPoint(BaseDatagramBuffer buffer, int i) {
		return TypeDecoder.read4F(buffer.byteBufferData, Math.toIntExact(101 + i * getDataFieldSize(buffer).getU()));
	}

	public static FFloat getRxAngle(BaseDatagramBuffer buffer, int i) {
		return TypeDecoder.read4F(buffer.byteBufferData, Math.toIntExact(105 + i * getDataFieldSize(buffer).getU()));
	}

	public static UInt getDetectionFlags(BaseDatagramBuffer buffer, int i) {
		return TypeDecoder.read4U(buffer.byteBufferData, Math.toIntExact(109 + i * getDataFieldSize(buffer).getU()));
	}

	public static UInt getQuality(BaseDatagramBuffer buffer, int i) {
		return TypeDecoder.read4U(buffer.byteBufferData, Math.toIntExact(113 + i * getDataFieldSize(buffer).getU()));
	}

	public static FFloat getUncertainty(BaseDatagramBuffer buffer, int i) {
		return TypeDecoder.read4F(buffer.byteBufferData, Math.toIntExact(117 + i * getDataFieldSize(buffer).getU()));
	}

	// optional data

	public static FFloat getFrequency(BaseDatagramBuffer buffer) {
		assertHasOD(buffer);
		return TypeDecoder.read4F(buffer.byteBufferData, odo(buffer));
	}

	public static DDouble getLatitude(BaseDatagramBuffer buffer) {
		assertHasOD(buffer);
		return TypeDecoder.read8F(buffer.byteBufferData, odo(buffer) + 4);
	}

	public static DDouble getLongitude(BaseDatagramBuffer buffer) {
		assertHasOD(buffer);
		return TypeDecoder.read8F(buffer.byteBufferData, odo(buffer) + 12);
	}

	public static FFloat getHeading(BaseDatagramBuffer buffer) {
		assertHasOD(buffer);
		return TypeDecoder.read4F(buffer.byteBufferData, odo(buffer) + 20);
	}

	public static UByte getHeightSource(BaseDatagramBuffer buffer) {
		assertHasOD(buffer);
		return TypeDecoder.read1U(buffer.byteBufferData, odo(buffer) + 24);
	}

	public static FFloat getTide(BaseDatagramBuffer buffer) {
		assertHasOD(buffer);
		return TypeDecoder.read4F(buffer.byteBufferData, odo(buffer) + 25);
	}

	public static FFloat getRoll(BaseDatagramBuffer buffer) {
		assertHasOD(buffer);
		return TypeDecoder.read4F(buffer.byteBufferData, odo(buffer) + 29);
	}

	public static FFloat getPitch(BaseDatagramBuffer buffer) {
		assertHasOD(buffer);
		return TypeDecoder.read4F(buffer.byteBufferData, odo(buffer) + 33);
	}

	public static FFloat getHeave(BaseDatagramBuffer buffer) {
		assertHasOD(buffer);
		return TypeDecoder.read4F(buffer.byteBufferData, odo(buffer) + 37);
	}

	public static FFloat getVehiculeDepth(BaseDatagramBuffer buffer) {
		assertHasOD(buffer);
		return TypeDecoder.read4F(buffer.byteBufferData, odo(buffer) + 41);
	}

	// optional per detection data

	public static FFloat getDetectionDepth(BaseDatagramBuffer buffer, int i) {
		assertHasOD(buffer);
		return TypeDecoder.read4F(buffer.byteBufferData, odo(buffer) + 45 + i * 20);
	}

	public static FFloat getDetectionAlongTrackDistance(BaseDatagramBuffer buffer, int i) {
		assertHasOD(buffer);
		return TypeDecoder.read4F(buffer.byteBufferData, odo(buffer) + 49 + i * 20);
	}

	public static FFloat getDetectionAcrossTrackDistance(BaseDatagramBuffer buffer, int i) {
		assertHasOD(buffer);
		return TypeDecoder.read4F(buffer.byteBufferData, odo(buffer) + 53 + i * 20);
	}

	public static FFloat getBeamPointingAngle(BaseDatagramBuffer buffer, int i) {
		assertHasOD(buffer);
		return TypeDecoder.read4F(buffer.byteBufferData, odo(buffer) + 57 + i * 20);
	}

	public static FFloat getBeamAzimuthAngle(BaseDatagramBuffer buffer, int i) {
		assertHasOD(buffer);
		return TypeDecoder.read4F(buffer.byteBufferData, odo(buffer) + 61 + i * 20);
	}
}
