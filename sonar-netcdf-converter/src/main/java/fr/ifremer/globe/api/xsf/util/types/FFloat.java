package fr.ifremer.globe.api.xsf.util.types;

public class FFloat implements Comparable<FFloat> {

    private float v;
    
    public FFloat(float v) {
        this.v = v;
    }
    
    public float get() {
        return v;
    }
    
    @Override
    public int compareTo(FFloat o) {
    	return Float.compare(v, o.v);
    }
}
