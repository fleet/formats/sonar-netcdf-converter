package fr.ifremer.globe.api.xsf.converter.all.datagram.clock;

import java.nio.ByteBuffer;

import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagram;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.UByte;
import fr.ifremer.globe.api.xsf.util.types.UInt;
import fr.ifremer.globe.api.xsf.util.types.UShort;

/**
 * Stateless Clock datagram
 */
public class Clock extends BaseDatagram {

	@Override
	public ClockMetadata getSpecificMetadata(AllFile metadata) {
		return metadata.getClock();
	}

	@Override
	protected void computeSpecificMetadata(AllFile metadata, ByteBuffer datagram, byte datagramType,
			long epochTime, DatagramPosition position) {

        final ClockMetadata index = getSpecificMetadata(metadata);

        index.addDatagram(position, getEpochTime(datagram), 1);
        index.setSerialNumber(getSerialNumber(datagram));
    }
	
	public static UShort getCounter(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 12);
	}

	public static UShort getSerialNumber(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 14);
	}

	public static UInt getDateFromExternalClock(ByteBuffer datagram) {
		return TypeDecoder.read4U(datagram, 16);
	}

	public static UInt getTimeFromExternalClock(ByteBuffer datagram) {
		return TypeDecoder.read4U(datagram, 20);
	}

	public static UByte getPPS(ByteBuffer datagram) {
		return TypeDecoder.read1U(datagram, 24);
	}

	public static void print(ByteBuffer datagram) {
		System.out.println(getCounter(datagram));
		System.out.println(getSerialNumber(datagram));

	}

}
