package fr.ifremer.globe.api.xsf.converter.all.datagram.extradetections;

import java.nio.ByteBuffer;

import fr.ifremer.globe.api.xsf.converter.all.datagram.PingDatagram;
import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.api.xsf.util.types.SByte;
import fr.ifremer.globe.api.xsf.util.types.SShort;
import fr.ifremer.globe.api.xsf.util.types.UByte;
import fr.ifremer.globe.api.xsf.util.types.UShort;

public class ExtraDetections extends PingDatagram {
	/** {@inheritDoc} */
	@Override
	public ExtraDetectionsMetadata getSpecificMetadata(AllFile metadata) {
		return metadata.getExtraDetections();
	}

	@Override
	protected void computeSpecificMetadata(AllFile metadata, ByteBuffer datagram, long epochTime, SwathIdentifier pingNumber,
			int serialNumber, DatagramPosition position) {
	    ExtraDetectionsMetadata edMeta = getSpecificMetadata(metadata);
	    
	    ExtraDetectionsCompletePingMetadata completing = edMeta.getPing(pingNumber);
	    if (completing == null) {
	        completing = new ExtraDetectionsCompletePingMetadata();
	        edMeta.addPing(pingNumber, completing);
	    }
	    
	    int nSamples = 0;
	    int detectionClasses = getDetectionClassesCounter(datagram).getU();
	    int detections = getExtraDetectionsCounter(datagram).getU();
	    
	    int offset = getExtraDetectionsOffset(datagram);
	    for (int i = 0; i < detections; i++) {
            // N samples = 2 * N Raw amplitude Samples +   1
	        nSamples += 2*(getRawAmplitudeSamplesCounter(datagram, offset).getU()) + 1;
	        offset = getNextExtraDetOffset(datagram, offset);
	    }
	    
	    completing.addAntenna(getSerialNumber(datagram).getU(), position, detectionClasses, detections, nSamples);
	}

	public static UShort getCounter(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 12);
	}
	public static UShort getSerialNumber(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 14);
	}
	public static UShort getDatagramCounter(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 16);
	}
	public static UShort getDatagramVersionID(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 18);
	}
	
	public static UShort getSwathCounter(ByteBuffer datagram) {
			return TypeDecoder.read2U(datagram, 20);
	}
	public static UShort getSwathIndex(ByteBuffer datagram) {
			return TypeDecoder.read2U(datagram, 22);
	}
	public static UShort getHeading(ByteBuffer datagram) {
			return TypeDecoder.read2U(datagram, 24);
	}
	public static UShort getSoundVelocityAtTx(ByteBuffer datagram) {
			return TypeDecoder.read2U(datagram, 26);
	}
	public static FFloat getDepthOfReferencePoint(ByteBuffer datagram) {
			return TypeDecoder.read4F(datagram, 28);
	}
	public static FFloat getWaterColumnSampleRate(ByteBuffer datagram) {
			return TypeDecoder.read4F(datagram, 32);
	}
	public static FFloat getRawAmplitudeSampleRate(ByteBuffer datagram) {
			return TypeDecoder.read4F(datagram, 36);
	}
	public static UShort getRXTransducerIndex(ByteBuffer datagram) {
			return TypeDecoder.read2U(datagram, 40);
	}
	// Variable Nd
	public static UShort getExtraDetectionsCounter(ByteBuffer datagram) {
			return TypeDecoder.read2U(datagram, 42);
	}
	// Variable Nc
	public static UShort getDetectionClassesCounter(ByteBuffer datagram) {
			return TypeDecoder.read2U(datagram, 44);
	}
	public static UShort getBytePerClassCounter(ByteBuffer datagram) {
			return TypeDecoder.read2U(datagram, 46);
	}
	public static UShort getAlarmFlagsCounter(ByteBuffer datagram) {
			return TypeDecoder.read2U(datagram, 48);
	}
	public static UShort getBytesPerDetectionCounter(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 50);
	}
	
	/**
	 * START Detection Classes Block - Nc
	 */
	public static UShort getStartDepth(ByteBuffer datagram, int ncIndex) {
			return TypeDecoder.read2U(datagram, 52 + ncIndex * 16);
	}
	public static UShort getStopDepth(ByteBuffer datagram, int ncIndex) {
			return TypeDecoder.read2U(datagram, 54 + ncIndex * 16);
	}
	public static UShort getQFThreshold(ByteBuffer datagram, int ncIndex) {
			return TypeDecoder.read2U(datagram, 56 + ncIndex * 16);
	}
	public static SShort getBSThreshold(ByteBuffer datagram, int ncIndex) {
			return TypeDecoder.read2S(datagram, 58 + ncIndex * 16);
	}
	public static UShort getSNRThreshold(ByteBuffer datagram, int ncIndex) {
			return TypeDecoder.read2U(datagram, 60 + ncIndex * 16);
	}
	public static UShort getAlarmThreshold(ByteBuffer datagram, int ncIndex) {
			return TypeDecoder.read2U(datagram, 62 + ncIndex * 16);
	}
	public static UShort getExtraDetectionsIndex(ByteBuffer datagram, int ncIndex) {
			return TypeDecoder.read2U(datagram, 64 + ncIndex * 16);
	}
	public static UByte getShowClass(ByteBuffer datagram, int ncIndex) {
			return TypeDecoder.read1U(datagram, 66 + ncIndex * 16);
	}
	public static UByte getAlarmFlag1(ByteBuffer datagram, int ncIndex) {
			return TypeDecoder.read1U(datagram, 67 + ncIndex * 16);
	}
		

	/* Compute Offset */
	
//	public static int getStartingDetClassesOffset(ByteBuffer datagram) {
//	    int nDC = getDetectionClassesCounter(datagram).getU();
//	    return 52 + 16 * nDC;
//	}
	
	public static int getExtraDetectionsOffset(ByteBuffer datagram) {
        int nDC = getDetectionClassesCounter(datagram).getU();
        return 52 + 16 * nDC;
	}
	
    public static int getNextExtraDetOffset(ByteBuffer datagram, int currentExtraDetOffset) {
        return currentExtraDetOffset + 68;
    }
	
	
	/**
	 * START Extra Detection Classes - Nd Block
	 */
	public static FFloat getDepth(ByteBuffer datagram, int startingOffset) {
		return TypeDecoder.read4F(datagram, startingOffset);
	}
	public static FFloat getAcross(ByteBuffer datagram, int startingOffset) {
			return TypeDecoder.read4F(datagram, startingOffset + 4);
	}
	public static FFloat getAlong(ByteBuffer datagram, int startingOffset) {
			return TypeDecoder.read4F(datagram, startingOffset + 8);
	}
	public static FFloat getDeltaLatitude(ByteBuffer datagram, int startingOffset) {
			return TypeDecoder.read4F(datagram, startingOffset + 12);
	}
	public static FFloat getDeltaLongitude(ByteBuffer datagram, int startingOffset) {
			return TypeDecoder.read4F(datagram, startingOffset + 16);
	}
	public static FFloat getBeamPointingAngle(ByteBuffer datagram, int startingOffset) {
			return TypeDecoder.read4F(datagram, startingOffset + 20);
	}
	public static FFloat getAppliedPointingAngleCorrection(ByteBuffer datagram, int  startingOffset) {
			return TypeDecoder.read4F(datagram, startingOffset + 24);
	}
	public static FFloat getTwoTimeTravelTime(ByteBuffer datagram, int startingOffset) {
			return TypeDecoder.read4F(datagram, startingOffset + 28);
	}
	public static FFloat getAppliedTwoWayTravelTimeCorrections(ByteBuffer datagram, int startingOffset) {
			return TypeDecoder.read4F(datagram, startingOffset + 32);
	}
	public static SShort getBS(ByteBuffer datagram, int startingOffset) {
			return TypeDecoder.read2S(datagram, startingOffset + 36);
	}
	public static SByte getBeamIncidenceAngleAdjustment(ByteBuffer datagram, int startingOffset) {
			return TypeDecoder.read1S(datagram, startingOffset + 38);
	}
	public static UByte getDetectionInfo(ByteBuffer datagram, int startingOffset) {
			return TypeDecoder.read1U(datagram, startingOffset + 39);
	}
//	public static UShort getSpare2(ByteBuffer datagram, int startingOffset) {
//			return TypeDecoder.read2U(datagram, startingOffset + 40);
//	}
	public static UShort getTXSectorIndex(ByteBuffer datagram, int startingOffset) {
			return TypeDecoder.read2U(datagram, startingOffset + 42);
	}
	public static UShort getDetectionWindowLength(ByteBuffer datagram, int startingOffset) {
			return TypeDecoder.read2U(datagram, startingOffset + 44);
	}
	public static UShort getQualityFactor(ByteBuffer datagram, int startingOffset) {
			return TypeDecoder.read2U(datagram, startingOffset + 46);
	}
	public static UShort getRealTimeCleaningInfo(ByteBuffer datagram, int startingOffset) {
			return TypeDecoder.read2U(datagram, startingOffset + 48);
	}
	public static UShort getRangeFactor(ByteBuffer datagram, int startingOffset) {
			return TypeDecoder.read2U(datagram, startingOffset + 50);
	}
	public static UShort getDetectionClassIndex(ByteBuffer datagram, int startingOffset) {
			return TypeDecoder.read2U(datagram, startingOffset + 52);
	}
	public static UShort getConfidenceLevel(ByteBuffer datagram, int startingOffset) {
			return TypeDecoder.read2U(datagram, startingOffset + 54);
	}
	public static UShort getQFIfr(ByteBuffer datagram, int startingOffset) {
			return TypeDecoder.read2U(datagram, startingOffset + 56);
	}
	public static UShort getWaterColumnBeamIndex(ByteBuffer datagram, int startingOffset) {
			return TypeDecoder.read2U(datagram, startingOffset + 58);
	}
	public static FFloat getBeamAngleAcrossReVertical(ByteBuffer datagram, int startingOffset) {
			return TypeDecoder.read4F(datagram, startingOffset + 60);
	}
	public static UShort getDetectedRange(ByteBuffer datagram, int startingOffset) {
			return TypeDecoder.read2U(datagram, startingOffset + 64);
	}
	public static UShort getRawAmplitudeSamplesCounter(ByteBuffer datagram, int startingOffset) {
			return TypeDecoder.read2U(datagram, startingOffset + 66);
	}
	/**
	 * Ns Entries
	 */
	public static SShort getRawAmplitudeSample(ByteBuffer datagram, int startingOffset) {
		return TypeDecoder.read2S(datagram, startingOffset);		
	}
    public static short[] getAllSamples(ByteBuffer datagram) {
    	// N samples = Nd * (2*Ns+1);
        int nParams = (getDetectionClassesCounter(datagram).getU() + 1) * getExtraDetectionsCounter(datagram).getU();
        
        short[] ret = new short[nParams];
    	int offset = 52 + 16 * getDetectionClassesCounter(datagram).getU() + 68 * getExtraDetectionsCounter(datagram).getU();
        for (int i = 0; i < nParams; i++) {
        	ret[i] = TypeDecoder.read2S(datagram, offset).get();
        	offset += 2;
        	
        }
        return ret;
    }

}
