package fr.ifremer.globe.api.xsf.converter.all.mbg;

import java.util.ArrayList;
import java.util.List;

import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.all.mbg.bathy.Depth68DatagramConverter;
import fr.ifremer.globe.api.xsf.converter.all.mbg.bathy.DepthXYZ88DatagramConverter;
import fr.ifremer.globe.api.xsf.converter.all.mbg.bathy.QualityFactorDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.all.mbg.bathy.RawRange102DatagramConverter;
import fr.ifremer.globe.api.xsf.converter.all.mbg.bathy.RawRange70DatagramConverter;
import fr.ifremer.globe.api.xsf.converter.all.mbg.bathy.RawRange78DatagramConverter;
import fr.ifremer.globe.api.xsf.converter.all.mbg.runtime.RuntimeDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.all.mbg.sensors.SoundSpeedProfileDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgBase;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgWriter;

public class AllMbgWriter extends MbgWriter<AllFile> {

	@Override
	public List<IDatagramConverter<AllFile, MbgBase>> getGroupConverters(AllFile allFile) {

		List<IDatagramConverter<AllFile, MbgBase>> dataConverters = new ArrayList<>();

		if (allFile.getDepth68().datagramCount > 0)
			dataConverters.add(new Depth68DatagramConverter(allFile));

		if (allFile.getXyzdepth().datagramCount > 0)
			dataConverters.add(new DepthXYZ88DatagramConverter(allFile));

		if (allFile.getRunTime().datagramCount > 0)
			dataConverters.add(new RuntimeDatagramConverter());

		if (allFile.getRawRange78().datagramCount > 0)
			dataConverters.add(new RawRange78DatagramConverter());

		if (allFile.getRawRange70().datagramCount > 0)
			dataConverters.add(new RawRange70DatagramConverter());

		if (allFile.getRawRange102().datagramCount > 0)
			dataConverters.add(new RawRange102DatagramConverter());

		if (allFile.getQualityFactor().datagramCount > 0)
			dataConverters.add(new QualityFactorDatagramConverter());

		if (allFile.getSoundSpeedProfile().datagramCount > 0)
			dataConverters.add(new SoundSpeedProfileDatagramConverter());

		return dataConverters;
	}

}
