package fr.ifremer.globe.api.xsf.converter.s7k.mbg;

import static java.lang.Math.toIntExact;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgBase;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgConstants;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgConstants.SoundingValidity;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgUtils;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2S1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2S1WithHScale;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2S2;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2S2WithHScale;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2S4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2U4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2WithHScale;
import fr.ifremer.globe.api.xsf.converter.common.xsf.types.DetectionTypeHelper;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7K7030InstallationParameters;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KFile;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KMultipingMetadata;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KMultipingSequenceMetadata;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KSwathMetadata;
import fr.ifremer.globe.api.xsf.converter.s7k.xsf.S7kConstants;
import fr.ifremer.globe.api.xsf.util.DefaultLogger;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.types.SByte;
import fr.ifremer.globe.api.xsf.util.types.SInt;
import fr.ifremer.globe.api.xsf.util.types.SShort;
import fr.ifremer.globe.api.xsf.util.types.UInt;
import fr.ifremer.globe.netcdf.api.convention.CFStandardNames;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCVariable;

/**
 * Abstract class to convert depth {@link BaseDatagramBuffer}.
 */
public abstract class AbstractDepthDatagramConverter implements IDatagramConverter<S7KFile, MbgBase> {

	protected final List<ValueD2> swathBeamValues = new LinkedList<>();;
	protected final List<ValueD2> swathAntennaValues = new LinkedList<>();
	protected final List<ValueD2WithHScale> swathBeamValuesWithHScale = new LinkedList<>();
	protected final List<ValueD2WithHScale> swathAntennaValuesWithHScale = new LinkedList<>();

	@Override
	public void declare(S7KFile s7kFile, MbgBase mbg) throws NCException {

		/** Cycle **/
		NCVariable cycleCountVariable = mbg.getVariable(MbgConstants.Cycle);
		swathAntennaValues
				.add(new ValueD2U4(cycleCountVariable, (buffer, beamIndex) -> new UInt((int) (getPingNumber(buffer)))));

		/** Date **/
		NCVariable dateVariable = mbg.getVariable(MbgConstants.Date);
		swathAntennaValues.add(new ValueD2S4(dateVariable,
				(buffer, beamIndex) -> new SInt((int) (getTime(buffer) / (24 * 3600 * 1000)))));

		/** Time **/
		NCVariable timeVariable = mbg.getVariable(MbgConstants.Time);
		swathAntennaValues.add(new ValueD2S4(timeVariable,
				(buffer, beamIndex) -> new SInt((int) (getTime(buffer) % (24 * 3600 * 1000)))));

		/** Abscissa (== longitude) **/
		NCVariable lonVariable = mbg.getVariable(MbgConstants.Abscissa);
		double lonScaleFactor = lonVariable.getAttributeDouble(CFStandardNames.CF_SCALE_FACTOR);
		swathAntennaValues.add(new ValueD2S4(lonVariable, (buffer, antennaIndex) -> {
			double lon = Math.toDegrees(getLongitude(buffer));
			return new SInt((int) Math.round(lon * (1 / lonScaleFactor)));
		}));

		/** Ordinate (= latitude) **/
		NCVariable latVariable = mbg.getVariable(MbgConstants.Ordinate);
		double latScaleFactor = latVariable.getAttributeDouble(CFStandardNames.CF_SCALE_FACTOR);
		swathAntennaValues.add(new ValueD2S4(latVariable, (buffer, antennaIndex) -> {
			double lat = Math.toDegrees(getLatitude(buffer));
			return new SInt((int) Math.round(lat * (1 / latScaleFactor)));
		}));

		/** Heading **/
		NCVariable headingVariable = mbg.getVariable(MbgConstants.Heading);
		double headingScaleFactor = headingVariable.getAttributeDouble(CFStandardNames.CF_SCALE_FACTOR);
		swathAntennaValues.add(new ValueD2S2(headingVariable, (buffer, iBeam) -> new SShort(
				(short) Math.round((float) Math.toDegrees(getHeading(buffer)) * (1 / headingScaleFactor)))));

		/** Distance scale **/
		NCVariable distanceScaleVariable = mbg.getVariable(MbgConstants.DistanceScale);
		double scaleFactor = distanceScaleVariable.getAttributeDouble(CFStandardNames.CF_SCALE_FACTOR);
		swathAntennaValuesWithHScale.add(new ValueD2S1WithHScale(distanceScaleVariable,
				(buffer, iBeam, hScale) -> new SByte((byte) (hScale / scaleFactor))));

		/** Across distance **/
		NCVariable acrossDistanceVariable = mbg.getVariable(MbgConstants.AcrossDistance);
		swathBeamValuesWithHScale.add(new ValueD2S2WithHScale(acrossDistanceVariable, (buffer, iBeam,
				hScale) -> new SShort((short) (getDetectionAcrossTrackDistance(buffer, iBeam) / hScale))));

		/** Along distance **/
		NCVariable alongDistanceVariable = mbg.getVariable(MbgConstants.AlongDistance);
		swathBeamValuesWithHScale.add(new ValueD2S2WithHScale(alongDistanceVariable, (buffer, iBeam,
				hScale) -> new SShort((short) (getDetectionAlongTrackDistance(buffer, iBeam) / hScale))));

		/** Across angle (= pointing angle) **/
		NCVariable acrossBeamAngleVariable = mbg.getVariable(MbgConstants.AcrossAngle);
		double acrossBeamAngleFactor = acrossBeamAngleVariable.getAttributeDouble(CFStandardNames.CF_SCALE_FACTOR);
		swathBeamValues.add(new ValueD2S2(acrossBeamAngleVariable, (buffer, iBeam) -> {
			float angle = (float) Math.toDegrees(getBeamPointingAngle(buffer, iBeam));
			// Reverse the pointing angle if > azimut angle
			if (Math.toDegrees(getBeamAzimuthAngle(buffer, iBeam)) > 180)
				angle *= -1;
			return new SShort((short) Math.round(angle * (1 / acrossBeamAngleFactor)));
		}));

		/** Along angle (= azimuth beam angle) **/
		NCVariable azimuthBeamAngleVariable = mbg.getVariable(MbgConstants.AlongAngle);
		double azimuthBeamAngleFactor = azimuthBeamAngleVariable.getAttributeDouble(CFStandardNames.CF_SCALE_FACTOR);
		swathBeamValues.add(new ValueD2S2(azimuthBeamAngleVariable, (buffer, iBeam) -> {
			float angle = (float) Math.toDegrees(getBeamAzimuthAngle(buffer, iBeam));
			return new SShort((short) Math.round(angle / azimuthBeamAngleFactor));
		}));

		/** Depth **/
		NCVariable depthVariable = mbg.getVariable(MbgConstants.Depth);
		double depthFactor = depthVariable.getAttributeDouble(CFStandardNames.CF_SCALE_FACTOR);
		swathBeamValues.add(new ValueD2S4(depthVariable,
				(buffer, iBeam) -> new SInt((int) Math.round(getDetectionDepth(buffer, iBeam) / depthFactor))));

		/** Vertical depth **/
		NCVariable verticalDepthVariable = mbg.getVariable(MbgConstants.VerticalDepth);
		double vertDepthScaleFactor = verticalDepthVariable.getAttributeDouble(CFStandardNames.CF_SCALE_FACTOR);
		swathAntennaValues.add(new ValueD2S4(verticalDepthVariable, (buffer, antennaIndex) -> {
			float minAcrossDistance = Float.MAX_VALUE;
			float verticalDepth = 0;
			for (int i = 0; i < toIntExact(getNumberOfDetectionPoints(buffer)); i++) {
				float currentAcrossDistance = Math.abs(getDetectionAcrossTrackDistance(buffer, i));
				if (getDetectionValidity(buffer, i) == SoundingValidity.VALID
						&& currentAcrossDistance < minAcrossDistance) {
					minAcrossDistance = currentAcrossDistance;
					verticalDepth = getDetectionDepth(buffer, i);
				}
			}
			return new SInt((int) Math.round(verticalDepth * (1 / vertDepthScaleFactor)));
		}));

		/** Reference depth (= immersion) **/
		NCVariable refDepthVariable = mbg.getVariable(MbgConstants.Immersion);
		double refDepthScaleFactor = refDepthVariable.getAttributeDouble(CFStandardNames.CF_SCALE_FACTOR);
		swathAntennaValues.add(new ValueD2S4(refDepthVariable, (buffer, antennaIndex) -> {
			S7K7030InstallationParameters lastConfiguration = s7kFile.getInstallationParameters(getTimestamp(buffer));
			// referenceDepth = waterLine - zOffset
			double referenceDepth = lastConfiguration.getWaterLineVerticalOffset()
					- lastConfiguration.getTransmitArrayZ();
			if (getDeviceIdentifier(buffer) == S7kConstants.SEABAT_7125) {
				referenceDepth += getVehiculeDepth(buffer);
			} else {
				referenceDepth -= getHeave(buffer);
			}
			return new SInt((int) Math.round(referenceDepth * (1 / refDepthScaleFactor)));
		}));

		/** Pitch **/
		NCVariable pitchVariable = mbg.getVariable(MbgConstants.Pitch);
		double pitchScaleFactor = pitchVariable.getAttributeDouble(CFStandardNames.CF_SCALE_FACTOR);
		swathAntennaValues.add(new ValueD2S2(pitchVariable, (buffer, antennaIndex) -> {
			double value = Math.toDegrees(getPitch(buffer));
			return new SShort((short) Math.round(value * (1 / pitchScaleFactor)));
		}));

		/** Roll **/
		NCVariable rollVariable = mbg.getVariable(MbgConstants.Roll);
		double rollScaleFactor = rollVariable.getAttributeDouble(CFStandardNames.CF_SCALE_FACTOR);
		swathAntennaValues.add(new ValueD2S2(rollVariable, (buffer, antennaIndex) -> {
			double value = Math.toDegrees(getRoll(buffer));
			return new SShort((short) Math.round(value * (1 / rollScaleFactor)));
		}));

		/** Heave **/
		NCVariable heaveVariable = mbg.getVariable(MbgConstants.Heave);
		double heaveScaleFactor = heaveVariable.getAttributeDouble(CFStandardNames.CF_SCALE_FACTOR);
		swathAntennaValues.add(new ValueD2S2(heaveVariable,
				(buffer, antennaIndex) -> new SShort((short) Math.round(-getHeave(buffer) * (1 / heaveScaleFactor)))));

		/** Reason quality factor **/
		NCVariable qualityVariable = mbg.getVariable(MbgConstants.SQuality);
		swathBeamValues.add(new ValueD2S1(qualityVariable, (buffer, iBeam) -> {
			byte quality = (byte) getQuality(buffer, iBeam);
			if (getDetectionType(buffer, iBeam) == DetectionTypeHelper.PHASE)
				quality += 128;
			return new SByte(quality);
		}));

		/** Validity **/
		NCVariable validityVariable = mbg.getVariable(MbgConstants.Validity);
		swathBeamValues.add(new ValueD2S1(validityVariable, (buffer, iBeam)//
		-> new SByte(getDetectionValidity(buffer, iBeam).getValue())));

		/** Cycle flag (valid if at least one detection is valid) */
		NCVariable cycleValidityVariable = mbg.getVariable(MbgConstants.CFlag);
		swathAntennaValues.add(new ValueD2S1(cycleValidityVariable, (buffer, antennaIndex) -> {
			for (int i = 0; i < toIntExact(getNumberOfDetectionPoints(buffer)); i++) {
				if (getDetectionValidity(buffer, i) == SoundingValidity.VALID)
					return new SByte((byte) 2);
			}
			return new SByte((byte) 0);
		}));

		/** Frequency plane */
		NCVariable frequencyPlaneVariable = mbg.getVariable(MbgConstants.Frequency);
		swathAntennaValues.add(new ValueD2S1(frequencyPlaneVariable,
				(buffer, antennaIndex) -> new SByte((byte) getMultiPingSequence(buffer))));

		/** Sonar frequency **/
		NCVariable frequencyVariable = mbg.getVariable(MbgConstants.SonarFrequency);
		double freqVarScaleFactor = frequencyVariable.getAttributeDouble(CFStandardNames.CF_SCALE_FACTOR);
		swathAntennaValues.add(new ValueD2S4(frequencyVariable,
				(buffer, antennaIndex) -> new SInt((int) (getFrequency(buffer) * (1 / freqVarScaleFactor)))));

		/** Range **/
		// TODO: avoid useless cast in double ect... and use Math.round() (was coded to exactly match with previous
		// converter)
		NCVariable rangeVariable = mbg.getVariable(MbgConstants.Range);
		float rangeFactor = (float) rangeVariable.getAttributeDouble(CFStandardNames.CF_SCALE_FACTOR);
		swathBeamValues.add(new ValueD2S4(rangeVariable,
				(buffer, iBeam) -> new SInt((int) (((double) getRange(buffer, iBeam)) / 2 / rangeFactor))));

	}

	@Override
	public void fillData(S7KFile s7kFile) throws IOException, NCException {
		DatagramBuffer buffer = new DatagramBuffer();
		long origin;

		// iterate over pings (by date)
		for (Entry<Long, S7KMultipingSequenceMetadata> posentry : getMultipingMetada(s7kFile).getEntries()) {
			// iterate over ping sequence
			for (Entry<Integer, S7KSwathMetadata> e : posentry.getValue().getEntries()) {
				DatagramReader.readDatagram(e.getValue().getPosition().getFile(), buffer,
						e.getValue().getPosition().getSeek());
				SwathIdentifier id = new SwathIdentifier(getPingNumber(buffer), getMultiPingSequence(buffer));

				var optIndex = s7kFile.getSwathIndexer().getIndex(id);
				if (optIndex.isEmpty()) {
					DefaultLogger.get().warn(
							"Datagram Depth associated with a missing ping is ignored (number = {}, sequence = {}).",
							id.getRawPingNumber(), id.getSwathPosition());
					continue;
				}
				origin = optIndex.get();

				swathBeamValues.forEach(ValueD2::clear);
				swathBeamValuesWithHScale.forEach(ValueD2WithHScale::clear);
				swathAntennaValues.forEach(ValueD2::clear);
				swathAntennaValuesWithHScale.forEach(ValueD2WithHScale::clear);

				/**
				 * @FIXME : the index in netCDF file (=origin) is computed from the SwathIndexGenerator, but the
				 *        variables are defined with dimensions computed from datagram count. The SwathIndexGenerator
				 *        may have more indexes than the depth datagram count of the s7k file (if there is ping without
				 *        bathy)!!!
				 */

				double horizontalScale = 0;

				// detections (swath x beam)
				if (getOptionalDataOffset(buffer) != 0) {
					final int nEntries = toIntExact(getNumberOfDetectionPoints(buffer));
					horizontalScale = computeHorizontaleScale(buffer, nEntries);
					for (int i = 0; i < nEntries; i++) {
						for (ValueD2 v : this.swathBeamValues)
							v.fill(buffer, i, i);
						for (ValueD2WithHScale v : swathBeamValuesWithHScale)
							v.fill(buffer, i, i, horizontalScale);
						computeStatistics(s7kFile, buffer, i);
					}

					for (ValueD2 v : this.swathBeamValues)
						v.write(new long[] { origin, 0 }, new long[] { 1, nEntries });
					for (ValueD2WithHScale v : swathBeamValuesWithHScale)
						v.write(new long[] { origin, 0 }, new long[] { 1, nEntries });
				}

				// Swath x Antenna values
				for (ValueD2 v : swathAntennaValues) {
					v.fill(buffer, 0, 0);// antennaIdx);
					v.write(new long[] { origin, 0 }, new long[] { 1, 1 });
				}

				// Swath x Antenna values (with hScale)
				for (ValueD2WithHScale v : swathAntennaValuesWithHScale) {
					v.fill(buffer, 0, 0, horizontalScale);
					v.write(new long[] { origin, 0 }, new long[] { 1, 1 });
				}
			}
		}
	}

	protected double computeHorizontaleScale(DatagramBuffer buffer, int beamCount) {
		float max = Float.MIN_VALUE;
		for (int i = 0; i < beamCount; i++) {
			max = Math.max(max, Math.abs(getDetectionAcrossTrackDistance(buffer, i)));
			max = Math.max(max, Math.abs(getDetectionAlongTrackDistance(buffer, i)));
		}
		return MbgUtils.getHorizontalScale(max);
	}

	private void computeStatistics(S7KFile s7kFile, DatagramBuffer buffer, int iBeam) {
		// compute statistics from valid detection
		if (getDetectionValidity(buffer, iBeam) == SoundingValidity.VALID) {
			// depth
			s7kFile.getStats().getDepth().register(getDetectionDepth(buffer, iBeam));
			// start/end date
			s7kFile.getStats().getDate().register(getTime(buffer));
			// detection latitude / longitude
			double lat = Math.toDegrees(getLatitude(buffer));
			double lon = Math.toDegrees(getLongitude(buffer));
			float heading = (float) Math.toDegrees(getHeading(buffer));
			float across = getDetectionAcrossTrackDistance(buffer, iBeam);
			float along = getDetectionAlongTrackDistance(buffer, iBeam);
			double[] latlon = MbgUtils.getDetectionPosition(lat, lon, along, across, heading);
			s7kFile.getStats().getDetectionLat().register(latlon[0]);
			s7kFile.getStats().getDetectionLon().register(latlon[1]);
		}
	}

	/**
	 * Abstract methods to get values from buffers.
	 */

	protected abstract S7KMultipingMetadata getMultipingMetada(S7KFile s7kFile);

	protected abstract int getOptionalDataOffset(BaseDatagramBuffer buffer);

	protected abstract int getDeviceIdentifier(BaseDatagramBuffer buffer);

	protected abstract int getMultiPingSequence(BaseDatagramBuffer buffer);

	protected abstract long getPingNumber(BaseDatagramBuffer buffer);

	// time

	/**
	 * @return date in nanosecond
	 */
	protected abstract long getTimestamp(BaseDatagramBuffer buffer);

	protected abstract long getTime(BaseDatagramBuffer buffer);

	// navigation

	protected abstract double getLatitude(BaseDatagramBuffer buffer);

	protected abstract double getLongitude(BaseDatagramBuffer buffer);

	protected abstract float getHeading(BaseDatagramBuffer buffer);

	// attitude

	protected abstract float getRoll(BaseDatagramBuffer buffer);

	protected abstract float getPitch(BaseDatagramBuffer buffer);

	protected abstract float getHeave(BaseDatagramBuffer buffer);

	protected abstract float getVehiculeDepth(BaseDatagramBuffer buffer);

	// detections

	protected abstract int getNumberOfDetectionPoints(BaseDatagramBuffer buffer);

	protected abstract DetectionTypeHelper getDetectionType(BaseDatagramBuffer buffer, int iBeam);

	protected abstract SoundingValidity getDetectionValidity(BaseDatagramBuffer buffer, int i);

	protected abstract float getDetectionDepth(BaseDatagramBuffer buffer, int iBeam);

	protected abstract float getBeamAzimuthAngle(BaseDatagramBuffer buffer, int iBeam);

	protected abstract float getBeamPointingAngle(BaseDatagramBuffer buffer, int iBeam);

	protected abstract float getDetectionAlongTrackDistance(BaseDatagramBuffer buffer, int iBeam);

	protected abstract float getDetectionAcrossTrackDistance(BaseDatagramBuffer buffer, int iBeam);

	protected abstract int getQuality(BaseDatagramBuffer buffer, int iBeam);

	protected abstract float getFrequency(BaseDatagramBuffer buffer);

	protected abstract float getRange(BaseDatagramBuffer buffer, int iBeam);
}
