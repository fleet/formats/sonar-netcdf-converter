package fr.ifremer.globe.api.xsf.converter.s7k.datagram;


import static java.lang.Math.toIntExact;

import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.DatagramMetadata;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KFile;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.DDouble;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.api.xsf.util.types.UInt;
import fr.ifremer.globe.api.xsf.util.types.ULong;
import fr.ifremer.globe.api.xsf.util.types.UShort;

public class S7K7007 extends S7KMultipingDatagram {

	@Override
	public void computeSpecificMetadata(S7KFile metadata, DatagramBuffer buffer, long type,
			DatagramPosition position, long time, SwathIdentifier swathId) {
		
	}
	
	@Override
	public SwathIdentifier genSwathIdentifier(S7KFile stats, DatagramBuffer buffer, long time) {
		return stats.getSwathId(getPingNumber(buffer).getU(), getMultiPingSequence(buffer).getU(), time);
	}

	@Override
	public DatagramMetadata getSpecificMetadata(S7KFile metadata) {
		// TODO
		return null;
	}
	
	public static ULong getSonarSerialNumber(DatagramBuffer buffer) {
		return TypeDecoder.read8U(buffer.byteBufferData, 0);
	}
	
	public static UInt getPingNumber(DatagramBuffer buffer) {
		return TypeDecoder.read4U(buffer.byteBufferData, 8);
	}
	
	public static UShort getMultiPingSequence(DatagramBuffer buffer) {
		return TypeDecoder.read2U(buffer.byteBufferData, 12);
	}
	
	public static FFloat getBeamPosition(DatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 14);
	}
	
	public static UInt getControlFlags(DatagramBuffer buffer) {
		return TypeDecoder.read4U(buffer.byteBufferData, 18);
	}
	
	public static UInt getNumberOfSamplesPerSide(DatagramBuffer buffer) {
		return TypeDecoder.read4U(buffer.byteBufferData, 22);
	}
	
	public static UShort[] getPortBeams(DatagramBuffer buffer) {
		final int numOfBeams = toIntExact(getNumberOfSamplesPerSide(buffer).getU());
		UShort[] ret = new UShort[numOfBeams];
		for (int i = 0; i < numOfBeams; i++) {
			ret[i] = TypeDecoder.read2U(buffer.byteBufferData, 64 + i * 2);
		}
		return ret;
	}
	
	public static UShort[] getStarboardBeams(DatagramBuffer buffer) {
		final int numOfBeams = toIntExact(getNumberOfSamplesPerSide(buffer).getU());
		UShort[] ret = new UShort[numOfBeams];
		for (int i = 0; i < numOfBeams; i++) {
			ret[i] = TypeDecoder.read2U(buffer.byteBufferData, 64 + numOfBeams * 2 + i * 2);
		}
		return ret;
	}
	
	public static FFloat getFrequency(DatagramBuffer buffer, int optionalDataOffset) {
		return TypeDecoder.read4F(buffer.byteBufferData, optionalDataOffset - 64);
	}
	
	public static DDouble getLatitude(DatagramBuffer buffer, int optionalDataOffset) {
		return TypeDecoder.read8F(buffer.byteBufferData, optionalDataOffset - 60);
	}
	
	public static DDouble getLongitude(DatagramBuffer buffer, int optionalDataOffset) {
		return TypeDecoder.read8F(buffer.byteBufferData, optionalDataOffset - 52);
	}
	
	public static FFloat getHeading(DatagramBuffer buffer, int optionalDataOffset) {
		return TypeDecoder.read4F(buffer.byteBufferData, optionalDataOffset - 44);
	}
	
	public static FFloat getDepth(DatagramBuffer buffer, int optionalDataOffset) {
		return TypeDecoder.read4F(buffer.byteBufferData, optionalDataOffset - 40);
	}
}
