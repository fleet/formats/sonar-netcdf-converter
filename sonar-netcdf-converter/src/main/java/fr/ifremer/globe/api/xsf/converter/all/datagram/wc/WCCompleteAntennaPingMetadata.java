package fr.ifremer.globe.api.xsf.converter.all.datagram.wc;

import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.AbstractCompleteAntennaPingMultiMetadata;

public class WCCompleteAntennaPingMetadata extends AbstractCompleteAntennaPingMultiMetadata {

    public WCCompleteAntennaPingMetadata(int datagramCount) {
        super(datagramCount);
    }
}
