package fr.ifremer.globe.api.xsf.converter.common;

import fr.ifremer.globe.api.xsf.converter.MbgConverterParameters;
import fr.ifremer.globe.api.xsf.converter.SounderFileConverterParameters;
import fr.ifremer.globe.api.xsf.converter.XsfConverterParameters;
import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgBase;
import fr.ifremer.globe.api.xsf.converter.common.sounder.ISounderFile;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfBase;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.KmallFile;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KFile;
import fr.ifremer.globe.netcdf.ucar.NCException;
import org.slf4j.Logger;

import java.io.IOException;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

/**
 * This class converts an {@link ISounderFile} (.all, .kmall, s7k...) to a new  NetcdfFile.
 *
 * @param <I> input sounder file type ( {@link AllFile}, {@link KmallFile}, {@link S7KFile}...)
 * @param <O> output netcdf file type ( {@link MbgBase}, {@link XsfBase}...)
 * @param <P> external parameters ( {@link MbgConverterParameters}, {@link XsfConverterParameters})
 */
public interface INcFileWriter<I extends ISounderFile, O extends AutoCloseable, P extends SounderFileConverterParameters> {

    /**
     * Creates the output netcdf file.
     */
    O createOutputNcFile(I inputSounderFile, P parameters) throws NCException, IOException;

    /**
     * Gets a list of "datagram -> netcdfGroup" converters.
     */
    List<IDatagramConverter<I, O>> getGroupConverters(I sounderFile) throws IOException;

    /**
     * Converts an {@link ISounderFile} (.all, .kmall, s7k...) to a new NetcdfFile.
     */
    default void write(I inputSounderFile, P parameters, Logger logger) throws Exception {
        // create the output netcdf file (MBG or XSF...)
        try (O outputNcFile = createOutputNcFile(inputSounderFile, parameters)) {

            // get datagram converters
            List<IDatagramConverter<I, O>> dataGroups = getGroupConverters(inputSounderFile);

            // declare netcdf dimensions and variables, and how they will be filled...
            for (IDatagramConverter<I, O> grp : dataGroups) {
                logger.info("Declare variable(s) for: {}...", grp.getClass().getSimpleName());
                grp.declare(inputSounderFile, outputNcFile);
            }

            // loop on datagrams to fill variables...
            for (IDatagramConverter<I, O> grp : dataGroups) {
                LocalTime startTime = LocalTime.now();
                grp.fillData(inputSounderFile);
                logger.info("Read datagrams and fill group {} in {} ms", grp.getClass().getSimpleName(),
                        startTime.until(LocalTime.now(), ChronoUnit.MILLIS));
            }

            // specific post process
            logger.info("Post processing...");
            LocalTime startTime = LocalTime.now();
            postProcess(inputSounderFile, outputNcFile, parameters, logger);
            logger.info("Post processing takes {} ms", startTime.until(LocalTime.now(), ChronoUnit.MILLIS));
        }
    }

    /**
     * Applies post treatments.
     */
    void postProcess(I inputSounderFile, O outputNcFile, P parameters, Logger logger) throws NCException;

}
