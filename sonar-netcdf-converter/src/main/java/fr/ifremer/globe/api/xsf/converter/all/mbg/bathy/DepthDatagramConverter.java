package fr.ifremer.globe.api.xsf.converter.all.mbg.bathy;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.all.datagram.PingDatagram;
import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.all.datagram.position.Position;
import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagram;
import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.AbstractBeamDatagramMetadata;
import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.AbstractCompleteAntennaPingSingleMetadata;
import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.AbstractCompletePingMetadata;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgBase;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgConstants;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgConstants.SoundingValidity;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgUtils;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2S1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2S2;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2S2WithAttitude;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2S2WithHScale;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2S4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2S4WithPosition;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2U4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2WithAttitude;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2WithHScale;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2WithPosition;
import fr.ifremer.globe.api.xsf.util.DefaultLogger;
import fr.ifremer.globe.api.xsf.util.ObjectBufferPool;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.types.SByte;
import fr.ifremer.globe.api.xsf.util.types.SInt;
import fr.ifremer.globe.api.xsf.util.types.SShort;
import fr.ifremer.globe.api.xsf.util.types.UInt;
import fr.ifremer.globe.netcdf.api.convention.CFStandardNames;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCVariable;
import fr.ifremer.globe.netcdf.util.Pair;

public abstract class DepthDatagramConverter implements IDatagramConverter<AllFile, MbgBase> {

	protected AbstractBeamDatagramMetadata<AbstractCompletePingMetadata<AbstractCompleteAntennaPingSingleMetadata>> metadata;

	/** Swath * antenna variables **/
	protected List<ValueD2> swathAntennaValues = new LinkedList<>();
	protected List<ValueD2WithPosition> swathAntennaValuesWithPosition = new LinkedList<>();
	protected List<ValueD2WithAttitude> swathAntennaValuesWithAttitude = new LinkedList<>();
	protected List<ValueD2WithHScale> swathAntennaValuesWithHScale = new LinkedList<>();

	/** Swath * beam variables **/
	protected List<ValueD2> swathBeamValues = new LinkedList<>();
	protected List<ValueD2WithPosition> swathBeamValuesWithPosition = new LinkedList<>();
	protected List<ValueD2WithAttitude> swathBeamValuesWithAttitude = new LinkedList<>();
	protected List<ValueD2WithHScale> swathBeamValuesWithHScale = new LinkedList<>();

	/**
	 * Constructor
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public DepthDatagramConverter(AbstractBeamDatagramMetadata metadata) {
		this.metadata = metadata;
	}

	/**
	 * Declares variables.
	 */
	@Override
	public void declare(AllFile allFile, MbgBase mbg) throws NCException {

		/** Across distance **/
		NCVariable acrossDistanceVariable = mbg.getVariable(MbgConstants.AcrossDistance);
		swathBeamValuesWithHScale.add(new ValueD2S2WithHScale(acrossDistanceVariable,
				(buffer, iBeam, hScale) -> new SShort((short) (getAcrossDistance(buffer, iBeam) / hScale))));

		/** Along distance **/
		NCVariable alongDistanceVariable = mbg.getVariable(MbgConstants.AlongDistance);
		swathBeamValuesWithHScale.add(new ValueD2S2WithHScale(alongDistanceVariable,
				(buffer, iBeam, hScale) -> new SShort((short) (getAlongDistance(buffer, iBeam) / hScale))));

		/** Abscissa (= longitude) **/
		NCVariable lonVariable = mbg.getVariable(MbgConstants.Abscissa);
		double lonScaleFactor = lonVariable.getAttributeDouble(CFStandardNames.CF_SCALE_FACTOR);
		double lonOffset = lonVariable.getAttributeDouble(CFStandardNames.CF_ADD_OFFSET);
		swathAntennaValuesWithPosition.add(new ValueD2S4WithPosition(lonVariable, (buffer, beamIndexSource, lat, lon,
				speed, posMetadata) -> (int) Math.round((lon - lonOffset) * (1 / lonScaleFactor))));

		/** Ordinate (= latitude) **/
		NCVariable latVariable = mbg.getVariable(MbgConstants.Ordinate);
		double latScaleFactor = latVariable.getAttributeDouble(CFStandardNames.CF_SCALE_FACTOR);
		double latOffset = latVariable.getAttributeDouble(CFStandardNames.CF_ADD_OFFSET);
		swathAntennaValuesWithPosition.add(new ValueD2S4WithPosition(latVariable, (buffer, beamIndexSource, lat, lon,
				speed, posMetadata) -> (int) Math.round((lat - latOffset) / latScaleFactor)));

		/** Pitch **/
		NCVariable pitchVariable = mbg.getVariable(MbgConstants.Pitch);
		double pitchScaleFactor = pitchVariable.getAttributeDouble(CFStandardNames.CF_SCALE_FACTOR);
		double pitchOffset = pitchVariable.getAttributeDouble(CFStandardNames.CF_ADD_OFFSET);
		swathAntennaValuesWithAttitude.add(new ValueD2S2WithAttitude(pitchVariable, (buffer, beamIndexSource, roll,
				pitch, heave) -> (short) Math.round((pitch - pitchOffset) * (1 / pitchScaleFactor))));

		/** Roll **/
		NCVariable rollVariable = mbg.getVariable(MbgConstants.Roll);
		double rollScaleFactor = rollVariable.getAttributeDouble(CFStandardNames.CF_SCALE_FACTOR);
		double rollOffset = rollVariable.getAttributeDouble(CFStandardNames.CF_ADD_OFFSET);
		swathAntennaValuesWithAttitude.add(new ValueD2S2WithAttitude(rollVariable, (buffer, beamIndexSource, roll,
				pitch, heave) -> (short) Math.round((roll - rollOffset) * (1 / rollScaleFactor))));

		/** Heave **/
		NCVariable heaveVariable = mbg.getVariable(MbgConstants.Heave);
		double heaveScaleFactor = heaveVariable.getAttributeDouble(CFStandardNames.CF_SCALE_FACTOR);
		double heaveOffset = heaveVariable.getAttributeDouble(CFStandardNames.CF_ADD_OFFSET);
		swathAntennaValuesWithAttitude.add(new ValueD2S2WithAttitude(heaveVariable, (buffer, beamIndexSource, roll,
				pitch, heave) -> (short) Math.round((heave - heaveOffset) * (1 / heaveScaleFactor))));

		/** Cycle **/
		NCVariable cycleCountVariable = mbg.getVariable(MbgConstants.Cycle);
		int cycleCountFactor = cycleCountVariable.getAttributeInt(CFStandardNames.CF_SCALE_FACTOR);
		int cycleCountOffset = cycleCountVariable.getAttributeInt(CFStandardNames.CF_ADD_OFFSET);
		swathAntennaValues.add(new ValueD2U4(cycleCountVariable, (buffer, beamIndex) //
		-> new UInt((PingDatagram.getPingNumber(buffer.byteBufferData).getU() - cycleCountOffset) / cycleCountFactor)));

		/** Date **/
		NCVariable dateVariable = mbg.getVariable(MbgConstants.Date);
		int dateFactor = cycleCountVariable.getAttributeInt(CFStandardNames.CF_SCALE_FACTOR);
		int dateOffset = cycleCountVariable.getAttributeInt(CFStandardNames.CF_ADD_OFFSET);
		swathAntennaValues.add(new ValueD2S4(dateVariable, (buffer, beamIndex) //
		-> new SInt((int) ((PingDatagram.getEpochTime(buffer.byteBufferData) / (24 * 3600 * 1000) - dateOffset)
				/ dateFactor))));

		/** Time **/
		NCVariable timeVariable = mbg.getVariable(MbgConstants.Time);
		int timeFactor = cycleCountVariable.getAttributeInt(CFStandardNames.CF_SCALE_FACTOR);
		int timeOffset = cycleCountVariable.getAttributeInt(CFStandardNames.CF_ADD_OFFSET);
		swathAntennaValues.add(new ValueD2S4(timeVariable, (buffer, beamIndex) //
		-> new SInt((int) ((PingDatagram.getEpochTime(buffer.byteBufferData) % (24 * 3600 * 1000) - timeOffset)
				/ timeFactor))));

		/** Across slope **/
		NCVariable acrossSlopeVariable = mbg.getVariable(MbgConstants.AcrossSlope);
		swathBeamValues.add(new ValueD2S2(acrossSlopeVariable, (buffer, i) -> new SShort((short) 0)));

		/** Along slope **/
		NCVariable alongSlopeVariable = mbg.getVariable(MbgConstants.AlongSlope);
		swathBeamValues.add(new ValueD2S2(alongSlopeVariable, (buffer, i) -> new SShort((short) 0)));

		/** Compensation layer mode **/
		NCVariable compLayerModeVariable = mbg.getVariable(MbgConstants.CompensationLayerMode);
		swathAntennaValues.add(new ValueD2S1(compLayerModeVariable, (buffer, antennaIndex) -> {
			return new SByte(allFile.getInstallation().has("SHC") && allFile.getInstallation().getInt("SHC") == 1 ? //
					MbgConstants.COMPENSATION_LAYER_MODE_INACTIVE : MbgConstants.COMPENSATION_LAYER_MODE_ACTIVE);
		}));

		/** Validity **/
		NCVariable validityVariable = mbg.getVariable(MbgConstants.Validity);
		swathBeamValues.add(new ValueD2S1(validityVariable,
				(buffer, iBeam) -> new SByte(getDectectionValidity(buffer, iBeam).getValue())));

		/** Cycle flag **/
		NCVariable cycleValidityVariable = mbg.getVariable(MbgConstants.CFlag);
		swathAntennaValues.add(new ValueD2S1(cycleValidityVariable, (buffer, antennaIndex) -> new SByte((byte) 2)));

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void fillData(AllFile allFile) throws IOException, NCException {
		long[] originSwathBeam;
		long[] originSwathAntenna;
		long[] swathAntennaDim = new long[] { 1, 1 };

		TreeMap<Long, ByteBuffer> positionDatagrams = allFile.getPosition().getSortedActivePositionDatagrams();
		if (positionDatagrams.isEmpty())
			throw new IOException("No valid position datagram");

		TreeMap<Long, short[]> attitudeDatagrams = allFile.getAttitude().getSortedActiveAttitudeDatagrams();

		ObjectBufferPool<DatagramBuffer> pool = new ObjectBufferPool<>(
				() -> new DatagramBuffer(allFile.getByteOrder()));

		for (Entry<SwathIdentifier, AbstractCompletePingMetadata<AbstractCompleteAntennaPingSingleMetadata>> pingEntry : metadata
				.getPings()) {

			// test for ping rejection
			if (!allFile.getGlobalMetadata().getSwathIds().contains(pingEntry.getKey())) {
				continue;
			}

			AbstractCompletePingMetadata<AbstractCompleteAntennaPingSingleMetadata> ping = pingEntry.getValue();
			List<Pair<DatagramBuffer, AbstractCompleteAntennaPingSingleMetadata>> data = DatagramReader.read(pool,
					ping.locate());

			// get origin from swath identifier (ignore if matching index not found)
			var swathId = pingEntry.getKey();
			var optIndex = allFile.getGlobalMetadata().getSwathIds().getIndex(swathId);
			if (optIndex.isEmpty()) {
				DefaultLogger.get().warn(
						"Datagram Depth associated with a missing ping is ignored (number = {}, sequence = {}).",
						swathId.getRawPingNumber(), swathId.getSwathPosition());
				continue;
			}
			originSwathBeam = new long[] { optIndex.get(), 0 };
			originSwathAntenna = new long[] { originSwathBeam[0], -1 };

			// clear buffers TODO : useful ? now data are written antenna by antenna (and no longer ping by ping)
			swathAntennaValues.forEach(ValueD2::clear);
			swathAntennaValuesWithPosition.forEach(ValueD2WithPosition::clear);
			swathAntennaValuesWithAttitude.forEach(ValueD2WithAttitude::clear);
			swathAntennaValuesWithHScale.forEach(ValueD2WithHScale::clear);
			swathBeamValues.forEach(ValueD2::clear);
			swathBeamValuesWithAttitude.forEach(ValueD2WithAttitude::clear);
			swathBeamValuesWithPosition.forEach(ValueD2WithPosition::clear);
			swathBeamValuesWithHScale.forEach(ValueD2WithHScale::clear);

			for (Pair<DatagramBuffer, AbstractCompleteAntennaPingSingleMetadata> antenna : data) {
				DatagramBuffer buffer = antenna.getFirst();

				// Swath x Antenna values
				for (ValueD2 v : swathAntennaValues)
					v.fill(buffer, 0, 0);

				// Swath x Antenna values (with hScale)
				double horizontalScale = computeHorizontaleScale(buffer);
				for (ValueD2WithHScale v : swathAntennaValuesWithHScale)
					v.fill(buffer, 0, 0, horizontalScale);

				// Swath x Antenna values (with position)
				long currentTime = BaseDatagram.getEpochTime(buffer.byteBufferData);
				double[] position = getPositionByInterpolation(currentTime, positionDatagrams);
				if (position.length > 0) {
					for (ValueD2WithPosition v : swathAntennaValuesWithPosition)
						v.fill(buffer, 0, 0, position[0], position[1], position[2], allFile.getPosition());
				}

				// Swath x Antenna values (with attitude)
				double[] attitude = MbgUtils.getAttitudeByInterpolation(currentTime, attitudeDatagrams);
				if (attitude != null) {
					for (ValueD2WithAttitude v : swathAntennaValuesWithAttitude)
						v.fill(buffer, 0, 0, attitude[0], attitude[1], attitude[2]);
				}

				// Swath x beam values
				int maxbeamIndexDest = 0;
				for (int i = 0; i < getBeamCount(buffer); i++) {
					int beamIndexDest = computeBeamIndexDestination(allFile, ping, buffer, i);

					// Swath x beam values
					for (ValueD2 v : swathBeamValues)
						v.fill(buffer, i, beamIndexDest);

					// Swath x beam values + horizontal scale
					for (ValueD2WithHScale v : swathBeamValuesWithHScale)
						v.fill(buffer, i, beamIndexDest, horizontalScale);

					// Swath x beam values + interpolated position
					for (ValueD2WithPosition v : swathBeamValuesWithPosition)
						v.fill(buffer, i, beamIndexDest, position[0], position[1], position[2], allFile.getPosition());

					// Swath x beam values + interpolated attitude
					for (ValueD2WithAttitude v : swathBeamValuesWithAttitude)
						v.fill(buffer, i, beamIndexDest, attitude[0], attitude[1], attitude[2]);

					maxbeamIndexDest = Math.max(beamIndexDest, maxbeamIndexDest);
				}

				// Write in netcdf variable
				originSwathAntenna[1] = computeAntennaIndexDestination(allFile, buffer);

				// if second antenna add an offset
				if (originSwathAntenna[1] == 1)
					originSwathBeam[1] = allFile.getDetectionCount() / 2;

				long[] swathBeamDim = new long[] { 1, maxbeamIndexDest + 1 };

				for (ValueD2 v : swathAntennaValues)
					v.write(originSwathAntenna, swathAntennaDim);

				for (ValueD2WithPosition v : swathAntennaValuesWithPosition)
					v.write(originSwathAntenna, swathAntennaDim);

				for (ValueD2WithAttitude v : swathAntennaValuesWithAttitude)
					v.write(originSwathAntenna, swathAntennaDim);

				for (ValueD2WithHScale v : swathAntennaValuesWithHScale)
					v.write(originSwathAntenna, swathAntennaDim);

				for (ValueD2 v : swathBeamValues)
					v.write(originSwathBeam, swathBeamDim);

				for (ValueD2WithPosition v : swathBeamValuesWithPosition)
					v.write(originSwathBeam, swathBeamDim);

				for (ValueD2WithAttitude v : swathBeamValuesWithAttitude)
					v.write(originSwathBeam, swathBeamDim);

				for (ValueD2WithHScale v : swathBeamValuesWithHScale)
					v.write(originSwathBeam, swathBeamDim);

			}

			// release the datagram buffers for the next ping
			for (Pair<DatagramBuffer, AbstractCompleteAntennaPingSingleMetadata> p : data)
				pool.release(p.getFirst());
		}

	}

	/**
	 * @return beam count for the specified {@link DatagramBuffer}
	 */
	protected abstract int getBeamCount(DatagramBuffer buffer);

	/**
	 * Computes the beam destination index (into the output NetCDF variable)
	 */
	protected int computeBeamIndexDestination(AllFile allFile,
			AbstractCompletePingMetadata<AbstractCompleteAntennaPingSingleMetadata> ping, DatagramBuffer buffer,
			int beamIndexSource) {
		return beamIndexSource;
	};

	/**
	 * Method to compute horizontal scale
	 */
	protected abstract double computeHorizontaleScale(DatagramBuffer buffer);

	/**
	 * @return {@link SoundingValidity} for the specified beam
	 */
	protected abstract SoundingValidity getDectectionValidity(BaseDatagramBuffer buffer, int iBeam);

	/**
	 * @return along distance for the specified beam
	 */
	protected abstract double getAlongDistance(BaseDatagramBuffer buffer, int iBeam);

	/**
	 * @return across distance for the specified beam
	 */
	protected abstract double getAcrossDistance(BaseDatagramBuffer buffer, int iBeam);

	/**
	 * @return vessel heading for the specified beam
	 */
	protected abstract double getVesselHeading(BaseDatagramBuffer buffer);

	/**
	 * Get interpolated latitude and longitude values for a specific timestamp.
	 */
	private double[] getPositionByInterpolation(long currentTime, TreeMap<Long, ByteBuffer> positionDatagrams) {

		if (positionDatagrams == null || positionDatagrams.size() < 2)
			return new double[] {};

		// Get position datagrams before and after the specified time stamp
		Long positionTimeBefore = positionDatagrams.floorKey(currentTime);
		Long positionTimeAfter = positionDatagrams.ceilingKey(currentTime);

		// If no datagram have been found before; use the 2 first datagrams
		if (positionTimeBefore == null) {
			positionTimeBefore = positionDatagrams.firstKey();
			positionTimeAfter = positionDatagrams.higherKey(positionTimeBefore);
		}
		// If no datagram have been found after; use the 2 last datagrams
		if (positionTimeAfter == null) {
			positionTimeAfter = positionDatagrams.lastKey();
			positionTimeBefore = positionDatagrams.lowerKey(positionTimeAfter);
		}

		ByteBuffer positionDatagramBefore = positionDatagrams.get(positionTimeBefore);
		ByteBuffer positionDatagramAfter = positionDatagrams.get(positionTimeAfter);

		// Linear interpolation
		double coeff = !positionTimeAfter.equals(positionTimeBefore)
				? ((double) (currentTime - positionTimeBefore)) / (positionTimeAfter - positionTimeBefore)
				: 0;

		// Get values in degrees (see datagram formats doc)
		double latBefore = Position.getLatitude(positionDatagramBefore).get() / 2e7;
		double lonBefore = Position.getLongitude(positionDatagramBefore).get() / 1e7;
		double speedBefore = Position.getSpeedOverGround(positionDatagramBefore).getU() / 100d;

		double latAfter = Position.getLatitude(positionDatagramAfter).get() / 2e7;
		double lonAfter = Position.getLongitude(positionDatagramAfter).get() / 1e7;
		double speedAfter = Position.getSpeedOverGround(positionDatagramAfter).getU() / 100d;

		double lat = latBefore + coeff * (latAfter - latBefore);
		double lon = lonBefore + coeff * (lonAfter - lonBefore);
		double speed = speedBefore + coeff * (speedAfter - speedBefore);

		return new double[] { lat, lon, speed };
	}

	/**
	 * @return position (lat/lon) for a a sounding detection.
	 */
	protected double[] getDetectionPosition(BaseDatagramBuffer buffer, int beamIndexSource, double latNav,
			double lonNav) {
		double along = getAlongDistance(buffer, beamIndexSource);
		double across = getAcrossDistance(buffer, beamIndexSource);
		double heading = getVesselHeading(buffer);
		return MbgUtils.getDetectionPosition(latNav, lonNav, along, across, heading);
	}

	/**
	 * @return the output antenna index
	 */
	protected int computeAntennaIndexDestination(AllFile allFile, DatagramBuffer buffer) {
		int currentAntennaSerialNumber = PingDatagram.getSerialNumber(buffer.byteBufferData).getU();
		// set correct beam offset if only the first antenna is missing
		Set<Integer> serialNumbers = allFile.getGlobalMetadata().getSerialNumbers();
		if (!serialNumbers.isEmpty() && currentAntennaSerialNumber != serialNumbers.iterator().next())
			return 1;
		return 0;
	}

}
