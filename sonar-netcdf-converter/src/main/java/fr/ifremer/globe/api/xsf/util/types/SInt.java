package fr.ifremer.globe.api.xsf.util.types;

public class SInt implements Comparable<SInt> {
    
    private int v;
    
    public SInt(int v) {
        this.v = v;
    }

    public int get() {
        return v;
    }
    
    public boolean isSigned() {
        return true;
    }
    
    @Override
    public int compareTo(SInt o) {
    	return Integer.compare(v, o.v);
    }
}
