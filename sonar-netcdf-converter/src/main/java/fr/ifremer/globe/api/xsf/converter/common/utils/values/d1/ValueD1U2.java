package fr.ifremer.globe.api.xsf.converter.common.utils.values.d1;

import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.api.xsf.util.types.UShort;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCVariable;

/***
 * class allocating storage for 1D matrix of short, and giving a interface allowing to fill the data
 */
public class ValueD1U2 extends ValueD1 {
	protected final short[] dataOut;
	protected final ValueProvider valueProvider;

	/**
	 * Constructor, allocate netcdf buffer for one variable and one ping
	 */
	public ValueD1U2(NCVariable variable, ValueProvider filler) throws NCException {
		this(variable, filler, false);
	}

	/**
	 * Constructor, allocate netcdf buffer for one variable and all pings
	 */
	public ValueD1U2(NCVariable variable, ValueProvider filler, boolean bufferCapacityIsDimension) throws NCException {
		super(variable, bufferCapacityIsDimension);
		dataOut = new short[dataOutCapacity];
		valueProvider = filler;
	}

	/** {@inheritDoc} */
	@Override
	public void fill(BaseDatagramBuffer buffer, int position) throws NCException {
		if (dataOutCapacity == 1) {
			dataOut[0] = valueProvider.get(buffer).getShort();
			dataOutPosition = 1;
		} else if (position < dataOutCapacity) {
			dataOut[position] = valueProvider.get(buffer).getShort();
			dataOutPosition = position + 1;
		} else {
			// For debugging purpose
			throw new NCException("Try to add a value over the buffer capacity");
		}
	}

	/** {@inheritDoc} */
	@Override
	public void write(long[] origin) throws NCException {
		try {
			variable.putu(origin, new long[] { dataOutCapacity }, dataOut);
			dataOutPosition = 0;
		} catch (Exception e) {
			logger.error(String.format("Error while writing variable %s (%s)", variable.getName(), e.getMessage()));
			throw e;
		}
	}

	@Override
	public void clear() throws NCException {
		if (dataOutCapacity == 1) {
			dataOut[0] = variable.getShortFillValue();
		} else {
			// Fill the buffer with the variable (prevent overwriting previous values)
			var variableData = variable.get_ushort(new long[] { 0l }, new long[] { dataOutCapacity });
			System.arraycopy(variableData, 0, dataOut, 0, dataOutCapacity);
		}
		dataOutPosition = 0;
	}

	@FunctionalInterface
	public interface ValueProvider {
		public UShort get(BaseDatagramBuffer buffer);
	}
}
