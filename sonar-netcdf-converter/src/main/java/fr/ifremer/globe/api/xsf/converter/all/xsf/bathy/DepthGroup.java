package fr.ifremer.globe.api.xsf.converter.all.xsf.bathy;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.all.datagram.PingDatagram;
import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.all.datagram.position.PositionMetadata;
import fr.ifremer.globe.api.xsf.converter.all.xsf.PlatformInterpolator;
import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagram;
import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.AbstractBeamDatagramMetadata;
import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.AbstractCompleteAntennaPingSingleMetadata;
import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.AbstractCompletePingMetadata;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1F4WithAttitude;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1F8WithPosition;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U8;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2U1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2WithAttitude;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2WithPosition;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfFromKongsberg;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.BathymetryGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.BeamGroup1Grp;
import fr.ifremer.globe.api.xsf.util.DefaultLogger;
import fr.ifremer.globe.api.xsf.util.ObjectBufferPool;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.types.DDouble;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.api.xsf.util.types.UInt;
import fr.ifremer.globe.api.xsf.util.types.ULong;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCVariable;
import fr.ifremer.globe.netcdf.util.Pair;
import fr.ifremer.globe.utils.maths.CoordinatesSystem;
import fr.ifremer.globe.utils.maths.RotationMatrix3x3D;
import fr.ifremer.globe.utils.maths.Vector3D;
import fr.ifremer.globe.utils.mbes.MbesUtils;

/**
 * Abstract bathy group which implements common functionality of bathy groups (Depth68 and XYZ88)
 */
public abstract class DepthGroup implements IDatagramConverter<AllFile, XsfFromKongsberg> {

	protected AllFile stats;
	protected AbstractBeamDatagramMetadata<AbstractCompletePingMetadata<AbstractCompleteAntennaPingSingleMetadata>> metadata;
	protected PositionMetadata positionMetadata;

	protected List<ValueD1> swathValues = new LinkedList<>();
	protected List<ValueD2> swathAntennaValues = new LinkedList<>();
	protected List<ValueD2> swathBeamValues = new LinkedList<>();
	protected List<ValueD1F8WithPosition> swathValuesWithPosition = new LinkedList<>();
	protected List<ValueD2WithPosition> swathBeamValuesWithPosition = new LinkedList<>();
	protected List<ValueD1F4WithAttitude> swathValuesWithAttitude = new LinkedList<>();
	protected List<ValueD2WithAttitude> swathBeamValuesWithAttitude = new LinkedList<>();
	protected List<ValueD2WithPosition> swathAntennaValuesWithPosition = new LinkedList<>();
	protected List<ValueD2WithAttitude> swathAntennaValuesWithAttitude = new LinkedList<>();

	protected BeamGroup1Grp beamGrp;
	protected BathymetryGrp bathyGrp;
	protected XsfFromKongsberg xsf;

	// declare the various shapes that will be used

	protected NCVariable addVar;
	protected ValueD2U1 soundingValidityBuffer;

	protected final RotationMatrix3x3D attitudeMatrix = new RotationMatrix3x3D();
	protected final Vector3D txAntennaSCS = new Vector3D();
	private Vector3D activePositionSCS = new Vector3D();

	/**
	 * Constructor
	 */
	@SuppressWarnings("unchecked")
	public DepthGroup(AllFile fileMetadata, @SuppressWarnings("rawtypes") AbstractBeamDatagramMetadata metadata) {
		this.stats = fileMetadata;
		this.metadata = metadata;
		positionMetadata = fileMetadata.getPosition();

	}

	/**
	 * Describe how to fill values with dimensions: beam * values (not the same process with Depth68 and XYZ88)
	 * 
	 * @throws NCException
	 */
	protected abstract void fillSwathBeamValues(
			AbstractCompletePingMetadata<AbstractCompleteAntennaPingSingleMetadata> ping, DatagramBuffer buffer);

	/**
	 * Describe how to fill values with dimensions: beam * values and position (not the same process with Depth68 and
	 * XYZ88)
	 */
	protected abstract void fillSwathBeamValuesWithPosition(
			AbstractCompletePingMetadata<AbstractCompleteAntennaPingSingleMetadata> ping, DatagramBuffer buffer,
			double lat, double lon, double speed);

	/**
	 * Describe how to fill values with dimensions: beam * values and attitude (not the same process with Depth68 and
	 * XYZ88)
	 */
	protected abstract void fillSwathBeamValuesWithAttitude(
			AbstractCompletePingMetadata<AbstractCompleteAntennaPingSingleMetadata> ping, DatagramBuffer buffer,
			double roll, double pitch, double heave);

	/**
	 * @see fr.ifremer.globe.driver.xsf.hsf.soundings.HFSGroup# declareAttributes()
	 */
	@Override
	public void declare(AllFile allFile, XsfFromKongsberg xsf) throws NCException {

		this.xsf = xsf;
		beamGrp = xsf.getBeamGroup();
		bathyGrp = xsf.getBathyGrp();
		/**********************************************************************************************************
		 * xsf common variables
		 **********************************************************************************************************/

		swathValuesWithAttitude.add(new ValueD1F4WithAttitude(beamGrp.getPlatform_roll(),
				/** Get interpolated attitude */
				(buffer, roll, pitch, heave) -> new FFloat((float) roll)));

		swathValuesWithAttitude.add(new ValueD1F4WithAttitude(beamGrp.getPlatform_pitch(),
				/** Get interpolated attitude */
				(buffer, roll, pitch, heave) -> new FFloat((float) pitch)));

		swathValuesWithPosition.add(new ValueD1F8WithPosition(beamGrp.getPlatform_longitude(),
				/** Get interpolated navigation */
				(buffer, lat, lon, speed) -> new DDouble(lon)));

		swathValuesWithPosition.add(new ValueD1F8WithPosition(beamGrp.getPlatform_latitude(),
				/** Get interpolated navigation */
				(buffer, lat, lon, speed) -> new DDouble(lat)));

		// FIXME set soudspeed tx in child class (depend of datagram type XYZ or 68)
		// swathValues.add(new ValueD1U2(beamGrp.getSound_speed_at_transducer(),
		// buffer -> XYZ88Depth.getSoundSpeedAtTx(buffer.byteBufferData)));

		swathValues.add(new ValueD1U8(beamGrp.getPing_time(),
				buffer -> new ULong(BaseDatagram.getXSFEpochTime(buffer.byteBufferData))));

		/**********************************************************************************************************
		 * .all specific variables
		 **********************************************************************************************************/

		/** variables with [swath(ping) * rxAntenna] dimension */

		swathValuesWithPosition.add(new ValueD1F8WithPosition(xsf.getBeamGroupVendorSpecific().getSwath_speed(),
				/** Get interpolated navigation **/
				(buffer, lat, lon, speed) -> new DDouble(speed)));

		swathValues.add(new ValueD1U4(xsf.getBeamGroupVendorSpecific().getPing_raw_count(),
				(buffer) -> new UInt(PingDatagram.getPingNumber(buffer.byteBufferData).getU())));

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.ifremer.globe.driver.xsf.hsf.soundings.HFSGroup# fillSoundingsData(java.io.RandomAccessFile)
	 */
	public void fillData(AllFile allFile) throws IOException, NCException {
		long[] origin = { 0, 0 };
		Vector3D activePositionOffset = xsf.getPreferredPositionOffset();

		TreeMap<Long, ByteBuffer> positionDatagrams = positionMetadata.getSortedActivePositionDatagrams();
		if (positionDatagrams.isEmpty())
			throw new IOException("No valid position datagram");

		TreeMap<Long, short[]> attitudeDatagrams = stats.getAttitude().getSortedActiveAttitudeDatagrams();

		ObjectBufferPool<DatagramBuffer> pool = new ObjectBufferPool<>(() -> new DatagramBuffer(stats.getByteOrder()));

		for (Entry<SwathIdentifier, AbstractCompletePingMetadata<AbstractCompleteAntennaPingSingleMetadata>> pingEntry : metadata
				.getPings()) {
			swathAntennaValues.forEach(ValueD2::clear);
			for (var valueD1F8WithPosition : swathValuesWithPosition)
				valueD1F8WithPosition.clear();
			swathValuesWithAttitude.forEach(ValueD1F4WithAttitude::clear);
			swathBeamValues.forEach(ValueD2::clear);
			swathBeamValuesWithPosition.forEach(ValueD2WithPosition::clear);
			swathBeamValuesWithAttitude.forEach(ValueD2WithAttitude::clear);
			swathAntennaValuesWithPosition.forEach(ValueD2WithPosition::clear);
			swathAntennaValuesWithAttitude.forEach(ValueD2WithAttitude::clear);
			final AbstractCompletePingMetadata<AbstractCompleteAntennaPingSingleMetadata> ping = pingEntry.getValue();

			// test for ping rejection
			if (!allFile.getGlobalMetadata().getSwathIds().contains(pingEntry.getKey())) {
				continue;
			}

			// make sure that the swathValidityBuffer is filled with 0s
			// since valid beams might not be contiguous.
			// soundingValidityBuffer.setDefault(new UByte((byte) 0));

			List<Pair<DatagramBuffer, AbstractCompleteAntennaPingSingleMetadata>> data = DatagramReader.read(pool,
					ping.locate());

			// get origin from swath identifier (ignore if matching index not found)
			var swathId = pingEntry.getKey();
			var optIndex = allFile.getGlobalMetadata().getSwathIds().getIndex(swathId);
			if (optIndex.isEmpty()) {
				DefaultLogger.get().warn(
						"Datagram Depth associated with a missing ping is ignored (number = {}, sequence = {}).",
						swathId.getRawPingNumber(), swathId.getSwathPosition());
				continue;
			}
			origin[0] = optIndex.get();

			final long beamCount = ping.getBeamCount();

			for (Pair<DatagramBuffer, AbstractCompleteAntennaPingSingleMetadata> antenna : data) {
				DatagramBuffer buffer = antenna.getFirst();

				// Get interpolated position
				long currentTime = BaseDatagram.getEpochTime(buffer.byteBufferData);
				double[] position = PlatformInterpolator.getPositionByInterpolation(currentTime, positionDatagrams);
				double[] attitude = PlatformInterpolator.getAttitudeByInterpolation(currentTime, attitudeDatagrams);
				double[] refPointPosition = position;

				if (attitude != null && position != null) {
					double roll = attitude[0];
					double pitch = attitude[1];
					// Set position offset in SCS coordinates
					setActivePositionSCS(CoordinatesSystem.fromVCStoSCS(activePositionOffset, roll, pitch));
					refPointPosition = getReferencePointLatLong(buffer, position[0], position[1]);
				}

				int antennaIdx = stats.getInstallation()
						.getRxAntennaIndex(PingDatagram.getSerialNumber(buffer.byteBufferData));

				// Swath values
				for (ValueD1 v : swathValues)
					v.fill(buffer);

				// Swath x Antenna values
				for (ValueD2 v : swathAntennaValues)
					v.fill(buffer, 0, antennaIdx);

				// Swath x Antenna values (with position)
				if (position != null) {
					for (ValueD1F8WithPosition v : swathValuesWithPosition)
						v.fill(buffer, refPointPosition[0], refPointPosition[1], position[2]);

					for (ValueD2WithPosition v : swathAntennaValuesWithPosition) {
						v.fill(buffer, 0, antennaIdx, refPointPosition[0], refPointPosition[1], position[2],
								stats.getPosition());
					}
				}

				// Swath x Antenna values (with attitude)
				if (attitude != null) {
					for (ValueD1F4WithAttitude v : swathValuesWithAttitude)
						v.fill(buffer, attitude[0], attitude[1], attitude[2]);

					for (ValueD2WithAttitude v : swathAntennaValuesWithAttitude) {
						v.fill(buffer, 0, antennaIdx, attitude[0], attitude[1], attitude[2]);
					}
				}

				// Swath x beam values
				fillSwathBeamValues(ping, buffer);

				// Swath x beam values + interpolated position
				if (position != null)
					fillSwathBeamValuesWithPosition(ping, buffer, refPointPosition[0], refPointPosition[1],
							position[2]);

				// Swath x beam values + interpolated attitude
				if (attitude != null)
					fillSwathBeamValuesWithAttitude(ping, buffer, attitude[0], attitude[1], attitude[2]);

			}

			long[] originD1 = new long[] { origin[0] };

			// Write in netcdf variable
			for (ValueD1 v : swathValues)
				v.write(originD1);

			for (ValueD2 v : swathAntennaValues)
				v.write(origin, new long[] { 1, stats.getGlobalMetadata().getSerialNumbers().size() });

			for (ValueD1F8WithPosition v : swathValuesWithPosition)
				v.write(originD1);

			for (ValueD1F4WithAttitude v : swathValuesWithAttitude)
				v.write(originD1);

			for (ValueD2 v : swathBeamValues)
				v.write(origin, new long[] { 1, beamCount });

			for (ValueD2WithPosition v : swathBeamValuesWithPosition)
				v.write(origin, new long[] { 1, beamCount });

			for (ValueD2WithAttitude v : swathBeamValuesWithAttitude)
				v.write(origin, new long[] { 1, beamCount });

			for (ValueD2WithPosition v : swathAntennaValuesWithPosition)
				v.write(origin, new long[] { 1, stats.getGlobalMetadata().getSerialNumbers().size() });

			for (ValueD2WithAttitude v : swathAntennaValuesWithAttitude)
				v.write(origin, new long[] { 1, stats.getGlobalMetadata().getSerialNumbers().size() });

			// release the datagram buffers for the next ping
			for (Pair<DatagramBuffer, AbstractCompleteAntennaPingSingleMetadata> p : data) {
				pool.release(p.getFirst());
			}
		}

	}

	/**
	 * @return position (lat/lon) for a a sounding detection.
	 */
	protected double[] getDetectionLatLong(BaseDatagramBuffer buffer, int beamIndexSource, double latNav,
			double lonNav) {
		double along = getAlongDistance(buffer, beamIndexSource);
		double across = getAcrossDistance(buffer, beamIndexSource);
		double heading = getVesselHeading(buffer);
		return MbesUtils.acrossAlongToWGS84LatLong(latNav, lonNav, heading, across, along);
	}

	/**
	 * @return position (lat/lon) for a the reference point.
	 */
	protected double[] getReferencePointLatLong(BaseDatagramBuffer buffer, double latNav, double lonNav) {
		double along = -activePositionSCS.getX();
		double across = -activePositionSCS.getY();
		double heading = getVesselHeading(buffer);
		return MbesUtils.acrossAlongToWGS84LatLong(latNav, lonNav, heading, across, along);
	}

	protected void setActivePositionSCS(Vector3D activePositionSCS) {
		this.activePositionSCS = activePositionSCS;
	}

	protected Vector3D getActivePositionSCS() {
		return activePositionSCS;
	}

	protected abstract double getAlongDistance(BaseDatagramBuffer buffer, int iBeam);

	protected abstract double getAcrossDistance(BaseDatagramBuffer buffer, int iBeam);

	protected abstract double getVesselHeading(BaseDatagramBuffer buffer);

}
