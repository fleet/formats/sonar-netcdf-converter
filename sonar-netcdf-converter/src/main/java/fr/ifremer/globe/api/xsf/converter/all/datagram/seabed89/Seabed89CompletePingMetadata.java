package fr.ifremer.globe.api.xsf.converter.all.datagram.seabed89;

import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.AbstractCompletePingMetadata;

public class Seabed89CompletePingMetadata extends AbstractCompletePingMetadata<Seabed89CompleteAntennaPingMetadata> {
	public Seabed89CompletePingMetadata(int[] serialNumbers) {
		super(0, serialNumbers);
	}
}
