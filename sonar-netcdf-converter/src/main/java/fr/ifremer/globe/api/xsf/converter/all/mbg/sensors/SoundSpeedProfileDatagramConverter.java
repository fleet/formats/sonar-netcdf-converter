package fr.ifremer.globe.api.xsf.converter.all.mbg.sensors;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicInteger;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.all.datagram.soundspeedprofile.SoundSpeedProfile;
import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgBase;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgConstants;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1S2;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1S4;
import fr.ifremer.globe.api.xsf.util.SimpleIdentifier;
import fr.ifremer.globe.api.xsf.util.types.SInt;
import fr.ifremer.globe.api.xsf.util.types.SShort;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCVariable;
import fr.ifremer.globe.utils.date.DateUtils;

/**
 * Converter for SoundSpeedProfile datagram (SSP) [0x55]
 */
public class SoundSpeedProfileDatagramConverter implements IDatagramConverter<AllFile, MbgBase> {

	private List<ValueD1> profileValues;

	@Override
	public void declare(AllFile allFile, MbgBase mbg) throws NCException {
		profileValues = new LinkedList<>();
		/** Velocity profile index **/
		AtomicInteger currProfilIndex = new AtomicInteger();
		NCVariable velProfileIndex = mbg.getVariable(MbgConstants.VelProfilIdx);
		profileValues.add(new ValueD1S2(velProfileIndex, //
				buffer -> new SShort((short) currProfilIndex.incrementAndGet())));

		/** Velocity profile date **/
		NCVariable velProfileDate = mbg.getVariable(MbgConstants.VelProfilDate);
		profileValues.add(new ValueD1S4(velProfileDate, buffer -> {
			long date = SoundSpeedProfile.getProfilEpochTime(allFile, buffer.byteBufferData);
			return new SInt(DateUtils.getDateTime(date)[0]);
		}));

		/** Velocity profile time **/
		NCVariable velProfileTime = mbg.getVariable(MbgConstants.VelProfilTime);
		profileValues.add(new ValueD1S4(velProfileTime, buffer -> {
			int date = DateUtils.getDateTime(SoundSpeedProfile.getProfilEpochTime(allFile, buffer.byteBufferData))[1];
			return new SInt(date);
		}));
	}

	@Override
	public void fillData(AllFile allFile) throws IOException, NCException {
		long[] origin = new long[] { 0 };
		DatagramBuffer buffer = new DatagramBuffer(allFile.getByteOrder());

		for (Entry<SimpleIdentifier, DatagramPosition> posentry : allFile.getSoundSpeedProfile().getDatagrams()) {
			// read raw data
			final DatagramPosition pos = posentry.getValue();
			DatagramReader.readDatagramAt(pos.getFile(), buffer, pos.getSeek());

			// fill values with buffer
			for (ValueD1 value : this.profileValues) {
				value.fill(buffer);
			}

			// flush into the output file
			for (ValueD1 value : this.profileValues) {
				value.write(origin);
			}
			origin[0] += 1;
		}
	}
}
