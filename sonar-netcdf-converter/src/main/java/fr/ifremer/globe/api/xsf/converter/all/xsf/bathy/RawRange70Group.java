package fr.ifremer.globe.api.xsf.converter.all.xsf.bathy;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.all.datagram.rawrange70.RawRange70;
import fr.ifremer.globe.api.xsf.converter.all.datagram.rawrange70.RawRange70CompleteAntennaPingMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.rawrange70.RawRange70CompletePingMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.rawrange70.RawRange70Metadata;
import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1F4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U8;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.*;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfFromKongsberg;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.BathymetryGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.BeamGroup1Grp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.BathymetryVendorSpecificGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.types.transmit_t;
import fr.ifremer.globe.api.xsf.util.DefaultLogger;
import fr.ifremer.globe.api.xsf.util.ObjectBufferPool;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.types.*;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.util.Pair;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

public class RawRange70Group implements IDatagramConverter<AllFile, XsfFromKongsberg> {

	protected List<ValueD1> swathValues = new LinkedList<>();
	protected BeamGroup1Grp beamGrp;
	private AllFile stats;
	private RawRange70Metadata metadata;
	private List<ValueD2> swathAntennaValues;
	private List<ValueD2> swathTxValues;
	private List<ValueD2> swathBeamValues;
	private BathymetryGrp bathyGrp;

	public RawRange70Group(AllFile stats) {
		this.stats = stats;
		this.metadata = stats.getRawRange70();
		this.swathAntennaValues = new LinkedList<>();
		this.swathTxValues = new LinkedList<>();
		this.swathBeamValues = new LinkedList<>();
	}

	@Override
	public void declare(AllFile allFile, XsfFromKongsberg xsf) throws NCException {
		beamGrp = xsf.getBeamGroup();
		bathyGrp = xsf.getBathyGrp();
		BathymetryVendorSpecificGrp bathyVendorGrp = xsf.getBathy();

		swathValues.add(new ValueD1U4(xsf.getBeamGroupVendorSpecific().getPing_raw_count(),
				(buffer) -> new UInt(RawRange70.getPingNumber(buffer.byteBufferData).getU())));

		swathValues.add(new ValueD1U8(beamGrp.getPing_time(),
				(buffer) -> new ULong(RawRange70.getXSFEpochTime(buffer.byteBufferData))));

		swathAntennaValues.add(new ValueD2U1(xsf.getBeamGroupVendorSpecific().getBeam_count(),
				(buffer, a) -> RawRange70.getValidBeamsCounter(buffer.byteBufferData)));

		swathAntennaValues.add(new ValueD2U2(xsf.getBeamGroupVendorSpecific().getTx_sector_count(),
				(buffer, a) -> new UShort((short) 1)));

		swathValues.add(new ValueD1F4(beamGrp.getSound_speed_at_transducer(),
				buffer -> new FFloat(RawRange70.getSoundSpeed(buffer.byteBufferData).getU() * 0.1f)));

		// declare the swath-beam variables
		/** Add variables with a ping/beam dimension */

		swathBeamValues.add(new ValueD2F4(bathyGrp.getDetection_beam_pointing_angle(),
				(buffer, i) -> new FFloat(RawRange70.getBeamPointingAngle(buffer.byteBufferData, i).get() * 0.01f)));

		swathTxValues.add(new ValueD2F4(xsf.getBeamGroupVendorSpecific().getRaw_tx_beam_tilt_angle(), (buffer, i) -> {
			float angleDeg = RawRange70.getTxTilt(buffer.byteBufferData, 0).get() * 0.01f;
			return new FFloat(angleDeg);
		}));

		swathBeamValues.add(new ValueD2U2(bathyVendorGrp.getEM3000detection_range(),
				(buffer, i) -> RawRange70.getRange(buffer.byteBufferData, i)));

		swathBeamValues.add(new ValueD2S2(bathyGrp.getDetection_tx_beam(), (buffer, i) -> new SShort((short) 0)));
		swathBeamValues.add(new ValueD2S2(bathyGrp.getDetection_tx_transducer_index(),
				(buffer, i) -> new SShort((short) stats.getInstallation().getTxTransducerIndex(0))));
		swathTxValues.add(new ValueD2S4(beamGrp.getTransmit_transducer_index(),
				(buffer, i) -> new SInt(stats.getInstallation().getTxTransducerIndex(0))));
		swathTxValues.add(
				new ValueD2S1(beamGrp.getTransmit_type(), (buffer, i) -> new SByte(transmit_t.CW.getValue())));

		// reflectivity is taken from depth datagram

	}

	@Override
	public void fillData(AllFile allFile) throws IOException, NCException {
		long origin[] = { 0, 0 };
		ObjectBufferPool<DatagramBuffer> pool = new ObjectBufferPool<>(() -> new DatagramBuffer(stats.getByteOrder()));

		for (Entry<SwathIdentifier, RawRange70CompletePingMetadata> pingEntry : metadata.getPings()) {
			swathAntennaValues.forEach(ValueD2::clear);
			swathBeamValues.forEach(ValueD2::clear);
			swathTxValues.forEach(ValueD2::clear);

			final RawRange70CompletePingMetadata ping = pingEntry.getValue();
			if (!ping.isComplete(stats.getGlobalMetadata().getSerialNumbers()) || !allFile.getGlobalMetadata()
					.getSwathIds().contains(pingEntry.getKey())) {
				continue;
			}

			List<Pair<DatagramBuffer, RawRange70CompleteAntennaPingMetadata>> data = DatagramReader.read(pool,
					ping.locate());

			// get origin from swath identifier (ignore if matching index not found)
			var swathId = pingEntry.getKey();
			var optIndex = stats.getGlobalMetadata().getSwathIds().getIndex(swathId);
			if (optIndex.isEmpty()) {
				DefaultLogger.get()
						.warn("Datagram RawRange70 associated with a missing ping is ignored (number = {}, sequence = {}).",
								swathId.getRawPingNumber(), swathId.getSwathPosition());
				continue;
			}
			origin[0] = optIndex.get();

			final int beamCount = ping.getBeamCount();

			for (Pair<DatagramBuffer, RawRange70CompleteAntennaPingMetadata> antenna : data) {

				DatagramBuffer buffer = antenna.getFirst();
				// Swath values
				for (ValueD1 v : swathValues)
					v.fill(buffer);

				// Swath x Antenna shared values
				for (ValueD2 v : swathAntennaValues) {
					v.fill(buffer, 0, stats.getInstallation()
							.getRxAntennaIndex(RawRange70.getSerialNumber(buffer.byteBufferData)));
				}

				// fill beam related data
				final int beamOffset = ping.getBeamOffset(RawRange70.getSerialNumber(buffer.byteBufferData).getU());
				for (int i = 0; i < RawRange70.getValidBeamsCounter(buffer.byteBufferData).getU(); i++) {
					final int beamIndex = beamOffset + RawRange70.getBeamNumber(buffer.byteBufferData, i).getU();
					for (ValueD2 v : swathBeamValues) {
						v.fill(antenna.getFirst(), i, beamIndex);
					}
				}

				// fill the tx sector related data (only 1 sector)
				for (ValueD2 v : swathTxValues) {
					v.fill(buffer, 0, 0);
				}

			}
			long[] originD1 = new long[] { origin[0] };
			// Write in netcdf variable
			for (ValueD1 v : swathValues)
				v.write(originD1);

			// fill the data
			for (ValueD2 v : swathAntennaValues)
				v.write(origin, new long[] { 1, stats.getGlobalMetadata().getSerialNumbers().size() });

			for (ValueD2 v : swathBeamValues)
				v.write(origin, new long[] { 1, beamCount });

			for (ValueD2 v : swathTxValues) {
				v.write(origin, new long[] { 1, 1 });
			}

			for (Pair<DatagramBuffer, RawRange70CompleteAntennaPingMetadata> antenna : data)
				pool.release(antenna.getFirst());

		}
	}
}
