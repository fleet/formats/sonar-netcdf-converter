package fr.ifremer.globe.api.xsf.converter.common.sounder;

import fr.ifremer.globe.utils.sounders.SounderDescription;

/**
 * Provides data to fill the summary group.
 */
public interface ISounderFile extends AutoCloseable   {

	
	/** Global **/
	String getFilePath();

	String getModelNumber();
	
	SounderDescription getSounderDescription();
	
	int getMbgModelNumber();

	int getMbgSerialNumber();
	
	String getRawInstallationParameters();
	
	public double[] getTxAntennaLevelArm();
	
	int getSwathCount();
	
	default int getSwathCountFromDepthDatagrams() {
		return getSwathCount();
	}

	int getDetectionCount();

	int getTxSectorCount();

	int getWcBeamCount();

	boolean hasPhaseValue();
	
	int getMaxSeabedSampleCount();

	int getMaxSeabedExtraSampleCount();

	int getAntennaNb();

	int getRxAntennaNb();

	int getTxAntennaNb();

	/** Date **/
	long getMinDate();

	long getMaxDate();
	
	default long getDepthDatagramMinDate() {
		return getMinDate();
	}

	default long getDepthDatagramMaxDate() {
		return getMaxDate();
	}

	/** Localization **/
	double getLatMin();

	double getLatMax();

	double getLonMin();

	double getLonMax();

	double getSOGMin();

	double getSOGMax();

	/** Bathy **/
	double getDepthMin();

	double getDepthMax();

	float getDepthMean();

	/** Attitude **/
	float getHeadingMin();

	float getHeadingMax();

	float getHeadingMean();

	float getRollMin();

	float getRollMax();

	float getRollMean();

	float getPitchMin();

	float getPitchMax();

	float getPitchMean();

	float getHeaveMin();

	float getHeaveMax();

	float getHeaveMean();

	/** Seabed **/
	float getBsMin();

	float getBsMax();

	float getBsMean();

	/** Surface sound speed **/
	
	/**
	 * retrieve the number of surface sound speed records
	 * */
	long getSurfaceSoundSpeedCount();
	
	float getSspMin();

	float getSspMax();

	float getSspMean();
	
	/**Sound Velocity Profile*/

	/**
	 * get the number of sound velocity profiles
	 * */
	long getSVPProfileCount();
	/**
	 * get the maximum number of sample of all svp profiles
	 * */
	long getSVPMaxSampleCount();
	
	/** get the number of position captor*/
	long getPositionCaptorCount();
	
	/** get the number of MRU (attitude) captor*/
	long getMRUCaptorCount();

	long getBeamDescriptionCount();

	long getSnippetCount();

	long getSidescanCount();
	
	/** mbg **/
	default String getAcrossAngleCorrectFlag() {
		return "";
	};


}
