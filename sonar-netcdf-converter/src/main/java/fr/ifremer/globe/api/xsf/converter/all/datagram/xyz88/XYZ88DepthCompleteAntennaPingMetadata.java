package fr.ifremer.globe.api.xsf.converter.all.datagram.xyz88;

import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.AbstractCompleteAntennaPingSingleMetadata;

public class XYZ88DepthCompleteAntennaPingMetadata extends AbstractCompleteAntennaPingSingleMetadata {

    public XYZ88DepthCompleteAntennaPingMetadata(DatagramPosition position, int beamCount, int sampleCount) {
        super(position, beamCount, sampleCount);
    }

}
