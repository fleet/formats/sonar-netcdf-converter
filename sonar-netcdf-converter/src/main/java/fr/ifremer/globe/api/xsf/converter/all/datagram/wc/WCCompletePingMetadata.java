package fr.ifremer.globe.api.xsf.converter.all.datagram.wc;

import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.AbstractCompletePingMetadata;

public class WCCompletePingMetadata extends AbstractCompletePingMetadata<WCCompleteAntennaPingMetadata> {

    public WCCompletePingMetadata(int txSectorCount, int[] serialNumbers) {
        super(txSectorCount, serialNumbers);
    }
}
