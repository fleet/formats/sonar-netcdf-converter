package fr.ifremer.globe.api.xsf.converter.all.xsf.sensors;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.all.datagram.IndividualSensorMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.attitude.Attitude;
import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagram;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.utils.Namer;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.*;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfFromKongsberg;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.AttitudeSubGroup;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.AttitudeSubGroupVendorSpecificGrp;
import fr.ifremer.globe.api.xsf.util.SimpleIdentifier;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.api.xsf.util.types.ULong;
import fr.ifremer.globe.api.xsf.util.types.UShort;
import fr.ifremer.globe.netcdf.ucar.NCException;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

public class AttitudeGroup implements IDatagramConverter<AllFile, XsfFromKongsberg> {

	private final AllFile metadata;
	private final List<AttitudeSensorGroup> subgroups = new LinkedList<>();

	/** Constructor **/
	public AttitudeGroup(AllFile metadata) {
		this.metadata = metadata;
	}

	@Override
	public void declare(AllFile allFile, XsfFromKongsberg xsf) throws NCException {
		metadata.getAttitude().forEachSensor((key, sensorMetadata) -> {
			if (sensorMetadata.getTotalNumberOfEntries() > 0) {
				subgroups.add(new AttitudeSensorGroup(key, sensorMetadata));
			}
		});
		for (AttitudeSensorGroup sensorGr : subgroups)
			sensorGr.declare(allFile, xsf);
	}

	@Override
	public void fillData(AllFile allFile) throws IOException, NCException {
		for (AttitudeSensorGroup sensorGr : subgroups)
			sensorGr.fillData(allFile);
	}

	/**
	 * Attitude sub group (one per sensor)
	 */
	private class AttitudeSensorGroup implements IDatagramConverter<AllFile, XsfFromKongsberg> {
		private final List<ValueD2> values;
		private final IndividualSensorMetadata sensorMetadata;
		private final Integer sensorId;
		int nEntries;
		private final List<NetworkAttitudeVelocityGroup> subgroups = new LinkedList<>();

		public AttitudeSensorGroup(Integer sensorId, IndividualSensorMetadata sensorMetadata) {
			this.sensorId = sensorId;
			this.sensorMetadata = sensorMetadata;
			this.nEntries = (int) sensorMetadata.getTotalNumberOfEntries();
			this.values = new LinkedList<>();

			if (metadata.getNetworkAttitudeVelocity().datagramCount > 0)
				subgroups.add(new NetworkAttitudeVelocityGroup(metadata, sensorId.shortValue()));

		}

		@Override
		public void declare(AllFile allFile, XsfFromKongsberg xsf) throws NCException {
			/**********************************************************************************************************
			 * xsf common variables
			 **********************************************************************************************************/
			// WE use Value2D to store data that will be effectively store in a 1D variable, values for some values will
			// be duplicated (like serial number and so on)

			AttitudeSubGroup attitudeGrp = xsf.addAttitudeGrp(Namer.getAttitudeSubGroupName(sensorId), nEntries);

			values.add(new ValueD2F4(attitudeGrp.getRoll(), sensorMetadata.getMaxEntriesPerDatagram(),
					(buffer, i) -> new FFloat(Attitude.getRoll(buffer.byteBufferData, i).get() * 0.01f)));

			values.add(new ValueD2F4(attitudeGrp.getPitch(), sensorMetadata.getMaxEntriesPerDatagram(),
					(buffer, i) -> new FFloat(Attitude.getPitch(buffer.byteBufferData, i).get() * 0.01f)));

			values.add(new ValueD2F4(attitudeGrp.getVertical_offset(), sensorMetadata.getMaxEntriesPerDatagram(),
					(buffer, i) -> new FFloat(Attitude.getHeave(buffer.byteBufferData, i).get() * -0.01f)));

			values.add(new ValueD2F4(attitudeGrp.getHeading(), sensorMetadata.getMaxEntriesPerDatagram(),
					(buffer, i) -> new FFloat(Attitude.getHeading(buffer.byteBufferData, i).getU() * 0.01f)));

			values.add(new ValueD2U8(attitudeGrp.getTime(), sensorMetadata.getMaxEntriesPerDatagram(),
					(buffer, i) -> new ULong(BaseDatagram.milliSecondToNano(BaseDatagram.getEpochTime(buffer.byteBufferData)
						+ Attitude.getTimeSinceRecordStart(buffer.byteBufferData, i).getU()))
			));
			/**********************************************************************************************************
			 * .all specific variables
			 **********************************************************************************************************/
			AttitudeSubGroupVendorSpecificGrp sensorVendorGroup = new AttitudeSubGroupVendorSpecificGrp(attitudeGrp,
					allFile, new HashMap<>());

			values.add(new ValueD2U2(sensorVendorGroup.getSystem_serial_number(),
					sensorMetadata.getMaxEntriesPerDatagram(),
					(buffer, i) -> Attitude.getSerialNumber(buffer.byteBufferData)));

			values.add(new ValueD2U2(sensorVendorGroup.getAttitude_raw_count(),
					sensorMetadata.getMaxEntriesPerDatagram(),
					(buffer, i) -> Attitude.getCounter(buffer.byteBufferData)));

			values.add(new ValueD2U2(sensorVendorGroup.getSensor_status(), sensorMetadata.getMaxEntriesPerDatagram(),
					(buffer, i) -> Attitude.getSensorStatus(buffer.byteBufferData, i)));
			values.add(new ValueD2U1(sensorVendorGroup.getSensor_system_descriptor(),
					sensorMetadata.getMaxEntriesPerDatagram(),
					(buffer, i) -> Attitude.getSensorDescriptor(buffer.byteBufferData)));

			for (NetworkAttitudeVelocityGroup sub : subgroups) {
				sub.declare(allFile, xsf, sensorVendorGroup);
			}

		}

		@Override
		public void fillData(AllFile allFile) throws IOException, NCException {
			long[] count = new long[] { 0 };
			long[] origin = new long[] { 0 };

			DatagramBuffer buffer = new DatagramBuffer(metadata.getByteOrder());
			for (Entry<SimpleIdentifier, DatagramPosition> posentry : sensorMetadata.getDatagrams()) {
				values.forEach(ValueD2::clear);
				final DatagramPosition pos = posentry.getValue();
				DatagramReader.readDatagramAt(pos.getFile(), buffer, pos.getSeek());
				long ncount = Attitude.getNumberEntries(buffer.byteBufferData).getU();

				// read the data from the source file
				for (int i = 0; i < ncount; i++) {
					for (ValueD2 value : this.values)
						value.fill(buffer, i, i);
				}

				// flush into the output file
				count[0] = ncount;
				for (ValueD2 value : this.values)
					value.write(origin, count);

				origin[0] += ncount;
			}
			for (NetworkAttitudeVelocityGroup sub : subgroups) {
				sub.fillData(allFile);
			}

		}
	}

}
