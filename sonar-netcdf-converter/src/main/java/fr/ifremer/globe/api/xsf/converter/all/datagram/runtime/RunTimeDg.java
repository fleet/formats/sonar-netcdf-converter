package fr.ifremer.globe.api.xsf.converter.all.datagram.runtime;

import java.nio.ByteBuffer;

import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagram;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.datagram.KmSounderLib;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.SByte;
import fr.ifremer.globe.api.xsf.util.types.SShort;
import fr.ifremer.globe.api.xsf.util.types.UByte;
import fr.ifremer.globe.api.xsf.util.types.UShort;

/**
 * Stateless Clock datagram
 */
public class RunTimeDg extends BaseDatagram {

	@Override
	public RunTimeMetadata getSpecificMetadata(AllFile metadata) {
		return metadata.getRunTime();
	}

	@Override
	protected void computeSpecificMetadata(AllFile metadata, ByteBuffer datagram, byte datagramType,
			long epochTime, DatagramPosition position) {

        final RunTimeMetadata sensorMetadata = getSpecificMetadata(metadata);
        sensorMetadata.addDatagram(position, getEpochTime(datagram), 1);

	}
	
	public static UShort getCounter(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 12);
	}

	public static UShort getSerialNumber(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 14);
	}

	public static UByte getOperatorStationStatus(ByteBuffer datagram) {
		return TypeDecoder.read1U(datagram, 16);
	}

	public static UByte getProcessingUnitStatus(ByteBuffer datagram) {
		return TypeDecoder.read1U(datagram, 17);
	}
	public static UByte getBSPStatus(ByteBuffer datagram) {
			return TypeDecoder.read1U(datagram, 18);
	}
	public static UByte getSonarHeadOrTransceiverStatus(ByteBuffer datagram) {
			return TypeDecoder.read1U(datagram, 19);
	}
	public static UByte getMode(ByteBuffer datagram) {
			return TypeDecoder.read1U(datagram, 20);
	}
	public static UByte getFilterIdentifier(ByteBuffer datagram) {
			return TypeDecoder.read1U(datagram, 21);
	}
	public static UShort getMinimumDepth(ByteBuffer datagram) {
			return TypeDecoder.read2U(datagram, 22);
	}
	public static UShort getMaximumDepth(ByteBuffer datagram) {
			return TypeDecoder.read2U(datagram, 24);
	}
	public static UShort getAbsorptionCoefficient(ByteBuffer datagram) {
			return TypeDecoder.read2U(datagram, 26);
	}
	public static UShort getTransmitPulseLength(ByteBuffer datagram) {
			return TypeDecoder.read2U(datagram, 28);
	}
	public static UShort getTransmitBeamwidth(ByteBuffer datagram) {
			return TypeDecoder.read2U(datagram, 30);
	}
	public static SByte getTransmitPowerReMaximum(ByteBuffer datagram) {
			return TypeDecoder.read1S(datagram, 32);
	}
	public static UByte getReceiveBeamwidth(ByteBuffer datagram) {
			return TypeDecoder.read1U(datagram, 33
					);
	}
	public static UByte getReceiveBandwidth(ByteBuffer datagram) {
			return TypeDecoder.read1U(datagram, 34);
	}
	public static UByte getMode2(ByteBuffer datagram) {
        int modelNum = getModelNumber(datagram);
        // For 2040 serie only.
        // TODO : identifier les modeles 2040M et 2040P
        if ((modelNum == KmSounderLib.EM_2040 || modelNum == KmSounderLib.EM_2040C)) {
            return TypeDecoder.read1U(datagram, 35);
        } else {
            throw new RuntimeException("Invalid model");
        }
    }
	public static UByte getReceiverFixedGain(ByteBuffer datagram) {
        int modelNum = getModelNumber(datagram);
        // For all models except 20140 serie.
        // TODO : identifier les modeles 2040M et 2040P
        if ((modelNum != KmSounderLib.EM_2040 && modelNum != KmSounderLib.EM_2040C)) {
            return TypeDecoder.read1U(datagram, 35);
        } else {
            throw new RuntimeException("Invalid model");
        }
    }   

	public static UByte getTVGLawCrossoverAngle(ByteBuffer datagram) {
			return TypeDecoder.read1U(datagram, 36);
	}
	public static UByte getSourceSoundSpeedTransducer(ByteBuffer datagram) {
			return TypeDecoder.read1U(datagram, 37);
	}
	public static UShort getMaximumPortSwathWidth(ByteBuffer datagram) {
			return TypeDecoder.read2U(datagram, 38);
	}
	public static UByte getBeamSpacing(ByteBuffer datagram) {
			return TypeDecoder.read1U(datagram, 40);
	}
	public static UByte getMaximumPortCoverage(ByteBuffer datagram) {
			return TypeDecoder.read1U(datagram, 41);
	}
	public static UByte getYawAndPitchStabilizationMode(ByteBuffer datagram) {
		return TypeDecoder.read1U(datagram, 42);
	}
	public static UByte getMaximumStarboardCoverage(ByteBuffer datagram) {
		return TypeDecoder.read1U(datagram, 43);
	}
	public static UShort getMaximumStarboardSwath(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 44);
	}
	public static UShort getDurotongSpeed(ByteBuffer datagram) {
        int modelNum = getModelNumber(datagram);
        // For 1002 only.
        if ((modelNum == 1002)) {
            return TypeDecoder.read2U(datagram, 46);
        } else {
            throw new RuntimeException("Invalid model");
        }
    }
	public static SShort getTransmitAlongTilt(ByteBuffer datagram) {
        int modelNum = getModelNumber(datagram);
        // For all models except 1002.
        if ((modelNum != 1002)) {
            return TypeDecoder.read2S(datagram, 46);
        } else {
            throw new RuntimeException("Invalid model");
        }
    }
	
	public static UByte getHiLoFrequencyAbsorptionCoef(ByteBuffer datagram) {
        int modelNum = getModelNumber(datagram);
        // For 1002 only.
        if ((modelNum == 1002)) {
            return TypeDecoder.read1U(datagram, 48);
        } else {
            throw new RuntimeException("Invalid model");
        }
    }
	
	public static UByte getFilterIdentifier2(ByteBuffer datagram) {
        int modelNum = getModelNumber(datagram);
        // For all models except 1002.
        if ((modelNum != 1002)) {
            return TypeDecoder.read1U(datagram, 48);
        } else {
            throw new RuntimeException("Invalid model");
        }
    }


	public static void print(ByteBuffer datagram) {
		System.out.println(getCounter(datagram));
		System.out.println(getSerialNumber(datagram));

	}

}
