package fr.ifremer.globe.api.xsf.converter.s7k.metadata;

import java.util.Map.Entry;
import java.util.Set;

import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.DatagramMetadata;

public class S7KIndividualSensorMetadata extends DatagramMetadata<Long, DatagramPosition>{

	protected long totalEntriesCount;
	protected int maxEntriesPerDg;
	
	public S7KIndividualSensorMetadata() {
		totalEntriesCount = 0;
		maxEntriesPerDg = 0;
	}
	
	public void addDatagram(DatagramPosition position, long time, int numEntries) {
		this.index.put(time, position);
		this.totalEntriesCount += numEntries;
		this.maxEntriesPerDg = Integer.max(this.maxEntriesPerDg, numEntries);
		//super.addDatagram(); Sometimes datagram can be duplicated (runtime/ssp) over several files
		this.datagramCount=this.index.size();
	}
	
	public long getTotalNumberOfEntries() {
		return totalEntriesCount;
	}
	
	public int getMaxEntriesPerDatagram() {
		return maxEntriesPerDg;
	}
	
	public Set<Entry<Long, DatagramPosition>> getDatagrams() {
		return getEntries();
	}
}
