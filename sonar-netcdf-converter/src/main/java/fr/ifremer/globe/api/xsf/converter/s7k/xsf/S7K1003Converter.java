package fr.ifremer.globe.api.xsf.converter.s7k.xsf;

import static java.lang.Math.toIntExact;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.sounder.ISounderFile;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1F4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1F8;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1S1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U8;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.PositionSubGroup;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.PositionSubGroupVendorSpecificGrp;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K1003;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7K1003InvidivualSensorMetadata;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KFile;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KIndividualSensorMetadata;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.api.xsf.util.types.SByte;
import fr.ifremer.globe.api.xsf.util.types.ULong;
import fr.ifremer.globe.netcdf.ucar.NCException;

public class S7K1003Converter implements IDatagramConverter<S7KFile, XsfFromS7k> {

	private List<S7K1003IndividualConverter> subgroups = new LinkedList<>();

	@Override
	public void declare(S7KFile s7kFile, XsfFromS7k xsf) throws NCException {
		for (Entry<Long, S7K1003InvidivualSensorMetadata> sensor : s7kFile.get1003Metadata().getEntries()) {
			if (sensor.getValue().usesGridPositionning()) {
				throw new RuntimeException("Grid positionning is not supported for 1003 datagrams");
			}

			S7K1003IndividualConverter grp = new S7K1003IndividualConverter(s7kFile, sensor.getKey(),
					sensor.getValue());

			grp.declare(s7kFile, xsf);
			subgroups.add(grp);
		}
	}

	@Override
	public void fillData(S7KFile s7kFile) throws IOException, NCException {
		for (S7K1003IndividualConverter grp : subgroups) {
			grp.fillData(s7kFile);
		}
	}

	private class S7K1003IndividualConverter implements IDatagramConverter<S7KFile, XsfFromS7k> {

		private long key;
		private S7KIndividualSensorMetadata md;
		private List<ValueD1> valuesD1 = new LinkedList<>();

		public S7K1003IndividualConverter(ISounderFile dataProxy, long key, S7KIndividualSensorMetadata md) {
			this.key = key;
			this.md = md;
		}

		@Override
		public void declare(S7KFile s7kFile, XsfFromS7k xsf) throws NCException {
			PositionSubGroup positionGroup = xsf.addPositionGrp(String.format("1003_%03d", this.key),
					toIntExact(md.getTotalNumberOfEntries()));
			PositionSubGroupVendorSpecificGrp positionVendor = new PositionSubGroupVendorSpecificGrp(positionGroup, s7kFile,new HashMap<String, Integer>());

			// Generic data
			valuesD1.add(new ValueD1U8(positionGroup.getTime(), buffer -> new ULong(S7K1003.getTimestampNano(buffer))));
			valuesD1.add(new ValueD1F8(positionGroup.getLatitude(), buffer -> S7K1003.getLatitudeOrNorthing(buffer)));
			valuesD1.add(new ValueD1F8(positionGroup.getLongitude(), buffer -> S7K1003.getLongitudeOrEasting(buffer)));
			valuesD1.add(new ValueD1F4(positionGroup.getHeight_above_reference_ellipsoid(),
					buffer -> new FFloat((float) S7K1003.getHeight(buffer).get())));

			// vendor specific data
			valuesD1.add(new ValueD1U4(positionVendor.getDatum_identifier(), buffer -> S7K1003.getDatumIdentifier(buffer)));
			valuesD1.add(new ValueD1F4(positionVendor.getLatency(), buffer -> S7K1003.getLatency(buffer)));
			valuesD1.add(new ValueD1S1(positionVendor.getPosition_type(),
					buffer -> new SByte(S7K1003.getPositionTypeFlag(buffer).getByte())));
			valuesD1.add(new ValueD1U1(positionVendor.getUtm_zone(), buffer -> S7K1003.getUTMZone(buffer)));
			valuesD1.add(
					new ValueD1S1(positionVendor.getQuality_flag(), buffer -> new SByte(S7K1003.getQualityFlag(buffer).getByte())));
			valuesD1.add(new ValueD1S1(positionVendor.getPositioning_method(),
					buffer -> new SByte(S7K1003.getPositioningMethod(buffer).getByte())));
		}

		@Override
		public void fillData(S7KFile s7kFile) throws IOException, NCException {
			DatagramBuffer buffer = new DatagramBuffer();

			long i = 0;
			for (Entry<Long, DatagramPosition> posentry : this.md.getDatagrams()) {
				DatagramReader.readDatagram(posentry.getValue().getFile(), buffer, posentry.getValue().getSeek());

				for (ValueD1 v : this.valuesD1) {
					v.fill(buffer);
					v.write(new long[] { i });
				}
				i++;
			}
		}
	}
}
