package fr.ifremer.globe.api.xsf.converter.s7k.datagram;

import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7K1003InvidivualSensorMetadata;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7K1003Metadata;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KFile;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.DDouble;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.api.xsf.util.types.UByte;
import fr.ifremer.globe.api.xsf.util.types.UInt;

public class S7K1003 extends S7KBaseDatagram {

	@Override
	public void computeSpecificMetadata(S7KFile metadata, DatagramBuffer buffer, long type,
			DatagramPosition position, long time) {
		S7K1003Metadata md = getSpecificMetadata(metadata);
		S7K1003InvidivualSensorMetadata sensorMd = getSpecificMetadata(metadata).getEntry(getDeviceIdentifier(buffer).getU());
		if (sensorMd == null) {
			sensorMd = new S7K1003InvidivualSensorMetadata();
			md.addSensor(getDeviceIdentifier(buffer).getU(), sensorMd);
		}
		
		sensorMd.addDatagram(position, time, 1);
		sensorMd.registerPositionType(getPositionTypeFlag(buffer).getU());
		if (getPositionTypeFlag(buffer).getU() == 0) {
			metadata.getStats().getLat().register(Math.toDegrees(getLatitudeOrNorthing(buffer).get()));
			metadata.getStats().getLon().register(Math.toDegrees(getLongitudeOrEasting(buffer).get()));
		}
	}

	@Override
	public S7K1003Metadata getSpecificMetadata(S7KFile metadata) {
		return metadata.get1003Metadata();
	}
	
	public static UInt getDatumIdentifier(BaseDatagramBuffer buff) {
		return TypeDecoder.read4U(buff.byteBufferData, 0);
	}
	
	public static FFloat getLatency(BaseDatagramBuffer buff) {
		return TypeDecoder.read4F(buff.byteBufferData, 4);
	}
	
	public static DDouble getLatitudeOrNorthing(BaseDatagramBuffer buff) {
		return TypeDecoder.read8F(buff.byteBufferData, 8);
	}

	public static DDouble getLongitudeOrEasting(BaseDatagramBuffer buff) {
		return TypeDecoder.read8F(buff.byteBufferData, 16);
	}
	
	public static DDouble getHeight(BaseDatagramBuffer buff) {
		return TypeDecoder.read8F(buff.byteBufferData, 24);
	}
	
	public static UByte getPositionTypeFlag(BaseDatagramBuffer buff) {
		return TypeDecoder.read1U(buff.byteBufferData, 32);
	}
	
	public static UByte getUTMZone(BaseDatagramBuffer buff) {
		return TypeDecoder.read1U(buff.byteBufferData, 33);
	}
	
	public static UByte getQualityFlag(BaseDatagramBuffer buff) {
		return TypeDecoder.read1U(buff.byteBufferData, 34);
	}
	
	public static UByte getPositioningMethod(BaseDatagramBuffer buff) {
		return TypeDecoder.read1U(buff.byteBufferData, 35);
	}

}
