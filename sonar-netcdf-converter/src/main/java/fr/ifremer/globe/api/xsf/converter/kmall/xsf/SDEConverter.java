package fr.ifremer.globe.api.xsf.converter.kmall.xsf;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1F4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1F8;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1String;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U2;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U8;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfFromKongsberg;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.DepthSensorGrp;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.KmAllBaseDatagram;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.SDE;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.KmallFile;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.SDEMetadata;
import fr.ifremer.globe.api.xsf.util.SimpleIdentifier;
import fr.ifremer.globe.api.xsf.util.types.ULong;
import fr.ifremer.globe.netcdf.ucar.NCException;

public class SDEConverter implements IDatagramConverter<KmallFile, XsfFromKongsberg> {

	private List<ValueD1> valuesD1;
	private KmallFile stats;
	private SDEMetadata metadata;
	long nEntries;

	public SDEConverter(KmallFile stats) {
		this.stats = stats;
		this.metadata = this.stats.getSDEMetadata();
		this.nEntries = this.metadata.getTotalNumberOfEntries();
		this.valuesD1 = new LinkedList<>();
	}

	@Override
	public void declare(KmallFile allFile, XsfFromKongsberg xsf) throws NCException {
		DepthSensorGrp depthSensor = new DepthSensorGrp(xsf.getEnvironmentVendor(), allFile,new HashMap<String, Integer>());

		valuesD1.add(new ValueD1U8(depthSensor.getDatagram_time(),
				buffer -> new ULong(KmAllBaseDatagram.getEpochTimeNano(buffer.byteBufferData))));

		valuesD1.add(new ValueD1U2(depthSensor.getSensor_status(), buffer -> SDE.getSensorStatus(buffer.byteBufferData)));

		valuesD1.add(new ValueD1F4(depthSensor.getDepth_used(), buffer -> SDE.getDepthUsed_m(buffer.byteBufferData)));

		valuesD1.add(new ValueD1F4(depthSensor.getOffset(), buffer -> SDE.getOffset(buffer.byteBufferData)));

		valuesD1.add(new ValueD1F4(depthSensor.getScale(), buffer -> SDE.getScale(buffer.byteBufferData)));

		valuesD1.add(new ValueD1F8(depthSensor.getLatitude(), buffer -> SDE.getLatitude(buffer.byteBufferData)));

		valuesD1.add(new ValueD1F8(depthSensor.getLongitude(), buffer -> SDE.getLongitude(buffer.byteBufferData)));
		
		valuesD1.add(new ValueD1String(depthSensor.getData_received_from_sensor(), buffer -> SDE.getDataFromSensor(buffer.byteBufferData)));

	}

	@Override
	public void fillData(KmallFile kmAllFile) throws IOException, NCException {
		long[] originD1 = new long[] { 0 };

		DatagramBuffer buffer = new DatagramBuffer();
		for (Entry<SimpleIdentifier, DatagramPosition> posentry : metadata.getDatagrams()) {
			final DatagramPosition pos = posentry.getValue();
			DatagramReader.readDatagram(pos.getFile(), buffer, pos.getSeek());

			// read the data from the source file
			for (int i = 0; i < nEntries; i++) {
				for (ValueD1 value : this.valuesD1) {
					value.fill(buffer);
				}
			}
			// flush into the output file
			for (ValueD1 value : this.valuesD1) {
				value.write(originD1);
			}
			originD1[0] += 1;

		}

	}

}
