package fr.ifremer.globe.api.xsf.converter.all.datagram.runtime;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.all.datagram.IndividualSensorMetadata;
import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagram;
import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;

/**
 * */
public class RunTimeMetadata extends IndividualSensorMetadata {
	TreeMap<Long, BaseDatagramBuffer> temporalIndex = new TreeMap<>();
	List<DatagramPosition> allDatagrams = new ArrayList<>();

	@Override
	public void addDatagram(DatagramPosition position, long rawSeqNumber, int numEntries) {
		super.addDatagram(position, rawSeqNumber, numEntries);
		
		allDatagrams.add(position);
    }
    
	
	
	/**
	 * Reads and sorts datagrams from each sensor on a temporal basis
	 */
	public TreeMap<Long, BaseDatagramBuffer> getSortedDatagrams() throws IOException {
		if(temporalIndex.isEmpty())
		{
			for (DatagramPosition pos : allDatagrams) {

				DatagramBuffer bufferPosition = new DatagramBuffer(DatagramReader.getByteOrder(pos.getFile()));
				DatagramReader.readDatagramAt(pos.getFile(), bufferPosition, pos.getSeek());

				long time = BaseDatagram.getEpochTime(bufferPosition.byteBufferData);

				temporalIndex.put(time, bufferPosition);
			}
		}
		return temporalIndex;
	}
}
