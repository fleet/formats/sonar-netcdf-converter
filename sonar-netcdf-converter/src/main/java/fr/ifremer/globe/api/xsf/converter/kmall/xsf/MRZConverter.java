package fr.ifremer.globe.api.xsf.converter.kmall.xsf;

import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.datagram.KmPingModeMapper;
import fr.ifremer.globe.api.xsf.converter.common.datagram.KmPingModeMapper.KmallPingMode;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.*;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.*;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfFromKongsberg;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.BathymetryGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.BeamGroup1Grp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.BathymetryVendorSpecificGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.BeamGroup1VendorSpecificGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.types.DetectionTypeHelper;
import fr.ifremer.globe.api.xsf.converter.common.xsf.types.transmit_t;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.*;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.KmallFile;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.MRZCompletePingAntennaMetadata;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.MRZCompletePingMetadata;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.MRZMetadata;
import fr.ifremer.globe.api.xsf.util.DefaultLogger;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.types.*;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCVariable;
import fr.ifremer.globe.netcdf.util.Pair;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.mbes.MbesUtils;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

/**
 * Given the retult of indexing MRZ datagrams, this converter declares and
 * filles the associated variables in a HSF (netcdf) file.
 */
public class MRZConverter implements IDatagramConverter<KmallFile, XsfFromKongsberg> {

    private final KmallFile stats;
    private final MRZMetadata metadata;
    private final List<ValueD2> valuesRxAntenna = new LinkedList<>();
    private final List<ValueD2> valuesTxSector = new LinkedList<>();
    private final List<ValueD2> valuesRxInfo = new LinkedList<>();
    private final List<ValueD2> valuesEdClass = new LinkedList<>();
    private final List<ValueD2> valuesSwathBeam = new LinkedList<>();
    private final List<ValueD2F8WithPosition> valuesSwathBeamWithPosition = new LinkedList<>();
    private final List<ValueD2> valuesSwathED = new LinkedList<>();

    private final List<ValueD1> swathValues = new LinkedList<>();
    private final List<ValueD1F8WithPosition> swathValuesWithPosition = new LinkedList<>();


    // variables used to store seabed images (managed manually)
    private NCVariable seabedImageSamples;

    // vriabled used to store seabed images around extra detections (managed
    // manually)
    // private NCVariable edSeabedImageSamples;
    // private NCVariable edSeabedImageOffset;
    private BeamGroup1Grp beamGrp;
    private BathymetryGrp bathyGrp;

    public MRZConverter(KmallFile stats) {
        this.stats = stats;
        this.metadata = stats.getMRZMetadata();
    }

    /**
     * TODO Extra detection is not done, since it implies to define it in XSF format
     * first
     */
    /*
     * public void declareEd(NCGroup rootGrp,KmallVendorHelper vendorHelper) throws
     * NCException { NCGroup edGroup = rootGrp.addGroup("extra_detections"); NCGroup
     * edClassesGroup = edGroup.addGroup("classes"); NCGroup edRxGroup =
     * edGroup.addGroup("detections");
     *
     *
     *
     * NCDimension edClassesDim =
     * edClassesGroup.addDimension("extradetection_classes_dim",
     * metadata.getNumExtraDetectionClasses()); NCDimension edDim =
     * edRxGroup.addDimension("extra_detections_dim",
     * metadata.getMaxExtraDetectionsCount()); NCDimension sampleDim =
     * edRxGroup.addDimension("seabed_sample_dim",
     * metadata.getTotalExtraDetectionSampleCount());
     *
     * List<NCDimension> dimsSwathAntenna = Arrays.asList(swathDim, rxAntennaDim);
     * List<NCDimension> dimsEdClasses = Arrays.asList(swathDim, edClassesDim);
     * List<NCDimension> dimsEd = Arrays.asList(swathDim, edDim);
     *
     *
     * // extradetection related data
     *
     * NCVariable addVar = edGroup.addVariable("extra_detection_alarm_flag",
     * DataType.USHORT, dimsSwathAntenna, "extradetection alarm flag",
     * "extradetection alarm flag"); valuesRxInfo .add(new ValueD2U2(addVar,
     * (buffer, i) -> MRZ.getExtraDetectionAlarmFlag(buffer.byteBufferData, i)));
     *
     * addVar = edGroup.addVariable("num_extra_detections", DataType.USHORT,
     * dimsSwathAntenna, "number of extra detections",
     * "number of extra detections"); valuesRxInfo.add(new ValueD2U2(addVar,
     * (buffer, i) -> MRZ.getNumExtraDetections(buffer.byteBufferData, i)));
     *
     * addVar = edGroup.addVariable("num_extra_detection_classes", DataType.USHORT,
     * dimsSwathAntenna, "number of extra detection classes",
     * "number of extra detection classes"); valuesRxInfo .add(new ValueD2U2(addVar,
     * (buffer, i) -> MRZ.getNumExtraDetectionClasses(buffer.byteBufferData, i)));
     *
     * // extra det class info addVar =
     * edClassesGroup.addVariable("num_extra_det_in_class", DataType.USHORT,
     * dimsEdClasses, "num extra det in class",
     * "number of extra detections in class"); valuesEdClass.add(new
     * ValueD2U2(addVar, (buffer, i) ->
     * MRZ.getNumExtraDetInClass(buffer.byteBufferData, i)));
     *
     * List<Pair<Byte, String>> vals = new LinkedList<>(); vals.add(new Pair<Byte,
     * String>((byte) 0, "no alarm")); vals.add(new Pair<Byte, String>((byte) 1,
     * "alarm")); addVar = edClassesGroup.addEnumVariableByte("alarm_flag",
     * dimsEdClasses, vals, "alarm flag", "alarm flag"); valuesEdClass .add(new
     * ValueD2S1(addVar, (buffer, i) -> new
     * SByte(MRZ.getAlarmFlag(buffer.byteBufferData, i).get())));
     *
     * declareSounding(edRxGroup.addGroup("detections"), dimsEd,
     * valuesSwathED,vendorHelper); edSeabedImageOffset =
     * edRxGroup.addVariable("si_offset", DataType.LONG, dimsEd);
     * edSeabedImageSamples = edRxGroup.addVariable("si_samples", DataType.SHORT,
     * Arrays.asList(new NCDimension[] { sampleDim })); }
     *
     */
    private void declareSounding(XsfFromKongsberg xsf) throws NCException {
        BathymetryVendorSpecificGrp bathyVendorGrp = xsf.getBathy();

        /**********************************************************************************************************
         * xsf common variables
         **********************************************************************************************************/
        valuesSwathBeam.add(new ValueD2S2(bathyGrp.getDetection_rx_transducer_index(),
                (buffer, i) -> new SShort((short) stats.getIIPMetadata()
                        .getRxIndex(KmAllMDatagram.getRxTransducerInd(buffer.byteBufferData).getInt()))));
        valuesSwathBeam.add(new ValueD2S2(bathyGrp.getDetection_tx_transducer_index(),
                (buffer, i) -> new SShort((short) stats.getIIPMetadata()
                        .getTxIndex(KmAllMDatagram.getTxTransducerInd(buffer.byteBufferData).getInt()))));

        /** detection_x **/
        valuesSwathBeam.add(new ValueD2F4(bathyGrp.getDetection_x(),
                (buffer, i) -> MRZ.getX_reRefPoint_m(buffer.byteBufferData, i)));

        /** detection_y **/
        valuesSwathBeam.add(new ValueD2F4(bathyGrp.getDetection_y(),
                (buffer, i) -> MRZ.getY_reRefPoint_m(buffer.byteBufferData, i)));

        /** detection_z **/
        valuesSwathBeam.add(new ValueD2F4(bathyGrp.getDetection_z(),
                (buffer, i) -> MRZ.getZ_reRefPoint_m(buffer.byteBufferData, i)));

        /** detection_two_way_travel_time **/
        valuesSwathBeam.add(new ValueD2F4(bathyGrp.getDetection_two_way_travel_time(),
                (buffer, i) -> MRZ.getTwoWayTravelTime_sec(buffer.byteBufferData, i)));

        /** detection_time_varying_gain **/
        valuesSwathBeam.add(new ValueD2F4(bathyGrp.getDetection_time_varying_gain(),
                (buffer, i) -> MRZ.getTVG_dB(buffer.byteBufferData, i)));

        /** detection_latitude **/
        valuesSwathBeamWithPosition.add(new ValueD2F8WithPosition(bathyGrp.getDetection_latitude(),
                (buffer, i, latNav, lonNav, speed, positionMetadata) -> new DDouble(latNav + MRZ.getDeltaLatitude_deg(buffer.byteBufferData, i).get())));

        /** detection_longitude **/
        valuesSwathBeamWithPosition.add(new ValueD2F8WithPosition(bathyGrp.getDetection_longitude(),
                (buffer, i, latNav, lonNav, speed, positionMetadata) -> new DDouble(lonNav + MRZ.getDeltaLongitude_deg(buffer.byteBufferData, i).get())));


        /** detection_reflectivity **/
        valuesSwathBeam.add(new ValueD2F4(bathyGrp.getDetection_backscatter_r(),
                (buffer, i) -> MRZ.getReflectivity1_dB(buffer.byteBufferData, i)));

        /** status **/
        valuesSwathBeam.add(new ValueD2S1(bathyGrp.getStatus(), (buffer, i) -> {
            byte detectionMethodByte = MRZ.getDetectionMethod(buffer.byteBufferData, i).getByte();
            return new SByte((byte) (detectionMethodByte == 0 ? 2 : 0));
        }));

        /** status detail */
        valuesSwathBeam.add(new ValueD2S1(bathyGrp.getStatus_detail(), (buffer, i) -> {
            byte detectionMethodByte = MRZ.getDetectionMethod(buffer.byteBufferData, i).getByte();
            return new SByte((byte) (detectionMethodByte == 0 ? 2 : 0));
        }));

        valuesSwathBeam.add(new ValueD2U2(bathyVendorGrp.getSounding_index(),
                (buffer, i) -> MRZ.getSoundingIndex(buffer.byteBufferData, i)));

        valuesSwathBeam.add(new ValueD2U1(bathyGrp.getDetection_tx_beam(),
                (buffer, i) -> MRZ.getCorrTxSectorNumb(buffer.byteBufferData, i)));

        valuesSwathBeam.add(new ValueD2S1(bathyVendorGrp.getDetection_type_raw(),
                (buffer, i) -> new SByte(MRZ.getDetectionType(buffer.byteBufferData, i).getByte())));

        valuesSwathBeam.add(new ValueD2S1(bathyGrp.getDetection_type(), (buffer, i) -> {
            byte value = DetectionTypeHelper.INVALID.get();
            byte kValue = MRZ.getDetectionMethod(buffer.byteBufferData, i).getByte();
            if (kValue == 1) {
                value = DetectionTypeHelper.AMPLITUDE.get();
            } else if (kValue == 2) {
                value = DetectionTypeHelper.PHASE.get();
            }
            return new SByte(value);
        }));
        /**********************************************************************************************************
         * .kmall specific variables
         **********************************************************************************************************/

        valuesSwathBeam.add(new ValueD2U1(bathyVendorGrp.getRejection_info_1(),
                (buffer, i) -> MRZ.getRejectionInfo1(buffer.byteBufferData, i)));

        valuesSwathBeam.add(new ValueD2U1(bathyVendorGrp.getRejection_info_2(),
                (buffer, i) -> MRZ.getRejectionInfo2(buffer.byteBufferData, i)));

        valuesSwathBeam.add(new ValueD2U1(bathyVendorGrp.getDetection_class(),
                (buffer, i) -> MRZ.getDetectionClass(buffer.byteBufferData, i)));

        valuesSwathBeam.add(new ValueD2U1(bathyVendorGrp.getDetection_confidence_level(),
                (buffer, i) -> MRZ.getDetectionConfidenceLevel(buffer.byteBufferData, i)));

        valuesSwathBeam.add(new ValueD2F4(bathyGrp.getDetection_quality_factor(),
                (buffer, i) -> MRZ.getQualityFactor(buffer.byteBufferData, i)));

        valuesSwathBeam.add(new ValueD2F4(bathyGrp.getDetection_receiver_sensitivity_applied(),
                (buffer, i) -> MRZ.getReceiverSensitivityApplied_dB(buffer.byteBufferData, i)));

        valuesSwathBeam.add(new ValueD2F4(bathyVendorGrp.getRange_factor(),
                (buffer, i) -> MRZ.getRangeFactor(buffer.byteBufferData, i)));

        valuesSwathBeam.add(new ValueD2F4(bathyVendorGrp.getDetection_uncertainty_ver(),
                (buffer, i) -> MRZ.getDetectionUncertaintyVer_m(buffer.byteBufferData, i)));

        valuesSwathBeam.add(new ValueD2F4(bathyVendorGrp.getDetection_uncertainty_hor(),
                (buffer, i) -> MRZ.getDetectionUncertaintyHor_m(buffer.byteBufferData, i)));

        valuesSwathBeam.add(new ValueD2F4(bathyVendorGrp.getDetection_window_length(),
                (buffer, i) -> MRZ.getDetectionWindowLength_sec(buffer.byteBufferData, i)));

        valuesSwathBeam.add(new ValueD2F4(bathyVendorGrp.getEcho_length(),
                (buffer, i) -> MRZ.getEchoLength_sec(buffer.byteBufferData, i)));

        valuesSwathBeam.add(new ValueD2U2(bathyVendorGrp.getWater_column_beam_number(),
                (buffer, i) -> MRZ.getWCBeamNumb(buffer.byteBufferData, i)));

        valuesSwathBeam.add(new ValueD2U2(bathyVendorGrp.getWater_column_range_samples(),
                (buffer, i) -> MRZ.getWCrange_samples(buffer.byteBufferData, i)));

        valuesSwathBeam.add(new ValueD2F4(bathyVendorGrp.getWater_column_beam_angle_across(),
                (buffer, i) -> MRZ.getWCNomBeamAngleAcross_deg(buffer.byteBufferData, i)));

        valuesSwathBeam.add(new ValueD2F4(bathyGrp.getDetection_mean_absorption_coefficient(),
                (buffer, i) -> MRZ.getMeanAbsCoeff_dBPerkm(buffer.byteBufferData, i)));

        valuesSwathBeam.add(new ValueD2F4(bathyVendorGrp.getReflectivity1(),
                (buffer, i) -> MRZ.getReflectivity1_dB(buffer.byteBufferData, i)));

        valuesSwathBeam.add(new ValueD2F4(bathyVendorGrp.getReflectivity2(),
                (buffer, i) -> MRZ.getReflectivity2_dB(buffer.byteBufferData, i)));

        valuesSwathBeam.add(new ValueD2F4(bathyGrp.getDetection_source_level_applied(),
                (buffer, i) -> MRZ.getSourceLevelApplied_dB(buffer.byteBufferData, i)));

        valuesSwathBeam.add(new ValueD2F4(bathyGrp.getDetection_backscatter_calibration(),
                (buffer, i) -> MRZ.getBScalibration_dB(buffer.byteBufferData, i)));

        valuesSwathBeam.add(new ValueD2F4(bathyGrp.getDetection_beam_pointing_angle(),
                (buffer, i) -> MRZ.getBeamAngleReRx_deg(buffer.byteBufferData, i)));

        valuesSwathBeam.add(new ValueD2F4(bathyVendorGrp.getBeam_angle_correction(),
                (buffer, i) -> MRZ.getBeamAngleCorrection_deg(buffer.byteBufferData, i)));

        valuesSwathBeam.add(new ValueD2F4(bathyVendorGrp.getDetection_two_way_travel_time_correction(),
                (buffer, i) -> MRZ.getTwoWayTravelTimeCorrection_sec(buffer.byteBufferData, i)));

        valuesSwathBeam.add(new ValueD2F4(bathyVendorGrp.getBeam_incidence_angle_adjustment(),
                (buffer, i) -> MRZ.getBeamIncAngleAdj_deg(buffer.byteBufferData, i)));

        // marked as not used
        // valuesSwathBeam.add(new ValueD2U2(v.getDetectionRealTimeCleaningInfo(),
        // (buffer, i) ->
        // MRZ.getRealTimeCleanInfo(buffer.byteBufferData, i)));

        valuesSwathBeam.add(new ValueD2U2(bathyGrp.getSeabed_image_start_range(),
                (buffer, i) -> MRZ.getSIstartRange_samples(buffer.byteBufferData, i)));

        valuesSwathBeam.add(new ValueD2U2(bathyGrp.getSeabed_image_center(),
                (buffer, i) -> MRZ.getSIcentreSample(buffer.byteBufferData, i)));

    }

    /**
     * Compute and return an array containing the start and stop frequency
     */
    public FFloat[] computeFrequenciesStartAndStop(BaseDatagramBuffer buffer, int i) {
        float fq = MRZ.getCentreFreq_Hz(buffer.byteBufferData, i).get();
        int waveForm = MRZ.getSignalWaveForm(buffer.byteBufferData, i).getU(); // 0:CW, 1:FM up 2:FM down
        float bandwidth = MRZ.getSignalBandWidth_Hz(buffer.byteBufferData, i).get();
        if (waveForm == 0) // CW
        {
            FFloat[] ret = {new FFloat(fq), new FFloat(fq)};
            return ret;
        } else if (waveForm == 1) {
            FFloat[] ret = {new FFloat(fq - bandwidth), new FFloat(fq + bandwidth)};
            return ret;

        } else {
            FFloat[] ret = {new FFloat(fq + bandwidth), new FFloat(fq - bandwidth)};
            return ret;
        }
    }

    private SByte toTransmitType(BaseDatagramBuffer buffer, int i) {
        short type = MRZ.getSignalWaveForm(buffer.byteBufferData, i).getU();
        if (type == 0) {
            return new SByte(transmit_t.CW.getValue());
        } else {
            return new SByte(transmit_t.LFM.getValue());
        }
    }

    @Override
    public void declare(KmallFile allFile, XsfFromKongsberg xsf) throws NCException {

        beamGrp = xsf.getBeamGroup();
        bathyGrp = xsf.getBathyGrp();
        BeamGroup1VendorSpecificGrp beam_group_vendor = xsf.getBeamGroupVendorSpecific();
        BathymetryVendorSpecificGrp bathyVendorGrp = xsf.getBathy();

        /**********************************************************************************************************
         * xsf common variables
         **********************************************************************************************************/

        swathValues.add(new ValueD1F4(beamGrp.getSound_speed_at_transducer(),
                buffer -> MRZ.getSoundSpeedAtTxDepth_mPerSec(buffer.byteBufferData)));

        swathValues.add(new ValueD1F4(beamGrp.getTx_transducer_depth(),
                buffer -> MRZ.getTxTransducerDepth_m(buffer.byteBufferData)));

        // latitude
        swathValuesWithPosition.add(new ValueD1F8WithPosition(beamGrp.getPlatform_latitude(),
                (buffer, lat, lon, speed) -> new DDouble(lat)));

        // longitude
        swathValuesWithPosition.add(new ValueD1F8WithPosition(beamGrp.getPlatform_longitude(),
                (buffer, lat, lon, speed) -> new DDouble(lon)));

        // heading
        swathValues.add(new ValueD1F4(beamGrp.getPlatform_heading(),
                buffer -> MRZ.getHeadingVessel_deg(buffer.byteBufferData)));

        // heave
        swathValues.add(new ValueD1F4(beamGrp.getPlatform_vertical_offset(),
                buffer -> MRZ.getZ_waterLevelReRefPoint_m(buffer.byteBufferData)));

        // time
        swathValues.add(new ValueD1U8(beamGrp.getPing_time(),
                buffer -> new ULong(KmAllBaseDatagram.getEpochTimeNano(buffer.byteBufferData))));

        /**********************************************************************************************************
         * .kmall specific variables
         **********************************************************************************************************/

        valuesRxAntenna.add(new ValueD2U1(beam_group_vendor.getSystem_id(),
                (buffer, i) -> KmAllBaseDatagram.getSystemId(buffer.byteBufferData)));

        valuesRxAntenna.add(new ValueD2U2(beam_group_vendor.getEcho_sounder_id(),
                (buffer, i) -> KmAllBaseDatagram.getEchoSounderId(buffer.byteBufferData)));

        swathValues.add(new ValueD1U4(beam_group_vendor.getPing_raw_count(),
                (buffer) -> new UInt(KmAllMDatagram.getPingCnt(buffer.byteBufferData))));

        valuesRxAntenna.add(new ValueD2U1(beam_group_vendor.getRx_fans_per_ping(),
                (buffer, i) -> KmAllMDatagram.getRxFansPerPing(buffer.byteBufferData)));

        valuesRxAntenna.add(new ValueD2U2(beam_group_vendor.getRx_fan_index(),
                (buffer, i) -> new UShort((short) KmAllMDatagram.getRxFanIndex(buffer.byteBufferData))));

        swathValues.add(new ValueD1U1(beam_group_vendor.getSwath_per_ping(),
                (buffer) -> KmAllMDatagram.getSwathPerPing(buffer.byteBufferData)));

        swathValues.add(new ValueD1U1(bathyGrp.getMultiping_sequence(),
                (buffer) -> KmAllMDatagram.getSwathAlongPosition(buffer.byteBufferData)));

        valuesRxAntenna.add(new ValueD2U1(beam_group_vendor.getNum_rx_transducers(),
                (buffer, i) -> KmAllMDatagram.getNumRxTransducers(buffer.byteBufferData)));

        valuesRxAntenna.add(new ValueD2U1(beam_group_vendor.getAlgorithm_type(),
                (buffer, i) -> KmAllMDatagram.getAlgorithmType(buffer.byteBufferData)));

        // Ping info
        valuesRxAntenna.add(new ValueD2F4(beam_group_vendor.getPing_rate(),
                (buffer, i) -> MRZ.getPingRate_Hz(buffer.byteBufferData)));

        valuesRxAntenna.add(new ValueD2S1(beam_group_vendor.getBeam_spacing(),
                (buffer, i) -> new SByte(MRZ.getBeamSpacing(buffer.byteBufferData).getByte())));

        valuesRxAntenna.add(new ValueD2S1(beam_group_vendor.getDepth_mode(), (buffer, i) -> {
            try {
                return new SByte((byte) (KmPingModeMapper
                        .fromkmall(KmallPingMode.fromValue(MRZ.getDepthMode(buffer.byteBufferData).getByte())).value));
            } catch (GIOException e) {
                return (new SByte((byte) KmPingModeMapper.XsfPingMode.UNKNOWN.value));
            }
        }));

        valuesRxAntenna.add(new ValueD2U1(beam_group_vendor.getSub_depth_mode(),
                (buffer, i) -> MRZ.getSubDepthMode(buffer.byteBufferData)));

        valuesRxAntenna.add(new ValueD2U1(beam_group_vendor.getDistance_between_swaths(),
                (buffer, i) -> MRZ.getDistanceBtwSwath(buffer.byteBufferData)));

        valuesRxAntenna.add(new ValueD2S1(beam_group_vendor.getDetection_mode(),
                (buffer, i) -> new SByte(MRZ.getDetectionMode(buffer.byteBufferData).getByte())));

        valuesRxAntenna.add(new ValueD2S1(beam_group_vendor.getPulse_form(),
                (buffer, i) -> new SByte(MRZ.getPulseForm(buffer.byteBufferData).getByte())));

        valuesRxAntenna.add(new ValueD2F4(beam_group_vendor.getFrequency_mode(),
                (buffer, i) -> MRZ.getFrequencyMode_Hz(buffer.byteBufferData)));

        valuesRxAntenna.add(new ValueD2F4(beam_group_vendor.getFrequency_range_low_lim(),
                (buffer, i) -> MRZ.getFreqRangeLowLim_Hz(buffer.byteBufferData)));

        valuesRxAntenna.add(new ValueD2F4(beam_group_vendor.getFrequency_range_high_lim(),
                (buffer, i) -> MRZ.getFreqRangeHighLim_Hz(buffer.byteBufferData)));

        valuesRxAntenna.add(new ValueD2F4(beam_group_vendor.getMax_total_tx_pulse_length(),
                (buffer, i) -> MRZ.getMaxTotalTxPulseLength_sec(buffer.byteBufferData)));

        valuesRxAntenna.add(new ValueD2F4(beam_group_vendor.getMax_effective_tx_pulse_length(),
                (buffer, i) -> MRZ.getMaxEffTxPulseLength_sec(buffer.byteBufferData)));

        valuesRxAntenna.add(new ValueD2F4(beam_group_vendor.getMax_effective_tx_bandwidth(),
                (buffer, i) -> MRZ.getMaxEffTxBandWidth_Hz(buffer.byteBufferData)));

        valuesRxAntenna.add(new ValueD2F4(bathyGrp.getDetection_mean_absorption_coefficient(),
                (buffer, i) -> MRZ.getAbsCoeff_dBPerkm(buffer.byteBufferData)));

        valuesRxAntenna.add(new ValueD2F4(beam_group_vendor.getPort_sector_edge(),
                (buffer, i) -> MRZ.getPortSectorEdge_deg(buffer.byteBufferData)));

        valuesRxAntenna.add(new ValueD2F4(beam_group_vendor.getStarboard_sector_edge(),
                (buffer, i) -> MRZ.getStarbSectorEdge_deg(buffer.byteBufferData)));

        valuesRxAntenna.add(new ValueD2F4(beam_group_vendor.getPort_mean_coverage_deg(),
                (buffer, i) -> MRZ.getPortMeanCov_deg(buffer.byteBufferData)));

        valuesRxAntenna.add(new ValueD2F4(beam_group_vendor.getStarboard_mean_coverage_deg(),
                (buffer, i) -> MRZ.getStarbMeanCov_deg(buffer.byteBufferData)));

        valuesRxAntenna.add(new ValueD2S2(beam_group_vendor.getPort_mean_coverage_m(),
                (buffer, i) -> MRZ.getPortMeanCov_m(buffer.byteBufferData)));

        valuesRxAntenna.add(new ValueD2S2(beam_group_vendor.getStarboard_mean_coverage_m(),
                (buffer, i) -> MRZ.getStarbMeanCov_m(buffer.byteBufferData)));

        valuesRxAntenna.add(new ValueD2U1(beam_group_vendor.getMode_and_stabilisation(),
                (buffer, i) -> MRZ.getModeAndStabilisation(buffer.byteBufferData)));

        valuesRxAntenna.add(new ValueD2U1(beam_group_vendor.getRuntime_filter_1(),
                (buffer, i) -> MRZ.getRuntimeFilter1(buffer.byteBufferData)));

        valuesRxAntenna.add(new ValueD2U2(beam_group_vendor.getRuntime_filter_2(),
                (buffer, i) -> MRZ.getRuntimeFilter2(buffer.byteBufferData)));

        valuesRxAntenna.add(new ValueD2U4(beam_group_vendor.getPipe_tracking_status(),
                (buffer, i) -> MRZ.getPipeTrackingStatus(buffer.byteBufferData)));

        valuesRxAntenna.add(new ValueD2F4(beam_group_vendor.getTransmit_array_size_used(),
                (buffer, i) -> MRZ.getTransmitArraySizeUsed_deg(buffer.byteBufferData)));

        valuesRxAntenna.add(new ValueD2F4(beam_group_vendor.getReceive_array_size_used(),
                (buffer, i) -> MRZ.getReceiveArraySizeUsed_deg(buffer.byteBufferData)));

        valuesRxAntenna.add(new ValueD2F4(beam_group_vendor.getTransmit_power(),
                (buffer, i) -> MRZ.getTransmitPower_dB(buffer.byteBufferData)));

        valuesRxAntenna.add(new ValueD2U2(beam_group_vendor.getSl_ramp_up_time_remaining(),
                (buffer, i) -> MRZ.getSLrampUpTimeRemaining(buffer.byteBufferData)));

        valuesRxAntenna.add(new ValueD2F4(beam_group_vendor.getYaw_angle(),
                (buffer, i) -> MRZ.getYawAngle_deg(buffer.byteBufferData)));

        valuesRxAntenna.add(new ValueD2U2(beam_group_vendor.getTx_sector_count(),
                (buffer, i) -> MRZ.getNumTxSectors(buffer.byteBufferData)));

        valuesRxAntenna.add(new ValueD2F4(beam_group_vendor.getX_kmallToall_m(),
                (buffer, i) -> MRZ.getX_kmallToall_m((buffer.byteBufferData))));

        valuesRxAntenna.add(new ValueD2F4(beam_group_vendor.getY_kmallToall_m(),
                (buffer, i) -> MRZ.getY_kmallToall_m(buffer.byteBufferData)));

        valuesRxAntenna.add(new ValueD2S1(beam_group_vendor.getLat_long_info(),
                (buffer, i) -> new SByte(MRZ.getLatLongInfo(buffer.byteBufferData).getByte())));

        valuesRxAntenna.add(new ValueD2S1(beam_group_vendor.getPosition_sensor_status(),
                (buffer, i) -> new SByte(MRZ.getPosSensorStatus(buffer.byteBufferData).getByte())));

        valuesRxAntenna.add(new ValueD2U1(beam_group_vendor.getAttitude_sensor_status(),
                (buffer, i) -> MRZ.getAttitudeSensorStatus(buffer.byteBufferData)));

        valuesRxAntenna.add(new ValueD2F4(beam_group_vendor.getEllipsoid_height_re_ref_point(),
                (buffer, i) -> MRZ.getEllipsoidHeightReRefPoint_m(buffer.byteBufferData)));

        if (metadata.getMrzVersion() >= 1) {
            valuesRxAntenna.add(new ValueD2F4(beam_group_vendor.getBsCorrectionOffset_dB(),
                    (buffer, i) -> MRZ.getBsCorrectionOffset_dB(buffer.byteBufferData)));
            valuesRxAntenna.add(new ValueD2U1(beam_group_vendor.getLambertsLawApplied(),
                    (buffer, i) -> MRZ.getLambertsLawApplied(buffer.byteBufferData)));
            valuesRxAntenna.add(new ValueD2U1(beam_group_vendor.getIceWindow(),
                    (buffer, i) -> MRZ.getIceWindow(buffer.byteBufferData)));
        }
        if (metadata.getMrzVersion() >= 2) {
            valuesRxAntenna.add(new ValueD2U1(beam_group_vendor.getActiveModes(),
                    (buffer, i) -> new UByte((byte) MRZ.getActiveModes(buffer.byteBufferData).getU())));
        }

        // TX sector info

        valuesTxSector.add(new ValueD2U2(beam_group_vendor.getTx_sector_number(),
                (buffer, i) -> new UShort(MRZ.getTxSectorNumb(buffer.byteBufferData, i).getU())));

        valuesTxSector.add(new ValueD2U1(beam_group_vendor.getTx_array_number(),
                (buffer, i) -> MRZ.getTxArrNumber(buffer.byteBufferData, i)));

        valuesTxSector.add(new ValueD2S4(beamGrp.getTransmit_transducer_index(),
                (buffer, i) -> new SInt(allFile.getIIPMetadata()
                        .getTxIndex(MRZ.getTxArrNumber(buffer.byteBufferData, i).getU()))));

        valuesTxSector.add(new ValueD2U1(beam_group_vendor.getTx_sub_array(),
                (buffer, i) -> MRZ.getTxSubArray(buffer.byteBufferData, i)));

        valuesTxSector.add(new ValueD2F4(beam_group_vendor.getTransmit_time_delay(),
                (buffer, i) -> MRZ.getSectorTransmitDelay_sec(buffer.byteBufferData, i)));

        valuesTxSector.add(new ValueD2F4(beam_group_vendor.getRaw_tx_beam_tilt_angle(), (buffer, i) -> {
            double angleDeg = MRZ.getTiltAngleReTx_deg(buffer.byteBufferData, i).get();

            return new FFloat((float) angleDeg);
        }));

        valuesTxSector.add(new ValueD2F4(beam_group_vendor.getTx_nominal_source_level(),
                (buffer, i) -> MRZ.getTxNominalSourceLevel_dB(buffer.byteBufferData, i)));

        valuesTxSector.add(new ValueD2F4(beam_group_vendor.getTx_focus_range(),
                (buffer, i) -> MRZ.getTxFocusRange_m(buffer.byteBufferData, i)));

        valuesTxSector.add(new ValueD2F4(beam_group_vendor.getCenter_frequency(),
                (buffer, i) -> MRZ.getCentreFreq_Hz(buffer.byteBufferData, i)));

        valuesTxSector.add(new ValueD2F4(beamGrp.getTransmit_frequency_start(),
                (buffer, i) -> computeFrequenciesStartAndStop(buffer, i)[0]));
        valuesTxSector.add(new ValueD2F4(beamGrp.getTransmit_frequency_stop(),
                (buffer, i) -> computeFrequenciesStartAndStop(buffer, i)[1]));

        valuesTxSector.add(new ValueD2F4(beamGrp.getTransmit_bandwidth(),
                (buffer, i) -> MRZ.getSignalBandWidth_Hz(buffer.byteBufferData, i)));

        valuesTxSector.add(new ValueD2F4(beamGrp.getTransmit_duration_nominal(), (buffer, i) -> {
            float bdw = MRZ.getSignalBandWidth_Hz(buffer.byteBufferData, i).get();
            float signal_length_nominal = 0;
            if (bdw != 0)
                signal_length_nominal = 1f / bdw;
            else
                signal_length_nominal = MRZ.getTotalSignalLength_sec(buffer.byteBufferData, i).get();
            return new FFloat(signal_length_nominal);
        }));

        valuesTxSector.add(new ValueD2F4(beam_group_vendor.getSignal_length(),
                (buffer, i) -> MRZ.getTotalSignalLength_sec(buffer.byteBufferData, i)));

        valuesTxSector.add(new ValueD2U1(beam_group_vendor.getPulse_shading(),
                (buffer, i) -> MRZ.getPulseShading(buffer.byteBufferData, i)));

        valuesTxSector.add(new ValueD2U1(beam_group_vendor.getSignal_waveform(),
                (buffer, i) -> MRZ.getSignalWaveForm(buffer.byteBufferData, i)));

        if (metadata.getMrzVersion() >= 1) {
            valuesTxSector.add(new ValueD2F4(beam_group_vendor.getHighVoltageLevel_dB(),
                    (buffer, i) -> MRZ.gethighVoltageLevel_dB(buffer.byteBufferData, i)));
            valuesTxSector.add(new ValueD2F4(beam_group_vendor.getSectorTrackingCorr_dB(),
                    (buffer, i) -> MRZ.getSectorTrackingCorr_dB(buffer.byteBufferData, i)));
            valuesTxSector.add(new ValueD2F4(beam_group_vendor.getEffectiveSignalLength_sec(),
                    (buffer, i) -> MRZ.getEffectiveSignalLength_sec(buffer.byteBufferData, i)));

        }

        valuesTxSector.add(new ValueD2S1(beamGrp.getTransmit_type(), (buffer, i) -> toTransmitType(buffer, i)));

        // rx info
        valuesRxInfo.add(new ValueD2U2(beam_group_vendor.getNum_sounding_max_main(),
                (buffer, i) -> MRZ.getNumSoundingsMaxMain(buffer.byteBufferData, i)));

        valuesRxInfo.add(new ValueD2U2(beam_group_vendor.getNum_soundings_valid_main(),
                (buffer, i) -> MRZ.getNumSoundingsValidMain(buffer.byteBufferData, i)));

        valuesRxInfo.add(new ValueD2F4(beam_group_vendor.getWc_sample_rate(),
                (buffer, i) -> MRZ.getWCSampleRate(buffer.byteBufferData, i)));

        valuesRxInfo.add(new ValueD2F4(beam_group_vendor.getSeabed_image_sample_rate(),
                (buffer, i) -> MRZ.getSeabedImageSampleRate(buffer.byteBufferData, i)));

        valuesRxInfo.add(new ValueD2F4(bathyVendorGrp.getBackscatter_normal_incidence_level(),
                (buffer, i) -> MRZ.getBSnormal_dB(buffer.byteBufferData, i)));

        valuesRxInfo.add(new ValueD2F4(bathyVendorGrp.getBackscatter_oblique_incidence_level(),
                (buffer, i) -> MRZ.getBSoblique_dB(buffer.byteBufferData, i)));

        declareSounding(xsf);
        seabedImageSamples = bathyGrp.getSeabed_image_samples_r();

        /*
         * if (metadata.getNumExtraDetectionClasses() > 0) {
         * declareEd(rootGrp,vendorHelper); }
         */
    }

    @Override
    public void fillData(KmallFile kmAllFile) throws IOException, NCException {

        long[] origin2D = {0, 0};
        // long si_origin[] = { 0 };
        long[] ed_si_origin = {0};
        final DatagramBuffer buffer = new DatagramBuffer();
        int detectionCount = stats.getMRZMetadata().getMaxBeamCount();
        int sampleCount = stats.getMRZMetadata().getMaxAbstractSampleCount();
        int[] rxAntennas = stats.getRxAntennas().stream().mapToInt(Integer::intValue).toArray();
        int activePosition = beamGrp.getAttributeInt(BeamGroup1Grp.ATT_PREFERRED_POSITION);
        // check if motion correction is already applied
        boolean motionCorrected = kmAllFile.getIIPMetadata().getPositions().get(activePosition).isMotionCorrected();

        for (Entry<SwathIdentifier, MRZCompletePingMetadata> pingEntry : metadata.getEntries()) {
            valuesRxAntenna.forEach(ValueD2::clear);
            valuesTxSector.forEach(ValueD2::clear);
            valuesRxInfo.forEach(ValueD2::clear);
            valuesEdClass.forEach(ValueD2::clear);
            valuesSwathBeam.forEach(ValueD2::clear);
            valuesSwathED.forEach(ValueD2::clear);
            valuesSwathBeamWithPosition.forEach(ValueD2F8WithPosition::clear);

            final MRZCompletePingMetadata ping = pingEntry.getValue();
            // Check if this datagram should be rejected
            if (!ping.isComplete(stats.getRxAntennas()) || !stats.getSwathIds().contains(pingEntry.getKey())) {
                continue;
            }
            // use same antenna order for all pings
            ping.setSerialNumbers(rxAntennas);

            // get origin from swath identifier (ignore if matching index not found)
            var optSwathIndex = stats.getSwathIds().getIndex(pingEntry.getKey());
            if (optSwathIndex.isEmpty()) {
                DefaultLogger.get().warn(
                        "Datagram MRZ associated with a missing ping is ignored (number = {}, sequence = {}).",
                        pingEntry.getKey().getRawPingNumber(), pingEntry.getKey().getSwathPosition());
                continue;
            }
            origin2D[0] = optSwathIndex.get();
            short[] sampleBuffer = new short[sampleCount * detectionCount];
            Arrays.fill(sampleBuffer, seabedImageSamples.getShortFillValue());

            for (Pair<DatagramPosition, MRZCompletePingAntennaMetadata> antenna : ping.locate()) {
                DatagramReader.readDatagram(antenna.getFirst().getFile(), buffer, antenna.getFirst().getSeek());

                // Get motion corrected position
                double latitude = MRZ.getLatitude_deg(buffer.byteBufferData).get();
                double longitude = MRZ.getLongitude_deg(buffer.byteBufferData).get();
                double[] refPointLatLong = {latitude, longitude};
                if (!motionCorrected) {
                    float heading = MRZ.getHeadingVessel_deg(buffer.byteBufferData).get();
                    double sensorAlong = -MRZ.getX_kmallToall_m(buffer.byteBufferData).get();
                    double sensorAcross = -MRZ.getY_kmallToall_m(buffer.byteBufferData).get();
                    refPointLatLong = MbesUtils.acrossAlongToWGS84LatLong(latitude, longitude, heading, sensorAcross, sensorAlong);
                }

                // Swath values
                for (ValueD1F8WithPosition v : swathValuesWithPosition)
                    v.fill(buffer, refPointLatLong[0], refPointLatLong[1], 0.0);

                for (ValueD1 v : swathValues)
                    v.fill(buffer);

                for (ValueD2 v : valuesRxAntenna)
                    v.fill(buffer, 0, MRZ.getRxTransducerInd(buffer.byteBufferData).getU());

                // handle tx data
                for (int i = 0; i < MRZ.getNumTxSectors(buffer.byteBufferData).getU(); i++) {
                    for (ValueD2 v : valuesTxSector)
                        v.fill(buffer, i, i);
                }

                // RX info data
                int offset = MRZ.getRxInfoOffset(buffer.byteBufferData);
                for (ValueD2 v : valuesRxInfo) {
                    v.fill(buffer, offset, MRZ.getRxTransducerInd(buffer.byteBufferData).getU());
                }

                // ED classes data
                for (int i = 0; i < MRZ.getNumExtraDetectionClasses(buffer.byteBufferData, offset).getU(); i++) {
                    offset = MRZ.getExtraDetClassInfoOffset(buffer.byteBufferData, i);
                    for (ValueD2 v : valuesEdClass) {
                        v.fill(buffer, offset, i);
                    }
                }

                // load all the seabed image to be able to store parts of it in the bathy group
                // and the other part in the ED group.
                final short[] si = MRZ.getSIsample_desidB(buffer.byteBufferData);
                int si_offset = 0;

                // sounding data
                // both main and extra detection are stored in the same block. We defferenciate
                // the way the are handled to store data in different groups.
                offset = MRZ.getRxInfoOffset(buffer.byteBufferData);
                int numSounding = (MRZ.getNumSoundingsMaxMain(buffer.byteBufferData, offset).getU()
                        + MRZ.getNumExtraDetections(buffer.byteBufferData, offset).getU());

                int maxDetection = MRZ.getNumSoundingsMaxMain(buffer.byteBufferData, offset).getU();
                int validDetection = MRZ.getNumSoundingsValidMain(buffer.byteBufferData, offset).getU();
                int invalidDetectionInPingCount = maxDetection - validDetection;

                offset = MRZ.getSoundingOffset(buffer.byteBufferData, offset);
                int soundingIndex = ping.getBeamOffset(MRZ.getRxTransducerInd(buffer.byteBufferData).getU());

                int extraDetection = 0;
                int validCount = 0;
                int invalidCount = 0;
                for (int i = 0; i < numSounding; i++) {
                    int numSI = MRZ.getSInumSamples(buffer.byteBufferData, offset).getU();

                    short detectionType = MRZ.getDetectionType(buffer.byteBufferData, offset).getU();
                    if (detectionType == 1) {
                        // Valid Extra detection
                        // WE SKIP EXTRA DETECTION
                        /*
                         * for (ValueD2 v : valuesSwathED) { v.fill(buffer, offset, edIndex); }
                         *
                         * edSeabedImageOffset.put(new long[] { origin[0], edIndex }, new long[] { 1, 1
                         * }, ed_si_origin); short[] dat = new short[numSI]; System.arraycopy(si,
                         * si_offset, dat, 0, numSI); edSeabedImageSamples.put(ed_si_origin, new long[]
                         * { numSI }, dat);
                         */
                        ed_si_origin[0] += numSI;
                        si_offset += numSI;
                        extraDetection++;
                    } else {
                        // Valid detection or invalid
                        boolean mustBeWritten = false;
                        if (detectionType == 0) {
                            validCount++;
                            mustBeWritten = true;

                        } else {
                            // A bug in kmall implies that even invalid extradetection are written (they
                            // shouldn't)
                            // Since there is no way to recognize them we skip the last invalid detection if
                            // there is
                            // too much of them regarding to the space allocated
                            invalidCount++;
                            if (invalidCount <= invalidDetectionInPingCount) {
                                mustBeWritten = true;
                            }

                        }
                        if (mustBeWritten) {

                            // Valid normal detection
                            for (ValueD2F8WithPosition v : valuesSwathBeamWithPosition)
                                v.fill(buffer, offset, soundingIndex, refPointLatLong[0], refPointLatLong[1], 0.0, null);

                            for (ValueD2 v : valuesSwathBeam) {
                                v.fill(buffer, offset, soundingIndex);
                            }

                            if (numSI > 0) {
                                int globalBeamOffset = soundingIndex * sampleCount;
                                System.arraycopy(si, si_offset, sampleBuffer, globalBeamOffset, numSI);

                                si_offset += numSI;
                            }
                            soundingIndex++;
                        }
                    }
                    offset = MRZ.getNextSoundingOffset(buffer.byteBufferData, offset);
                }

            }
            // fill samples
            if (sampleCount > 0)
                seabedImageSamples.put(new long[]{origin2D[0], 0, 0}, new long[]{1, detectionCount, sampleCount},
                        sampleBuffer);

            // Write in netcdf variable
            for (ValueD1F8WithPosition v : swathValuesWithPosition)
                v.write(new long[]{origin2D[0]});

            for (ValueD1 v : swathValues)
                v.write(new long[]{origin2D[0]});

            for (ValueD2 v : valuesRxAntenna)
                v.write(origin2D, new long[]{1, stats.getRxAntennaCount()});

            for (ValueD2 v : valuesTxSector)
                v.write(origin2D, new long[]{1, ping.getNumTxSector()});

            for (ValueD2 v : valuesRxInfo)
                v.write(origin2D, new long[]{1, stats.getRxAntennaCount()});

            for (ValueD2 v : valuesEdClass)
                v.write(origin2D, new long[]{1, ping.getNumExtraDetectionClasses()});

            for (ValueD2F8WithPosition v : valuesSwathBeamWithPosition)
                v.write(origin2D, new long[]{1, ping.getBeamCount()});

            for (ValueD2 v : valuesSwathBeam)
                v.write(origin2D, new long[]{1, ping.getBeamCount()});

            for (ValueD2 v : valuesSwathED)
                v.write(origin2D, new long[]{1, ping.getExtraDetectionsCount()});

        }
    }
}
