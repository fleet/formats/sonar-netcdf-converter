package fr.ifremer.globe.api.xsf.converter.common.xsf.types;

import java.util.EnumSet;

/**
 * "invalidity flag for soundings data, if one of the byte field is set (value != 0) the data are considered as invalid
 * for the given reasons :
 * 
 * <ul>
 * rejected : rejected by user operation, detailed reason is given by the detailed status field
 * </ul>
 * <ul>
 * invalid_acquisition : marked invalid by sounder
 * </ul>
 * <ul>
 * invalid_conversion : tagged as invalid during file creation (typically missing datagram)
 * </ul>
 * <ul>
 * invalid_swath : swath is invalidated (typically by the user)
 * </ul>
 * <ul>
 * invalid_sounding_row (range of sounding invalidated along sounding_row dimension)
 * </ul>
 */
public enum SonarDetectionStatus {

	VALID((byte) 0x0),
	/**
	 * rejected : rejected by user operation, detailed reason is given by the detailed status field
	 */
	REJECTED((byte) 0x01),
	/**
	 * invalid_acquisition : marked invalid by sounder
	 */
	INVALID_ACQUISITION((byte) 0x02),
	/**
	 * invalid_conversion : tagged as invalid during file creation (typically missing datagram)
	 */
	INVALID_CONVERSION((byte) 0x04),
	/**
	 * invalid_swath : swath is invalidated (typically by the user)
	 */
	INVALID_SWATH((byte) 0x08),
	/**
	 * invalid_sounding_row (range of sounding invalidated along sounding_row dimension)
	 */
	INVALID_SOUNDING_ROW((byte) 0x10);

	protected byte mask;

	private SonarDetectionStatus(byte mask) {
		this.mask = mask;
	}

	public static EnumSet<SonarDetectionStatus> getStatus(byte b) {
		EnumSet<SonarDetectionStatus> status = EnumSet.noneOf(SonarDetectionStatus.class);
		for (SonarDetectionStatus s : SonarDetectionStatus.values())
			if ((b & s.mask) > 0)
				status.add(s);
		return status;
	}

	public static boolean isValid(byte b) {
		return b == 0; // faster than getStatus(b).isEmpty();
	}

	/** @retur* value for the given enum */
	public byte getValue() {
		return mask;
	}

	/** @return the byte value for the given {@link EnumSet} */
	public static byte getValue(EnumSet<SonarDetectionStatus> statusSet) {
		byte initialValue = VALID.mask;
		for (SonarDetectionStatus s : statusSet)
			initialValue = (byte) (initialValue | s.mask);
		return initialValue;
	}

	/**
	 * Set of flag which must be turn off if detection is editbale.
	 */
	public static final EnumSet<SonarDetectionStatus> EDITABLE_DETECTION_MASK = EnumSet.of(//
			INVALID_CONVERSION, // check conversion validity
			// INVALID_ACQUISITION,// check acquistion validity
			INVALID_SWATH, // check swath validity
			INVALID_SOUNDING_ROW // check sounding row validity
	);
	public static final byte EDITABLE_DETECTION_MASK_BYTE = getValue(EDITABLE_DETECTION_MASK);

}
