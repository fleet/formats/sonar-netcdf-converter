package fr.ifremer.globe.api.xsf.converter.all.datagram.attitude;

import java.nio.ByteBuffer;

import fr.ifremer.globe.api.xsf.converter.all.datagram.IndividualSensorMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagram;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.SShort;
import fr.ifremer.globe.api.xsf.util.types.UByte;
import fr.ifremer.globe.api.xsf.util.types.UShort;

/**
 * Stateless Attitude datagram
 */
public class Attitude extends BaseDatagram {

	@Override
	public AttitudeMetadata getSpecificMetadata(AllFile metadata) {
		return metadata.getAttitude();
	}

	@Override
	protected void computeSpecificMetadata(AllFile metadata, ByteBuffer datagram, byte datagramType, long epochTime,
			DatagramPosition position) {
		final AttitudeMetadata index = getSpecificMetadata(metadata);
		int id = getSensorId(datagram);

		IndividualSensorMetadata sensorMetadata = index.getOrCreateSensor(id);
		sensorMetadata.addDatagram(position, getEpochTime(datagram), Attitude.getNumberEntries(datagram).getU());
		if (isActive(datagram)) {
			index.setActiveId(id);
		}
		// Calcul de stats
		for (int i = 0; i < getNumberEntries(datagram).getU(); i++) {
			index.addAttValue(getHeading(datagram, i).getU(), getRoll(datagram, i).get(), getPitch(datagram, i).get(),
					getHeave(datagram, i).get());
			index.computeMinMaxValue(getHeading(datagram, i).getU(), getRoll(datagram, i).get(),
					getPitch(datagram, i).get(), getHeave(datagram, i).get());
		}
	}

	public static UShort getCounter(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 12);
	}

	public static UShort getSerialNumber(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 14);
	}

	public static UShort getNumberEntries(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 16);
	}

	// Read Repeat Cycle - N Entries
	public static UShort getTimeSinceRecordStart(ByteBuffer datagram, int attIndex) {
		return TypeDecoder.read2U(datagram, 18 + attIndex * 12);
	}

	// TODO, strange sensor status is note on each sample
	public static UShort getSensorStatus(ByteBuffer datagram, int attIndex) {
		return TypeDecoder.read2U(datagram, 20 + attIndex * 12);
	}

	public static SShort getRoll(ByteBuffer datagram, int attIndex) {
		return TypeDecoder.read2S(datagram, 22 + attIndex * 12);
	}

	public static SShort getPitch(ByteBuffer datagram, int attIndex) {
		return TypeDecoder.read2S(datagram, 24 + attIndex * 12);
	}

	public static SShort getHeave(ByteBuffer datagram, int attIndex) {
		return TypeDecoder.read2S(datagram, 26 + attIndex * 12);
	}

	public static UShort getHeading(ByteBuffer datagram, int beamIndex) {
		return TypeDecoder.read2U(datagram, 28 + beamIndex * 12);
	}

	public static UByte getSensorDescriptor(ByteBuffer datagram) {
		return TypeDecoder.read1U(datagram, 18 + getNumberEntries(datagram).getU() * 12);
	}

	public static boolean isActive(ByteBuffer datagram) {
		/*
		 * 
		 * xxxx xxx1 – heading from the sensor is active xxxx xx0x – roll from the sensor is active xxxx x0xx – pitch
		 * from the sensor is active xxxx 0xxx – heave from the sensor is active
		 */
		boolean isHeadingActive = (getSensorDescriptor(datagram).getU() & 0x01) == 1;
		boolean isRollPitchActive = (getSensorDescriptor(datagram).getU() & 0x06) == 0;
		return isHeadingActive && isRollPitchActive;
	}

	public static int getSensorId(ByteBuffer datagram) {
		return ((getSensorDescriptor(datagram).getU() >> 4) & 0x1) + 1;
	}

}
