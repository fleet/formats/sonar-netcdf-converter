package fr.ifremer.globe.api.xsf.converter.common.exceptions;

/**
 * 
 * */
@SuppressWarnings("serial")
public class UnsupportedSounderException extends Exception {
	public UnsupportedSounderException() {

	}

	public UnsupportedSounderException(String message) {
		super(message);
	}

}
