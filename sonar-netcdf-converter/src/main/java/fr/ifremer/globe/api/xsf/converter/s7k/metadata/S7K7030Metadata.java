package fr.ifremer.globe.api.xsf.converter.s7k.metadata;

import java.util.Map.Entry;

import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.DatagramMetadata;

public class S7K7030Metadata extends DatagramMetadata<Long, S7K7030IndividualDeviceMetadata> {

	public S7K7030IndividualDeviceMetadata getOrCreateSensor(long sensorId, S7KFile metadata) {
		S7K7030IndividualDeviceMetadata r = index.get(sensorId);
		if (r == null) {
			r = new S7K7030IndividualDeviceMetadata(metadata);
			index.put(sensorId, r);
		}
		return r;
	}
	
	@Override
	public String toString() {
		StringBuilder bld = new StringBuilder();
		bld.append(this.getClass().getSimpleName());
		
		for (Entry<Long, S7K7030IndividualDeviceMetadata> e : getEntries()) {
			bld.append(System.lineSeparator() + "\t[" + Long.toString(e.getKey()) + "] " + e.getValue().datagramCount + " datagrams");
		}
		return bld.toString();
	}
	
}
