package fr.ifremer.globe.api.xsf.converter.common.datagram.metadata;

import java.util.List;

import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;

public abstract class AbstractCompleteAntennaPingMetadata {

    public abstract boolean isComplete();
    
    public abstract int getBeamCount();
    
    public abstract int getMaxAbstractSampleCount();

    public abstract List<DatagramPosition> locate();

}
