package fr.ifremer.globe.api.xsf.converter.s7k.xsf;

import static java.lang.Math.toIntExact;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.sounder.ISounderFile;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1F4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1S1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U8;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.PositionSubGroup;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.PositionSubGroupVendorSpecificGrp;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K1015;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KFile;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KIndividualSensorMetadata;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.api.xsf.util.types.SByte;
import fr.ifremer.globe.api.xsf.util.types.ULong;
import fr.ifremer.globe.netcdf.ucar.NCException;

public class S7K1015Converter implements IDatagramConverter<S7KFile, XsfFromS7k> {

	private List<S7K1015IndividualConverter> subgroups;

	@Override
	public void declare(S7KFile s7kFile, XsfFromS7k xsf) throws NCException {
		subgroups = new LinkedList<>();
		for (Entry<Long, S7KIndividualSensorMetadata> sensor : s7kFile.get1015Metadata().getEntries()) {
			S7K1015IndividualConverter grp = new S7K1015IndividualConverter(sensor.getKey(), s7kFile, sensor.getValue());
			// No need to export 1015 to Xsf ?
			grp.declare(s7kFile, xsf);
			subgroups.add(grp);
		}
	}
	
	@Override
	public void fillData(S7KFile s7kFile) throws IOException, NCException {
		for (S7K1015IndividualConverter grp : subgroups) {
			grp.fillData(s7kFile);
		}
	}

	private class S7K1015IndividualConverter implements IDatagramConverter<S7KFile, XsfFromS7k> {

		private ISounderFile dProxy;
		private S7KIndividualSensorMetadata md;
		private long key;
		private List<ValueD1> valuesD1;
		
		public S7K1015IndividualConverter(long key, ISounderFile dProxy, S7KIndividualSensorMetadata md) {
			this.md = md;
			this.dProxy = dProxy;
			this.key = key;
			this.valuesD1 = new LinkedList<>();
		}
		
		@Override
		public void declare(S7KFile s7kFile, XsfFromS7k xsf) throws NCException {
			PositionSubGroup positionGroup = xsf.addPositionGrp(String.format("1015_%03d", this.key), toIntExact(this.md.getTotalNumberOfEntries()));
			PositionSubGroupVendorSpecificGrp vendorGroup = new PositionSubGroupVendorSpecificGrp(positionGroup, this.dProxy,new HashMap<String, Integer>());
			
			// generic data
			valuesD1.add(new ValueD1F4(positionGroup.getCourse_over_ground(),
					(buffer) -> new FFloat((float) Math.toDegrees(S7K1015.getCourseOverGround(buffer).get()))));
			valuesD1.add(new ValueD1F4(positionGroup.getHeading(),
					(buffer) -> new FFloat((float) Math.toDegrees(S7K1015.getHeading(buffer).get()))));
			valuesD1.add(new ValueD1F4(positionGroup.getHeight_above_reference_ellipsoid(),
					(buffer) -> S7K1015.getVesselHeight(buffer)));
			valuesD1.add(new ValueD1F4(positionGroup.getLatitude(),
					(buffer) -> new FFloat((float) Math.toDegrees(S7K1015.getLatitude(buffer).get()))));
			valuesD1.add(new ValueD1F4(positionGroup.getLongitude(),
					(buffer) -> new FFloat((float) Math.toDegrees(S7K1015.getLongitude(buffer).get()))));
			valuesD1.add(new ValueD1F4(positionGroup.getSpeed_over_ground(),
					(buffer) -> S7K1015.getSpeedOverGround(buffer)));
			valuesD1.add(new ValueD1U8(positionGroup.getTime(),
					(buffer) -> new ULong(S7K1015.getTimestampNano(buffer))));
			
			// TODO see how to handle vertical_reference
			
			// specific data
			valuesD1.add(new ValueD1S1(vendorGroup.getVertical_reference(),
					(buffer) -> new SByte(S7K1015.getVerticalReference(buffer).getByte())));
			valuesD1.add(new ValueD1F4(vendorGroup.getHorizontal_position_accuracy(),
					(buffer) -> S7K1015.getHorizontalPositionAccuracy(buffer)));
			valuesD1.add(new ValueD1F4(vendorGroup.getHeight_accuracy(),
					(buffer) -> S7K1015.getHeightAccuracy(buffer)));
		}
		
		@Override
		public void fillData(S7KFile s7kFile) throws IOException, NCException {
			DatagramBuffer buffer = new DatagramBuffer();
			
			long i = 0;
			for (Entry<Long, DatagramPosition> posentry : this.md.getDatagrams()) {
				DatagramReader.readDatagram(posentry.getValue().getFile(), buffer, posentry.getValue().getSeek());
				
				for (ValueD1 v : this.valuesD1) {
					v.fill(buffer);
					v.write(new long[] { i });
				}
				i++;
			}
		}
		
	}
}
