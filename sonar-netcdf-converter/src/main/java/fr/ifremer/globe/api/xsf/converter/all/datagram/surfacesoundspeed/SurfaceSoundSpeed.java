package fr.ifremer.globe.api.xsf.converter.all.datagram.surfacesoundspeed;

import java.nio.ByteBuffer;

import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagram;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.UShort;

/**
 * Stateless Position datagram
 */
public class SurfaceSoundSpeed extends BaseDatagram {

	@Override
	public SurfaceSoundSpeedMetadata getSpecificMetadata(AllFile metadata) {
		return metadata.getSurfaceSoundSpeed();
	}

	@Override
	protected void computeSpecificMetadata(AllFile metadata, ByteBuffer datagram, byte datagramType,
			long epochTime, DatagramPosition position) {

        final SurfaceSoundSpeedMetadata index = getSpecificMetadata(metadata);

        index.addDatagram(position, getEpochTime(datagram), getNumberEntries(datagram).getU());
        index.setSerialNumber(getSerialNumber(datagram));

		// Calcul de stats
		for (int i = 0; i < getNumberEntries(datagram).getU(); i++)
		{
        	index.addSurfaceSoundSpeed(getSurfaceSoundSpeed(datagram, i).getU());
    		index.computeMinMaxSSS(getSurfaceSoundSpeed(datagram, i).getU());
		}
	}

	public static UShort getCounter(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 12);
	}
	
	public static UShort getSerialNumber(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 14);
	}

	public static UShort getNumberEntries(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 16);
	}

	// Read Repeat Cycle - N Entries
	public static UShort getTimeSinceRecordStart(ByteBuffer datagram, int sssIndex) {
		return TypeDecoder.read2U(datagram, 18 + sssIndex * 4);
	}
	
	public static UShort getSurfaceSoundSpeed(ByteBuffer datagram, int sssIndex) {
		return TypeDecoder.read2U(datagram, 20 + sssIndex * 4);
	}
	
	public static void print(ByteBuffer datagram) {
		System.out.println(getCounter(datagram));
		System.out.println(getSerialNumber(datagram));

	}

}
