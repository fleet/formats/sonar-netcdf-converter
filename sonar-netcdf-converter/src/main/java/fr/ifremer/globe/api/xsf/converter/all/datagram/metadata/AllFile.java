package fr.ifremer.globe.api.xsf.converter.all.datagram.metadata;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import fr.ifremer.globe.api.xsf.converter.common.exceptions.UnexpectedEndOfFile;
import org.slf4j.Logger;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramParser;
import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.all.datagram.DetectionBoundingBox;
import fr.ifremer.globe.api.xsf.converter.all.datagram.IndividualSensorMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.attitude.AttitudeMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.clock.ClockMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.depth68.Depth68CompletePingMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.depth68.Depth68Metadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.echosounderdepth.EchoSounderDepthMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.extradetections.ExtraDetectionsMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.extraparameters.ExtraParametersMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.heading.HeadingMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.height.HeightMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.installation.InstallationMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.installation.InstallationMetadata.Antenna;
import fr.ifremer.globe.api.xsf.converter.all.datagram.installation.InstallationMetadata.AntennaType;
import fr.ifremer.globe.api.xsf.converter.all.datagram.mechanicaltransducertilt.MechanicalTransducerTiltMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.networkattitudevelocity.NetworkAttitudeVelocityMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.position.PositionMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.qualityfactor.QualityFactorMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.rawrange102.RawRange102Metadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.rawrange70.RawRange70Metadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.rawrange78.RawRange78Metadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.runtime.RunTimeMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.seabed83.Seabed83Metadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.seabed89.Seabed89Metadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.soundspeedprofile.SoundSpeedProfileMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.stavedata.StaveDataMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.surfacesoundspeed.SurfaceSoundSpeedMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.tide.TideMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.unknown.UnknownMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.wc.WCMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.xyz88.XYZ88DepthMetadata;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.exceptions.BadETXRecord;
import fr.ifremer.globe.api.xsf.converter.common.exceptions.BadSTXRecord;
import fr.ifremer.globe.api.xsf.converter.common.exceptions.UnsupportedSounderException;
import fr.ifremer.globe.api.xsf.converter.common.sounder.IKongsbergFile;
import fr.ifremer.globe.api.xsf.util.DefaultLogger;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.utils.sounders.SounderDescription.Constructor;
import fr.ifremer.globe.utils.sounders.SoundersLibrary;

public class AllFile implements IKongsbergFile {

	public static final String EXTENSION_ALL = ".all";
	public static final String EXTENSION_ALL_WC = ".wcd";

	private final Logger logger = DefaultLogger.get();
	private final String filePath;
	private final DatagramParser parser;
	private final List<RandomAccessFile> openedSounderFiles = new ArrayList<>();
	private final ByteOrder byteOrder;

	/**
	 * Default constructor (parse water column data)
	 *
	 * @throws UnsupportedSounderException
	 */
	public AllFile(String filePath) throws IOException, UnsupportedSounderException {
		this(filePath, true);
	}

	/**
	 * Constructor
	 *
	 * @throws UnsupportedSounderException
	 */
	public AllFile(String filePath, boolean readWaterColum) throws IOException, UnsupportedSounderException {
		this.parser = new DatagramParser(readWaterColum);
		this.filePath = filePath;
		// index .all
		RandomAccessFile allRandowAccessFile = indexFile(filePath);
		openedSounderFiles.add(allRandowAccessFile);
		byteOrder = DatagramReader.getByteOrder(allRandowAccessFile);

		// try to index water column file if exists
		String waterColumnFilePath = filePath.replace(EXTENSION_ALL, EXTENSION_ALL_WC);
		if (readWaterColum && new File(waterColumnFilePath).exists())
			openedSounderFiles.add(indexFile(waterColumnFilePath));

		index();
		getInstallation().analyse();
	}

	private RandomAccessFile indexFile(String filePath) throws IOException {
		logger.info("Opening and indexation of file : {}...", filePath);
		RandomAccessFile raf = new RandomAccessFile(filePath, "r");
		DatagramBuffer buffer = new DatagramBuffer(DatagramReader.getByteOrder(raf));
		long offsetInfile = 0;
		int byteRead = 0;
		try {
			while ((byteRead = DatagramReader.readDatagram(raf, buffer)) > 0) {
				if (byteRead > 4) {
					parser.computeMetaData(this, buffer.byteBufferData, new DatagramPosition(raf, offsetInfile));
				}
				offsetInfile += byteRead;
				if (raf.getFilePointer() != offsetInfile) {
					throw new IOException();
				}
			}
		} catch (BadSTXRecord | BadETXRecord | UnexpectedEndOfFile e) {
			// invalid datagram (error with end flag) : error logged, but the conversion can be try with valid datagrams
			logger.error("Invalid datagram at {} : {} ", offsetInfile, e.getMessage(), e);
			logger.warn("Try conversion with valid datagrams...");
		}
		return raf;
	}

	@Override
	public void close() {
		openedSounderFiles.forEach(raffile -> {
			try {
				raffile.close();
			} catch (IOException e) {
				logger.error("Error while closing file {} (or .wcd) : {}", filePath, e.getMessage(), e);
			}
		});
	}

	GlobalMetaData globalMetadata = new GlobalMetaData();
	UnknownMetadata unknown = new UnknownMetadata();

	XYZ88DepthMetadata xyz88depth = new XYZ88DepthMetadata();
	Depth68Metadata depth68 = new Depth68Metadata();
	WCMetadata wc = new WCMetadata();
	QualityFactorMetadata qualityfactor = new QualityFactorMetadata();
	RawRange70Metadata rawrange70 = new RawRange70Metadata();
	RawRange78Metadata rawrange78 = new RawRange78Metadata();
	RawRange102Metadata rawrange102 = new RawRange102Metadata();
	ExtraDetectionsMetadata extradetections = new ExtraDetectionsMetadata();
	Seabed83Metadata seabed83 = new Seabed83Metadata();
	Seabed89Metadata seabed89 = new Seabed89Metadata();
	StaveDataMetadata stavedata = new StaveDataMetadata();

	AttitudeMetadata attitude = new AttitudeMetadata();
	NetworkAttitudeVelocityMetadata networkattitudevelocity = new NetworkAttitudeVelocityMetadata();
	HeadingMetadata heading = new HeadingMetadata();
	ClockMetadata clock = new ClockMetadata();
	PositionMetadata position = new PositionMetadata();
	TideMetadata tide = new TideMetadata();
	EchoSounderDepthMetadata echosounder = new EchoSounderDepthMetadata();
	HeightMetadata height = new HeightMetadata();

	SurfaceSoundSpeedMetadata surfacesoundspeed = new SurfaceSoundSpeedMetadata();
	SoundSpeedProfileMetadata soundspeedprofile = new SoundSpeedProfileMetadata();

	InstallationMetadata installation = new InstallationMetadata();
	MechanicalTransducerTiltMetadata mechanicaltransducertilt = new MechanicalTransducerTiltMetadata();
	ExtraParametersMetadata extraparameters = new ExtraParametersMetadata();
	RunTimeMetadata runtime = new RunTimeMetadata();

	DetectionBoundingBox detectionBB = new DetectionBoundingBox();

	public DetectionBoundingBox getDetectionBB() {
		return detectionBB;
	}

	public ByteOrder getByteOrder() {
		return byteOrder;
	}

	/**
	 * @return the rawrange (46h, 70d)
	 */
	public RawRange70Metadata getRawRange70() {
		return rawrange70;
	}

	/**
	 * @return the rawrange (66h, 102d)
	 */
	public RawRange102Metadata getRawRange102() {
		return rawrange102;
	}

	/**
	 * @return the rawrange (4eh, 78d)
	 */
	public RawRange78Metadata getRawRange78() {
		return rawrange78;
	}

	/**
	 * @return the wc
	 */
	public WCMetadata getWc() {
		return wc;
	}

	/**
	 * @return the unknown
	 */
	public UnknownMetadata getUnknown() {
		return unknown;
	}

	/**
	 * @return the xyzdepth
	 */
	public XYZ88DepthMetadata getXyzdepth() {
		return xyz88depth;
	}

	/**
	 * @return the xyzdepth
	 */
	public Depth68Metadata getDepth68() {
		return depth68;
	}

	/**
	 * @return the clock
	 */
	public ClockMetadata getClock() {
		return clock;
	}

	/**
	 * @return the position dgm
	 */
	public PositionMetadata getPosition() {
		return position;
	}

	/**
	 * @return the qualityfactor79 (from ifr)
	 */
	public QualityFactorMetadata getQualityFactor() {
		return qualityfactor;
	}

	public GlobalMetaData getGlobalMetadata() {
		return globalMetadata;
	}

	public InstallationMetadata getInstallation() {
		return installation;
	}

	public TideMetadata getTide() {
		return tide;
	}

	public AttitudeMetadata getAttitude() {
		return attitude;
	}

	public NetworkAttitudeVelocityMetadata getNetworkAttitudeVelocity() {
		return networkattitudevelocity;
	}

	public HeadingMetadata getHeading() {
		return heading;
	}

	public EchoSounderDepthMetadata getEchoSounder() {
		return echosounder;
	}

	public HeightMetadata getHeight() {
		return height;
	}

	public SoundSpeedProfileMetadata getSoundSpeedProfile() {
		return soundspeedprofile;
	}

	public SurfaceSoundSpeedMetadata getSurfaceSoundSpeed() {
		return surfacesoundspeed;
	}

	public MechanicalTransducerTiltMetadata getMechanicalTransducerTilt() {
		return mechanicaltransducertilt;
	}

	public ExtraParametersMetadata getExtraParameters() {
		return extraparameters;
	}

	public RunTimeMetadata getRunTime() {
		return runtime;
	}

	public Seabed83Metadata getSeabed83() {
		return seabed83;
	}

	public Seabed89Metadata getSeabed89() {
		return seabed89;
	}

	public ExtraDetectionsMetadata getExtraDetections() {
		return extradetections;
	}

	public StaveDataMetadata getStaveData() {
		return stavedata;
	}

	/**
	 * return true if the ping is not rejected
	 */
	private boolean isPingKept(SwathIdentifier is) {
		boolean hasBathyMetry = (getDepth68().contains(is) && getDepth68().getPing(is).isComplete(getGlobalMetadata().getSerialNumbers())) ||
				(getXyzdepth().contains(is) && getXyzdepth().getPing(is).isComplete(getGlobalMetadata().getSerialNumbers()));
		boolean hasWaterColumn = getWc().contains(is) && getWc().getPing(is).isComplete(getGlobalMetadata().getSerialNumbers());
		return hasBathyMetry || hasWaterColumn;
	}

	/**
	 * compute metadata and indexes after file parsing
	 */
	public void index() {

		// compute the list of retained ping, all others will be discarded
		getGlobalMetadata().updateRetainedIndex(id -> this.isPingKept(id));

		attitude.forEachSensor((key, metadata) -> metadata.getGenerator().generateIndex());
		networkattitudevelocity.forEachSensor((key, metadata) -> metadata.getGenerator().generateIndex());
		heading.getGenerator().generateIndex();
		clock.getGenerator().generateIndex();
		position.forEachSensor((key, metadata) -> metadata.getGenerator().generateIndex());
		tide.getGenerator().generateIndex();
		height.getGenerator().generateIndex();

		// echosounder.datagramCount

		surfacesoundspeed.getGenerator().generateIndex();
		soundspeedprofile.getGenerator().generateIndex();
		runtime.getGenerator().generateIndex();
		extraparameters.getGenerator().generateIndex();
		mechanicaltransducertilt.getGenerator().generateIndex();

		getWc().computeIndex(getGlobalMetadata().getSerialNumbers());
		getSeabed89().computeIndex(getGlobalMetadata().getSerialNumbers());
		getSeabed83().computeIndex(getGlobalMetadata().getSerialNumbers());
		getQualityFactor().computeIndex(getGlobalMetadata().getSerialNumbers());
		getXyzdepth().computeIndex(getGlobalMetadata().getSerialNumbers());
		getDepth68().computeIndex(getGlobalMetadata().getSerialNumbers());

		getRawRange78().computeIndex(getGlobalMetadata().getSerialNumbers());
		getRawRange70().computeIndex(getGlobalMetadata().getSerialNumbers());
		getRawRange102().computeIndex(getGlobalMetadata().getSerialNumbers());
		getStaveData().computeIndex(getGlobalMetadata().getSerialNumbers());
		getExtraDetections().computeIndex(getGlobalMetadata().getSerialNumbers());

	}

	@Override
	public String getFilePath() {
		return filePath;
	}

	@Override
	public String getModelNumber() {
		return Integer.toString(getGlobalMetadata().getModelNumber());
	}

	@Override
	public int getMbgModelNumber() {
		int modelNumber = getInstallation().getModelNumber();
		return SoundersLibrary.get().fromConstructorId(modelNumber, Constructor.Kongsberg).getGenericId();
	}

	@Override
	public int getMbgSerialNumber() {
		int serialNumber = getInstallation().getFirstTxAntenna().getId().getU();
		if (serialNumber == 0)
			serialNumber = getInstallation().getSerialNumber().getU();
		// if serial number is 0; try to get it from depth68 datagram
		if (serialNumber == 0 && !getDepth68().getPings().isEmpty()) {
			Optional<Entry<SwathIdentifier, Depth68CompletePingMetadata>> optPing = getDepth68().getPings().stream()
					.findFirst();
			if (optPing.isPresent())
				serialNumber = optPing.get().getValue().getAntennas().keySet().stream().findFirst().orElse(0);
		}
		return serialNumber;
	}

	@Override
	public String getRawInstallationParameters() {
		return getInstallation().getRawConf();
	}

	@Override
	public int getSwathCount() {
		return getGlobalMetadata().getSwathCount();
	}

	@Override
	public int getDetectionCount() {
		if (getXyzdepth().datagramCount > 0)
			return getXyzdepth().getMaxBeamCount();
		if (getDepth68().datagramCount > 0)
			return getDepth68().getMaxBeamCount();
		return -1;
	}

	@Override
	public int getTxSectorCount() {
		if (getRawRange78().datagramCount > 0)
			return getRawRange78().getTxSectorCount();
		if (getRawRange70().datagramCount > 0)
			return getRawRange70().getTxSectorCount();
		if (getRawRange102().datagramCount > 0)
			return getRawRange102().getTxSectorCount();
		if (getWc().datagramCount > 0)
			return getWc().getTxSectorCount();
		return -1;
	}

	@Override
	public int getWcBeamCount() {
		return getWc().getMaxBeamCount();
	}

	@Override
	public int getMaxSeabedSampleCount() {
		if (getSeabed83().datagramCount > 0)
			return getSeabed83().getMaxAbstractSampleCount();
		if (getSeabed89().datagramCount > 0)
			return getSeabed89().getMaxAbstractSampleCount();
		return -1;
	}

	@Override
	public int getAntennaNb() {
		return getInstallation().getAntennas().size();
	}

	@Override
	public int getRxAntennaNb() {
		return getInstallation().getAntennas().stream().filter(an -> an.getType() == AntennaType.RX).toArray().length;
	}

	@Override
	public int getTxAntennaNb() {
		return getInstallation().getAntennas().stream().filter(an -> an.getType() == AntennaType.TX).toArray().length;
	}

	@Override
	public long getMinDate() {
		return getGlobalMetadata().getMinDate();
	}

	@Override
	public long getMaxDate() {
		return getGlobalMetadata().getMaxDate();
	}

	@Override
	public double getLatMin() {
		return getDetectionBB().getLatMin();
	}

	@Override
	public double getLatMax() {
		return getDetectionBB().getLatMax();
	}

	@Override
	public double getLonMin() {
		return getDetectionBB().getLonMin();
	}

	@Override
	public double getLonMax() {
		return getDetectionBB().getLonMax();
	}

	@Override
	public double getSOGMin() {
		return getPosition().getSOGMin();
	}

	@Override
	public double getSOGMax() {
		return getPosition().getSOGMax();
	}

	@Override
	public double getDepthMin() {
		if (getXyzdepth().datagramCount > 0)
			return getXyzdepth().getDepthMin();
		else if (getDepth68().datagramCount > 0)
			return getDepth68().getDepthMin();
		else
			return Float.NaN;
	}

	@Override
	public double getDepthMax() {
		if (getXyzdepth().datagramCount > 0)
			return getXyzdepth().getDepthMax();
		else if (getDepth68().datagramCount > 0)
			return getDepth68().getDepthMax();
		else
			return Float.NaN;
	}

	@Override
	public float getDepthMean() {
		if (getXyzdepth().datagramCount > 0)
			return getXyzdepth().getDepthMean();
		else if (getDepth68().datagramCount > 0)
			return getDepth68().getDepthMean();
		else
			return Float.NaN;
	}

	@Override
	public float getHeadingMin() {
		return getAttitude().getHeadingMin();
	}

	@Override
	public float getHeadingMax() {
		return getAttitude().getHeadingMax();
	}

	@Override
	public float getHeadingMean() {
		return getAttitude().getHeadingMean();
	}

	@Override
	public float getRollMin() {
		return getAttitude().getRollMin();
	}

	@Override
	public float getRollMax() {
		return getAttitude().getRollMax();
	}

	@Override
	public float getRollMean() {
		return getAttitude().getRollMean();
	}

	@Override
	public float getPitchMin() {
		return getAttitude().getPitchMin();
	}

	@Override
	public float getPitchMax() {
		return getAttitude().getPitchMax();
	}

	@Override
	public float getPitchMean() {
		return getAttitude().getPitchMean();
	}

	@Override
	public float getHeaveMin() {
		return getAttitude().getHeaveMin();
	}

	@Override
	public float getHeaveMax() {
		return getAttitude().getHeaveMax();
	}

	@Override
	public float getHeaveMean() {
		return getAttitude().getHeaveMean();
	}

	@Override
	public float getBsMin() {
		if (getSeabed83().datagramCount > 0)
			return getSeabed83().getBSMin();
		else if (getSeabed89().datagramCount > 0)
			return getSeabed89().getBSMin();
		else
			return Float.NaN;
	}

	@Override
	public float getBsMax() {
		if (getSeabed83().datagramCount > 0)
			return getSeabed83().getBSMax();
		else if (getSeabed89().datagramCount > 0)
			return getSeabed89().getBSMax();
		else
			return Float.NaN;
	}

	@Override
	public float getBsMean() {
		if (getSeabed83().datagramCount > 0)
			return getSeabed83().getBSMean();
		else if (getSeabed89().datagramCount > 0)
			return getSeabed89().getBSMean();
		else
			return Float.NaN;
	}

	@Override
	public float getSspMin() {
		return getSurfaceSoundSpeed().getSurfaceSoundSpeedMin();
	}

	@Override
	public float getSspMax() {
		return getSurfaceSoundSpeed().getSurfaceSoundSpeedMax();
	}

	@Override
	public float getSspMean() {
		return getSurfaceSoundSpeed().getSurfaceSoundSpeedMean();
	}

	@Override
	public int getMaxSeabedExtraSampleCount() {
		return 0;
	}

	@Override
	public int getRuntimeDgCount() {
		return getRunTime().getDatagrams().size();
	}

	@Override
	public long getSVPProfileCount() {
		return getSoundSpeedProfile().getDatagrams().size();
	}

	@Override
	public long getSVPMaxSampleCount() {
		return getSoundSpeedProfile().getMaxEntriesPerDatagram();
	}

	@Override
	public long getSurfaceSoundSpeedCount() {
		return getSurfaceSoundSpeed().getTotalNumberOfEntries();
	}

	@Override
	public long getHeightCount() {
		return getHeight().getTotalNumberOfEntries();
	}

	@Override
	public long getClockCount() {
		return getClock().getTotalNumberOfEntries();
	}

	@Override
	public long getDepthCount() {
		return 0;
	}

	@Override
	public long getHeadingCount() {
		return getHeading().getTotalNumberOfEntries();
	}

	@Override
	public long getPositionCaptorCount() {
		Map<Integer, IndividualSensorMetadata> sensors = getPosition().getSensors();
		return sensors.keySet().size();
	}

	@Override
	public long getMRUCaptorCount() {
		return getAttitude().getSensors().keySet().size();
	}

	@Override
	public int getModelNumberCode() {
		return getGlobalMetadata().getModelNumber();
	}

	@Override
	public long getBeamDescriptionCount() {
		return 0;
	}

	@Override
	public long getSnippetCount() {
		return 0;
	}

	@Override
	public long getSidescanCount() {
		return 0;
	}

	@Override
	public double[] getTxAntennaLevelArm() {
		Antenna txAntenna = getInstallation().getFirstTxAntenna();
		return new double[] { txAntenna.getXWithoutOffset(), txAntenna.getYWithoutOffset(),
				txAntenna.getZWithoutOffset() };
	}

	/**
	 * @return a specific string to fill the attribute like in previous MBG converter...
	 */
	@Override
	public String getAcrossAngleCorrectFlag() {
		return Character.toString((char) 1);
	}

	@Override
	public long getDepthDatagramMinDate() {
		return getDepth68().datagramCount > 0 ? getDepth68().getMinDate() : getXyzdepth().getMinDate();
	}

	@Override
	public long getDepthDatagramMaxDate() {
		return getDepth68().datagramCount > 0 ? getDepth68().getMaxDate() : getXyzdepth().getMaxDate();
	}

	@Override
	public boolean hasPhaseValue() {
		return false;
	}

}
