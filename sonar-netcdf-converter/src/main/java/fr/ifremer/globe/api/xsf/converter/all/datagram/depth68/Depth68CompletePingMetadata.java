package fr.ifremer.globe.api.xsf.converter.all.datagram.depth68;

import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.AbstractCompletePingMetadata;

public class Depth68CompletePingMetadata extends AbstractCompletePingMetadata<Depth68CompleteAntennaPingMetadata>{

	public Depth68CompletePingMetadata(int[] serialNumbers) {
        super(0, serialNumbers);
    }
}
