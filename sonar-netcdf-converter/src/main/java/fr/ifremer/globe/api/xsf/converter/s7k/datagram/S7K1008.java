package fr.ifremer.globe.api.xsf.converter.s7k.datagram;

import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7K1008Metadata;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KFile;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KIndividualSensorMetadata;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.api.xsf.util.types.UByte;

public class S7K1008 extends S7KBaseDatagram {

	@Override
	public void computeSpecificMetadata(S7KFile metadata, DatagramBuffer buffer, long type,
			DatagramPosition position, long time) {
		S7K1008Metadata md = getSpecificMetadata(metadata);
		S7KIndividualSensorMetadata sensorMd = md.getEntry(getDeviceIdentifier(buffer).getU());
		if (sensorMd == null) {
			sensorMd = new S7KIndividualSensorMetadata();
			md.addSensor(getDeviceIdentifier(buffer).getU(), sensorMd);
		}
		
		sensorMd.addDatagram(position, time, 1);
	}

	@Override
	public S7K1008Metadata getSpecificMetadata(S7KFile metadata) {
		return metadata.get1008Metadata();
	}

	public static UByte getDepthDescriptor(DatagramBuffer buffer) {
		return TypeDecoder.read1U(buffer.byteBufferData, 0);
	}
	
	public static UByte getCorrectionFlag(DatagramBuffer buffer) {
		return TypeDecoder.read1U(buffer.byteBufferData, 1);
	}
	
	public static FFloat getDepth(DatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 4);
	}
}
