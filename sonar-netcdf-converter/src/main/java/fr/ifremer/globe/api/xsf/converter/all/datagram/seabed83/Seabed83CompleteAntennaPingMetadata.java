package fr.ifremer.globe.api.xsf.converter.all.datagram.seabed83;

import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.AbstractCompleteAntennaPingSingleMetadata;

public class Seabed83CompleteAntennaPingMetadata extends AbstractCompleteAntennaPingSingleMetadata {

    public Seabed83CompleteAntennaPingMetadata(DatagramPosition position, int beamCount, int maxSampleCount) {
        super(position, beamCount, maxSampleCount);
    }
}
