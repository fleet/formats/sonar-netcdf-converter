package fr.ifremer.globe.api.xsf.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;

/**
 * This class store {@link SwathIdentifier} and their computed index
 */
public class SwathIndexContainer {

	/** {@link Map} between a {@link SwathIdentifier} and the matching index */
	private HashMap<SwathIdentifier, Integer> id2Index = new HashMap<>();

	public void generateIndex(SwathIndexGenerator generator, Predicate<SwathIdentifier> filter) {
		generator.sort();
		// then create a list of unique identifier and their indexes
		id2Index.clear();

		// Atomic integer to be able to increment it in lambda ....
		AtomicInteger index = new AtomicInteger(0);

		generator.stream().filter(filter) // keep only swathId matching the predicate
				.filter(swathId -> !id2Index.containsKey(swathId)) // prevent from adding several times the same swathid
				// if not already in map, use the current index and compute next index value
				.forEach(swathId -> id2Index.put(swathId, index.getAndIncrement()));
	}

	/**
	 * @return true if this map contains an index for the specified id.
	 */
	public boolean contains(SwathIdentifier id) {
		return id2Index.containsKey(id);
	}

	/**
	 * @return the number of generated swath indexes.
	 */
	public int getIndexSize() {
		return id2Index.size();
	}

	public Set<Entry<SwathIdentifier, Integer>> getEntries() {
		return id2Index.entrySet();
	}

	/**
	 * @return the index associated to the {@link SwathIdentifier} if exists, else an empty {@link Optional}.
	 */
	public Optional<Integer> getIndex(SwathIdentifier id) {
		return id2Index.containsKey(id) ? Optional.of(id2Index.get(id)) : Optional.empty();
	}

}
