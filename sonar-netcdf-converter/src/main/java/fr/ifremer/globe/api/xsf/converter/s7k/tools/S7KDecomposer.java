package fr.ifremer.globe.api.xsf.converter.s7k.tools;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;

import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7KBaseDatagram;

public class S7KDecomposer {
	
	public static String infile = "";
	public static String dest = "";
	
	public static void main(String[] args) throws IOException {
		final RandomAccessFile raf = new RandomAccessFile(infile, "r");
		final DatagramBuffer buffer = new DatagramBuffer();
		
		
		final File outFile = new File(dest);
		if (outFile.exists()) {
			outFile.delete();
		}
		
		int dgCount = 0;
		while (DatagramReader.readDatagram(raf, buffer) > 0) {
			final File byTypeFolder = new File(outFile, Long.toString(S7KBaseDatagram.getRecordType(buffer).getU()));
			if (!byTypeFolder.exists()) {
				byTypeFolder.mkdirs();
			}
			final File dgFile = new File(byTypeFolder, String.format("%05d.s7k", dgCount++));
			FileOutputStream binOut = new FileOutputStream(dgFile);
			binOut.write(buffer.rawBufferHeader);
			binOut.write(buffer.rawBufferData, 0, buffer.byteBufferData.limit());
			binOut.close();
		}
	}

}
