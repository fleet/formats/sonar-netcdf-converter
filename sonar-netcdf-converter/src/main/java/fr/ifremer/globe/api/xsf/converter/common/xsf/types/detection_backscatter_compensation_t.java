package fr.ifremer.globe.api.xsf.converter.common.xsf.types;

/**
 * "Indicate Whether or not the beam direction is compensated for platform
 * motion.
 */
public enum detection_backscatter_compensation_t {
	not_compensated((byte) 0), compensated((byte) 1);

	private byte value;

	public byte get() {
		return value;
	}

	detection_backscatter_compensation_t(byte value) {
		this.value = value;
	}
}
