package fr.ifremer.globe.api.xsf.converter.all.mbg.bathy;

import java.util.ArrayList;
import java.util.List;

import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgBase;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2WithAttitude;

/**
 * Abstract class for all RawRange converter
 */
public abstract class AbstractRawRangeDatagramConverter implements IDatagramConverter<AllFile, MbgBase> {

	protected List<ValueD2> swathBeamValues = new ArrayList<>();
	protected List<ValueD2WithAttitude> swathBeamWithAttitudeValues = new ArrayList<>();

}
