package fr.ifremer.globe.api.xsf.util;

/**
 * a simple integer identifier
 * */
public class SimpleIdentifier implements DgIdentifier, Comparable<SimpleIdentifier> {
	private long rawId;

	SimpleIdentifier(long rawId)
	{
		this.rawId=rawId;
	}

	public long getRawId() {
		return rawId;
	}

	@Override
	public int hashCode() {
		return Long.hashCode(rawId);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SimpleIdentifier other = (SimpleIdentifier) obj;
		if (rawId != other.rawId)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SimpleIdentifier [rawId=" + rawId + "]";
	}


	@Override
	public int compareTo(SimpleIdentifier other) {
		return Long.compare(this.rawId,other.rawId);
	}
}
