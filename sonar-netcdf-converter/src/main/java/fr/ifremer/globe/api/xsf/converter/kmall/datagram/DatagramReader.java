package fr.ifremer.globe.api.xsf.converter.kmall.datagram;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.LinkedList;
import java.util.List;

import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.exceptions.UnexpectedEndOfFile;
import fr.ifremer.globe.api.xsf.util.ObjectBufferPool;
import fr.ifremer.globe.netcdf.util.Pair;

public class DatagramReader {

    /**
     * Read a kmall datagram at the current position in an opened file
     * @param raf
     * @param buffer
     * @return
     * @throws IOException
     */
    public static int readDatagram(RandomAccessFile raf, DatagramBuffer buffer) throws IOException {
        int byteRead = 0;
        
        byteRead = raf.read(buffer.rawBufferHeader);
        if (byteRead < 0) {
            return byteRead;
        }
        buffer.byteBufferSizeHeader.rewind();
        long datagramSize = Integer.toUnsignedLong(buffer.byteBufferSizeHeader.getInt());
        int bodySize = (int) datagramSize - 4;
        buffer.allocate(bodySize);
        int read = raf.read(buffer.rawBufferData, 0, bodySize);
        if (read != datagramSize - 4) {
            throw new UnexpectedEndOfFile();
        }
        byteRead += read;
        buffer.byteBufferData.limit(bodySize);
        buffer.byteBufferData.rewind();
        
        // perform basic validation
        buffer.validate();
        
        return byteRead;
    }
    
    /**
     * Move the current position in the file and read one kmall datagram
     * @param raf
     * @param buffer
     * @param fileOffset
     * @return
     * @throws IOException
     */
    public static int readDatagram(RandomAccessFile raf, DatagramBuffer buffer, long fileOffset) throws IOException {
        if (fileOffset >= 0) {
            raf.seek(fileOffset);
        }
        return readDatagram(raf, buffer);
    }
    
    /**
     * Load a set of datagrams from a list of positions
     * 
     * @param pool
     * @param locations
     * @return
     * @throws IOException
     */
    public static <T> List<Pair<DatagramBuffer, T>> read(ObjectBufferPool<DatagramBuffer> pool, List<Pair<DatagramPosition, T>> locations) throws IOException {
        List<Pair<DatagramBuffer, T>> ret = new LinkedList<Pair<DatagramBuffer,T>>();
        for (Pair<DatagramPosition, T> loc : locations) {
            DatagramBuffer buffer = pool.borrow();
            readDatagram(loc.getFirst().getFile(), buffer, loc.getFirst().getSeek());
            ret.add(new Pair<DatagramBuffer, T>(buffer, loc.getSecond()));
        }
        return ret;
    }
}
