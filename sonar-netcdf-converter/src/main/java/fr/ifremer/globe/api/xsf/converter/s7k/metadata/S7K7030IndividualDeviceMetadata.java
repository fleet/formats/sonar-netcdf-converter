package fr.ifremer.globe.api.xsf.converter.s7k.metadata;

import java.io.File;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.DatagramMetadata;
import fr.ifremer.globe.api.xsf.converter.s7k.tools.S7K7030InstallationParamSerializer;

public class S7K7030IndividualDeviceMetadata extends DatagramMetadata<Long, DatagramPosition> {

	private final S7KFile metadata;
	private final Map<Long, S7K7030InstallationParameters> configs = new TreeMap<>();

	/** Constructor **/
	public S7K7030IndividualDeviceMetadata(S7KFile metadata) {
		this.metadata = metadata;
	}

	public void registerSonarInstallationParameters(DatagramPosition position, long time,
			S7K7030InstallationParameters params) {
		this.addDatagram();
		this.index.put(time, position);
		configs.put(time, params);
		// Save installation parameters in a local file (for future conversion of file without install parameters)
		S7K7030InstallationParamSerializer.serialize(new File(metadata.getFilePath()).getParent(), params);
	}

	public S7K7030InstallationParameters getConfiguration(long time) {
		Iterator<Entry<Long, S7K7030InstallationParameters>> it = configs.entrySet().iterator();
		if (it.hasNext()) {
			Entry<Long, S7K7030InstallationParameters> e = it.next();

			while (e.getKey() > time && it.hasNext()) {
				e = it.next();
			}

			return e.getValue();
		} else {
			return null;
		}
	}
}
