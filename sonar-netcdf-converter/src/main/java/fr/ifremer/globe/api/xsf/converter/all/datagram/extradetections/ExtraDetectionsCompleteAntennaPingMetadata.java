package fr.ifremer.globe.api.xsf.converter.all.datagram.extradetections;

import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;

/**
 * all datagram metadata for a ping/antenna couple
 */
public class ExtraDetectionsCompleteAntennaPingMetadata {

    public int getDetectionClassesCount() {
        return detectionClassesCount;
    }

    public int getDetectionCount() {
        return detectionCount;
    }

    public int getRawSampleCount() {
        return rawSampleCount;
    }

    DatagramPosition position;
    int detectionClassesCount;
    int detectionCount;
    int rawSampleCount;
    
    public ExtraDetectionsCompleteAntennaPingMetadata(DatagramPosition position, int detectionClasses, int detections, int rawSamples) {
        this.position = position;
        this.detectionClassesCount = detectionClasses;
        this.detectionCount = detections;
        this.rawSampleCount = rawSamples;
    }
}