package fr.ifremer.globe.api.xsf.converter.kmall.xsf;

import com.sun.jna.Memory;
import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.*;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.*;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfFromKongsberg;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.BeamGroup1Grp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.BeamGroup1VendorSpecificGrp;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.*;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.KmallFile;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.MWCCompletePingAntennaMetadata;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.MWCCompletePingMetadata;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.MWCMetadata;
import fr.ifremer.globe.api.xsf.util.DefaultLogger;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.types.*;
import fr.ifremer.globe.netcdf.jna.Nc4prototypes.Vlen_t;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCVariable;
import fr.ifremer.globe.netcdf.util.Pair;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

public class MWCConverter implements IDatagramConverter<KmallFile, XsfFromKongsberg> {

	private KmallFile stats;
	private MWCMetadata metadata;
	private List<ValueD2> valuesRxAntenna;
	private List<ValueD2> valuesSwathTxSector;
	private List<ValueD2> valuesRxInfo;
	private List<ValueD2> valuesSwathBeam;
	private List<ValueD1> valuesSwath;
	private BeamGroup1Grp beamGrp;

	public MWCConverter(KmallFile stats) {
		this.stats = stats;
		this.metadata = stats.getMWCMetadata();

		valuesRxAntenna = new LinkedList<>();
		valuesSwathTxSector = new LinkedList<>();
		valuesRxInfo = new LinkedList<>();
		valuesSwathBeam = new LinkedList<>();
		valuesSwath = new LinkedList<>();
	}

	@Override
	public void declare(KmallFile allFile, XsfFromKongsberg xsf) throws NCException {
		beamGrp = xsf.getBeamGroup();
		BeamGroup1VendorSpecificGrp beamGrpVendor = xsf.getBeamGroupVendorSpecific();

		valuesRxAntenna.add(new ValueD2U1(beamGrpVendor.getSystem_id(),
				(buffer, i) -> KmAllBaseDatagram.getSystemId(buffer.byteBufferData)));

		valuesRxAntenna.add(new ValueD2U2(beamGrpVendor.getEcho_sounder_id(),
				(buffer, i) -> KmAllBaseDatagram.getEchoSounderId(buffer.byteBufferData)));

		valuesSwath.add(new ValueD1U8(beamGrp.getPing_time(),
				buffer -> new ULong(KmAllBaseDatagram.getEpochTimeNano(buffer.byteBufferData))));

		valuesSwath.add(new ValueD1U4(beamGrpVendor.getPing_raw_count(),
				(buffer) -> new UInt(KmAllMDatagram.getPingCnt(buffer.byteBufferData))));

		valuesSwath.add(new ValueD1S1(beamGrpVendor.getPing_validity(), (buffer) -> new SByte((byte) 0)));

		valuesRxAntenna.add(new ValueD2U1(beamGrpVendor.getRx_fans_per_ping(),
				(buffer, i) -> KmAllMDatagram.getRxFansPerPing(buffer.byteBufferData)));

		valuesRxAntenna.add(new ValueD2U1(beamGrpVendor.getRx_fan_index(),
				(buffer, i) -> KmAllMDatagram.getFanIndexU(buffer.byteBufferData)));

		valuesSwath.add(new ValueD1U1(xsf.getBathyGrp().getMultiping_sequence(),
				(buffer) -> KmAllMDatagram.getSwathAlongPosition(buffer.byteBufferData)));

		valuesRxAntenna.add(new ValueD2U1(beamGrpVendor.getNum_rx_transducers(),
				(buffer, i) -> KmAllMDatagram.getNumRxTransducers(buffer.byteBufferData)));

		valuesRxAntenna.add(new ValueD2U1(beamGrpVendor.getAlgorithm_type(),
				(buffer, i) -> KmAllMDatagram.getAlgorithmType(buffer.byteBufferData)));

		valuesRxAntenna.add(new ValueD2U2(beamGrpVendor.getTx_sector_count(),
				(buffer, i) -> MWC.getNumTxSectors(buffer.byteBufferData)));

		// TX info
		valuesSwathTxSector.add(new ValueD2F4(beamGrpVendor.getRaw_tx_beam_tilt_angle(), (buffer, i) -> {
			double angleDeg = MWC.getTiltAngleReTx_deg(buffer.byteBufferData, i).get();
			return new FFloat((float) angleDeg);
		}));

		// done with MRZ datagram
		valuesSwathTxSector.add(new ValueD2F4(beamGrpVendor.getCenter_frequency(),
				(buffer, i) -> MWC.getCentreFreq_Hz(buffer.byteBufferData, i)));

		valuesSwathTxSector.add(new ValueD2U2(beamGrpVendor.getTx_sector_number(),
				(buffer, i) -> MWC.getTxSectorNum(buffer.byteBufferData, i)));

		valuesSwathTxSector.add(new ValueD2F4(beamGrpVendor.getTx_along_beam_width(),
				(buffer, i) -> MWC.getTxBeamWidthAlong_deg(buffer.byteBufferData, i)));

		// General RX info
		valuesRxInfo.add(
				new ValueD2U2(beamGrpVendor.getBeam_count(), (buffer, i) -> MWC.getNumBeams(buffer.byteBufferData)));

		valuesRxInfo.add(new ValueD2S1(beamGrpVendor.getPhase_flag(),
				(buffer, i) -> new SByte(MWC.getPhaseFlag(buffer.byteBufferData).getByte())));

		valuesRxInfo.add(new ValueD2U1(beamGrpVendor.getTvg_function_applied(),
				(buffer, i) -> MWC.getTVGfunctionApplied(buffer.byteBufferData)));
		valuesRxInfo.add(new ValueD2S1(beamGrpVendor.getTvg_offset(),
				(buffer, i) -> MWC.getTVGoffset_dB(buffer.byteBufferData)));

		valuesSwath.add(new ValueD1F4(beamGrp.getSample_interval(),
				buffer -> new FFloat(1.f / (MWC.getSampleFreq_Hz(buffer.byteBufferData).get()))));

		valuesRxAntenna.add(new ValueD2F4(beamGrpVendor.getTx_heave(),
				(buffer, rxAntennaIdx) -> (MWC.getHeave_m(buffer.byteBufferData))));

		valuesSwath.add(new ValueD1F4(beamGrp.getSound_speed_at_transducer(),
				buffer -> MWC.getSoundVelocity_mPerSec(buffer.byteBufferData)));

		// values ping-beam
		valuesSwathBeam.add(new ValueD2U1(beamGrpVendor.getWc_validity(), (buffer, offset) -> new UByte((byte) 1),
				new UByte((byte) 0)));

		valuesSwathTxSector.add(new ValueD2S4(beamGrp.getTransmit_transducer_index(), (buffer, i) -> new SInt(
				allFile.getIIPMetadata().getTxIndex(KmAllMDatagram.getTxTransducerInd(buffer.byteBufferData).getU()))));

		// we extends getRxTransducerInd given per swath to a value given for each beam
		valuesSwathBeam.add(new ValueD2S4(beamGrpVendor.getRaw_receive_transducer_index(),
				(buffer, i) -> new SInt(KmAllMDatagram.getRxTransducerInd(buffer.byteBufferData).getU())));

		valuesSwathBeam.add(new ValueD2F4(beamGrpVendor.getRaw_rx_beam_pointing_angle(), (buffer, offset) -> {
			// compute beam pointing angle relative to vertical
			double angleDeg = MWC.getBeamPointAngReVertical_deg(buffer.byteBufferData, offset).get();
			return new FFloat((float) angleDeg);
		}));

		// getBeamwidth_receive_major is computed in post processing
		valuesSwathTxSector.add(new ValueD2F4(beamGrp.getBeamwidth_transmit_minor(), (buffer, txSectInd) -> {
			float beamWidth = MWC.getTxBeamWidthAlong_deg(buffer.byteBufferData, txSectInd).get();
			float txTiltAngle = MWC.getTiltAngleReTx_deg(buffer.byteBufferData, txSectInd).get();
			return new FFloat((float) Math.abs(beamWidth / Math.cos(Math.toRadians(txTiltAngle))));

		}));

		valuesSwathBeam.add(new ValueD2F4(beamGrp.getBlanking_interval(), (buffer, i) -> {
			float samplingFq = MWC.getSampleFreq_Hz(buffer.byteBufferData).get();
			float v = MWC.getStartRangeSampleNum(buffer.byteBufferData, i).getU() / samplingFq;
			return new FFloat(v);
		}));
		valuesSwathBeam.add(new ValueD2U2(beamGrp.getTransmit_beam_index(),
				(buffer, i) -> MWC.getBeamTxSectorNum(buffer.byteBufferData, i)));

		valuesSwathBeam.add(new ValueD2F4(beamGrpVendor.getBeam_detection_range(), (buffer, i) -> {
			if (MWC.getDgmVersion(buffer.byteBufferData).getU() > 0)
				return MWC.getDetectedRangeInSamplesHR(buffer.byteBufferData, i);
			return new FFloat(MWC.getDetectedRangeInSamples(buffer.byteBufferData, i).getU());
		}));

		valuesSwathBeam.add(new ValueD2F4(beamGrp.getDetected_bottom_range(), (buffer, i) -> {
			double soundSpeed = MWC.getSoundVelocity_mPerSec(buffer.byteBufferData).get();
			double sampleRate = MWC.getSampleFreq_Hz(buffer.byteBufferData).get();
			double rangeInSample = (MWC.getDgmVersion(buffer.byteBufferData).getU() > 0)
					? MWC.getDetectedRangeInSamplesHR(buffer.byteBufferData, i).get()
					: MWC.getDetectedRangeInSamples(buffer.byteBufferData, i).getU() * 1f;
			double rangeInMeter = soundSpeed * rangeInSample / (2 * sampleRate);
			return new FFloat((float) rangeInMeter);
		}));

		// declare backscatter variable
		beamGrp.getBackscatter_r();
		beamGrp.getBackscatter_r().addAttribute("scale_factor", 0.5f);// scale factor of 0.5f allowing to convert raw

		// declare sample_count variable
		beamGrp.getSample_count();

		// amplitude to float values
		if (metadata.hasPhaseValues()) {
			// declare phase variable
			beamGrp.getEchoangle_major();
		}
	}

	@Override
	public void fillData(KmallFile kmAllFile) throws IOException, NCException {

		long[] origin = { 0, 0 };

		final DatagramBuffer buffer = new DatagramBuffer();
		Vlen_t[] backscatter_phase = (Vlen_t[]) new Vlen_t().toArray(stats.getMWCMetadata().getMaxBeamCount());
		Vlen_t[] vlen_backscatter_amp = (Vlen_t[]) new Vlen_t().toArray(stats.getMWCMetadata().getMaxBeamCount());
		boolean firstPing = true;
		int[] received_transducer_index = null;

		received_transducer_index = new int[stats.getMWCMetadata().getMaxBeamCount()];
		Arrays.fill(received_transducer_index, stats.getIIPMetadata().getRxIndex(0)); // fill default values
		boolean hasPhaseValues = false;

		int[] sampleCount = new int[stats.getWcBeamCount()]; // number of sample per beam
		int[] rxAntennas = stats.getRxAntennas().stream().mapToInt(Integer::intValue).toArray();

		for (Entry<SwathIdentifier, MWCCompletePingMetadata> pingEntry : metadata.getPings()) {
			valuesRxAntenna.forEach(ValueD2::clear);
			valuesSwathTxSector.forEach(ValueD2::clear);
			valuesRxInfo.forEach(ValueD2::clear);
			valuesSwathBeam.forEach(ValueD2::clear);
			final MWCCompletePingMetadata ping = pingEntry.getValue();
			// Check if this datagram should be rejected
			if (!ping.isComplete(stats.getRxAntennas()) || !stats.getSwathIds().contains(pingEntry.getKey())) {
				continue;
			}
			// use same antenna order for all pings
			ping.setSerialNumbers(rxAntennas);

			Arrays.fill(sampleCount, 0); // fill default values

			// get origin from swath identifier (ignore if matching index not found)
			var optIndex = stats.getSwathIds().getIndex(pingEntry.getKey());
			if (optIndex.isEmpty()) {
				DefaultLogger.get()
						.warn("Datagram MRZ associated with a missing ping is ignored (number = {}, sequence = {}).",
								pingEntry.getKey().getRawPingNumber(), pingEntry.getKey().getSwathPosition());
				continue;
			}
			origin[0] = optIndex.get();

			for (Pair<DatagramPosition, MWCCompletePingAntennaMetadata> antenna : ping.locate()) {
				DatagramReader.readDatagram(antenna.getFirst().getFile(), buffer, antenna.getFirst().getSeek());
				int datagramVersion = MWC.getDgmVersion(buffer.byteBufferData).getU();

				for (ValueD1 v : valuesSwath) {
					v.fill(buffer);
				}

				for (ValueD2 v : valuesRxAntenna) {
					v.fill(buffer, 0, (int) MWC.getRxTransducerInd(buffer.byteBufferData).getU());
				}

				for (int i = 0; i < MWC.getNumTxSectors(buffer.byteBufferData).getU(); i++) {
					for (ValueD2 v : valuesSwathTxSector) {
						v.fill(buffer, i, i);
					}
				}

				int offset = MWC.getRxInfoOffset(buffer.byteBufferData);
				for (ValueD2 v : valuesRxInfo) {
					v.fill(buffer, offset, (int) MWC.getRxTransducerInd(buffer.byteBufferData).getU());
				}
				int transducerIndex = 0;
				if (firstPing) {
					// fill rxbeam info buffer
					transducerIndex = (int) MWC.getRxTransducerInd(buffer.byteBufferData).getU();
				}

				offset = MWC.getInitialRxBeamOffset(buffer.byteBufferData);
				int beamcount_per_datagram = MWC.getNumBeams(buffer.byteBufferData).getU();
				for (int i = 0; i < beamcount_per_datagram; i++) {
					final int globalBeam =
							i + ping.getBeamOffset(KmAllMDatagram.getRxTransducerInd(buffer.byteBufferData).getU());
					if (firstPing) {
						received_transducer_index[globalBeam] = transducerIndex;

					}
					for (ValueD2 v : valuesSwathBeam) {
						v.fill(buffer, offset, globalBeam);
					}

					{
						UShort numSamples = MWC.getNumSampleData(buffer.byteBufferData, offset);
						int count = numSamples.getU();
						if (count > 0) {
							int bufferlength = count; // 1 byte per sample
							Memory ampMem = new Memory(bufferlength);
							ByteBuffer local_buffer = ampMem.getByteBuffer(0, bufferlength);
							MWC.getSampleAmplitude05dB_p(buffer.byteBufferData, offset, local_buffer, datagramVersion);
							vlen_backscatter_amp[globalBeam].p = ampMem;
							vlen_backscatter_amp[globalBeam].len = bufferlength;
							sampleCount[globalBeam] = count;
						} else {
							vlen_backscatter_amp[globalBeam].p = null;
							vlen_backscatter_amp[globalBeam].len = 0;
							sampleCount[globalBeam] = 0;
						}
					}

					if (MWC.getPhaseFlag(buffer.byteBufferData).getU() > 0) {
						hasPhaseValues = true;
						int count = MWC.getNumSampleData(buffer.byteBufferData, offset).getU();
						if (count > 0) {
							int bufferlength = count * Float.BYTES;
							Memory phaseMem = new Memory(bufferlength);
							ByteBuffer local_buffer = phaseMem.getByteBuffer(0, bufferlength);
							MWC.getSamplePhase(buffer.byteBufferData, offset, local_buffer, datagramVersion);
							backscatter_phase[globalBeam].p = phaseMem;
							backscatter_phase[globalBeam].len = count;
						} else {
							backscatter_phase[globalBeam].p = null;
							backscatter_phase[globalBeam].len = 0;
						}
					}

					offset = MWC.getNextRxBeamOffset(buffer.byteBufferData, offset, datagramVersion);
				}
			}
			// really write the full (ie all antennas) bs variable
			beamGrp.getBackscatter_r()
					.put(new long[] { origin[0], 0, 0 }, new long[] { 1, stats.getMWCMetadata().getMaxBeamCount(), 1 },
							vlen_backscatter_amp);
			if (hasPhaseValues) {

				beamGrp.getEchoangle_major()
						.put(new long[] { origin[0], 0 }, new long[] { 1, stats.getMWCMetadata().getMaxBeamCount() },
								backscatter_phase);
			}

			// write indicative variable beam sample count
			beamGrp.getSample_count()
					.put(new long[] { origin[0], 0, 0 }, new long[] { 1, stats.getMWCMetadata().getMaxBeamCount(), 1 },
							sampleCount);

			// write rxbeam constant values
			if (firstPing) {
				NCVariable variable = beamGrp.getReceive_transducer_index();
				try {
					variable.put(new long[] { 0 }, new long[] { received_transducer_index.length },
							received_transducer_index);
				} catch (Exception e) {
					DefaultLogger.get().error(String.format("Error while writing variable %s (%s)", variable.getName(),
							e.getMessage()));
					throw e;
				}
				firstPing = false;
			}

			/*
			 * Effectively write datasets
			 */
			long[] originD1 = new long[] { origin[0] };

			for (ValueD1 v : valuesSwath) {
				v.write(originD1);
			}
			for (ValueD2 v : valuesRxAntenna) {
				v.write(origin, new long[] { 1, stats.getRxAntennaCount() });
			}
			for (ValueD2 v : valuesSwathTxSector) {
				v.write(origin, new long[] { 1, ping.getNumTxSector() });
			}
			for (ValueD2 v : valuesRxInfo) {
				v.write(origin, new long[] { 1, stats.getRxAntennaCount() });
			}
			for (ValueD2 v : valuesSwathBeam) {
				v.write(origin, new long[] { 1, ping.getBeamCount() });
			}
		}

	}

}
