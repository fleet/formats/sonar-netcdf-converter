package fr.ifremer.globe.api.xsf.converter.all.xsf;

import fr.ifremer.globe.api.xsf.converter.all.datagram.position.Position;

import java.nio.ByteBuffer;
import java.util.TreeMap;


/**
 * Tools for platform position and attitude interpolation
 */
public class PlatformInterpolator {
    private PlatformInterpolator() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Get interpolated latitude, longitude, speed, heading values for a specific timestamp.
     */
    public static double[] getPositionByInterpolation(long currentTime, TreeMap<Long, ByteBuffer> positionDatagrams) {

        if (positionDatagrams == null || positionDatagrams.size() < 2)
            return null;

        // Get position datagrams before and after the specified time stamp
        Long positionTimeBefore = positionDatagrams.floorKey(currentTime);
        Long positionTimeAfter = positionDatagrams.ceilingKey(currentTime);

        // If no datagram have been found before; use the 2 first datagrams
        if (positionTimeBefore == null) {
            positionTimeBefore = positionDatagrams.firstKey();
            positionTimeAfter = positionDatagrams.higherKey(positionTimeBefore);
        }
        // If no datagram have been found after; use the 2 last datagrams
        if (positionTimeAfter == null) {
            positionTimeAfter = positionDatagrams.lastKey();
            positionTimeBefore = positionDatagrams.lowerKey(positionTimeAfter);
        }

        ByteBuffer positionDatagramBefore = positionDatagrams.get(positionTimeBefore);
        ByteBuffer positionDatagramAfter = positionDatagrams.get(positionTimeAfter);

        // Linear interpolation
        double coeff = !positionTimeAfter.equals(positionTimeBefore)
                ? ((double) (currentTime - positionTimeBefore)) / (positionTimeAfter - positionTimeBefore)
                : 0;

        // Get values in degrees (see datagram formats doc)
        double latBefore = Position.getLatitude(positionDatagramBefore).get() / 2e7;
        double lonBefore = Position.getLongitude(positionDatagramBefore).get() / 1e7;
        double speedBefore = Position.getSpeedOverGround(positionDatagramBefore).getU() / 100d;
        double headingBefore = Math.toRadians(Position.getHeading(positionDatagramBefore).getU() / 100d);
        double cosHeadingBefore = Math.cos(headingBefore);
        double sinHeadingBefore = Math.sin(headingBefore);

        double latAfter = Position.getLatitude(positionDatagramAfter).get() / 2e7;
        double lonAfter = Position.getLongitude(positionDatagramAfter).get() / 1e7;
        double speedAfter = Position.getSpeedOverGround(positionDatagramAfter).getU() / 100d;
        double headingAfter = Math.toRadians(Position.getHeading(positionDatagramAfter).getU() / 100d);
        double cosHeadingAfter = Math.cos(headingAfter);
        double sinHeadingAfter = Math.sin(headingAfter);

        double lat = latBefore + coeff * (latAfter - latBefore);
        double lon = lonBefore + coeff * (lonAfter - lonBefore);
        double speed = speedBefore + coeff * (speedAfter - speedBefore);

        double cosHeading = cosHeadingBefore + coeff * (cosHeadingAfter - cosHeadingBefore);
        double sinHeading = sinHeadingBefore + coeff * (sinHeadingAfter - sinHeadingBefore);
        double heading = Math.toDegrees(Math.atan2(sinHeading, cosHeading));
        if (heading < 0.0)
            heading += 360.0;

        return new double[]{lat, lon, speed, heading};
    }

    /**
     * Get interpolated roll, pitch, heave values for a specific timestamp.
     */
    public static double[] getAttitudeByInterpolation(long currentTime, TreeMap<Long, short[]> attitudeValues) {

        // Get attitude values before and after the specified time stamp
        Long timeBefore = attitudeValues.floorKey(currentTime);
        Long timeAfter = attitudeValues.ceilingKey(currentTime);

        // If no attitude values have been found before; use the first value
        if (timeBefore == null) {
            timeBefore = attitudeValues.firstKey();
        }
        // If no attitude values have been found after; use the last value
        if (timeAfter == null) {
            timeAfter = attitudeValues.lastKey();
        }

        short[] attitudeBefore = attitudeValues.get(timeBefore);
        short[] attitudeAfter = attitudeValues.get(timeAfter);
        double rollBefore = attitudeBefore[0] / 100d;
        double pitchBefore = attitudeBefore[1] / 100d;
        double heaveBefore = attitudeBefore[2] / 100d;
        double rollAfter = attitudeAfter[0] / 100d;
        double pitchAfter = attitudeAfter[1] / 100d;
        double heaveAfter = attitudeAfter[2] / 100d;

        if (currentTime <= timeBefore) {
            return new double[]{rollBefore, pitchBefore, heaveBefore};
        } else if (currentTime >= timeAfter) {
            return new double[]{rollAfter, pitchAfter, heaveAfter};
        } else {
            // Linear interpolation
            double coeff = ((double) (currentTime - timeBefore)) / (timeAfter - timeBefore);

            double roll = rollBefore + coeff * (rollAfter - rollBefore);
            double pitch = pitchBefore + coeff * (pitchAfter - pitchBefore);
            double heave = heaveBefore + coeff * (heaveAfter - heaveBefore);

            return new double[]{roll, pitch, heave};
        }
    }
}
