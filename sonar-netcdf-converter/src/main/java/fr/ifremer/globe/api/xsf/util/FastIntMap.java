package fr.ifremer.globe.api.xsf.util;

import java.util.ArrayList;

/**
 * Array indexed map. Allocate an array of Object indexed by their id (int). The
 * aim is to have fast retrieval of objects in the map
 * <p>
 * the size of the array is computed by the maximum index put in map. So be
 * careful of the size allocated for this map.
 * 
 */
public class FastIntMap<Value> {

	ArrayList<Value> indexedValues = new ArrayList<>();
	Value defaultValue = null;

	public FastIntMap(Value defaultValue) {
		this.defaultValue = defaultValue;
	}

	/**
	 * Associates the specified value with the specified key in this map.
	 */
	public void put(int id, Value v) {
		indexedValues.ensureCapacity(id + 1);
		while (indexedValues.size() - 1 < id) {
			indexedValues.add(null);
		}
		indexedValues.set(id, v);
	}

	/**
	 * retrieve value associated with given id in this map, if not found return
	 * default value;
	 */
	public Value get(int id) {
		Value dg = null;
		if (id < indexedValues.size()) {
			dg = indexedValues.get(id);
		}
		if (dg != null) {
			return dg;
		} else {
			return defaultValue;
		}
	}

}
