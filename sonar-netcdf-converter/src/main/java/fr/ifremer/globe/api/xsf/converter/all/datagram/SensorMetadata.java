package fr.ifremer.globe.api.xsf.converter.all.datagram;

import java.util.Map;
import java.util.TreeMap;

import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.DatagramMetadata;

/**
 * Does metadata about a class of sensor. Multiple individual sensors
 * can share the same datagram, this class handles the dispatching.
 *
 */
public abstract class SensorMetadata<K extends Comparable<K>, M extends IndividualSensorMetadata> extends DatagramMetadata<K,M> {

    // by sensor individual metadata
    protected Map<K, M> bySensorIndex;
    private M individualSensorInstance;
    
    public SensorMetadata(M instance) {
        bySensorIndex = new TreeMap<K, M>();
        individualSensorInstance = instance;
    }
    
    @SuppressWarnings("unchecked")
    public M getOrCreateSensor(K key) {
        if (!bySensorIndex.containsKey(key)) {
            M newInstance;
            try {
                newInstance = (M) individualSensorInstance.getClass().newInstance();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            bySensorIndex.put(key, newInstance);
        }
        return bySensorIndex.get(key);
    }
    
    public interface SensorMetadataIteration<K, M> {
        public void forSensor(K key, M metadata);
    }
    
    public void forEachSensor(SensorMetadataIteration<K, M> op) {
        bySensorIndex.forEach((key, metadata) -> {
            op.forSensor(key, metadata);
        });
    }
    
    public  Map<K, M> getSensors() {
    	return bySensorIndex;
    }
}
