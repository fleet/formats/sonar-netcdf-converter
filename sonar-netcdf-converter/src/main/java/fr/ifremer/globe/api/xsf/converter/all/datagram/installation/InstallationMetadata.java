package fr.ifremer.globe.api.xsf.converter.all.datagram.installation;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import fr.ifremer.globe.api.xsf.converter.all.datagram.KmSounder;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.datagram.KmSounderLib;
import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.DatagramMetadata;
import fr.ifremer.globe.api.xsf.converter.common.exceptions.UnsupportedSounderException;
import fr.ifremer.globe.api.xsf.converter.common.utils.XLog;
import fr.ifremer.globe.api.xsf.util.types.UShort;

/**
 * Summarize the information contained in the Installation datagrams.
 *
 */
public class InstallationMetadata extends DatagramMetadata<Integer, DatagramPosition> {

	/**
	 * The Installation datagram encodes an String to String map.
	 */
	private Map<String, String> conf;
	private String rawConf;
	private UShort serialNumber;
	private UShort secondarySerialNumber;
	private int modelNumber;
	private int _SISVersion = -1;

	// Offset indiqués dans la doc de réf : 160692av_em_datagram_formats.pdf
	// page 115. Valeurs en XYZ données en mm.

	public final static double[][] TxOffset2040 = { { 0, -55.4e-3, -12e-3 }, { 0, 13.15e-3, -6e-3 }, { 0, 55.4e-3, -12e-3 } };

	public final static double[][] RxOffset2040 = { { 11e-3, 0, -6e-3 } };

	public final static double[][] TxOffset2040c = { { 3.8e-3, 40.0e-3, -6e-3 } };

	public final static double[][] RxOffset2040c = { { -45.5e-3, 0, -6e-3 } };

	public final static double[][] TxOffset2040p = { { 2e-3, -104.2e-3, -14.9e-3 }, { 2e-3, -0, -6e-3 },
			{ 2e-3, 104.2e-3, -14.9e-3 } };

	public final static double[][] RxOffset2040p = { { 204e-3, 0, -31.5e-3 } };

	public final static  double[][] TxOffset2040m = { { 0, -104e-3, 2e-3, -45.4e-3 }, { 0, -0, -6e-3 },
			{ 0, 104e-3, 2e-3, -45.4e-3 } };

	public final static  double[][] RxOffset2040m = { { 202e-3, 0, -31.5e-3 } };

	public enum AntennaType {
		RX, TX
	}

	public class Antenna {

		private UShort id;

		public UShort getId() {
			return id;
		}

		private double xOffset = 0;

		private double yOffset = 0;
		private double zOffset = 0;
		private double x, y, z = 0;
		private double heading, pitch, roll = 0;

		private final AntennaType type;

		Antenna(String parsing_keyname, UShort id, AntennaType type, Optional<double[]> xyz_offset) {
			this.id = id;
			this.type = type;
			// Ajout des offset.
			// TODO : apply offset in the good referential. Offset are given in transducer
			// referencial, not vessel referential like installation parameters.
			if (xyz_offset.isPresent()) {
				xOffset = xyz_offset.get()[0];
				yOffset = xyz_offset.get()[1];
				zOffset = xyz_offset.get()[2];
			}
			x = getDouble(parsing_keyname + "X");
			y = getDouble(parsing_keyname + "Y");
			z = getDouble(parsing_keyname + "Z");
			heading = getDouble(parsing_keyname + "H");
			pitch = getDouble(parsing_keyname + "P");
			roll = getDouble(parsing_keyname + "R");

		}

		public AntennaType getType() {
			return type;
		}

		public double getXWithoutOffset() {
			return x;
		}

		public double getYWithoutOffset() {
			return y;
		}

		public double getZWithoutOffset() {
			return z;
		}

		public double getX() {
			return x + xOffset;
		}

		public double getY() {
			return y + yOffset;
		}

		public double getZ() {
			return z + zOffset;
		}

		public double getH() {
			return heading;
		}

		public double getR() {
			return roll;
		}

		public double getP() {
			return pitch;
		}
	}

	protected List<Antenna> antennas;
	private KmSounder sounderDesc;

	public InstallationMetadata() {
		super();
		conf = new HashMap<>();
		antennas = new LinkedList<>();
	}

	public void addDatagram(DatagramPosition pos) {
		index.put(datagramCount, pos);
	}

	public int parseSISVersion() {
		int version = -1;
		// retrieve SIS version

		if (conf.containsKey("OSV")) {
			try {
				String[] data = conf.get("OSV").split("SIS ");
				if (data.length > 1) {
					version = Integer.valueOf(data[1].replaceAll("\\.", ""));
					version = version > 50 && version < 100 ? version * 10 : version;
				} else {
					XLog.warn("Missing OSV (System version) data, using default value ("+conf.get("OSV")+')');
				}
			} catch (Exception e) {
				XLog.error("Error while parsing SIS version information :" + conf.get("OSV"), e);
			}
		} else {
			XLog.warn("Missing OSV (System version) data, using default value");
		}

		return version;
	}

	/**
	 * 
	 * Parses the body of the Installation (string part) and stores it in the internal map.
	 *
	 * @param body The raw data extracted from the Installation datagram
	 * @see Installation.getBody
	 */
	public void setInstallation(String body) {
		// Decode string body
		this.rawConf = body;
		for (String pair : body.split(",")) {
			pair = pair.trim();
			if (pair != "") {
				String[] kv = pair.split("=");
				if (kv.length == 2) {
					conf.put(kv[0], kv[1]);
				}
			}
		}
		_SISVersion=parseSISVersion();
	}

	/**
	 * Retrieve a parameter by name
	 *
	 * @param key
	 * @return
	 */
	public String get(String key) {
		return conf.get(key);
	}

	/**
	 * Retrieve a numerical parameter by name
	 *
	 * @param key
	 * @return
	 */
	public int getInt(String key) {
		return Integer.parseInt(conf.get(key));
	}

	/**
	 * Retrieve a floating point value parameter by name
	 *
	 * @param key
	 * @return
	 */
	public float getFloat(String key) {
		return Float.parseFloat(conf.get(key));
	}

	public double getDouble(String key) {
		return Double.parseDouble(conf.get(key));
	}

	/**
	 * Tells if a parameter is present given its name.
	 * 
	 * @param key
	 * @return
	 */
	public boolean has(String key) {
		return conf.containsKey(key);
	}

	/**
	 * Gives the original, unmodified configuration description
	 * 
	 * @return
	 */
	public String getRawConf() {
		return rawConf;
	}

	public int getSISVersion() {
		return _SISVersion;
	}

	public UShort getSerialNumber() {
		return serialNumber;
	}

	public KmSounder getSounderDesc() {
		return sounderDesc;
	}

	public void setSerialNumber(UShort serialNumber) {
		this.serialNumber = serialNumber;
	}

	public UShort getSecondarySerialNumber() {
		return secondarySerialNumber;
	}

	public void setModelNumber(int modelNumber) {
		this.modelNumber = modelNumber;
	}

	public int[] getSerialNumbers() {
		if (secondarySerialNumber != null) {
			return new int[] { this.serialNumber.getU(), this.secondarySerialNumber.getU() };
		}
		return new int[] { this.serialNumber.getU() };
	}

	public int getModelNumber() {
		return this.modelNumber;
	}

	public void setSecondarySerialNumber(UShort secondarySerialNumber) {
		this.secondarySerialNumber = secondarySerialNumber;
	}

	public List<Antenna> getAntennas() {
		return antennas;
	}

	public int getFirstTxAntennaIndex() {

		int i = 0;
		for (Antenna antenna : antennas) {
			if (antenna.getType() == AntennaType.TX)
				return i;
			i++;
		}
		return i;
	}
	
	/**
	 * Convert km transducer index to the index in antenna declaration
	 */
	public int getTxTransducerIndex(int kongsbergIndex)
	{
		return getFirstTxAntennaIndex()+kongsbergIndex;
	}

	public Antenna getFirstTxAntenna() {
		for (Antenna antenna : antennas) {
			if (antenna.getType() == AntennaType.TX)
				return antenna;
		}
		return null;
	}

	/**
	 * Given a serial number, return its index (0 for primary, 1 for secondary)
	 * 
	 * @param serialNumber
	 * @return
	 */
	public int getRxAntennaIndex(UShort serialNumber) {
		int i = 0;
		for (Antenna a : antennas) {
			if (a.getType() == AntennaType.RX) {
				if (serialNumber.compareTo(a.id) == 0) {
					return i;
				}
			}
			i++;
		}
		// Retour a 0 pour s'assurer que les n° d'antennes ne soient pas en erreur si
		// ils sont
		// différents entre datagramme et paquet installation.
		// Cas du 0004_20030314_103209_raw.all.
		return 0;
	}

	public void analyse() throws UnsupportedSounderException {
		// Declaration des offset des Antennes.
		antennas = new LinkedList<>();
		int stc = 0;
		if (conf.containsKey("STC"))
			stc = getInt("STC");

		// Peut etre problematique si la configuration n'est pas clairement identifiee
		// (cas des fichiers anciens).
		else {
			switch (getModelNumber()) {

			case KmSounderLib.EM_2040:
				if (getSecondarySerialNumber().getU() >= 100)
					stc = 3; // 2040 dual RX
				break;
			case KmSounderLib.EM_2040C:
				stc = 1; // 2040C single head
				if(conf.containsKey("R2S"))
					stc = 2; // 2040C Dual
				break;
			case 3002:
				stc = 1;
			case 3020:// em3002
				// Cas du fichier 0009_20150203_210705_Belgica.all - Sondeur Dual Head
				if (getSecondarySerialNumber().getU() >= 100) {
					stc = 2; // EM3002 dual head
				}
			default:
				stc = 0; // single rx single tx

			}
		}
		// check for model type
		this.sounderDesc = new KmSounder(modelNumber, stc);

		UShort txSerialNumber = new UShort(has("TXS") ? Short.decode(get("TXS")) : 0);
		UShort txSecondary_serialNumber = new UShort(has("T2X") ? Short.decode(get("T2X")) : 1);

		UShort rxSerialNumber = getSerialNumber();
		UShort rxSecondary_serialNumber = getSecondarySerialNumber();

		switch (stc) {
		case 0: // single RX + single TX
			if (modelNumber == 850) { // ME70, transducer is TX and RX
				antennas.add(new Antenna("S1", rxSerialNumber, AntennaType.RX, Optional.empty()));
				antennas.add(new Antenna("S1", txSerialNumber, AntennaType.TX, Optional.empty()));
			} else if (modelNumber == 2040) {
				antennas.add(new Antenna("S2", rxSerialNumber, AntennaType.RX, Optional.of(RxOffset2040[0])));
				antennas.add(new Antenna("S1", txSerialNumber, AntennaType.TX, Optional.of(TxOffset2040[0])));
				antennas.add(new Antenna("S1", txSerialNumber, AntennaType.TX, Optional.of(TxOffset2040[1])));
				antennas.add(new Antenna("S1", txSerialNumber, AntennaType.TX, Optional.of(TxOffset2040[2])));
			} else if (modelNumber == 1002) {
				antennas.add(new Antenna("S1", rxSerialNumber, AntennaType.RX, Optional.empty()));
				antennas.add(new Antenna("S1", txSerialNumber, AntennaType.TX, Optional.empty()));
			} else {
				antennas.add(new Antenna("S2", rxSerialNumber, AntennaType.RX, Optional.empty()));
				antennas.add(new Antenna("S1", txSerialNumber, AntennaType.TX, Optional.empty()));
			}

			break;
		case 1:
			// EM3002 ou 2040c - single Head

			if (modelNumber == KmSounderLib.EM_2040C) // 2040c
			{
				antennas.add(new Antenna("S1", rxSerialNumber, AntennaType.RX, Optional.of(RxOffset2040c[0])));
				antennas.add(new Antenna("S1", txSerialNumber, AntennaType.TX, Optional.of(TxOffset2040c[0])));
			} else {
				antennas.add(new Antenna("S1", rxSerialNumber, AntennaType.RX, Optional.empty()));
				antennas.add(new Antenna("S2", txSerialNumber, AntennaType.TX, Optional.empty()));
			}
			break;
		case 2:
			if (modelNumber == KmSounderLib.EM_2040C)// 2040C dual head
			{
				antennas.add(new Antenna("S1", rxSerialNumber, AntennaType.RX, Optional.of(RxOffset2040c[0])));
				antennas.add(
						new Antenna("S2", rxSecondary_serialNumber, AntennaType.RX, Optional.of(RxOffset2040c[0])));

				antennas.add(new Antenna("S1", txSerialNumber, AntennaType.TX, Optional.of(TxOffset2040c[0])));
				antennas.add(
						new Antenna("S2", txSecondary_serialNumber, AntennaType.TX, Optional.of(TxOffset2040c[0])));
			} else {
				antennas.add(new Antenna("S1", rxSerialNumber, AntennaType.RX, Optional.empty()));
				antennas.add(new Antenna("S2", rxSecondary_serialNumber, AntennaType.RX, Optional.empty()));

				antennas.add(new Antenna("S1", txSerialNumber, AntennaType.TX, Optional.empty()));
				antennas.add(new Antenna("S2", txSecondary_serialNumber, AntennaType.TX, Optional.empty()));
			}
			break;
		case 3: // EM20 Dual Rx
			antennas.add(new Antenna("S2", rxSerialNumber, AntennaType.RX, Optional.of(RxOffset2040[0])));
			antennas.add(new Antenna("S3", rxSecondary_serialNumber, AntennaType.RX, Optional.of(RxOffset2040[0])));
			antennas.add(new Antenna("S1", txSerialNumber, AntennaType.TX, Optional.of(TxOffset2040[0])));
			antennas.add(new Antenna("S1", txSerialNumber, AntennaType.TX, Optional.of(TxOffset2040[1])));
			antennas.add(new Antenna("S1", txSerialNumber, AntennaType.TX, Optional.of(TxOffset2040[2])));

			break;
		case 4: // EM20 Dual Rx Dual Tx
			antennas.add(new Antenna("S2", rxSerialNumber, AntennaType.RX, Optional.empty()));
			antennas.add(new Antenna("S4", rxSecondary_serialNumber, AntennaType.RX, Optional.empty()));
			antennas.add(new Antenna("S0", txSerialNumber, AntennaType.TX, Optional.of(TxOffset2040[0])));
			antennas.add(new Antenna("S0", txSerialNumber, AntennaType.TX, Optional.of(TxOffset2040[1])));
			antennas.add(new Antenna("S0", txSerialNumber, AntennaType.TX, Optional.of(TxOffset2040[2])));

			antennas.add(new Antenna("S1", txSecondary_serialNumber, AntennaType.TX, Optional.of(TxOffset2040[0])));
			antennas.add(new Antenna("S1", txSecondary_serialNumber, AntennaType.TX, Optional.of(TxOffset2040[1])));
			antennas.add(new Antenna("S1", txSecondary_serialNumber, AntennaType.TX, Optional.of(TxOffset2040[2])));
			break;
		case 5:
			// EM2040P 1 RX, 3 TX arrays
			antennas.add(new Antenna("S1", rxSerialNumber, AntennaType.RX, Optional.of(RxOffset2040p[0])));
			antennas.add(new Antenna("S1", txSerialNumber, AntennaType.TX, Optional.of(TxOffset2040p[0])));
			antennas.add(new Antenna("S1", txSerialNumber, AntennaType.TX, Optional.of(TxOffset2040p[1])));
			antennas.add(new Antenna("S1", txSerialNumber, AntennaType.TX, Optional.of(TxOffset2040p[2])));
			break;
		case 6:
			// EM2040M
			antennas.add(new Antenna("S1", rxSerialNumber, AntennaType.RX, Optional.of(RxOffset2040m[0])));
			antennas.add(new Antenna("S1", txSerialNumber, AntennaType.TX, Optional.of(TxOffset2040m[0])));
			antennas.add(new Antenna("S1", txSerialNumber, AntennaType.TX, Optional.of(TxOffset2040m[1])));
			antennas.add(new Antenna("S1", txSerialNumber, AntennaType.TX, Optional.of(TxOffset2040m[2])));
			break;
		default:
			throw new UnsupportedSounderException("Unknown STC field: " + get("STC"));
		}

	}
}
