package fr.ifremer.globe.api.xsf.converter.all.xsf.runtime;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.all.datagram.IndividualSensorMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.all.datagram.runtime.RunTimeDg;
import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.datagram.KmPingModeMapper;
import fr.ifremer.globe.api.xsf.converter.common.datagram.KmPingModeMapper.AllPingMode;
import fr.ifremer.globe.api.xsf.converter.common.datagram.KmSounderLib;
import fr.ifremer.globe.api.xsf.converter.common.datagram.PulseLengthModeMapper;
import fr.ifremer.globe.api.xsf.converter.common.datagram.PulseLengthModeMapper.AllPulseLengthMode;
import fr.ifremer.globe.api.xsf.converter.common.utils.XLog;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.*;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfFromKongsberg;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.RuntimeGrp;
import fr.ifremer.globe.api.xsf.util.SimpleIdentifier;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.api.xsf.util.types.SByte;
import fr.ifremer.globe.api.xsf.util.types.SInt;
import fr.ifremer.globe.api.xsf.util.types.ULong;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.exception.GIOException;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

public class RunTimeGroup implements IDatagramConverter<AllFile, XsfFromKongsberg> {

	private List<ValueD1> values;
	private IndividualSensorMetadata sensorMetadata;

	private AllFile metadata;

	public RunTimeGroup(AllFile metadata) {
		this.metadata = metadata;
		this.sensorMetadata = metadata.getRunTime();
		this.values = new LinkedList<>();
	}

	@Override
	public void declare(AllFile allFile, XsfFromKongsberg xsf) throws NCException {
		RuntimeGrp platformVendorRuntimeGrp = xsf.getRuntime().get();

		// declare all the variables

		values.add(new ValueD1U8(platformVendorRuntimeGrp.getTime(),
				buffer -> new ULong(RunTimeDg.getXSFEpochTime(buffer.byteBufferData))));

		values.add(new ValueD1U2(platformVendorRuntimeGrp.getRuntime_raw_count(),
				buffer -> RunTimeDg.getCounter(buffer.byteBufferData)));

		values.add(new ValueD1U1(platformVendorRuntimeGrp.getOperator_station_status(),
				buffer -> RunTimeDg.getOperatorStationStatus(buffer.byteBufferData)));

		values.add(new ValueD1U1(platformVendorRuntimeGrp.getProcessing_unit_status(),
				buffer -> RunTimeDg.getProcessingUnitStatus(buffer.byteBufferData)));

		values.add(new ValueD1U1(platformVendorRuntimeGrp.getBsp_status(),
				buffer -> RunTimeDg.getBSPStatus(buffer.byteBufferData)));

		values.add(new ValueD1U1(platformVendorRuntimeGrp.getSonar_head_or_transceiver_status(),
				buffer -> RunTimeDg.getSonarHeadOrTransceiverStatus(buffer.byteBufferData)));

		values.add(new ValueD1U1(platformVendorRuntimeGrp.getMode(),
				buffer -> RunTimeDg.getMode(buffer.byteBufferData)));
		int modelNumber = allFile.getGlobalMetadata().getModelNumber();

		// if not EM2040 or 2040C
		if (modelNumber != KmSounderLib.EM_2040 && modelNumber != KmSounderLib.EM_2040C) {
			values.add(new ValueD1S1(platformVendorRuntimeGrp.getPing_mode(), buffer -> {
				try {
					return new SByte((byte) KmPingModeMapper.fromAll(
							AllPingMode.fromValue(RunTimeDg.getMode(buffer.byteBufferData).getU() & 0x0F)).value);
				} catch (GIOException e) {
					return new SByte((byte) KmPingModeMapper.XsfPingMode.UNKNOWN.value);
				}
			}));
			values.add(new ValueD1S4(platformVendorRuntimeGrp.getFrequency_mode(), b -> new SInt(-1)));
		} else {
			// if 2040 or 2040C we do not have any ping mode data
			values.add(new ValueD1S1(platformVendorRuntimeGrp.getPing_mode(),
					b -> new SByte((byte) KmPingModeMapper.XsfPingMode.UNKNOWN.value)));
			// set frequency mode now
			if (modelNumber == KmSounderLib.EM_2040) {
				values.add(new ValueD1S4(platformVendorRuntimeGrp.getFrequency_mode(), buffer -> {
					int mode = RunTimeDg.getMode(buffer.byteBufferData).getU() & 0x0F; //0000_1111
					if (mode == 0)
						return new SInt(200000);
					else if (mode == 1)
						return new SInt(300000);
					else
						return new SInt(400000);
				}));
			} else if (modelNumber == KmSounderLib.EM_2040C) {
				values.add(new ValueD1S4(platformVendorRuntimeGrp.getFrequency_mode(), buffer -> {
					int mode = RunTimeDg.getMode(buffer.byteBufferData).getU() & 0x1F; //0001_1111
					return new SInt(180_000 + 10_000 * mode);
				}));
			}

		}

		//Dual swath mode
		if (modelNumber == KmSounderLib.EM_2040 || modelNumber == KmSounderLib.EM_710
				|| modelNumber == KmSounderLib.EM_302 || modelNumber == KmSounderLib.EM_122) {
			// check for dual swath mode
			values.add(new ValueD1S1(platformVendorRuntimeGrp.getDual_swath_mode(), buffer -> {
				// no dual swath mode
				int mode = RunTimeDg.getMode(buffer.byteBufferData).getU() & 0xC0;
				if (mode == 0) // 0000 0000 => Off
					return new SByte((byte) 0);
				if (mode == 0x40) // 0100 0000 => Fixed
					return new SByte((byte) 1);
				if (mode == 0x80)
					return new SByte((byte) 2); // 1000 0000 => Dynamic
				return new SByte((byte) 2); // => default is not defined in specification, set to dual swath
			}));
		} else {
			values.add(new ValueD1S1(platformVendorRuntimeGrp.getDual_swath_mode(), buffer -> {
				// no dual swath mode
				return new SByte((byte) 0);
			}));
		}

		//tx Pulse form
		if (modelNumber == KmSounderLib.EM_2040 || modelNumber == KmSounderLib.EM_2040C
				|| modelNumber == KmSounderLib.EM_710 || modelNumber == KmSounderLib.EM_302
				|| modelNumber == KmSounderLib.EM_122) {
			// check for tx pulse form
			int pulseFormMask = modelNumber == KmSounderLib.EM_2040C ? 0x20 : 0x30;
			values.add(new ValueD1S1(platformVendorRuntimeGrp.getTx_pulse_form(), buffer -> {
				// no dual swath mode
				int mode = RunTimeDg.getMode(buffer.byteBufferData).getU() & pulseFormMask;
				if (mode == 0) // 0000 0000 => CW
					return new SByte((byte) 0);
				if (mode == 0x10) // 0001 0000 => Mixed
					return new SByte((byte) 1);
				if (mode == 0x20)
					return new SByte((byte) 2); // 0010 0000 => FM
				return new SByte((byte) 1); // => default is not defined in specification, set to Mixed
			}));
		} else {
			values.add(new ValueD1S1(platformVendorRuntimeGrp.getTx_pulse_form(), buffer -> {
				// no pulse form, set CW by default
				return new SByte((byte) 0);
			}));
		}

		values.add(new ValueD1U1(platformVendorRuntimeGrp.getFilter_identifier(),
				buffer -> RunTimeDg.getFilterIdentifier(buffer.byteBufferData)));

		values.add(new ValueD1U2(platformVendorRuntimeGrp.getMinimum_depth(),
				buffer -> RunTimeDg.getMinimumDepth(buffer.byteBufferData)));

		values.add(new ValueD1U2(platformVendorRuntimeGrp.getMaximum_depth(),
				buffer -> RunTimeDg.getMaximumDepth(buffer.byteBufferData)));

		values.add(new ValueD1F4(platformVendorRuntimeGrp.getAbsorption_coefficient(),
				buffer -> new FFloat(RunTimeDg.getAbsorptionCoefficient(buffer.byteBufferData).getU() * 0.01f)));

		values.add(new ValueD1U2(platformVendorRuntimeGrp.getTx_pulse_length(),
				buffer -> RunTimeDg.getTransmitPulseLength(buffer.byteBufferData)));

		values.add(new ValueD1S1(platformVendorRuntimeGrp.getTx_power_re_maximum(),
				buffer -> RunTimeDg.getTransmitPowerReMaximum(buffer.byteBufferData)));

		if (modelNumber == KmSounderLib.EM_1002) {
			//Runtime datagram is wrongly filled for EM1002. Force it to beamwidth 2x2 as in product description document
			values.add(new ValueD1F4(platformVendorRuntimeGrp.getTx_beamwidth(), buffer -> new FFloat(2.f)));

			values.add(new ValueD1F4(platformVendorRuntimeGrp.getReceiver_beamwidth(),
					buffer -> new FFloat(2.f)));
		} else {
			values.add(new ValueD1F4(platformVendorRuntimeGrp.getTx_beamwidth(),
					buffer -> new FFloat(RunTimeDg.getTransmitBeamwidth(buffer.byteBufferData).getU() * 0.1f)));

			values.add(new ValueD1F4(platformVendorRuntimeGrp.getReceiver_beamwidth(),
					buffer -> new FFloat(RunTimeDg.getReceiveBeamwidth(buffer.byteBufferData).getU() * 0.1f)));
		}

		values.add(new ValueD1F4(platformVendorRuntimeGrp.getReceiver_bandwith(),
				buffer -> new FFloat(RunTimeDg.getReceiveBandwidth(buffer.byteBufferData).getU() * 50)));

		// Cas du mode 2 ou Receiver fixed gain

		if (metadata.getGlobalMetadata().getModelNumber() == KmSounderLib.EM_2040
				|| metadata.getGlobalMetadata().getModelNumber() == KmSounderLib.EM_2040C) {
			values.add(new ValueD1U1(platformVendorRuntimeGrp.getMode2(),
					buffer -> RunTimeDg.getMode2(buffer.byteBufferData)));

			// same value but decoded
			if (modelNumber == KmSounderLib.EM_2040) {
				values.add(new ValueD1S1(platformVendorRuntimeGrp.getPulse_length_mode(), buffer -> {
					int v = RunTimeDg.getMode2(buffer.byteBufferData).getInt();// xxxx 1100
					try {
						return new SByte(
								(byte) PulseLengthModeMapper.fromAll(AllPulseLengthMode.from2040Value(v)).value);
					} catch (GIOException e) {
						XLog.error("Error while parsing runtimeGroup", e);
						return new SByte((byte) PulseLengthModeMapper.XSFPulseLengthMode.UNKNOWN.value);
					}
				}));
			}
			if (modelNumber == KmSounderLib.EM_2040C) {
				values.add(new ValueD1S1(platformVendorRuntimeGrp.getPulse_length_mode(), buffer -> {
					try {
						int v = RunTimeDg.getMode2(buffer.byteBufferData).getInt();// x000 xxxx
						return new SByte(
								(byte) PulseLengthModeMapper.fromAll(AllPulseLengthMode.from2040CValue(v)).value);
					} catch (GIOException e) {
						XLog.error("Error while parsing runtimeGroup", e);
						return new SByte((byte) PulseLengthModeMapper.XSFPulseLengthMode.UNKNOWN.value);
					}
				}));

			}
		} else { // neither 2040 nor 2040C
			values.add(new ValueD1U1(platformVendorRuntimeGrp.getReceiver_fixed_gain(),
					buffer -> RunTimeDg.getReceiverFixedGain(buffer.byteBufferData)));
			values.add(new ValueD1S1(platformVendorRuntimeGrp.getPulse_length_mode(),
					buffer -> new SByte((byte) PulseLengthModeMapper.XSFPulseLengthMode.UNKNOWN.value)));
		}

		values.add(new ValueD1F4(platformVendorRuntimeGrp.getTVG_law_crossover_angle(),
				buffer -> new FFloat(RunTimeDg.getTVGLawCrossoverAngle(buffer.byteBufferData).getU())));

		values.add(new ValueD1U1(platformVendorRuntimeGrp.getSource_sound_speed_transducer(),
				buffer -> RunTimeDg.getSourceSoundSpeedTransducer(buffer.byteBufferData)));

		values.add(new ValueD1U2(platformVendorRuntimeGrp.getMaximum_port_swath_width(),
				buffer -> RunTimeDg.getMaximumPortSwathWidth(buffer.byteBufferData)));

		values.add(new ValueD1U1(platformVendorRuntimeGrp.getBeam_spacing(),
				buffer -> RunTimeDg.getBeamSpacing(buffer.byteBufferData)));

		values.add(new ValueD1U1(platformVendorRuntimeGrp.getMaximum_port_coverage(),
				buffer -> RunTimeDg.getMaximumPortCoverage(buffer.byteBufferData)));

		values.add(new ValueD1U1(platformVendorRuntimeGrp.getYaw_pitch_stabilization_mode(),
				buffer -> RunTimeDg.getYawAndPitchStabilizationMode(buffer.byteBufferData)));

		values.add(new ValueD1U1(platformVendorRuntimeGrp.getMaximum_starboard_coverage(),
				buffer -> RunTimeDg.getMaximumStarboardCoverage(buffer.byteBufferData)));

		values.add(new ValueD1U2(platformVendorRuntimeGrp.getMaximum_starboard_swath_width(),
				buffer -> RunTimeDg.getMaximumStarboardSwath(buffer.byteBufferData)));

		// Cas du mode 2 ou Receiver fixed gain
		if (metadata.getGlobalMetadata().getModelNumber() == 1002) {
			values.add(new ValueD1U2(platformVendorRuntimeGrp.getDurotong_speed(),
					buffer -> RunTimeDg.getDurotongSpeed(buffer.byteBufferData)));
		} else {
			values.add(new ValueD1F4(platformVendorRuntimeGrp.getTx_along_tilt(),
					buffer -> new FFloat(RunTimeDg.getTransmitAlongTilt(buffer.byteBufferData).get() * 0.1f)));
		}

		// Cas du mode 2 ou Receiver fixed gain
		if (metadata.getGlobalMetadata().getModelNumber() == 1002) {
			values.add(new ValueD1U1(platformVendorRuntimeGrp.getHilo_frequency_absorption_coef(),
					buffer -> RunTimeDg.getHiLoFrequencyAbsorptionCoef(buffer.byteBufferData)));
		} else {
			values.add(new ValueD1U1(platformVendorRuntimeGrp.getFilter_identifier2(),
					buffer -> RunTimeDg.getFilterIdentifier2(buffer.byteBufferData)));
		}
	}

	@Override
	public void fillData(AllFile allFile) throws IOException, NCException {
		long[] origin = new long[] { 0 };

		DatagramBuffer buffer = new DatagramBuffer(metadata.getByteOrder());
		for (Entry<SimpleIdentifier, DatagramPosition> posentry : sensorMetadata.getDatagrams()) {
			final DatagramPosition pos = posentry.getValue();
			DatagramReader.readDatagramAt(pos.getFile(), buffer, pos.getSeek());

			// read the data from the source file
			for (ValueD1 value : this.values) {
				value.fill(buffer);
			}

			// flush into the output file
			for (ValueD1 value : this.values) {
				value.write(origin);
			}
			origin[0] += 1;
		}
	}

}
