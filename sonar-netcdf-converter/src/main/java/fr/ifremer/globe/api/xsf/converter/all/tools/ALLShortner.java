package fr.ifremer.globe.api.xsf.converter.all.tools;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.common.utils.DateUtil;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;

public class ALLShortner {
    static String file = "";
    static String dest = "";

    public static void main(String[] args) throws FileNotFoundException, IOException {
        try (RandomAccessFile raf = new RandomAccessFile(file, "r")) {
            final File outFile = new File(dest);
            if (outFile.exists()) {
                outFile.delete();
            }

            try (FileOutputStream output = new FileOutputStream(dest)) {                
                final DatagramBuffer buffer = new DatagramBuffer(DatagramReader.getByteOrder(raf));
                long ref_time = -1;

                while (DatagramReader.readDatagram(raf, buffer) > 0) {
                    if (ref_time < 0) {
                        ref_time = getEpochTime(buffer.byteBufferData);
                    } else {
                        if ((getEpochTime(buffer.byteBufferData) - ref_time) > 10000) {
                            break;
                        }
                    }
                    output.write(buffer.rawBufferHeader);
                    output.write(buffer.rawBufferData, 0, buffer.byteBufferData.limit());
                }
            }
        }
    }
    
    public static long getEpochTime(ByteBuffer datagram) {
        long dateKongsbergFormat = TypeDecoder.read4U(datagram, 4).getU();
        // time since midnight in milliseconds
        long timeSinceMidnight = TypeDecoder.read4U(datagram, 8).getU();
        long epochTime = DateUtil.convertKongsbergTime(dateKongsbergFormat, timeSinceMidnight);
        return epochTime;
    }
}
