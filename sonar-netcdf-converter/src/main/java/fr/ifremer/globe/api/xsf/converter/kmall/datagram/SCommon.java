package fr.ifremer.globe.api.xsf.converter.kmall.datagram;

import java.nio.ByteBuffer;

import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.UShort;

public abstract class SCommon extends KmAllBaseDatagram {

    public static UShort getNumBytesCmnPart(ByteBuffer buffer) {
        return TypeDecoder.read2U(buffer, 16);
    }
    public static UShort getSensorSystem(ByteBuffer buffer) {
    	return TypeDecoder.read2U(buffer, 18);
    }
    public static UShort getSensorStatus(ByteBuffer buffer) {
    	return TypeDecoder.read2U(buffer, 20);
    }
    
}
