package fr.ifremer.globe.api.xsf.converter.kmall.datagram;

import java.nio.ByteBuffer;


import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.KmallFile;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.MWCCompletePingAntennaMetadata;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.MWCCompletePingMetadata;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.MWCMetadata;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.api.xsf.util.types.SByte;
import fr.ifremer.globe.api.xsf.util.types.UByte;
import fr.ifremer.globe.api.xsf.util.types.UShort;

public class MWC extends KmAllMDatagram {
	public static void dump4debug(ByteBuffer datagram)
	{
		System.err.println("-----------------------");
		System.err.println("MWC");

		KmAllMDatagram.dump4debug(datagram);

		System.err.println("NumBytesTxInfo:"+getNumBytesTxInfo(datagram));
		System.err.println("NumTxSectors:"+getNumTxSectors(datagram).getU());
		System.err.println("NumBytesPerTxSector:"+getNumBytesPerTxSector(datagram));
		System.err.println("Heave_m:"+getHeave_m(datagram).get());


		// 
		System.err.println("--SECTORS");
		for(int tx =0;tx<getNumTxSectors(datagram).getU();tx++)
		{
			System.err.println("TiltAngleReTx_deg ["+tx+"]:"+getTiltAngleReTx_deg(datagram,tx).get());
			System.err.println("CentreFreq_Hz ["+tx+"]:"+getCentreFreq_Hz(datagram,tx).get());
			System.err.println("TxBeamWidthAlong_deg ["+tx+"]:"+getTxBeamWidthAlong_deg(datagram,tx).get());
			System.err.println("TxSectorNum ["+tx+"]:"+getTxSectorNum(datagram,tx).getU());
		}
		System.err.println("NumBytesRxInfo:"+getNumBytesRxInfo(datagram));
		System.err.println("NumBeams:"+getNumBeams(datagram).getU());
		System.err.println("NumBytesPerBeamEntry:"+getNumBytesPerBeamEntry(datagram));
		System.err.println("PhaseFlag:"+getPhaseFlag(datagram).getU());
		System.err.println("TVGfunctionApplied:"+getTVGfunctionApplied(datagram).getU());
		System.err.println("TVGoffset_dB:"+getTVGoffset_dB(datagram).get());
		System.err.println("SampleFreq_Hz:"+getSampleFreq_Hz(datagram).get());
		System.err.println("SoundVelocity_mPerSec:"+getSoundVelocity_mPerSec(datagram).get());
		int nBeams = getNumBeams(datagram).getU();
		int datagramVersion = getDgmVersion(datagram).getU();

		System.err.println("--RX BEAMS");
		int offset = getInitialRxBeamOffset(datagram);
		for (int beam=0;beam<nBeams;beam++)
		{

			System.err.println("beam "+beam);

			System.err.println("BeamPointAngReVertical_deg:"+getBeamPointAngReVertical_deg(datagram,offset).get());
			System.err.println("StartRangeSampleNum:"+getStartRangeSampleNum(datagram,offset).getU());
			System.err.println("DetectedRangeInSamples:"+getDetectedRangeInSamples(datagram,offset).getU());
			System.err.println("BeamTxSectorNum:"+getBeamTxSectorNum(datagram,offset).getU());
			System.err.println("NumSampleData:"+getNumSampleData(datagram,offset).getU());
			if(datagramVersion>=1)
				System.err.println("DetectedRangeInSamplesHighResolution:"+	getDetectedRangeInSamplesHR(datagram,offset).get());

			offset = getNextRxBeamOffset(datagram, offset,datagramVersion);
		}
		System.err.println("-----------------------");

	}
	@Override
	public MWCMetadata getSpecificMetadata(KmallFile metadata) {
		return metadata.getMWCMetadata();
	}

	@Override
	public void computeSpecificMMetadata(KmallFile metadata, ByteBuffer datagram, String datagramType,
			SwathIdentifier swathNum, DatagramPosition position) {
		MWCMetadata md = getSpecificMetadata(metadata);

		MWCCompletePingMetadata completing = md.getPing(swathNum);
		if (completing == null) {
			completing = new MWCCompletePingMetadata(getNumTxSectors(datagram).getU(), new int[] {});
			md.addPing(swathNum, completing);
		}


	//	dump4debug(datagram);
		int datagramVersion = getDgmVersion(datagram).getU();
		int nBeams = getNumBeams(datagram).getU();
		int maxSamples = 0;
		int offset = getInitialRxBeamOffset(datagram);
		for (int i = 0; i < nBeams; i++) {
			try {
				maxSamples = Math.max(maxSamples, getNumSampleData(datagram, offset).getU());
				offset = getNextRxBeamOffset(datagram, offset,datagramVersion);
			} catch(IndexOutOfBoundsException e) {
				break;
			}
		}
		if(MWC.getPhaseFlag(datagram).getU()>0)
		{
			md.setHasPhaseValues(true);
		}
		completing.addAntenna(getRxTransducerInd(datagram).getU(),
				new MWCCompletePingAntennaMetadata(position, nBeams,maxSamples));
	}

	/**
	 * return the offset of ping info part in byte buffer
	 * */
	private static int getPingInfoDatagramOffset(ByteBuffer buffer)
	{
		return HEADER_SIZE-DATAGRAMM_OFFSET+PARTITION_SIZE+getNumByteCmnPart(buffer);
	}
	public static final int MWC_TXINFO_SIZE= 12; //size in octet of header definition  struct EMdgmHeader_def 



	// txSectorInfo
	public static int getTxSectorOffset(ByteBuffer buffer, int txSectorIndex) {
		return getPingInfoDatagramOffset(buffer) + MWC_TXINFO_SIZE + txSectorIndex * getNumBytesPerTxSector(buffer);
	}

	public static int getNumBytesTxInfo(ByteBuffer buffer) {
		return TypeDecoder.read2U(buffer, getPingInfoDatagramOffset(buffer)).getU();
	}

	public static UShort getNumTxSectors(ByteBuffer buffer) {
		return TypeDecoder.read2U(buffer, getPingInfoDatagramOffset(buffer)+2);
	}

	public static int getNumBytesPerTxSector(ByteBuffer buffer) {
		return TypeDecoder.read2U(buffer, getPingInfoDatagramOffset(buffer)+4).getU();
	}

	public static FFloat getHeave_m(ByteBuffer buffer) {
		return TypeDecoder.read4F(buffer, getPingInfoDatagramOffset(buffer)+8);
	}

	// EMdgmMWCtxSectorData
	public static FFloat getTiltAngleReTx_deg(ByteBuffer buffer, int txSectInd) {
		return TypeDecoder.read4F(buffer, getTxSectorOffset(buffer,txSectInd) );
	}
	public static FFloat getCentreFreq_Hz(ByteBuffer buffer, int txSectInd) {
		return TypeDecoder.read4F(buffer, getTxSectorOffset(buffer,txSectInd) + 4);
	}
	public static FFloat getTxBeamWidthAlong_deg(ByteBuffer buffer, int txSectInd) {
		return TypeDecoder.read4F(buffer, getTxSectorOffset(buffer,txSectInd) + 8);
	}
	public static UShort getTxSectorNum(ByteBuffer buffer, int txSectInd) {
		return TypeDecoder.read2U(buffer, getTxSectorOffset(buffer,txSectInd) + 12);
	}

	// rxInfo
	public static int getRxInfoOffset(ByteBuffer buffer) {
		return getTxSectorOffset(buffer,getNumTxSectors(buffer).getU());
	}
	public static int getNumBytesRxInfo(ByteBuffer buffer) {
		return TypeDecoder.read2U(buffer, getRxInfoOffset(buffer)).getU();
	}
	public static UShort getNumBeams(ByteBuffer buffer) {
		return TypeDecoder.read2U(buffer,  getRxInfoOffset(buffer) + 2);
	}
	public static int getNumBytesPerBeamEntry(ByteBuffer buffer) {
		return TypeDecoder.read1U(buffer,  getRxInfoOffset(buffer) + 4).getU();
	}
	public static UByte getPhaseFlag(ByteBuffer buffer) {
		return TypeDecoder.read1U(buffer,  getRxInfoOffset(buffer) + 5);
	}
	public static UByte getTVGfunctionApplied(ByteBuffer buffer) {
		return TypeDecoder.read1U(buffer,  getRxInfoOffset(buffer) + 6);
	}
	public static SByte getTVGoffset_dB(ByteBuffer buffer) {
		return TypeDecoder.read1S(buffer,  getRxInfoOffset(buffer) + 7);
	}
	public static FFloat getSampleFreq_Hz(ByteBuffer buffer) {
		return TypeDecoder.read4F(buffer,  getRxInfoOffset(buffer) + 8);
	}
	public static FFloat getSoundVelocity_mPerSec(ByteBuffer buffer) {
		return TypeDecoder.read4F(buffer,  getRxInfoOffset(buffer) + 12);
	}
	/**
	 * return the size if EMdgmMWCrxBeamData header (size of the struct except for samples)
	 * */
	private static int getEMdgmMWCrxBeamDataSize(int datagramVersion)
	{
		if(datagramVersion == 0 )
			return 12;
		return 16;// detectedRangeInSamplesHighResolution was added in version 1

	}

	// EMdgmMWCrxBeamData
	public static int getInitialRxBeamOffset(ByteBuffer buffer) {
		return getRxInfoOffset(buffer)+getNumBytesRxInfo(buffer);
	}
	public static int getNextRxBeamOffset(ByteBuffer buffer, int currOffset,int datagramVersion) {
		UByte phase= getPhaseFlag(buffer);
		return currOffset + getEMdgmMWCrxBeamDataSize(datagramVersion) /**Size of EMdgmMWCrxBeamData*/ + getNumSampleData(buffer, currOffset).getU()*(phase.getU()+1);
	}
	public static FFloat getBeamPointAngReVertical_deg(ByteBuffer buffer, int rxBeamOffset) {
		return TypeDecoder.read4F(buffer, rxBeamOffset);
	}
	public static UShort getStartRangeSampleNum(ByteBuffer buffer, int rxBeamOffset) {
		return TypeDecoder.read2U(buffer, rxBeamOffset + 4);
	}
	public static UShort getDetectedRangeInSamples(ByteBuffer buffer, int rxBeamOffset) {
		return TypeDecoder.read2U(buffer, rxBeamOffset + 6);
	}
	public static UShort getBeamTxSectorNum(ByteBuffer buffer, int rxBeamOffset) {
		return TypeDecoder.read2U(buffer, rxBeamOffset + 8);
	}
	public static UShort getNumSampleData(ByteBuffer buffer, int rxBeamOffset) {
		return TypeDecoder.read2U(buffer, rxBeamOffset + 10);
	}
	public static FFloat getDetectedRangeInSamplesHR(ByteBuffer buffer, int rxBeamOffset) {
		checkMinimuVersion(1, buffer);
		return TypeDecoder.read4F(buffer, rxBeamOffset + 12);
	}
	public static byte[] getSampleAmplitude05dB_p(ByteBuffer buffer, int rxBeamOffset,int datagramVersion) {
		final int nSamples = getNumSampleData(buffer, rxBeamOffset).getU();

		byte[] ret = new byte[nSamples];
		int sampleOffset = getEMdgmMWCrxBeamDataSize( datagramVersion);
		if (buffer.hasArray()) {
			System.arraycopy(buffer.array(), rxBeamOffset + sampleOffset, ret, 0, nSamples);
		} else {
			int currPos = buffer.position();
			buffer.position(rxBeamOffset + sampleOffset);
			buffer.get(ret, 0, nSamples);
			buffer.position(currPos);
		}

		return ret;
	}
	public static void getSampleAmplitude05dB_p(ByteBuffer buffer, int rxBeamOffset,ByteBuffer output,int datagramVersion) {
		final int nSamples = getNumSampleData(buffer, rxBeamOffset).getU();
		int currPos = buffer.position(); //retain old position
		int currLim=buffer.limit();//retain old limit
		int sampleOffset = getEMdgmMWCrxBeamDataSize( datagramVersion);
		buffer.position(rxBeamOffset + sampleOffset);//set position to start of wc
		buffer.limit(buffer.position()+nSamples); //set limit to end of wc
		output.put(buffer);  //copy data to output
		buffer.limit(currLim); //reset limit
		buffer.position(currPos);//reset position
	}

	/**
	 * retrieve phase information as byte in degree
	 * */
	public static float[] getSamplePhase(ByteBuffer buffer, int rxBeamOffset,int datagramVersion) {
		final int nSamples = getNumSampleData(buffer, rxBeamOffset).getU();
		short phase= getPhaseFlag(buffer).getU();
		float[] ret = new float[nSamples];
		int byteLength=nSamples*phase; 	//total number of byte to read
		ByteBuffer readB=ByteBuffer.allocate(byteLength);

		int sampleOffset = getEMdgmMWCrxBeamDataSize( datagramVersion);
		int currPos = buffer.position();
		buffer.position(rxBeamOffset + sampleOffset+nSamples);
		buffer.get(readB.array(), 0, byteLength);
		buffer.position(currPos);
		for(int i=0;i<nSamples;i++)
		{
			if(phase==1)
			{
				ret[i]=readB.get(i)*180f/128;
			}
			if(phase==2)
			{
				ret[i]=readB.getShort(i*Short.BYTES)*0.01f;
			}
		}

		return ret;
	}

	/**
	 * retrieve phase information as byte in degree
	 * */
	public static void getSamplePhase(ByteBuffer buffer, int rxBeamOffset,ByteBuffer output,int datagramVersion) {
		final int nSamples = getNumSampleData(buffer, rxBeamOffset).getU();
		short phase= getPhaseFlag(buffer).getU();
		int byteLength=nSamples*phase; 	//total number of byte to read
		ByteBuffer readB=ByteBuffer.allocate(byteLength);

		int currPos = buffer.position();
		int sampleOffset = getEMdgmMWCrxBeamDataSize( datagramVersion);

		buffer.position(rxBeamOffset + sampleOffset+nSamples);
		buffer.get(readB.array(), 0, byteLength);
		buffer.position(currPos);
		for(int i=0;i<nSamples;i++)
		{
			if(phase==1)
			{
				output.putFloat(readB.get(i)*180f/128);
			}
			if(phase==2)
			{
				output.putFloat(readB.getShort(i*Short.BYTES)*0.01f);
			}
		}

	}
}
