package fr.ifremer.globe.api.xsf.converter.common.utils.values.d2;

import java.util.Arrays;

import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.api.xsf.util.types.DDouble;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCVariable;

/***
 * class allocating storage for 2D matrix, and giving a interface allowing to fill the data
 */
public class ValueD2F8 extends ValueD2 {
	protected double[] dataOut;
	private ValueProvider valueProvider;

	/***
	 * Constructor, allocate netcdf buffer for one variable and one ping
	 */
	public ValueD2F8(NCVariable variable, ValueProvider filler) {
		super(variable);
		// allocate storage
		this.dataOut = new double[dim];
		clear();
		this.valueProvider = filler;
	}

	public ValueD2F8(NCVariable variable, int dim, ValueProvider filler) {
		super(variable, dim);
		// allocate storage
		this.dataOut = new double[dim];
		clear();
		this.valueProvider = filler;
	}
	
	@Override
	public void clear() {
		Arrays.fill(this.dataOut, variable.getDoubleFillValue());
	}

	public interface ValueProvider {
		public DDouble get(BaseDatagramBuffer buffer, int beamIndexSource);
	}

	/**
	 * call the lambda to retrieve the value and set it in the allocated netcdf buffer dataOut
	 */
	@Override
	public void fill(BaseDatagramBuffer buffer, int beamIndexSource, int beamIndexDest) {
		dataOut[beamIndexDest] = valueProvider.get(buffer, beamIndexSource).get();
	}

	@Override
	public void write(long[] origin, long[] count) throws NCException {
		try {
			variable.put(origin, count, dataOut);
		} catch (Exception e) {
			logger.error(String.format("Error while writing variable %s (%s)", variable.getName(), e.getMessage()));
			throw e;
		}
	}
}
