package fr.ifremer.globe.api.xsf.converter.common.exceptions;

import java.io.IOException;

public class ByteOrderDetectionFailed extends IOException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

}
