package fr.ifremer.globe.api.xsf.converter.common.xsf.types;

public enum beam_t {
	single((byte)0), split_aperture((byte)1);
	private byte value;

	public byte getValue() {
		return value;
	}

	beam_t(byte value)
	{
		this.value=value;
	}

}
