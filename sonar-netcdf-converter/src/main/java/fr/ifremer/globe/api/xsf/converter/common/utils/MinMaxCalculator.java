package fr.ifremer.globe.api.xsf.converter.common.utils;

import java.lang.reflect.Constructor;

public class MinMaxCalculator<M extends Number & Comparable<M>> {

	private M min;
	private M max;
	private int count;
	private double sum;
	/***/
	private M defaultValue;
	
	/**
	 * Constructor
	 * @param defaultValue value returned by default when no data have been added
	 * */
	protected MinMaxCalculator(M defaultValue) {
		this.min = null;
		this.max = null;
		this.count = 0;
		this.sum = 0.0;
		this.defaultValue=defaultValue;
	}
	
	public void register(M v) {
		if (min == null) {
			min = v;
		} else {
			if (min.compareTo(v) > 0) {
				this.min = v;
			}
		}
		
		if (max == null) {
			max = v;
		} else {
			if (max.compareTo(v) < 0) {
				this.max = v;
			}
		}
		
		count++;
		this.sum += v.doubleValue();
	}
	
	public M getMin() {
		if(this.min==null) return defaultValue; else return this.min;
	}
	
	public M getMax() {
		if(this.max==null) return defaultValue; else return this.max;
	}
	
	public double getMean() {
		if(this.count==0) return Double.NaN; else return this.sum / this.count;
	}
}
