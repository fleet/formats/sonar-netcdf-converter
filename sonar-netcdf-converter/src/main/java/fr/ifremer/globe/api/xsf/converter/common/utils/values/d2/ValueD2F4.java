package fr.ifremer.globe.api.xsf.converter.common.utils.values.d2;

import java.util.Arrays;

import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCVariable;

/***
 * class allocating storage for 2D matrix, and giving a interface allowing to fill the data
 */
public class ValueD2F4 extends ValueD2 {
	protected float[] dataOut;
	private ValueProvider valueProvider;

	/***
	 * Constructor, allocate netcdf buffer for one variable and one ping
	 */
	public ValueD2F4(NCVariable variable, ValueProvider filler) {
		super(variable);
		// allocate storage
		this.dataOut = new float[dim];
		clear();
		this.valueProvider = filler;
	}

	public ValueD2F4(NCVariable variable, int dim, ValueProvider filler) {
		super(variable, dim);
		// allocate storage
		this.dataOut = new float[this.dim];
		clear();
		this.valueProvider = filler;
	}
	
	@Override
	public void clear() {
		Arrays.fill(this.dataOut, variable.getFloatFillValue());
	}

	public interface ValueProvider {
		public FFloat get(BaseDatagramBuffer buffer, int beamIndexSource);
	}

	/**
	 * call the lambda to retrieve the value and set it in the allocated netcdf buffer dataOut
	 */
	@Override
	public void fill(BaseDatagramBuffer buffer, int beamIndexSource, int beamIndexDest) {
		try {
			dataOut[beamIndexDest] = valueProvider.get(buffer, beamIndexSource).get();
		} catch (Exception e) {
			logger.error(String.format("Error while writing variable %s (%s)", variable.getName(), e.getMessage()));
			throw e;
		}
	}

	@Override
	public void write(long[] origin, long[] count) throws NCException {
		try {
			variable.put(origin, count, dataOut);
		} catch (Exception e) {
			logger.error(String.format("Error while writing variable %s (%s)", variable.getName(), e.getMessage()));
			throw e;
		}
	}
}
