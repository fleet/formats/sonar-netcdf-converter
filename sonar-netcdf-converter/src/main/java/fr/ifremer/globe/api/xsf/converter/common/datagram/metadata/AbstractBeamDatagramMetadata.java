package fr.ifremer.globe.api.xsf.converter.common.datagram.metadata;

import java.util.Map.Entry;

import fr.ifremer.globe.api.xsf.util.SwathIdentifier;

import java.util.Set;

public abstract class AbstractBeamDatagramMetadata<CP extends AbstractCompletePingMetadata<? extends AbstractCompleteAntennaPingMetadata>>
		extends DatagramMetadata<SwathIdentifier, CP> {

	protected int maxBeamCount;
	
	/** max number of abstract samples for one beam. Contains WC or Seabed sample count*/
	protected int maxAbstractSampleCount;
	protected int txSectorCount;

	public AbstractBeamDatagramMetadata() {
		super();
	}

	/**
	 * Check if the given id is datagram map
	 */
	public boolean contains(SwathIdentifier swathId) {
		return index.containsKey(swathId);
	}

	public CP getPing(SwathIdentifier swathId) {
		return index.get(swathId);
	}

	public void addPing(SwathIdentifier swathId, CP pingMetadata) {
		index.put(swathId, pingMetadata);
	}

	public Set<Entry<SwathIdentifier, CP>> getPings() {
		return getEntries();
	}

	public int getMaxBeamCount() {
		return maxBeamCount;
	}

	public int getMaxAbstractSampleCount() {
		return maxAbstractSampleCount;
	}

	public int getTxSectorCount() {
		return txSectorCount;
	}

	public void computeIndex(Set<Integer> allAntennas) {
		maxBeamCount = 0;
		maxAbstractSampleCount = 0;
		txSectorCount = 0;
		for (CP ping : index.values()) {
			if (ping.isComplete(allAntennas)) {
				ping.computeIndex();
				maxBeamCount = Math.max(maxBeamCount, ping.getBeamCount());
				maxAbstractSampleCount = Math.max(maxAbstractSampleCount, ping.getMaxAbstractSampleCount());
				txSectorCount = Math.max(txSectorCount, ping.getNumTxSector());
			}
		}
	}
}
