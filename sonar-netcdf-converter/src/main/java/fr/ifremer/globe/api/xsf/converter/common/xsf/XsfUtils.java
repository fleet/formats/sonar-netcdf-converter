package fr.ifremer.globe.api.xsf.converter.common.xsf;

public class XsfUtils {
	/**
	 * @return string value from flag byte.
	 */
	public static String flagToString(byte flagByte) {
		switch (flagByte) {
		case XsfConstants.ATT_PROCESSING_STATUS_FLAG_ON:
			return "Done";
		case XsfConstants.ATT_PROCESSING_STATUS_FLAG_OFF:
			return "No";
		default:
			return "Invalid (" + flagByte + ")";
		}
	}
}
