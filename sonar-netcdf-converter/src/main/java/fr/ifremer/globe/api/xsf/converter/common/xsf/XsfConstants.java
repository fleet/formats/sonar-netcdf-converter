package fr.ifremer.globe.api.xsf.converter.common.xsf;

/**
 * This class contains 'xsf' file constants (groups, variables, attributes and dimensions) .
 */
public class XsfConstants {

	/**********************************************************************************************************
	 * XSF GROUPS
	 **********************************************************************************************************/

	public static final String GRP_FILE = "/";

	/* SUMMARY */
	public static final String GRP_SUMMARY = "/summary";
	public static final String GRP_SUMMARY_ATTITUDE = GRP_SUMMARY + "/attitude";
	public static final String GRP_SUMMARY_BATHY = GRP_SUMMARY + "/bathy";
	public static final String GRP_SUMMARY_BOUNDING_BOX = GRP_SUMMARY + "/bounding_box";
	public static final String GRP_SUMMARY_IMMERSION = GRP_SUMMARY + "/immersion";
	public static final String GRP_SUMMARY_SEABED = GRP_SUMMARY + "/seabed";
	public static final String GRP_SUMMARY_SURFACE_SOUND_SPEED = GRP_SUMMARY + "/surface_sound_speed";

	public static final String SONAR_TYPE_NAME = "Bathymetry Multibeam Sonar";

	// **********************************************************************************************************
	// * XSF ATTRIBUTES
	// **********************************************************************************************************/
	public static final String ATT_VERSION = "xsf_convention_version";
	//

	//
	/* BOUNDING BOX (GEO BOX) */
	public static final String ATT_BOUNDING_BOX_MAX_LON = GRP_SUMMARY_BOUNDING_BOX + "/longitude_max_deg";
	public static final String ATT_BOUNDING_BOX_MIN_LON = GRP_SUMMARY_BOUNDING_BOX + "/longitude_min_deg";
	public static final String ATT_BOUNDING_BOX_MAX_LAT = GRP_SUMMARY_BOUNDING_BOX + "/latitude_max_deg";
	public static final String ATT_BOUNDING_BOX_MIN_LAT = GRP_SUMMARY_BOUNDING_BOX + "/latitude_min_deg";

	/* SWATH DATE */
	public static final String ATT_SWATH_DATE_MIN = GRP_SUMMARY + "/swath_date_min";
	public static final String ATT_SWATH_DATE_MAX = GRP_SUMMARY + "/swath_date_max";

	/* MIN / MAX DEPTHS */
	public static final String ATT_MIN_DEPTH = GRP_SUMMARY_BATHY + "/min_depth_m";
	public static final String ATT_MAX_DEPTH = GRP_SUMMARY_BATHY + "/max_depth_m";

	// Processing status flags
	public static final byte ATT_PROCESSING_STATUS_FLAG_ON = 1;
	public static final byte ATT_PROCESSING_STATUS_FLAG_OFF = 0;

	//processing status fields
	public static final String ATT_PROCESSING_STATUS_AUTOMATIC_CLEANING = "automaticCleaning";
	public static final String ATT_PROCESSING_STATUS_MANUAL_CLEANING = "manualCleaning";
	public static final String ATT_PROCESSING_STATUS_VELOCITY_CORRECTION = "velocityCorrection";
	public static final String ATT_PROCESSING_STATUS_BIAS_CORRECTION = "biasCorrection";
	public static final String ATT_PROCESSING_STATUS_TIDE_CORRECTION = "tideCorrection";
	public static final String ATT_PROCESSING_STATUS_POSITION_CORRECTION = "positionCorrection";
	public static final String ATT_PROCESSING_STATUS_DRAUGHT_CORRECTION = "draughtCorrection";
	public static final String ATT_PROCESSING_STATUS_BACKSCATTER_CORRECTION = "backscatterCorrection";
	// specific status field containing list of bias correctors already applied
	public static final String ATT_PROCESSING_STATUS_BIAS_CORRECTORS = "biasCorrectors";
	
}
