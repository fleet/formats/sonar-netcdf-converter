package fr.ifremer.globe.api.xsf.converter.s7k.metadata;

import java.util.Map.Entry;

import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.DatagramMetadata;

public class S7KSensorMetadata<SM extends S7KIndividualSensorMetadata> extends DatagramMetadata<Long, SM> {

	public void addSensor(long sensorId, SM sensorMD) {
		setEntry(sensorId, sensorMD);
	}
	
	@Override
	public String toString() {
		StringBuilder bld = new StringBuilder();
		bld.append(this.getClass().getSimpleName());
		
		for (Entry<Long, SM> e : getEntries()) {
			bld.append(System.lineSeparator() + "\t[" + Long.toString(e.getKey()) + "] " + e.getValue().datagramCount + " datagrams");
		}
		return bld.toString();
	}
}
