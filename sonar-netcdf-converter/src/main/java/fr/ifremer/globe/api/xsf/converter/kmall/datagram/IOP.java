package fr.ifremer.globe.api.xsf.converter.kmall.datagram;

import java.nio.ByteBuffer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.utils.XLog;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.IOPMetadata;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.KmallFile;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.UShort;

public class IOP extends KmAllBaseDatagram {

	@Override
	public void computeSpecificMetadata(KmallFile metadata, ByteBuffer datagram, String datagramType,
			DatagramPosition position) {
		getSpecificMetadata(metadata).addDatagram(position);
	}

	@Override
	public IOPMetadata getSpecificMetadata(KmallFile metadata) {
		return metadata.getIOPMetadata();
	}

	public static UShort getInfo(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 18);
	}

	public static UShort getStatus(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 20);
	}

	public static String getRuntimeTxt(ByteBuffer datagram) {
		int body_size = TypeDecoder.read2U(datagram, 16).getU();

		byte[] str = new byte[body_size - 4];
		for (int i = 0; i < str.length; i++) {
			str[i] = datagram.get(22 + i);
		}
		return new String(str).trim();
	}

	/**
	 * Parses the runtimeTxt field to extract cross over angle
	 */
	public static float getTvgLawCrossOverAngle(ByteBuffer datagram) {
		float crossOverAngle = 0.f;
		try {
			Pattern anglePatt = Pattern.compile("Normal incidence corr.*:\\s*(?<angle>\\S+)", Pattern.CASE_INSENSITIVE);
			Matcher angleM = anglePatt.matcher(getRuntimeTxt(datagram));
			angleM.find();
			crossOverAngle = Float.parseFloat(angleM.group("angle"));
		} catch (Exception e) {
			XLog.warn("Error while parsing tvg law crossover angle in IOP datagram", e);
		}
		return crossOverAngle;
	}
}
