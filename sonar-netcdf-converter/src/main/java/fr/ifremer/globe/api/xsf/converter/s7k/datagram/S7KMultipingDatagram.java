package fr.ifremer.globe.api.xsf.converter.s7k.datagram;

import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KFile;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;

public abstract class S7KMultipingDatagram extends S7KBaseDatagram {

	public abstract SwathIdentifier genSwathIdentifier(S7KFile stats, DatagramBuffer buffer, long time);
	
	@Override
	public void computeSpecificMetadata(S7KFile metadata, DatagramBuffer buffer, long type,
			DatagramPosition position, long time) {
		final SwathIdentifier swathId = genSwathIdentifier(metadata, buffer, time);
		computeSpecificMetadata(metadata, buffer, type, position, time, swathId);
	}
	
	public abstract void computeSpecificMetadata(S7KFile metadata, DatagramBuffer buffer, long type,
			DatagramPosition position, long time, SwathIdentifier swathId);
}
