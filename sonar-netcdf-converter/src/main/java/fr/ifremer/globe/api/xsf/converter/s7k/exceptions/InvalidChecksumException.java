package fr.ifremer.globe.api.xsf.converter.s7k.exceptions;

import java.io.IOException;

public class InvalidChecksumException extends IOException {

	public InvalidChecksumException(String message) {
		super(message);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
