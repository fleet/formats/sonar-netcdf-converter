package fr.ifremer.globe.api.xsf.converter.all.xsf.intallation;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.all.datagram.IndividualSensorMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.extraparameters.ExtraParameters;
import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1String;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U2;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U8;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfFromKongsberg;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.ExtraParametersGrp;
import fr.ifremer.globe.api.xsf.util.SimpleIdentifier;
import fr.ifremer.globe.api.xsf.util.types.ULong;
import fr.ifremer.globe.netcdf.ucar.NCException;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

public class ExtraParametersGroup implements IDatagramConverter<AllFile, XsfFromKongsberg> {

	private List<ValueD1> values;
	private IndividualSensorMetadata sensorMetadata;
	long nEntries;

	private AllFile metadata;

	public ExtraParametersGroup(AllFile metadata) {
		this.metadata = metadata;
		this.sensorMetadata = metadata.getExtraParameters();
		this.nEntries = sensorMetadata.getTotalNumberOfEntries();
		this.values = new LinkedList<>();
	}

	@Override
	public void declare(AllFile allFile, XsfFromKongsberg xsf) throws NCException {

		ExtraParametersGrp v = new ExtraParametersGrp(xsf.getPlatformVendorGrp(), allFile, this.nEntries,new HashMap<String, Integer>());
		// declare all the variables

		values.add(new ValueD1U2(v.getSystem_serial_number(),
				buffer -> ExtraParameters.getSerialNumber(buffer.byteBufferData)));

		values.add(new ValueD1U8(v.getExternal_time(),
				buffer -> new ULong(ExtraParameters.getXSFEpochTime(buffer.byteBufferData))));

		values.add(new ValueD1U2(v.getRaw_count(), buffer -> ExtraParameters.getCounter(buffer.byteBufferData)));

		values.add(new ValueD1U2(v.getContent_identifier(),
				buffer -> ExtraParameters.getContentIdentifier(buffer.byteBufferData)));

		values.add(new ValueD1String(v.getExtra_param_information(),
				buffer -> ExtraParameters.getInformationDatagram(buffer.byteBufferData)));

	}

	@Override
	public void fillData(AllFile allFile) throws IOException, NCException {
		long[] origin = new long[] { 0 };

		DatagramBuffer buffer = new DatagramBuffer(metadata.getByteOrder());
		for (Entry<SimpleIdentifier, DatagramPosition> posentry : sensorMetadata.getDatagrams()) {
			final DatagramPosition pos = posentry.getValue();
			DatagramReader.readDatagramAt(pos.getFile(), buffer, pos.getSeek());

			// read the data from the source file
			for (ValueD1 value : this.values) {
				value.fill(buffer);
			}

			// flush into the output file
			for (ValueD1 value : this.values) {
				value.write(origin);
			}
			origin[0] += 1;
		}
	}

}
