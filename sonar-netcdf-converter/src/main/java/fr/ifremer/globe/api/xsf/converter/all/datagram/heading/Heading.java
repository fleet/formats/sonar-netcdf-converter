package fr.ifremer.globe.api.xsf.converter.all.datagram.heading;

import java.nio.ByteBuffer;

import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagram;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.UByte;
import fr.ifremer.globe.api.xsf.util.types.UShort;

/**
 * Stateless Position datagram
 */
public class Heading extends BaseDatagram {

	@Override
	public HeadingMetadata getSpecificMetadata(AllFile metadata) {
		return metadata.getHeading();
	}
	

	@Override
	protected void computeSpecificMetadata(AllFile metadata, ByteBuffer datagram, byte datagramType,
			long epochTime, DatagramPosition position) {

        getSpecificMetadata(metadata).addDatagram(position, getEpochTime(datagram), getNumberEntries(datagram).getU());
	}

	public static UShort getCounter(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 12);
	}
	
	public static UShort getSerialNumber(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 14);
	}

	public static UShort getNumberEntries(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 16);
	}

	// Read Repeat Cycle - N Entries
	public static UShort getTimeSinceRecordStart(ByteBuffer datagram, int headingIndex) {
		return TypeDecoder.read2U(datagram, 18 + headingIndex * 4);
	}
	
	public static UShort getHeading(ByteBuffer datagram, int headingIndex) {
		return TypeDecoder.read2U(datagram, 20 + headingIndex * 4);
	}
		
	public static UByte getHeadingIndicator(ByteBuffer datagram) {
		return TypeDecoder.read1U(datagram,18 + getNumberEntries(datagram).getU()*4);
	}	
	
	public static void print(ByteBuffer datagram) {
		System.out.println(getCounter(datagram));
		System.out.println(getSerialNumber(datagram));

	}

}
