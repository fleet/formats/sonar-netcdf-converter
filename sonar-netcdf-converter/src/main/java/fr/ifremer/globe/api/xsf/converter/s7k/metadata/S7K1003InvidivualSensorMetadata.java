package fr.ifremer.globe.api.xsf.converter.s7k.metadata;

public class S7K1003InvidivualSensorMetadata extends S7KIndividualSensorMetadata {

	private boolean hasGridPositionning;
	
	public S7K1003InvidivualSensorMetadata() {
		hasGridPositionning = false;
	}
	
	public void registerPositionType(short positionType) {
		 if (positionType == 1) {
			 hasGridPositionning = true;
		 }
	}
	
	public boolean usesGridPositionning() {
		return this.hasGridPositionning;
	}
}
