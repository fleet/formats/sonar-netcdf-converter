package fr.ifremer.globe.api.xsf.converter;

import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgConstants;

public class MbgConverterParameters extends SounderFileConverterParameters {

	/** MBG parameters **/
	private String shipName = "";
	private String surveyName = "";
	private String reference = "";
	private String cdi = "";

	/**
	 * Constructor
	 */
	public MbgConverterParameters(String inputFilePath, String outFilePath) {
		super(inputFilePath, outFilePath, MbgConstants.EXTENSION_MBG);
	}

	/**
	 * Constructor
	 */
	public MbgConverterParameters(String inputFilePath, String outFilePath, boolean overwrite) {
		this(inputFilePath, outFilePath);
		this.setOverwrite(overwrite);
	}

	public String getShipName() {
		return shipName;
	}

	public void setShipName(String shipName) {
		this.shipName = shipName;
	}

	public String getSurveyName() {
		return surveyName;
	}

	public void setSurveyName(String surveyName) {
		this.surveyName = surveyName;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getCdi() {
		return cdi;
	}

	public void setCdi(String cdi) {
		this.cdi = cdi;
	}
}
