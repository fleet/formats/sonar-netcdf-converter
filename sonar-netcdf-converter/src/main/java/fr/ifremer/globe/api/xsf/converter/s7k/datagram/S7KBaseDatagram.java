package fr.ifremer.globe.api.xsf.converter.s7k.datagram;

import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.DatagramMetadata;
import fr.ifremer.globe.api.xsf.converter.common.utils.DateUtil;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KFile;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.UInt;
import fr.ifremer.globe.api.xsf.util.types.UShort;

public abstract class S7KBaseDatagram implements S7KDatagram {

	public void computeMetaData(S7KFile stats, DatagramBuffer buffer, long type,
			DatagramPosition datagramPosition) {
		final long time = getTimestampNano(buffer);
		getSpecificMetadata(stats).addDatagram();
		stats.getStats().getDeviceIds().add(getDeviceIdentifier(buffer).getU());
		computeSpecificMetadata(stats, buffer, type, datagramPosition, time);
	}

	public abstract void computeSpecificMetadata(S7KFile metadata, DatagramBuffer buffer, long type,
			DatagramPosition position, long time);

	@SuppressWarnings("rawtypes")
	public abstract DatagramMetadata getSpecificMetadata(S7KFile metadata);

	public static UShort getProtocolVersion(BaseDatagramBuffer buff) {
		return TypeDecoder.read2U(buff.byteBufferSizeHeader, 0);
	}

	public static UShort getOffset(BaseDatagramBuffer buff) {
		return TypeDecoder.read2U(buff.byteBufferSizeHeader, 2);
	}

	public static UInt getSyncPattern(BaseDatagramBuffer buff) {
		return TypeDecoder.read4U(buff.byteBufferSizeHeader, 4);
	}

	public static UInt getSize(BaseDatagramBuffer buff) {
		return TypeDecoder.read4U(buff.byteBufferSizeHeader, 8);
	}

	public static UInt getOptionalDataOffset(BaseDatagramBuffer buff) {
		return TypeDecoder.read4U(buff.byteBufferSizeHeader, 12);
	}

	/**
	 * Gives the optional data offset, within the byteBufferData buffer of buff
	 * 
	 * @param buff
	 * @return
	 */
	protected static int odo(BaseDatagramBuffer buff) {
		return Math.toIntExact(getOptionalDataOffset(buff).getU()) - 64;
	}

	protected static void assertHasOD(BaseDatagramBuffer buff) {
		if (getOptionalDataOffset(buff).getU() == 0) {
			throw new RuntimeException("Current datagram does not have optional data");
		}
	}

	public static UInt getOptionalDataIdentifier(BaseDatagramBuffer buff) {
		return TypeDecoder.read4U(buff.byteBufferData, 16);
	}

	/**
	 * @return time in milliseconds.
	 */
	public static long getTimeMilli(BaseDatagramBuffer buff) {
		int year = Short.toUnsignedInt(buff.byteBufferSizeHeader.getShort(20));
		long days = Short.toUnsignedLong(buff.byteBufferSizeHeader.getShort(22));
		long hours = Byte.toUnsignedLong(buff.byteBufferSizeHeader.get(28));
		long minutes = Byte.toUnsignedLong(buff.byteBufferSizeHeader.get(29));
		float seconds = buff.byteBufferSizeHeader.getFloat(24);
		return DateUtil.convertResonTime(year, days, hours, minutes, seconds);
	}

	/**
	 * @return time in nanoseconds.
	 */
	public static long getTimestampNano(BaseDatagramBuffer buff) {
		int year = Short.toUnsignedInt(buff.byteBufferSizeHeader.getShort(20));
		long days = Short.toUnsignedLong(buff.byteBufferSizeHeader.getShort(22));
		long hours = Byte.toUnsignedLong(buff.byteBufferSizeHeader.get(28));
		long minutes = Byte.toUnsignedLong(buff.byteBufferSizeHeader.get(29));
		float seconds = buff.byteBufferSizeHeader.getFloat(24);
		return DateUtil.convertResonTimeInNano(year, days, hours, minutes, seconds);
	}

	public static UInt getRecordType(BaseDatagramBuffer buff) {
		return TypeDecoder.read4U(buff.byteBufferSizeHeader, 32);
	}

	public static UInt getDeviceIdentifier(BaseDatagramBuffer buff) {
		return TypeDecoder.read4U(buff.byteBufferSizeHeader, 36);
	}

	public static UInt getSystemEnumerator(BaseDatagramBuffer buff) {
		return TypeDecoder.read4U(buff.byteBufferSizeHeader, 40);
	}

	public static UShort getFlags(BaseDatagramBuffer buff) {
		return TypeDecoder.read2U(buff.byteBufferSizeHeader, 48);
	}

	public static UInt getRecordsInFragmentedSet(BaseDatagramBuffer buff) {
		return TypeDecoder.read4U(buff.byteBufferSizeHeader, 56);
	}

	public static UInt getFragmentNumber(BaseDatagramBuffer buff) {
		return TypeDecoder.read4U(buff.byteBufferSizeHeader, 60);
	}

	public static UInt getChecksum(BaseDatagramBuffer buff) {
		return TypeDecoder.read4U(buff.byteBufferData, buff.byteBufferData.limit() - 4);
	}

	public static UInt computeChecksum(BaseDatagramBuffer buff) {
		int sum = 0;
		for (int i = 0; i < buff.byteBufferSizeHeader.limit(); i++) {
			sum += Byte.toUnsignedInt(buff.byteBufferSizeHeader.get(i));
		}
		for (int i = 0; i < buff.byteBufferData.limit() - 4; i++) {
			sum += Byte.toUnsignedInt(buff.byteBufferData.get(i));
		}
		return new UInt(sum);
	}

	public static boolean validChecksum(BaseDatagramBuffer buff) {
		boolean hasChecksumFlag = (getFlags(buff).getU() & 0x0001) == 0x0001;
		if (!hasChecksumFlag) {
			// can be discussed
			return true;
		} else {
			return getChecksum(buff).getInt() == computeChecksum(buff).getInt();
		}
	}
}
