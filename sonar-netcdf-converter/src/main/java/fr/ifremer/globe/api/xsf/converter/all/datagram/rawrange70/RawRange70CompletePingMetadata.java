package fr.ifremer.globe.api.xsf.converter.all.datagram.rawrange70;

import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.AbstractCompletePingMetadata;

public class RawRange70CompletePingMetadata extends AbstractCompletePingMetadata<RawRange70CompleteAntennaPingMetadata> {
	public RawRange70CompletePingMetadata(int[] serialNumbers) {
        super(0, serialNumbers);
    }
}
