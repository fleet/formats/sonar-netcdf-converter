package fr.ifremer.globe.api.xsf.converter.kmall.datagram;

import java.nio.ByteBuffer;

import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.KmallFile;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.SVPMetadata;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.DDouble;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.api.xsf.util.types.UInt;
import fr.ifremer.globe.api.xsf.util.types.UShort;

public class SVP extends KmAllBaseDatagram {

	@Override
	public void computeSpecificMetadata(KmallFile metadata, ByteBuffer datagram, String datagramType,
			DatagramPosition position) {
		SVPMetadata md = getSpecificMetadata(metadata);
		md.addDatagram(position, getSamplesCount(datagram).getU());

		for (int i = 0; i < getSamplesCount(datagram).getU(); i++) {
			// Calcul de stats
			md.addSSP(getSoundVelocity_mPerSec(datagram, i).get());
			md.addTemp(getTemp_C(datagram, i).get());
			md.addSal(getSalinity(datagram, i).get());
		}
	}

	@Override
	public SVPMetadata getSpecificMetadata(KmallFile metadata) {
		return metadata.getSVPMetadata();
	}

	public static UShort getNumBytesCmnPart(ByteBuffer buffer) {
		return TypeDecoder.read2U(buffer, 16);
	}

	public static UShort getSamplesCount(ByteBuffer buffer) {
		return TypeDecoder.read2U(buffer, 18);
	}

	public static String getSensorFormat(ByteBuffer buffer) {
		byte[] sensorFormat = new byte[4];
		for (int i = 0; i < 4; i++) {
			sensorFormat[i] = buffer.get(20 + i);
		}
		return new String(sensorFormat);
	}

	public static UInt getTime_sec(ByteBuffer buffer) {
		return TypeDecoder.read4U(buffer, 24);
	}

	public static DDouble getLatitude_deg(ByteBuffer buffer) {
		DDouble value = TypeDecoder.read8F(buffer, 28);
		if (value.get() == 200) // 200 is invalid value
			return DDouble.NAN;
		return value;
	}

	public static DDouble getLongitude_deg(ByteBuffer buffer) {
		DDouble value = TypeDecoder.read8F(buffer, 36);
		if (value.get() == 200) // 200 is invalid value
			return DDouble.NAN;
		return value;
	}

	// SVPPoint Struct
	public static FFloat getDepth_m(ByteBuffer buffer, int indexProfil) {
		return TypeDecoder.read4F(buffer, 44 + 20 * indexProfil);
	}

	public static FFloat getSoundVelocity_mPerSec(ByteBuffer buffer, int indexProfil) {
		return TypeDecoder.read4F(buffer, 48 + 20 * indexProfil);
	}

	public static FFloat getTemp_C(ByteBuffer buffer, int indexProfil) {
		return TypeDecoder.read4F(buffer, 56 + 20 * indexProfil);
	}

	public static FFloat getSalinity(ByteBuffer buffer, int indexProfil) {
		return TypeDecoder.read4F(buffer, 60 + 20 * indexProfil);
	}

}
