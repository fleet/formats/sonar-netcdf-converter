package fr.ifremer.globe.api.xsf.converter.s7k.xsf;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;

import fr.ifremer.globe.api.xsf.converter.XsfConverterParameters;
import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.utils.Angles;
import fr.ifremer.globe.api.xsf.converter.common.utils.processing.VariableComputer;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfWriter;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.AttitudeGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.PlatformGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.PositionGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.BeamGroup1VendorSpecificGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.InstallationGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.types.beam_stabilisation_t;
import fr.ifremer.globe.api.xsf.converter.common.xsf.types.transducer_type_t;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7K7030InstallationParameters;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7K7030InstallationParameters.Origin;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KFile;
import fr.ifremer.globe.netcdf.ucar.NCException;

public class S7KXsfWriter extends XsfWriter<S7KFile, XsfFromS7k> {

	@Override
	public XsfFromS7k createOutputNcFile(S7KFile s7kFile, XsfConverterParameters params)
			throws NCException, IOException {
		return new XsfFromS7k(s7kFile, params);
	}

	@Override
	public List<IDatagramConverter<S7KFile, XsfFromS7k>> getGroupConverters(S7KFile s7kFile) {
		List<IDatagramConverter<S7KFile, XsfFromS7k>> dataGroups = new LinkedList<>();

		// we fill transverse data first
		dataGroups.add(new TransverseGroup(s7kFile.getSwathIndexer()));

		if (s7kFile.get1003Metadata().datagramCount > 0) {
			dataGroups.add(new S7K1003Converter());
		}
		if (s7kFile.get1010Metadata().datagramCount > 0) {
			dataGroups.add(new S7K1010Converter());
		}
		if (s7kFile.get1015Metadata().datagramCount > 0) {
			dataGroups.add(new S7K1015Converter());
		}
		if (s7kFile.get1016Metadata().datagramCount > 0) {
			dataGroups.add(new S7K1016Converter());
		}
		if (s7kFile.get7000Metadata().datagramCount > 0) {
			dataGroups.add(new S7K7000Converter());
		}
		if (s7kFile.get7004Metadata().datagramCount > 0) {
			dataGroups.add(new S7K7004Converter());
		}
		if (s7kFile.get7001Metadata().datagramCount > 0) {
			dataGroups.add(new S7K7001Converter());
		}
		if (s7kFile.get7006Metadata().datagramCount > 0) {
			dataGroups.add(new S7K7006Converter());
		}
		if (s7kFile.get7009Metadata().datagramCount > 0) {
			dataGroups.add(new S7K7009Converter());
		}
		if (s7kFile.get7027Metadata().datagramCount > 0) {
			dataGroups.add(new S7K7027Converter());
		}
		if (s7kFile.get7041Metadata().datagramCount > 0) {
			dataGroups.add(new S7K7041Converter(s7kFile));
		}
		if (s7kFile.get7057Metadata().datagramCount > 0) {
			dataGroups.add(new S7K7057Converter());
		}
		if (s7kFile.get7058Metadata().datagramCount > 0) {
			dataGroups.add(new S7K7058Converter());
		}
		if (s7kFile.get7200Metadata().datagramCount > 0) {
			dataGroups.add(new S7K7200Converter());
		}

		return dataGroups;
	}

	@Override
	public void postProcess(S7KFile s7kFile, XsfFromS7k xsf, XsfConverterParameters params, Logger logger)
			throws NCException {
		super.postProcess(s7kFile, xsf, params, logger);
		logger.info("Fill platform group with installation parameters...");
		fillPlatformGroup(s7kFile, xsf, logger);
		logger.info("Fill platform attitude and position...");
		VariableComputer.fillPlatformAttitudeAndPosition(s7kFile, xsf);
		logger.info("Fill transmit beam width...");
		fillTransmitBeamWidth(xsf);
		logger.info("Compute beam angle...");
		computeBeamAngle(xsf);
		computeDetectionBeamPointingAngle(xsf);
		VariableComputer.fillSwathTideDraught(s7kFile, xsf);

		// declare missing variable when WC is missing
		xsf.getBeamGroup().getSample_count();

	}

	public static void fillTransmitBeamWidth(XsfFromS7k xsf) throws NCException {
		if (xsf.getBeamGroup().getDim_beam() == null) {
			return;
			// fill tx beam widths
		}

		BeamGroup1VendorSpecificGrp beamVendor = xsf.getBeamVendor();
		long swathCount = xsf.getBeamGroup().getDim_ping_time().getLength();
		long tx_beamCount = xsf.getBeamGroup().getDim_tx_beam().getLength();

		for (int txbeam = 0; txbeam < tx_beamCount; txbeam++) {
			long[] start = { 0, txbeam };
			long[] count = { swathCount, 1 };
			long[] start_values = { 0 };
			long[] count_values = { swathCount };
			float[] v = beamVendor.getProjector_beam_minus3dB_beam_width_vertical().get_float(start_values,
					count_values);
			xsf.getBeamGroup().getBeamwidth_transmit_minor().put(start, count, v);
			v = beamVendor.getProjector_beam_minus3dB_beam_width_horizontal().get_float(start_values, count_values);
			xsf.getBeamGroup().getBeamwidth_transmit_major().put(start, count, v);
		}

	}

	public static void computeBeamAngle(XsfFromS7k xsf) throws NCException {
		// compute beam angles variables in order to give their angles relatively to the
		// RX array or vertical depending
		// on stabilization
		BeamGroup1VendorSpecificGrp beamVendorGrp = xsf.getBeamVendor();
		if (xsf.getBeamGroup().getDim_beam() == null) {
			return;
		}

		long swathCount = xsf.getBeamGroup().getDim_ping_time().getLength();
		long beamCount = xsf.getBeamGroup().getDim_beam().getLength();
		long tx_beamCount = xsf.getBeamGroup().getDim_tx_beam().getLength();

		byte[] stab = new byte[(int) swathCount];
		Arrays.fill(stab, beam_stabilisation_t.stabilised.getValue());
		xsf.getBeamGroup().getBeam_stabilisation().put(new long[] { 0l }, new long[] { swathCount }, stab);

		// RX
		{
			long[] start = { 0l, 0l };
			long[] count = { swathCount, beamCount };
			float[] values = beamVendorGrp.getBeam_horizontal_direction_angle().get_float(start, count);
			// Rotation convention differs from Reson and Sonar Netcdf
			for (int i = 0; i < values.length; i++) {
				values[i] *= -1;
			}
			xsf.getBeamGroup().getRx_beam_rotation_phi().put(start, count, values);

			values = beamVendorGrp.getBeam_vertical_direction_angle().get_float(start, count);
			// Is convention reversed for tilt angle, we cannot test, always equals to zero
			for (int i = 0; i < values.length; i++) {
				values[i] *= -1;
			}
			xsf.getBeamGroup().getRx_beam_rotation_theta().put(start, count, values);

			Arrays.fill(values, 0f);
			xsf.getBeamGroup().getRx_beam_rotation_psi().put(start, count, values);
		}

		float[] value = { Float.NaN };
		for (long swath = 0; swath < swathCount; swath++) {
			for (int tx_beam = 0; tx_beam < tx_beamCount; tx_beam++) {
				long[] start = { swath, tx_beam };
				long[] count = { 1, 1 }; // only one tx beam for reson
				// In a first approach we're gonna estimate that angles are stabilized and thus
				// should be given
				// relatively to the vertical

				long[] r_start = { swath };
				long[] r_read = { 1 };
				float[] raw_value = beamVendorGrp.getProjector_beam_steering_angle_vertical().get_float(r_start,
						r_read);
				// Angle convention differs from Reson and Sonar Netcdf
				for (int i = 0; i < raw_value.length; i++) {
					raw_value[i] *= -1;
				}
				xsf.getBeamGroup().getTx_beam_rotation_theta().put(start, count, raw_value);

				raw_value = beamVendorGrp.getProjector_beam_steering_angle_horizontal().get_float(r_start, r_read);
				// Is convention reversed for tilt angle, we cannot test, always equals to zero
				for (int i = 0; i < raw_value.length; i++) {
					raw_value[i] *= -1;
				}
				xsf.getBeamGroup().getTx_beam_rotation_phi().put(start, count, raw_value);

				value[0] = 0f;
				xsf.getBeamGroup().getTx_beam_rotation_psi().put(start, count, value);
			}
		}
	}

	public static void computeDetectionBeamPointingAngle(XsfFromS7k xsf) throws NCException {
		// compute detection beam pointing angles variables in order to give their
		// angles relatively to the
		// RX array or vertical depending
		// on stabilization
		long swathCount = xsf.getBeamGroup().getDim_ping_time().getLength();
		long detectionCount = xsf.getBathyGrp().getDim_detection().getLength();

		byte[] stab = new byte[(int) swathCount];
		Arrays.fill(stab, beam_stabilisation_t.stabilised.getValue());
		xsf.getBathyGrp().getDetection_beam_stabilisation().put(new long[] { 0l }, new long[] { swathCount }, stab);
		
		// guess detection position relative to receiver and apply matching sign on angle
		// detection angles are positive on port 
		long[] start = { 0l, 0l };
		long[] count = { swathCount, detectionCount };
		float[] angle = xsf.getBathyVendor().getBeam_depression_angle().get_float(start, count);
		float[] azimuth = xsf.getBathyVendor().getBeam_azimuth_angle().get_float(start, count);
		for (int i = 0; i < angle.length; i++) {
			angle[i] = (float)Math.toDegrees(Math.atan(Math.tan(Math.toRadians(angle[i]))*Math.sin(Math.toRadians(-azimuth[i]))));
		}
		xsf.getBathyGrp().getDetection_beam_pointing_angle().put(start, count, angle);
		
	}

	/**
	 * Fills the platform group with installation parameters.
	 */
	private void fillPlatformGroup(S7KFile s7kFile, XsfFromS7k xsf, Logger logger) throws NCException {
		PlatformGrp platformGrp = xsf.getPlatformGrp();
		InstallationGrp platformInstallationVendorGrp = new InstallationGrp(xsf.getPlatformVendorGrp(), s7kFile,
				new HashMap<String, Integer>());

		// only one intallation datagram is expected
		S7K7030InstallationParameters installationParameters = s7kFile.getInstallationParameters(s7kFile.getMinDate());

		// log warning about parameters origin
		logger.info("Installation parameters retrieved from {}.", installationParameters.getOrigin().toString());
		if (installationParameters.getOrigin() == Origin.FROM_XML_FILE) {
			logger.warn("Installation parameters not found in datagram, retrived from XML file : '{}'",
					installationParameters.getOriginFile());
		}
		if (installationParameters.getOrigin() == Origin.FROM_CONSTANTS) {
			logger.warn(
					"Installation parameters not found in datagram or XML file, default values are used, could be obsolete!");
		}
		logger.info("Installation parameters : {}", installationParameters.toString());

		long[] origin = { 0 };
		long[] size = { 1 };

		// transmit tranducer
		platformGrp.getTransducer_function().put(origin, size,
				new byte[] { transducer_type_t.Tx_Transducer.getValue() });
		// ... offset
		platformGrp.getTransducer_offset_x().put(origin, size,
				new float[] { installationParameters.getTransmitArrayY() });
		platformGrp.getTransducer_offset_y().put(origin, size,
				new float[] { installationParameters.getTransmitArrayX() });
		platformGrp.getTransducer_offset_z().put(origin, size,
				new float[] { -1 * installationParameters.getTransmitArrayZ() });
		// ... rotation
		platformGrp.getTransducer_rotation_x().put(origin, size,
				new float[] { (float) Angles.trim180(Math.toDegrees(installationParameters.getTransmitArrayRoll())) });
		platformGrp.getTransducer_rotation_y().put(origin, size,
				new float[] { (float) Angles.trim180(Math.toDegrees(installationParameters.getTransmitArrayPitch())) });
		platformGrp.getTransducer_rotation_z().put(origin, size, new float[] {
				(float) Angles.trim180(Math.toDegrees(installationParameters.getTransmitArrayHeading())) });

		// receive tranducer
		origin = new long[] { 1 }; // update position in variables
		platformGrp.getTransducer_function().put(origin, size,
				new byte[] { transducer_type_t.Rx_Transducer.getValue() });
		// ... offset
		platformGrp.getTransducer_offset_x().put(origin, size,
				new float[] { installationParameters.getReceiveArrayY() });
		platformGrp.getTransducer_offset_y().put(origin, size,
				new float[] { installationParameters.getReceiveArrayX() });
		platformGrp.getTransducer_offset_z().put(origin, size,
				new float[] { -1 * installationParameters.getReceiveArrayZ() });
		// ... rotation
		platformGrp.getTransducer_rotation_x().put(origin, size,
				new float[] { (float) Angles.trim180(Math.toDegrees(installationParameters.getReceiveArrayRoll())) });
		platformGrp.getTransducer_rotation_y().put(origin, size,
				new float[] { (float) Angles.trim180(Math.toDegrees(installationParameters.getReceiveArrayPitch())) });
		platformGrp.getTransducer_rotation_z().put(origin, size, new float[] {
				(float) Angles.trim180(Math.toDegrees(installationParameters.getReceiveArrayHeading())) });

		// position sensor
		origin = new long[] { 0 };
		// ... id
		PositionGrp positionGrp = xsf.getPositionGrp();
		String[] positionId = { positionGrp.getGroups().get(0).getName() };
		platformGrp.getPosition_ids().put(origin, size, positionId);
		// ... time delay
		platformInstallationVendorGrp.getPosition_time_delay().putu(origin, size,
				new int[] { installationParameters.getPositionSensorTimeDelay() }); // USHORT
		// ... offset
		platformGrp.getPosition_offset_x().put(origin, size,
				new float[] { installationParameters.getPositionSensorY() });
		platformGrp.getPosition_offset_y().put(origin, size,
				new float[] { installationParameters.getPositionSensorX() });
		platformGrp.getPosition_offset_z().put(origin, size,
				new float[] { -1 * installationParameters.getPositionSensorZ() });

		// motion sensor (MRU)
		origin = new long[] { 0 };
		// ... id
		AttitudeGrp attitudeGrp = xsf.getAttitudeGrp();
		String[] attitudeId = { attitudeGrp.getGroups().get(0).getName() };
		platformGrp.getMRU_ids().put(origin, size, attitudeId);
		// ... time delay
		platformInstallationVendorGrp.getMRU_time_delay().putu(origin, size,
				new int[] { installationParameters.getMotionSensorTimeDelay() }); // USHORT
		// ... offset
		platformGrp.getMRU_offset_x().put(origin, size, new float[] { installationParameters.getMotionSensorX() });
		platformGrp.getMRU_offset_y().put(origin, size, new float[] { installationParameters.getMotionSensorY() });
		platformGrp.getMRU_offset_z().put(origin, size, new float[] { -1 * installationParameters.getMotionSensorZ() });
		// ... rotation
		platformGrp.getMRU_rotation_x().put(origin, size, new float[] {
				(float) Angles.trim180(Math.toDegrees(installationParameters.getMotionSensorRollCalibration())) });
		platformGrp.getMRU_rotation_y().put(origin, size, new float[] {
				(float) Angles.trim180(Math.toDegrees(installationParameters.getMotionSensorPitchCalibration())) });
		platformGrp.getMRU_rotation_z().put(origin, size, new float[] {
				(float) Angles.trim180(Math.toDegrees(installationParameters.getMotionSensorHeadingCalibration())) });

		// water level
		origin = new long[] { 0 };
		platformGrp.getWater_level().put(origin, size,
				new float[] { -1 * installationParameters.getWaterLineVerticalOffset() });

		
		// specific vendor intallation parameters
		platformInstallationVendorGrp.getFrequency().put(origin, size,
				new float[] { installationParameters.getFrequency() });

		platformInstallationVendorGrp.getWater_line_vertical_offset().put(origin, size,
				new float[] { -1 * installationParameters.getWaterLineVerticalOffset() });

		platformInstallationVendorGrp.getSoftware_version().put(origin, size,
				new String[] { installationParameters.getSoftwareVersion() });

		platformInstallationVendorGrp.getS7k_software_version().put(origin, size,
				new String[] { installationParameters.getS7KSoftwareVersion() });

		platformInstallationVendorGrp.getFirmware_version().put(origin, size,
				new String[] { installationParameters.getFirmwareVersion() });

		platformInstallationVendorGrp.getRecord_protocol_version().put(origin, size,
				new String[] { installationParameters.getProtocolVersionInfo() });
	}
}
