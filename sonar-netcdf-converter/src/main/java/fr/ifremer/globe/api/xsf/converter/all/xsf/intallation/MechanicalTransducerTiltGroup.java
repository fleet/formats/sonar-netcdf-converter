package fr.ifremer.globe.api.xsf.converter.all.xsf.intallation;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.all.datagram.IndividualSensorMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.attitude.Attitude;
import fr.ifremer.globe.api.xsf.converter.all.datagram.mechanicaltransducertilt.MechanicalTransducerTilt;
import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagram;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2F4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2U2;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2U8;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfFromKongsberg;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.MechanicalTransducerTiltGrp;
import fr.ifremer.globe.api.xsf.util.SimpleIdentifier;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.api.xsf.util.types.ULong;
import fr.ifremer.globe.netcdf.ucar.NCException;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

public class MechanicalTransducerTiltGroup implements IDatagramConverter<AllFile, XsfFromKongsberg> {

	long nEntries;
	private List<ValueD2> values;
	private IndividualSensorMetadata sensorMetadata;
	private AllFile metadata;

	public MechanicalTransducerTiltGroup(AllFile metadata) {
		this.metadata = metadata;
		this.sensorMetadata = metadata.getExtraParameters();
		this.nEntries = sensorMetadata.getTotalNumberOfEntries();
		this.values = new LinkedList<>();
	}

	@Override
	public void declare(AllFile allFile, XsfFromKongsberg xsf) throws NCException {
		MechanicalTransducerTiltGrp v = new MechanicalTransducerTiltGrp(xsf.getPlatformVendorGrp(), allFile, nEntries,
				new HashMap<>());

		values.add(new ValueD2U2(v.getSystem_serial_number(),
				(buffer, i) -> MechanicalTransducerTilt.getSerialNumber(buffer.byteBufferData)));

		values.add(new ValueD2U8(v.getTime(), sensorMetadata.getMaxEntriesPerDatagram(), (buffer, i) -> new ULong(
				BaseDatagram.milliSecondToNano(MechanicalTransducerTilt.getEpochTime(buffer.byteBufferData)
						+ MechanicalTransducerTilt.getTimeSinceRecordStart(buffer.byteBufferData, i).getU()))));

		values.add(new ValueD2U2(v.getRaw_count(),
				(buffer, i) -> MechanicalTransducerTilt.getCounter(buffer.byteBufferData)));

		values.add(new ValueD2F4(v.getMechanical_tilt(), (buffer, i) -> new FFloat(
				MechanicalTransducerTilt.getMechanicalTransducerTilt(buffer.byteBufferData, i).get() * 0.01f)));

	}

	@Override
	public void fillData(AllFile allFile) throws IOException, NCException {
		long[] count = new long[] { 0 };
		long[] origin = new long[] { 0 };

		DatagramBuffer buffer = new DatagramBuffer(metadata.getByteOrder());
		for (Entry<SimpleIdentifier, DatagramPosition> posentry : sensorMetadata.getDatagrams()) {
			this.values.forEach(ValueD2::clear);
			final DatagramPosition pos = posentry.getValue();
			DatagramReader.readDatagramAt(pos.getFile(), buffer, pos.getSeek());
			long nbValue = Attitude.getNumberEntries(buffer.byteBufferData).getU();

			// read the data from the source file
			for (int i = 0; i < nbValue; i++) {
				for (ValueD2 value : this.values) {
					value.fill(buffer, i, i);
				}
			}
			// flush into the output file
			count[0] = nbValue;
			for (ValueD2 value : this.values) {
				value.write(origin, count);
			}
			origin[0] += nbValue;
		}
	}

}
