package fr.ifremer.globe.api.xsf.util.types;

public class UInt implements Comparable<UInt> {
    
    private int v;
    
    public UInt(int v) {
        this.v = v;
    }

    public int getInt() {
        return v;
    }
    
    public long getU() {
        return Integer.toUnsignedLong(v);
    }
    
    public boolean isSigned() {
        return false;
    }

	@Override
	public int compareTo(UInt o) {
		return Integer.compareUnsigned(v, o.v);
	}
}
