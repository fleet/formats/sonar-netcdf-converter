package fr.ifremer.globe.api.xsf.util.types;

public class UByte implements Comparable<UByte> {
    
    private byte v;
    
    public UByte(byte v) {
        this.v = v;
    }
    
    public byte getByte() {
        return v;
    }
    
    public short getU() {
        return (short) Byte.toUnsignedInt(v);
    }
    
    public int getInt() {
        return (int) v & 0xFF;
    }

    public boolean isSigned() {
        return false;
    }

	@Override
	public int compareTo(UByte o) {
		return Short.compare(getU(), o.getU());
	}

}
