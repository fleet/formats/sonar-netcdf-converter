package fr.ifremer.globe.api.xsf.converter.all.datagram.depth68;

import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.AbstractCompleteAntennaPingSingleMetadata;

public class Depth68CompleteAntennaPingMetadata extends AbstractCompleteAntennaPingSingleMetadata {

    public Depth68CompleteAntennaPingMetadata(DatagramPosition position, int beamCount) {
        super(position, beamCount, 0);
    }

}
