package fr.ifremer.globe.api.xsf.util.types;

public class SShort implements Comparable<SShort> {
    
    private short v;
    
    public SShort(short v) {
        this.v = v;
    }

    public short get() {
        return v;
    }
    
    public boolean isSigned() {
        return true;
    }
    
    @Override
    public int compareTo(SShort o) {
    	return Short.compare(v, o.v);
    }
}
