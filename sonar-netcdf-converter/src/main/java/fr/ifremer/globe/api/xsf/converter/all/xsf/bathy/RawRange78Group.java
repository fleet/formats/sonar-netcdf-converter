package fr.ifremer.globe.api.xsf.converter.all.xsf.bathy;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.all.datagram.rawrange78.RawRange78;
import fr.ifremer.globe.api.xsf.converter.all.datagram.rawrange78.RawRange78CompleteAntennaPingMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.rawrange78.RawRange78CompletePingMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.rawrange78.RawRange78Metadata;
import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.common.datagram.KmSounderLib;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.*;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.*;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfFromKongsberg;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.BathymetryGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.BeamGroup1Grp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.BathymetryVendorSpecificGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.BeamGroup1VendorSpecificGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.types.beam_stabilisation_t;
import fr.ifremer.globe.api.xsf.converter.common.xsf.types.transmit_t;
import fr.ifremer.globe.api.xsf.util.DefaultLogger;
import fr.ifremer.globe.api.xsf.util.ObjectBufferPool;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.types.*;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.util.Pair;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

public class RawRange78Group implements IDatagramConverter<AllFile, XsfFromKongsberg> {

	protected List<ValueD1> swathValues = new LinkedList<>();
	protected BeamGroup1Grp beamGrp;
	private AllFile stats;
	private RawRange78Metadata metadata;
	private List<ValueD2> swathAntennaValues;
	private List<ValueD2> swathTxValues;
	private List<ValueD2> swathBeamValues;
	private BathymetryGrp bathyGrp;

	public RawRange78Group(AllFile stats) {
		this.stats = stats;
		this.metadata = stats.getRawRange78();
		this.swathAntennaValues = new LinkedList<>();
		this.swathTxValues = new LinkedList<>();
		this.swathBeamValues = new LinkedList<>();
	}

	/**
	 * Compute and return an array containing the start and stop frequency
	 */
	public FFloat[] computeFrequenciesStartAndStop(BaseDatagramBuffer buffer, int i) {
		float fq = RawRange78.getTxCenterFrequency(buffer.byteBufferData, i).get();
		int waveForm = RawRange78.getWaveFormIdentifier(buffer.byteBufferData, i).getU(); // 0:CW, 1:FM
		float bandwidth = RawRange78.getSignalBandwidth(buffer.byteBufferData, i).get();
		if (waveForm == 0) // CW
		{
			FFloat[] ret = { new FFloat(fq), new FFloat(fq) };
			return ret;
		} else {
			FFloat[] ret = { new FFloat(fq - bandwidth), new FFloat(fq + bandwidth) };
			return ret;

		}

	}

	private SByte toTransmitType(BaseDatagramBuffer buffer, int i) {
		short type = RawRange78.getWaveFormIdentifier(buffer.byteBufferData, i).getU();
		if (type == 0) {
			return new SByte(transmit_t.CW.getValue());
		} else {
			return new SByte(transmit_t.LFM.getValue());
		}
	}

	@Override
	public void declare(AllFile allFile, XsfFromKongsberg xsf) throws NCException {

		beamGrp = xsf.getBeamGroup();
		bathyGrp = xsf.getBathyGrp();
		BathymetryVendorSpecificGrp bathyVendorGrp = xsf.getBathy();
		BeamGroup1VendorSpecificGrp vendorBeamGrp = xsf.getBeamGroupVendorSpecific();

		// Create the swath only variables

		swathValues.add(new ValueD1U4(vendorBeamGrp.getPing_raw_count(),
				(buffer) -> new UInt(RawRange78.getPingNumber(buffer.byteBufferData).getU())));

		swathValues.add(new ValueD1U8(beamGrp.getPing_time(),
				buffer -> new ULong((RawRange78.getXSFEpochTime(buffer.byteBufferData)))));

		swathValues.add(new ValueD1F4(beamGrp.getSound_speed_at_transducer(),
				buffer -> new FFloat(RawRange78.getSoundSpeed(buffer.byteBufferData).getU() * 0.1f)));

		if (stats.getInstallation().getModelNumber() == KmSounderLib.ME_70) {
			swathValues.add(new ValueD1S1(bathyGrp.getDetection_beam_stabilisation(),
					buffer -> new SByte(beam_stabilisation_t.stabilised.getValue())));
		} else {
			swathValues.add(new ValueD1S1(bathyGrp.getDetection_beam_stabilisation(),
					buffer -> new SByte(beam_stabilisation_t.not_stabilised.getValue())));
		}

		swathAntennaValues.add(new ValueD2U2(vendorBeamGrp.getTx_sector_count(),
				(buffer, a) -> RawRange78.getNTxTransmitSectorCounter(buffer.byteBufferData)));

		swathAntennaValues.add(new ValueD2F4(bathyVendorGrp.getDetection_sampling_freq(),
				(buffer, a) -> RawRange78.getSamplingFn(buffer.byteBufferData)));

		swathAntennaValues.add(new ValueD2U4(bathyVendorGrp.getDoppler_scale(),
				(buffer, a) -> RawRange78.getDscale(buffer.byteBufferData)));

		swathAntennaValues.add(new ValueD2U2(vendorBeamGrp.getBeam_count(),
				(buffer, a) -> RawRange78.getValidBeamCounter(buffer.byteBufferData)));

		// Declare the txSector related data

		swathTxValues.add(new ValueD2F4(vendorBeamGrp.getRaw_tx_beam_tilt_angle(), (buffer, i) -> {
			double angleDeg = RawRange78.getTxTilt(buffer.byteBufferData, i).get() * 0.01f;
			return new FFloat((float) angleDeg);
		}));

		swathTxValues.add(new ValueD2U2(vendorBeamGrp.getFocus_range(),
				(buffer, i) -> RawRange78.getFocusRange(buffer.byteBufferData, i)));

		swathTxValues.add(new ValueD2F4(beamGrp.getTransmit_duration_nominal(), (buffer, i) -> {
			float bdw = RawRange78.getSignalBandwidth(buffer.byteBufferData, i).get();
			float signal_length_nominal = 0;
			if (bdw != 0)
				signal_length_nominal = 1f / bdw;
			else
				signal_length_nominal = RawRange78.getSignalLength(buffer.byteBufferData, i).get();
			return new FFloat(signal_length_nominal);
		}));

		swathTxValues.add(new ValueD2F4(vendorBeamGrp.getTransmit_time_delay(),
				(buffer, i) -> RawRange78.getSectorTransmitDelay(buffer.byteBufferData, i)));

		swathTxValues.add(new ValueD2F4(beamGrp.getTransmit_frequency_start(),
				(buffer, i) -> computeFrequenciesStartAndStop(buffer, i)[0]));

		swathTxValues.add(new ValueD2F4(beamGrp.getTransmit_frequency_stop(),
				(buffer, i) -> computeFrequenciesStartAndStop(buffer, i)[1]));
		swathTxValues.add(new ValueD2F4(vendorBeamGrp.getCenter_frequency(),
				(buffer, i) -> RawRange78.getTxCenterFrequency(buffer.byteBufferData, i)));

		swathTxValues.add(new ValueD2U2(vendorBeamGrp.getMean_abs_coeff(),
				(buffer, i) -> RawRange78.getMeanAbsorptionCoef(buffer.byteBufferData, i)));

		swathTxValues.add(new ValueD2U1(vendorBeamGrp.getSignal_waveform(),
				(buffer, i) -> RawRange78.getWaveFormIdentifier(buffer.byteBufferData, i)));
		swathTxValues.add(new ValueD2S1(beamGrp.getTransmit_type(), (buffer, i) -> toTransmitType(buffer, i)));

		swathTxValues.add(new ValueD2F4(beamGrp.getTransmit_bandwidth(),
				(buffer, i) -> RawRange78.getSignalBandwidth(buffer.byteBufferData, i)));

		// declare the swath-beam variables
		/** Add variables with a ping/beam dimension */
		// addVar = rxGrp.addVariable("sounding_validity", DataType.BYTE, dimsSwathBeam, "Sounding validity", "Swath
		// beam mask validity");
		// addVar.setByteFillValue((byte) 0);
		// addVar.addAttribute("valid_min", 0);
		// addVar.addAttribute("valid_max", 1);
		// soundingValidityBuffer = new ValueD2U1(addVar, (buffer, beamIndexSource) -> {
		// UByte detectionInfo = RawRange78.getDetectionInfo(buffer.byteBufferData, beamIndexSource);
		// return detectionInfo.get() >= 0 ? new UByte((byte) 2) : new UByte((byte) 0);
		// });
		// swathBeamValues.add(soundingValidityBuffer);

		// we keep detection info from depth datagram since we do not want to have a valid detection but no depth set

		swathBeamValues.add(new ValueD2F4(bathyGrp.getDetection_beam_pointing_angle(),
				(buffer, i) -> new FFloat(RawRange78.getBeamPointingAngle(buffer.byteBufferData, i).get() * 0.01f)));

		swathBeamValues.add(new ValueD2U1(bathyGrp.getDetection_tx_beam(),
				(buffer, i) -> RawRange78.getTxSectorIndex(buffer.byteBufferData, i)));

		if (stats.getInstallation().getModelNumber() == KmSounderLib.EM_2040) {
			swathBeamValues.add(new ValueD2U2(bathyGrp.getDetection_tx_transducer_index(), (buffer, i) -> {
				int sectorIndex = RawRange78.getTxSectorIndex(buffer.byteBufferData, i).getInt();
				// retrieve associated transducer
				int txID = RawRange78.getTxSectorCounter(buffer.byteBufferData, sectorIndex).getInt();
				return new UShort((short) stats.getInstallation().getTxTransducerIndex(txID));
			}));

			swathTxValues.add(new ValueD2S4(beamGrp.getTransmit_transducer_index(), (buffer, i) -> {
				int txID = RawRange78.getTxSectorCounter(buffer.byteBufferData, i).getInt();
				return new SInt(stats.getInstallation().getTxTransducerIndex(txID));

			}));

		} else {
			swathBeamValues.add(new ValueD2S2(bathyGrp.getDetection_tx_transducer_index(),
					(buffer, i) -> new SShort((short) stats.getInstallation().getTxTransducerIndex(0))));
			swathTxValues.add(new ValueD2S4(beamGrp.getTransmit_transducer_index(),
					(buffer, i) -> new SInt(stats.getInstallation().getTxTransducerIndex(0))));
		}

		swathBeamValues.add(new ValueD2F4(bathyVendorGrp.getDetection_window_length(), (buffer, i) -> {
			return new FFloat(RawRange78.getDetectionLength(buffer.byteBufferData, i).getU() / RawRange78.getSamplingFn(
					buffer.byteBufferData).get());
		}));

		swathBeamValues.add(new ValueD2U1(bathyVendorGrp.getRawrange_quality_factor(),
				(buffer, i) -> RawRange78.getQualityFactor(buffer.byteBufferData, i)));

		swathBeamValues.add(new ValueD2S1(bathyVendorGrp.getDoppler_correction(),
				(buffer, i) -> RawRange78.getDcorr(buffer.byteBufferData, i)));

		swathBeamValues.add(new ValueD2F4(bathyGrp.getDetection_two_way_travel_time(),
				(buffer, i) -> RawRange78.getRange(buffer.byteBufferData, i)));

		// we do not use reflectivity and real time cleaning info, (use XYZ88)
	}

	@Override
	public void fillData(AllFile allFile) throws IOException, NCException {
		long origin[] = { 0, 0 };
		ObjectBufferPool<DatagramBuffer> pool = new ObjectBufferPool<>(() -> new DatagramBuffer(stats.getByteOrder()));

		for (Entry<SwathIdentifier, RawRange78CompletePingMetadata> pingEntry : metadata.getPings()) {
			swathAntennaValues.forEach(ValueD2::clear);
			swathTxValues.forEach(ValueD2::clear);
			swathBeamValues.forEach(ValueD2::clear);
			final RawRange78CompletePingMetadata ping = pingEntry.getValue();
			if (!ping.isComplete(stats.getGlobalMetadata().getSerialNumbers()) || !allFile.getGlobalMetadata()
					.getSwathIds().contains(pingEntry.getKey())) {
				continue;
			}

			List<Pair<DatagramBuffer, RawRange78CompleteAntennaPingMetadata>> data = DatagramReader.read(pool,
					ping.locate());

			// get origin from swath identifier (ignore if matching index not found)
			var swathId = pingEntry.getKey();
			var optIndex = stats.getGlobalMetadata().getSwathIds().getIndex(swathId);
			if (optIndex.isEmpty()) {
				DefaultLogger.get()
						.warn("Datagram RawRange78 associated with a missing ping is ignored (number = {}, sequence = {}).",
								swathId.getRawPingNumber(), swathId.getSwathPosition());
				continue;
			}
			origin[0] = optIndex.get();

			final int beamCount = ping.getBeamCount();
			final int ntx = ping.getNumTxSector();

			for (Pair<DatagramBuffer, RawRange78CompleteAntennaPingMetadata> antenna : data) {

				DatagramBuffer buffer = antenna.getFirst();
				int nTx = RawRange78.getNTxTransmitSectorCounter(buffer.byteBufferData).getU();

				// Swath values
				for (ValueD1 v : swathValues)
					v.fill(buffer);

				for (ValueD2 v : swathAntennaValues) {
					v.fill(antenna.getFirst(), 0, stats.getInstallation()
							.getRxAntennaIndex(RawRange78.getSerialNumber(buffer.byteBufferData)));
				}

				// fill the tx sector related data
				for (ValueD2 v : swathTxValues) {
					for (int i = 0; i < nTx; i++) {
						v.fill(buffer, i, i);
					}
				}

				// fill beam related data
				final int beamOffset = ping.getBeamOffset(RawRange78.getSerialNumber(buffer.byteBufferData).getU());
				final int offsetInDg = RawRange78.getStartingRxOffset(buffer.byteBufferData);
				for (int i = 0; i < antenna.getSecond().getBeamCount(); i++) {
					final int beamIndex = beamOffset + i;
					for (ValueD2 v : swathBeamValues) {
						v.fill(antenna.getFirst(), RawRange78.getBeamOffset(offsetInDg, i), beamIndex);
					}
				}
			}
			long[] originD1 = new long[] { origin[0] };
			// Write in netcdf variable
			for (ValueD1 v : swathValues)
				v.write(originD1);

			for (ValueD2 v : swathAntennaValues) {
				v.write(origin, new long[] { 1, stats.getGlobalMetadata().getSerialNumbers().size() });
			}
			for (ValueD2 v : swathTxValues) {
				v.write(origin, new long[] { 1, ntx });
			}
			for (ValueD2 v : swathBeamValues) {
				v.write(origin, new long[] { 1, beamCount });
			}

			for (Pair<DatagramBuffer, RawRange78CompleteAntennaPingMetadata> antenna : data) {
				pool.release(antenna.getFirst());
			}
		}
	}
}
