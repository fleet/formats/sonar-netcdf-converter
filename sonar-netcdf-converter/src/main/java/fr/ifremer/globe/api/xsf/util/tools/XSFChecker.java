package fr.ifremer.globe.api.xsf.util.tools;

import java.util.regex.Pattern;

import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCFile;
import fr.ifremer.globe.netcdf.ucar.NCFile.Mode;

public class XSFChecker {

    protected Pattern snakeCasePattern;
    
    public XSFChecker() {
        snakeCasePattern = Pattern.compile("^[a-z_0-9]*$");
    }
    
    /**
     * Check if the HSF file designated by path follows the convention
     * @param path
     * @return
     * @throws NCException 
     */
    public boolean check(String path) throws NCException {
        try (NCFile reader = NCFile.open(path, Mode.readonly)) {
        	//just check that file exist and is netcdf
        	//TODO add check for mandatory attributes
        	return true;
        }
    }
    
  
}
