package fr.ifremer.globe.api.xsf.converter.s7k.mbg;

import java.util.Calendar;
import java.util.TimeZone;

import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgBase;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgConstants;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgConstants.SoundingValidity;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2S1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2S4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2U2;
import fr.ifremer.globe.api.xsf.converter.common.xsf.types.DetectionTypeHelper;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K7006;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KFile;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KMultipingMetadata;
import fr.ifremer.globe.api.xsf.converter.s7k.xsf.S7kConstants;
import fr.ifremer.globe.api.xsf.util.types.SByte;
import fr.ifremer.globe.api.xsf.util.types.SInt;
import fr.ifremer.globe.api.xsf.util.types.UShort;
import fr.ifremer.globe.netcdf.api.convention.CFStandardNames;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCVariable;

public class S7K7006Converter extends AbstractDepthDatagramConverter {

	private static long startingDateUseQF;
	static {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, 2011);
		calendar.set(Calendar.MONTH, 03);
		calendar.set(Calendar.DAY_OF_MONTH, 19);
		calendar.setTimeZone(TimeZone.getTimeZone("GMT"));
		startingDateUseQF = calendar.getTime().getTime();
	}

	@Override
	protected S7KMultipingMetadata getMultipingMetada(S7KFile s7kFile) {
		return s7kFile.get7006Metadata().getEntries().iterator().next().getValue();
	}

	@Override
	public void declare(S7KFile s7kFile, MbgBase mbg) throws NCException {
		super.declare(s7kFile, mbg);

		// declares specific variables of S7K7006

		/** Sound velocity **/
		NCVariable soundVelVariable = mbg.getVariable(MbgConstants.SoundVelocity);
		double soundVelFactor = soundVelVariable.getAttributeDouble(CFStandardNames.CF_SCALE_FACTOR);
		double soundVelOffset = soundVelVariable.getAttributeDouble(CFStandardNames.CF_ADD_OFFSET);
		swathAntennaValues.add(new ValueD2U2(soundVelVariable, (buffer, iBeam) -> new UShort(
				(short) Math.round((S7K7006.getSoundVelocity(buffer).get() - soundVelOffset) * (1 / soundVelFactor)))));

		/** IFREMER quality factor **/
		NCVariable ifrQualityVariable = mbg.getVariable(MbgConstants.Quality);
		double qualityFactor = ifrQualityVariable.getAttributeDouble(CFStandardNames.CF_SCALE_FACTOR);
		swathBeamValues.add(new ValueD2S4(ifrQualityVariable, (buffer, iBeam) -> {
			float ifremerQualityFactor = isIfremerQFEnabled(buffer) ? S7K7006.getIntensity(buffer, iBeam).get() : 0;
			return new SInt((int) Math.round(ifremerQualityFactor * (1 / qualityFactor)));
		}));

		/** Compensation layer mode **/
		// u8 flag indicating if the layer compensation is on or off (- 0 = Off - 1 = On)
		NCVariable compLayerModeVariable = mbg.getVariable(MbgConstants.CompensationLayerMode);
		swathAntennaValues.add(new ValueD2S1(compLayerModeVariable, (buffer, antennaIndex) -> {
			byte layerCompensationFlag = S7K7006.getLayerCompensationFlag(buffer).getByte();
			if (layerCompensationFlag == 0)
				return new SByte(MbgConstants.COMPENSATION_LAYER_MODE_INACTIVE);
			if (layerCompensationFlag == 1)
				return new SByte(MbgConstants.COMPENSATION_LAYER_MODE_ACTIVE);
			return new SByte(MbgConstants.COMPENSATION_LAYER_MODE_UNKNOWN);
		}));

		/** Reflectivity **/
		NCVariable reflectivityVariable = mbg.getVariable(MbgConstants.Reflectivity);
		double reflectivityScaleFactor = reflectivityVariable.getAttributeDouble(CFStandardNames.CF_SCALE_FACTOR);
		swathBeamValues.add(new ValueD2S1(reflectivityVariable, (buffer, iBeam) -> {
			// check reflectivity field is not used for IFREMER QF & detection is not missing
			float reflectivity = !isIfremerQFEnabled(buffer)
					&& getDetectionValidity(buffer, iBeam) != SoundingValidity.MISSING
							? S7K7006.getIntensity(buffer, iBeam).get() * 2 // x2 to get value in dB
							: 0;
			return new SByte((byte) Math.round(reflectivity * (1 / reflectivityScaleFactor)));
		}));

	}

	/**
	 * @return true if the IFREMER quality facotr is enabled (if IFREMER quality factor is stored in reflectivity field
	 *         for SEABAT_7150/7111 since 19/03/11.
	 */
	private boolean isIfremerQFEnabled(BaseDatagramBuffer buffer) {
		int deviceId = S7K7006.getDeviceIdentifier(buffer).getInt();
		return ((deviceId == S7kConstants.SEABAT_7150 || deviceId == S7kConstants.SEABAT_7111)
				&& S7K7006.getTimeMilli(buffer) > startingDateUseQF);
	}

	@Override
	protected int getOptionalDataOffset(BaseDatagramBuffer buffer) {
		return (int) S7K7006.getOptionalDataOffset(buffer).getU();
	}

	@Override
	protected int getDeviceIdentifier(BaseDatagramBuffer buffer) {
		return (int) S7K7006.getDeviceIdentifier(buffer).getU();
	}

	@Override
	protected int getMultiPingSequence(BaseDatagramBuffer buffer) {
		return S7K7006.getMultiPingSequence(buffer).getU();
	}

	@Override
	protected long getPingNumber(BaseDatagramBuffer buffer) {
		return S7K7006.getPingNumber(buffer).getU();
	}

	@Override
	protected long getTimestamp(BaseDatagramBuffer buffer) {
		return S7K7006.getTimestampNano(buffer);
	}

	@Override
	protected long getTime(BaseDatagramBuffer buffer) {
		return S7K7006.getTimeMilli(buffer);
	}

	@Override
	protected double getLatitude(BaseDatagramBuffer buffer) {
		return S7K7006.getLatitude(buffer, getOptionalDataOffset(buffer)).get();
	}

	@Override
	protected double getLongitude(BaseDatagramBuffer buffer) {
		return S7K7006.getLongitude(buffer, getOptionalDataOffset(buffer)).get();
	}

	@Override
	protected float getHeading(BaseDatagramBuffer buffer) {
		return S7K7006.getHeading(buffer, getOptionalDataOffset(buffer)).get();
	}

	@Override
	protected float getRoll(BaseDatagramBuffer buffer) {
		return S7K7006.getRoll(buffer, getOptionalDataOffset(buffer)).get();
	}

	@Override
	protected float getPitch(BaseDatagramBuffer buffer) {
		return S7K7006.getPitch(buffer, getOptionalDataOffset(buffer)).get();
	}

	@Override
	protected float getHeave(BaseDatagramBuffer buffer) {
		return S7K7006.getHeave(buffer, getOptionalDataOffset(buffer)).get();
	}

	@Override
	protected float getVehiculeDepth(BaseDatagramBuffer buffer) {
		return S7K7006.getVehicleDepth(buffer, getOptionalDataOffset(buffer)).get();
	}

	@Override
	protected int getNumberOfDetectionPoints(BaseDatagramBuffer buffer) {
		return S7K7006.getNumberOfReceivedBeams(buffer).getInt();
	}

	@Override
	protected DetectionTypeHelper getDetectionType(BaseDatagramBuffer buffer, int iBeam) {
		byte detectionInfoByte = S7K7006.getQuality(buffer, iBeam).getByte();
		if ((detectionInfoByte & 0x04) > 0)
			return DetectionTypeHelper.AMPLITUDE;
		if ((detectionInfoByte & 0x08) > 0)
			return DetectionTypeHelper.PHASE;
		return DetectionTypeHelper.INVALID;
	}

	@Override
	protected SoundingValidity getDetectionValidity(BaseDatagramBuffer buffer, int beamIndex) {
		if (getDetectionType(buffer, beamIndex) == DetectionTypeHelper.INVALID)
			return SoundingValidity.MISSING;

		// if colinearity fail or brightness fail
		int resonQualityFactor = S7K7006.getQuality(buffer, beamIndex).getU();
		if (!isIfremerQFEnabled(buffer) && ((resonQualityFactor & 0x2) == 0 || (resonQualityFactor & 0x1) == 0))
			return SoundingValidity.UNVALID_ACQUISITION;

		return SoundingValidity.VALID;
	}

	@Override
	protected float getDetectionDepth(BaseDatagramBuffer buffer, int iBeam) {
		return S7K7006.getDepth(buffer, iBeam).get();
	}

	@Override
	protected float getBeamAzimuthAngle(BaseDatagramBuffer buffer, int iBeam) {
		return S7K7006.getAzimuthAngle(buffer, iBeam).get();
	}

	@Override
	protected float getBeamPointingAngle(BaseDatagramBuffer buffer, int iBeam) {
		return S7K7006.getPointingAngle(buffer, iBeam).get();
	}

	@Override
	protected float getDetectionAlongTrackDistance(BaseDatagramBuffer buffer, int iBeam) {
		return S7K7006.getAlongTrackDistance(buffer, iBeam).get();
	}

	@Override
	protected float getDetectionAcrossTrackDistance(BaseDatagramBuffer buffer, int iBeam) {
		return S7K7006.getAcrossTrackDistance(buffer, iBeam).get();
	}

	@Override
	protected int getQuality(BaseDatagramBuffer buffer, int iBeam) {
		return S7K7006.getQuality(buffer, iBeam).getU();
	}

	@Override
	protected float getFrequency(BaseDatagramBuffer buffer) {
		return S7K7006.getFrequency(buffer, getOptionalDataOffset(buffer)).get() / 1000; // frequency in kHz;
	}

	@Override
	protected float getRange(BaseDatagramBuffer buffer, int iBeam) {
		return S7K7006.getRange(buffer, iBeam).get();
	}
}
