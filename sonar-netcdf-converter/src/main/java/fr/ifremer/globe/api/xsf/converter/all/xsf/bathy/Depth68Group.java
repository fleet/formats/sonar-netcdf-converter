package fr.ifremer.globe.api.xsf.converter.all.xsf.bathy;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.depth68.Depth68;
import fr.ifremer.globe.api.xsf.converter.all.datagram.installation.InstallationMetadata.Antenna;
import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.AbstractCompleteAntennaPingSingleMetadata;
import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.AbstractCompletePingMetadata;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1F4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1F4WithAttitude;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2F4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2F4WithAttitude;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2F8WithPosition;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2S1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2S2;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2U1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2U2;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2WithAttitude;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2WithPosition;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfFromKongsberg;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.BathymetryVendorSpecificGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.types.DetectionTypeHelper;
import fr.ifremer.globe.api.xsf.util.types.DDouble;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.api.xsf.util.types.SByte;
import fr.ifremer.globe.api.xsf.util.types.SShort;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.maths.MatrixBuilder;
import fr.ifremer.globe.utils.maths.Ops;
import fr.ifremer.globe.utils.maths.Vector3D;

public class Depth68Group extends DepthGroup {

	/**
	 * The attributes list
	 */
	public Depth68Group(AllFile md) {
		super(md, md.getDepth68());
	}

	@Override
	public void declare(AllFile allFile, XsfFromKongsberg xsf) throws NCException {
		super.declare(allFile, xsf);

		int modelNum = stats.getGlobalMetadata().getModelNumber();
		BathymetryVendorSpecificGrp bathyVendorGrp = xsf.getBathy();

		/**********************************************************************************************************
		 * xsf common variables
		 **********************************************************************************************************/

		/** detection_x **/
		swathBeamValues.add(new ValueD2F4(bathyGrp.getDetection_x(), (buffer, iBeam) -> {
			return new FFloat((float)getAlongDistance(buffer, iBeam));
		}));

		/** detection_y **/
		swathBeamValues.add(new ValueD2F4(bathyGrp.getDetection_y(), (buffer, iBeam) -> {
			return new FFloat((float)getAcrossDistance(buffer, iBeam));
		}));

		/** ship_vertical_offset **/
		swathValuesWithAttitude.add(new ValueD1F4WithAttitude(beamGrp.getPlatform_vertical_offset(),
				/** Get interpolated attitude */
				(buffer, roll, pitch, heave) -> {

					MatrixBuilder.setDegRotationMatrix(0, pitch, roll, attitudeMatrix);
					Antenna txAnt = stats.getInstallation().getFirstTxAntenna(); // TODO case with several tx antenna
					Ops.mult(attitudeMatrix, new Vector3D(txAnt.getX(), txAnt.getY(), txAnt.getZ()), txAntennaSCS);

					return new FFloat((float) txAntennaSCS.getZ()
							- Depth68.getTransmitTransducerDepth(buffer.byteBufferData).getU() / 100f);
				}));

		/** detection_z : depth_from_antenna + zAntenna_in_SCS **/

		if ((modelNum == 120) || (modelNum == 300)) {
			swathBeamValuesWithAttitude
					.add(new ValueD2F4WithAttitude(bathyGrp.getDetection_z(), (buffer, iBeam, roll, pitch, heave) -> {

						// Get zAntenna in Sufarce Coordinate System
						MatrixBuilder.setDegRotationMatrix(0, pitch, roll, attitudeMatrix);
						Antenna txAnt = stats.getInstallation().getFirstTxAntenna(); // TODO manage case with several tx
																						// antenna
						Ops.mult(attitudeMatrix, new Vector3D(txAnt.getX(), txAnt.getY(), txAnt.getZ()), txAntennaSCS);

						float resolution = Depth68.getZResolution(buffer.byteBufferData).getU() / 100f;

						return new FFloat(Depth68.getDepth2U(buffer.byteBufferData, iBeam).getU() * resolution
								+ (float) txAntennaSCS.getZ());
					}));
		} else {
			swathBeamValuesWithAttitude
					.add(new ValueD2F4WithAttitude(bathyGrp.getDetection_z(), (buffer, iBeam, roll, pitch, heave) -> {

						// Get zAntenna in Sufarce Coordinate System
						MatrixBuilder.setDegRotationMatrix(0, pitch, roll, attitudeMatrix);
						Antenna txAnt = stats.getInstallation().getFirstTxAntenna(); // TODO manage case with several tx
																						// antenna
						Ops.mult(attitudeMatrix, new Vector3D(txAnt.getX(), txAnt.getY(), txAnt.getZ()), txAntennaSCS);

						float resolution = Depth68.getZResolution(buffer.byteBufferData).getU() / 100f;

						return new FFloat(Depth68.getDepth2S(buffer.byteBufferData, iBeam).get() * resolution
								+ (float) txAntennaSCS.getZ());
					}));
		}

				
		/** status unknown for those files : keep it valid **/
		swathBeamValues.add(new ValueD2S1(bathyGrp.getStatus(),
				(buffer, i) -> new SByte((byte)0)));
		
		/** type (phase/amp) **/
		swathBeamValues.add(new ValueD2S1(bathyGrp.getDetection_type(), (buffer, i) -> {
			byte detectionInfoByte = Depth68.getQualityFactor(buffer.byteBufferData, i).getByte();
			if ((detectionInfoByte & 0x80) == 0x00) { // amplitude detection
				return new SByte(DetectionTypeHelper.AMPLITUDE.get());
			} else {
				return new SByte(DetectionTypeHelper.PHASE.get());
			}			
		}));
		
		/** detection_constructor_quality_factor **/
		swathBeamValues.add(new ValueD2U1(bathyVendorGrp.getDetection_constructor_quality_factor(),
				(buffer, iBeam) -> Depth68.getQualityFactor(buffer.byteBufferData, iBeam)));

		/** tx_transducer_zdepth **/
		swathValues.add(new ValueD1F4(beamGrp.getTx_transducer_depth(),
				buffer -> new FFloat(Depth68.getTransmitTransducerDepth(buffer.byteBufferData).getU() / 100f)));

		/** Reflectivity **/
		swathBeamValues.add(new ValueD2F4(bathyGrp.getDetection_backscatter_r(), (buffer,
				iBeam) -> new FFloat((float) Depth68.getReflectivity(buffer.byteBufferData, iBeam).get() / 2)));

		/** Ship heading **/
		swathValues.add(new ValueD1F4(beamGrp.getPlatform_heading(),
				buffer -> new FFloat(Depth68.getHeadingOfVessel(buffer.byteBufferData).getU() / 100f)));

		/** Detection latitude */
		swathBeamValuesWithPosition.add(new ValueD2F8WithPosition(bathyGrp.getDetection_latitude(),
				(buffer, iBeam, latNav, lonNav, speedNav, positionMetadata) -> {
					double lat = getDetectionLatLong(buffer, iBeam, latNav, lonNav)[0];

					// Update geobox
					positionMetadata.computeBoundingBoxLat(lat);

					return new DDouble(lat);

				}));

		/** Detection longitude */
		swathBeamValuesWithPosition.add(new ValueD2F8WithPosition(bathyGrp.getDetection_longitude(),
				(buffer, iBeam, latNav, lonNav, speedNav, positionMetadata) -> {
					double lon = getDetectionLatLong(buffer, iBeam, latNav, lonNav)[1];

					// Update geobox
					positionMetadata.computeBoundingBoxLon(lon);

					return new DDouble(lon);
				}));

		/**********************************************************************************************************
		 * .all specific variables
		 **********************************************************************************************************/

		/** Add variables with a ping only dimension */

		swathAntennaValues.add(new ValueD2U1(xsf.getBeamGroupVendorSpecific().getDetection_mode(),
				(buffer, i) -> Depth68.getValidDetectionCount(buffer.byteBufferData)));

		swathAntennaValues.add(new ValueD2F4(xsf.getBeamGroupVendorSpecific().getZ_resolution(),
				(buffer, a) -> new FFloat(Depth68.getZResolution(buffer.byteBufferData).getU() * 0.01f)));

		swathAntennaValues.add(new ValueD2F4(xsf.getBeamGroupVendorSpecific().getXy_resolution(),
				(buffer, a) -> new FFloat(Depth68.getZResolution(buffer.byteBufferData).getU() * 0.01f)));

		if ((modelNum < 3002) || (modelNum > 3008)) {
			swathAntennaValues.add(new ValueD2F4(bathyVendorGrp.getDetection_sampling_freq(),
					(buffer, a) -> new FFloat(Depth68.getSamplingFrequency(buffer.byteBufferData).getU())));
			swathAntennaValues.add(new ValueD2F4(xsf.getBeamGroupVendorSpecific().getSeabed_image_sample_rate(),
					(buffer, a) -> new FFloat(Depth68.getSamplingFrequency(buffer.byteBufferData).getU())));
		} else {
			swathAntennaValues.add(new ValueD2S2(xsf.getBeamGroupVendorSpecific().getDepth_diff_between_head(),
					(buffer, a) -> Depth68.getDepthDiffBetweenSonarHeads(buffer.byteBufferData)));
		}

		/** Add variables with a ping/beam dimension */
		swathBeamValues.add(new ValueD2S2(bathyGrp.getDetection_rx_transducer_index(),
				(buffer, beamIndexSource) -> new SShort((short) stats.getInstallation()
						.getRxAntennaIndex(Depth68.getSerialNumber(buffer.byteBufferData)))));

		swathBeamValues.add(new ValueD2F4(bathyVendorGrp.getBeam_depression_angle(),
				(buffer, iBeam) -> new FFloat(Depth68.getBeamDepressionAngle(buffer.byteBufferData, iBeam).get() * 0.01f)));

		swathBeamValues.add(new ValueD2F4(bathyVendorGrp.getBeam_azimuth_angle(),
				(buffer, iBeam) -> new FFloat(Depth68.getBeamAzimuthAngle(buffer.byteBufferData, iBeam).getU() * 0.01f)));

		swathBeamValues.add(new ValueD2U2(bathyVendorGrp.getDetection_range(),
				(buffer, iBeam) -> Depth68.getRange(buffer.byteBufferData, iBeam)));

		swathBeamValues.add(new ValueD2F4(bathyVendorGrp.getDetection_window_length(),
				(buffer, iBeam) -> new FFloat(
						((float) Depth68.getLengthOfDetectionWindow(buffer.byteBufferData, iBeam).getU() * 4)
								/ Depth68.getSamplingFrequency(buffer.byteBufferData).getU())));

		/** for .all file, no backscatter calibration info is available */
		swathBeamValues
				.add(new ValueD2F4(bathyGrp.getDetection_backscatter_calibration(), (a, b) -> new FFloat(Float.NaN)));

	}

	@Override
	protected void fillSwathBeamValues(AbstractCompletePingMetadata<AbstractCompleteAntennaPingSingleMetadata> ping,
			DatagramBuffer buffer) {
		final int receivedBeamCount = Depth68.getValidDetectionCount(buffer.byteBufferData).getU();
		final int beamOffset = ping.getBeamOffset(Depth68.getSerialNumber(buffer.byteBufferData).getU());
		for (int i = 0; i < receivedBeamCount; i++) {
			final int indexInPing = beamOffset + Depth68.getBeamNumber(buffer.byteBufferData, i).getU() - 1;
			for (ValueD2 v : swathBeamValues)
				v.fill(buffer, i, indexInPing);
		}
	}

	@Override
	protected void fillSwathBeamValuesWithPosition(
			AbstractCompletePingMetadata<AbstractCompleteAntennaPingSingleMetadata> ping, DatagramBuffer buffer,
			double lat, double lon, double speed) {
		final int receivedBeamCount = Depth68.getValidDetectionCount(buffer.byteBufferData).getU();
		final int beamOffset = ping.getBeamOffset(Depth68.getSerialNumber(buffer.byteBufferData).getU());
		for (int i = 0; i < receivedBeamCount; i++) {
			final int indexInPing = beamOffset + Depth68.getBeamNumber(buffer.byteBufferData, i).getU() - 1;
			for (ValueD2WithPosition v : swathBeamValuesWithPosition)
				v.fill(buffer, i, indexInPing, lat, lon, speed, positionMetadata);
		}
	}

	@Override
	protected void fillSwathBeamValuesWithAttitude(
			AbstractCompletePingMetadata<AbstractCompleteAntennaPingSingleMetadata> ping, DatagramBuffer buffer,
			double roll, double pitch, double heave) {
		final int receivedBeamCount = Depth68.getValidDetectionCount(buffer.byteBufferData).getU();
		final int beamOffset = ping.getBeamOffset(Depth68.getSerialNumber(buffer.byteBufferData).getU());
		for (int i = 0; i < receivedBeamCount; i++) {
			final int indexInPing = beamOffset + Depth68.getBeamNumber(buffer.byteBufferData, i).getU() - 1;
			for (ValueD2WithAttitude v : swathBeamValuesWithAttitude)
				v.fill(buffer, i, indexInPing, roll, pitch, heave);
		}
	}

	@Override
	protected double getAlongDistance(BaseDatagramBuffer buffer, int iBeam) {
		float resolution = Depth68.getXYResolution(buffer.byteBufferData).getU() / 100f;
		return Depth68.getAlongDistance(buffer.byteBufferData, iBeam).get() * resolution + getActivePositionSCS().getX();
	}

	@Override
	protected double getAcrossDistance(BaseDatagramBuffer buffer, int iBeam) {
		float resolution = Depth68.getXYResolution(buffer.byteBufferData).getU() / 100f;
		return Depth68.getAcrossDistance(buffer.byteBufferData, iBeam).get() * resolution + getActivePositionSCS().getY();
	}

	@Override
	protected double getVesselHeading(BaseDatagramBuffer buffer) {
		return Depth68.getHeadingOfVessel(buffer.byteBufferData).getU() / 100.;
	}
}
