package fr.ifremer.globe.api.xsf.converter.all.datagram.seabed83;

import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.AbstractBeamDatagramMetadata;

public class Seabed83Metadata extends AbstractBeamDatagramMetadata<Seabed83CompletePingMetadata> {

    private float bsSum;
    private long bsNumber;
    public float bsMin, bsMax;
    
    public Seabed83Metadata() {
        bsSum       = 0;
        bsNumber    = 0;
        bsMin       = Float.MAX_VALUE;
        bsMax       = -Float.MAX_VALUE;
    }
    
    // Methodes pour les statistiques de Summary.
    public void addBS(float bsValue) {
        this.bsSum += bsValue;
        this.bsNumber++;
    }
    
    public float getBSMean() {
        return this.bsSum / this.bsNumber;
    }
    
    public void computeMinMaxBS(float bsValue) {
        bsMin = Float.min((float)(bsValue*0.1), (float) bsMin);
        bsMax = Float.max((float)(bsValue*0.1), (float) bsMax);     
    }
    
    public float getBSMin()
    {
        return bsMin;
    }
    public float getBSMax()
    {
        return bsMax;
    }   
}
