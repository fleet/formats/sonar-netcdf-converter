package fr.ifremer.globe.api.xsf.converter.common.utils.values.d2;

import java.util.Optional;

import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCVariable;

/***
 * class allocating storage for 2D matrix, and giving a interface allowing to fill the data
 */
public abstract class ValueD2Composite {
	public NCVariable variable;
	protected int dim;

	/***
	 * Constructor, allocate netcdf buffer for one variable and one ping
	 */
	public ValueD2Composite(NCVariable variable) {
		this.variable = variable;
		this.dim = (int) variable.getShape().get(1).getLength();
	}

	/**
	 * call the lambda to retrieve the value and set it in the allocated netcdf buffer dataOut
	 * 
	 * @param buffer the source buffer
	 * @param beamIndexSource the beam index in the source buffer
	 * @param other, another linked datagram this datagram is not of the same type, it can be missing in the file and
	 *            thus is optional
	 * @param the beam index in the destination buffer
	 */
	public abstract void fill(BaseDatagramBuffer buffer, int beamIndexSource, Optional<BaseDatagramBuffer> other, int beamIndexDest);

	/**
	 * Flush the data into the underlying netcdf variable.
	 * 
	 * @param origin
	 * @param count
	 * @throws NCException
	 */
	public abstract void write(long[] origin, long[] count) throws NCException;

	/**
	 * Resets data array.
	 */
	public abstract void clear();

}
