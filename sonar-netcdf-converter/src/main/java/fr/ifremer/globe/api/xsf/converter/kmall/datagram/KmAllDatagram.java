package fr.ifremer.globe.api.xsf.converter.kmall.datagram;

import java.nio.ByteBuffer;

import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.DatagramMetadata;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.KmallFile;

public interface KmAllDatagram {

    public void computeMetaData(KmallFile metadata, ByteBuffer datagram, String datagramType, DatagramPosition position);
    
    @SuppressWarnings("rawtypes")
    public default DatagramMetadata getSpecificMetadata(KmallFile metadata) {
        return metadata.getUnknown();
    }
}
