package fr.ifremer.globe.api.xsf.converter.common.utils.values.d1;

import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCVariable;

/***
 * class allocating storage for 1D matrix, and giving a interface allowing to fill the data with attitude
 */
public class ValueD1F4WithAttitude extends ValueD1WithAttitude {
	public ValueD1F4WithAttitude(NCVariable variable, ValueProvider filler) {
		super(variable);
		this.dataOut = new float[1];
		clear();
		this.valueProvider = filler;
	}

	protected float[] dataOut;
	ValueProvider valueProvider;

	public interface ValueProvider {
		public FFloat get(BaseDatagramBuffer buffer, double roll, double pitch, double heave);
	}

	/**
	 * call the lambda to retrieve the value and set it in the allocated netcdf buffer dataOut
	 */
	@Override
	public void fill(BaseDatagramBuffer buffer, double roll, double pitch, double heave) {
		dataOut[0] = valueProvider.get(buffer, roll, pitch, heave).get();
	}

	@Override
	public void write(long[] origin) throws NCException {
		try {
			variable.put(origin, new long[] { 1 }, dataOut);
		} catch (Exception e) {
			logger.error(String.format("Error while writing variable %s (%s)", variable.getName(), e.getMessage()));
			throw e;
		}
	}

	/** {@inheritDoc} */
	@Override
	public void clear() {
		dataOut[0] = variable.getFloatFillValue();
	}

}