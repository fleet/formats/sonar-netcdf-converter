package fr.ifremer.globe.api.xsf.converter.common.datagram.metadata;

import java.util.Map;
import java.util.Map.Entry;

import java.util.Set;
import java.util.TreeMap;

/**
 * Base class for all datagram metadata. Each kind of datagram have its own set
 * of specifics statistics and information
 */
public abstract class DatagramMetadata<KEY,T> {

    protected Map<KEY, T> index;
    
	public int datagramCount = 0;

	public DatagramMetadata() {
	    this.index = new TreeMap<>();
	}
	
	public void addDatagram() {
		datagramCount++;
	}
	
	public T getEntry(KEY key) {
	    return index.get(key);
	}
	
	public void setEntry(KEY key, T value) {
	    index.put(key, value);
	}
	
	public Set<Entry<KEY, T>> getEntries() {
	    return index.entrySet();
	}

	@Override
	public String toString() {
		return "-->" + this.getClass().getSimpleName() + System.lineSeparator() + "datagram count " + datagramCount;
	}

}
