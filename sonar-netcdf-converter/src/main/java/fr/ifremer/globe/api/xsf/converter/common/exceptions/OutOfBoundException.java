package fr.ifremer.globe.api.xsf.converter.common.exceptions;

/**
 * 
 * */
@SuppressWarnings("serial")
public class OutOfBoundException extends RuntimeException {
	public OutOfBoundException() {

	}

	public OutOfBoundException(String message) {
		super(message);
	}

}
