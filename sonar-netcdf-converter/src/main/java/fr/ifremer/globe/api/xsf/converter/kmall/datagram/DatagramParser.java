package fr.ifremer.globe.api.xsf.converter.kmall.datagram;

import java.nio.ByteBuffer;
import java.util.Map;
import java.util.TreeMap;

import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.KmallFile;

public class DatagramParser {
    
    private Map<String, KmAllBaseDatagram> parsers;
    private Unknown unknownDatagram;
    
    public DatagramParser(boolean parseWaterColumn) {
        unknownDatagram = new Unknown();
        parsers = new TreeMap<>();
        parsers.put("#IIP", new IIP());
        parsers.put("#IOP", new IOP());
        parsers.put("#MRZ", new MRZ());
        if(parseWaterColumn)
        {
        	parsers.put("#MWC", new MWC());
        }
        
        parsers.put("#SCL", new SCL());
        parsers.put("#SPO", new SPO());
        parsers.put("#SHI", new SHI());
        parsers.put("#SDE", new SDE());
        parsers.put("#SVP", new SVP());
        parsers.put("#SKM", new SKM());
    }

    /**
     * Extract the type of datagram from a DatagramBuffer
     * @param buffer
     * @return
     */
    public String getDatagramType(DatagramBuffer buffer) {
        return KmAllBaseDatagram.getDgmType(buffer.byteBufferData);
    }

    public void computeMetadata(KmallFile stats, ByteBuffer byteBufferData, DatagramPosition datagramPosition) {
        final String datagramType = KmAllBaseDatagram.getDgmType(byteBufferData);
        KmAllBaseDatagram datagram = parsers.get(datagramType);
        if (datagram != null) {
        	datagram.computeMetaData(stats, byteBufferData, datagramType, datagramPosition);            	
        } else {
            unknownDatagram.computeMetaData(stats, byteBufferData, datagramType, datagramPosition);
        }
    }
}
