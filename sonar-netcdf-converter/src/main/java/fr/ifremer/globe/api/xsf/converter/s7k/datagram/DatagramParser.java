package fr.ifremer.globe.api.xsf.converter.s7k.datagram;

import java.util.Map;
import java.util.TreeMap;

import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KFile;

public class DatagramParser {

	private Map<Long, S7KBaseDatagram> parsers;
	private S7KUnknownDatagram unknownDatagram;

	public DatagramParser(boolean readWC, boolean readAllDatagrams) {
		parsers = new TreeMap<>();

		parsers.put(7000l, new S7K7000());  
		parsers.put(7006l, new S7K7006()); // depth
		parsers.put(7027l, new S7K7027()); // depth
		parsers.put(7030l, new S7K7030()); // installation
		if(readWC)
		{	
			parsers.put(7041l, new S7K7041());
			parsers.put(7004l, new S7K7004());
		}
		// parse following datagrams only for XSF
		if (readAllDatagrams) {
			parsers.put(1003l, new S7K1003());
			parsers.put(1008l, new S7K1008());
			parsers.put(1010l, new S7K1010());
			parsers.put(1015l, new S7K1015());
			parsers.put(1016l, new S7K1016());
			parsers.put(7001l, new S7K7001());
			parsers.put(7009l, new S7K7009());
			parsers.put(7057l, new S7K7057());
			parsers.put(7058l, new S7K7058());
			parsers.put(7200l, new S7K7200());
		}
		unknownDatagram = new S7KUnknownDatagram();
	}

	public static long getDatagramType(DatagramBuffer buffer) {
		return S7KBaseDatagram.getRecordType(buffer).getU();
	}

	public void computeMetadata(S7KFile stats, DatagramBuffer buffer, DatagramPosition datagramPosition) {
		final long type = getDatagramType(buffer);
		S7KBaseDatagram datagram = parsers.get(type);
		if (datagram != null) {
			datagram.computeMetaData(stats, buffer, type, datagramPosition);
		} else {
			unknownDatagram.computeMetaData(stats, buffer, type, datagramPosition);
		}
	}
}
