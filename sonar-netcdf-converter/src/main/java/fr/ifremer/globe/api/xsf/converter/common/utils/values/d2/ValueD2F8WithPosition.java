package fr.ifremer.globe.api.xsf.converter.common.utils.values.d2;

import java.util.Arrays;

import fr.ifremer.globe.api.xsf.converter.all.datagram.position.PositionMetadata;
import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.api.xsf.util.types.DDouble;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCVariable;

/***
 * class allocating storage for 2D matrix, and giving a interface allowing to fill the data (with latitude and longitude
 * values)
 */
public class ValueD2F8WithPosition extends ValueD2WithPosition {
	protected double[] dataOut;
	private ValueProvider valueProvider;

	/** Constructor **/
	public ValueD2F8WithPosition(NCVariable variable, ValueProvider filler) {
		super(variable);
		// allocate storage
		this.dataOut = new double[dim];
		this.valueProvider = filler;
	}
	
	@Override
	public void clear() {
		Arrays.fill(this.dataOut, variable.getDoubleFillValue());
	}

	/**
	 * Here the lambda expression will expect latitude and longitude data in addition of the input buffer
	 */
	public interface ValueProvider {
		public DDouble get(BaseDatagramBuffer buffer, int beamIndexSource, double lat, double lon, double speed, PositionMetadata posMetadata);
	}

	/**
	 * call the lambda to retrieve the value and set it in the allocated netcdf buffer dataOut
	 */
	public void fill(BaseDatagramBuffer buffer, int beamIndexSource, int beamIndexDest, double lat, double lon, double speed, PositionMetadata posMetadata) {
		dataOut[beamIndexDest] = valueProvider.get(buffer, beamIndexSource, lat, lon, speed, posMetadata).get();
	}

	@Override
	public void write(long[] origin, long[] count) throws NCException {
		try {
			variable.put(origin, count, dataOut);
		} catch (Exception e) {
			logger.error(String.format("Error while writing variable %s (%s)", variable.getName(), e.getMessage()));
			throw e;

		}
	}
}
