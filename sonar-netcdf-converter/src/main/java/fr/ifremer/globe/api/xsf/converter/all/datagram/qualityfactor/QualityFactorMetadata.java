package fr.ifremer.globe.api.xsf.converter.all.datagram.qualityfactor;

import java.util.Set;

import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.DatagramMetadata;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;

/**
 * */
public class QualityFactorMetadata extends DatagramMetadata<SwathIdentifier,QualityFactorCompletePingMetadata> {
    
    public QualityFactorMetadata() {
        super();
    }
    
	long totalParamCount = 0;

	/**
	 * max Seabed beamCount per datagram
	 */
	int maxBeamCount = 0;

    /**
	 * @return the totalParamCount
	 */
	public long getTotalParamCount() {
		return totalParamCount;
	}

	/**
	 * push the value of max beam count per datagram
	 */
	public void pushMaxBeamCount(int beamCount) {
		maxBeamCount = Math.max(maxBeamCount, beamCount);
	}

	/**
	 * return the max QualityFactor beamCount per datagram
	 */
	public int getMaxBeamCount() {
		return maxBeamCount;
	}

	@Override
	public String toString() {
		return super.toString() + System.lineSeparator() + "param count "
				+ String.format("%,3d", getTotalParamCount()) + System.lineSeparator() + "max beam count "
				+ getMaxBeamCount();
	}

    public void computeIndex(Set<Integer> allAntennas) {
        totalParamCount = 0;
        maxBeamCount = 0;
        for (QualityFactorCompletePingMetadata ping : index.values()) {
            if (ping.isComplete(allAntennas)) {
                totalParamCount += ping.getParamCount();
                maxBeamCount = Integer.max(maxBeamCount, ping.getBeamCount());
            }
        }
    }
}
