package fr.ifremer.globe.api.xsf.converter.common;

import java.io.IOException;

import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgBase;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfBase;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.KmallFile;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KFile;
import fr.ifremer.globe.netcdf.ucar.NCException;

/**
 * This class provides methods to bind a datagram to the netcdf file.
 * 
 * @param <I> input sounder file type ( {@link AllFile}, {@link KmallFile}, {@link S7KFile}...)
 * @param <O> output netcdf file type ( {@link MbgBase}, {@link XsfBase}...)
 */
public interface IDatagramConverter<I, O> {

	/**
	 * Declares netcdf variables associated with the current type of datagram.
	 */
	void declare(I sounderFile, O netcdfFile) throws NCException, IOException;

	/**
	 * Loop on datagrams & fills the netcdf variables.
	 */
	void fillData(I sounderFile) throws IOException, NCException;
}