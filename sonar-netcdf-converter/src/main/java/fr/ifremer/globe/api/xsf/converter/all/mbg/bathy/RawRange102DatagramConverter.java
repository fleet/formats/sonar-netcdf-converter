package fr.ifremer.globe.api.xsf.converter.all.mbg.bathy;

import java.io.IOException;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.all.datagram.rawrange102.RawRange102;
import fr.ifremer.globe.api.xsf.converter.all.datagram.rawrange102.RawRange102CompleteAntennaPingMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.rawrange102.RawRange102CompletePingMetadata;
import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagram;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgBase;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgConstants;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgUtils;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2S2WithAttitude;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2WithAttitude;
import fr.ifremer.globe.api.xsf.util.DefaultLogger;
import fr.ifremer.globe.api.xsf.util.ObjectBufferPool;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.types.UShort;
import fr.ifremer.globe.netcdf.api.convention.CFStandardNames;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCVariable;
import fr.ifremer.globe.netcdf.util.Pair;

/**
 * Converter for RawRange datagram
 */
public class RawRange102DatagramConverter extends AbstractRawRangeDatagramConverter {

	@Override
	public void declare(AllFile allFile, MbgBase mbg) throws NCException {
		if (allFile.getDepth68().datagramCount < 1) {

			/** across angle **/
			NCVariable acrossBeamVariable = mbg.getVariable(MbgConstants.AcrossAngle);
			double acrossBeamFactor = acrossBeamVariable.getAttributeDouble(CFStandardNames.CF_SCALE_FACTOR);
			swathBeamWithAttitudeValues
					.add(new ValueD2S2WithAttitude(acrossBeamVariable, (buffer, beam, roll, pitch, heave) -> {
						double angle = RawRange102.getBeamPointingAngle(buffer.byteBufferData, beam).get() / 100d;

						if (allFile.getGlobalMetadata().getModelNumber() == 850)
							return (short) Math.round(angle / acrossBeamFactor);

						UShort serialNumber = RawRange102.getSerialNumber(buffer.byteBufferData);
						int index = allFile.getInstallation().getRxAntennaIndex(serialNumber);
						float antennaOffset = (float) allFile.getInstallation().getAntennas().get(index).getR();

						return (short) Math.round((-angle - antennaOffset - roll) / acrossBeamFactor);
					}));
		}
	}

	@Override
	public void fillData(AllFile allFile) throws IOException, NCException {
		long[] origin = { 0, 0 };
		ObjectBufferPool<DatagramBuffer> pool = new ObjectBufferPool<>(
				() -> new DatagramBuffer(allFile.getByteOrder()));
		TreeMap<Long, short[]> attitudeDatagrams = allFile.getAttitude().getSortedActiveAttitudeDatagrams();

		for (Entry<SwathIdentifier, RawRange102CompletePingMetadata> pingEntry : allFile.getRawRange102().getPings()) {
			final RawRange102CompletePingMetadata ping = pingEntry.getValue();
			// test for ping rejection
			if (!allFile.getGlobalMetadata().getSwathIds().contains(pingEntry.getKey())) {
				continue;
			}

			// clear buffers
			swathBeamValues.forEach(ValueD2::clear);
			swathBeamWithAttitudeValues.forEach(ValueD2WithAttitude::clear);

			List<Pair<DatagramBuffer, RawRange102CompleteAntennaPingMetadata>> data = DatagramReader.read(pool,
					ping.locate());

			// get origin from swath identifier (ignore if matching index not found)
			var swathId = pingEntry.getKey();
			var optIndex = allFile.getGlobalMetadata().getSwathIds().getIndex(swathId);
			if (optIndex.isEmpty()) {
				DefaultLogger.get().warn(
						"Datagram RawRange102 associated with a missing ping is ignored (number = {}, sequence = {}).",
						swathId.getRawPingNumber(), swathId.getSwathPosition());
				continue;
			}
			origin[0] = optIndex.get();

			for (Pair<DatagramBuffer, RawRange102CompleteAntennaPingMetadata> antenna : data) {
				DatagramBuffer buffer = antenna.getFirst();

				// fill beam related data
				// beam offset not computed as in other converter...?!
				final int beamOffset = ping.getBeamOffset(RawRange102.getSerialNumber(buffer.byteBufferData).getU());
				final int offsetInDg = RawRange102.getStartingRxOffset(buffer.byteBufferData);
				final int validBeams = RawRange102.getReceivedBeamCounter(buffer.byteBufferData).getU();

				for (int i = 0; i < validBeams; i++) {
					int bOffset = RawRange102.getBeamOffset(offsetInDg, i);
					int datagramBeamIndex = RawRange102.getBeamIndex(buffer.byteBufferData, bOffset).get();
					int beamIndex = beamOffset + datagramBeamIndex;

					int txSectorIndex = RawRange102.getTxSectorIndex(buffer.byteBufferData, offsetInDg).getInt();
					double beamDuration = RawRange102.getTransmitTimeOffset(buffer.byteBufferData, txSectorIndex).getU()
							+ RawRange102.getRange(buffer.byteBufferData, offsetInDg).getU();
					long currentTimeRx = BaseDatagram.getEpochTime(buffer.byteBufferData)
							+ ((long) (beamDuration * 1000));

					double[] attitude = MbgUtils.getAttitudeByInterpolation(currentTimeRx, attitudeDatagrams);

					for (ValueD2 v : swathBeamValues) {
						v.fill(antenna.getFirst(), bOffset, beamIndex);
					}

					for (ValueD2WithAttitude v : swathBeamWithAttitudeValues) {
						v.fill(antenna.getFirst(), bOffset, beamIndex, attitude[0], attitude[1], attitude[2]);
					}
				}
			}

			long[] count = { 1, allFile.getDetectionCount() };

			for (ValueD2 v : swathBeamValues)
				v.write(origin, count);

			for (ValueD2WithAttitude v : swathBeamWithAttitudeValues)
				v.write(origin, count);

			for (Pair<DatagramBuffer, RawRange102CompleteAntennaPingMetadata> antenna : data)
				pool.release(antenna.getFirst());

		}
	}

}
