package fr.ifremer.globe.api.xsf.converter.kmall.tools;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.nio.file.Paths;

import fr.ifremer.globe.api.xsf.converter.kmall.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.DatagramParser;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.DatagramReader;

/**
 * Tool used to generate one sample file for each datagram type seen in a batch
 * of kmall files.
 * 
 * This is intended to be used to generate a bunch of reference files for unit testing
 *
 */
public class KmAllDatagramExtractor {
    
    
    public static String input = "";
    public static String output = "";

    public static void main(String[] args) throws IOException {

        // make sure output exists
        final File outFile = new File(output);
        outFile.mkdirs();

        Files.walk(Paths.get(input))
        .filter(Files::isRegularFile)
        .filter((p) -> {
            return p.toString().endsWith(".kmall");
        })
        .forEach((f) -> {
            RandomAccessFile raf = null;
            try {
                raf = new RandomAccessFile(f.toFile(), "r");

                final DatagramBuffer buffer = new DatagramBuffer();
                final DatagramParser parser = new DatagramParser(true);

                while ((DatagramReader.readDatagram(raf, buffer) > 0)) {
                    final String datagramType = parser.getDatagramType(buffer);
                    final File dgmFolder = new File(outFile, datagramType);

                    if (!dgmFolder.exists()) {
                        dgmFolder.mkdir();
                        // generate a micro file
                        FileOutputStream binRawFile = new FileOutputStream(new File(dgmFolder, "datagram.kmall"));
                        binRawFile.write(buffer.rawBufferHeader);
                        binRawFile.write(buffer.rawBufferData, 0, buffer.byteBufferData.limit());
                        binRawFile.close();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (raf != null) {
                    try {
                        raf.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

    }
}
