package fr.ifremer.globe.api.xsf.converter.s7k.datagram;

import static java.lang.Math.toIntExact;

import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7K7006Metadata;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KFile;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.DDouble;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.api.xsf.util.types.UByte;
import fr.ifremer.globe.api.xsf.util.types.UInt;
import fr.ifremer.globe.api.xsf.util.types.ULong;
import fr.ifremer.globe.api.xsf.util.types.UShort;

public class S7K7006 extends S7KMultipingDatagram {

	@Override
	public void computeSpecificMetadata(S7KFile metadata, DatagramBuffer buffer, long type,
			DatagramPosition position, long time, SwathIdentifier swathId) {
		getSpecificMetadata(metadata).getOrCreateMetadataForDevice(getDeviceIdentifier(buffer).getU()).registerSwath(
				position, getPingNumber(buffer).getU(), getMultiPingSequence(buffer).getU(),
				toIntExact(getNumberOfReceivedBeams(buffer).getU()), getOptionalDataOffset(buffer).getU() != 0);
		metadata.getStats().getDate().register(getTimeMilli(buffer));
	}

	@Override
	public SwathIdentifier genSwathIdentifier(S7KFile stats, DatagramBuffer buffer, long time) {
		return stats.getSwathId(getPingNumber(buffer).getU(), getMultiPingSequence(buffer).getU(), time);
	}

	@Override
	public S7K7006Metadata getSpecificMetadata(S7KFile metadata) {
		return metadata.get7006Metadata();
	}

	public static ULong getSonarSerialNumber(BaseDatagramBuffer buffer) {
		return TypeDecoder.read8U(buffer.byteBufferData, 0);
	}

	public static UInt getPingNumber(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4U(buffer.byteBufferData, 8);
	}

	public static UShort getMultiPingSequence(BaseDatagramBuffer buffer) {
		return TypeDecoder.read2U(buffer.byteBufferData, 12);
	}

	public static UInt getNumberOfReceivedBeams(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4U(buffer.byteBufferData, 14);
	}

	public static UByte getLayerCompensationFlag(BaseDatagramBuffer buffer) {
		return TypeDecoder.read1U(buffer.byteBufferData, 18);
	}

	public static UByte getSoundVelocityFlag(BaseDatagramBuffer buffer) {
		return TypeDecoder.read1U(buffer.byteBufferData, 19);
	}

	public static FFloat getSoundVelocity(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 20);
	}

	public static FFloat getRange(BaseDatagramBuffer buffer, int i) {
		return TypeDecoder.read4F(buffer.byteBufferData, 24 + i * 4);
	}

	public static float[] getRange(BaseDatagramBuffer buffer) {
		final int nBeams = toIntExact(getNumberOfReceivedBeams(buffer).getU());
		float[] ret = new float[nBeams];
		for (int i = 0; i < nBeams; i++) {
			ret[i] = getRange(buffer, i).get();
		}
		return ret;
	}

	public static UByte getQuality(BaseDatagramBuffer buffer, int i) {
		final int nBeams = toIntExact(getNumberOfReceivedBeams(buffer).getU());
		return TypeDecoder.read1U(buffer.byteBufferData, 24 + nBeams * 4 + i);
	}

	public static UByte[] getQuality(BaseDatagramBuffer buffer) {
		final int nBeams = toIntExact(getNumberOfReceivedBeams(buffer).getU());
		UByte[] ret = new UByte[nBeams];
		for (int i = 0; i < nBeams; i++) {
			ret[i] = getQuality(buffer, i);
		}
		return ret;
	}

	public static FFloat getIntensity(BaseDatagramBuffer buffer, int i) {
		final int nBeams = toIntExact(getNumberOfReceivedBeams(buffer).getU());
		return TypeDecoder.read4F(buffer.byteBufferData, 24 + nBeams * 5 + i * 4);
	}

	public static float[] getIntensity(BaseDatagramBuffer buffer) {
		final int nBeams = toIntExact(getNumberOfReceivedBeams(buffer).getU());
		float[] ret = new float[nBeams];
		for (int i = 0; i < nBeams; i++) {
			ret[i] = getIntensity(buffer, i).get();
		}
		return ret;
	}

	public static FFloat getMinFilterInfo(BaseDatagramBuffer buffer, int i) {
		final int nBeams = toIntExact(getNumberOfReceivedBeams(buffer).getU());
		return TypeDecoder.read4F(buffer.byteBufferData, 24 + nBeams * 9 + i * 4);
	}

	public static float[] getMinFilterInfo(BaseDatagramBuffer buffer) {
		final int nBeams = toIntExact(getNumberOfReceivedBeams(buffer).getU());
		float[] ret = new float[nBeams];
		for (int i = 0; i < nBeams; i++) {
			ret[i] = getMinFilterInfo(buffer, i).get();
		}
		return ret;
	}

	public static FFloat getMaxFilterInfo(BaseDatagramBuffer buffer, int i) {
		final int nBeams = toIntExact(getNumberOfReceivedBeams(buffer).getU());
		return TypeDecoder.read4F(buffer.byteBufferData, 24 + nBeams * 13 + i * 4);
	}

	public static float[] getMaxFilterInfo(BaseDatagramBuffer buffer) {
		final int nBeams = toIntExact(getNumberOfReceivedBeams(buffer).getU());
		float[] ret = new float[nBeams];
		for (int i = 0; i < nBeams; i++) {
			ret[i] = getMaxFilterInfo(buffer, i).get();
		}
		return ret;
	}

	// Optional data
	public static FFloat getFrequency(BaseDatagramBuffer buffer, int optionalDataOffset) {
		return TypeDecoder.read4F(buffer.byteBufferData, optionalDataOffset - 64);
	}

	public static DDouble getLatitude(BaseDatagramBuffer buffer, int optionalDataOffset) {
		return TypeDecoder.read8F(buffer.byteBufferData, optionalDataOffset - 60);
	}

	public static DDouble getLongitude(BaseDatagramBuffer buffer, int optionalDataOffset) {
		return TypeDecoder.read8F(buffer.byteBufferData, optionalDataOffset - 52);
	}

	public static FFloat getHeading(BaseDatagramBuffer buffer, int optionalDataOffset) {
		return TypeDecoder.read4F(buffer.byteBufferData, optionalDataOffset - 44);
	}

	public static UByte getHeightSource(BaseDatagramBuffer buffer, int optionalDataOffset) {
		return TypeDecoder.read1U(buffer.byteBufferData, optionalDataOffset - 40);
	}

	public static FFloat getTide(BaseDatagramBuffer buffer, int optionalDataOffset) {
		return TypeDecoder.read4F(buffer.byteBufferData, optionalDataOffset - 39);
	}

	public static FFloat getRoll(BaseDatagramBuffer buffer, int optionalDataOffset) {
		return TypeDecoder.read4F(buffer.byteBufferData, optionalDataOffset - 35);
	}

	public static FFloat getPitch(BaseDatagramBuffer buffer, int optionalDataOffset) {
		return TypeDecoder.read4F(buffer.byteBufferData, optionalDataOffset - 31);
	}

	public static FFloat getHeave(BaseDatagramBuffer buffer, int optionalDataOffset) {
		return TypeDecoder.read4F(buffer.byteBufferData, optionalDataOffset - 27);
	}

	public static FFloat getVehicleDepth(BaseDatagramBuffer buffer, int optionalDataOffset) {
		return TypeDecoder.read4F(buffer.byteBufferData, optionalDataOffset - 23);
	}

	public static class OptionalBeamData {
		public float depth;
		public float alongTrackDistance;
		public float acrossTrackDistance;
		public float pointingAngle;
		public float azimuthAngle;
	}

	public static FFloat getDepth(BaseDatagramBuffer buffer, int i) {
		final int optionalDataOffset = toIntExact(getOptionalDataOffset(buffer).getU());
		return TypeDecoder.read4F(buffer.byteBufferData, optionalDataOffset - 19 + i * 20);
	}

	public static FFloat getAlongTrackDistance(BaseDatagramBuffer buffer, int i) {
		final int optionalDataOffset = toIntExact(getOptionalDataOffset(buffer).getU());
		return TypeDecoder.read4F(buffer.byteBufferData, optionalDataOffset - 15 + i * 20);
	}

	public static FFloat getAcrossTrackDistance(BaseDatagramBuffer buffer, int i) {
		final int optionalDataOffset = toIntExact(getOptionalDataOffset(buffer).getU());
		return TypeDecoder.read4F(buffer.byteBufferData, optionalDataOffset - 11 + i * 20);
	}

	public static FFloat getPointingAngle(BaseDatagramBuffer buffer, int i) {
		final int optionalDataOffset = toIntExact(getOptionalDataOffset(buffer).getU());
		return TypeDecoder.read4F(buffer.byteBufferData, optionalDataOffset - 7 + i * 20);
	}

	public static FFloat getAzimuthAngle(BaseDatagramBuffer buffer, int i) {
		final int optionalDataOffset = toIntExact(getOptionalDataOffset(buffer).getU());
		return TypeDecoder.read4F(buffer.byteBufferData, optionalDataOffset - 3 + i * 20);
	}

	public static OptionalBeamData[] getOptionalBeamData(BaseDatagramBuffer buffer, int optionalDataOffset) {
		final int numOfBeams = toIntExact(getNumberOfReceivedBeams(buffer).getU());
		OptionalBeamData[] ret = new OptionalBeamData[numOfBeams];
		for (int i = 0; i < numOfBeams; i++) {
			OptionalBeamData v = new OptionalBeamData();
			v.depth = getDepth(buffer, i).get();
			v.alongTrackDistance = getAlongTrackDistance(buffer, i).get();
			v.acrossTrackDistance = getAcrossTrackDistance(buffer, i).get();
			v.pointingAngle = getPointingAngle(buffer, i).get();
			v.azimuthAngle = getAzimuthAngle(buffer, i).get();
			ret[i] = v;
		}
		return ret;
	}
}
