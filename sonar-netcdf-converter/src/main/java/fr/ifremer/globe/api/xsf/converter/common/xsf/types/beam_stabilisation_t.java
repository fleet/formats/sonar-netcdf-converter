package fr.ifremer.globe.api.xsf.converter.common.xsf.types;
/**
 * "Indicate Whether or not the beam direction is compensated for platform motion.
 * */
public enum beam_stabilisation_t {
	not_stabilised((byte)0),
	stabilised((byte)1);
	private byte value;

	public byte getValue() {
		return value;
	}

	beam_stabilisation_t(byte value)
	{
		this.value=value;
	}
}
