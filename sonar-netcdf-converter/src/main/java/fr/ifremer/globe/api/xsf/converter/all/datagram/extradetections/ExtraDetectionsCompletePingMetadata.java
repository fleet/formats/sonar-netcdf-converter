package fr.ifremer.globe.api.xsf.converter.all.datagram.extradetections;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.all.datagram.PingDatagram;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.util.ObjectBufferPool;
import fr.ifremer.globe.api.xsf.util.types.UShort;
import fr.ifremer.globe.netcdf.util.Pair;

/**
 * Extra detections ping datagram can be stored in several parts,
 * <p>
 * This class represents a complete ping metadata, ie the x datagrams
 */
public class ExtraDetectionsCompletePingMetadata {
	TreeMap<Integer, ExtraDetectionsCompleteAntennaPingMetadata> map = new TreeMap<>();

    private int detectionClassesCount;
	private int detectionCount;
	private int sampleCount;

	public ExtraDetectionsCompletePingMetadata() {
	}
	
	public void addAntenna(int serialnumber, DatagramPosition position, int detectionClasses, int detections, int samples) {
	    map.put(serialnumber, new ExtraDetectionsCompleteAntennaPingMetadata(position, detectionClasses, detections, samples));
	}

	/**
	 * compute Extradetections for this complete ping. If all datagram has not yet
	 * been added, the result can change
	 */
	public int getExtradetectionsCount() {
		int result = 0;
		for (ExtraDetectionsCompleteAntennaPingMetadata antennaPing : map.values()) {
			result += antennaPing.detectionCount;
		}
		return result;
	}

	public List<Pair<DatagramBuffer, ExtraDetectionsCompleteAntennaPingMetadata>> read(
			ObjectBufferPool<DatagramBuffer> pool) throws IOException {
		ArrayList<Pair<DatagramBuffer, ExtraDetectionsCompleteAntennaPingMetadata>> result = new ArrayList<>();
		// for each antenna
		for (ExtraDetectionsCompleteAntennaPingMetadata antennaPing : map.values()) {
			// for each datagram
				DatagramBuffer buffer = pool.borrow();
				DatagramReader.readDatagramAt(antennaPing.position.getFile(), buffer, antennaPing.position.getSeek());

				result.add(new Pair<DatagramBuffer, ExtraDetectionsCompleteAntennaPingMetadata>(buffer, antennaPing));
		}
		return result;
	}

	public UShort getPingNumber(List<Pair<DatagramBuffer, ExtraDetectionsCompleteAntennaPingMetadata>> data) {
		DatagramBuffer datagram = data.get(0).getFirst();
		return PingDatagram.getPingNumber(datagram.byteBufferData);
	}

	/**
	 * compute completeness for this ping, ie all datagram have been received
	 */
	public boolean isComplete(Set<Integer> allSNs) {
		boolean result = true;
		
		for (int sn : allSNs) {
		    result &= map.containsKey(sn);
		}

		return result;
	}

	/**
	 * Called after a first full reading of all datagram is done compute
	 * indexation and links between global values (like beamIndex) and datagrams
	 */
	public void computeIndex() {
	    this.detectionClassesCount = 0;
	    this.detectionCount = 0;
	    this.sampleCount = 0;
	    
	    for (ExtraDetectionsCompleteAntennaPingMetadata antenna : map.values()) {
	        this.detectionClassesCount = Integer.max(this.detectionClassesCount, antenna.detectionClassesCount);
	        this.detectionCount += antenna.detectionCount;
	        this.sampleCount += antenna.rawSampleCount;
	    }
	};

    public int getDetectionClassesCount() {
        return detectionClassesCount;
    }

    public int getDetectionCount() {
        return detectionCount;
    }

    public int getSampleCount() {
        return sampleCount;
    }

    public int getDetectionOffset(int serial) {
        int ret = 0;
        for (int sn : map.keySet()) {
            if (sn == serial) {
                return ret;
            }
            ret += map.get(sn).detectionCount;
        }
        return ret;
    }
    
    public int getSampleOffset(int serial) {
        int ret = 0;
        for (int sn : map.keySet()) {
            if (sn == serial) {
                return ret;
            }
            ret += map.get(sn).rawSampleCount;
        }
        return ret;
    }
}
