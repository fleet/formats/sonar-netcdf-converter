package fr.ifremer.globe.api.xsf.converter.s7k.metadata;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

import fr.ifremer.globe.api.xsf.converter.common.utils.DoubleStatCalculator;
import fr.ifremer.globe.api.xsf.converter.common.utils.FloatStatCalculator;
import fr.ifremer.globe.api.xsf.converter.common.utils.LongStatCalculator;
import fr.ifremer.globe.api.xsf.converter.common.utils.MinMaxCalculator;

public class S7KStats {

	private Set<Long> deviceIds;
	private MinMaxCalculator<Long> date;
	private MinMaxCalculator<Double> lat;
	private MinMaxCalculator<Double> lon;
	private MinMaxCalculator<Double> detectionLat;
	private MinMaxCalculator<Double> detectionLon;
	private MinMaxCalculator<Double> sog;
	private MinMaxCalculator<Double> cog;
	private MinMaxCalculator<Float> depth;
	private MinMaxCalculator<Float> heading;
	private MinMaxCalculator<Float> roll;
	private MinMaxCalculator<Float> pitch;
	private MinMaxCalculator<Float> heave;
	private MinMaxCalculator<Float> ssp;
	
	public S7KStats() {
		this.deviceIds = new TreeSet<Long>();
		this.date = new LongStatCalculator();
		this.lat = new DoubleStatCalculator();
		this.lon = new DoubleStatCalculator();
		this.detectionLon = new DoubleStatCalculator();
		this.detectionLat = new DoubleStatCalculator();
		this.sog = new DoubleStatCalculator();
		this.cog = new DoubleStatCalculator();
		this.depth = new FloatStatCalculator();
		this.heading = new FloatStatCalculator();
		this.roll = new FloatStatCalculator();
		this.pitch = new FloatStatCalculator();
		this.heave = new FloatStatCalculator();
		this.ssp = new FloatStatCalculator();
	}
	
	public MinMaxCalculator<Long> getDate() {
		return this.date;
	}

	public MinMaxCalculator<Double> getLat() {
		return this.lat;
	}

	public MinMaxCalculator<Double> getLon() {
		return this.lon;
	}
	
	public MinMaxCalculator<Double> getDetectionLat() {
		return detectionLat;
	}

	public MinMaxCalculator<Double> getDetectionLon() {
		return detectionLon;
	}

	public MinMaxCalculator<Double> getSog() {
		return this.sog;
	}
	
	public MinMaxCalculator<Double> getCog() {
		return this.cog;
	}

	public MinMaxCalculator<Float> getDepth() {
		return this.depth;
	}
	
	public MinMaxCalculator<Float> getHeading() {
		return this.heading;
	}

	public MinMaxCalculator<Float> getRoll() {
		return roll;
	}

	public MinMaxCalculator<Float> getPitch() {
		return pitch;
	}

	public MinMaxCalculator<Float> getHeave() {
		return heave;
	}

	public MinMaxCalculator<Float> getSsp() {
		return ssp;
	}

	public Set<Long> getDeviceIds() {
		return deviceIds;
	}
	
	/**
	 * @return the device ID SEABAT_7125 (Victor) or SEABAT_7150 (PP)
	 */
	public int getUniqueDeviceId() {
		Iterator<Long> it = deviceIds.iterator();
		while (it.hasNext()) {
			long modelNumber = it.next();
			if (modelNumber != 7003 && modelNumber != 7000)
				return (int) modelNumber;
		}
		return -1;
	}
}
