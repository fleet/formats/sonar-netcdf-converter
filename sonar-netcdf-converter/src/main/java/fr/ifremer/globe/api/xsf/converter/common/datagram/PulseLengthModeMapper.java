package fr.ifremer.globe.api.xsf.converter.common.datagram;

import fr.ifremer.globe.utils.exception.GIOException;

import java.util.stream.Stream;

/**
 * Utility mapper from .all or .kmall files to xsf runtime mode
 */
public class PulseLengthModeMapper {
    /**
     * Enum for Pulse length mode (EM2040 and 2040C, see mode2 parameter)
     */
    public enum AllPulseLengthMode {
        UNKNOWN(-1), VERY_SHORT_CW(0), SHORT_CW(1), MEDIUM_CW(2), LONG_CW(3), VERY_LONG_CW(4), EXTRA_LONG_CW(
                5), SHORT_FM(6), LONG_FM(7);

        public final int value;

        AllPulseLengthMode(int value) {
            this.value = value;
        }

        /**
         * Map a 2040 pulse length mode to {@link AllPulseLengthMode}
         */
        public static AllPulseLengthMode from2040Value(int value) {

            /**
             * Pulselength (EM 2040)  xxxx 00xx - Short CW  xxxx 01xx - Medium CW  xxxx 10xx - Long CW  xxxx 11xx -
             * FM
             */
            value = value & 0xc; // xxxx 1100
            if (value == 0)
                return SHORT_CW;
            if (value == 4) // 0100
                return MEDIUM_CW;
            if (value == 8) // 1000
                return LONG_CW;
            if (value == 12) // 1100
                return SHORT_FM;
            return UNKNOWN;
        }

        /**
         * Map a 2040 pulse length mode to {@link AllPulseLengthMode}
         *
         * @throws GIOException
         */
        public static AllPulseLengthMode from2040CValue(int v) throws GIOException {
            v = v & 0x70;// 0111 0000
            int value_f = v >> 4;

            return Stream.of(values()).//
                    filter(x -> x.value == value_f).//
                    findFirst().//
                    orElseThrow(() -> new GIOException("Trying to match invalid pulse length mode :" + value_f));
        }

    }

    /**
     * XSF pulse mode values
     */
    public enum XSFPulseLengthMode {
        UNKNOWN(-1), VERY_SHORT_CW(0), SHORT_CW(1), MEDIUM_CW(2), LONG_CW(3), VERY_LONG_CW(4), EXTRA_LONG_CW(
                5), SHORT_FM(6), LONG_FM(7);

        public final int value;

        XSFPulseLengthMode(int value) {
            this.value = value;
        }

        static XSFPulseLengthMode fromValue(AllPulseLengthMode srcMode) throws GIOException {
            // mapping is one to one for both enum values
            return Stream.of(values()).//
                    filter(x -> x.value == srcMode.value).//
                    findFirst().//
                    orElseThrow(() -> new GIOException("Trying to match invalid pulse length  value :" + srcMode.value));
        }
    }

    public static XSFPulseLengthMode fromAll(AllPulseLengthMode srcMode) throws GIOException {
        return XSFPulseLengthMode.fromValue(srcMode);
    }
}
