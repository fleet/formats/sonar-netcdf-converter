package fr.ifremer.globe.api.xsf.converter.all.xsf.extra;

import java.io.IOException;

import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfFromKongsberg;
import fr.ifremer.globe.netcdf.ucar.NCException;

public class ExtraDetectionsGroup implements IDatagramConverter<AllFile, XsfFromKongsberg> {
	/*
	 * private FileMetaData stats; private ExtraDetectionsMetadata metadata;
	 * 
	 * private List<ValueD2> swathValues; private List<ValueD2> swathDetClassesValues; private List<ValueD2>
	 * swathExtraDetValues; private NCVariable variableSamples; private NCVariable variableSampleOffset;
	 */

	public ExtraDetectionsGroup(AllFile md) {
		/*
		 * this.stats = md; this.metadata = md.getExtraDetections(); swathValues = new LinkedList<>();
		 * swathDetClassesValues = new LinkedList<>(); swathExtraDetValues = new LinkedList<>();
		 */
	}

	@Override
	public void declare(AllFile allFile, XsfFromKongsberg xsf) throws NCException {

		// TODO extra detection is removed waiting for them to be defined in XSF

		// WARNING : IF NUMBER OF EXTRA DETECTION EXCEED NUMBER OF PING THE DATAGRAM SHOULD BE DISCARDED

		/*
		 * NCGroup rootGrp = xsf.getSounderGrp();
		 * 
		 * // declare the groups that will be used NCGroup edGrp = rootGrp.addGroup("extra_detections"); NCGroup rxDCGrp
		 * = edGrp.addGroup("rx_detection_classes"); NCGroup rxEDGrp = edGrp.addGroup("rx_extra_detections");
		 * 
		 * // declare the dimensions NCDimension swathDim = rootGrp.addDimension("swath_dim",
		 * stats.getOverallMetadata().getSwathCount()); NCDimension rxAntennaDim =
		 * rootGrp.addDimension("rx_antenna_dim", stats.getOverallMetadata().getSerialNumbers().size()); NCDimension
		 * detectionClassesDim = edGrp.addDimension("detection_classes_dim", metadata.getDetectionClasses());
		 * NCDimension extraDetectionsDim = edGrp.addDimension("extra_detections_dim", metadata.getMaxExtraDections());
		 * NCDimension samplesDim = edGrp.addDimension("sample_dim", stats.getExtraDetections().getTotalSampleCount());
		 * 
		 * // declare the various shapes that will be used List<NCDimension> dimsSwath = Arrays.asList(swathDim,
		 * rxAntennaDim); List<NCDimension> dimsSwathDC = Arrays.asList(swathDim, detectionClassesDim);
		 * List<NCDimension> dimsSwathED = Arrays.asList(swathDim, extraDetectionsDim); List<NCDimension> dims1DSamples
		 * = Arrays.asList(new NCDimension[]{ samplesDim });
		 * 
		 * // Create the swath only variables NCVariable addVar = edGrp.addVariable("swath_validity", DataType.BYTE,
		 * dimsSwath, "swath validity", "Swath mask validity"); addVar.setByteFillValue( (byte) 0 ); swathValues.add(
		 * new ValueD2U1(addVar, (buffer, i) -> { return new UByte((byte)1); })); addVar.addAttribute("valid_min", 0);
		 * addVar.addAttribute("valid_max", 1);
		 * 
		 * addVar = rootGrp.addVariable("ping_raw_count", DataType.USHORT, dimsSwath); addVar.addAttribute("valid_min",
		 * 0); addVar.addAttribute("valid_max", 65535); swathValues.add( new ValueD2U2( addVar, (buffer, i) -> { return
		 * ExtraDetections.getPingNumber(buffer.byteBufferData); } ));
		 * 
		 * addVar = rootGrp.addVariable("time", DataType.ULONG, dimsSwath, "time", "time",
		 * "milliseconds since 1970-01-01 00:00:00 +00:00"); addVar.addAttribute("valid_min", 0);
		 * addVar.addAttribute("valid_max", 86399999); swathValues.add( new ValueD2U8( addVar, (buffer, i) -> { return
		 * new ULong(ExtraDetections.getEpochTime(buffer.byteBufferData)); }));
		 * 
		 * addVar = rootGrp.addVariable("time_nano_seconds", DataType.UINT, dimsSwath, "nanoseconds", "nanoseconds",
		 * "nanoseconds"); swathValues.add( new ValueD2U4( addVar, (buffer, i) -> { return new UInt(0); }));
		 * 
		 * addVar = edGrp.addVariable("datagram_counter", DataType.USHORT, dimsSwath, "datagram counter",
		 * "Datagram Counter"); swathValues.add( new ValueD2U2( addVar, (buffer, i) -> { return
		 * ExtraDetections.getDatagramCounter(buffer.byteBufferData);}));
		 * 
		 * addVar = edGrp.addVariable("datagram_version_id", DataType.USHORT, dimsSwath, "datagram version id",
		 * "Datagram version ID"); swathValues.add( new ValueD2U2( addVar, (buffer, i) -> { return
		 * ExtraDetections.getDatagramVersionID(buffer.byteBufferData);}));
		 * 
		 * addVar = rootGrp.addVariable("heading", DataType.USHORT, dimsSwath, "heading", "heading of vessel",
		 * "degrees"); addVar.addAttribute("scale_factor", 0.01); addVar.addAttribute("valid_min", 0);
		 * addVar.addAttribute("valid_max", 3599); swathValues.add( new ValueD2U2( addVar, (buffer, i) -> {return
		 * ExtraDetections.getHeading(buffer.byteBufferData);} ));
		 * 
		 * addVar = rootGrp.addVariable("sound_speed_at_transducer", DataType.USHORT, dimsSwath, "sound_speed",
		 * "sound speed at transducer depth", "meters/sec"); addVar.addAttribute("scale_factor", 0.1); swathValues.add(
		 * new ValueD2U2( addVar, (buffer, i) -> {return ExtraDetections.getSoundVelocityAtTx(buffer.byteBufferData);}
		 * ));
		 * 
		 * addVar = rootGrp.addVariable("depth_reference_point", DataType.FLOAT, dimsSwath, "depth reference point",
		 * "Depth of reference point", "meters"); swathValues.add( new ValueD2F4( addVar, (buffer, i) -> {return
		 * ExtraDetections.getDepthOfReferencePoint(buffer.byteBufferData);} ));
		 * 
		 * addVar = rootGrp.addVariable("water_column_sample_rate", DataType.FLOAT, dimsSwath,
		 * "water_column_sample_rate", "Water Column sample rate (ExtraDetectionssr)"); swathValues.add( new ValueD2F4(
		 * addVar, (buffer, i) -> { return ExtraDetections.getWaterColumnSampleRate(buffer.byteBufferData); }));
		 * 
		 * addVar = rootGrp.addVariable("raw_amplitude_sample_rate", DataType.FLOAT, dimsSwath,
		 * "Raw Amplitude Sample Rate", "Raw amplitude (Seabed Image) sample rate (SIsr)");
		 * addVar.addAttribute("valid_min", 0.1); swathValues.add( new ValueD2F4( addVar, (buffer, i) -> { return
		 * ExtraDetections.getRawAmplitudeSampleRate(buffer.byteBufferData); }));
		 * 
		 * addVar = rootGrp.addVariable("rx_transducer_index", DataType.USHORT, dimsSwath, "Rx transducer index",
		 * "RX transducer index"); swathValues.add( new ValueD2U2( addVar, (buffer, i) -> { return
		 * ExtraDetections.getRXTransducerIndex(buffer.byteBufferData); }));
		 * 
		 * addVar = edGrp.addVariable("extra_detections_counter", DataType.USHORT, dimsSwath,
		 * "number of extra detections", "Number of extra detections (Nd)"); addVar.addAttribute("valid_min", 0);
		 * swathValues.add( new ValueD2U2( addVar, (buffer, i) -> { return
		 * ExtraDetections.getExtraDetectionsCounter(buffer.byteBufferData); }));
		 * 
		 * addVar = edGrp.addVariable("detections_classes_counter", DataType.USHORT, dimsSwath,
		 * "number of detection classes", "Number of detection classes (Nc)"); addVar.addAttribute("valid_min", 10);
		 * swathValues.add( new ValueD2U2( addVar, (buffer, i) -> { return
		 * ExtraDetections.getDetectionClassesCounter(buffer.byteBufferData); }));
		 * 
		 * addVar = edGrp.addVariable("byte_per_class_counter", DataType.USHORT, dimsSwath, "number of byte per class",
		 * "Number of byte per class (cycle 1)"); swathValues.add( new ValueD2U2( addVar, (buffer, i) -> { return
		 * ExtraDetections.getBytePerClassCounter(buffer.byteBufferData); }));
		 * 
		 * addVar = rootGrp.addVariable("alarm_flags_counter", DataType.USHORT, dimsSwath, "number_of_alarm_flags",
		 * "Number of alarm flags"); swathValues.add( new ValueD2U2( addVar, (buffer, i) -> { return
		 * ExtraDetections.getAlarmFlagsCounter(buffer.byteBufferData); }));
		 * 
		 * addVar = edGrp.addVariable("bytes_per_detection_counter", DataType.USHORT, dimsSwath,
		 * "bytes_per_detection_counter", "Number of bytes per detection (cycle 2)"); swathValues.add( new ValueD2U2(
		 * addVar, (buffer, i) -> { return ExtraDetections.getBytesPerDetectionCounter(buffer.byteBufferData); }));
		 * 
		 * 
		 * // Declare the Detection Classes (Nc) related data addVar = rxDCGrp.addVariable("start_depth",
		 * DataType.USHORT, dimsSwathDC, "start_depth", "Start depth (% of depth)", "percent");
		 * addVar.addAttribute("valid_min", 1); addVar.addAttribute("valid_max", 10); swathDetClassesValues.add( new
		 * ValueD2U2( addVar, (buffer, i) -> { return ExtraDetections.getStartDepth(buffer.byteBufferData, i); } ));
		 * 
		 * addVar = rxDCGrp.addVariable("stop_depth", DataType.USHORT, dimsSwathDC, "stop_depth",
		 * "stop depth (% of depth)", "percent"); addVar.addAttribute("valid_min", 0); addVar.addAttribute("valid_max",
		 * 300); swathDetClassesValues.add( new ValueD2U2( addVar, (buffer, i) -> { return
		 * ExtraDetections.getStopDepth(buffer.byteBufferData, i); } ));
		 * 
		 * addVar = rxDCGrp.addVariable("qf_threshold", DataType.USHORT, dimsSwathDC, "QF Threshold",
		 * "Quality Factor Threshold", "percent"); addVar.addAttribute("valid_min", 0.01);
		 * addVar.addAttribute("valid_max", 1); swathDetClassesValues.add( new ValueD2U2( addVar, (buffer, i) -> {
		 * return ExtraDetections.getQFThreshold(buffer.byteBufferData, i); } ));
		 * 
		 * addVar = rxDCGrp.addVariable("bs_threshold", DataType.SHORT, dimsSwathDC, "BS Threshold",
		 * "BackScatter Threshold", "db"); addVar.addAttribute("valid_min", -10); addVar.addAttribute("valid_max", -60);
		 * swathDetClassesValues.add( new ValueD2S2( addVar, (buffer, i) -> { return
		 * ExtraDetections.getBSThreshold(buffer.byteBufferData, i); } ));
		 * 
		 * addVar = rxDCGrp.addVariable("snr_threshold", DataType.USHORT, dimsSwathDC, "SNR Threshold",
		 * "Signal Noise Ratio Threshold", "db"); addVar.addAttribute("valid_min", 5); addVar.addAttribute("valid_max",
		 * 15); swathDetClassesValues.add( new ValueD2U2( addVar, (buffer, i) -> { return
		 * ExtraDetections.getSNRThreshold(buffer.byteBufferData, i); } ));
		 * 
		 * addVar = rxDCGrp.addVariable("alarm_threshold", DataType.USHORT, dimsSwathDC, "Alarm threshold",
		 * "Alarm threshold (number of extra det. required)"); addVar.addAttribute("valid_min", 1);
		 * addVar.addAttribute("valid_max", 99); swathDetClassesValues.add( new ValueD2U2( addVar, (buffer, i) -> {
		 * return ExtraDetections.getAlarmThreshold(buffer.byteBufferData, i); } ));
		 * 
		 * addVar = rxDCGrp.addVariable("extra_detections_index", DataType.USHORT, dimsSwathDC,
		 * "Extra detections Index", "Number of extra detections"); addVar.addAttribute("valid_min", 0);
		 * swathDetClassesValues.add( new ValueD2U2( addVar, (buffer, i) -> { return
		 * ExtraDetections.getExtraDetectionsIndex(buffer.byteBufferData, i); } ));
		 * 
		 * addVar = rxDCGrp.addVariable("show_class", DataType.UBYTE, dimsSwathDC, "show_class", "show_class");
		 * addVar.addAttribute("valid_min", 0); addVar.addAttribute("valid_max", 1); swathDetClassesValues.add( new
		 * ValueD2U1( addVar, (buffer, i) -> { return ExtraDetections.getShowClass(buffer.byteBufferData, i); } ));
		 * 
		 * addVar = rxDCGrp.addVariable("alarm_flag_1", DataType.UBYTE, dimsSwathDC, "Alarm Flag 1", "Alarm Flag 1");
		 * addVar.addAttribute("valid_min", 0); addVar.addAttribute("valid_max", 17); swathDetClassesValues.add( new
		 * ValueD2U1( addVar, (buffer, i) -> { return ExtraDetections.getAlarmFlag1(buffer.byteBufferData, i); } ));
		 * 
		 * // Create the Extra Detection (Nc) variables addVar = rxEDGrp.addVariable("depth", DataType.FLOAT,
		 * dimsSwathED, "depth", "Depth (z)", "meters"); addVar.addAttribute("CS", "SCS"); swathExtraDetValues.add( new
		 * ValueD2F4( addVar, (buffer, i) -> { return ExtraDetections.getDepth(buffer.byteBufferData, i); } ));
		 * 
		 * addVar = rxEDGrp.addVariable("across", DataType.FLOAT, dimsSwathED, "Across (y)", "Across (y)", "meters");
		 * addVar.addAttribute("CS", "SCS"); swathExtraDetValues.add( new ValueD2F4( addVar, (buffer, i) -> { return
		 * ExtraDetections.getAcross(buffer.byteBufferData, i); } ));
		 * 
		 * addVar = rxEDGrp.addVariable("along", DataType.FLOAT, dimsSwathED, "Along (x)", "Along (x)", "meters");
		 * addVar.addAttribute("CS", "SCS"); swathExtraDetValues.add( new ValueD2F4( addVar, (buffer, i) -> { return
		 * ExtraDetections.getAlong(buffer.byteBufferData, i); } ));
		 * 
		 * addVar = rxEDGrp.addVariable("delta_latitude", DataType.FLOAT, dimsSwathED, "delta latitude",
		 * "Delta Latitude", "degrees"); addVar.addAttribute("CS", "SCS"); swathExtraDetValues.add( new ValueD2F4(
		 * addVar, (buffer, i) -> { return ExtraDetections.getDeltaLatitude(buffer.byteBufferData, i); } ));
		 * 
		 * addVar = rxEDGrp.addVariable("delta_longitude", DataType.FLOAT, dimsSwathED, "delta longitude",
		 * "Delta Longitude", "degrees"); addVar.addAttribute("CS", "SCS"); swathExtraDetValues.add( new ValueD2F4(
		 * addVar, (buffer, i) -> { return ExtraDetections.getDeltaLongitude(buffer.byteBufferData, i); } ));
		 * 
		 * addVar = rxEDGrp.addVariable("beam_pointing_angle", DataType.FLOAT, dimsSwathED, "Beam Pointing Angle",
		 * "Beam Pointing angle", "degrees"); swathExtraDetValues.add( new ValueD2F4( addVar, (buffer, i) -> { return
		 * ExtraDetections.getBeamPointingAngle(buffer.byteBufferData, i); } ));
		 * 
		 * addVar = rxEDGrp.addVariable("applied_pointing_angle_correction", DataType.FLOAT, dimsSwathED,
		 * "Applied pointing angle correction", "Applied two way travel time corrections", "degrees");
		 * swathExtraDetValues.add( new ValueD2F4( addVar, (buffer, i) -> { return
		 * ExtraDetections.getAppliedPointingAngleCorrection(buffer.byteBufferData, i); } ));
		 * 
		 * 
		 * addVar = rxEDGrp.addVariable("two_time_travel_time", DataType.FLOAT, dimsSwathED, "Two time travel time",
		 * "Two time travel time", "secs"); swathExtraDetValues.add( new ValueD2F4( addVar, (buffer, i) -> { return
		 * ExtraDetections.getTwoTimeTravelTime(buffer.byteBufferData, i); } ));
		 * 
		 * 
		 * addVar = rxEDGrp.addVariable("applied_two_way_travel_time_corrections", DataType.FLOAT, dimsSwathED,
		 * "Applied two way travel time corrections", "Applied two way travel time corrections", "secs");
		 * swathExtraDetValues.add( new ValueD2F4( addVar, (buffer, i) -> { return
		 * ExtraDetections.getAppliedTwoWayTravelTimeCorrections(buffer.byteBufferData, i); } ));
		 * 
		 * addVar = rxEDGrp.addVariable("bs", DataType.SHORT, dimsSwathED, "Backscatter", "Backscatter", "db");
		 * addVar.addAttribute("scale_factor", 0.1); swathExtraDetValues.add( new ValueD2S2( addVar, (buffer, i) -> {
		 * return ExtraDetections.getBS(buffer.byteBufferData, i); } ));
		 * 
		 * addVar = rxEDGrp.addVariable("beam_incidence_angle_adjustement", DataType.BYTE, dimsSwathED,
		 * "Beam incidence angle adjustment", "Beam incidence angle adjustment (IBA)", "deg");
		 * addVar.addAttribute("scale_factor", 0.1); addVar.addAttribute("valid_min", -128);
		 * addVar.addAttribute("valid_max", 126); swathExtraDetValues.add( new ValueD2S1( addVar, (buffer, i) -> {
		 * return ExtraDetections.getBeamIncidenceAngleAdjustment(buffer.byteBufferData, i); } ));
		 * 
		 * addVar = rxEDGrp.addVariable("detection_info", DataType.UBYTE, dimsSwathED, "Detection info",
		 * "Detection info"); addVar.addAttribute("valid_min", 0); addVar.addAttribute("valid_max", 17);
		 * swathExtraDetValues.add( new ValueD2U1( addVar, (buffer, i) -> { return
		 * ExtraDetections.getDetectionInfo(buffer.byteBufferData, i); } ));
		 * 
		 * // spare
		 * 
		 * addVar = rxEDGrp.addVariable("tx_sector_number", DataType.USHORT, dimsSwathED, "TX sector number",
		 * "TX sector number/TX Array index"); swathExtraDetValues.add( new ValueD2U2( addVar, (buffer, i) -> { return
		 * ExtraDetections.getTXSectorIndex(buffer.byteBufferData, i); } ));
		 * 
		 * addVar = rxEDGrp.addVariable("detection_window_length", DataType.USHORT, dimsSwathED,
		 * "Detection window length", "Detection window length"); swathExtraDetValues.add( new ValueD2U2( addVar,
		 * (buffer, i) -> { return ExtraDetections.getDetectionWindowLength(buffer.byteBufferData, i); } ));
		 * 
		 * 
		 * addVar = rxEDGrp.addVariable("quality_factor", DataType.USHORT, dimsSwathED, "Quality Factor",
		 * "Quality factor (old)"); swathExtraDetValues.add( new ValueD2U2( addVar, (buffer, i) -> { return
		 * ExtraDetections.getQualityFactor(buffer.byteBufferData, i); } ));
		 * 
		 * addVar = rxEDGrp.addVariable("real_time_cleaning_info", DataType.USHORT, dimsSwathED,
		 * "Real time cleaning info", "Real time cleaning info"); swathExtraDetValues.add( new ValueD2U2( addVar,
		 * (buffer, i) -> { return ExtraDetections.getRealTimeCleaningInfo(buffer.byteBufferData, i); } ));
		 * 
		 * addVar = rxEDGrp.addVariable("range_factor", DataType.USHORT, dimsSwathED, "Range factor", "Range factor",
		 * "percent"); swathExtraDetValues.add( new ValueD2U2( addVar, (buffer, i) -> { return
		 * ExtraDetections.getRangeFactor(buffer.byteBufferData, i); } ));
		 * 
		 * addVar = rxEDGrp.addVariable("detection_class_index", DataType.USHORT, dimsSwathED, "detection class index",
		 * "Detection Class Number"); swathExtraDetValues.add( new ValueD2U2( addVar, (buffer, i) -> { return
		 * ExtraDetections.getDetectionClassIndex(buffer.byteBufferData, i); } )); addVar =
		 * rxEDGrp.addVariable("confidence_level", DataType.USHORT, dimsSwathED, "confidence level",
		 * "Confidence level"); swathExtraDetValues.add( new ValueD2U2( addVar, (buffer, i) -> { return
		 * ExtraDetections.getConfidenceLevel(buffer.byteBufferData, i); } )); addVar =
		 * rxEDGrp.addVariable("ifr_quality_factor", DataType.USHORT, dimsSwathED, "ifremer Quality factor",
		 * "QF * 10 (Ifremer Quality factor)"); addVar.addAttribute("scale_factor", 10); swathExtraDetValues.add( new
		 * ValueD2U2( addVar, (buffer, i) -> { return ExtraDetections.getQFIfr(buffer.byteBufferData, i); } ));
		 * 
		 * addVar = rxEDGrp.addVariable("water_column_beam_index", DataType.USHORT, dimsSwathED,
		 * "Water column beam index", "Water column beam number"); swathExtraDetValues.add( new ValueD2U2( addVar,
		 * (buffer, i) -> { return ExtraDetections.getWaterColumnBeamIndex(buffer.byteBufferData, i); } )); addVar =
		 * rxEDGrp.addVariable("beam_angle_across_re_vertical", DataType.FLOAT, dimsSwathED, "Beam angle across",
		 * "Beam angle across re vertical", "degrees"); swathExtraDetValues.add( new ValueD2F4( addVar, (buffer, i) -> {
		 * return ExtraDetections.getBeamAngleAcrossReVertical(buffer.byteBufferData, i); } ));
		 * 
		 * addVar = rxEDGrp.addVariable("detected_range", DataType.USHORT, dimsSwathED, "Detected range",
		 * "Detected range in (WCsr) samples"); swathExtraDetValues.add( new ValueD2U2( addVar, (buffer, i) -> { return
		 * ExtraDetections.getDetectedRange(buffer.byteBufferData, i); } ));
		 * 
		 * addVar = rxEDGrp.addVariable("raw_amplitude_samples_count", DataType.USHORT, dimsSwathED,
		 * "Raw amplitude samples count", "Raw amplitude samples (Ns)"); swathExtraDetValues.add( new ValueD2U2( addVar,
		 * (buffer, i) -> { return ExtraDetections.getDetectedRange(buffer.byteBufferData, i); } ));
		 * 
		 * variableSampleOffset = rxEDGrp.addVariable("raw_amplitude_sample_offsets", DataType.LONG, dimsSwathED,
		 * "sample_offset", "Sample Offset for amplitude");
		 * 
		 * // Samples - Boucles sur les Ns x Nd variableSamples = rxEDGrp.addVariable("raw_sample_amplitude",
		 * DataType.SHORT, dims1DSamples, "raw_sample_amplitude", "Sample Amplitude", "db");
		 * variableSamples.addAttribute("scale_factor", 0.1); variableSamples.addAttribute("linked_offset",
		 * "/sounder/extra_detections/rx_extra_detections/raw_amplitude_sample_offset");
		 * variableSamples.addAttribute("linked_count",
		 * "/sounder/extra_detections/rx_extra_detections/raw_amplitude_sample_count");
		 * 
		 */

	}

	@Override
	public void fillData(AllFile allFile) throws IOException, NCException {
		/*
		 * long origin[] = { 0, 0 }; ObjectBufferPool<DatagramBuffer> pool = new ObjectBufferPool<>(() -> new
		 * DatagramBuffer(stats.getByteOrder())); long sampleOrigin[] = { 0 };
		 * 
		 * for (Entry<SwathIdentifier, ExtraDetectionsCompletePingMetadata> pingentry : metadata.getEntries()) { final
		 * ExtraDetectionsCompletePingMetadata ping = pingentry.getValue(); if
		 * (!ping.isComplete(stats.getOverallMetadata().getSerialNumbers())) { continue; }
		 * 
		 * List<Pair<DatagramBuffer, ExtraDetectionsCompleteAntennaPingMetadata>> data = ping.read(pool); try {
		 * origin[0] = stats.getOverallMetadata().getGenerator().getIndex(pingentry.getKey()); } catch
		 * (IndexingException e) {
		 * DefaultLogger.get().error(String.format("Error decoding %s cannot retrieve storage index for ping %s",this.
		 * getClass().getSimpleName(), pingentry.getKey())); continue; } final int extraDetCount =
		 * ping.getExtradetectionsCount(); final int nDetClasses = ping.getDetectionClassesCount();
		 * 
		 * // create a link between rxbeam index and datagram/antenna for (Pair<DatagramBuffer,
		 * ExtraDetectionsCompleteAntennaPingMetadata> p : data) { DatagramBuffer buf = p.getFirst();
		 * 
		 * // handle the swath - rxAntenna data int rxIndex =
		 * stats.getInstallation().getRxAntennaIndex(ExtraDetections.getSerialNumber(buf.byteBufferData)); for (ValueD2
		 * v : swathValues) { v.fill(data.get(0).getFirst(), 0, rxIndex); }
		 * 
		 * // handle the swath - detectionClasses values for (int i = 0; i < p.getSecond().getDetectionClassesCount();
		 * i++) { for (ValueD2 v : swathDetClassesValues) { v.fill(buf, i, i); } }
		 * 
		 * // handle the swath - extraDetection values final int detectionAntennaOffset =
		 * ping.getDetectionOffset(ExtraDetections.getSerialNumber(buf.byteBufferData).getU());
		 * 
		 * int offset = ExtraDetections.getExtraDetectionsOffset(buf.byteBufferData); int rawSampleCounts[] = new
		 * int[p.getSecond().getDetectionCount()]; for (int i = 0; i < p.getSecond().getDetectionCount(); i++) { final
		 * int detectionIndex = detectionAntennaOffset + i; for (ValueD2 v : swathExtraDetValues) { v.fill(buf, offset,
		 * detectionIndex); } // N samples = 2 * N Raw amplitude Samples + 1 rawSampleCounts[i] =
		 * 2*(ExtraDetections.getRawAmplitudeSamplesCounter(buf.byteBufferData, offset).getU()) + 1; offset =
		 * ExtraDetections.getNextExtraDetOffset(buf.byteBufferData, offset); }
		 * 
		 * // handle the raw samples final long sampleAntennaOffset = sampleOrigin[0] +
		 * ping.getSampleOffset(ExtraDetections.getSerialNumber(buf.byteBufferData).getU()); long samplesOffsets[] = new
		 * long[p.getSecond().getDetectionCount()]; samplesOffsets[0] = sampleAntennaOffset; for (int i = 1; i <
		 * p.getSecond().getDetectionCount(); i++) { samplesOffsets[i] = samplesOffsets[i - 1] + rawSampleCounts[i]; }
		 * 
		 * 
		 * short[] rawSamples = ExtraDetections.getAllSamples(buf.byteBufferData); variableSamples.put(sampleOrigin, new
		 * long[] { p.getSecond().getRawSampleCount() }, rawSamples); variableSampleOffset.put(new long[] { origin[0],
		 * detectionAntennaOffset }, new long[] { 1, p.getSecond().getDetectionCount() }, samplesOffsets); }
		 * sampleOrigin[0] += ping.getSampleCount();
		 * 
		 * // Effectively write datasets for (ValueD2 v : swathValues) { v.write(origin, new long[] { 1,
		 * stats.getOverallMetadata().getSerialNumbers().size() }); } for (ValueD2 v : swathDetClassesValues) {
		 * v.write(origin, new long[] { 1, nDetClasses }); } for (ValueD2 v : swathExtraDetValues) { v.write(origin, new
		 * long[] { 1, extraDetCount }); }
		 * 
		 * // release the datagram buffers for the next ping for (Pair<DatagramBuffer,
		 * ExtraDetectionsCompleteAntennaPingMetadata> p : data) { pool.release(p.getFirst()); } }
		 */

	}

}
