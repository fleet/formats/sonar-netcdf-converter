package fr.ifremer.globe.api.xsf.converter.common.utils.values.d1;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCVariable;

/***
 * class allocating storage for 1D matrix, and giving a interface allowing to fill the data with attitude values
 */
public abstract class ValueD1WithAttitude {
	protected static final Logger logger = LoggerFactory.getLogger(ValueD1WithAttitude.class);

	public NCVariable variable;

	/***
	 * Constructor, allocate netcdf buffer for one variable and one ping
	 */
	public ValueD1WithAttitude(NCVariable variable) {
		this.variable = variable;
		// allocate storage
	}

	/**
	 * Call the lambda to fill in the internal buffer
	 */
	public abstract void fill(BaseDatagramBuffer buffer, double roll, double pitch, double heave);

	/**
	 * Write the internal buffer into the netCDF file.
	 * 
	 * @param dataFile
	 * @param origin
	 * @throws IOException
	 * @throws InvalidRangeException
	 */
	public abstract void write(long[] origin) throws NCException;

	/** Reset the netCDF variable to its FillValue */
	public abstract void clear();

}
