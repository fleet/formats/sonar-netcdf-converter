package fr.ifremer.globe.api.xsf.converter.s7k.datagram;

import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7K7000Metadata;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KFile;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KMultipingMetadata;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.api.xsf.util.types.UInt;
import fr.ifremer.globe.api.xsf.util.types.ULong;
import fr.ifremer.globe.api.xsf.util.types.UShort;

public class S7K7000 extends S7KMultipingDatagram {

	@Override
	public void computeSpecificMetadata(S7KFile metadata, DatagramBuffer buffer, long type,
			DatagramPosition position, long time, SwathIdentifier swathId) {
		S7KMultipingMetadata md = getSpecificMetadata(metadata).getOrCreateMetadataForDevice(getDeviceIdentifier(buffer).getU());		
		md.registerSwath(position, getPingNumber(buffer).getU(), getMultiPingSequence(buffer).getU(), 1, getOptionalDataOffset(buffer).getU() != 0);
	}

	@Override
	public S7K7000Metadata getSpecificMetadata(S7KFile metadata) {
		return metadata.get7000Metadata();
	}

	@Override
	public SwathIdentifier genSwathIdentifier(S7KFile stats, DatagramBuffer buffer, long time) {
		return stats.getSwathId(getPingNumber(buffer).getU(), getMultiPingSequence(buffer).getU(), time);
	}

	public static ULong getSonarSerialNumber(BaseDatagramBuffer buffer) {
		return TypeDecoder.read8U(buffer.byteBufferData, 0);
	}
	
	public static UInt getPingNumber(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4U(buffer.byteBufferData, 8);
	}
	
	public static UShort getMultiPingSequence(BaseDatagramBuffer buffer) {
		return TypeDecoder.read2U(buffer.byteBufferData, 12);
	}
	
	public static FFloat getFrequency(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 14);
	}
	
	public static FFloat getSampleRate(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 18);
	}
	
	public static FFloat getReceiverBandwidth(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 22);
	}
	
	public static FFloat getTxPulseWidth(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 26);
	}
	
	public static UInt getTxPulseTypeIdentifier(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4U(buffer.byteBufferData, 30);
	}
	
	public static UInt getTxPulseEnvelopeIdentifier(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4U(buffer.byteBufferData, 34);
	}
	
	public static FFloat getTxPulseEnvelopeParameter(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 38);
	}
	
	public static UInt getTxPulseReserved(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4U(buffer.byteBufferData, 42);
	}
	
	public static FFloat getMaxPingRate(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 46);
	}
	
	public static FFloat getPingPeriod(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 50);
	}
	
	public static FFloat getRangeSelection(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 54);
	}
	
	public static FFloat getPowerSelection(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 58);
	}
	
	public static FFloat getGainSelection(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 62);
	}
	
	public static UInt getControlFlag(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4U(buffer.byteBufferData, 66);
	}
	
	public static UInt getProjectorIdentifier(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4U(buffer.byteBufferData, 70);
	}
	
	public static FFloat getProjectorBeamSteeringAngleVertical(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 74);
	}
	
	public static FFloat getProjectorBeamSteeringAngleHorizontal(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 78);
	}
	
	public static FFloat getProjectorBeamMinus3dBBeamWidthVertical(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 82);
	}
	
	public static FFloat getProjectorBeamMinus3dBBeamWidthHorizontal(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 86);
	}
	
	public static FFloat getProjectorBeamFocalPoint(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 90);
	}
	
	public static UInt getProjectorBeamWeightingWindowType(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4U(buffer.byteBufferData, 94);
	}
	
	public static FFloat getProjectorBeamWeightingWindowParameter(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 98);
	}
	
	public static UInt getTransmitFlags(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4U(buffer.byteBufferData, 102);
	}
	
	public static UInt getHydrophoneIdentifier(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4U(buffer.byteBufferData, 106);
	}
	
	public static UInt getReceiveBeamWeightingWindow(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4U(buffer.byteBufferData, 110);
	}
	
	public static FFloat getReceiveBeamWeightingParameter(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 114);
	}
	
	public static UInt getReceiveFlags(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4U(buffer.byteBufferData, 118);
	}
	
	public static FFloat getReceiveBeamWidth(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 122);
	}
	
	public static FFloat getBottomDetectionFilterInfoMinRange(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 126);
	}
	
	public static FFloat getBottomDetectionFilterInfoMaxRange(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 130);
	}
	
	public static FFloat getBottomDetectionFilterInfoMinDepth(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 134);
	}
	
	public static FFloat getBottomDetectionFilterInfoMaxDepth(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 138);
	}
	
	public static FFloat getAbsorption(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 142);
	}
	
	public static FFloat getSoundVelocity(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 146);
	}
	
	public static FFloat getSpreading(BaseDatagramBuffer buffer) {
		return TypeDecoder.read4F(buffer.byteBufferData, 150);
	}

}
