package fr.ifremer.globe.api.xsf.converter.s7k.xsf;

import static java.lang.Math.toIntExact;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.sounder.ISounderFile;
import fr.ifremer.globe.api.xsf.converter.common.utils.Namer;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2F4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2U8;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.AttitudeSubGroup;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K1016;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KFile;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KIndividualSensorMetadata;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.api.xsf.util.types.ULong;
import fr.ifremer.globe.netcdf.ucar.NCException;

public class S7K1016Converter implements IDatagramConverter<S7KFile, XsfFromS7k> {

	private List<S7K1016IndividualConverter> subgroups;

	@Override
	public void declare(S7KFile s7kFile, XsfFromS7k xsf) throws NCException {
		this.subgroups = new LinkedList<>();
		for (Entry<Long, S7KIndividualSensorMetadata> entry : s7kFile.get1016Metadata().getEntries()) {
			S7K1016IndividualConverter subg = new S7K1016IndividualConverter(s7kFile, entry.getKey(), entry.getValue());
			subg.declare(s7kFile, xsf);
			subgroups.add(subg);
		}
	}

	@Override
	public void fillData(S7KFile s7kFile) throws IOException, NCException {
		for (S7K1016IndividualConverter subg : subgroups) {
			subg.fillData(s7kFile);
		}
	}

	private class S7K1016IndividualConverter implements IDatagramConverter<S7KFile, XsfFromS7k> {

		private long key;
		private S7KIndividualSensorMetadata md;
		private List<ValueD2> valuesD2;

		public S7K1016IndividualConverter(ISounderFile dataproxy, long key, S7KIndividualSensorMetadata md) {
			this.key = key;
			this.md = md;
			this.valuesD2 = new LinkedList<>();
		}

		@Override
		public void declare(S7KFile s7kFile, XsfFromS7k xsf) throws NCException {
			AttitudeSubGroup attitudeGrp = xsf.addAttitudeGrp(Namer.getAttitudeSubGroupName(this.key),
					toIntExact(this.md.getTotalNumberOfEntries()));

			final int maxNRecordPerDg = this.md.getMaxEntriesPerDatagram();
			valuesD2.add(new ValueD2U8(attitudeGrp.getTime(), maxNRecordPerDg,
					(buffer, i) -> new ULong(S7K1016.getRecordTimestamp(buffer, i))));
			valuesD2.add(new ValueD2F4(attitudeGrp.getRoll(), maxNRecordPerDg,
					(buffer, i) -> new FFloat((float) Math.toDegrees(S7K1016.getRoll(buffer, i).get()))));
			valuesD2.add(new ValueD2F4(attitudeGrp.getPitch(), maxNRecordPerDg,
					(buffer, i) -> new FFloat((float) Math.toDegrees(S7K1016.getPitch(buffer, i).get()))));
			valuesD2.add(new ValueD2F4(attitudeGrp.getVertical_offset(), maxNRecordPerDg,
					(buffer, i) -> new FFloat(S7K1016.getHeave(buffer, i).get() * -1.f)));
			valuesD2.add(new ValueD2F4(attitudeGrp.getHeading(), maxNRecordPerDg,
					(buffer, i) -> new FFloat((float) Math.toDegrees(S7K1016.getHeading(buffer, i).get()))));
		}

		@Override
		public void fillData(S7KFile s7kFile) throws IOException, NCException {
			DatagramBuffer buffer = new DatagramBuffer();
			long i = 0;
			for (Entry<Long, DatagramPosition> posentry : this.md.getEntries()) {
				valuesD2.forEach(ValueD2::clear);
				DatagramReader.readDatagram(posentry.getValue().getFile(), buffer, posentry.getValue().getSeek());

				final int numEntries = S7K1016.getNumberOfAttitudeData(buffer).getU();
				for (ValueD2 v : this.valuesD2) {
					for (int k = 0; k < numEntries; k++) {
						v.fill(buffer, k, k);
					}
				}
				for (ValueD2 v : this.valuesD2) {
					v.write(new long[] { i }, new long[] { numEntries });
				}
				i += numEntries;
			}

		}

	}
}
