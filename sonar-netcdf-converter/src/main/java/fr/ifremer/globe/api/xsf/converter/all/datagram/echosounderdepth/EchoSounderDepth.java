package fr.ifremer.globe.api.xsf.converter.all.datagram.echosounderdepth;

import java.nio.ByteBuffer;

import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagram;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.SInt;
import fr.ifremer.globe.api.xsf.util.types.UByte;
import fr.ifremer.globe.api.xsf.util.types.UInt;
import fr.ifremer.globe.api.xsf.util.types.UShort;

/**
 * Stateless Clock datagram
 */
public class EchoSounderDepth extends BaseDatagram {

	@Override
	public EchoSounderDepthMetadata getSpecificMetadata(AllFile metadata) {
		return metadata.getEchoSounder();
	}

	@Override
	protected void computeSpecificMetadata(AllFile metadata, ByteBuffer datagram, byte datagramType,
			long epochTime, DatagramPosition position) {
	    
	}
	
	public static UShort getCounter(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 12);
	}

	public static UShort getSerialNumber(ByteBuffer datagram) {
		return TypeDecoder.read2U(datagram, 14);
	}

	public static UInt getDate(ByteBuffer datagram) {
		return TypeDecoder.read4U(datagram, 16);
	}

	public static UInt getTime(ByteBuffer datagram) {
		return TypeDecoder.read4U(datagram, 20);
	}

	public static SInt getEchoSounderDepth(ByteBuffer datagram) {
		return TypeDecoder.read4S(datagram, 24);
	}

	public static UByte getSourceIdentifier(ByteBuffer datagram) {
		return TypeDecoder.read1U(datagram, 28);
	}

	public static void print(ByteBuffer datagram) {
		System.out.println(getCounter(datagram));
		System.out.println(getSerialNumber(datagram));

	}

}
