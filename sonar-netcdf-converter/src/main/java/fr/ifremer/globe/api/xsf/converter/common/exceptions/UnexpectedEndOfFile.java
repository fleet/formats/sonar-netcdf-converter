package fr.ifremer.globe.api.xsf.converter.common.exceptions;

import java.io.IOException;

@SuppressWarnings("serial")
/**
 * Exception thrown when we reach an unexpected EOF
 */
public class UnexpectedEndOfFile extends IOException {
	public UnexpectedEndOfFile()
	{
		super("Unexpected End of file encountered, possible invalid length field, truncated datagram or file corruption");
	}
}
