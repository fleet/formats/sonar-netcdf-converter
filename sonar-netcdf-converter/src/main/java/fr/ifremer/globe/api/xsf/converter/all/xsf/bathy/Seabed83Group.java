package fr.ifremer.globe.api.xsf.converter.all.xsf.bathy;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.all.datagram.seabed83.Seabed83;
import fr.ifremer.globe.api.xsf.converter.all.datagram.seabed83.Seabed83CompleteAntennaPingMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.seabed83.Seabed83CompletePingMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.seabed83.Seabed83Metadata;
import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2F4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2S4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2U2;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfFromKongsberg;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.BathymetryGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.BathymetryVendorSpecificGrp;
import fr.ifremer.globe.api.xsf.util.DefaultLogger;
import fr.ifremer.globe.api.xsf.util.ObjectBufferPool;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCVariable;
import fr.ifremer.globe.netcdf.util.Pair;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

public class Seabed83Group implements IDatagramConverter<AllFile, XsfFromKongsberg> {

    private AllFile stats;
    private Seabed83Metadata metadata;

    private List<ValueD2> swathAntennaValues;
    private List<ValueD2> swathBeamValues;
    private NCVariable samples;

    private BathymetryGrp bathyGrp;

    public Seabed83Group(AllFile md) {
        this.stats = md;
        this.metadata = md.getSeabed83();
        swathAntennaValues = new LinkedList<>();
        swathBeamValues = new LinkedList<>();
    }

    @Override
    public void declare(AllFile allFile, XsfFromKongsberg xsf) throws NCException {

        bathyGrp = xsf.getBathyGrp();
        BathymetryVendorSpecificGrp bathy_group_vendor = xsf.getBathy();

        swathAntennaValues.add(new ValueD2F4(bathyGrp.getDetection_mean_absorption_coefficient(), (buffer,
                                                                                                   a) -> new FFloat(Seabed83.getMeanAbsorptionCoefficient(buffer.byteBufferData).getU() * 0.01f)));

        swathAntennaValues.add(new ValueD2U2(bathy_group_vendor.getDetection_pulse_length(),
                (buffer, a) -> Seabed83.getPulseLength(buffer.byteBufferData)));

        swathAntennaValues.add(new ValueD2U2(bathy_group_vendor.getRange_to_normal_incidence(),
                (buffer, a) -> Seabed83.getRangeToNormalIncidence(buffer.byteBufferData)));

        swathAntennaValues.add(new ValueD2U2(bathy_group_vendor.getStart_range_normal_tvg_ramp(),
                (buffer, a) -> Seabed83.getStartRangeSampleTVGRamp(buffer.byteBufferData)));

        swathAntennaValues.add(new ValueD2U2(bathy_group_vendor.getStop_range_normal_tvg_ramp(),
                (buffer, a) -> Seabed83.getStopRangeSampleTVGRamp(buffer.byteBufferData)));

        swathAntennaValues.add(new ValueD2F4(bathy_group_vendor.getBackscatter_normal_incidence_level(),
                (buffer, a) -> new FFloat(Seabed83.getNormalIncidenceBS(buffer.byteBufferData).get())));

        swathAntennaValues.add(new ValueD2F4(bathy_group_vendor.getBackscatter_oblique_incidence_level(),
                (buffer, a) -> new FFloat(Seabed83.getObliqueBS(buffer.byteBufferData).get())));

        swathAntennaValues.add(new ValueD2F4(bathy_group_vendor.getTx_beam_width_along(),
                (buffer, a) -> new FFloat(Seabed83.getTxBeamWidth(buffer.byteBufferData).getU() * 0.1f)));

        swathAntennaValues.add(new ValueD2F4(bathy_group_vendor.getTvg_law_cross_over_angle(),
                (buffer, a) -> new FFloat(Seabed83.getTVGLawCrossOver(buffer.byteBufferData).getU() * 0.1f)));

        swathBeamValues.add(new ValueD2S4(bathyGrp.getSeabed_image_center(),
                (buffer, i) -> Seabed83.getCenterSample(buffer.byteBufferData, i)));
        samples = xsf.getBathyGrp().getSeabed_image_samples_r();
    }

    @Override
    public void fillData(AllFile allFile) throws IOException, NCException {
        // To be tested with real data (This datagram is used for EM 2000, EM 3000, EM
        // 3002, EM 1002, EM 300 and EM 120.)
        long[] origin2D = {0, 0};
        ObjectBufferPool<DatagramBuffer> pool = new ObjectBufferPool<>(() -> new DatagramBuffer(stats.getByteOrder()));
        int detectionCount = stats.getDetectionCount();
        int maxSampleCount = stats.getSeabed83().getMaxAbstractSampleCount();

        for (Entry<SwathIdentifier, Seabed83CompletePingMetadata> pingEntry : metadata.getPings()) {
            swathAntennaValues.forEach(ValueD2::clear);
            swathBeamValues.forEach(ValueD2::clear);
            final Seabed83CompletePingMetadata ping = pingEntry.getValue();
            if (!ping.isComplete(stats.getGlobalMetadata().getSerialNumbers())
                    || !allFile.getGlobalMetadata().getSwathIds().contains(pingEntry.getKey())) {
                continue;
            }

            List<Pair<DatagramBuffer, Seabed83CompleteAntennaPingMetadata>> data = DatagramReader.read(pool,
                    ping.locate());

            // get origin from swath identifier (ignore if matching index not found)
            var swathId = pingEntry.getKey();
            var optSwathIndex = stats.getGlobalMetadata().getSwathIds().getIndex(swathId);
            if (optSwathIndex.isEmpty()) {
                DefaultLogger.get().warn(
                        "Datagram Seabed83 associated with a missing ping is ignored (number = {}, sequence = {}).",
                        swathId.getRawPingNumber(), swathId.getSwathPosition());
                continue;
            }
            origin2D[0] = optSwathIndex.get();
            short[] sampleBuffer = new short[maxSampleCount * detectionCount];
            Arrays.fill(sampleBuffer, samples.getShortFillValue());

            // iterate over the antenna
            for (Pair<DatagramBuffer, Seabed83CompleteAntennaPingMetadata> antenna : data) {
                DatagramBuffer buffer = antenna.getFirst();

                for (ValueD2 v : swathAntennaValues) {
                    v.fill(buffer, 0,
                            stats.getInstallation().getRxAntennaIndex(Seabed83.getSerialNumber(buffer.byteBufferData)));
                }

                // fill all the data based on their ping number
                final int nBeam = Seabed83.getReceivedBeamsCounter(buffer.byteBufferData).getU();
                final int beamOffset = ping.getBeamOffset(Seabed83.getSerialNumber(buffer.byteBufferData).getU());

                // copy all the samples for this antenna at once
                int[] currOffsets = new int[antenna.getSecond().getBeamCount()];
                int[] currCounts = new int[antenna.getSecond().getBeamCount()];
                byte[] currSamples = Seabed83.getAllSamples(buffer.byteBufferData, currOffsets, currCounts);

                int sampleLocalOffset = 0;
                // for each actually received beam
                for (int i = 0; i < nBeam; i++) {
                    final int antennaBeamIdx = Seabed83.getBeamIndex(buffer.byteBufferData, i).getU();
                    // compute the global beam number across all the antennas
                    final int globalBeamNumber = beamOffset + antennaBeamIdx;
                    int globalBeamOffset = globalBeamNumber * maxSampleCount;
                    // adjust the number of samples for the current beam
                    int beamSampleCount = currCounts[antennaBeamIdx];
                    if (beamSampleCount > 0) {
                        for (int b = 0; b < beamSampleCount; b++) {
                            // convert from 0.5dB byte to 0.1dB short
                            sampleBuffer[globalBeamOffset + b] = (short) (currSamples[b + sampleLocalOffset] * 5);
                        }
                        sampleLocalOffset += beamSampleCount;
                        for (ValueD2 value : swathBeamValues) {
                            value.fill(buffer, i, globalBeamNumber);
                        }
                    }
                }
            }
            // fill samples
            if (maxSampleCount > 0)
                samples.put(new long[]{origin2D[0], 0, 0}, new long[]{1, detectionCount, maxSampleCount},
                        sampleBuffer);

            // fill the data
            for (ValueD2 v : swathAntennaValues) {
                v.write(origin2D, new long[]{1, stats.getGlobalMetadata().getSerialNumbers().size()});
            }
            for (ValueD2 v : swathBeamValues) {
                v.write(origin2D, new long[]{1, ping.getBeamCount()});
            }

            for (Pair<DatagramBuffer, Seabed83CompleteAntennaPingMetadata> antenna : data) {
                pool.release(antenna.getFirst());
            }
        }
    }

}
