package fr.ifremer.globe.api.xsf.converter.all.datagram;

import java.io.IOException;
import java.nio.ByteBuffer;

import fr.ifremer.globe.api.xsf.converter.all.datagram.attitude.Attitude;
import fr.ifremer.globe.api.xsf.converter.all.datagram.clock.Clock;
import fr.ifremer.globe.api.xsf.converter.all.datagram.depth68.Depth68;
import fr.ifremer.globe.api.xsf.converter.all.datagram.echosounderdepth.EchoSounderDepth;
import fr.ifremer.globe.api.xsf.converter.all.datagram.extradetections.ExtraDetections;
import fr.ifremer.globe.api.xsf.converter.all.datagram.extraparameters.ExtraParameters;
import fr.ifremer.globe.api.xsf.converter.all.datagram.heading.Heading;
import fr.ifremer.globe.api.xsf.converter.all.datagram.height.Height;
import fr.ifremer.globe.api.xsf.converter.all.datagram.installation.Installation;
import fr.ifremer.globe.api.xsf.converter.all.datagram.mechanicaltransducertilt.MechanicalTransducerTilt;
import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.all.datagram.networkattitudevelocity.NetworkAttitudeVelocity;
import fr.ifremer.globe.api.xsf.converter.all.datagram.position.Position;
import fr.ifremer.globe.api.xsf.converter.all.datagram.qualityfactor.QualityFactor;
import fr.ifremer.globe.api.xsf.converter.all.datagram.rawrange102.RawRange102;
import fr.ifremer.globe.api.xsf.converter.all.datagram.rawrange70.RawRange70;
import fr.ifremer.globe.api.xsf.converter.all.datagram.rawrange78.RawRange78;
import fr.ifremer.globe.api.xsf.converter.all.datagram.runtime.RunTimeDg;
import fr.ifremer.globe.api.xsf.converter.all.datagram.seabed83.Seabed83;
import fr.ifremer.globe.api.xsf.converter.all.datagram.seabed89.Seabed89;
import fr.ifremer.globe.api.xsf.converter.all.datagram.soundspeedprofile.SoundSpeedProfile;
import fr.ifremer.globe.api.xsf.converter.all.datagram.stavedata.StaveData;
import fr.ifremer.globe.api.xsf.converter.all.datagram.surfacesoundspeed.SurfaceSoundSpeed;
import fr.ifremer.globe.api.xsf.converter.all.datagram.tide.Tide;
import fr.ifremer.globe.api.xsf.converter.all.datagram.unknown.Unknown;
import fr.ifremer.globe.api.xsf.converter.all.datagram.wc.WC;
import fr.ifremer.globe.api.xsf.converter.all.datagram.xyz88.XYZ88Depth;
import fr.ifremer.globe.api.xsf.converter.common.datagram.Datagram;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.util.FastIntMap;

public class DatagramParser {
	FastIntMap<Datagram> map = new FastIntMap<>(new Unknown());

	public DatagramParser() {
		this(true); // by default the water column is parsed
	}

	public DatagramParser(boolean parseWaterColumn) {
		map.put(0x44, new Depth68()); // Depth68 (44h, 68d)
		map.put(0x58, new XYZ88Depth()); // XYZ88 (58h, 88d)

		if (parseWaterColumn)
			map.put(0x6B, new WC()); // Water column datagram (6Bh, 107d)

		map.put(0x4e, new RawRange78()); // Raw Range (4eh, 78d)
		map.put(0x46, new RawRange70()); // Raw Range (4fh, 78d)
		map.put(0x66, new RawRange102()); // Raw Range (66h, 78d)
		map.put(0x43, new Clock()); // Clock (43h, 67d)
		map.put(0x4f, new QualityFactor()); // QualityFactor79 (79d)
		map.put(0x6C, new ExtraDetections()); // ExtraDetections (108)
		map.put(0x53, new Seabed83()); // Seabed (53h, 83)
		map.put(0x59, new Seabed89()); // Seabed (59h, 89)
		map.put(0x6D, new StaveData());
		map.put(0x41, new Attitude()); // Attitude (41h, 65)
		map.put(0x6e, new NetworkAttitudeVelocity()); // NetworkAttitudeVelocity (6eh, 110)
		map.put(0x50, new Position()); // Position (50h, 80d)
		map.put(0x54, new Tide()); // Tide (54h, 84d)
		map.put(0x48, new Heading()); // Heading (72d)
		map.put(0x45, new EchoSounderDepth()); // EchoSounder (69d)
		map.put(0x64, new Height()); // EchoSounder (68h, 104d)

		map.put(0x33, new ExtraParameters()); // ExtraParameters (51d)
		map.put(0x4A, new MechanicalTransducerTilt()); // MechanicalTransducerTilt (74d)
		map.put(0x52, new RunTimeDg()); // RunTime (66d)
		map.put(0x49, new Installation()); // Installation (Start, 73d)
		map.put(0x68, new Height());
		map.put(0x69, new Installation()); // Installation (Stop, 105d)
		map.put(0x70, new Installation()); // Installation (remote, 111d)

		map.put(0x47, new SurfaceSoundSpeed()); // SoundSpeed (71d)
		map.put(0x55, new SoundSpeedProfile()); // SurfaceSoundSpeed (85d)
	}

	public byte getDatagramType(ByteBuffer buffer) {
		return buffer.get(1);
	}

	public void computeMetaData(AllFile stats, ByteBuffer byteBufferDg, DatagramPosition location) throws IOException {
		// parse header
		byte datagramType = getDatagramType(byteBufferDg);
		Datagram datagram = map.get(datagramType);

		datagram.computeMetaData(stats, byteBufferDg, datagramType, location);
	}
}
