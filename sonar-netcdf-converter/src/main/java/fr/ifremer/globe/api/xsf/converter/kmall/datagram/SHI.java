package fr.ifremer.globe.api.xsf.converter.kmall.datagram;

import java.nio.ByteBuffer;

import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.KmallFile;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.SHIMetadata;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.api.xsf.util.types.UShort;

public class SHI extends SCommon {

    @Override
    public void computeSpecificMetadata(KmallFile metadata, ByteBuffer datagram, String datagramType,
            DatagramPosition position) {
        getSpecificMetadata(metadata).addDatagram(position);        
    }
    
    @Override
    public SHIMetadata getSpecificMetadata(KmallFile metadata) {
        return metadata.getSHIMetadata();
    }
    
    // SHIDataFromSensor
    public static UShort getSensorType(ByteBuffer buffer) {
        return TypeDecoder.read2U(buffer, 24);
	}
	public static FFloat getHeigthUsed(ByteBuffer buffer) {
	        return TypeDecoder.read4F(buffer, 26);
	}	
 
    public static String getDataFromSensor(ByteBuffer datagram) {
        final long fullDgSize = TypeDecoder.read4U(datagram, datagram.limit() - 4).getU();
        final int dataFromSensorOffset = 30;
        final int rawDataLen = (int) (fullDgSize - 8 - dataFromSensorOffset);
        
		final int pos = datagram.position();
        byte[] ret = new byte[rawDataLen];
        datagram.position(dataFromSensorOffset);
        datagram.get(ret, 0, rawDataLen);
        datagram.position(pos);
        
        return new String(ret);
    }
}
