package fr.ifremer.globe.api.xsf.converter.all.datagram.rawrange102;

import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.AbstractCompletePingMetadata;

public class RawRange102CompletePingMetadata extends AbstractCompletePingMetadata<RawRange102CompleteAntennaPingMetadata> {

    public RawRange102CompletePingMetadata(int ntx, int[] serialNumbers) {
        super(ntx, serialNumbers);
    }
    
}
