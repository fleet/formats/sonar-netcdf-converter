package fr.ifremer.globe.api.xsf.converter.s7k.metadata;

import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;

public class S7KSwathMetadata {

	private int numBeams;
	private boolean hasOptionalData;
	private DatagramPosition position;
	
	public S7KSwathMetadata(DatagramPosition position, int numBeams, boolean hasOptionalData) {
		this.position = position;
		this.numBeams = numBeams;
		this.hasOptionalData = hasOptionalData;
	}

	public int getNumBeams() {
		return numBeams;
	}

	public DatagramPosition getPosition() {
		return position;
	}
	
	public boolean hasOptionalData() {
		return this.hasOptionalData;
	}
}
