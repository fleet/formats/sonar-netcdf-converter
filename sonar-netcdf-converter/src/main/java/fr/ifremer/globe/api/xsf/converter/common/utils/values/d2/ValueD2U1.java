package fr.ifremer.globe.api.xsf.converter.common.utils.values.d2;

import java.util.Arrays;

import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.api.xsf.util.types.UByte;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCVariable;

/***
 * class allocating storage for 2D matrix, and giving a interface allowing to fill the data *
 */
public class ValueD2U1 extends ValueD2 {
	protected byte[] dataOut;
	private ValueProvider valueProvider;
	private UByte fillValue;

	/***
	 * Constructor, allocate netcdf buffer for one variable and one ping
	 */
	public ValueD2U1(NCVariable variable, ValueProvider filler) {
		super(variable);
		// allocate storage
		this.dataOut = new byte[dim];
		this.fillValue=new UByte(variable.getByteFillValue());

		clear();
		this.valueProvider = filler;
	}
	
	@Override
	public void clear() {
		Arrays.fill(this.dataOut, fillValue.getByte());
	}

	public ValueD2U1(NCVariable variable, int dim, ValueProvider filler) {
		super(variable, dim);
		// allocate storage
		this.dataOut = new byte[dim];
		this.valueProvider = filler;
		this.fillValue=new UByte(variable.getByteFillValue());
	}

	public interface ValueProvider {
		public UByte get(BaseDatagramBuffer buffer, int beamIndexSource);
	}

	public ValueD2U1(NCVariable variable, ValueProvider filler, UByte fillValue) {
		this(variable, filler);
		this.fillValue=fillValue;
		setDefault(fillValue);
	}

	public void setDefault(UByte dft) {
		for (int i = 0; i < dataOut.length; i++) {
			dataOut[i] = dft.getByte();
		}
	}

	/**
	 * call the lambda to retrieve the value and set it in the allocated netcdf buffer dataOut
	 */
	@Override
	public void fill(BaseDatagramBuffer buffer, int beamIndexSource, int beamIndexDest) {
		try{
			dataOut[beamIndexDest] = valueProvider.get(buffer, beamIndexSource).getByte();
		} catch (Exception e) {
			logger.error(String.format("Error while writing variable %s (%s)", variable.getName(), e.getMessage()));
			throw e;
		}
	}

	@Override
	public void write(long[] origin, long[] count) throws NCException {
		try {
			variable.putu(origin, count, dataOut);
		} catch (Exception e) {
			logger.error(String.format("Error while writing variable %s (%s)", variable.getName(), e.getMessage()));
			throw e;
		}
	}
}
