package fr.ifremer.globe.api.xsf.converter.all.xsf.sensors;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import fr.ifremer.globe.api.xsf.converter.all.datagram.IndividualSensorMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfFromKongsberg;
import fr.ifremer.globe.netcdf.ucar.NCException;

public class TideGroup implements IDatagramConverter<AllFile, XsfFromKongsberg> {

	private List<ValueD1> values;
	private IndividualSensorMetadata sensorMetadata;
	long nEntries;

	private AllFile metadata;

	public TideGroup(AllFile metadata) {
		this.metadata = metadata;
		this.sensorMetadata = metadata.getTide();
		this.nEntries = sensorMetadata.getTotalNumberOfEntries();
		this.values = new LinkedList<>();
	}

	@Override
	public void declare(AllFile allFile, XsfFromKongsberg xsf) throws NCException {

		// This is not testable without any .all dataset with tide datagram, and should be located in tide indicative.
		
		/*
		 * //really specific to .all file VendorSpecificGrp
		 * v=xsf.getEnvironmentVendor(); if(nEntries==0) return; NCGroup tideGrp =
		 * v.addGroup("tide");
		 * 
		 * NCDimension dim = tideGrp.addDimension("tide_cnt", this.nEntries);
		 * List<NCDimension> dims = Arrays.asList(new NCDimension[] { dim });
		 * 
		 * // declare all the variables NCVariable addVar;
		 * 
		 * addVar = tideGrp.addVariable("system_serial_number", DataType.USHORT, dims,
		 * "system_serial_number", "System Serial Number"); values.add(new
		 * ValueD1U2(addVar, (buffer) -> { return new
		 * UShort(Tide.getSerialNumber(buffer.byteBufferData).get()); }));
		 * 
		 * addVar = tideGrp.addVariable("time", DataType.UINT, dims, "time", "time",
		 * "secs since 1970/01/01 00:00:00"); addVar.addAttribute("scale_factor",
		 * 0.001); addVar.addAttribute("valid_min", 0); addVar.addAttribute("valid_max",
		 * 86399999); values.add(new ValueD1U4(addVar, (buffer) -> { return new
		 * UInt((int) Tide.getEpochTime(buffer.byteBufferData)); }));
		 * 
		 * addVar = tideGrp.addVariable("tide_raw_count", DataType.USHORT, dims);
		 * addVar.addAttribute("valid_min", 0); addVar.addAttribute("valid_max", 65535);
		 * values.add(new ValueD1U2(addVar, (buffer) -> { return
		 * Tide.getCounter(buffer.byteBufferData); }));
		 * 
		 * addVar = tideGrp.addVariable("date_from_input", DataType.UINT, dims,
		 * "date_from_input",
		 * "Date from input datagram (year*10000 + month*100 + day)"); values.add(new
		 * ValueD1U4(addVar, (buffer) -> { return
		 * Tide.getDateFromInput(buffer.byteBufferData); }));
		 * 
		 * addVar = tideGrp.addVariable("time_from_input", DataType.UINT, dims,
		 * "time_from_input", "Time since midnight in milliseconds", "secs");
		 * addVar.addAttribute("scale_factor", 0.001); addVar.addAttribute("valid_min",
		 * 0); addVar.addAttribute("valid_max", 86399999); values.add(new
		 * ValueD1U4(addVar, (buffer) -> { return
		 * Tide.getTimeFromInput(buffer.byteBufferData); }));
		 * 
		 * addVar = tideGrp.addVariable("tidal_offset", DataType.INT, dims,
		 * "tide_offset", "Tide offset", "meters"); addVar.addAttribute("CS", "FCS");
		 * addVar.addAttribute("scale_factor", 0.01); addVar.addAttribute("valid_min",
		 * -32768); addVar.addAttribute("valid_max", 32766); addVar.addAttribute("CS",
		 * "FCS"); values.add(new ValueD1S2(addVar, (buffer) -> { return
		 * Tide.getTidalOffset(buffer.byteBufferData); }));
		 */
	}

	@Override
	public void fillData(AllFile allFile) throws IOException, NCException {
//		long[] origin = new long[] { 0 };
//
//		DatagramBuffer buffer = new DatagramBuffer(metadata.getByteOrder());
//		for (Entry<SimpleIdentifier, DatagramPosition> posentry : sensorMetadata.getDatagrams()) {
//			final DatagramPosition pos = posentry.getValue();
//			DatagramReader.readDatagram(pos.getFile(), buffer, pos.getSeek());
//
//			// read the data from the source file
//			// for (int i = 0; i < nEntries; i++) {
//			for (ValueD1 value : this.values) {
//				value.fill(buffer);
//			}
//			// }
//
//			// flush into the output file
//			for (ValueD1 value : this.values) {
//				value.write(origin);
//			}
//			origin[0] += 1;
//		}
	}

}
