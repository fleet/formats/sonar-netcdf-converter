package fr.ifremer.globe.api.xsf.converter.common.utils.values.d2;

import java.util.Arrays;

import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.api.xsf.util.types.SByte;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCVariable;

/***
 * class allocating storage for 2D matrix, and giving a interface allowing to fill the data
 */
public class ValueD2S1WithHScale extends ValueD2WithHScale {
	protected byte[] dataOut;
	private ValueProvider valueProvider;

	/***
	 * Constructor, allocate netcdf buffer for one variable and one ping
	 */
	public ValueD2S1WithHScale(NCVariable variable, ValueProvider filler) {
		super(variable);
		// allocate storage
		this.dataOut = new byte[dim];
		this.valueProvider = filler;
	}
	
	@Override
	public void clear() {
		Arrays.fill(this.dataOut, variable.getByteFillValue());
	}

	public interface ValueProvider {
		public SByte get(BaseDatagramBuffer buffer, int beamIndexSource, double hScale);
	}

	/**
	 * call the lambda to retrieve the value and set it in the allocated netcdf buffer dataOut
	 */
	@Override
	public void fill(BaseDatagramBuffer buffer, int beamIndexSource, int beamIndexDest, double hScale) {
		try {
			dataOut[beamIndexDest] = valueProvider.get(buffer, beamIndexSource, hScale).get();
		} catch (Exception e) {
			logger.error(String.format("Error while writing variable %s (%s)", variable.getName(), e.getMessage()));
			throw e;
		}
	}

	@Override
	public void write(long[] origin, long[] count) throws NCException {
		try {
			variable.put(origin, count, dataOut);
		} catch (Exception e) {
			logger.error(String.format("Error while writing variable %s (%s)", variable.getName(), e.getMessage()));
			throw e;
		}
	}
	public void directFill(int i, byte c) {
		dataOut[i] = c;
	}
}
