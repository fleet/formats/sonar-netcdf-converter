package fr.ifremer.globe.api.xsf.converter.kmall.datagram;

import java.nio.ByteBuffer;

import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.KmallFile;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.UnknownDatagramMetadata;

public class Unknown extends KmAllBaseDatagram {

    @Override
    public void computeSpecificMetadata(KmallFile metadata, ByteBuffer datagram, String datagramType,
            DatagramPosition position) {
        getSpecificMetadata(metadata).pushUnknownType(datagramType);
    }

    @Override
    public UnknownDatagramMetadata getSpecificMetadata(KmallFile metadata) {
        // TODO Auto-generated method stub
        return metadata.getUnknown();
    }

}
