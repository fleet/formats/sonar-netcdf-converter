package fr.ifremer.globe.api.xsf.converter.kmall.metadata;


import fr.ifremer.globe.api.xsf.converter.all.datagram.IndividualSensorMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.SensorMetadata;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.SKM;
import fr.ifremer.globe.api.xsf.util.SimpleIdentifier;
import fr.ifremer.globe.api.xsf.util.types.UByte;
import fr.ifremer.globe.utils.date.DateUtils;

import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

public class SKMMetadata extends SensorMetadata<UByte, IndividualSensorMetadata> {

    public float headingMin,
            headingMax,
            rollMin,
            rollMax,
            pitchMin,
            pitchMax,
            heaveMin,
            heaveMax;
    private float headingSum,
            rollSum,
            pitchSum,
            heaveSum;
    private long headingCount,
            pitchCount,
            rollCount,
            heaveCount;

    public SKMMetadata() {
        super(new IndividualSensorMetadata());

        headingCount = 0;
        headingMin = Float.MAX_VALUE;
        headingMax = -Float.MAX_VALUE;
        rollCount = 0;
        rollMin = Float.MAX_VALUE;
        rollMax = -Float.MAX_VALUE;
        pitchCount = 0;
        pitchMin = Float.MAX_VALUE;
        pitchMax = -Float.MAX_VALUE;
        heaveCount = 0;
        heaveMin = Float.MAX_VALUE;
        heaveMax = -Float.MAX_VALUE;
    }

    public void addHeading(float heading) {
        this.headingSum += heading;
        this.headingCount++;
        headingMin = Float.min(heading, headingMin);
        headingMax = Float.max(heading, headingMax);
    }

    public float getHeadingMean() {
        return this.headingSum / this.headingCount;
    }

    public float getHeadingMin() {
        return headingMin;
    }

    public float getHeadingMax() {
        return headingMax;
    }

    public void addRoll(float roll) {
        this.rollSum += roll;
        this.rollCount++;
        rollMin = Float.min(roll, rollMin);
        rollMax = Float.max(roll, rollMax);
    }

    public float getRollMean() {
        return this.rollSum / this.rollCount;
    }

    public float getRollMin() {
        return rollMin;
    }

    public float getRollMax() {
        return rollMax;
    }

    public void addPitch(float pitch) {
        this.pitchSum += pitch;
        this.pitchCount++;
        pitchMin = Float.min(pitch, pitchMin);
        pitchMax = Float.max(pitch, pitchMax);
    }

    public float getPitchMean() {
        return this.pitchSum / this.pitchCount;
    }

    public float getPitchMin() {
        return pitchMin;
    }

    public float getPitchMax() {
        return pitchMax;
    }

    public void addHeave(float heave) {
        this.heaveSum += heave;
        this.heaveCount++;
        heaveMin = Float.min(heave, heaveMin);
        heaveMax = Float.max(heave, heaveMax);
    }

    public float getHeaveMean() {
        return this.heaveSum / this.heaveCount;
    }

    public float getHeaveMin() {
        return heaveMin;
    }

    public float getHeaveMax() {
        return heaveMax;
    }

    /**
     * Reads and sorts attitude values from each sensor
     */
    public TreeMap<Long, float[]> getSortedActiveAttitudeDatagrams() throws IOException {
        TreeMap<Long, float[]> attitudeValues = new TreeMap<>();

        for (IndividualSensorMetadata sensorMetadata : getSensors().values()) {
            for (Map.Entry<SimpleIdentifier, DatagramPosition> entry : sensorMetadata.getDatagrams()) {
                final DatagramPosition attDatagram = entry.getValue();

                DatagramBuffer buffer = new DatagramBuffer();
                DatagramReader.readDatagram(attDatagram.getFile(), buffer, attDatagram.getSeek());

                boolean active = SKM.isActive(buffer.byteBufferData);
                if (!active) {
                    continue;
                }

                int valueCount = SKM.getNumSamplesArray(buffer.byteBufferData).getU();
                for (int i = 0; i < valueCount; i++) {
                    final long time = DateUtils.secondToNano(SKM.getTimeFromKMBinary_sec(buffer.byteBufferData, i).getU())
                            + SKM.getTimeFromKMBinary_nanosec(buffer.byteBufferData, i).getU();

                    attitudeValues.put(time, new float[]{
                            SKM.getRoll_deg(buffer.byteBufferData, i).get(),
                            SKM.getPitch_deg(buffer.byteBufferData, i).get(),
                            SKM.getHeave_m(buffer.byteBufferData, i).get(),
                            SKM.getHeading_deg(buffer.byteBufferData, i).get()
                    });
                }
            }
        }
        return attitudeValues;
    }
}
