package fr.ifremer.globe.api.xsf.converter.s7k.mbg;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgBase;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgWriter;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KFile;

public class S7KMbgWriter extends MbgWriter<S7KFile> {

	@Override
	public List<IDatagramConverter<S7KFile, MbgBase>> getGroupConverters(S7KFile s7kFile) throws IOException {
		List<IDatagramConverter<S7KFile, MbgBase>> dataConverters = new ArrayList<>();

		// sonar settings
		if (s7kFile.get7000Metadata().datagramCount > 0)
			dataConverters.add(new S7K7000Converter());

		// depth
		if (s7kFile.get7006Metadata().datagramCount > 0) {
			// deprecated s7k format not handled (2006 version with a mix of several sonar datagram in same file)
			if (s7kFile.get7006Metadata().getEntries().size() != 1)
				throw new IOException("This converter only supports one sonar");
			dataConverters.add(new S7K7006Converter());
		}

		if (s7kFile.get7027Metadata().datagramCount > 0) {
			// deprecated s7k format not handled (2006 version with a mix of several sonar datagram in same file)
			if (s7kFile.get7027Metadata().getEntries().size() != 1)
				throw new IOException("This converter only supports one sonar");
			dataConverters.add(new S7K7027Converter());
		}

		return dataConverters;
	}
}
