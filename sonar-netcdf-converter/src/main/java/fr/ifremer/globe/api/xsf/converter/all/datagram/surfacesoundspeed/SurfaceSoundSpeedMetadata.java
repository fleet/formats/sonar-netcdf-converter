package fr.ifremer.globe.api.xsf.converter.all.datagram.surfacesoundspeed;

import fr.ifremer.globe.api.xsf.converter.all.datagram.IndividualSensorMetadata;
import fr.ifremer.globe.api.xsf.util.types.UShort;

/**
 * */
public class SurfaceSoundSpeedMetadata extends IndividualSensorMetadata{
	
	private UShort serialNumber;
	private float sssSum;
	private long sssNumber;
	public float sssMin, sssMax;
	
	public SurfaceSoundSpeedMetadata() {
		sssNumber 		= 0;
		sssMin 		= 999999f;
		sssMax 		= -99999f;
	}

	public void setSerialNumber(UShort serialNumber) {
		this.serialNumber = serialNumber;
	}
	
	public UShort getSerialNumber() {
		return serialNumber;
	}	

	// Methodes pour les statistiques de Summary.
	public void addSurfaceSoundSpeed(float sssValue) {
		this.sssSum += sssValue*0.1;
		this.sssNumber++;
	}
	
	public float getSurfaceSoundSpeedMean() {
		return this.sssSum / this.sssNumber;
	}
	
	public void computeMinMaxSSS(float sssValue) {
     	sssMin = Float.min((float)(sssValue*0.1), (float) sssMin);
    	sssMax = Float.max((float)(sssValue*0.1), (float) sssMax);		
	}
	
    public float getSurfaceSoundSpeedMin()
    {
    	return sssMin;
    }

    public float getSurfaceSoundSpeedMax()
    {
    	return sssMax;
    }	
}