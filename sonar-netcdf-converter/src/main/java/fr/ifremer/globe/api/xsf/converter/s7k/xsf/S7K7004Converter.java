package fr.ifremer.globe.api.xsf.converter.s7k.xsf;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U4;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d1.ValueD1U8;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2F4;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.BeamGroup1VendorSpecificGrp;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K7004;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KFile;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KIndividualSensorMetadata;
import fr.ifremer.globe.api.xsf.util.types.ULong;
import fr.ifremer.globe.netcdf.ucar.NCException;

public class S7K7004Converter implements IDatagramConverter<S7KFile, XsfFromS7k> {

	// The data from this datagram are usually handeled by datagrams that needs it.
	// This converter only converts the raw data (usually one datagram per ping)

	private List<ValueD1> valuesD1;
	private List<ValueD2> valuesD2;

	@Override
	public void declare(S7KFile s7kFile, XsfFromS7k xsf) throws NCException {
		this.valuesD1 = new LinkedList<ValueD1>();
		this.valuesD2 = new LinkedList<ValueD2>();
		BeamGroup1VendorSpecificGrp beamVendor = xsf.getBeamVendor();

		valuesD1.add(new ValueD1U8(beamVendor.getRaw_beamgeometry_time(),
				(buffer) -> new ULong(S7K7004.getTimestampNano(buffer))));
		valuesD1.add(new ValueD1U4(beamVendor.getRaw_beamgeometry_numBeams(),
				(buffer) -> S7K7004.getNumberOfReceivedBeams(buffer)));

		valuesD2.add(new ValueD2F4(beamVendor.getRaw_beamgeometry_horizontal_direction_angle(),
				(buffer, i) -> S7K7004.getBeamHorizontalDirection(buffer, i)));
		valuesD2.add(new ValueD2F4(beamVendor.getRaw_beamgeometry_vertical_direction_angle(),
				(buffer, i) -> S7K7004.getBeamVerticalDirection(buffer, i)));
		valuesD2.add(new ValueD2F4(beamVendor.getRaw_beamgeometry_minus3dB_width_x(),
				(buffer, i) -> S7K7004.getMinus3dBBeamWidthX(buffer, i)));
		valuesD2.add(new ValueD2F4(beamVendor.getRaw_beamgeometry_minus3dB_width_y(),
				(buffer, i) -> S7K7004.getMinus3dBBeamWidthY(buffer, i)));
	}

	@Override
	public void fillData(S7KFile s7kFile) throws IOException, NCException {
		DatagramBuffer buffer = new DatagramBuffer();
		long i = 0;

		S7KIndividualSensorMetadata smd = s7kFile.get7004Metadata().getEntries().iterator().next().getValue();
		for (Entry<Long, DatagramPosition> posentry : smd.getEntries()) {
			for(var valueD1 : valuesD1)
				valueD1.clear();
			valuesD2.forEach(ValueD2::clear);
			DatagramReader.readDatagram(posentry.getValue().getFile(), buffer, posentry.getValue().getSeek());

			for (ValueD1 v : this.valuesD1) {
				v.fill(buffer);
				v.write(new long[] { i });
			}

			final int nBeams = Math.toIntExact(S7K7004.getNumberOfReceivedBeams(buffer).getU());
			for (ValueD2 v : this.valuesD2) {
				for (int b = 0; b < nBeams; b++) {
					v.fill(buffer, b, b);
				}
				v.write(new long[] { i, 0 }, new long[] { 1, nBeams });
			}

			i++;
		}
	}
}
