package fr.ifremer.globe.api.xsf.converter.all.mbg.runtime;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.all.datagram.PingDatagram;
import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.all.datagram.rawrange78.RawRange78;
import fr.ifremer.globe.api.xsf.converter.all.datagram.rawrange78.RawRange78CompleteAntennaPingMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.rawrange78.RawRange78CompletePingMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.runtime.RunTimeDg;
import fr.ifremer.globe.api.xsf.converter.common.IDatagramConverter;
import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagram;
import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.AbstractBeamDatagramMetadata;
import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.AbstractCompleteAntennaPingSingleMetadata;
import fr.ifremer.globe.api.xsf.converter.common.datagram.metadata.AbstractCompletePingMetadata;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgBase;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgConstants;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2S1;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2S2;
import fr.ifremer.globe.api.xsf.converter.common.utils.values.d2.ValueD2U2;
import fr.ifremer.globe.api.xsf.util.DefaultLogger;
import fr.ifremer.globe.api.xsf.util.ObjectBufferPool;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.TypeDecoder;
import fr.ifremer.globe.api.xsf.util.types.SByte;
import fr.ifremer.globe.api.xsf.util.types.SShort;
import fr.ifremer.globe.api.xsf.util.types.UShort;
import fr.ifremer.globe.netcdf.api.convention.CFStandardNames;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCVariable;
import fr.ifremer.globe.netcdf.util.Pair;

/**
 * Converter for Runtime datagram
 */
public class RuntimeDatagramConverter implements IDatagramConverter<AllFile, MbgBase> {

	private List<ValueD2> antennaValues = new ArrayList<>();
	private MbgBase mbg;

	public enum FrequencyPlan {
		NO_FREQUENCY_PLAN(0), FREQUENCY_PLAN_1(1), FREQUENCY_PLAN_2(2);

		private int value;

		FrequencyPlan(int value) {
			this.value = value;
		}

		int getValue() {
			return value;
		}
	}

	@Override
	public void declare(AllFile allFile, MbgBase mbg) throws NCException {
		this.mbg = mbg;

		NCVariable absoprtionVariable = mbg.getVariable(MbgConstants.AbsorptionCoefficient);
		int absorptionFactor = absoprtionVariable.getAttributeInt(CFStandardNames.CF_SCALE_FACTOR);
		int absorpOffset = absoprtionVariable.getAttributeInt(CFStandardNames.CF_ADD_OFFSET);
		antennaValues.add(new ValueD2U2(absoprtionVariable,
				(buffer, i) -> new UShort(
						(short) ((RunTimeDg.getAbsorptionCoefficient(buffer.byteBufferData).getU() - absorpOffset)
								/ absorptionFactor))));

		NCVariable transmitBandVariable = mbg.getVariable(MbgConstants.TransmitBeamwidth);
		int transmitFactor = transmitBandVariable.getAttributeInt(CFStandardNames.CF_SCALE_FACTOR);
		int transmitOffset = transmitBandVariable.getAttributeInt(CFStandardNames.CF_ADD_OFFSET);
		// FIXME : explain why is not divided by 10 & ValueD2U2 (see constructor doc)
		antennaValues.add(new ValueD2S2(transmitBandVariable, (buffer, i) -> {
			return new SShort(
					(short) ((Math.round(RunTimeDg.getTransmitBeamwidth(buffer.byteBufferData).getU() /* / 10f */)
							- transmitOffset) / transmitFactor));
		}));

		NCVariable transmitPowerMaxVariable = mbg.getVariable(MbgConstants.TransmitPowerReMax);
		int transmitPowerFactor = transmitPowerMaxVariable.getAttributeInt(CFStandardNames.CF_SCALE_FACTOR);
		int transmitPowerOffset = transmitPowerMaxVariable.getAttributeInt(CFStandardNames.CF_ADD_OFFSET);
		antennaValues.add(new ValueD2S1(transmitPowerMaxVariable,
				(buffer, i) -> new SByte(
						(byte) ((RunTimeDg.getTransmitPowerReMaximum(buffer.byteBufferData).get() - transmitPowerOffset)
								/ transmitPowerFactor))));

		NCVariable transmitPulseVariable = mbg.getVariable(MbgConstants.TransmitPulseLength);
		int transmitPulseFactor = transmitPulseVariable.getAttributeInt(CFStandardNames.CF_SCALE_FACTOR);
		int transmitPulseOffset = transmitPulseVariable.getAttributeInt(CFStandardNames.CF_ADD_OFFSET);
		antennaValues.add(new ValueD2U2(transmitPulseVariable,
				(buffer, i) -> new UShort(
						(short) ((RunTimeDg.getTransmitPulseLength(buffer.byteBufferData).getU() - transmitPulseOffset)
								/ transmitPulseFactor))));

		NCVariable velocitySourceVariable = mbg.getVariable(MbgConstants.TransVelocitySource);
		int velocitySourceFactor = velocitySourceVariable.getAttributeInt(CFStandardNames.CF_SCALE_FACTOR);
		int velocitySourceOffset = velocitySourceVariable.getAttributeInt(CFStandardNames.CF_ADD_OFFSET);
		antennaValues.add(new ValueD2S1(velocitySourceVariable, (buffer, i) -> new SByte(
				(byte) ((RunTimeDg.getSourceSoundSpeedTransducer(buffer.byteBufferData).getU() - velocitySourceOffset)
						/ velocitySourceFactor))));

		NCVariable tvgVariable = mbg.getVariable(MbgConstants.TVGLawCrossoverAngle);
		int tvgFactor = tvgVariable.getAttributeInt(CFStandardNames.CF_SCALE_FACTOR);
		int tvgOffset = tvgVariable.getAttributeInt(CFStandardNames.CF_ADD_OFFSET);
		antennaValues.add(new ValueD2S1(tvgVariable, (buffer, i) -> new SByte(
				(byte) ((RunTimeDg.getTVGLawCrossoverAngle(buffer.byteBufferData).getU() - tvgOffset) / tvgFactor))));

		NCVariable yawPitchStabMode = mbg.getVariable(MbgConstants.YawPitchStabMode);
		int yawPitchStabModeFactor = yawPitchStabMode.getAttributeInt(CFStandardNames.CF_SCALE_FACTOR);
		int yawPitchStabModeOffset = yawPitchStabMode.getAttributeInt(CFStandardNames.CF_ADD_OFFSET);
		antennaValues.add(new ValueD2S1(yawPitchStabMode,
				(buffer, i) -> new SByte((byte) ((RunTimeDg.getYawAndPitchStabilizationMode(buffer.byteBufferData).getU()
						- yawPitchStabModeOffset) / yawPitchStabModeFactor))));

		NCVariable beamSpacingVariable = mbg.getVariable(MbgConstants.BeamSpacing);
		int beamSpacingFactor = beamSpacingVariable.getAttributeInt(CFStandardNames.CF_SCALE_FACTOR);
		int beamSpacingOffset = beamSpacingVariable.getAttributeInt(CFStandardNames.CF_ADD_OFFSET);
		antennaValues.add(new ValueD2S1(beamSpacingVariable,
				(buffer, i) -> new SByte(
						(byte) ((RunTimeDg.getBeamSpacing(buffer.byteBufferData).getU() - beamSpacingOffset)
								/ beamSpacingFactor))));

		NCVariable BSPStatusVariable = mbg.getVariable(MbgConstants.BSPStatus);
		int BSPStatusFactor = BSPStatusVariable.getAttributeInt(CFStandardNames.CF_SCALE_FACTOR);
		int BSPStatusOffset = BSPStatusVariable.getAttributeInt(CFStandardNames.CF_ADD_OFFSET);
		antennaValues.add(new ValueD2S1(BSPStatusVariable, (buffer, i) -> new SByte(
				(byte) ((RunTimeDg.getBSPStatus(buffer.byteBufferData).getU() - BSPStatusOffset) / BSPStatusFactor))));

		NCVariable filterIdentifierVariable = mbg.getVariable(MbgConstants.FilterIdentifier);
		int filterIdentifierFactor = filterIdentifierVariable.getAttributeInt(CFStandardNames.CF_SCALE_FACTOR);
		int filterIdentifierOffset = filterIdentifierVariable.getAttributeInt(CFStandardNames.CF_ADD_OFFSET);
		antennaValues.add(new ValueD2S1(filterIdentifierVariable,
				(buffer, i) -> new SByte(
						(byte) ((RunTimeDg.getFilterIdentifier(buffer.byteBufferData).getU() - filterIdentifierOffset)
								/ filterIdentifierFactor))));

		NCVariable paramMaxDepthVariable = mbg.getVariable(MbgConstants.ParamMaximumDepth);
		int paramMaxDepthFactor = paramMaxDepthVariable.getAttributeInt(CFStandardNames.CF_SCALE_FACTOR);
		int paramMaxDepthOffset = paramMaxDepthVariable.getAttributeInt(CFStandardNames.CF_ADD_OFFSET);
		antennaValues.add(new ValueD2S2(paramMaxDepthVariable,
				(buffer, i) -> new SShort(
						(short) ((RunTimeDg.getMaximumDepth(buffer.byteBufferData).getU() - paramMaxDepthOffset)
								/ paramMaxDepthFactor))));

		NCVariable paramMinDepthVariable = mbg.getVariable(MbgConstants.ParamMinimumDepth);
		int paramMinDepthFactor = paramMinDepthVariable.getAttributeInt(CFStandardNames.CF_SCALE_FACTOR);
		int paramMinDepthOffset = paramMinDepthVariable.getAttributeInt(CFStandardNames.CF_ADD_OFFSET);
		antennaValues.add(new ValueD2S2(paramMinDepthVariable,
				(buffer, i) -> new SShort(
						(short) ((RunTimeDg.getMinimumDepth(buffer.byteBufferData).getU() - paramMinDepthOffset)
								/ paramMinDepthFactor))));

		NCVariable processingUnitVariable = mbg.getVariable(MbgConstants.ProcessingUnitStatus);
		int processingUnitFactor = processingUnitVariable.getAttributeInt(CFStandardNames.CF_SCALE_FACTOR);
		int processingUnitOffset = processingUnitVariable.getAttributeInt(CFStandardNames.CF_ADD_OFFSET);
		antennaValues.add(new ValueD2S1(processingUnitVariable,
				(buffer, i) -> new SByte(
						(byte) ((RunTimeDg.getProcessingUnitStatus(buffer.byteBufferData).getU() - processingUnitOffset)
								/ processingUnitFactor))));

		NCVariable receiveBeamWidthVariable = mbg.getVariable(MbgConstants.ReceiveBeamwidth);
		int receiveBeamWidthFactor = receiveBeamWidthVariable.getAttributeInt(CFStandardNames.CF_SCALE_FACTOR);
		int receiveBeamWidthOffset = receiveBeamWidthVariable.getAttributeInt(CFStandardNames.CF_ADD_OFFSET);
		antennaValues.add(new ValueD2S1(receiveBeamWidthVariable,
				(buffer, i) -> new SByte(
						(byte) ((RunTimeDg.getReceiveBeamwidth(buffer.byteBufferData).getU() - receiveBeamWidthOffset)
								/ receiveBeamWidthFactor))));

		NCVariable receiveBandWidthVariable = mbg.getVariable(MbgConstants.ReceiveBandwidth);
		int receiveBandWidthFactor = receiveBandWidthVariable.getAttributeInt(CFStandardNames.CF_SCALE_FACTOR);
		int receiveBandWidthOffset = receiveBandWidthVariable.getAttributeInt(CFStandardNames.CF_ADD_OFFSET);
		antennaValues.add(new ValueD2S1(receiveBandWidthVariable,
				(buffer, i) -> new SByte(
						(byte) ((RunTimeDg.getReceiveBandwidth(buffer.byteBufferData).getU() - receiveBandWidthOffset)
								/ receiveBandWidthFactor))));

		NCVariable maxPortWidthVariable = mbg.getVariable(MbgConstants.MaxPortWidth);
		int maxPortWidthFactor = maxPortWidthVariable.getAttributeInt(CFStandardNames.CF_SCALE_FACTOR);
		int maxPortWidthOffset = maxPortWidthVariable.getAttributeInt(CFStandardNames.CF_ADD_OFFSET);
		antennaValues.add(new ValueD2S2(maxPortWidthVariable,
				(buffer, i) -> new SShort(
						(short) ((RunTimeDg.getMaximumPortSwathWidth(buffer.byteBufferData).getU() - maxPortWidthOffset)
								/ maxPortWidthFactor))));

		NCVariable maxPortCovVariable = mbg.getVariable(MbgConstants.MaxPortCoverage);
		int maxPortCovFactor = maxPortCovVariable.getAttributeInt(CFStandardNames.CF_SCALE_FACTOR);
		int maxPortCovOffset = maxPortCovVariable.getAttributeInt(CFStandardNames.CF_ADD_OFFSET);
		antennaValues.add(new ValueD2S1(maxPortCovVariable,
				(buffer, i) -> new SByte(
						(byte) ((RunTimeDg.getMaximumPortCoverage(buffer.byteBufferData).getU() - maxPortCovOffset)
								/ maxPortCovFactor))));

		NCVariable maxStarboardVariable = mbg.getVariable(MbgConstants.MaxStarboardCoverage);
		int maxStarboardFactor = maxStarboardVariable.getAttributeInt(CFStandardNames.CF_SCALE_FACTOR);
		int maxStarboardOffset = maxStarboardVariable.getAttributeInt(CFStandardNames.CF_ADD_OFFSET);
		antennaValues.add(new ValueD2S1(maxStarboardVariable, (buffer, i) -> new SByte(
				(byte) ((RunTimeDg.getMaximumStarboardCoverage(buffer.byteBufferData).getU() - maxStarboardOffset)
						/ maxStarboardFactor))));

		NCVariable maxStarboardWidthVariable = mbg.getVariable(MbgConstants.MaxStarboardWidth);
		int maxStarboardWidthFactor = maxStarboardWidthVariable.getAttributeInt(CFStandardNames.CF_SCALE_FACTOR);
		int maxStarboardWidthOffset = maxStarboardWidthVariable.getAttributeInt(CFStandardNames.CF_ADD_OFFSET);
		antennaValues.add(new ValueD2S2(maxStarboardWidthVariable, (buffer, i) -> {
			return new SShort(
					(short) ((RunTimeDg.getMaximumStarboardSwath(buffer.byteBufferData).getU() - maxStarboardWidthOffset)
							/ maxStarboardWidthFactor));
		}));

		NCVariable opStationVariable = mbg.getVariable(MbgConstants.OperatorStationStatus);
		int opStationFactor = opStationVariable.getAttributeInt(CFStandardNames.CF_SCALE_FACTOR);
		int opStationOffset = opStationVariable.getAttributeInt(CFStandardNames.CF_ADD_OFFSET);
		antennaValues.add(new ValueD2S1(opStationVariable,
				(buffer, i) -> new SByte(
						(byte) ((RunTimeDg.getOperatorStationStatus(buffer.byteBufferData).getU() - opStationOffset)
								/ opStationFactor))));

		NCVariable hiLoAbspRatioVariable = mbg.getVariable(MbgConstants.HiLoAbsorptionRatio);
		int hiLoAbspRatioFactor = hiLoAbspRatioVariable.getAttributeInt(CFStandardNames.CF_SCALE_FACTOR);
		int hiLoAbspRatioOffset = hiLoAbspRatioVariable.getAttributeInt(CFStandardNames.CF_ADD_OFFSET);

		antennaValues.add(new ValueD2S1(hiLoAbspRatioVariable,
				(buffer, i) -> new SByte(
						(byte) ((TypeDecoder.read1U(buffer.byteBufferData, 48).getU() - hiLoAbspRatioOffset)
								/ hiLoAbspRatioFactor))));

		NCVariable sonarStatusVariable = mbg.getVariable(MbgConstants.SonarStatus);
		int sonarStatusFactor = sonarStatusVariable.getAttributeInt(CFStandardNames.CF_SCALE_FACTOR);
		int sonarStatusOffset = sonarStatusVariable.getAttributeInt(CFStandardNames.CF_ADD_OFFSET);
		antennaValues.add(new ValueD2S1(sonarStatusVariable, (buffer, i) -> new SByte(
				(byte) ((RunTimeDg.getSonarHeadOrTransceiverStatus(buffer.byteBufferData).getU() - sonarStatusOffset)
						/ sonarStatusFactor))));

		NCVariable receiverGainVariable = mbg.getVariable(MbgConstants.ReceiverFixedGain);
		int receiverGainFactor = receiverGainVariable.getAttributeInt(CFStandardNames.CF_SCALE_FACTOR);
		int receiverGainOffset = receiverGainVariable.getAttributeInt(CFStandardNames.CF_ADD_OFFSET);
		antennaValues.add(new ValueD2S1(receiverGainVariable,
				(buffer, i) -> new SByte(
						(byte) ((TypeDecoder.read1U(buffer.byteBufferData, 35).getU() - receiverGainOffset)
								/ receiverGainFactor))));

		/** Sounder mode **/
		NCVariable sounderModeVariable = mbg.getVariable(MbgConstants.SounderMode);
		antennaValues.add(new ValueD2S1(sounderModeVariable,
				(buffer, i) -> new SByte((byte) (RunTimeDg.getMode(buffer.byteBufferData).getU() & 0x3F))));

		NCVariable durotongVariable = mbg.getVariable(MbgConstants.DurotongSpeed);
		int durotongFactor = durotongVariable.getAttributeInt(CFStandardNames.CF_SCALE_FACTOR);
		int durotongOffset = durotongVariable.getAttributeInt(CFStandardNames.CF_ADD_OFFSET);

		if (allFile.getGlobalMetadata().getModelNumber() == 1002) {
			antennaValues.add(new ValueD2U2(durotongVariable,
					(buffer, i) -> new UShort(
							(short) ((RunTimeDg.getOperatorStationStatus(buffer.byteBufferData).getU() - durotongOffset)
									/ durotongFactor))));

		} else {
			antennaValues.add(new ValueD2U2(durotongVariable, (buffer, i) -> new UShort((short) 0)));
		}

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void fillData(AllFile allFile) throws IOException, NCException {
		long origin[] = { 0, 0 };
		float[] freqs = getCenterFreq(allFile);

		TreeMap<Long, BaseDatagramBuffer> runtimeDatagrams = allFile.getRunTime().getSortedDatagrams();

		ObjectBufferPool<DatagramBuffer> pool = new ObjectBufferPool<>(
				() -> new DatagramBuffer(allFile.getByteOrder()));

		// get suitable depth datagrams
		AbstractBeamDatagramMetadata<AbstractCompletePingMetadata<AbstractCompleteAntennaPingSingleMetadata>> metadata = null;
		if (allFile.getDepth68().datagramCount > 0) {
			metadata = (AbstractBeamDatagramMetadata) allFile.getDepth68();
		}
		if (allFile.getXyzdepth().datagramCount > 0) {
			metadata = (AbstractBeamDatagramMetadata) allFile.getXyzdepth();
		}

		for (Entry<SwathIdentifier, AbstractCompletePingMetadata<AbstractCompleteAntennaPingSingleMetadata>> pingEntry : metadata
				.getPings()) {
			final AbstractCompletePingMetadata<AbstractCompleteAntennaPingSingleMetadata> ping = pingEntry.getValue();
			List<Pair<DatagramBuffer, AbstractCompleteAntennaPingSingleMetadata>> data = DatagramReader.read(pool,
					ping.locate());
			// test ping rejection
			if (!allFile.getGlobalMetadata().getSwathIds().contains(pingEntry.getKey())) {
				continue;
			}

			// get origin from swath identifier (ignore if matching index not found)
			var swathId = pingEntry.getKey();
			var optIndex = allFile.getGlobalMetadata().getSwathIds().getIndex(swathId);
			if (optIndex.isEmpty()) {
				DefaultLogger.get().warn(
						"Datagram Runtime associated with a missing ping is ignored (number = {}, sequence = {}).",
						swathId.getRawPingNumber(), swathId.getSwathPosition());
				continue;
			}
			origin[0] = optIndex.get();

			for (Pair<DatagramBuffer, AbstractCompleteAntennaPingSingleMetadata> antenna : data) {
				DatagramBuffer buffer = antenna.getFirst();

				long currentTime = BaseDatagram.getEpochTime(buffer.byteBufferData);
				Long t = runtimeDatagrams.navigableKeySet().first();
				BaseDatagramBuffer runtimeDg = runtimeDatagrams.get(t);

				for (Long time : runtimeDatagrams.descendingKeySet()) {
					if (time <= currentTime) {
						runtimeDg = runtimeDatagrams.get(time);
						break;
					}
				}

				int antennaIdx = allFile.getInstallation()
						.getRxAntennaIndex(PingDatagram.getSerialNumber(buffer.byteBufferData));

				if (origin[0] < allFile.getGlobalMetadata().getSwathCount() - 1) {
					writeFrequencyPlan(allFile, runtimeDg, (int) origin[0], freqs[(int) origin[0]],
							freqs[(int) origin[0] + 1]);
				} else {
					// last swath : compare with previous frequency
					writeFrequencyPlan(allFile, runtimeDg, (int) origin[0], freqs[(int) origin[0]],
							freqs[(int) origin[0] - 1]);
				}

				for (ValueD2 antennaValue : antennaValues) {
					antennaValue.fill(runtimeDg, 0, antennaIdx);
				}
			}

			// Write in netcdf variable
			for (ValueD2 antennaValue : antennaValues) {
				antennaValue.write(origin, new long[] { 1, allFile.getGlobalMetadata().getSerialNumbers().size() });
			}

			// release the datagram buffers for the next ping
			for (Pair<DatagramBuffer, AbstractCompleteAntennaPingSingleMetadata> p : data) {
				pool.release(p.getFirst());
			}
		}

	}

	private float[] getCenterFreq(AllFile allFile) throws IOException {
		float res[] = new float[allFile.getGlobalMetadata().getSwathCount()];

		ObjectBufferPool<DatagramBuffer> pool = new ObjectBufferPool<>(
				() -> new DatagramBuffer(allFile.getByteOrder()));

		for (Entry<SwathIdentifier, RawRange78CompletePingMetadata> pingEntry : allFile.getRawRange78().getPings()) {
			final RawRange78CompletePingMetadata ping = pingEntry.getValue();
			if (!ping.isComplete(allFile.getGlobalMetadata().getSerialNumbers())
					|| !allFile.getGlobalMetadata().getSwathIds().contains(pingEntry.getKey())) {
				continue;
			}

			List<Pair<DatagramBuffer, RawRange78CompleteAntennaPingMetadata>> data = DatagramReader.read(pool,
					ping.locate());
			int index;

			// get origin from swath identifier (ignore if matching index not found)
			var swathId = pingEntry.getKey();
			var optIndex = allFile.getGlobalMetadata().getSwathIds().getIndex(swathId);
			if (optIndex.isEmpty()) {
				DefaultLogger.get().warn(
						"Datagram Runtime associated with a missing ping is ignored (number = {}, sequence = {}).",
						swathId.getRawPingNumber(), swathId.getSwathPosition());
				continue;
			}
			index = optIndex.get();

			res[index] = RawRange78.getTxCenterFrequency(data.get(0).getFirst().byteBufferData, 0).get();
		}

		return res;
	}

	/**
	 * Compute the frequency plan of an antenna
	 * 
	 * @param buffer
	 * @param index
	 * @param freq
	 * @param nextFreq
	 * @throws NCException
	 */
	private void writeFrequencyPlan(AllFile allFile, BaseDatagramBuffer buffer, int index, float freq, float nextFreq)
			throws NCException {
		NCVariable frequencyVariable = mbg.getVariable(MbgConstants.Frequency);

		byte mode = RunTimeDg.getMode(buffer.byteBufferData).getByte();
		byte dualMode = (byte) ((mode & 0xC0) >>> 6);

		if (dualMode == 0) {
			byte b = (byte) FrequencyPlan.NO_FREQUENCY_PLAN.getValue();
			frequencyVariable.put(new long[] { index, 0 },
					new long[] { 1, allFile.getGlobalMetadata().getSerialNumbers().size() }, new byte[] { b, b });
		} else if (freq < nextFreq) {
			byte b = (byte) FrequencyPlan.FREQUENCY_PLAN_1.getValue();
			frequencyVariable.put(new long[] { index, 0 },
					new long[] { 1, allFile.getGlobalMetadata().getSerialNumbers().size() }, new byte[] { b, b });
		} else {
			byte b = (byte) FrequencyPlan.FREQUENCY_PLAN_2.getValue();
			frequencyVariable.put(new long[] { index, 0 },
					new long[] { 1, allFile.getGlobalMetadata().getSerialNumbers().size() }, new byte[] { b, b });
		}
	}

}
