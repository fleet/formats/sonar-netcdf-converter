package fr.ifremer.globe.api.xsf.converter.common.utils.values.d2;

import java.util.Arrays;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagramBuffer;
import fr.ifremer.globe.api.xsf.util.types.FFloat;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCVariable;

/***
 * class allocating storage for 2D matrix, and giving a interface allowing to fill the data *
 */
public class ValueD2F4Composite extends ValueD2Composite {
	protected static final Logger logger = LoggerFactory.getLogger(ValueD2Composite.class);
	protected float[] dataOut;
	private ValueProvider valueProvider;

	/***
	 * Constructor, allocate netcdf buffer for one variable and one ping
	 */
	public ValueD2F4Composite(NCVariable variable, ValueProvider filler) {
		super(variable);
		// allocate storage
		this.dataOut = new float[dim];
		this.valueProvider = filler;
	}

	public interface ValueProvider {
		public FFloat get(BaseDatagramBuffer buffer, int beamIndexSource, Optional<BaseDatagramBuffer> other);
	}

	/**
	 * call the lambda to retrieve the value and set it in the allocated netcdf buffer dataOut
	 */
	@Override
	public void fill(BaseDatagramBuffer buffer, int beamIndexSource, Optional<BaseDatagramBuffer> other, int beamIndexDest) {
		dataOut[beamIndexDest] = valueProvider.get(buffer, beamIndexSource, other).get();
	}

	@Override
	public void write(long[] origin, long[] count) throws NCException {
		try {
			variable.put(origin, count, dataOut);
		} catch (Exception e) {
			logger.error(String.format("Error while writing variable %s (%s)", variable.getName(), e.getMessage()));
			throw e;
		}
	}

	/** {@inheritDoc} */
	@Override
	public void clear() {
		Arrays.fill(this.dataOut, variable.getFloatFillValue());
	}

}
