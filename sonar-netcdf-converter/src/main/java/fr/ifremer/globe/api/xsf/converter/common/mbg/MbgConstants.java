/**
 * 
 */
package fr.ifremer.globe.api.xsf.converter.common.mbg;

import java.util.Arrays;
import java.util.List;

import fr.ifremer.globe.netcdf.api.convention.CFStandardNames;

/**
 * All constants for MBG files
 */
public class MbgConstants {

	public static final String EXTENSION_MBG = ".mbg";
	public final String FILE_EXTENSION = "mbg";

	// Dimensions
	public static final String CYCLE_NUMBER = "mbCycleNbr";
	public static final String BEAM_NUMBER = "mbBeamNbr";
	public static final String ANTENNA_NUMBER = "mbAntennaNbr";
	public static final String VELOCITY_PROFILE_NUMBER = "mbVelocityProfilNbr";

	// History (attributes & dimensions)
	public static final String HISTORY_REC_NBR = "mbHistoryRecNbr";
	public static final String NbrHistoryRec = "mbNbrHistoryRec";
	public static final int HistoryRecNbrValue = 20;
	public static final int MinNameLengthValue = 1;
	public static final String NameLength = "mbNameLength";
	public static final int NameLengthValue = 32;
	public static final int MediumNameLengthValue = 80;
	public static final int CDINameLengthValue = 100;
	public static final int InstallParametersLengthValue = 1024;
	public static final String CommentLength = "mbCommentLength";
	public static final int CommentLengthValue = 256;

	// Global attributes
	public static final String Version = "mbVersion";
	public static final Short SupportedVersion = 200;
	public static final Short ActualVersion = 210;
	public static final String Name = "mbName";
	public static final String Classe = "mbClasse";
	public static final String MBGClasse = "MB_SWATH";
	public static final String Level = "mbLevel";
	public static final String TimeReference = "mbTimeReference";

	public static final String StartDate = "mbStartDate";
	public static final String StartTime = "mbStartTime";
	public static final String EndDate = "mbEndDate";
	public static final String EndTime = "mbEndTime";

	public static final String NorthLatitude = "mbNorthLatitude";
	public static final String SouthLatitude = "mbSouthLatitude";
	public static final String EastLongitude = "mbEastLongitude";
	public static final String WestLongitude = "mbWestLongitude";

	public static final String Meridian180 = "mbMeridian180";
	public static final String GeoDictionnary = "GeoDictionnary";
	public static final String GeoRepresentation = "mbGeoRepresentation";
	public static final String GeodesicSystem = "mbGeodesicSystem";
	public static final String EllipsoidName = "mbEllipsoidName";
	public static final String EllipsoidA = "mbEllipsoidA";
	public static final String EllipsoidInvF = "mbEllipsoidInvF";
	public static final String EllipsoidE2 = "mbEllipsoidE2";
	public static final String ProjType = "mbProjType";
	public static final String ProjParameterValue = "mbProjParameterValue";
	public static final String ProjParameterCode = "mbProjParameterCode";

	public static final String Sounder = "mbSounder";
	public static final String SerialNumber = "mbSerialNumber";
	public static final String Ship = "mbShip";
	public static final String Survey = "mbSurvey";
	public static final String Reference = "mbReference";
	public static final String AntennaOffset = "mbAntennaOffset";
	public static final String mbTxAntennaLeverArm = "mbTxAntennaLeverArm"; // bras de levier du sonder, obtenu des
																			// datagrammes d'installation
	public static final String AntennaDelay = "mbAntennaDelay";// Retard de l'horloge du capteur de position
	public static final String SounderOffset = "mbSounderOffset";// Decalage pour le rattachement
																	// Pt-de-reference/sondeur (m)
	public static final String SounderDelay = "mbSounderDelay"; // Retard de l'horloge du sondeur
	public static final String VRUOffset = "mbVRUOffset";// Decalage pour le rattachement Pt-de-reference/centrale (m)
	public static final String VRUDelay = "mbVRUDelay";// Retard de l'horloge de la centrale d'attitude
	public static final String HeadingBias = "mbHeadingBias"; // Biais sur le cap
	public static final String RollBias = "mbRollBias";
	public static final String PitchBias = "mbPitchBias";
	public static final String HeaveBias = "mbHeaveBias";
	public static final String BiasCorrectionRef = "mbBiasCorrectionRef";// Reference des corrections de biais	
	public static final String Draught = "mbDraught";
	public static final String NavType = "mbNavType"; // Degre de traitement de la navigation (brute, validee,...)
	public static final String NavRef = "mbNavRef"; // Reference de la navigation
	public static final String TideType = "mbTideType";// Code la maree (nulle, predite, mesuree)
	public static final String TideRef = "mbTideRef";// Reference de la maree
	public static final String MinDepth = "mbMinDepth";
	public static final String MaxDepth = "mbMaxDepth";
	public static final String CycleCounter = "mbCycleCounter"; // Nombre de cycles presents dans le fichier
	public static final String CDI = "mbCDI";

	public static List<String> GlobalAttributes = Arrays.asList(Version, Name, Classe, Level, NbrHistoryRec,
			TimeReference, StartDate, StartTime, EndDate, EndTime, NorthLatitude, SouthLatitude, WestLongitude,
			EastLongitude, Meridian180, GeodesicSystem, GeodesicSystem, EllipsoidName, EllipsoidA, EllipsoidInvF,
			EllipsoidE2, ProjType, ProjParameterValue, ProjParameterCode, Sounder, SerialNumber, Ship, Survey,
			Reference, CDI, AntennaOffset);

	// correction flags
	public static final String AutomaticCleaning = "mbAutomaticCleaning";
	public static final String ManualCleaning = "mbManualCleaning";
	public static final String PositionCorrection = "mbPositionCorrection";
	public static final String VelocityCorrection = "mbVelocityCorrection";
	public static final String BiasCorrection = "mbBiasCorrection";
	public static final String TideCorrection = "mbTideCorrection";
	public static final String DraughtCorrection = "mbSoundingCorrection";
	public static final String AcrossAngleCorrect = "mbAcrossAngleCorrect";
	public static final String ImReflectivityOrigin = "mbImReflectivityOrigin";

	public static final String InstallParameters = "mbInstallParameters";

	public static final String HistDate = "mbHistDate";
	public static final String HistTime = "mbHistTime";
	public static final String HistCode = "mbHistCode";
	public static final String HistAutor = "mbHistAutor";
	public static final String HistModule = "mbHistModule";
	public static final String HistComment = "mbHistComment";

	public static final String Cycle = "mbCycle";// Cycle number
	public static final String Date = "mbDate"; // Date of cycle
	public static final String Time = "mbTime";// Time of cycle
	public static final String Ordinate = "mbOrdinate";// Y
	public static final String Abscissa = "mbAbscissa";// X
	public static final String Frequency = "mbFrequency";// Frequency plane of cycle
	public static final String SonarFrequency = "mbSonarFrequency";// Sonar frequency of cycle
	public static final String SounderMode = "mbSounderMode";// Sounder mode
	public static final String Immersion = "mbReferenceDepth";// Depth of the reference point
	public static final String DyDraught = "mbDynamicDraught";// Dynamic draught correction
	public static final String Tide = "mbTide"; // Tide correction
	public static final String SoundVelocity = "mbSoundVelocity"; // Surface sound velocity
	public static final String Heading = "mbHeading";// Heading of ship
	public static final String Roll = "mbRoll";
	public static final String Pitch = "mbPitch";
	public static final String Heave = "mbTransmissionHeave";
	public static final String DistanceScale = "mbDistanceScale";
	public static final String DepthScale = "mbDepthScale";
	public static final String VerticalDepth = "mbVerticalDepth";
	public static final String BFlag = "mbBFlag";// Flag of beam
	public static final String CFlag = "mbCFlag";// Flag of cycle
	public static final String Interlacing = "mbInterlacing";// Interlacing 1=Port 2=Starboard
	public static final String SamplingRate = "mbSamplingRate";// Signal sampling rate
	public static final String CompensationLayerMode = "mbCompensationLayerMode";// Mode de compensation layer
	public static final String TransmitBeamwidth = "mbTransmitBeamwidth";// Largeur des faisceaux
	public static final String ReceiveBeamwidth = "mbReceiveBeamwidth";// Largeur des faisceaux recus
	public static final String TransmitPulseLength = "mbTransmitPulseLength";// Frequence d'impulsion
	public static final String OperatorStationStatus = "mbOperatorStationStatus"; // Operator station status
	public static final String ProcessingUnitStatus = "mbProcessingUnitStatus"; // Processing unit status
	public static final String BSPStatus = "mbBSPStatus";// BSP status
	public static final String SonarStatus = "mbSonarStatus";// Sonar head or Tranceiver status
	public static final String FilterIdentifier = "mbFilterIdentifier";
	public static final String ParamMinimumDepth = "mbParamMinimumDepth";
	public static final String ParamMaximumDepth = "mbParamMaximumDepth";
	public static final String AbsorptionCoefficient = "mbAbsorptionCoefficient";// Absorption coefficient in 0.01 dB/km
	public static final String TransmitPowerReMax = "mbTransmitPowerReMax"; // Transmit power re maximum in dB
	public static final String ReceiveBandwidth = "mbReceiveBandwidth";// Receive bandwidth in 50 Hz resoution
	public static final String ReceiverFixedGain = "mbReceiverFixedGain";
	public static final String TVGLawCrossoverAngle = "mbTVGLawCrossoverAngle";
	public static final String TransVelocitySource = "mbTransVelocitySource";// Source of sound speed at transducer
	public static final String MaxPortWidth = "mbMaxPortWidth";
	public static final String BeamSpacing = "mbBeamSpacing";
	public static final String MaxPortCoverage = "mbMaxPortCoverage";// ava
	public static final String YawPitchStabMode = "mbYawPitchStabMode";// ava
	public static final String MaxStarboardCoverage = "mbMaxStarboardCoverage";
	public static final String MaxStarboardWidth = "mbMaxStarboardWidth";
	public static final String DurotongSpeed = "mbDurotongSpeed";// Durotong speed
	public static final String HiLoAbsorptionRatio = "mbHiLoAbsorptionRatio"; // HiLo frequency absorption coefficient
																				// ratio

	public static final String CQuality = "mbCQuality";// Quality factor

	public static final String AlongDistance = "mbAlongDistance";
	public static final String AcrossDistance = "mbAcrossDistance";
	public static final String Depth = "mbDepth";
	public static final String Range = "mbRange";
	public static final String ReceptionHeave = "mbReceptionHeave";
	public static final String Reflectivity = "mbReflectivity"; // Reflectivity
	public static final String SoundingBias = "mbSoundingBias";// Biais du faisceau
	public static final String Quality = "mbQuality"; // Ifremer quality of quality datagram
	public static final String SQuality = "mbSQuality";// Simrad quality factor of depth datagram
	public static final String SLengthOfDetection = "mbSLengthOfDetection";// Length of detection window
	public static final String Validity = "mbSFlag";// Flag of sounding
	public static final String AcrossAngle = "mbAcrossBeamAngle";
	public static final String AlongAngle = "mbAzimutBeamAngle";
	public static final String AlongSlope = "mbAlongSlope";
	public static final String AcrossSlope = "mbAcrossSlope";

	public static final String Antenna = "mbAntenna";// Antenna index
	public static final String BeamValidity = "mbBFlag";// Flag of beam
	public static final String Beam = "mbBeam"; // Number of beams
	public static final String AFlag = "mbAFlag";// Flag of antenna

	// sound velocity profiles
	public static final String VelProfilRef = "mbVelProfilRef";
	public static final String VelProfilIdx = "mbVelProfilIdx";
	public static final String VelProfilDate = "mbVelProfilDate";
	public static final String VelProfilTime = "mbVelProfilTime";

	// parameters
	public static final String MissingValue = "missing_value";
	public static final String AddOffset = CFStandardNames.CF_ADD_OFFSET;
	public static final String ScaleFactor = CFStandardNames.CF_SCALE_FACTOR;
	public static final String ValidMinimum = "valid_minimum";
	public static final String ValidMaximum = "valid_maximum";

	public static final String Unit = CFStandardNames.CF_UNITS;

	public static final short SHORT_MIN_VAL = -32768; // Valeur de la constante min des valeur SHORT
	public static final short SHORT_MAX_VAL = 32767; // Valeur de la constante max des valeur SHORT
	public static final long LONG_MAX_VAL = 2147483647L; // Valeur de la constante max des valeur LONG
	public static final long LONG_MIN_VAL = -2147483648L; // Valeur de la constante min des valeur LONG
	public static final char FLAG_SND_MISSING = 0;
	public static final int AMPLITUDE_DETECTION = 4;
	public static final char FLAG_SND_UNVALID_ACQUIS = (char) -3;

	public static final byte CYCLE_ATTRIBUTE = 1; // Attribute of cycle type
	public static final byte BEAM_ATTRIBUTE = 2; // Attribute of beam type

	public static final short OPPOSITE_SIGN = -1; // change the sign
	public static final short NOT_OPPOSITE_SIGN = 1; // not change the sign

	public static final short LevelValue = 0;

	public static final String TimeReferenceValue = "Julian date for 1970/01/01 = 2 440 588                                          ";
	public static final long TimeReferenceInt = 2440588;

	public static final byte COMPENSATION_LAYER_MODE_UNKNOWN = 0;
	public static final byte COMPENSATION_LAYER_MODE_ACTIVE = 1;
	public static final byte COMPENSATION_LAYER_MODE_INACTIVE = 2;

	public enum SoundingValidity {
		VALID(2), MISSING(0), UNVALID(-1), UNVALID_ACQUISITION(-3);

		private byte value;

		private SoundingValidity(int value) {
			this.value = (byte) value;
		}

		/**
		 * @return the byte value for the given enum
		 */
		public byte getValue() {
			return value;
		}
	}
	

	// Correction flags
	public static final byte MB_TRT_FLAG_ON = 1;
	public static final byte MB_TRT_FLAG_OFF = 0;
	
	// Tide flags
	public static final short MB_FLAG_NO_TIDE = 0; // Pas de maree
	public static final short MB_FLAG_PREDICTED_TIDE = 1; // Maree predite
	public static final short MB_FLAG_MESURED_TIDE = 2; // Maree mesuree
	public static final short MB_FLAG_GPS_TIDE = 4; // Maree GPS

}
