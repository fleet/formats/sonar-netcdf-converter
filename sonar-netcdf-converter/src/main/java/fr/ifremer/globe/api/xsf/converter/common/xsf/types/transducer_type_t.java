package fr.ifremer.globe.api.xsf.converter.common.xsf.types;

public enum transducer_type_t
{
	Rx_Transducer((byte)0), Tx_Transducer((byte)1), RxTx_Transducer((byte)3);
	private byte value;

	public byte getValue() {
		return value;
	}

	transducer_type_t(byte value)
	{
		this.value=value;
	}

}
