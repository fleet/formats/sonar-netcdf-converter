package fr.ifremer.globe.api.xsf.converter.common.sounder;

import fr.ifremer.globe.utils.sounders.SounderDescription;
import fr.ifremer.globe.utils.sounders.SoundersLibrary;
import fr.ifremer.globe.utils.sounders.SounderDescription.Constructor;

/**
 * Proxy for data common between kmall and all file formats
 */
public interface IKongsbergFile extends ISounderFile {

	/**
	 * return the kongsberg code of the model (2040, 710, 850 (ME70)....)
	 */
	int getModelNumberCode();

	@Override
	public default SounderDescription getSounderDescription() {
		return SoundersLibrary.get().fromConstructorId(getModelNumberCode(), Constructor.Kongsberg);
	}

	int getRuntimeDgCount();

	/** Height data */
	long getHeightCount();

	/** Depth external sensor data */
	long getDepthCount();

	/** Clock data */
	long getClockCount();

	/**
	 * Heading sensor input count
	 */
	long getHeadingCount();
	
	
	
}
