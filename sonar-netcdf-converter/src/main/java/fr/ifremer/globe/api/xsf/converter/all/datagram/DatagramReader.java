package fr.ifremer.globe.api.xsf.converter.all.datagram;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.LinkedList;
import java.util.List;

import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.exceptions.BadETXRecord;
import fr.ifremer.globe.api.xsf.converter.common.exceptions.BadSTXRecord;
import fr.ifremer.globe.api.xsf.converter.common.exceptions.ByteOrderDetectionFailed;
import fr.ifremer.globe.api.xsf.converter.common.exceptions.UnexpectedEndOfFile;
import fr.ifremer.globe.api.xsf.util.ObjectBufferPool;
import fr.ifremer.globe.netcdf.util.Pair;

public class DatagramReader {


    public static int readDatagram(RandomAccessFile raf, DatagramBuffer buffer) throws IOException {
        int byteRead = 0;
        boolean ignoreValidation=false;
        byteRead = raf.read(buffer.rawBufferHeader);
        if (byteRead < 4) { //4 is header size
            return byteRead;
        }
        buffer.byteBufferSizeHeader.rewind();
        long datagramSize = Integer.toUnsignedLong(buffer.byteBufferSizeHeader.getInt());
        if(datagramSize>0)
        {
        	int read = raf.read(buffer.rawBufferData, 0, (int) datagramSize);
        	if (read != datagramSize) {
        		// throw exception here
        		throw new UnexpectedEndOfFile();
        	}
        	byteRead += read;
        	buffer.byteBufferData.limit((int) datagramSize);
        	buffer.byteBufferData.rewind();
        	
        	// perform integrity check at parsing time
        	try{
        		buffer.validate();
        	} catch (BadETXRecord e) {
        		if (!ignoreValidation)
        		{
        			throw e;
        		}
        	} catch (BadSTXRecord e) {
        		if (!ignoreValidation)
        		{
        			throw e;
        		}
        	
        	}
        	
        }

        return byteRead;
    }

    /**
     * read one datagram and fill the buffers
     * 
     * @throws IOException
     * @return the number of byte read (i.e the size of the datagram) of -1 if
     *         end of file was reached
     */
    public static int readDatagramAt(RandomAccessFile raf, DatagramBuffer buffer, long fileOffset) throws IOException {
        /**
         * move to starting point in file
         */
        if (fileOffset >= 0) {
            raf.seek(fileOffset);
        }
        return readDatagram(raf, buffer);
    }

    /**
     * Read the first datagram and guess the associated byte order.
     * 
     * Once a byte order have been guessed, check for valid STX and ETX tags
     * to confirm the guess.
     * 
     * @param raf
     * @return
     * @throws IOException 
     */
    public static ByteOrder getByteOrder(RandomAccessFile raf) throws IOException {
        long pos = raf.getFilePointer();
        raf.seek(0);
        byte[] rawBuffer = new byte[4];
        ByteBuffer bufferLE = ByteBuffer.wrap(rawBuffer);
        bufferLE.order(ByteOrder.LITTLE_ENDIAN);
        bufferLE.rewind();

        ByteBuffer bufferBE = ByteBuffer.wrap(rawBuffer);
        bufferBE.order(ByteOrder.BIG_ENDIAN);
        bufferBE.rewind();

        // Read the size
        int byteRead = raf.read(rawBuffer);
        if (byteRead != 4) {
            throw new IOException();
        }
        raf.seek(0);

        long sizeLE = Integer.toUnsignedLong(bufferLE.getInt());
        long sizeBE = Integer.toUnsignedLong(bufferBE.getInt());
        ByteOrder ret;
        if (sizeLE < sizeBE) {
            ret = ByteOrder.LITTLE_ENDIAN;
        } else {
            ret = ByteOrder.BIG_ENDIAN;
        }

        // validate by reading a complete datagram
        DatagramBuffer buffer = new DatagramBuffer(ret);
        readDatagram(raf, buffer);
        raf.seek(pos);
        if (!buffer.isValid()) {
            throw new ByteOrderDetectionFailed();
        }

        return ret;
    }
    
    /**
     * Load a set of datagrams from a list of positions
     * 
     * @param pool
     * @param locations
     * @return
     * @throws IOException
     */
    public static <T> List<Pair<DatagramBuffer, T>> read(ObjectBufferPool<DatagramBuffer> pool, List<Pair<DatagramPosition, T>> locations) throws IOException {
        List<Pair<DatagramBuffer, T>> ret = new LinkedList<Pair<DatagramBuffer,T>>();
        for (Pair<DatagramPosition, T> loc : locations) {
            DatagramBuffer buf = pool.borrow();
            readDatagramAt(loc.getFirst().getFile(), buf, loc.getFirst().getSeek());
            ret.add(new Pair<DatagramBuffer, T>(buf, loc.getSecond()));
        }
        return ret;
    }
}
