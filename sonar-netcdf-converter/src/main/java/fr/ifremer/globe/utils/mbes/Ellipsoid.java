package fr.ifremer.globe.utils.mbes;

/**
 * @author AVA,IPSIS;
 * 
 */
public enum Ellipsoid {
	NONE(0.0000, 0.000000000, "Unknownhhhhh", "Unknowniiiiiii"),
	CLARKE1880(6378249.2000, 6356514.999988159, "Clarke-1880(IGN)", "clrk80"),
	HAYFORD(6378388.0000, 6356911.946127946, "Europe-50", "intl"),
	WGS72(6378135.0000, 6356750.520016094, "WGS-72", "WGS72"),
	BESSEL(6377397.1550, 6356078.962818189, "Bessel-1841", "bessel"),
	EVEREST(6377276.3450, 6356075.413140240, "Everest-1830", "evrst30"),
	CLARKE1866(6378206.4000, 6356583.800000000, "Clarke-1866", "clrk66"),
	MERCURY59(6378166.0000, 6356784.283607107, "Mercury-59", "fschr60"),
	MERCURY68(6378150.0000, 6356768.337244385, "Mercury-68","fschr68"),
	NWL8D(6378145.0000, 6356759.769488684, "WGS-66(NWL-9D)", "WGS66"),
	NWL69(6378144.0000, 6356757.340000000, "NWL-69", "NWL9D"),
	SAO69(6378155.0000, 6356770.300000000, "SAO-69","SEasia"),
	AIGGRS1967(6378160.0000, 6356774.516089331, "AIG-67(SAD-69)", "aust_SA"),
	AIG1975(6378140.0000, 6356755.288156210, "AIG-1975", "IAU76"),
	WGS84(6378137.0000, 6356752.314245179,"WGS-84", "WGS84"),
	CLARKE1880RGS(6378249.1453, 6356514.869848753, "Clarke-1880(RGS)", "clrk80"),
	GRS80(6378137.0000, 6356752.314140356, "GRS80", "GRS80"),
	USER(0.0000, 0.000000000,"UserDefined", "UserDefined");
	
	private double half_A; // Demi grand axe
	private double half_B; // Demi petit axe
	private double eccentricity2;  // Eccentricitu square
	private double flatness; // Aplatissement
	private String ellipsName;
	private String proj4Name;



	Ellipsoid(double half_A, double half_B, String name, String proj4name) {
		this.half_A = half_A;
		this.half_B = half_B;
		this.eccentricity2 = 0.;
		this.flatness = 0.;
		this.ellipsName = name;
		this.proj4Name = proj4name;
		eccentricity2();
		flatness();
	}

	public double eccentricity2() {
		this.eccentricity2 = 1. - (half_B / half_A) * (half_B / half_A);
		return this.eccentricity2;
	}
	
//	public double applat() {
//		flatness = 1. - (Math.sqrt(1. - eccentricity2));
//		return flatness;
//	}

	public double flatness() {
		this.flatness = 1. - (Math.sqrt(1. - this.eccentricity2));
		return this.flatness;
	}
	
	public double getFlatness(){
		return (this.flatness);
	}

	public static Ellipsoid get(String aName) {
		Ellipsoid ellipsoid = NONE;
		for (Ellipsoid e : Ellipsoid.values()) {
			if (aName.equals(e.ellipsName)) {
				ellipsoid = Ellipsoid.valueOf(e.name());
				break;
			}
		}
		return ellipsoid;
	}

	public void setHalf_A(double half_A) {
		this.half_A = half_A;
	}

	public double getHalf_A() {
		return(half_A);
	}

	public void setHalf_B(double half_A, double eccentricity2) {
		this.half_B = Math.sqrt(1. - eccentricity2) * half_A;
	}
	
	public void setHalf_B(double half_B) {
		this.half_B = half_B;
	}
	
	static public double computeHalf_B(double half_A, double eccentricity2) {
		return (Math.sqrt(1. - eccentricity2) * half_A);
	}

	public double getHalf_B() {
		return(half_B);
	}

	public String getEllipsName() {
		return(ellipsName);
	}
	
	public void setEccentricity2(double eccentricity) {
		this.eccentricity2 = eccentricity;
	}

	public double getEccentricity2() {
		return (eccentricity2);
	}

	/*	public void setProj4Name(String aProj4Name) {
		for (Ellipsoid e : Ellipsoid.values()) {
			if (aProj4Name.equals(e.proj4name)) {
				this.half_A = e.half_A;
				this.half_B = e.half_B;
				this.ellipsName = e.ellipsName;
				this.proj4name = e.proj4name;
				break;
			}
		}

	}*/

	public String getProj4Name() {
		return(proj4Name);
	}

	// note will be moved to a ellipsoid object later..
	// semi major axis, set by default to WGS84 value
	// private double semiMajorAxis=LocalEllispoid.half_A;//6378137.0 ;
	// inverse flattening, set by default to WGS84 value
	// private double inverseFlattening=298.25722356299167;
	// EXCENTRICITE, set by default to WGS84 value
	// private double EXCENTRICITE= LocalEllispoid.excentricite();
	// //0.006694380004260827;
	public double GetCurveRadius(double latInDeg) {
		// ----------------------------------------------------------------------------
		// local data
		// ----------------------------------------------------------------------------
		double l_temp;
		// ----------------------------------------------------------------------------
		// work
		// ----------------------------------------------------------------------------
		l_temp = 1.0 - eccentricity2() * Math.pow(Math.sin(latInDeg * MbesCalcConstants.DEGREE_RADIAN), 2);
		return (GetNormal(latInDeg) * (1. - eccentricity2()) / l_temp);
	}

	public double GetNormal(double latInDeg) {
		// ----------------------------------------------------------------------------
		// local data
		// ----------------------------------------------------------------------------
		double l_temp;
		// ----------------------------------------------------------------------------
		// work
		// ----------------------------------------------------------------------------
		l_temp = 1.0 - eccentricity2() * Math.pow(Math.sin(latInDeg * MbesCalcConstants.DEGREE_RADIAN), 2);
		return (half_A / Math.sqrt(l_temp));
	}
	
	public double GetGeodeticDist(double lat1, double lon1, double lat2, double lon2) {
		//Algo CARAIBES (CIB_CCO_Ellipsoid.cc)
		//----------------------------------------------------------------------------
		// DONNEES LOCALES
		//----------------------------------------------------------------------------
		double p_theLength; // Distance entre les deux lat/lons
		double l_aplat2 = flatness * flatness;	// Carre de l'aplatissement
	//	double l_aF1 = l_aplat2 * 1.25;		// facteur du carre de l'aplatissement
		double l_aF2 = l_aplat2 * 0.5;		// facteur du carre de l'aplatissement
	//	double l_aF3 = l_aplat2 * 0.25;		// facteur du carre de l'aplatissement
		double l_aF4 = l_aplat2 * 0.125;		// facteur du carre de l'aplatissement
		double l_aF5 = l_aplat2 * 0.0625;		// facteur du carre de l'aplatissement
		double l_aF6 = l_aplat2 + flatness;		// facteur du carre de l'aplatissement
		double l_aF7 = l_aF6 + 1.0;		// facteur du carre de l'aplatissement
		double l_aF8 = l_aF6 * 0.5;		// facteur du carre de l'aplatissement

		double l_rLat1;		// latitude 1 en radian
		double l_rLat2;		// latitude 2 en radian

		double l_rBeta1;	// angle BETA 1 en radian fonction de latitude 1
		double l_cBeta1;	// cosinus de l'angle BETA 1
		double l_sBeta1;	// sinus de l'angle BETA 1
		double l_rBeta2;	// angle BETA 2 en radian fonction de latitude 2
		double l_cBeta2;	// cosinus de l'angle BETA 2
		double l_sBeta2;	// sinus de l'angle BETA 2

		double l_rDelta;	// angle DELTA en longitude en radian
	//	double l_sDelta;	// sinus de l'angle DELTA
		double l_aDelta;	// valeur absolue de l'angle DELTA
		double l_saDelta;	// sinus de la valeur absolue de l'angle DELTA
		double l_caDelta;	// cosinus de la valeur absolue de l'angle DELTA

		double l_rPhi;		// angle PHI
		double l_uPhi2;		// carre de l'angle PHI
		double l_cPhi;		// cosinus de l'angle PHI
		double l_sPhi;		// sinus de l'angle PHI
		double l_ssPhi;		// 1 / sinus de l'angle PHI
		double l_cotPhi;	// cotangente de l'angle PHI

		double l_uPsy;		// angle PSY

		double l_term1;		// valeur temporaire 1
		double l_term2;		// valeur temporaire 2
		double l_term3;		// valeur temporaire 3
		double l_term4;		// valeur temporaire 4
		double l_term5;		// valeur temporaire 5
		double l_term6;		// valeur temporaire 6

		double l_temp1;		// valeur de calcul 1
		double l_temp2;		// valeur de calcul 2
		double l_temp3;		// valeur de calcul 3
		double l_temp4;		// valeur de calcul 4

		//double l_uZ12;		// valeur temporaire de calcul de l'azimuth 12
	//	double l_uZ21;		// valeur temporaire de calcul de l'azimuth 21

		//----------------------------------------------------------------------------
		// TRAITEMENTS
		//----------------------------------------------------------------------------

		l_rLat1 = Math.toRadians(lat1);
		l_rLat2 = Math.toRadians(lat2);

		l_rDelta = Math.toRadians(lon1) - Math.toRadians(lon2);

		if (l_rLat1 == l_rLat2 && l_rDelta == 0.0)
		{
			p_theLength   = 0.0;
			//			   p_azimuth1to2 = 0.0;
			//			   p_azimuth2to1 = 0.0;
		}
		else
		{
			if (l_rDelta >   Math.PI)  l_rDelta -=  Math.PI*2;
			if (l_rDelta < - Math.PI)  l_rDelta += Math.PI*2;

			if (Math.abs(l_rLat1) != Math.PI/2)
			{
				l_rBeta1 = Math.atan((1.0 - flatness) * Math.sin(l_rLat1) / Math.cos(l_rLat1));
				l_sBeta1 = Math.sin(l_rBeta1);
				l_cBeta1 = Math.cos(l_rBeta1);
			}
			else
			{
				l_sBeta1 = 0.0;
				l_cBeta1 = 1.0;
			}
			if (Math.abs(l_rLat2) != Math.PI/2)
			{
				l_rBeta2 = Math.atan((1.0 - flatness) * Math.sin(l_rLat2) / Math.cos(l_rLat2));
				l_sBeta2 = Math.sin(l_rBeta2);
				l_cBeta2 = Math.cos(l_rBeta2);
			}
			else
			{
				l_sBeta2 = 0.0;
				l_cBeta2 = 1.0;
			}

		//	l_sDelta = Math.sin(l_rDelta);
			l_aDelta = Math.abs(l_rDelta);

			if (l_aDelta >= Math.PI) l_aDelta = Math.PI*2 - l_aDelta;

			l_saDelta = Math.sin(l_aDelta);
			l_caDelta = Math.cos(l_aDelta);

			l_temp1 = l_sBeta1 * l_sBeta2;
			l_temp2 = l_cBeta1 * l_cBeta2;

			l_cPhi  = l_temp1 + l_temp2*l_caDelta;
			l_sPhi  = Math.sqrt(Math.pow(l_saDelta*l_cBeta2, 2) + Math.pow(l_sBeta2*l_cBeta1 - l_sBeta1*l_cBeta2*l_caDelta, 2.));
			l_temp3 = l_temp2 * l_saDelta / l_sPhi;
			l_temp4 = 1.0 - (l_temp3 * l_temp3);
			l_rPhi  = Math.asin(l_sPhi);

			if (l_cPhi < 0.0) l_rPhi = Math.PI - l_rPhi;

			l_uPhi2  = l_rPhi * l_rPhi;
			l_ssPhi  = 1.0 / l_sPhi;
			l_cotPhi = l_cPhi / l_sPhi;
			l_uPsy   = l_sPhi * l_cPhi;

			l_term1 = l_aF7 * l_rPhi;
			l_term2 = l_temp1 * ((l_aF6 * l_sPhi) - (l_aF2 * l_uPhi2 * l_ssPhi));
			l_term3 = l_temp4 * ((l_aF2 * l_uPhi2 * l_cotPhi) - (l_aF8 * (l_rPhi + l_uPsy)));
			l_term4 = l_temp1 * l_temp1 * l_aF2 * l_uPsy;
			l_term5 = l_temp4 * l_temp4 * ((l_aF5 * (l_rPhi + l_uPsy)) - 
					(l_aF2 * l_uPhi2 * l_cotPhi) -
					(l_aF4 * l_uPsy * l_cPhi * l_cPhi));
			l_term6 = l_temp1 * l_temp4 * l_aF2 * ((l_uPhi2 * l_ssPhi) +
					(l_uPsy * l_cPhi));

			p_theLength = half_A * Math.sqrt(1.0 - eccentricity2) * (l_term1 + l_term2 + l_term3 -
					l_term4 + l_term5 + l_term6);

		//	l_uZ12 = ((l_sBeta2 / l_cBeta2) * l_cBeta1) - (l_sBeta1 * l_caDelta);
		//	l_uZ21 = (l_sBeta2 * l_caDelta) - ((l_sBeta1 / l_cBeta1) * l_cBeta2);

			//			   p_azimuth1to2 = CIB_CCO_RADIAN_DEGREE * Math.atan2(- l_sDelta, l_uZ12);
			//			   p_azimuth2to1 = CIB_CCO_RADIAN_DEGREE * (Math.atan2(- l_sDelta, l_uZ21) + 
					//					   Math.PI);

			//			   if (p_azimuth1to2 < 0.0) p_azimuth1to2 += CIB_CCO_2_PI_DEGREE;
		}

		return p_theLength;
	}

}
