package fr.ifremer.globe.utils.sounders;

import java.util.Collection;

import fr.ifremer.globe.utils.sounders.SounderDescription.Constructor;
/**
 * A list of known sounders and their link with their construtors ID and ID identified in MBG
 * 
 * */
public class SoundersLibrary {

	private MiscLibrary misc;
	static SoundersLibrary instance=new SoundersLibrary();

	public static SoundersLibrary get()
	{
		return instance;
	}
	
	public SoundersLibrary()
	{
		misc=new MiscLibrary();
		
	}
	
	/**
	 * Find sounder description  
	 * */
	public Collection<SounderDescription> getSounderDescription(int genericID)
	{
		return misc.getWithGenericId(genericID);
	}
	
	/**
	 * return the first and robust sounder description for the given generic id, 
	 * if at least one SounderDescription is associated to the given id, return the first one
	 * if non is found, return a generic description
	 * */
	public SounderDescription getFirstSounderDescription(int genericID)
	{
		Collection<SounderDescription> ret=misc.getWithGenericId(genericID);
		if(ret==null || ret.isEmpty() )
			return misc.genericSounder;
		else return ret.iterator().next();
	}
	
	/**
	 * get the sounder description of a given constructor model id and constructor type
	 * @param constructor the constructor {@link Constructor}
	 * @param constructorId the constructor model Id
	 * @return the value found or a genericSounder if not found
	 * */
	public SounderDescription fromConstructorId(int constructorId, Constructor constructor)
	{
		SounderDescription desc=misc.getWithConstructorId(constructorId, constructor);
		if(desc==null)
			return misc.genericSounder;
		return desc;
	}
	
	class KongsberDescription extends SounderDescription
	{

		public KongsberDescription( int constructorSounderID, int genericId, String name) {
			super(Constructor.Kongsberg, constructorSounderID, (short)genericId, name);
		}
		
	}
	class ResonDescription extends SounderDescription
	{

		public ResonDescription( int constructorSounderID, int genericId, String name) {
			super(Constructor.Reson, constructorSounderID, (short)genericId, name);
		}
		
	}
	class MiscDescription extends SounderDescription
	{

		public MiscDescription( int constructorSounderID, int genericId, String name) {
			super(Constructor.Unknown, constructorSounderID, (short)genericId, name);
		}
		
	}
	
	/**
	 * The Library of known sounders
	 * */
	class MiscLibrary extends Library
	{
		SounderDescription genericSounder=new MiscDescription(0,(short) 0, "Unknow Sounder");
		MiscLibrary()
		{
			super("Library");
			buildKonsgberg();
			buildReson();
			build();
		}
		private void buildReson() {

			add(new ResonDescription(7111, 84,"7111 (Reson)"));
			add(new ResonDescription(7125, 85,"7125 (Reson)"));
			add(new ResonDescription(7150, 86,"7150 (Reson)"));

		}
		private void buildKonsgberg() {

			add(new KongsberDescription(100, 50,"EM100 (Kongsberg)"));
			add(new KongsberDescription(1000, 51,"EM950/EM1000 (Kongsberg)"));
			//			add(new SounderDescription(12, 52,"EM12- single (Kongsberg)"));
			//			add(new SounderDescription(12, 53,"EM12-dual (Kongsberg)"));
			add(new KongsberDescription(3000, 54,"EM3000-single (Kongsberg)"));
			add(new KongsberDescription(3002, 55,"EM3000-dual (Kongsberg)"));
			add(new KongsberDescription(3003, 55,"EM3000-dual (Kongsberg)"));
			add(new KongsberDescription(3004, 55,"EM3000-dual (Kongsberg)"));
			add(new KongsberDescription(3005, 55,"EM3000-dual (Kongsberg)"));
			add(new KongsberDescription(3006, 55,"EM3000-dual (Kongsberg)"));
			add(new KongsberDescription(3007, 55,"EM3000-dual (Kongsberg)"));
			add(new KongsberDescription(3008, 55,"EM3000-dual (Kongsberg)"));

			add(new KongsberDescription(300, 56,"EM300 (Kongsberg)"));
			add(new KongsberDescription(1002, 57,"EM1002 (Kongsberg)"));
			add(new KongsberDescription(120, 58,"EM120 (Kongsberg)"));
			add(new KongsberDescription(2000, 59,"EM2000 (Kongsberg)"));

			add(new KongsberDescription(122, 65,"EM122 (Kongsberg)"));

			// EM3002: (Equidistant (60), high density(61), dual(62), high density
			// and dual (68)
			// improve that!!!!!
			add(new KongsberDescription(3020, 60,"EM3002 (Kongsberg)"));
			add(new KongsberDescription(3020,61, "EM3002-high frequency (Kongsberg)"));
			add(new KongsberDescription(3020,  62, "EM3002-dual (Kongsberg)"));
			add(new KongsberDescription(3020, 68, "EM3002 HD dual (Kongsberg)"));
			//			SoundersNames.put((short) 60, "EM3002-equidistant (Kongsberg)");
			//			SoundersNames.put((short) 61, "EM3002-high frequency (Kongsberg)");
			//			SoundersNames.put((short) 62, "EM3002-dual (Kongsberg)");
			//			SoundersNames.put((short) 68, "EM3002 HD dual (Kongsberg)");

			add(new KongsberDescription(710, 63,"EM710 (Kongsberg)"));

			add(new KongsberDescription(850, 64,"ME70 (Simrad)"));
			add(new KongsberDescription(302, 66,"EM302 (Kongsberg)"));

			add(new KongsberDescription(2040, 67,"EM2040 (Kongsberg)"));
			add(new KongsberDescription(2040, 69,"EM2040 Dual Head (Kongsberg)"));
			add(new KongsberDescription(304, 94,"EM304 (Kongsberg)"));
			add(new KongsberDescription(2045, 95,"EM2040C (Kongsberg)"));
			add(new KongsberDescription(712, 96,"EM712 (Kongsberg)"));
		}
		private void build() {

			add(genericSounder);
			add(new MiscDescription(0,(short) 10, "SeaBeam (GIC)"));
			add(new MiscDescription(0,(short) 11, "SeaBeam 2100"));
			add(new MiscDescription(0,(short) 12, "SeaBeam 2000"));
			add(new MiscDescription(0,(short) 13, "SeaBeam 1185"));
			add(new MiscDescription(0,(short) 20, "Echos-xd (Hollming, 15 beams)"));
			add(new MiscDescription(0,(short) 21, "Echos-xd (Hollming, 60 beams)"));
			add(new MiscDescription(0,(short) 30, "Hydrosweep (Krupp-Atlas)"));
			add(new MiscDescription(0,(short) 34, "Hydrosweep (ATLAS (EVA)"));
			add(new MiscDescription(0,(short) 35, "Hydrosweep MD (ATLAS)"));
			add(new MiscDescription(0,(short) 36, "Hydrosweep DS (ATLAS)"));
			add(new MiscDescription(0,(short) 40, "Lennermor (Thomson)"));
			add(new MiscDescription(0,(short) 41, "Sea Falcon 11 or TMS-5265B (Thomson)"));
			add(new MiscDescription(0,(short) 42, "Imbat (Thalès)"));

			add(new MiscDescription(0,(short) 70, "Furuno"));
			add(new MiscDescription(0,(short) 80, "SEABAT_8101 (Reson)"));
			add(new MiscDescription(0,(short) 81, "SEABAT_8111 (Reson)"));
			add(new MiscDescription(0,(short) 82, "SEABAT_8150 (Reson)"));
			add(new MiscDescription(0,(short) 83, "SEABAT_8125 (Reson)"));

			add(new MiscDescription(0,(short) 87, "MS2000 (Simrad)"));
			add(new MiscDescription(0,(short) 88, "SEABAT_8160 (Reson)"));
			add(new MiscDescription(0,(short) 90, "Geoswath (Geoacoustic)"));
			add(new MiscDescription(0,(short) 91, "e1050 (Elac)"));
			add(new MiscDescription(0,(short) 92, "e1180 (Elac)"));
			add(new MiscDescription(0,(short) 93, "Generic Sounder"));
			
		}
	}

	


}
