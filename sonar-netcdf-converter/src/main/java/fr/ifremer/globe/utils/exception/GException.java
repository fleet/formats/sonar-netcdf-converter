package fr.ifremer.globe.utils.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Base class for Globe Exception
 * */
public abstract class GException extends Exception {
	private final Logger logger = LoggerFactory.getLogger(GException.class);

	/**
	 * 
	 */
	private static final long serialVersionUID = -1505557213263654012L;

	/**
	 * @see Exception#Exception()
	 * */
	public GException() {
		super();
		logger.error("Exception thrown");

	}

	/**
	 * @see Exception#Exception(String , Throwable )
	 * */
	public GException(String message, Throwable cause) {
		super(message, cause);
		logger.error(message, cause);

	}

	/**
	 * @see Exception#Exception(String)
	 * */
	public GException(String message) {
		super(message);
		
		logger.error(message);

	}
}
