package fr.ifremer.globe.utils.sounders;

import java.util.Collection;
import java.util.HashMap;

import org.apache.commons.collections4.map.MultiValueMap;

import fr.ifremer.globe.utils.sounders.SounderDescription.Constructor;

/**
 * A library specific for a given constructor
 * id are supposed to be uniques for a given constructor
 * */
class Library
{
	private String name;

	public String getName() {
		return name;
	}

	Library(String constructor)
	{
		this.name=constructor;
	}
	
	MultiValueMap<Short, SounderDescription> mapPerGenericId=new MultiValueMap<>(); // several constructor ids can be mapped to the same generic id
	
	// for each the constructor, there is a one to one relation between the model id and its description
	HashMap<Constructor,HashMap<Integer,SounderDescription>> mapPerConstructor=new HashMap<>();
	/**
	 * add a description to the Library
	 * */
	public void add(SounderDescription desc)
	{
		mapPerGenericId.put(desc.genericId, desc);
		if(mapPerConstructor.get(desc.getCompany())==null)
		{
			mapPerConstructor.put(desc.getCompany(),new HashMap<>());
		}
		mapPerConstructor.get(desc.getCompany()).put(desc.getConstructorModelID(),desc);
	}
	
	/**
	 * return the SounderDescriptions for the given generics ID
	 * */
	public Collection<SounderDescription> getWithGenericId(int genericID)
	{
		return mapPerGenericId.getCollection((short)genericID);
	}
	/**
	 * return the SounderDescription for the given model ID for a constructor
	 * can return null if constructor is not known or if model id is not known
	 * */
	public SounderDescription getWithConstructorId(int modelId, Constructor company)
	{
		HashMap<Integer,SounderDescription> constructorMap=mapPerConstructor.get(company);
		if(constructorMap!=null)
		{
			return constructorMap.get(modelId);
		}
		return null;
	}
}
