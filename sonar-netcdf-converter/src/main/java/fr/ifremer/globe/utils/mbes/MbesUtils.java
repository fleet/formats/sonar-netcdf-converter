package fr.ifremer.globe.utils.mbes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class MbesUtils {

	protected static Logger logger = LoggerFactory.getLogger(MbesUtils.class);

	/**
	 * Utility class, private constructor
	 * */
	private MbesUtils()
	{
		
	}
	
	/**
	 * Compute lat lon position from a ship position and across along distance
	 * @param shiplat ship latitude in deg
	 * @param shiplon ship longitude
	 * @param heading the heading in degree
	 * @param acrossDistance distance in the across ship direction in meter
	 * @param along distance in the alongship direction in meter
	 * @return the position lat[0] and lon[1]
	 * */
	public static double[] acrossAlongToWGS84LatLong(double shiplat, double shiplon,double headingDeg,double acrossDistance, double alongDistance)
	{
		double[] normRayon = MbesUtils.calcNormRayon(shiplat, Ellipsoid.WGS84);
		double norm = normRayon[0];
		double rayon = normRayon[1];
		double cosHeading = Math.cos(Math.toRadians(headingDeg));
		double sinHeading = Math.sin(Math.toRadians(headingDeg));
		return MbesUtils.acrossAlongToLatitudeLongitude(shiplon,shiplat, alongDistance,acrossDistance, rayon, sinHeading, cosHeading, norm);

	}

	/**
	 * @param lonNav
	 *            longitude navigation in radians
	 * @param latNav
	 *            latitude navigation in radians
	 * @param along
	 *            along in meter
	 * @param accross
	 *            accross in meter
	 * @param rayon
	 *            rayon
	 * @param sinHeading
	 *            sin of the head
	 * @param cosHeading
	 *            cos of the head
	 * @param norm
	 * @return the latitude and the longitude
	 */
	public static void acrossAlongToLatitudeLongitude(double[] results,final double lonNav, final double latNav, double along, double accross, final double rayon, final double sinHeading, final double cosHeading,
			final double norm) {


		results[0] = latNav + Math.toDegrees(along * cosHeading - accross * sinHeading) / rayon;

		results[1] = lonNav + Math.toDegrees(along * sinHeading + accross * cosHeading) / norm / Math.cos(Math.toRadians(latNav));

	}

	/**
	 * @param lonNav
	 *            longitude navigation in radians
	 * @param latNav
	 *            latitude navigation in radians
	 * @param along
	 *            along in meter
	 * @param accross
	 *            accross in meter
	 * @param rayon
	 *            rayon
	 * @param sinHeading
	 *            sin of the head
	 * @param cosHeading
	 *            cos of the head
	 * @param norm
	 * @return the latitude and the longitude
	 */
	public static double[] acrossAlongToLatitudeLongitude(final double lonNav, final double latNav, double along, double accross, final double rayon, final double sinHeading, final double cosHeading,
			final double norm) {

		double[] ret = new double[2];

		acrossAlongToLatitudeLongitude(ret,lonNav,latNav,  along,  accross,  rayon,   sinHeading,   cosHeading, norm);
		return ret;
	}
	/**
	 * @param latNav
	 *            latitude navigation in degrees
	 * @param eccentricitySquared
	 *            excentricite
	 * @param semiMajorAxis
	 *            demi grand axe
	 * @return the norm and the rayon
	 */
	public static double[] calcNormRayon(final double latNav, final double eccentricitySquared, final double semiMajorAxis) {
		double[] ret = new double[2];
		calcNormRayon(ret,latNav,eccentricitySquared,semiMajorAxis);
		
		return ret;
	}
	/**
	 * @param latNav
	 *            latitude navigation in degrees
	 * @param eccentricitySquared
	 *            excentricite
	 * @param semiMajorAxis
	 *            demi grand axe
	 * @return the norm and the rayon
	 */
	public static void calcNormRayon(double[] ret,final double latNav, final double eccentricitySquared, final double semiMajorAxis) {

		double sinLN2 = Math.pow(Math.sin(Math.toRadians(latNav)), 2);
		double e2MoinssinLN2 = 1.0 - eccentricitySquared * sinLN2;
		ret[0] = semiMajorAxis / Math.sqrt(e2MoinssinLN2);
		ret[1] = ret[0] * (1.0 - eccentricitySquared) / e2MoinssinLN2;

	}
	/**
	 * @param navLat
	 *            latitude navigation in radians
	 * @return the norm and the rayon
	 */
	public static double[] calcNormRayon(final double navLat, final Ellipsoid ellipsoid) {
		return calcNormRayon(navLat, ellipsoid.getEccentricity2(), ellipsoid.getHalf_A());
	}
}
