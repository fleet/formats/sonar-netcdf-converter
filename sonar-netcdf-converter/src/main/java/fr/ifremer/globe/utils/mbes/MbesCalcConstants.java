package fr.ifremer.globe.utils.mbes;

public class MbesCalcConstants {

	/**
	 * Prevents instantiation.
	 */
	private MbesCalcConstants() {
	}
	public static double RADIAN_DEGREE = 180. / Math.PI;
	public static double DEGREE_RADIAN = Math.PI / 180.;
	/**
	 * 
	 */
	public static final double ECCENTRICITY_SQUARED = 0.00669437999014144;

	/**
	 * 
	 */
	public static final double SEMI_MAJOR_AXIS = 6378137;
	/**
	 * 
	 */
	public static final double AVERAGE_SOUND_VELOCITY = 1512.3;

}
