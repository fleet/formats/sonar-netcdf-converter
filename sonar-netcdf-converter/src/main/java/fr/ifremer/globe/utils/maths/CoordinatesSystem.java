package fr.ifremer.globe.utils.maths;

public class CoordinatesSystem {
	private CoordinatesSystem() {}
	
	/**
	 * Conversion from VCS to SCS (Vessel Coordinate System to Surface Coordinate System)
	 * */
	public static Vector3D fromVCStoSCS(Vector3D positionVCS, double roll, double pitch)
	{
		Vector3D positionSCS = new Vector3D();
		RotationMatrix3x3D attitudeMatrix = new RotationMatrix3x3D();
		MatrixBuilder.setDegRotationMatrix(0, pitch, roll, attitudeMatrix);
		Ops.mult(attitudeMatrix, positionVCS, positionSCS);
		return positionSCS;
	}
}
