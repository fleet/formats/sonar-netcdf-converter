package fr.ifremer.globe.utils.maths;

/**
 * A rotation matrix is a special cases of matrix since A<sup>-1</sup> = A<sup>T</sup>
 * <p> This allow to make some assumption in {@link Ops}
 * Matrix are equals to Identity matrix when created
 * */
public class RotationMatrix3x3D extends AbstractMatrix3x3D {
	// we use ejml to implements computation 
	/**
	 * Construct a new Identity matrix
	 * */
	public RotationMatrix3x3D() {
	}

}
