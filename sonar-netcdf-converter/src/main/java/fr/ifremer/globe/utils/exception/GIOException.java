package fr.ifremer.globe.utils.exception;
/**
 * A globe file access or decoding exception
 * */
public class GIOException extends GException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5294535773979573406L;

	public GIOException(String paramString) {
		super(paramString);
	}

	public GIOException(String string, Throwable e) {
		super(string,e);
	}
	
	
	/***
	 * Wrap an Exception into a {@link GIOException}
	 * */
	public static GIOException wrap(String message,Exception e)
	{
		return new GIOException(message, e);
	}
	
}
