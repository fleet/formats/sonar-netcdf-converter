package fr.ifremer.globe.utils.maths;

/**
 * Exception occured during math Calculation
 */
public class GMathException extends Exception {
    /**
     * default serial
     */
    private static final long serialVersionUID = 1L;

    GMathException() {
        super();
    }

    GMathException(String message) {
        super(message);

    }

    GMathException(String message, Throwable cause) {
        super(message, cause);
    }
}
