package fr.ifremer.globe.utils.maths;
/**
 * Matrix builder utility <p>
 * Allow to create rotation matrix
 * 
 * */
public class MatrixBuilder {
	private MatrixBuilder() {
		throw new IllegalStateException("Utility class");
	}

	/**
	 * Update the matrix with values corresponding to a rotation matrix along z axis (heading), x axis (roll),y axis (pitch)
	 * @param heading the angle value in degrees
	 * @param pitch the angle value in degrees
	 * @param roll the angle value in degrees
	 * @param out the updated matrix (modified)
	 * */
	public static void setDegRotationMatrix(double heading, double pitch, double roll,RotationMatrix3x3D out)
	{
		setRotationMatrix(Math.toRadians(heading), Math.toRadians(pitch), Math.toRadians(roll), out);
	}
	
	/**
	 * Update the matrix with values corresponding to a rotation matrix along z axis (heading), x axis (roll),y axis (pitch)
	 * @param heading the angle value in radians
	 * @param pitch the angle value in radians
	 * @param roll the angle value in radians
	 * @param out the updated matrix (modified)
	 * */
	public static void setRotationMatrix(double heading, double pitch, double roll,RotationMatrix3x3D out)
	{

		double pitch11=Math.cos(pitch);
		double pitch12= 0;
		double pitch13=Math.sin(pitch);
		double pitch21=0;
		double pitch22= 1;
		double pitch23=0;
		double pitch31=-Math.sin(pitch);
		double pitch32=0;
		double pitch33= Math.cos(pitch);


		double roll11=1;
		double roll12=0;
		double roll13=0;

		double roll21=0;
		double roll22=Math.cos(roll);
		double roll23=-Math.sin(roll);

		double roll31=0;
		double roll32=Math.sin(roll);
		double roll33=Math.cos(roll);

		double heading11=Math.cos(heading);
		double heading12= -Math.sin(heading);
		double heading13=0;

		double heading21=Math.sin(heading);
		double heading22=Math.cos(heading);
		double heading23=0;

		double heading31=0;
		double heading32=0;
		double heading33=1;

		double pitchroll11 = pitch11*roll11 + pitch12*roll21 + pitch13*roll31;
		double pitchroll12 = pitch11*roll12 + pitch12*roll22 + pitch13*roll32;
		double pitchroll13 = pitch11*roll13 + pitch12*roll23 + pitch13*roll33;
		double pitchroll21 = pitch21*roll11 + pitch22*roll21 + pitch23*roll31;
		double pitchroll22 = pitch21*roll12 + pitch22*roll22 + pitch23*roll32;
		double pitchroll23 = pitch21*roll13 + pitch22*roll23 + pitch23*roll33;
		double pitchroll31 = pitch31*roll11 + pitch32*roll21 + pitch33*roll31;
		double pitchroll32 = pitch31*roll12 + pitch32*roll22 + pitch33*roll32;
		double pitchroll33 = pitch31*roll13 + pitch32*roll23 + pitch33*roll33;

		out.imp.a11 = heading11*pitchroll11 + heading12*pitchroll21 + heading13*pitchroll31;
		out.imp.a12 = heading11*pitchroll12 + heading12*pitchroll22 + heading13*pitchroll32;
		out.imp.a13 = heading11*pitchroll13 + heading12*pitchroll23 + heading13*pitchroll33;
		out.imp.a21 = heading21*pitchroll11 + heading22*pitchroll21 + heading23*pitchroll31;
		out.imp.a22 = heading21*pitchroll12 + heading22*pitchroll22 + heading23*pitchroll32;
		out.imp.a23 = heading21*pitchroll13 + heading22*pitchroll23 + heading23*pitchroll33;
		out.imp.a31 = heading31*pitchroll11 + heading32*pitchroll21 + heading33*pitchroll31;
		out.imp.a32 = heading31*pitchroll12 + heading32*pitchroll22 + heading33*pitchroll32;
		out.imp.a33 = heading31*pitchroll13 + heading32*pitchroll23 + heading33*pitchroll33;
	}

	/**
	 * Update the matrix with values corresponding to a rotation matrix along z axis (heading)
	 * @param angleDeg the angle value in degrees
	 * @param out the updated matrix (modified)
	 * */
	public static void setDegHeadingRotationMatrix(double angleDeg,RotationMatrix3x3D out)
	{
		setHeadingRotationMatrix(Math.toRadians(angleDeg), out);
	}
	/**
	 * Update the matrix with values corresponding to a rotation matrix along z axis (heading)
	 * @param angleRad the angle value in radian
	 * @param out the updated matrix (modified)
	 * */
	public static void setHeadingRotationMatrix(double angleRad,RotationMatrix3x3D out)
	{
		out.set(Math.cos(angleRad), -Math.sin(angleRad), 0, 
				Math.sin(angleRad), Math.cos(angleRad), 0, 
				0, 0, 1);
	}
	/**
	 * Update the matrix with values corresponding to a rotation matrix along x axis (roll)
	 * @param angleDeg the angle value in degrees
	 * @param out the updated matrix (modified)
	 * */
	public static void setDegRollRotationMatrix(double angleDeg,RotationMatrix3x3D out)
	{	
		setRollRotationMatrix(Math.toRadians(angleDeg), out);
	}
	
	/**
	 * Update the matrix with values corresponding to a rotation matrix along x axis (roll)
	 * @param angleRad the angle value in radian
	 * @param out the updated matrix (modified)
	 * */
	public static void setRollRotationMatrix(double angleRad,RotationMatrix3x3D out)
	{
		out.set(1,0,0,
				0,Math.cos(angleRad),-Math.sin(angleRad),
				0,Math.sin(angleRad),Math.cos(angleRad));
	}
	/**
	 * Update the matrix with values corresponding to a rotation matrix along y axis (pitch)
	 * @param angleDeg the angle value in degree
	 * @param out the updated matrix (modified)
	 * */
	public static void setDegPithRotationMatrix(double angleDeg,RotationMatrix3x3D out) {
		setPithRotationMatrix(Math.toRadians(angleDeg),out);
	}
	/**
	 * Update the matrix with values corresponding to a rotation matrix along y axis (pitch)
	 * @param angleRad the angle value in radians
	 * @param out the updated matrix (modified)
	 * */
	public static void setPithRotationMatrix(double angleRad,RotationMatrix3x3D out)
	{
		out.set(Math.cos(angleRad), 0, Math.sin(angleRad), 
				0, 1,0, 
				-Math.sin(angleRad), 0, Math.cos(angleRad));
	}

}
