package fr.ifremer.globe.utils.signal;

public class SignalUtils {

	public static double dBToEnergy(double reflectivity) {
		return Math.pow(10, reflectivity/10);

	}

	public static double energyTodB(double natural) {
		if(natural<0)
			return Double.NaN;
		return 10*Math.log10(natural);
	}
	public static double dBToAmplitude(double reflectivity) {
		return Math.pow(10, reflectivity / 20.0);

	}

	public static double amplitudeTodB(double natural) {
		if(natural<0)
			return Double.NaN;
		return 20.0 * Math.log10(natural);
	}
	/**
	 * Convert a distance in sample given the current soundSpeed and the
	 * sampling Rate)
	 * 
	 * @param distance
	 *            the distance to convert in meters
	 * @param soundSpeed
	 *            the sound speed in meters per second
	 * @param samplingRate
	 *            the sampling rate in Hz
	 * @return the sample number as integer
	 * */
	public static int distanceToSample(double distance, double soundSpeed, double samplingRate) {
		// compute the size of a sample
		double sampling = computeSamplingSize(soundSpeed, samplingRate);
		return distanceToSample(distance, sampling);
	}

	/**
	 * Convert a distance in sample given the sampling size
	 * 
	 * @param distance
	 *            the distance to convert in meters
	 * @param samplingSize
	 *            the sampling size in meter
	 * @return the sample number as integer
	 * */
	public static int distanceToSample(double distance, double samplingSize) {
		// compute the size of a sample
		int distanceInSample = (int) Math.ceil(distance / samplingSize);
		return distanceInSample;
	}

	/**
	 * Compute the size of a sample given the sound speed and the sampling Rate
	 * 
	 * @param soundSpeed
	 *            the sound speed in meters per second
	 * @param samplingRate
	 *            the sampling rate in Hz
	 * @return the sample size as double
	 * */
	public static double computeSamplingSize(double soundSpeed, double samplingRate) {
		// compute the size of a sample
		double sampling = (soundSpeed / 2) / samplingRate;
		return sampling;
	}
}
