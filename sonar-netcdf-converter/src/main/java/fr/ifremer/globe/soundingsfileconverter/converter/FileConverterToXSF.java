package fr.ifremer.globe.soundingsfileconverter.converter;

import fr.ifremer.globe.api.xsf.converter.SounderFileConverter;
import fr.ifremer.globe.api.xsf.converter.XsfConverterParameters;
import fr.ifremer.globe.netcdf.ucar.NCException;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

public class FileConverterToXSF implements IFileConverter {

    /**
     * logger
     */
    protected static Logger logger = LoggerFactory.getLogger(FileConverterToXSF.class);

    /**
     * XSF parameters: retrieved from the command line
     **/
    private boolean overwriteOpt;

    public static final String TITLE_OPT = "XsfTitle";
    private Optional<String> titleOpt;

    public static final String SUMMARY_OPT = "XsfSummary";
    private Optional<String> summaryOpt;

    public static final String KEYWORDS_OPT = "XsfKeywords";
    private Optional<String> keywordsOpt;

    public static final String LICENSE_OPT = "XsfLicense";
    private Optional<String> licenseOpt;

    public static final String RIGHTS_OPT = "XsfRights";
    private Optional<String> rightsOpt;

    public static final String IGNORE_WATER_COLUMN_DESC = "Ignore water column datagrams and water column specific files";
    public static final String IGNORE_WATER_COLUMN_OPT = "ignoreWC";
    private boolean ignoreWaterColumn = false;

    /**
     * Checks if the file can be used by the converter.
     */
    @Override
    public boolean canBeConverted(String filename) {
        return new SounderFileConverter().accept(filename);
    }

    /**
     * Adds specific XSF options.
     */
    public static Options createCommandLineOptions(Options options) {
        options.addOption(
                Option.builder(IGNORE_WATER_COLUMN_OPT).required(false).hasArg(false).desc(IGNORE_WATER_COLUMN_DESC).build());

        options.addOption(
                Option.builder(TITLE_OPT).required(false).hasArg(true).desc(XsfConverterParameters.TITLE_DESC).build());

        options.addOption(Option.builder(SUMMARY_OPT).required(false).hasArg(true)
                .desc(XsfConverterParameters.SUMMARY_DESC).build());

        options.addOption(Option.builder(KEYWORDS_OPT).required(false).hasArg(true)
                .desc(XsfConverterParameters.KEYWORDS_DESC).build());

        options.addOption(Option.builder(LICENSE_OPT).required(false).hasArg(true)
                .desc(XsfConverterParameters.LICENSE_DESC).build());

        options.addOption(Option.builder(RIGHTS_OPT).required(false).hasArg(true)
                .desc(XsfConverterParameters.RIGHTS_DESC).build());

        return options;
    }

    /**
     * Parses command line to get specific options.
     */
    @Override
    public void parseCommandLine(CommandLine commandLine) throws ParseException {
        overwriteOpt = commandLine.hasOption(Utils.OVERWRITE_OPTION);

        titleOpt = commandLine.hasOption(TITLE_OPT) ? Optional.of(commandLine.getOptionValue(TITLE_OPT))
                : Optional.empty();

        summaryOpt = commandLine.hasOption(SUMMARY_OPT) ? Optional.of(commandLine.getOptionValue(SUMMARY_OPT))
                : Optional.empty();

        keywordsOpt = commandLine.hasOption(KEYWORDS_OPT) ? Optional.of(commandLine.getOptionValue(KEYWORDS_OPT))
                : Optional.empty();

        licenseOpt = commandLine.hasOption(LICENSE_OPT) ? Optional.of(commandLine.getOptionValue(LICENSE_OPT))
                : Optional.empty();

        rightsOpt = commandLine.hasOption(RIGHTS_OPT) ? Optional.of(commandLine.getOptionValue(RIGHTS_OPT))
                : Optional.empty();

        ignoreWaterColumn = commandLine.hasOption(IGNORE_WATER_COLUMN_OPT);
    }

    /**
     * Performs the conversion to XSF.
     *
     * @throws NCException
     */
    @Override
    public void performConversion(File inputFile, File outputFileOrDirectory) throws IOException, NCException {

        // define parameters
        XsfConverterParameters params = new XsfConverterParameters(inputFile.getCanonicalPath(),
                outputFileOrDirectory.getCanonicalPath());
        params.setOverwrite(overwriteOpt);
        params.setIgnoreWaterColumn(ignoreWaterColumn);
        titleOpt.ifPresent(params::setTitle);
        summaryOpt.ifPresent(params::setSummary);
        keywordsOpt.ifPresent(params::setKeywords);
        licenseOpt.ifPresent(params::setLicense);
        rightsOpt.ifPresent(params::setRights);

        // perform conversion
        new SounderFileConverter(logger).convertToXsf(params);
    }

}
