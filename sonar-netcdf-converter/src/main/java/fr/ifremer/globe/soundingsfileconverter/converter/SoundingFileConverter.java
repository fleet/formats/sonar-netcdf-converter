package fr.ifremer.globe.soundingsfileconverter.converter;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ifremer.globe.api.xsf.converter.SounderFileConverter;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.TimeUnit;


/**
 * Stand alone application to convert soundings files.<br>
 * Command line arguments are :
 * <ul>
 * <li>in : File / directory to convert.</li>
 * <li>filter : extension use to filter files in directory in.</li>
 * <li>o : overwrite output file.</li>
 * <li>out : output file or directory.</li>
 * <li>fmt : conversion format. MBG by default.</li>
 *
 * <br>
 * MBG parameters:<br>
 * <li>MbgEllipsoid : Ellipsoid.</li>
 * <li>MbgShipName : Ship name.</li>
 * <li>MbgSurveyName : Survey Name.</li>
 * <li>MbgCdi : Common Data Index.</li>
 * <li>MbgReference : Reference point description.</li>
 * <li>MbgLongOffset : X sounder position.</li>
 * <li>MbgTransOffset : Y sounder position.</li>
 * <li>MbgVertOffset : vertical sounder position.</li>
 * </ul>
 */
public class SoundingFileConverter {

    /**
     * Help option.
     */
    private static final String HELP_OPTION = "h";
    /**
     * Version option.
     */
    private static final String VERSION_OPTION = "v";

    /**
     * Output format.
     */
    private static final String FORMAT_OPTION = "fmt";
    /**
     * MBG format.
     */
    private static final String FORMAT_MBG_OPTION = "MBG";
    /**
     * XSF format.
     */
    private static final String FORMAT_XSF_OPTION = "XSF";


    /**
     * Logger.
     */
    protected static Logger logger = LoggerFactory.getLogger(SoundingFileConverter.class);

    private int fileProccessedCount = 0;
    private int successCount = 0;
    private StringBuilder errorMessage;

    /**
     * Execute the command line.
     */
    public int execute(String[] args) throws ParseException {

        // Parse given arguments
        CommandLine commandLine = new DefaultParser().parse(Utils.createCommandLineOptions(), args);
        if (commandLine != null) {
            if (commandLine.hasOption(HELP_OPTION)) { // Display help if asked
                displayHelp();
                return 0;
            } else if (commandLine.hasOption(VERSION_OPTION)) { // Display version if asked
                displayVersion();
                return 0;
            } else if (commandLine.getOptions().length == 0 && commandLine.getArgs().length == 1 && commandLine.getArgs()[0].endsWith(".json")) {
                commandLine = parseConfiguration(commandLine.getArgs()[0]);
            }
            return startConversion(commandLine);
        }
        return -1;
    }

    /**
     * Parse the command line and perform the conversion.
     */
    protected int startConversion(CommandLine commandLine) throws IllegalArgumentException, ParseException {
        // File checking
        File input = Utils.checkInputFile(commandLine);
        File outputFileOrDirectory = Utils.checkOutputFileOrDirectory(commandLine);
        if (outputFileOrDirectory == null)
            outputFileOrDirectory = input.isFile() ? input.getParentFile() : input;

        // Get the right converter
        IFileConverter converter = null;

        switch (commandLine.getOptionValue(FORMAT_OPTION, FORMAT_XSF_OPTION).toUpperCase()) {
            case FORMAT_MBG_OPTION:
                converter = new FileConverterToMBG();
                break;
            case FORMAT_XSF_OPTION:
                converter = new FileConverterToXSF();
                break;
            default:
                throw new IllegalArgumentException("Format not recognized");
        }

        // Logs
        fileProccessedCount = 0;
        successCount = 0;
        errorMessage = new StringBuilder();
        long time = System.currentTimeMillis();

        logger.info("Prepare conversion process...");
        logger.info("Input: " + input.getAbsolutePath());
        logger.info("Output: " + outputFileOrDirectory.getAbsolutePath());
        logger.info("Converter: " + converter.getClass().getName());

        // Parse the command line (useful to provide specific options)
        converter.parseCommandLine(commandLine);

        // Perform conversion
        if (input.isFile()) {
            performConversion(converter, input, outputFileOrDirectory);
        } else {
            String filter = commandLine.getOptionValue(Utils.IN_FILTER_OPTION);
            for (File f : input.listFiles()) {
                if (f.isFile() && (FilenameUtils.getExtension(f.getName()).equalsIgnoreCase(filter))
                        || (filter == null && converter.canBeConverted(f.getName())))
                    performConversion(converter, f, outputFileOrDirectory);
            }
        }

        // Logs
        logger.info("Process ended: ");
        logger.info("File proccessed : " + fileProccessedCount + " -> successful: " + successCount
                + " / error(s): " + (fileProccessedCount - successCount));
        if (errorMessage.length() > 0)
            logger.error("Error(s): \n" + errorMessage);

        // Print elapsed time
        long estimatedTime = System.currentTimeMillis() - time;
        String elapsedTimeStr = String.format("%d min, %d sec", TimeUnit.MILLISECONDS.toMinutes(estimatedTime),
                estimatedTime - TimeUnit.MINUTES.toSeconds(TimeUnit.SECONDS.toMinutes(estimatedTime)));
        logger.info("Duration: " + elapsedTimeStr);
        return Math.abs(fileProccessedCount - successCount);
    }

    /**
     * Performs a conversion (and manages errors)
     */
    private void performConversion(IFileConverter converter, File inputFile, File outputFileOrDirectory) {
        // Log info
        fileProccessedCount++;
//        logger.info("Converting file: " + inputFile.getName() + " ...");
//        logger.info("Input file = " + inputFile.getAbsolutePath());
//        logger.info("Output file/directory = " + outputFileOrDirectory.getAbsolutePath());

        // Do the conversion
        try {
            converter.performConversion(inputFile, outputFileOrDirectory);
            successCount++;
        }
        // Manage exceptions coming from the converter
        catch (Exception e) {
            File destLogFile = new File(outputFileOrDirectory.getAbsolutePath() + File.separator
                    + FilenameUtils.removeExtension(inputFile.getName()) + "_error.log");

            logger.error("Error : " + e.getMessage());
            errorMessage.append("\nFile " + inputFile.getName() + " : " + e.getMessage());
            errorMessage.append("\n");
        }
    }

    /**
     * Display help.
     */
    public void displayHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("sonarnetcdf-converter [Options] [JsonConfigurationFile]", Utils.createCommandLineOptions());
    }

    /**
     * Display Globe version.
     */
    public void displayVersion() {
        System.out.println(SounderFileConverter.VERSION);
    }

    /**
     * Display usage.
     */
    public void displayUsage() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printUsage(new PrintWriter(System.out, true), 100, "xsfconverter [Options] [JsonConfigurationFile]",
                Utils.createCommandLineOptions());
    }

    /**
     * Convert the configuration file to a command line
     */
    private CommandLine parseConfiguration(String conf) throws ParseException {
        File jsonConf = new File(conf);
        if (!jsonConf.isFile())
            throw new ParseException("Configuration file not found : " + conf);

        ObjectMapper mapper = new ObjectMapper();
        try {
            ProcessArguments arguments = mapper.readValue(jsonConf, ProcessArguments.class);
            return new DefaultParser().parse(Utils.createCommandLineOptions(), arguments.toCommandLine());
        } catch (IOException e) {
            e.printStackTrace();
            throw new ParseException("Configuration file has an invalid format : " + conf);
        }
    }
}
