package fr.ifremer.globe.soundingsfileconverter.converter;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.api.xsf.converter.MbgConverterParameters;
import fr.ifremer.globe.api.xsf.converter.SounderFileConverter;
import fr.ifremer.globe.netcdf.ucar.NCException;

public class FileConverterToMBG implements IFileConverter {

	/** logger */
	protected static Logger logger = LoggerFactory.getLogger(FileConverterToMBG.class);

	/** MBG constant. */
	public static final String MBG = "Mbg";
	public static final String SHIPNAME_PARAM = MBG + "ShipName";
	public static final String SURVEYNAME_PARAM = MBG + "SurveyName";
	public static final String REFERENCE_PARAM = MBG + "Reference";
	public static final String CDI_PARAM = MBG + "Cdi";

	/** MBG parameters: retrieved from the command line **/
	private boolean overwriteOpt;
	private Optional<String> shipNameOpt;
	private Optional<String> surveyNameOpt;
	private Optional<String> referenceOpt;
	private Optional<String> cdiOpt;

	/**
	 * Check if the file can be used by the converter.
	 */
	@Override
	public boolean canBeConverted(String filename) {
		return new SounderFileConverter().accept(filename);
	}

	/**
	 * Add specific MBG options
	 */
	public static Options createCommandLineOptions(Options options) {
		options.addOption(Option.builder(SHIPNAME_PARAM).required(false).hasArg(true).desc("Ship name").build());
		options.addOption(Option.builder(SURVEYNAME_PARAM).required(false).hasArg(true).desc("Survey name").build());
		options.addOption(Option.builder(REFERENCE_PARAM).required(false).hasArg(true)
				.desc("Reference point description").build());
		options.addOption(
				Option.builder(CDI_PARAM).required(false).hasArg(true).desc("CDI (Common Data Index)").build());
		return options;
	}

	/**
	 * Parse command line to get specific options
	 */
	@Override
	public void parseCommandLine(CommandLine commandLine) throws ParseException {

		// OVERWRITE
		overwriteOpt = commandLine.hasOption(Utils.OVERWRITE_OPTION);

		// SHIPNAME
		shipNameOpt = commandLine.hasOption(SHIPNAME_PARAM)
				? Optional.of(commandLine.getOptionValue(SHIPNAME_PARAM, ""))
				: Optional.empty();
		// SURVEYNAME
		surveyNameOpt = commandLine.hasOption(SURVEYNAME_PARAM)
				? Optional.of(commandLine.getOptionValue(SURVEYNAME_PARAM, ""))
				: Optional.empty();
		// REFERENCE
		referenceOpt = commandLine.hasOption(REFERENCE_PARAM)
				? Optional.of(commandLine.getOptionValue(REFERENCE_PARAM, ""))
				: Optional.empty();
		// CDI
		cdiOpt = commandLine.hasOption(CDI_PARAM) ? Optional.of(commandLine.getOptionValue(CDI_PARAM, ""))
				: Optional.empty();

		// ELLIPSOID : no longer asked, forced to WGS84
		/*
		 * String ellipsoidValue = commandLine.getOptionValue(MBG + MbgParameters.ELLIPSOID, Ellipsoid.WGS84.name());
		 * try { Ellipsoid ellipsoid = Ellipsoid.valueOf(ellipsoidValue.toUpperCase()); logger.info("Ellipsoid : " +
		 * ellipsoid.name()); mbgParameters.set(MbgParameters.ELLIPSOID, ellipsoid.getEllipsName()); } catch
		 * (IllegalArgumentException e) { throw new ParseException("Bad value for " + MBG + MbgParameters.ELLIPSOID +
		 * " (" + ellipsoidValue + ")"); }
		 */
	}

	/**
	 * Convert the file to MBG format.
	 * 
	 * @throws IOException
	 * @throws NCException
	 */
	@Override
	public void performConversion(File inputFile, File outputFileOrDirectory) throws ParseException, IOException, NCException {
		logger.info("Launch MBG conversion...");

		// set parameters
		MbgConverterParameters mbgParameters = new MbgConverterParameters(inputFile.getAbsolutePath(),
				outputFileOrDirectory.getAbsolutePath());
		mbgParameters.setOverwrite(overwriteOpt);
		shipNameOpt.ifPresent(mbgParameters::setShipName);
		surveyNameOpt.ifPresent(mbgParameters::setSurveyName);
		referenceOpt.ifPresent(mbgParameters::setReference);
		cdiOpt.ifPresent(mbgParameters::setCdi);

		// perform conversion
		new SounderFileConverter(logger).convertToMbg(mbgParameters);
	}
}
