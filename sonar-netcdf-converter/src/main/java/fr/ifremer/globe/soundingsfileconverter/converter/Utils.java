package fr.ifremer.globe.soundingsfileconverter.converter;

import fr.ifremer.globe.api.xsf.converter.SounderFileConverter;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FilenameUtils;

import java.io.File;

public class Utils {

    /**
     * Help option.
     */
    private static final String HELP_OPTION = "h";
    /**
     * Version option.
     */
    private static final String VERSION_OPTION = "v";
    /**
     * Option File to convert.
     */
    public static final String IN_OPTION = "in";
    /**
     * Option filter for files to convert in directory.
     */
    public static final String IN_FILTER_OPTION = "filter";
    /**
     * Output file/directory option.
     */
    public static final String OUT_OPTION = "out";
    /**
     * Output format.
     */
    public static final String FORMAT_OPTION = "fmt";
    /**
     * Overwrite option.
     */
    public static final String OVERWRITE_OPTION = "o";

    /**
     * Check the presence of the in file.
     */
    protected static File checkInputFile(CommandLine commandLine) throws ParseException {
        if (!commandLine.hasOption(IN_OPTION))
            throw new ParseException("Input file/directory not specified (-" + IN_OPTION + ") ");

        String filePath = commandLine.getOptionValue(IN_OPTION);

        File result = new File(filePath);
        if (!result.exists())
            throw new ParseException("Input file/directory does not exist : " + filePath);

        return result;
    }

    /**
     * Check the output directory.
     */
    protected static File checkOutputFileOrDirectory(CommandLine commandLine) throws ParseException {
        File result = null;
        if (commandLine.hasOption(OUT_OPTION)) {
            String filePath = commandLine.getOptionValue(OUT_OPTION);
            File outputFileOrDirectory = new File(filePath);
            if(FilenameUtils.getExtension(outputFileOrDirectory.getName()).isEmpty()) {
                // Output is a directory
                if (!outputFileOrDirectory.exists() && !outputFileOrDirectory.mkdirs()) {
                    throw new ParseException("Output directory does not exist : " + filePath);
                }
                if (!outputFileOrDirectory.isDirectory()) {
                    throw new ParseException("Output path is not a directory : " + filePath);
                }
            } else {
                // Output is a file
                File parentDirectory = outputFileOrDirectory.getParentFile();
                if (!parentDirectory.exists() && !parentDirectory.mkdirs()) {
                    throw new ParseException("Output directory does not exist : " + parentDirectory.getAbsolutePath());
                }
            }
            result = outputFileOrDirectory;
        }

        return result;
    }

    /**
     * Creates command line options.
     */
    protected static Options createCommandLineOptions() {

        Options result = new Options();

        // Common options
        result.addOption(Option.builder(IN_OPTION).required(false).hasArg(true)
                .desc("Sounding file/directory to convert").build());
        result.addOption(Option.builder(IN_FILTER_OPTION).required(false).hasArg(true)
                .desc("Sounding files extension to convert").build());
        result.addOption(Option.builder(OUT_OPTION).required(false).hasArg(true)
                .desc("Destination file or directory for converted files").build());
        result.addOption(
                Option.builder(HELP_OPTION).required(false).hasArg(false).longOpt("help").desc("Help message").build());
        result.addOption(Option.builder(VERSION_OPTION).required(false).hasArg(false).longOpt("version")
                .desc(SounderFileConverter.NAME + " version").build());
        result.addOption(Option.builder(OVERWRITE_OPTION).required(false).hasArg(false).longOpt("overwrite")
                .desc("Overwrite existing files").build());
        result.addOption(Option.builder(FORMAT_OPTION).required(false).hasArg(true)
                .desc("Output format (mbg, xsf[default])").build());

        // MBG specific options
        FileConverterToMBG.createCommandLineOptions(result);

        // XSF specific options
        FileConverterToXSF.createCommandLineOptions(result);

        return result;
    }

}
