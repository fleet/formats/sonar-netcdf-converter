package fr.ifremer.globe.soundingsfileconverter.converter;

import fr.ifremer.globe.netcdf.ucar.NCException;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.ParseException;

import java.io.File;
import java.io.IOException;

public interface IFileConverter {

    /**
     * Check if the file can be used by the converter.
     */
    boolean canBeConverted(String filename);

    /**
     * Parse command line to get specific options
     */
    void parseCommandLine(CommandLine commandLine) throws ParseException;

    /**
     * Perform the conversion
     */
    void performConversion(File inputFile, File outputFileOrFolder) throws ParseException, IOException, NCException;

}
