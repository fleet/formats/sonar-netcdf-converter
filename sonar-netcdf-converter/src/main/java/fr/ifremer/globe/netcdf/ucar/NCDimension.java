package fr.ifremer.globe.netcdf.ucar;

import fr.ifremer.globe.netcdf.jna.Nc4prototypes;

public class NCDimension {

    private int dimid;
    private long length;
    private String name;
	private boolean unlimited;
    
    public NCDimension(int dimid, long length, String name) {
        this.dimid = dimid;
        this.length = length;
        this.name = name;
		this.unlimited = (length == Nc4prototypes.NC_UNLIMITED);
    }

	public NCDimension(int dimid, long length, String name, boolean unlimited) {
		this.dimid = dimid;
		this.length = length;
		this.name = name;
		this.unlimited = unlimited;
	}

    public String getName() {
        return name;
    }
    
    public int getId() {
        return dimid;
    }
    
    public long getLength() {
        return length;
    }
    
    public boolean isUnlimited() {
		return unlimited;
    }
    
    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + dimid;
		result = prime * result + (int) (length ^ (length >>> 32));
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public String toString() {
		return "NCDimension [length=" + length + ", name=" + name + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NCDimension other = (NCDimension) obj;
		if (dimid != other.dimid)
			return false;
		if (length != other.length)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}
