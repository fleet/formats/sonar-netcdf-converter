package fr.ifremer.globe.netcdf.ucar;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.jna.Pointer;
import com.sun.jna.ptr.ByteByReference;
import com.sun.jna.ptr.DoubleByReference;
import com.sun.jna.ptr.FloatByReference;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.ptr.LongByReference;
import com.sun.jna.ptr.ShortByReference;

import fr.ifremer.globe.netcdf.jna.Nc4prototypes;
import fr.ifremer.globe.netcdf.jna.Nc4prototypes.Vlen_t;
import fr.ifremer.globe.netcdf.jna.SizeT;

/**
 * Variable in a netcdf file
 */
@SuppressWarnings("unused")
public class NCVariable extends NCObject {

	protected static final Logger logger = LoggerFactory.getLogger(NCVariable.class);

	private static SizeT[] Scalar_offset = toSizeT(new long[] { 0 });
	private static SizeT[] Scalar_count = toSizeT(new long[] { 1 });
	/** Common ByteBuffer to retrieve the fill value of the variable */
	private static ByteBuffer FILL_VALUE = ByteBuffer.allocate(8);
	static {
		FILL_VALUE.order(ByteOrder.nativeOrder());
	}

	protected NCGroup group;
	private String name;

	private int type;
	private List<NCDimension> shape;

	public static final String VALID_MAX_ATT_NAME = "valid_maximum";

	public NCVariable(int ncid, NCGroup group, String name, int type, List<NCDimension> shape) {
		super(ncid);
		this.group = group;
		this.name = name;
		this.type = type;
		this.shape = shape;
		setDefaultFillValue();
	}

	/** @return the variable path */
	@Override
	public String getPath() {
		return getGroup().getPath() + '/' + getName();
	}

	/**
	 * Get the group containing the variable
	 * 
	 * @return
	 */
	public NCGroup getGroup() {
		return this.group;
	}

	@Override
	protected int getGroupId() {
		return group.getGroupId();
	}

	@Override
	protected int getVarId() {
		return ncid;
	}

	@Override
	public String toString() {

		return " NCVariable [name=" + name + "] [group=[" + group.toString() + "]";
	}

	/**
	 * Get the name of the variable
	 * 
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * Get the shape of the variable
	 * 
	 * @return
	 */
	public List<NCDimension> getShape() {
		return shape;
	}

	/**
	 * Get the data type of the variable
	 */
	public int getType() {
		return this.type;
	}

	/**
	 * Get the data type (as DataType) of the variable
	 * 
	 * @throws NCException
	 */
	public DataType getRawDataType() throws NCException {

		DataType result = DataType.getDataTypeFromNCType(getType());
		// Tests following are mandatory for variables from MBG File
		// (Example: Some mbg variables are defined as Short in MBG File but have to
		// read as UShort (mbHeading)
		// (Example: Some mbg variables are defined as Char in MBG File but have to read
		// as byte (mbReflectivity)

		if (result == DataType.CHAR && hasAttribute(VALID_MAX_ATT_NAME)
				&& getAttributeInt(VALID_MAX_ATT_NAME) <= Byte.MAX_VALUE)
			result = DataType.BYTE;
		else if (result == DataType.SHORT && hasAttribute(VALID_MAX_ATT_NAME)
				&& getAttributeInt(VALID_MAX_ATT_NAME) > Short.MAX_VALUE)
			result = DataType.USHORT;
		else if (result == DataType.INT && hasAttribute(VALID_MAX_ATT_NAME)
				&& getAttributeInt(VALID_MAX_ATT_NAME) > Integer.MAX_VALUE)
			result = DataType.UINT;
		return result;
	}

	/**
	 * convert a long[] into a SizeT[]
	 * 
	 * @param coords
	 * @return
	 */
	private static SizeT[] toSizeT(long[] coords) {
		SizeT[] p = new SizeT[coords.length];
		for (int i = 0; i < coords.length; i++) {
			p[i] = new SizeT(coords[i]);
		}
		return p;
	}

	/**
	 * convert a SizeT[] into a long[]
	 * 
	 * @param sizes
	 * @return
	 */
	private static long[] fromSizeT(SizeT[] sizes) {
		long[] p = new long[sizes.length];
		for (int i = 0; i < sizes.length; i++) {
			p[i] = sizes[i].longValue();
		}
		return p;
	}

	/**
	 * Ensure that variable is a scalar variable
	 * 
	 * @throws NCException
	 */
	private void checkScalarShape() throws NCException {
		if (!shape.isEmpty())
			throw new NCException(String.format("Invalid shape %s for variable %s, expecting to be a scalar shape",
					Arrays.toString(shape.toArray()), getName()));
	}

	/**
	 * utility function to check that the description of the subset of the variable
	 * 
	 * @param start
	 * @param count
	 * @throws NCException
	 */
	private void checkShape(long[] start, long[] count) throws NCException {
		if (!shape.isEmpty()) {
			if (start.length != shape.size() || count.length != shape.size()) {
				throw new NCException(
						String.format("Invalid shape %s for variable %s", Arrays.toString(shape.toArray()), getName()));
			}
			for (int i = 0; i < shape.size(); i++) {
				if (!shape.get(i).isUnlimited() && start[i] + count[i] > shape.get(i).getLength()) {
					throw new NCException("overflow");
				}
			}
		} else {
			if (start.length != 1 && start[0] != 0 && count.length != 1 && count[0] != 1) {
				throw new NCException(String.format(
						"Error while trying to write scalar value, invalid shape (start %s count %s) for variable %s",
						Arrays.toString(start), Arrays.toString(count), getName()));
			}
		}
	}

	/**
	 * Set the fill value for the netCDF variable.
	 * 
	 * @param v
	 * @throws NCException
	 */
	public void setDoubleFillValue(double v) throws NCException {
		check(nc4.nc_def_var_fill(group.ncid, ncid, 0, new DoubleByReference(v)));
	}

	/**
	 * @return the fill value for the netCDF variable.
	 */
	public double getDoubleFillValue() {
		var result = loadFillValue();
		return result != null ? result.getDouble() : Double.NaN; // NaN by default
	}

	/**
	 * Set the fill value for the netCDF variable.
	 * 
	 * @param v
	 * @throws NCException
	 */
	public void setIntFillValue(int v) throws NCException {
		check(nc4.nc_def_var_fill(group.ncid, ncid, 0, new IntByReference(v)));
	}

	/**
	 * @return the fill value for the netCDF variable.
	 */
	public int getIntFillValue() {
		var result = loadFillValue();
		return result != null ? result.getInt() : 0; // 0 by default
	}

	/**
	 * Set the fill value for the netCDF variable.
	 * 
	 * @param v
	 * @throws NCException
	 */
	public void setShortFillValue(short v) throws NCException {
		check(nc4.nc_def_var_fill(group.ncid, ncid, 0, new ShortByReference(v)));
	}

	/**
	 * @return the fill value for the netCDF variable.
	 */
	public short getShortFillValue() {
		var result = loadFillValue();
		return result != null ? result.getShort() : 0; // 0 by default
	}

	/**
	 * Set the fill value for the netCDF variable.
	 * 
	 * @param v
	 * @throws NCException
	 */
	public void setByteFillValue(byte v) throws NCException {
		check(nc4.nc_def_var_fill(group.ncid, ncid, 0, new ByteByReference(v)));
	}

	/**
	 * @return the fill value for the netCDF variable.
	 */
	public byte getByteFillValue() {
		var result = loadFillValue();
		return result != null ? result.get() : 0; // 0 by default
	}

	/**
	 * Set the fill value for the netCDF variable.
	 * 
	 * @param v
	 * @throws NCException
	 */
	public void setFloatFillValue(float v) throws NCException {
		check(nc4.nc_def_var_fill(group.ncid, ncid, 0, new FloatByReference(v)));
	}

	/**
	 * @return the fill value for the netCDF variable.
	 */
	public float getFloatFillValue() {
		var result = loadFillValue();
		return result != null ? result.getFloat() : Float.NaN; // NaN by default
	}

	/**
	 * Set the fill value for the netCDF variable.
	 * 
	 * @param v
	 * @throws NCException
	 */
	public void setLongFillValue(long v) throws NCException {
		check(nc4.nc_def_var_fill(group.ncid, ncid, 0, new LongByReference(v)));
	}

	/**
	 * @return the fill value for the netCDF variable.
	 */
	public long getLongFillValue() {
		var result = loadFillValue();
		return result != null ? result.getLong() : 0l; // 0 by default
	}

	/* string */
	public void put(long[] start, long[] count, String[] v) throws NCException {
		checkShape(start, count);
		check(nc4.nc_put_vara_string(group.ncid, ncid, toSizeT(start), toSizeT(count), v));
	}

	public String[] get_string(long[] start, long[] count) throws NCException {
		checkShape(start, count);
		String[] ret = new String[(int) Arrays.stream(count).reduce(1, (a, b) -> {
			return a * b;
		})];
		nc4.nc_get_vara_string(group.ncid, ncid, toSizeT(start), toSizeT(count), ret);
		return ret;
	}

	public void putScalar(String v) throws NCException {
		checkScalarShape();
		check(nc4.nc_put_vara_string(group.ncid, ncid, Scalar_offset, Scalar_count, new String[] { v }));
	}

	public String getScalar_String() throws NCException {
		checkScalarShape();
		String[] ret = new String[1];
		check(nc4.nc_get_vara_string(group.ncid, ncid, Scalar_offset, Scalar_count, ret));
		return ret[0];
	}

	/* vlen */
	public void put(long[] start, long[] count, Vlen_t[] v) throws NCException {
		checkShape(start, count);
		check(nc4.nc_put_vara(group.ncid, ncid, toSizeT(start), toSizeT(count), v));
	}

	/* byte */
	public void put(long[] start, long[] count, byte[] v) throws NCException {
		checkShape(start, count);
		check(nc4.nc_put_vara(group.ncid, ncid, toSizeT(start), toSizeT(count), v));
	}

	public void put(ByteBuffer buffer) throws NCException {
		check(nc4.nc_put_var(group.ncid, ncid, buffer));
	}

	public void put(long[] start, long[] count, ByteBuffer buffer) throws NCException {
		checkShape(start, count);
		check(nc4.nc_put_vara(group.ncid, ncid, toSizeT(start), toSizeT(count), buffer));
	}

	public void putu(long[] start, long[] count, byte[] v) throws NCException {
		checkShape(start, count);
		check(nc4.nc_put_vara_uchar(group.ncid, ncid, toSizeT(start), toSizeT(count), v));
	}

	public byte[] get_byte(long[] start, long[] count) throws NCException {
		checkShape(start, count);
		byte[] ret = new byte[(int) Arrays.stream(count).reduce(1, (a, b) -> {
			return a * b;
		})];
		check(nc4.nc_get_vara(group.ncid, ncid, toSizeT(start), toSizeT(count), ret));
		return ret;
	}

	public void get_byte(long[] start, long[] count, ByteBuffer buffer) throws NCException {
		checkShape(start, count);
		check(nc4.nc_get_vara(group.ncid, ncid, toSizeT(start), toSizeT(count), buffer));
	}

	public byte[] get_ubyte(long[] start, long[] count) throws NCException {
		checkShape(start, count);
		byte[] ret = new byte[(int) Arrays.stream(count).reduce(1, (a, b) -> {
			return a * b;
		})];
		check(nc4.nc_get_vara_uchar(group.ncid, ncid, toSizeT(start), toSizeT(count), ret));
		return ret;
	}

	public void putScalar(byte v) throws NCException {
		checkScalarShape();
		check(nc4.nc_put_vara(group.ncid, ncid, Scalar_offset, Scalar_count, new byte[] { v }));
	}

	public byte getScalar_byte() throws NCException {
		checkScalarShape();
		byte[] ret = new byte[1];
		check(nc4.nc_get_vara(group.ncid, ncid, Scalar_offset, Scalar_count, ret));
		return ret[0];
	}

	/* short */
	public void put(long[] start, long[] count, short[] v) throws NCException {
		checkShape(start, count);
		check(nc4.nc_put_vara_short(group.ncid, ncid, toSizeT(start), toSizeT(count), v));
	}

	public void putu(long[] start, long[] count, short[] v) throws NCException {
		checkShape(start, count);
		check(nc4.nc_put_vara_ushort(group.ncid, ncid, toSizeT(start), toSizeT(count), v));
	}

	public short[] get_short(long[] start, long[] count) throws NCException {
		checkShape(start, count);
		short[] ret = new short[(int) Arrays.stream(count).reduce(1, (a, b) -> {
			return a * b;
		})];
		check(nc4.nc_get_vara_short(group.ncid, ncid, toSizeT(start), toSizeT(count), ret));
		return ret;
	}

	public short[] get_ushort(long[] start, long[] count) throws NCException {
		checkShape(start, count);
		short[] ret = new short[(int) Arrays.stream(count).reduce(1, (a, b) -> {
			return a * b;
		})];
		check(nc4.nc_get_vara_ushort(group.ncid, ncid, toSizeT(start), toSizeT(count), ret));
		return ret;
	}

	public void putScalar(short v) throws NCException {
		checkScalarShape();
		check(nc4.nc_put_vara_short(group.ncid, ncid, Scalar_offset, Scalar_count, new short[] { v }));
	}

	public short getScalar_short() throws NCException {
		checkScalarShape();
		short[] ret = new short[1];
		check(nc4.nc_get_vara_short(group.ncid, ncid, Scalar_offset, Scalar_count, ret));
		return ret[0];
	}

	public void putScalarU(short v) throws NCException {
		checkScalarShape();
		check(nc4.nc_put_vara_ushort(group.ncid, ncid, Scalar_offset, Scalar_count, new short[] { v }));
	}

	public short getScalar_ushort() throws NCException {
		checkScalarShape();
		short[] ret = new short[1];
		check(nc4.nc_get_vara_ushort(group.ncid, ncid, Scalar_offset, Scalar_count, ret));
		return ret[0];
	}

	/* int */
	public void put(long[] start, long[] count, int[] v) throws NCException {
		checkShape(start, count);
		check(nc4.nc_put_vara_int(group.ncid, ncid, toSizeT(start), toSizeT(count), v));
	}

	public void putu(long[] start, long[] count, int[] v) throws NCException {
		checkShape(start, count);
		check(nc4.nc_put_vara_uint(group.ncid, ncid, toSizeT(start), toSizeT(count), v));
	}

	public int[] get_int(long[] start, long[] count) throws NCException {
		checkShape(start, count);
		int[] ret = new int[(int) Arrays.stream(count).reduce(1, (a, b) -> {
			return a * b;
		})];
		check(nc4.nc_get_vara_int(group.ncid, ncid, toSizeT(start), toSizeT(count), ret));
		return ret;
	}

	public int[] get_uint(long[] start, long[] count) throws NCException {
		checkShape(start, count);
		int[] ret = new int[(int) Arrays.stream(count).reduce(1, (a, b) -> {
			return a * b;
		})];
		check(nc4.nc_get_vara_uint(group.ncid, ncid, toSizeT(start), toSizeT(count), ret));
		return ret;
	}

	public Vlen_t[] get_vlen(long[] start, long[] count) throws NCException {
		checkShape(start, count);
		int number = (int) Arrays.stream(count).reduce(1, (a, b) -> {
			return a * b;
		});
		Vlen_t[] samples = (Vlen_t[]) new Vlen_t().toArray(number);
		check(nc4.nc_get_vara(group.ncid, ncid, toSizeT(start), toSizeT(count), samples));
		return samples;
	}

	public void free_vlen(Vlen_t[] values) throws NCException {
		check(nc4.nc_free_vlens(new SizeT(values.length), values));
	}

	public void putScalar(int v) throws NCException {
		checkScalarShape();
		check(nc4.nc_put_vara_int(group.ncid, ncid, Scalar_offset, Scalar_count, new int[] { v }));
	}

	public int getScalar_int() throws NCException {
		checkScalarShape();
		int[] ret = new int[1];
		check(nc4.nc_get_vara_int(group.ncid, ncid, Scalar_offset, Scalar_count, ret));
		return ret[0];
	}

	public void putScalarU(int v) throws NCException {
		checkScalarShape();
		check(nc4.nc_put_vara_uint(group.ncid, ncid, Scalar_offset, Scalar_count, new int[] { v }));
	}

	public int getScalar_uint() throws NCException {
		checkScalarShape();
		int[] ret = new int[1];
		check(nc4.nc_get_vara_uint(group.ncid, ncid, Scalar_offset, Scalar_count, ret));
		return ret[0];
	}

	/* long */
	public void put(long[] start, long[] count, long[] v) throws NCException {
		checkShape(start, count);
		check(nc4.nc_put_vara_longlong(group.ncid, ncid, toSizeT(start), toSizeT(count), v));
	}

	public void putu(long[] start, long[] count, long[] v) throws NCException {
		checkShape(start, count);
		check(nc4.nc_put_vara_ulonglong(group.ncid, ncid, toSizeT(start), toSizeT(count), v));
	}

	public long[] get_long(long[] start, long[] count) throws NCException {
		checkShape(start, count);
		long[] ret = new long[(int) Arrays.stream(count).reduce(1, (a, b) -> {
			return a * b;
		})];
		check(nc4.nc_get_vara_longlong(group.ncid, ncid, toSizeT(start), toSizeT(count), ret));
		return ret;
	}

	public long[] get_ulong(long[] start, long[] count) throws NCException {
		checkShape(start, count);
		long[] ret = new long[(int) Arrays.stream(count).reduce(1, (a, b) -> {
			return a * b;
		})];
		check(nc4.nc_get_vara_ulonglong(group.ncid, ncid, toSizeT(start), toSizeT(count), ret));
		return ret;
	}

	public void putScalar(long v) throws NCException {
		checkScalarShape();
		check(nc4.nc_put_vara_longlong(group.ncid, ncid, Scalar_offset, Scalar_count, new long[] { v }));
	}

	public void putScalarU(long v) throws NCException {
		checkScalarShape();
		check(nc4.nc_put_vara_ulonglong(group.ncid, ncid, Scalar_offset, Scalar_count, new long[] { v }));
	}

	public long getScalar_long() throws NCException {
		checkScalarShape();
		long[] ret = new long[1];
		check(nc4.nc_get_vara_longlong(group.ncid, ncid, Scalar_offset, Scalar_count, ret));
		return ret[0];
	}

	public long getScalar_ulong() throws NCException {
		checkScalarShape();
		long[] ret = new long[1];
		check(nc4.nc_get_vara_ulonglong(group.ncid, ncid, Scalar_offset, Scalar_count, ret));
		return ret[0];
	}
	/* float */

	public void put(long[] start, long[] count, float[] v) throws NCException {
		checkShape(start, count);
		check(nc4.nc_put_vara_float(group.ncid, ncid, toSizeT(start), toSizeT(count), v));
	}

	public float[] get_float(long[] start, long[] count) throws NCException {
		checkShape(start, count);
		float[] ret = new float[(int) Arrays.stream(count).reduce(1, (a, b) -> {
			return a * b;
		})];
		check(nc4.nc_get_vara_float(group.ncid, ncid, toSizeT(start), toSizeT(count), ret));
		return ret;
	}

	public void putScalar(float v) throws NCException {
		checkScalarShape();
		check(nc4.nc_put_vara_float(group.ncid, ncid, Scalar_offset, Scalar_count, new float[] { v }));
	}

	public float getScalar_float() throws NCException {
		checkScalarShape();
		float[] ret = new float[1];
		check(nc4.nc_get_vara_float(group.ncid, ncid, Scalar_offset, Scalar_count, ret));
		return ret[0];
	}

	/* double */
	public void put(long[] start, long[] count, double[] v) throws NCException {
		checkShape(start, count);
		check(nc4.nc_put_vara_double(group.ncid, ncid, toSizeT(start), toSizeT(count), v));
	}

	public double[] get_double(long[] start, long[] count) throws NCException {
		checkShape(start, count);
		double[] ret = new double[(int) Arrays.stream(count).reduce(1, (a, b) -> {
			return a * b;
		})];
		check(nc4.nc_get_vara_double(group.ncid, ncid, toSizeT(start), toSizeT(count), ret));
		return ret;
	}

	public void putScalar(double v) throws NCException {
		checkScalarShape();
		check(nc4.nc_put_vara_double(group.ncid, ncid, Scalar_offset, Scalar_count, new double[] { v }));
	}

	public double getScalar_double() throws NCException {
		checkScalarShape();
		double[] ret = new double[1];
		check(nc4.nc_get_vara_double(group.ncid, ncid, Scalar_offset, Scalar_count, ret));
		return ret[0];
	}

	/**
	 * @see fr.ifremer.globe.netcdf.ucar.NCObject#getAttributeCount()
	 */
	public int getAttributeCount() throws NCException {
		byte[] name = new byte[Nc4prototypes.NC_MAX_NAME + 1];
		IntByReference nDims = new IntByReference();
		IntByReference type = new IntByReference();
		IntByReference nAttrs = new IntByReference();
		check(nc4.nc_inq_var(getGroupId(), ncid, name, type, nDims, (Pointer) null, nAttrs));

		return nAttrs.getValue();
	}

	public static final int DEFLATE_LEVEL = 4;
	public static final long DEFAULT_CHUNKSIZE = 1 << 20; // 1MB

	public int getCompressionLevel() throws NCException {
		IntByReference shuffle = new IntByReference();
		IntByReference compression = new IntByReference();
		IntByReference comp_level = new IntByReference();
		check(nc4.nc_inq_var_deflate(group.ncid, ncid, shuffle, compression, comp_level));
		return compression.getValue() > 0 ? comp_level.getValue() : 0;
	}

	public void setCompression() throws NCException {
		check(nc4.nc_def_var_deflate(group.ncid, ncid, 1, 1, DEFLATE_LEVEL));
	}

	/**
	 * define chunking dividing the first dimension only. It allows to write pings
	 * incrementally more efficiently.
	 */
	public void setChunkingWithFirstDimension() throws NCException {
		SizeT[] chunksize = new SizeT[shape.size()];
		if (!shape.get(0).isUnlimited() && shape.get(0).getLength() > 0) {
			long firstDimSize = shape.get(0).getLength();
			long additionnalDimSize = 1;
			for (int i = 1; i < shape.size(); i++) {
				chunksize[i] = new SizeT(shape.get(i).getLength());
				additionnalDimSize *= shape.get(i).getLength();
			}
			// Retrieve the minimum number of chunk to ensure maxChunksize < 1MB
			long minChunkNumber = Math.max(1, (long)Math.ceil(firstDimSize*additionnalDimSize * 1.0 / DEFAULT_CHUNKSIZE));
			// Deduce the best size for the first dim
			long firstDimMaxSize = (long) Math.ceil(firstDimSize * 1.0 / minChunkNumber);
			chunksize[0] = new SizeT(firstDimMaxSize);

			check(nc4.nc_def_var_chunking(group.ncid, ncid, Nc4prototypes.NC_CHUNKED, chunksize));
		}
	}

	public long[] getChunkSizes() throws NCException {
		IntByReference storagep = new IntByReference();
		SizeT[] chunksizes = new SizeT[shape.size()];
		check(nc4.nc_inq_var_chunking(group.ncid, ncid, storagep, chunksizes));
		return fromSizeT(chunksizes);
	}

	/**
	 * @return the fill value for the netCDF variable.
	 */
	protected ByteBuffer loadFillValue() {
		var noFillMode = new IntByReference(0);
		FILL_VALUE.clear();
		Arrays.fill(FILL_VALUE.array(), (byte) 0);
		if (0 == nc4.nc_inq_var_fill(group.ncid, ncid, noFillMode, FILL_VALUE.array())) {
			// Mode == 0 => No fill value defined
			if (noFillMode.getValue() == 1) {
				return null;
			} else {
				FILL_VALUE.rewind();
			}
		}
		return FILL_VALUE;
	}

	/**
	 * Set a default FillValue on this variable depending on the type.
	 */
	protected void setDefaultFillValue() {
		try {
			// Don't use DefaultFillValue for classic file (MBG...)
			if (group == null || !group.isWithFillValue()) {
				// File is read only.
				return;
			}
			switch (getType()) {
			case Nc4prototypes.NC_BYTE:
				setByteFillValue(Byte.MIN_VALUE);
				break;
			case Nc4prototypes.NC_CHAR:
				setByteFillValue(Byte.MAX_VALUE);
				break;
			case Nc4prototypes.NC_SHORT:
				setShortFillValue(Short.MIN_VALUE);
				break;
			case Nc4prototypes.NC_INT:
				setIntFillValue(Integer.MIN_VALUE);
				break;
			case Nc4prototypes.NC_INT64:
				setLongFillValue(Long.MIN_VALUE);
				break;
			case Nc4prototypes.NC_FLOAT:
				setFloatFillValue(Float.NaN);
				break;
			case Nc4prototypes.NC_DOUBLE:
				setDoubleFillValue(Double.NaN);
				break;
			case Nc4prototypes.NC_USHORT:
				setIntFillValue(0xFFFF);
				break;
			case Nc4prototypes.NC_UBYTE:
				setShortFillValue((short) 0xFF);
				break;
			case Nc4prototypes.NC_UINT:
				setLongFillValue((long) 0xFFFFFFFF);
				break;
			case Nc4prototypes.NC_UINT64:
				// No possible FillValue for this type
				break;
			default: // STRING, Enum...
				if (group.isEnum(getType())) {
					setByteFillValue(Byte.MIN_VALUE);
				}
				break;
			}
		} catch (NCException e) {
			logger.error("Unable to set a default fill_value to NC variable {}", getName());
		}
	}

}
