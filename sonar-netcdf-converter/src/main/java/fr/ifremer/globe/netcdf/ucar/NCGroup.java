package fr.ifremer.globe.netcdf.ucar;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.sun.jna.Pointer;
import com.sun.jna.ptr.IntByReference;

import fr.ifremer.globe.netcdf.jna.Nc4Iosp;
import fr.ifremer.globe.netcdf.jna.Nc4prototypes;
import fr.ifremer.globe.netcdf.jna.SizeT;
import fr.ifremer.globe.netcdf.jna.SizeTByReference;
import fr.ifremer.globe.netcdf.util.Pair;

/**
 * Group in a netcdf file
 */
public class NCGroup extends NCObject {

	@Override
	public String toString() {
		return "NCGroup [name=" + name + "] [path=" + getPath() + "]";
	}

	final NCGroup parent;
	final String name;

	// cache of already declared groups
	protected Map<String, NCGroup> groups;
	// cache of created dims
	private Map<String, NCDimension> dims;
	// cache of creaded variables
	protected Map<String, NCVariable> variables;

	/**
	 * Build a ncgroup.
	 * 
	 * @param grpid  underlyind id
	 * @param name   name of the group
	 * @param parent parent of the group (null if the group is the file)
	 */
	protected NCGroup(int grpid, String name, NCGroup parent) {
		super(grpid);
		this.name = name;
		this.parent = parent;
		dims = new HashMap<>();
		variables = new HashMap<>();
		groups = new HashMap<>();
	}

	/**
	 * Build a new group in the specified parent
	 * 
	 * @param name
	 * @param parent
	 * @throws NCException
	 */
	protected NCGroup(String name, NCGroup parent) throws NCException {

		if (parent.groups.containsKey(name))
			throw new NCException("Group name already used");

		if (name.trim().isEmpty()) {
			// match the root group
			this.ncid = parent.ncid;
		} else {
			IntByReference gid = new IntByReference();
			check(Nc4Iosp.getCLibrary().nc_def_grp(parent.ncid, name, gid));
			this.ncid = gid.getValue();

		}

		this.name = name;
		this.parent = parent;
		this.parent.addGroup(this);
		dims = new HashMap<>();
		variables = new HashMap<>();
		groups = new HashMap<>();
	}

	@Override
	public int getGroupId() {
		return ncid;
	}

	@Override
	protected int getVarId() {
		return Nc4prototypes.NC_GLOBAL;
	}

	/**
	 * Return the parent of the current group
	 * 
	 * @return
	 */
	public NCGroup getParent() {
		return this.parent;
	}

	/**
	 * Create a subgroup
	 * 
	 * @param name The name of the subgroup to create
	 * @return the newly created group
	 * @throws Exception
	 */
	public NCGroup addGroup(String name) throws NCException {
		if (!groups.containsKey(name)) {
			IntByReference gid = new IntByReference();
			check(Nc4Iosp.getCLibrary().nc_def_grp(this.ncid, name, gid));
			NCGroup grp = new NCGroup(gid.getValue(), name, this);
			groups.put(name, grp);
		}
		return groups.get(name);
	}

	public NCGroup addGroup(NCGroup grp) {
		if (!groups.containsKey(grp.name)) {
			groups.put(grp.name, grp);
		}
		return groups.get(grp.name);
	}

	/**
	 * read child groups from the underlying file
	 * 
	 * @return
	 * @throws NCException
	 */
	public List<? extends NCGroup> getGroups() throws NCException {
		if (groups.size() != 0) {
			return List.copyOf(groups.values());
		}
		IntByReference ngrps = new IntByReference();
		check(nc4.nc_inq_grps(ncid, ngrps, (Pointer) null));
		int[] gcids = new int[ngrps.getValue()];
		List<NCGroup> ret = new ArrayList<NCGroup>(ngrps.getValue());
		check(nc4.nc_inq_grps(ncid, ngrps, gcids));

		for (int gid : gcids) {
			byte[] name = new byte[Nc4prototypes.NC_MAX_NAME + 1];
			check(nc4.nc_inq_grpname(gid, name));
			String grpName = makeString(name);
			NCGroup child = new NCGroup(gid, makeString(name), this);
			groups.put(grpName, child);
			ret.add(child);
		}

		return ret;
	}

	/**
	 * Retrieve an existing group
	 * 
	 * @param name
	 * @return
	 * @throws NCException
	 */
	public NCGroup getGroup(String name) throws NCException {
		if (groups.size() == 0) {
			// force a read from the underlying netcdf file
			getGroups();
		}

		// force a parsing if group name contains hierachy
		String[] paths = name.split("/");

		return recurseGetGroup(paths);
	}

	/**
	 * Find a group recursively
	 * 
	 * @param path group hierarchy nodes
	 * @throws NCException
	 */
	private NCGroup recurseGetGroup(String[] path) throws NCException {
		if (groups.size() == 0)
			getGroups();

		if (path.length == 1) {
			// terminal node, just return it
			return groups.get(path[0]);
		}
		if (path.length > 1) {
			String[] newpath = Arrays.copyOfRange(path, 1, path.length);
			if (path[0].trim().isEmpty()) {
				return recurseGetGroup(newpath);
			} else {
				NCGroup g = groups.get(path[0]);
				if (g == null)
					return null;
				else
					return g.recurseGetGroup(newpath);
			}

		}
		return null;
	}

	/**
	 * Create a new dimension
	 * 
	 * @param name
	 * @param len
	 * @return
	 * @throws NCException
	 */
	public NCDimension addDimension(String name, long len) throws NCException {
		if (len < 0) {
			throw new RuntimeException("Please, check declared dimension which = 0 !");
		}
		// IF len is == 0, this means that dimension is an unlimited dimension, data can
		// be happened to the associated variables
		// Check if the dim have already been declared
		if (dims.containsKey(name)) {
			NCDimension dim = dims.get(name);
			if (dim.getLength() == len) {
				return dim;
			} else {
				throw new NCException("Dimension '" + name + "' already exists with same name but different length ("
						+ len + "≠" + dim.getLength() + ")");
			}
		} else {
			SizeT slen = new SizeT(len);
			IntByReference dimId = new IntByReference();
			check(nc4.nc_def_dim(ncid, name, slen, dimId));
			NCDimension dim = new NCDimension(dimId.getValue(), len, name);
			dims.put(name, dim);
			return dim;
		}
	}

	/**
	 * read the dimensions from the file
	 * 
	 * @return
	 * @throws NCException
	 */
	public List<NCDimension> getDimensions() throws NCException {
		List<NCDimension> ret = new LinkedList<>();
		IntByReference ndims = new IntByReference();
		check(nc4.nc_inq_ndims(ncid, ndims));
		int[] dimids = new int[ndims.getValue()];
		check(nc4.nc_inq_dimids(ncid, ndims, dimids, 0));

		for (int dimid : dimids) {
			byte[] name = new byte[Nc4prototypes.NC_MAX_NAME + 1];
			SizeTByReference len = new SizeTByReference();
			check(nc4.nc_inq_dim(ncid, dimid, name, len));

			NCDimension dim = new NCDimension(dimid, len.getValue().longValue(), makeString(name));
			dims.put(dim.getName(), dim);
			ret.add(dim);
		}
		return ret;
	}

	/**
	 * Get an existing dimension by name
	 * 
	 * @param name
	 * @return
	 * @throws NCException
	 */
	public NCDimension getDimension(String name) throws NCException {
		if (dims.size() == 0) {
			getDimensions();
		}
		NCDimension res = dims.get(name);

		if (parent != null && res == null) {
			return parent.getDimension(name);
		}

		return res;
	}

	public NCVariable addVariable(String name, DataType type, List<NCDimension> dimensions, String comName,
			String longName) throws NCException {
		return addVariable(name, DataType.getNCType(type), dimensions, comName, longName);
	}

	public NCVariable addVariable(String name, DataType type, List<NCDimension> dimensions, String comName,
			String longName, String unit) throws NCException {
		return addVariable(name, DataType.getNCType(type), dimensions, comName, longName, unit);
	}

	public NCVariable addVariable(String name, DataType type, List<NCDimension> dimensions) throws NCException {
		return addVariable(name, DataType.getNCType(type), dimensions);
	}

	public NCVariable addVariable(String name, byte type, List<NCDimension> dimensions, String comName, String longName)
			throws NCException {
		return addVariable(name, (int) type, dimensions, comName, longName);
	}

	public NCVariable addVariable(String name, int type, List<NCDimension> dimensions, String comName, String longName)
			throws NCException {
		NCVariable v = addVariable(name, type, dimensions);
		v.addAttribute("name", comName);
		v.addAttribute("long_name", longName);
		return v;
	}

	public NCVariable addVariable(String name, byte type, List<NCDimension> dimensions) throws NCException {
		return addVariable(name, (int) type, dimensions);
	}

	public NCVariable addVariable(String name, int type, List<NCDimension> dimensions, String comName, String longName,
			String unit) throws NCException {
		NCVariable v = addVariable(name, type, dimensions, comName, longName);
		v.addAttribute("unit", unit);
		return v;
	}

	/**
	 * Create a new variable
	 * 
	 * @param name
	 * @param type
	 * @param shape
	 * @return
	 * @throws NCException
	 */
	public NCVariable addVariable(String name, int type, List<NCDimension> shape) throws NCException {
		if (variables.containsKey(name)) {
			NCVariable theVar = variables.get(name);

			// check for shape
			if (theVar.getShape().size() != shape.size()) {
				throw new NCException("Trying to re-create the variable " + name + " with incompatible shape");
			}
			Iterator<NCDimension> requestedIt = shape.iterator();
			Iterator<NCDimension> existingIt = theVar.getShape().iterator();
			while (requestedIt.hasNext()) {
				NCDimension thisDim = existingIt.next();
				NCDimension newDim = requestedIt.next();
				if (thisDim == null || !thisDim.equals(newDim)) {
					throw new NCException("Trying to create a variable with different dimensions: " + thisDim.getName()
							+ " != " + newDim.getName());
				}
			}

			// check for type
			if (type != theVar.getType()) {
				throw new NCException("Trying to redefine variable " + name + " with different data type");
			}

			return theVar;
		} else {
			int ndims = shape.size();

			int[] dimIds = null;
			if (ndims >= 0)
				dimIds = new int[shape.size()];
			for (int i = 0; i < shape.size(); i++) {
				if (shape.get(i) == null)
					throw new NullPointerException("Error while defining variable " + name);
				dimIds[i] = shape.get(i).getId();
			}
			IntByReference varId = new IntByReference();

			check(nc4.nc_def_var(ncid, name, new SizeT(type), ndims, dimIds, varId));
			NCVariable var = new NCVariable(varId.getValue(), this, name, type, shape);
			if (!isCompressionDisabled() && (ndims > 0)) {
				var.setCompression();
				var.setChunkingWithFirstDimension();
			}
			variables.put(name, var);
			return var;
		}
	}

	public NCVariable addEnumVariableByte(String name, List<NCDimension> shape, List<Pair<Byte, String>> labels,
			String shortName, String longName) throws NCException {
		// TODO handle caching of enum variables
		int ndims = shape.size();
		int[] dimIds = new int[shape.size()];
		for (int i = 0; i < shape.size(); i++) {
			dimIds[i] = shape.get(i).getId();
		}
		IntByReference varId = new IntByReference();

		IntByReference typeId = new IntByReference();
		check(nc4.nc_def_enum(ncid, Nc4prototypes.NC_UBYTE, name + "_enum", typeId));
		check(nc4.nc_def_var(ncid, name, new SizeT(typeId.getValue()), ndims, dimIds, varId));

		NCVariable var = new NCVariable(varId.getValue(), this, name, typeId.getValue(), shape);
		if (!isCompressionDisabled() && (ndims > 0)) {
			var.setCompression();
			var.setChunkingWithFirstDimension();
		}
		for (Pair<Byte, String> label : labels) {
			check(nc4.nc_insert_enum(ncid, typeId.getValue(), label.getSecond(), new IntByReference(label.getFirst())));
		}

		var.addAttribute("name", shortName);
		var.addAttribute("long_name", longName);
		variables.put(name, var);

		return var;
	}

	/**
	 * read the variables defined in the current group
	 * 
	 * @return
	 * @throws NCException
	 */
	public List<? extends NCVariable> getVariables() throws NCException {
		IntByReference nVars = new IntByReference();
		check(nc4.nc_inq_varids(ncid, nVars, null));
		List<NCVariable> ret = new ArrayList<>(nVars.getValue());
		int[] ids = new int[nVars.getValue()];
		check(nc4.nc_inq_varids(ncid, nVars, ids));

		for (int i = 0; i < nVars.getValue(); i++) {
			byte[] name = new byte[Nc4prototypes.NC_MAX_NAME + 1];
			IntByReference nDims = new IntByReference();
			IntByReference type = new IntByReference();
			IntByReference nAttrs = new IntByReference();
			check(nc4.nc_inq_var(ncid, ids[i], name, type, nDims, (Pointer) null, nAttrs));
			int[] dimids = new int[nDims.getValue()];
			check(nc4.nc_inq_var(ncid, ids[i], name, type, nDims, dimids, nAttrs));

			List<NCDimension> shape = new ArrayList<>(nDims.getValue());
			for (int j = 0; j < nDims.getValue(); j++) {
				byte[] dimName = new byte[Nc4prototypes.NC_MAX_NAME + 1];
				SizeTByReference dimSize = new SizeTByReference();

				check(nc4.nc_inq_dim(ncid, dimids[j], dimName, dimSize));
				shape.add(new NCDimension(dimids[j], dimSize.getValue().longValue(), makeString(dimName)));
			}
			NCVariable var = new NCVariable(ids[i], this, makeString(name), type.getValue(), shape);
			variables.put(var.getName(), var);
			ret.add(var);
		}
		return ret;
	}

	/**
	 * Get an existing variable
	 * 
	 * @param name
	 * @return
	 * @throws NCException
	 */
	public NCVariable getVariable(String name) throws NCException {
		if (variables.size() == 0) {
			getVariables();
		}
		return variables.get(name);
	}

	public String getName() {
		return name;
	}

	/**
	 * @see fr.ifremer.globe.netcdf.ucar.NCObject#getAttributeCount()
	 */
	@Override
	public int getAttributeCount() throws NCException {
		IntByReference nAtts = new IntByReference();
		check(nc4.nc_inq_natts(ncid, nAtts));
		return nAtts.getValue();
	}

	public String getPath() {
		if (this.getParent() != null) {
			return this.getParent().getPath() + "/" + name;
		} else
			return "/" + name;
	}

	/**
	 * Map storing declared types
	 */
	Map<String, IntByReference> types = new HashMap<>();
	Map<String, IntByReference> enums = new HashMap<>();

	/**
	 * declare a new netcdf type
	 * 
	 * @param name      the variable name
	 * @param base_type the type of data as defined in Nc4prototypes, for example
	 *                  Nc4prototypes.NC_BYTE
	 * @throws NCException
	 **/
	public void addType(String name, int base_type) throws NCException {
		IntByReference typeIdRef = new IntByReference();
		check(NCObject.nc4.nc_def_vlen(ncid, name, base_type, typeIdRef));
		types.put(name, typeIdRef);
	}

	/**
	 * recurse ascending hierarchy to find a type
	 * 
	 * @throws NCException if was not previously declared in hierarchy
	 */
	public IntByReference getType(String key) throws NCException {
		if (types.containsKey(key))
			return types.get(key);
		if (parent == null)
			throw new NCException(String.format("Using %s type but it was not declared in groups hierarchy", key));
		return parent.getType(key);
	}

	public IntByReference addEnumByte(String name, List<Pair<Byte, String>> labels) throws NCException {

		IntByReference typeId = new IntByReference();
		check(nc4.nc_def_enum(ncid, Nc4prototypes.NC_BYTE, name, typeId));

		for (Pair<Byte, String> label : labels) {
			check(nc4.nc_insert_enum(ncid, typeId.getValue(), label.getSecond(), new IntByReference(label.getFirst())));
		}

		this.enums.put(name, typeId);

		return typeId;
	}

	public IntByReference getEnum(String name) throws NCException {
		if (enums.containsKey(name))
			return enums.get(name);
		if (parent == null)
			throw new NCException(String.format("Using %s type but it was not declared in groups hierarchy", name));
		return parent.getEnum(name);

	}

	/**
	 * @returns true if the specified type matches an enumerate declared on the
	 *          hierarchy of this group
	 */
	public boolean isEnum(int type) {
		for (var enumId : enums.values())
			if (enumId.getValue() == type)
				return true;
		if (parent != null)
			return parent.isEnum(type);
		return false;
	}

	/** @returns true if the file can manage the FillValue attribute */
	public boolean isWithFillValue() {
		return parent != null && parent.isWithFillValue();
	}

	/** @returns true if compression should be disabled for group's variables */
	public boolean isCompressionDisabled() {
		return parent != null && parent.isCompressionDisabled();
	}

}
