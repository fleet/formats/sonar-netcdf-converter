package fr.ifremer.globe.netcdf.ucar;

/**
 * Exception related to the NetCDF API.
 */
public class NCException extends Exception {

    /**
     * auto generated
     */
    private static final long serialVersionUID = -2843305429764362273L;


    /**
     * Instanciate a NCException
     *
     * @param message message associated with the exception
     */
    public NCException(String message) {
        super(message);
    }

    public NCException(String message, Exception e) {
        super(message, e);
    }
}
