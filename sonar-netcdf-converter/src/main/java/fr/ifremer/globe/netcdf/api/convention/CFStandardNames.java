package fr.ifremer.globe.netcdf.api.convention;

public class CFStandardNames {
	/**
	 * private constructor for utility classes
	 */
	private CFStandardNames() {

	}

	/* CF: http://cf-pcmdi.llnl.gov/documents/cf-conventions/1.5/cf-conventions.html */
	/* NUG: http://www.unidata.ucar.edu/software/netcdf/docs/netcdf.html#Variables */
	public static final String CF_STD_NAME = "standard_name";
	public static final String CF_LNG_NAME = "long_name";
	public static final String CF_UNITS = "units";
	public static final String CF_ADD_OFFSET = "add_offset";
	public static final String CF_SCALE_FACTOR = "scale_factor";
	public static final String CF_MISSING_VALUE = "missing_value";
	public static final String CF_FILL_VALUE = "_FillValue";
	public static final String CF_VALID_MIN = "valid_min";
	public static final String CF_VALID_MAX = "valid_max";

	/* should be SRS_UL_METER but use meter now for compat with gtiff files */
	public static final String CF_UNITS_M = "meter";
	public static final String CF_UNITS_D = "degree";
	public static final String CF_PROJ_X_VAR_NAME = "x";
	public static final String CF_PROJ_Y_VAR_NAME = "y";
	public static final String CF_PROJ_X_COORD = "projection_x_coordinate";
	public static final String CF_PROJ_Y_COORD = "projection_y_coordinate";
	public static final String CF_PROJ_X_COORD_LONG_NAME = "x coordinate of projection";
	public static final String CF_PROJ_Y_COORD_LONG_NAME = "y coordinate of projection";
	public static final String CF_GRD_MAPPING_NAME = "grid_mapping_name";
	public static final String CF_GRD_MAPPING = "grid_mapping";
	public static final String CF_COORDINATES = "coordinates";

	public static final String CF_LONGITUDE_VAR_NAME = "lon";
	public static final String CF_LONGITUDE_STD_NAME = "longitude";
	public static final String CF_LONGITUDE_LNG_NAME = "longitude";
	public static final String CF_LATITUDE_VAR_NAME = "lat";
	public static final String CF_LATITUDE_STD_NAME = "latitude";
	public static final String CF_LATITUDE_LNG_NAME = "latitude";
	public static final String CF_DEGREES_NORTH = "degrees_north";
	public static final String CF_DEGREES_EAST = "degrees_east";

	public static final String CF_AXIS = "axis";
}
