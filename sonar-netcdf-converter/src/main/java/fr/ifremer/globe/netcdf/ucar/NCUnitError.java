package fr.ifremer.globe.netcdf.ucar;

public class NCUnitError extends NCException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	public NCUnitError(String message) {
		super(message);
	}

	public NCUnitError(String message, Exception e) {
		super(message, e);
	}

}
