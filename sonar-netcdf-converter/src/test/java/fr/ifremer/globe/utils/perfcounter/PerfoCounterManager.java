package fr.ifremer.globe.utils.perfcounter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Observable;

/**
 * class managing perfocounter. This class use a singleton pattern and is
 * responsible for creation and managing of all perfocounter.
 * 
 * */
public class PerfoCounterManager extends Observable {
	static PerfoCounterManager instance = null;

	/**
	 * return the singleton
	 * */
	public static PerfoCounterManager getInstance() {
		if (instance == null) {
			instance = new PerfoCounterManager();
		}
		return instance;
	}

	private PerfoCounterManager() {
		mapKey2Index = new HashMap<String, Integer>();
		counters = new ArrayList<PerfoCounterEntry>();
	}

	/**
	 * Array containing PerfoCounterEntry
	 * */
	ArrayList<PerfoCounterEntry> counters;
	/**
	 * map matching key and index in the array
	 * */
	HashMap<String, Integer> mapKey2Index;

	PerfoCounterEntry getPerfoCounter(String key) {
		Integer entry = mapKey2Index.get(key);
		if (entry == null) {
			return createEntry(key);
		} else {
			return counters.get(entry);
		}
	}

	PerfoCounterEntry getPerfoCounter(int index) {
		return counters.get(index);
	}

	public List<PerfoCounterEntry> getPerfoCounterAsList() {
		return Collections.unmodifiableList(counters);
	}

	private PerfoCounterEntry createEntry(String key) {
		PerfoCounterEntry entry = new PerfoCounterEntry(key);
		mapKey2Index.put(key, counters.size());
		counters.add(entry);
		setChanged();
		notifyObservers();
		return entry;
	}
}
