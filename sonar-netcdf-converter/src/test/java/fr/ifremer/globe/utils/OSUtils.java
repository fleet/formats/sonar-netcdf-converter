package fr.ifremer.globe.utils;

/**
 * Utility methods to deal with OS specific behaviors.
 * 
 * @author gbourel
 */
public class OSUtils {

	/** Enumeration of known OS. */
	public enum Platform {
		Windows, Mac, Unix, Solaris, Unknown
	}

	/** Lazy intialized current OS. */
	private static Platform currentOS = null;

	public static Platform getOS() {
		if (currentOS == null) {
			String os = System.getProperty("os.name").toLowerCase();
			currentOS = Platform.Unknown;
			if (os != null) {
				if (os.indexOf("win") >= 0) {
					currentOS = Platform.Windows; // Windows
				}
				if (os.indexOf("mac") >= 0) {
					currentOS = Platform.Mac; // Mac
				}
				if (os.indexOf("nux") >= 0) {
					currentOS = Platform.Unix; // Linux
				}
				if (os.indexOf("nix") >= 0) {
					currentOS = Platform.Unix; // Unix
				}
				if (os.indexOf("sunos") >= 0) {
					currentOS = Platform.Solaris; // Solaris
				}
			}
		}

		return currentOS;
	}

	public static boolean isWindows() {
		return Platform.Windows.equals(getOS());
	}

	public static boolean isMac() {
		return Platform.Mac.equals(getOS());
	}

	public static boolean isUnix() {
		return Platform.Unix.equals(getOS());
	}

	public static boolean isSolaris() {
		return Platform.Solaris.equals(getOS());
	}
}
