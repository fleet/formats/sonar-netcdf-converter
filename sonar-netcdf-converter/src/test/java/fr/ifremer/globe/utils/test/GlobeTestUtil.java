package fr.ifremer.globe.utils.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.utils.OSUtils;

/**
 * This class will determine where is located the data test files
 * This location is determined, on order of priority by : <p>
 * the {@link #ENV_TESTDATA_DIR} environnement variable <p>
 * the {@link #USER_TXT_FILE} file in the user directory <p>
 * by default the current directory will be used
 * */
public class GlobeTestUtil {
	/**
	 * Nom de la variable d'environnement pour la configuration du répertoire
	 * des données de test.
	 */
	public final static String ENV_TESTDATA_DIR = "V3D_TESTDATA_DIR";

	/**
	 * Nom de la fichier d'environnement pour la configuration du répertoire
	 * des données de test.
	 */
	public final static String USER_TXT_FILE = ".GLOBE_TESTDATADIR.txt";


	private static Logger logger = LoggerFactory.getLogger(GlobeTestUtil.class);

	private static String path = null;

	/**
	 * Renvoi le chemin contenant les données de tests. Ce chemin peut être
	 * paramétré en utilisant la variable d'environnement 3DV_TESTDATA_DIR
	 */
	public static String getTestDataPath() {
		if (path == null) {
			path = initPath();
			logger.info("Using " + path + " as a test directory");
		}

		return path;
	}

	private static String initPath() {
		path = System.getenv(ENV_TESTDATA_DIR);
		if (path != null)
			return path;

		if(OSUtils.isWindows())
		{
			path=readFile("c:\\");
		} else
		{
			path=readFile("\\");
		}
		if(path!=null)
			return path;
		String userDir=System.getProperty("user.home");
		if(userDir!=null)
		{
			path=readFile(userDir);
		}
		//default case
		if(path==null)
			path = "./";
		return path;
	}

	/**
	 * Read the file containing the path definition
	 * @return the path or null if the file does not exist
	 * */
	private static String readFile(String userDir)
	{
		String path=null;
		Path file=Paths.get(userDir,USER_TXT_FILE);

		if(Files.exists(file))
		{
			try (BufferedReader reader = Files.newBufferedReader(file)) {
				String line = null;
				while ((line = reader.readLine()) != null) {
					if(Files.exists(Paths.get(line)))
					{
						path=line;
					}

				}
			} catch (IOException x) {
				logger.error("Error while reading file "+file.getFileName() + " " +x.getMessage());
			}

		} else 
		{
			logger.info("File does not exist"+file.getFileName());
		}
		return path;
	}
}
