/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.utils.array.impl;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.utils.array.IArray;

/**
 * {@link IArray} using a HeapByteBuffer to store primitives.
 * 
 * @author Apside
 *
 */
public class TinyArray extends AbstractByteBufferArray {

	/** Le logger. */
	protected static Logger LOGGER = LoggerFactory.getLogger(TinyArray.class);

	/** ByteBuffer to store the values. */
	protected final ByteBuffer byteBuffer;

	/**
	 * Wrap a ByteBuffer into a TinyArray.
	 *
	 * @param buffer
	 *            Wrapped buffer.
	 * @param sizeOfOneElement
	 *            Size of one stored primitive.
	 */
	public TinyArray(ByteBuffer buffer, int sizeOfOneElement) {
		super(buffer.limit() / sizeOfOneElement, sizeOfOneElement);
		this.byteBuffer = buffer;
	}

	/**
	 * Wrap a ByteBuffer into a TinyArray.
	 *
	 * @param buffer
	 *            Wrapped buffer.
	 * @param sizeOfOneElement
	 *            Size of one stored primitive.
	 * @param colCount
	 *            number of elements by row.
	 */
	public TinyArray(ByteBuffer buffer, int sizeOfOneElement, int colCount) {
		super(buffer.limit() / sizeOfOneElement, sizeOfOneElement, colCount);
		this.byteBuffer = buffer;
	}

	/**
	 * Wrap a ByteBuffer into a TinyArray.
	 *
	 * @param buffer
	 *            Wrapped buffer.
	 * @param sizeOfOneElement
	 *            Size of one stored primitive.
	 * @param yCount
	 *            number of elements int the y axis.
	 * @param zCount
	 *            number of elements int the z axis.
	 */
	public TinyArray(ByteBuffer buffer, int sizeOfOneElement, int yCount, int zCount) {
		super(buffer.limit() / sizeOfOneElement, sizeOfOneElement, yCount, zCount);
		this.byteBuffer = buffer;
	}

	/**
	 * Create a TinyArray.
	 *
	 * @param elementCount
	 *            Number of element.
	 * @param sizeOfOneElement
	 *            Size of one stored primitive.
	 */
	public TinyArray(int elementCount, int sizeOfOneElement) {
		super(elementCount, sizeOfOneElement);
		this.byteBuffer = ByteBuffer.allocateDirect(sizeOfOneElement * elementCount);
	}

	/**
	 * Create a TinyArray for a two-dimensional access.
	 *
	 * @param elementCount
	 *            Number of element.
	 * @param sizeOfOneElement
	 *            Size of one stored primitive.
	 * @param colCount
	 *            number of elements by row. Number of elements by column is
	 *            deducted from file size.
	 */
	public TinyArray(int elementCount, int sizeOfOneElement, int colCount) {
		super(elementCount, sizeOfOneElement, colCount);
		this.byteBuffer = ByteBuffer.allocateDirect(sizeOfOneElement * elementCount);
	}

	/**
	 * Create a TinyArray for a three-dimensional access.
	 *
	 * @param elementCount
	 *            Number of element.
	 * @param sizeOfOneElement
	 *            Size of one stored primitive.
	 * @param yCount
	 *            number of elements int the y axis.
	 * @param zCount
	 *            number of elements int the z axis.
	 */
	public TinyArray(int elementCount, int sizeOfOneElement, int yCount, int zCount) {
		super(elementCount, sizeOfOneElement, yCount, zCount);
		this.byteBuffer = ByteBuffer.allocateDirect(sizeOfOneElement * elementCount);
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.utils.array.IArray#close()
	 */
	@Override
	public void close() {
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.utils.array.IArray#dispose()
	 */
	@Override
	public void dispose() {
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.utils.array.IArray#flush()
	 */
	@Override
	public void flush() {
		// Nothing to do.
	}

	/**
	 * ByteBuffer accessor.
	 */
	@Override
	public ByteBuffer getByteBuffer(int index) {
		return this.byteBuffer;
	}

	/**
	 * ByteBuffer accessor.
	 */
	@Override
	public ByteBuffer getByteBuffer(long index) {
		return this.byteBuffer;
	}

	/**
	 * ByteBuffer accessor.
	 */
	@Override
	public ByteBuffer getByteBuffer(int rowIndex, int colIndex) {
		return this.byteBuffer;
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.utils.array.impl.AbstractByteBufferArray#getByteBuffer(int,
	 *      int, int)
	 */
	@Override
	public ByteBuffer getByteBuffer(int x, int y, int z) {
		return this.byteBuffer;
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.utils.array.IArray#setByteOrder(java.nio.ByteOrder)
	 */
	@Override
	public void setByteOrder(ByteOrder byteOrder) {
		this.byteBuffer.order(byteOrder);
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.utils.array.IArray#reduceElementCount(long)
	 */
	@Override
	public void reduceElementCount(long newLimit) {
		setElementCount(newLimit);
		this.byteBuffer.limit((int) (newLimit << getShift()));
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.utils.array.impl.AbstractByteBufferArray#cloneArray()
	 */
	@Override
	protected TinyArray cloneArray() throws IOException {
		ByteBuffer cloneByteBuffer = ByteBuffer.allocateDirect(this.byteBuffer.capacity());
		cloneByteBuffer.order(this.byteBuffer.order());
		this.byteBuffer.rewind();// copy from the beginning
		cloneByteBuffer.put(this.byteBuffer);
		this.byteBuffer.rewind();
		cloneByteBuffer.flip();
		return new TinyArray(cloneByteBuffer, 1 << getShift(), getYCount(), getZCount());
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.utils.array.IArray#dump(java.io.File)
	 */
	@Override
	public void dump(File destination) throws IOException {
		byte[] destinationBuffer = new byte[this.byteBuffer.capacity()];
		this.byteBuffer.get(destinationBuffer);
		Files.write(destination.toPath(), destinationBuffer, StandardOpenOption.TRUNCATE_EXISTING);
	}

}
