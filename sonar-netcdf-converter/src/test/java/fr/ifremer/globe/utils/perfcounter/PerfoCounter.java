package fr.ifremer.globe.utils.perfcounter;

import fr.ifremer.globe.utils.perfcounter.PerfoCounterEntry.Measure;
/**
 * A performance counter counting time elapsed between start and stop call.
 * */
public class PerfoCounter {
	private String detailedMsg;
	PerfoCounterEntry entry = null;

	private long start;

	float elapsedTimeS;

	public PerfoCounter(String key) {
		this(key, null);
	}

	/**
	 * Create a perfo counter with the associated key and detailed message
	 * 
	 * @param key
	 * */
	public PerfoCounter(String key, String detailedMsg) {
		entry = PerfoCounterManager.getInstance().getPerfoCounter(key);

		this.detailedMsg = detailedMsg;
	}

	/**
	 * Start the counter
	 * */
	public PerfoCounter start() {
		// Get current time
		elapsedTimeS = 0;
		start = System.currentTimeMillis();
		return this;
	}

	/**
	 * Stop the counter and print the time elapsed
	 * */
	public Measure stop() {
		elapsedTimeS = (System.currentTimeMillis() - start) / 1000F;
		return entry.addMeasure(System.currentTimeMillis(), elapsedTimeS, detailedMsg);
	}
}
