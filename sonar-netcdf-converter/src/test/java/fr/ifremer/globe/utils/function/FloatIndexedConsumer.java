/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.utils.function;

/**
 * Float consumer for a specific index
 */
@FunctionalInterface
public interface FloatIndexedConsumer {
	void accept(int index, float value);
}
