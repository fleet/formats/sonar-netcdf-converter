package fr.ifremer.globe.core.runtime.datacontainer.layers;

import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.DoubleBuffer3D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.operation.IAbstractBaseLayerOperation;
import fr.ifremer.globe.utils.exception.GIOException;

public class DoubleLayer3D extends AbstractBaseLayer<DoubleBuffer3D> {

	public DoubleLayer3D(String name, String longName, String unit) {
		super(name, longName, unit, DoubleBuffer3D::new);
	}

	public double get(int x, int y, int z) {
		return buffer.get(x, y, z);
	}

	public void set(int x, int y, int z, double value) {
		buffer.set(x, y, z, value);
	}

	/** {@inheritDoc} */
	@Override
	public void process(IAbstractBaseLayerOperation operation) throws GIOException {
		operation.visit(this);
	}
}
