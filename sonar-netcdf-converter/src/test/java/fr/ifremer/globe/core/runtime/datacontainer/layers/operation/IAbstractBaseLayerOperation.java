/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.runtime.datacontainer.layers.operation;

import fr.ifremer.globe.core.runtime.datacontainer.layers.BooleanLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.BooleanLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ByteLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ByteLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLayer3D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IntLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IntLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IntLayer3D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.LongLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.LongLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ShortLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ShortLayer2D;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Design pattern visitor to perform operations on AbstractBaseLayer instances
 */
public interface IAbstractBaseLayerOperation {

	/**
	 * Perform the operation for a BooleanLayer1D layer
	 */
	default void visit(BooleanLayer1D layer) throws GIOException {
	}

	/**
	 * Perform the operation for a ByteLayer1D layer
	 */
	default void visit(ByteLayer1D layer) throws GIOException {
	}

	/**
	 * Perform the operation for a ShortLayer1D layer
	 */
	default void visit(ShortLayer1D layer) throws GIOException {
	}

	/**
	 * Perform the operation for a IntLayer1D layer
	 */
	default void visit(IntLayer1D layer) throws GIOException {
	}

	/**
	 * Perform the operation for a LongLayer1D layer
	 */
	default void visit(LongLayer1D layer) throws GIOException {
	}

	/**
	 * Perform the operation for a FloatLayer1D layer
	 */
	default void visit(FloatLayer1D layer) throws GIOException {
	}

	/**
	 * Perform the operation for a DoubleLayer1D layer
	 */
	default void visit(DoubleLayer1D layer) throws GIOException {
	}

	/**
	 * Perform the operation for a BooleanLayer2D layer
	 */
	default void visit(BooleanLayer2D layer) throws GIOException {
	}

	/**
	 * Perform the operation for a ByteLayer2D layer
	 */
	default void visit(ByteLayer2D layer) throws GIOException {
	}

	/**
	 * Perform the operation for a ShortLayer2D layer
	 */
	default void visit(ShortLayer2D layer) throws GIOException {
	}

	/**
	 * Perform the operation for a IntLayer2D layer
	 */
	default void visit(IntLayer2D layer) throws GIOException {
	}

	/**
	 * Perform the operation for a LongLayer2D layer
	 */
	default void visit(LongLayer2D layer) throws GIOException {
	}

	/**
	 * Perform the operation for a FloatLayer2D layer
	 */
	default void visit(FloatLayer2D layer) throws GIOException {
	}

	/**
	 * Perform the operation for a DoubleLayer2D layer
	 */
	default void visit(DoubleLayer2D layer) throws GIOException {
	}

	/**
	 * Perform the operation for a IntLayer3D layer
	 */
	default void visit(IntLayer3D layer) throws GIOException {
	}

	/**
	 * Perform the operation for a DoubleLayer3D layer
	 */
	default void visit(DoubleLayer3D layer) throws GIOException {
	}

}