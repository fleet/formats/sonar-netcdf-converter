package fr.ifremer.globe.core.runtime.datacontainer.layers.buffers;

import java.io.IOException;

import fr.ifremer.globe.utils.exception.GIOException;

public class BooleanBuffer1D extends AbstractBuffer {

	public BooleanBuffer1D(int length, String name) throws GIOException {
		super(boolean.class, Byte.BYTES, length, name);
	}

	/**
	 * Constructor used for copy
	 * 
	 * @throws IOException
	 **/
	private BooleanBuffer1D(BooleanBuffer1D source) throws IOException {
		super(source);
	}

	@Override
	public BooleanBuffer1D copy() throws IOException {
		return new BooleanBuffer1D(this);
	}

	public boolean get(int index) {
		return backedBuffer.getBoolean(index);
	}

	public void set(int index, boolean value) {
		setDirty(true);
		backedBuffer.putBoolean(index, value);
	}

}
