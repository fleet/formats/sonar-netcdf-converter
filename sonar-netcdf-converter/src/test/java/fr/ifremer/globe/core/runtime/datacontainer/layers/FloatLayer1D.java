package fr.ifremer.globe.core.runtime.datacontainer.layers;

import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.FloatBuffer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.operation.IAbstractBaseLayerOperation;
import fr.ifremer.globe.utils.exception.GIOException;

public class FloatLayer1D extends AbstractBaseLayer<FloatBuffer1D> {

	public FloatLayer1D(String name, String longName, String unit) {
		super(name, longName, unit, FloatBuffer1D::new);
	}

	public float get(int index) {
		return buffer.get(index);
	}

	public void set(int index, float value) {
		buffer.set(index, value);
	}

	/** {@inheritDoc} */
	@Override
	public void process(IAbstractBaseLayerOperation operation) throws GIOException {
		operation.visit(this);
	}

}
