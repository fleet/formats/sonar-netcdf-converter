package fr.ifremer.globe.core.runtime.datacontainer.layers;

import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.LongBuffer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.operation.IAbstractBaseLayerOperation;
import fr.ifremer.globe.utils.exception.GIOException;

public class LongLayer2D extends AbstractBaseLayer<LongBuffer2D> {

	public LongLayer2D(String name, String longName, String unit) {
		super(name, longName, unit, LongBuffer2D::new);
	}

	public long get(int row, int col) {
		return buffer.get(row, col);
	}

	public void set(int row, int col, long value) {
		buffer.set(row, col, value);
	}

	/** {@inheritDoc} */
	@Override
	public void process(IAbstractBaseLayerOperation operation) throws GIOException {
		operation.visit(this);
	}
}
