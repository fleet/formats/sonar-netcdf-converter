package fr.ifremer.globe.core.runtime.datacontainer.layers.buffers;

import java.io.IOException;

import fr.ifremer.globe.utils.exception.GIOException;

public class FloatBuffer1D extends AbstractBuffer {

	public FloatBuffer1D(int length, String name) throws GIOException {
		super(float.class, Float.BYTES, length, name);
	}

	/** Constructor used for copy **/
	private FloatBuffer1D(FloatBuffer1D source) throws IOException {
		super(source);
	}

	@Override
	public FloatBuffer1D copy() throws IOException {
		return new FloatBuffer1D(this);
	}

	public float get(int index) {
		return backedBuffer.getFloat(index);
	}

	public void set(int index, float value) {
		setDirty(true);
		backedBuffer.putFloat(index, value);
	}

}
