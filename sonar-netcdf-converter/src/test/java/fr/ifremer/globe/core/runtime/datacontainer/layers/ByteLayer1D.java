package fr.ifremer.globe.core.runtime.datacontainer.layers;

import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.ByteBuffer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.operation.IAbstractBaseLayerOperation;
import fr.ifremer.globe.utils.exception.GIOException;

public class ByteLayer1D extends AbstractBaseLayer<ByteBuffer1D> {

	public ByteLayer1D(String name, String longName, String unit) {
		super(name, longName, unit, ByteBuffer1D::new);
	}

	public byte get(int index) {
		return buffer.get(index);
	}

	public void set(int index, byte value) {
		buffer.set(index, value);
	}

	public int getElementSizeInBytes() {
		return Byte.BYTES;
	}

	/** {@inheritDoc} */
	@Override
	public void process(IAbstractBaseLayerOperation operation) throws GIOException {
		operation.visit(this);
	}

}
