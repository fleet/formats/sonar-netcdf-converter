package fr.ifremer.globe.core.runtime.datacontainer.layers.buffers;

import java.io.IOException;

import fr.ifremer.globe.utils.exception.GIOException;

public class FloatBuffer2D extends AbstractBuffer {

	public FloatBuffer2D(int xSize, int ySize, String name) throws GIOException {
		super(float.class, Float.BYTES, xSize, ySize, name);
	}

	/** Constructor used for copy **/
	private FloatBuffer2D(FloatBuffer2D source) throws IOException {
		super(source);
	}

	@Override
	public FloatBuffer2D copy() throws IOException {
		return new FloatBuffer2D(this);
	}

	public float get(int row, int col) {
		return backedBuffer.getFloat(row, col);
	}

	public void set(int row, int col, float value) {
		setDirty(true);
		backedBuffer.putFloat(row, col, value);
	}

}
