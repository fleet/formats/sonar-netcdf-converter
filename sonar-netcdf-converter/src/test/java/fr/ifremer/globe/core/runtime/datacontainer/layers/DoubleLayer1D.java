package fr.ifremer.globe.core.runtime.datacontainer.layers;

import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.DoubleBuffer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.operation.IAbstractBaseLayerOperation;
import fr.ifremer.globe.utils.exception.GIOException;

public class DoubleLayer1D extends AbstractBaseLayer<DoubleBuffer1D> {

	public DoubleLayer1D(String name, String longName, String unit) {
		super(name, longName, unit, DoubleBuffer1D::new);
	}

	public double get(int index) {
		return buffer.get(index);
	}

	public void set(int index, double value) {
		buffer.set(index, value);
	}

	/** {@inheritDoc} */
	@Override
	public void process(IAbstractBaseLayerOperation operation) throws GIOException {
		operation.visit(this);
	}

}
