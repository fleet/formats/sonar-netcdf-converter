package fr.ifremer.globe.core.runtime.datacontainer.layers.buffers;

import java.io.IOException;

import fr.ifremer.globe.utils.exception.GIOException;

public class IntBuffer2D extends AbstractBuffer {

	public IntBuffer2D(int xSize, int ySize, String name) throws GIOException {
		super(int.class, Integer.BYTES, xSize, ySize, name);
	}

	/** Constructor used for copy **/
	private IntBuffer2D(IntBuffer2D source) throws IOException {
		super(source);
	}

	@Override
	public IntBuffer2D copy() throws IOException {
		return new IntBuffer2D(this);
	}

	public int get(int row, int col) {
		return backedBuffer.getInt(row, col);
	}

	public void set(int row, int col, int value) {
		setDirty(true);
		backedBuffer.putInt(row, col, value);
	}

}
