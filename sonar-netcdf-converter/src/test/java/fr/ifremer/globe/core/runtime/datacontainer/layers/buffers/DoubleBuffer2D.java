package fr.ifremer.globe.core.runtime.datacontainer.layers.buffers;

import java.io.IOException;

import fr.ifremer.globe.utils.exception.GIOException;

public class DoubleBuffer2D extends AbstractBuffer {

	public DoubleBuffer2D(int xSize, int ySize, String name) throws GIOException {
		super(double.class, Double.BYTES, xSize, ySize, name);
	}

	/** Constructor used for copy **/
	private DoubleBuffer2D(DoubleBuffer2D source) throws IOException {
		super(source);
	}

	@Override
	public DoubleBuffer2D copy() throws IOException {
		return new DoubleBuffer2D(this);
	}

	public double get(int row, int col) {
		return backedBuffer.getDouble(row, col);
	}

	public void set(int row, int col, double value) {
		setDirty(true);
		backedBuffer.putDouble(row, col, value);
	}

}
