package fr.ifremer.globe.core.runtime.datacontainer.layers;

import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.ShortBuffer1D;
import fr.ifremer.globe.utils.exception.GIOException;

public class ShortLoadableLayer1D extends ShortLayer1D implements ILoadableLayer<ShortBuffer1D> {

	private boolean isLoaded;

	public ShortLoadableLayer1D(String name, String longName, String unit, ILayerLoader<ShortBuffer1D> l) throws GIOException {
		super(name, longName, unit);
		loader = l;
		dimensions = loader.getDimensions();
	}

	ILayerLoader<ShortBuffer1D> loader;

	@Override
	public ILayerLoader<ShortBuffer1D> getLoader() {
		return loader;
	}

	@Override
	public boolean isLoaded() {
		return isLoaded;
	}

	@Override
	public void setLoaded(boolean b) {
		isLoaded = b;
	}

}
