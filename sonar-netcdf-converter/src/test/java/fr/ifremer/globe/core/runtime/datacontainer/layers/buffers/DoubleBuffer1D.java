package fr.ifremer.globe.core.runtime.datacontainer.layers.buffers;

import java.io.IOException;

import fr.ifremer.globe.utils.exception.GIOException;

public class DoubleBuffer1D extends AbstractBuffer {

	public DoubleBuffer1D(int length, String name) throws GIOException {
		super(double.class, Double.BYTES, length, name);
	}

	/** Constructor used for copy **/
	private DoubleBuffer1D(DoubleBuffer1D source) throws IOException {
		super(source);
	}

	@Override
	public DoubleBuffer1D copy() throws IOException {
		return new DoubleBuffer1D(this);
	}

	public double get(int index) {
		return backedBuffer.getDouble(index);
	}

	public void set(int index, double value) {
		setDirty(true);
		backedBuffer.putDouble(index, value);
	}
}
