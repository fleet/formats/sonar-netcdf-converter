package fr.ifremer.globe.core.runtime.datacontainer.layers;

import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.IntBuffer3D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.operation.IAbstractBaseLayerOperation;
import fr.ifremer.globe.utils.exception.GIOException;

public class IntLayer3D extends AbstractBaseLayer<IntBuffer3D> {

	public IntLayer3D(String name, String longName, String unit) {
		super(name, longName, unit, IntBuffer3D::new);
	}

	public int get(int x, int y, int z) {
		return buffer.get(x, y, z);
	}

	public void set(int x, int y, int z, int value) {
		buffer.set(x, y, z, value);
	}

	/** {@inheritDoc} */
	@Override
	public void process(IAbstractBaseLayerOperation operation) throws GIOException {
		operation.visit(this);
	}
}
