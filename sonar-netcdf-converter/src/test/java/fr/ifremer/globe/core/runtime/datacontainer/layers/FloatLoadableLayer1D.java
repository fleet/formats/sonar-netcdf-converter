package fr.ifremer.globe.core.runtime.datacontainer.layers;

import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.FloatBuffer1D;
import fr.ifremer.globe.utils.exception.GIOException;

public class FloatLoadableLayer1D extends FloatLayer1D implements ILoadableLayer<FloatBuffer1D> {

	private boolean isLoaded;
	private float fillValue;

	public FloatLoadableLayer1D(String name, String longName, String unit, ILayerLoader<FloatBuffer1D> l) throws GIOException {
		super(name, longName, unit);
		loader = l;
		dimensions = loader.getDimensions();
		fillValue = loader.getFloatFillValue();
	}

	ILayerLoader<FloatBuffer1D> loader;

	@Override
	public ILayerLoader<FloatBuffer1D> getLoader() {
		return loader;
	}

	@Override
	public boolean isLoaded() {
		return isLoaded;
	}

	@Override
	public void setLoaded(boolean b) {
		isLoaded = b;
	}

	public float getFillValue() {
		return fillValue;
	}
	
}
