package fr.ifremer.globe.core.runtime.datacontainer.layers;

import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.AbstractBuffer;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Interface allowing to build a new instance of a layer
 * 
 */
@FunctionalInterface
public interface ILayerBuilder<T extends AbstractBuffer> {

	public ILoadableLayer<T> build(String name, String longName, String unit, ILayerLoader<T> m) throws GIOException;

}
