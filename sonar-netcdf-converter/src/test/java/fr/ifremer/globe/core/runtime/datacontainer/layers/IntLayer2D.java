package fr.ifremer.globe.core.runtime.datacontainer.layers;

import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.IntBuffer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.operation.IAbstractBaseLayerOperation;
import fr.ifremer.globe.utils.exception.GIOException;

public class IntLayer2D extends AbstractBaseLayer<IntBuffer2D> {

	public IntLayer2D(String name, String longName, String unit) {
		super(name, longName, unit, IntBuffer2D::new);
	}

	public int get(int row, int col) {
		return buffer.get(row, col);
	}

	public void set(int row, int col, int value) {
		buffer.set(row, col, value);
	}

	/** {@inheritDoc} */
	@Override
	public void process(IAbstractBaseLayerOperation operation) throws GIOException {
		operation.visit(this);
	}
}
