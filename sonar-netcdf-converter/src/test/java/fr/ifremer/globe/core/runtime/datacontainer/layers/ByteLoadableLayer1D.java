package fr.ifremer.globe.core.runtime.datacontainer.layers;

import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.ByteBuffer1D;
import fr.ifremer.globe.utils.exception.GIOException;

public class ByteLoadableLayer1D extends ByteLayer1D implements ILoadableLayer<ByteBuffer1D> {

	private boolean isLoaded;

	public ByteLoadableLayer1D(String name, String longName, String unit, ILayerLoader<ByteBuffer1D> l) throws GIOException {
		super(name, longName, unit);
		loader = l;
		dimensions = loader.getDimensions();
	}

	ILayerLoader<ByteBuffer1D> loader;

	@Override
	public ILayerLoader<ByteBuffer1D> getLoader() {
		return loader;
	}

	@Override
	public boolean isLoaded() {
		return isLoaded;
	}

	@Override
	public void setLoaded(boolean b) {
		isLoaded = b;
	}

}
