package fr.ifremer.globe.core.runtime.datacontainer.layers;

import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.DoubleBuffer1D;
import fr.ifremer.globe.utils.exception.GIOException;

public class DoubleLoadableLayer1D extends DoubleLayer1D implements ILoadableLayer<DoubleBuffer1D> {

	private boolean isLoaded;

	public DoubleLoadableLayer1D(String name, String longName, String unit, ILayerLoader<DoubleBuffer1D> l) throws GIOException {
		super(name, longName, unit);
		loader = l;
		dimensions = loader.getDimensions();
	}

	ILayerLoader<DoubleBuffer1D> loader;

	@Override
	public ILayerLoader<DoubleBuffer1D> getLoader() {
		return loader;
	}

	@Override
	public boolean isLoaded() {
		return isLoaded;
	}

	@Override
	public void setLoaded(boolean b) {
		isLoaded = b;
	}

}
