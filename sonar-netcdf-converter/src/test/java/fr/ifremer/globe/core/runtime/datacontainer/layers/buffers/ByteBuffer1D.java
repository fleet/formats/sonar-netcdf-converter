package fr.ifremer.globe.core.runtime.datacontainer.layers.buffers;

import java.io.IOException;

import fr.ifremer.globe.utils.exception.GIOException;

public class ByteBuffer1D extends AbstractBuffer {

	public ByteBuffer1D(int length, String name) throws GIOException {
		super(byte.class, Byte.BYTES, length, name);
	}

	/** Constructor used for copy **/
	private ByteBuffer1D(ByteBuffer1D source) throws IOException {
		super(source);
	}

	@Override
	public ByteBuffer1D copy() throws IOException {
		return new ByteBuffer1D(this);
	}

	public byte get(int index) {
		return backedBuffer.getByte(index);
	}

	public void set(int index, byte value) {
		setDirty(true);
		backedBuffer.putByte(index, value);
	}
}
