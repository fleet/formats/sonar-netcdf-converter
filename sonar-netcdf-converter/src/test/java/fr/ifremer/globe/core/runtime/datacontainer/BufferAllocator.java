package fr.ifremer.globe.core.runtime.datacontainer;

import java.io.IOException;

import fr.ifremer.globe.utils.array.IArrayFactory;
import fr.ifremer.globe.utils.array.impl.LargeArray;
import fr.ifremer.globe.utils.exception.GIOException;

public class BufferAllocator {

	private static final String PREFIX = "sdc_layer_";

	/**
	 * utility class
	 */
	private BufferAllocator() {
	}

	public static LargeArray allocateBuffer1D(int xSize, int elementSizeInBytes, String name) throws GIOException {
		try {
			return IArrayFactory.grab().makeLargeArray(xSize, elementSizeInBytes, PREFIX + name);
		} catch (IOException e) {
			throw new GIOException("Loading error : arrayFactory.makeLargeArray exception: " + e.getMessage(), e);
		}
	}

	public static LargeArray allocateBuffer2D(int xSize, int ySize, int elementSizeInBytes, String name)
			throws GIOException {
		try {
			return IArrayFactory.grab().makeLargeArray(xSize, ySize, elementSizeInBytes, PREFIX + name);
		} catch (IOException e) {
			throw new GIOException("Loading error : arrayFactory.makeLargeArray exception: " + e.getMessage(), e);
		}
	}

	public static LargeArray allocateBuffer3D(int xSize, int ySize, int zSize, int elementSizeInBytes, String name)
			throws GIOException {
		try {
			return IArrayFactory.grab().makeLargeArray(xSize, ySize, zSize, elementSizeInBytes, PREFIX + name);
		} catch (IOException e) {
			throw new GIOException("Loading error : arrayFactory.makeLargeArray exception: " + e.getMessage(), e);
		}
	}

}
