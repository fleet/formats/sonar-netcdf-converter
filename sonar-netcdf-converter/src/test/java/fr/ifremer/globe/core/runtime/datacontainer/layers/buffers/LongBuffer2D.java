package fr.ifremer.globe.core.runtime.datacontainer.layers.buffers;

import java.io.IOException;

import fr.ifremer.globe.utils.exception.GIOException;

public class LongBuffer2D extends AbstractBuffer {

	public LongBuffer2D(int xSize, int ySize, String name) throws GIOException {
		super(long.class, Long.BYTES, xSize, ySize, name);
	}

	/** Constructor used for copy **/
	private LongBuffer2D(LongBuffer2D source) throws IOException {
		super(source);
	}

	@Override
	public LongBuffer2D copy() throws IOException {
		return new LongBuffer2D(this);
	}

	public long get(int row, int col) {
		return backedBuffer.getLong(row, col);
	}

	public void set(int row, int col, long value) {
		setDirty(true);
		backedBuffer.putLong(row, col, value);
	}

}
