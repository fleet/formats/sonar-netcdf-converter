package fr.ifremer.globe.netcdf.api.buffer;

import java.nio.ByteBuffer;

public abstract class ABufferWrapper {

	/** Wrapped buffer **/
	protected final ByteBuffer buffer;

	/** Constructor **/
	public ABufferWrapper(ByteBuffer buffer) {
		this.buffer = buffer;
	}

	/**
	 * get a value as {@code float}
	 */
	public abstract float getAsFloat(int index);

	/**
	 * get a value as {@code double}
	 */
	public abstract double getAsDouble(int index);

	/**
	 * get a value as {@code int}
	 */
	public abstract int getAsInt(int index);

	/**
	 * get a value as {@code short}
	 */
	public abstract short getAsShort(int index);

	/**
	 * get a value as {@code byte}
	 */
	public abstract byte getAsByte(int index);

	/**
	 * get a value as {@code long}
	 */
	public abstract long getAsLong(int index);

	/**
	 * put a value as {@code float}
	 */
	public abstract void putAsFloat(int index, float value);

	/**
	 * put a value as {@code double}
	 */
	public abstract void putAsDouble(int index, double value);

	/**
	 * put a value as {@code int}
	 */
	public abstract void putAsInt(int index, int value);

	/**
	 * put a value as {@code short}
	 */
	public abstract void putAsShort(int index, short value);

	/**
	 * put a value as {@code byte}
	 */
	public abstract void putAsByte(int index, byte value);

	/**
	 * put a value as {@code long}
	 */
	public abstract void putAsLong(int index, long value);

	/**
	 * put a value from an other buffer wrapper
	 * @param bufferSrc : source buffer
	 * @param index : index
	 */
	public abstract void putFrom(ABufferWrapper bufferSrc, int index);

}
