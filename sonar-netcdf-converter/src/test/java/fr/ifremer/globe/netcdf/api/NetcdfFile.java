
package fr.ifremer.globe.netcdf.api;

import java.util.TreeMap;

import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCFile;
import fr.ifremer.globe.netcdf.ucar.NCFile.Mode;
import fr.ifremer.globe.netcdf.ucar.NCGroup;
import fr.ifremer.globe.netcdf.ucar.NCObject;

public class NetcdfFile extends NetcdfGroup implements AutoCloseable {

	private NCFile file;
	
	/**
	 * Open an existing XSF file
	 * 
	 * @param path
	 *            file path
	 * @param mode
	 *            read only, or read/write
	 */
	public static NetcdfFile open(String path, Mode mode) throws NCException {
		return new NetcdfFile(NCFile.open(path, mode));
	}

	public NetcdfFile(NCFile f) {
		super(f.getGroupId(), f.getName(), null);
		this.file = f;
		this.file.setWithFillValue(false);
	}

	/**
	 * Close the file
	 */
	@Override
	public void close() {
		this.file.close();
	}

	/**
	 * Synchronize to disk.
	 */
	public void synchronize() throws NCException {
		this.file.synchronize();
	}

	/**
	 * Get group (input parameter can be a path)
	 */
	@Override
	public NetcdfGroup getGroup(String path) throws NCException {
		if (path.startsWith("/"))
			path = path.replaceFirst("/", "");

		if (path.isEmpty())
			return this;

		String[] groups = path.split("/");
		if (groups.length == 1)
			return super.getGroup(groups[0]);

		NetcdfGroup currentGroup = this;
		for (int i = 0; i < groups.length; i++) {
			currentGroup = currentGroup.getGroup(groups[i]);
			if (currentGroup == null)
				return null;
		}

		return currentGroup;
	}

	/**
	 * Get variable/layer (input parameter can be a path)
	 */
	@Override
	public NetcdfVariable getVariable(String path) throws NCException {
		if (path.lastIndexOf("/") == -1)
			return super.getVariable(path);
		else {
			String groupPath = path.substring(0, path.lastIndexOf("/"));
			String var = path.substring(path.lastIndexOf("/") + 1);
			NetcdfGroup grp = getGroup(groupPath);
			if (grp == null)
				return null;
			return grp.getVariable(var);
		}
	}

	/**
	 * Get dimension (input parameter can be a path)
	 */
	@Override
	public NCDimension getDimension(String path) throws NCException {
		if (path.lastIndexOf("/") == -1)
			return super.getDimension(path);
		else {
			String groupPath = path.substring(0, path.lastIndexOf("/"));
			String dim = path.substring(path.lastIndexOf("/") + 1);
			NCGroup grp = getGroup(groupPath);
			if (grp == null)
				return null;
			return grp.getDimension(dim);
		}
	}

	/**
	 * Get group or variable : try to find group with the specified path, if it doesn't exist: try to find a variable.
	 * 
	 * @return Found group or variable, or null.
	 */
	private NCObject getGroupOrVariableIfExists(String path) throws NCException {
		NCObject result = getGroup(path);
		if (result == null)
			result = getVariable(path);
		return result;

	}
	
	
	/**
	 * Get group or variable : try to find group with the specified path, if it doesn't exist: try to find a variable.
	 * 
	 * @return Found group or variable, or null.
	 */
	private NCObject getGroupOrVariable(String path) throws NCException {
		NCObject result = getGroupOrVariableIfExists(path);
		if (result == null)
			throw new NCException("invalid path: " + path + " ; file: " + file.getName());
		return result;

	}

	/**
	 * getAttibuteXXX methods : overridden to handle path as input parameter (ex:
	 * getAttibuteXXX('/group1/group2/my_attribute'))
	 */

	@Override
	public int getAttributeType(String path) throws NCException {
		String att = path.substring(path.lastIndexOf('/') + 1);
		NCObject nco = getGroupOrVariable(path.substring(0, path.length() - att.length()));
		return nco == this ? super.getAttributeType(att) : nco.getAttributeType(att);
	}
	
	@Override
	public boolean hasAttribute(String path) throws NCException {
		String att = path.substring(path.lastIndexOf('/') + 1);
		NCObject nco = getGroupOrVariableIfExists(path.substring(0, path.length() - att.length()));
		if (nco == null) return false;
		return nco == this ? super.hasAttribute(att) : nco.hasAttribute(att);
	}

	@Override
	public double getAttributeDouble(String path) throws NCException {
		String att = path.substring(path.lastIndexOf('/') + 1);
		NCObject nco = getGroupOrVariable(path.substring(0, path.length() - att.length()));
		return nco == this ? super.getAttributeDouble(att) : nco.getAttributeDouble(att);
	}

	@Override
	public float getAttributeFloat(String path) throws NCException {
		String att = path.substring(path.lastIndexOf('/') + 1);
		NCObject nco = getGroupOrVariable(path.substring(0, path.length() - att.length()));
		return nco == this ? super.getAttributeFloat(att) : nco.getAttributeFloat(att);
	}

	@Override
	public int getAttributeInt(String path) throws NCException {
		String att = path.substring(path.lastIndexOf('/') + 1);
		NCObject nco = getGroupOrVariable(path.substring(0, path.length() - att.length()));
		return nco == this ? super.getAttributeInt(att) : nco.getAttributeInt(att);
	}

	@Override
	public long getAttributeLong(String path) throws NCException {
		String att = path.substring(path.lastIndexOf('/') + 1);
		NCObject nco = getGroupOrVariable(path.substring(0, path.length() - att.length()));
		return nco == this ? super.getAttributeLong(att) : nco.getAttributeLong(att);
	}

	@Override
	public String getAttributeText(String path) throws NCException {
		String att = path.substring(path.lastIndexOf('/') + 1);
		NCObject nco = getGroupOrVariable(path.substring(0, path.length() - att.length()));
		return nco == this ? super.getAttributeText(att) : nco.getAttributeText(att);
	}

	@Override
	public String getAttributeAsString(String path) throws NCException {
		String att = path.substring(path.lastIndexOf('/') + 1);
		NCObject nco = getGroupOrVariable(path.substring(0, path.length() - att.length()));
		return nco == this ? super.getAttributeAsString(att) : nco.getAttributeAsString(att);
	}

	/**
	 * Returns attributes of the root group and the summary group
	 * 
	 * @throws NCException
	 */
	public TreeMap<String, String> getGlobalAttributs() throws NCException {
		TreeMap<String, String> result = new TreeMap<>();
		result.putAll(getAttributes());
		return result;
	}

	/** @returns true if the file can manage the FillValue attribute */
	@Override
	public boolean isWithFillValue() {
		return false;
	}
}