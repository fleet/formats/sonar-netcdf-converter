package fr.ifremer.globe.netcdf.api;

import java.util.List;

import fr.ifremer.globe.netcdf.api.convention.CFStandardNames;
import fr.ifremer.globe.netcdf.api.operation.FloatUnaryOperator;
import fr.ifremer.globe.netcdf.jna.Nc4prototypes.Vlen_t;
import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCGroup;


/**
 * Netcdf class handling vlen variables 
 * */
public class NetcdfVlenVariable extends NetcdfVariable {

	
	
	public NetcdfVlenVariable(int ncid, NCGroup group, String name, int type, List<NCDimension> shape) {
		super(ncid, group, name, type, shape);
	}
	
	
	/**
	 * Wrapp a vlen in order to give float values
	 * */
	public interface VlenWrapper
	{
		float get(Vlen_t values,int index);
	}
	
	public boolean isVlen()
	{
		return true;
	}
	
	/**
	 * compute the operation transform taking into account for scale factor, add offset 
	 * @throws NCException 
	 * */
	public FloatUnaryOperator getOperation() throws NCException
	{
		float scaleFactor =1f;
		if (hasScaleFactor()) { // Scale offset
			scaleFactor = getAttributeFloat(CFStandardNames.CF_SCALE_FACTOR);
		}
		final float s=scaleFactor;
		float addOffset=0f;
		if (hasAddOffset()) {// Add offset
			addOffset = getAttributeFloat(CFStandardNames.CF_ADD_OFFSET);
		}
		final float off=addOffset;
		return new FloatUnaryOperator() {
			
			@Override
			public float applyAsFloat(float value) {
				return value*s+off;
			}
		};
//		if (unitTransformIsNeeded) // Unit transform
//			floatOperation = andThen(floatOperation, UnitOperation.withFloat(srcUnit.get(), dstUnit.get()));
//
	}
	public VlenWrapper getVlenWrapper() throws NCException
	{
		DataType type=getRawDataType();
		switch(type)
		{
			case FLOAT:
				return new VlenWrapper() {
					
					@Override
					public float get(Vlen_t values, int index) {
						return values.p.getFloat(index*Float.BYTES);
					}
				};
			case SHORT:
				return new VlenWrapper() {
					
					@Override
					public float get(Vlen_t values, int index) {
						return (float)values.p.getShort(index*Short.BYTES);
					}
				};
			case BOOLEAN:
			case BYTE:
				return new VlenWrapper() {
					
					@Override
					public float get(Vlen_t values, int index) {
						return (float)values.p.getByte(index);
					}
				};
			case CHAR:
			case DOUBLE:
			case ENUM1:
			case ENUM2:
			case ENUM4:
			case INT:
			case LONG:
			case OBJECT:
			case OPAQUE:
			case STRING:
			case UBYTE:
			case UINT:
			case ULONG:
			case USHORT:
			default:
				throw new NCException("Unsupported vlen type");
		}
	}
}
