package fr.ifremer.globe.netcdf.api.buffer;

import java.nio.ByteBuffer;

import fr.ifremer.globe.netcdf.ucar.DataType;

/**
 * Manages unsigned short, due to java having no type ushort, data are stored in an int
 */
public class UShortBufferWrapper extends ABufferWrapper {

	public UShortBufferWrapper(ByteBuffer buffer) {
		super(buffer);
	}

	private int readValue(int index) {
		buffer.position(index * DataType.SHORT.getSize());
		return Short.toUnsignedInt(buffer.getShort());
	}

	private void writeValue(int index, int value) {
		buffer.position(index * DataType.SHORT.getSize());
		buffer.putShort((short) value);
	}

	@Override
	public float getAsFloat(int index) {
		return readValue(index);
	}

	@Override
	public double getAsDouble(int index) {
		return (double) readValue(index);
	}

	@Override
	public int getAsInt(int index) {
		return readValue(index);
	}

	@Override
	public short getAsShort(int index) {
		return (short) readValue(index);
	}

	@Override
	public byte getAsByte(int index) {
		return (byte) readValue(index);
	}

	@Override
	public long getAsLong(int index) {
		return (long) readValue(index);
	}

	@Override
	public void putAsFloat(int index, float value) {
		writeValue(index, (int) Math.round(value));
	}

	@Override
	public void putAsDouble(int index, double value) {
		writeValue(index, (int) Math.round(value));
	}

	@Override
	public void putAsInt(int index, int value) {
		writeValue(index, value);
	}

	@Override
	public void putAsShort(int index, short value) {
		writeValue(index, value);
	}

	@Override
	public void putAsByte(int index, byte value) {
		writeValue(index, value);
	}

	@Override
	public void putAsLong(int index, long value) {
		writeValue(index, (int) value);
	}

	@Override
	public void putFrom(ABufferWrapper src, int index) {
		writeValue(index, src.getAsInt(index));
	}
}
