package fr.ifremer.globe.netcdf.api.buffer;

import java.nio.ByteBuffer;

import fr.ifremer.globe.netcdf.api.UnsupportedDataType;
import fr.ifremer.globe.netcdf.ucar.DataType;	

/**
 * Provides byte buffer wrappers 
 */
public class BufferWrapperProvider {
	
	private BufferWrapperProvider() {};

	/**
	 * @return {@link ByteBufferWrapper} corresponding to the specified {@link DataType}
	 */
	public static ABufferWrapper get(ByteBuffer buffer, DataType type) throws UnsupportedDataType {
		switch (type) {
		case BOOLEAN:
		case BYTE:
			return new ByteBufferWrapper(buffer);
		case DOUBLE:
			return new DoubleBufferWrapper(buffer);
		case FLOAT:
			return new FloatBufferWrapper(buffer);
		case INT:
			return new IntBufferWrapper(buffer);
		case LONG:
			return new LongBufferWrapper(buffer);
		case CHAR:
			 return new UByteBufferWrapper(buffer);

		case SHORT:
			return new ShortBufferWrapper(buffer);
		case UBYTE:
			 return new UByteBufferWrapper(buffer);
		case UINT:
			 return new UIntBufferWrapper(buffer);
		case USHORT:
			 return new UShortBufferWrapper(buffer);
		case ULONG:
			 return new LongBufferWrapper(buffer);
		case STRING:
		case ENUM1:
		case ENUM2:
		case ENUM4:
		case OBJECT:
		case OPAQUE:
		default:
			throw new UnsupportedDataType(type);
		}
	}
}
