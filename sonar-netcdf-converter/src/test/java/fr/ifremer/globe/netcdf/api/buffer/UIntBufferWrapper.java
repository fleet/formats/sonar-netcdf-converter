package fr.ifremer.globe.netcdf.api.buffer;

import java.nio.ByteBuffer;

import fr.ifremer.globe.netcdf.ucar.DataType;

/**
 * Manages unsigned int, due to java having no type uint, data are stored in an long
 */
public class UIntBufferWrapper extends ABufferWrapper {

	public UIntBufferWrapper(ByteBuffer buffer) {
		super(buffer);
	}

	private long readValue(int index) {
		buffer.position(index * DataType.INT.getSize());
		return Integer.toUnsignedLong(buffer.getInt());
	}

	private void writeValue(int index, long value) {
		buffer.position(index * DataType.INT.getSize());
		buffer.putInt((int) value);
	}

	@Override
	public float getAsFloat(int index) {
		return readValue(index);
	}

	@Override
	public double getAsDouble(int index) {
		return (double) readValue(index);
	}

	@Override
	public int getAsInt(int index) {
		return (int) readValue(index);
	}

	@Override
	public short getAsShort(int index) {
		return (short) readValue(index);
	}

	@Override
	public byte getAsByte(int index) {
		return (byte) readValue(index);
	}

	@Override
	public long getAsLong(int index) {
		return readValue(index);
	}

	@Override
	public void putAsFloat(int index, float value) {
		writeValue(index, (long) Math.round(value));
	}

	@Override
	public void putAsDouble(int index, double value) {
		writeValue(index, (long) Math.round(value));
	}

	@Override
	public void putAsInt(int index, int value) {
		writeValue(index, (long) value);
	}

	@Override
	public void putAsShort(int index, short value) {
		writeValue(index, (long) value);
	}

	@Override
	public void putAsByte(int index, byte value) {
		writeValue(index, (long) value);
	}

	@Override
	public void putAsLong(int index, long value) {
		writeValue(index, value);
	}

	@Override
	public void putFrom(ABufferWrapper src, int index) {
		writeValue(index, src.getAsLong(index));
	}
}
