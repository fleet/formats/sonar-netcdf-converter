package fr.ifremer.globe.api.xsf.tests.s7k.datagrams;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramParser;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K7027;

public class Test7027 extends TestDatagram {

	@Test
	public void testDecode() throws IOException {
		DatagramBuffer buffer = getDataBuffer(7027);
		
		assertEquals(7027, DatagramParser.getDatagramType(buffer));
		
		assertEquals(0, S7K7027.getSonarId(buffer).getLong());
		assertEquals(334879, S7K7027.getPingNumber(buffer).getU());
		assertEquals(0, S7K7027.getMultiPingSequence(buffer).getU());
		assertEquals(512, S7K7027.getNumberOfDetectionPoints(buffer).getU());
		assertEquals(22, S7K7027.getDataFieldSize(buffer).getU());
		assertEquals(2, S7K7027.getDetectionAlgorithm(buffer).getU());
		assertEquals(2, S7K7027.get7027Flags(buffer).getU());
		assertEquals(34482.75, S7K7027.getSamplingRate(buffer).get(), 0.01);
		assertEquals(0.0, S7K7027.getTxAngle(buffer).get(), 0.0);
		
		assertEquals(0, S7K7027.getBeamDescriptor(buffer, 0).getU());
		assertEquals(909.9, S7K7027.getDetectionPoint(buffer, 0).get(), 0.1);
		assertEquals(-71.41, Math.toDegrees(S7K7027.getRxAngle(buffer, 0).get()), 0.01);
		assertEquals(0x0006, S7K7027.getDetectionFlags(buffer, 0).getU());
		assertEquals(0x0003, S7K7027.getQuality(buffer, 0).getU());
		assertEquals(0.001, S7K7027.getUncertainty(buffer, 0).get(), 0.001);
		
		assertEquals(10, S7K7027.getBeamDescriptor(buffer, 10).getU());
		assertEquals(887.15, S7K7027.getDetectionPoint(buffer, 10).get(), 0.1);
		assertEquals(-70.66, Math.toDegrees(S7K7027.getRxAngle(buffer, 10).get()), 0.01);
		assertEquals(0x0006, S7K7027.getDetectionFlags(buffer, 10).getU());
		assertEquals(0x0003, S7K7027.getQuality(buffer, 10).getU());
		assertEquals(0.001, S7K7027.getUncertainty(buffer, 10).get(), 0.001);
		
		// test optional data
		
		assertEquals(396000.0, S7K7027.getFrequency(buffer).get(), 0.01);
		assertEquals(50.35, Math.toDegrees(S7K7027.getLatitude(buffer).get()), 0.01);
		assertEquals(-4.17, Math.toDegrees(S7K7027.getLongitude(buffer).get()), 0.01);
		assertEquals(44.13, Math.toDegrees(S7K7027.getHeading(buffer).get()), 0.01);
		assertEquals(0x0, S7K7027.getHeightSource(buffer).getU());
		assertEquals(0.0, S7K7027.getTide(buffer).get(), 0.01);
		assertEquals(-1.39, Math.toDegrees(S7K7027.getRoll(buffer).get()), 0.01);
		assertEquals(0.12, Math.toDegrees(S7K7027.getPitch(buffer).get()), 0.01);
		assertEquals(0.55, Math.toDegrees(S7K7027.getHeave(buffer).get()), 0.01);
		assertEquals(-1.0, S7K7027.getVehiculeDepth(buffer).get(), 0.01);
		
		// optional per detection data
		
		assertEquals(7.49, S7K7027.getDetectionDepth(buffer, 0).get(), 0.01);
		assertEquals(2.63, S7K7027.getDetectionAlongTrackDistance(buffer, 0).get(), 0.01);
		assertEquals(-18.5, S7K7027.getDetectionAcrossTrackDistance(buffer, 0).get(), 0.01);
		assertEquals(68.54, Math.toDegrees(S7K7027.getBeamPointingAngle(buffer, 0).get()), 0.01);
		assertEquals(270.39, Math.toDegrees(S7K7027.getBeamAzimuthAngle(buffer, 0).get()), 0.01);
		
		assertEquals(7.55, S7K7027.getDetectionDepth(buffer, 10).get(), 0.01);
		assertEquals(2.59, S7K7027.getDetectionAlongTrackDistance(buffer, 10).get(), 0.01);
		assertEquals(-17.94, S7K7027.getDetectionAcrossTrackDistance(buffer, 10).get(), 0.01);
		assertEquals(67.79, Math.toDegrees(S7K7027.getBeamPointingAngle(buffer, 10).get()), 0.01);
		assertEquals(270.41, Math.toDegrees(S7K7027.getBeamAzimuthAngle(buffer, 10).get()), 0.01);
		
		assertEquals(12.39, S7K7027.getDetectionDepth(buffer, 511).get(), 0.01);
		assertEquals(-1.44, S7K7027.getDetectionAlongTrackDistance(buffer, 511).get(), 0.01);
		assertEquals(36.43, S7K7027.getDetectionAcrossTrackDistance(buffer, 511).get(), 0.01);
		assertEquals(71.48, Math.toDegrees(S7K7027.getBeamPointingAngle(buffer, 511).get()), 0.01);
		assertEquals(89.63, Math.toDegrees(S7K7027.getBeamAzimuthAngle(buffer, 511).get()), 0.01);
	}
}
