package fr.ifremer.globe.api.xsf.tests.kmall.datagrams;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.kmall.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.SVP;


public class TestSVPDatagrams extends DatagramTest {

    @Test
    public void testRead() throws IOException {
    	DatagramBuffer buffer = getDatagram("SVP");
        
        // part common to all datagrams
        assertEquals("#SVP", SVP.getDgmType(buffer.byteBufferData));       
        assertEquals(140, SVP.getSystemId(buffer.byteBufferData).getU());
        assertEquals(0, SVP.getDgmVersion(buffer.byteBufferData).getU());
        assertEquals(2040, SVP.getEchoSounderId(buffer.byteBufferData).getU());
        assertEquals(1473163103, SVP.getTimeSec(buffer.byteBufferData).getU());
        assertEquals(752055760, SVP.getTimeNano(buffer.byteBufferData).getU());
        
        // Sensor Common data  datagram related part
        assertEquals(28, SVP.getNumBytesCmnPart(buffer.byteBufferData).getU());
        assertEquals(2, SVP.getSamplesCount(buffer.byteBufferData).getU());
        
        //TODO : assertEquals("", SVP.getSensorFormat(buffer.byteBufferData));
        assertEquals(0, SVP.getTime_sec(buffer.byteBufferData).getU());
        assertEquals(0, SVP.getLatitude_deg(buffer.byteBufferData).get(), 0.0001);
        assertEquals(0, SVP.getLongitude_deg(buffer.byteBufferData).get(), 0.0001);
         
        // Profil 1
        int indexProfil = 0;
        assertEquals(0, SVP.getDepth_m(buffer.byteBufferData, indexProfil).get(), 0.0001);
        assertEquals(1460, SVP.getSoundVelocity_mPerSec(buffer.byteBufferData, indexProfil).get(), 0.0001);
        assertEquals(-99, SVP.getTemp_C(buffer.byteBufferData, indexProfil).get(), 0.0001);
        assertEquals(-99, SVP.getSalinity(buffer.byteBufferData, indexProfil).get(), 0.0001);
        
        
       // Profil 2
       indexProfil = 1;
       assertEquals(12000, SVP.getDepth_m(buffer.byteBufferData, indexProfil).get(), 0.0001);
       assertEquals(1500, SVP.getSoundVelocity_mPerSec(buffer.byteBufferData, indexProfil).get(), 0.0001);
       assertEquals(-99, SVP.getTemp_C(buffer.byteBufferData, indexProfil).get(), 0.0001);
       assertEquals(-99, SVP.getSalinity(buffer.byteBufferData, indexProfil).get(), 0.0001);
        
    }
}


