package fr.ifremer.globe.api.xsf.tests.all.datagrams;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.qualityfactor.QualityFactor;

public class TestQualityFactor extends DatagramTest {

	@Test
	public void testGetBody() throws IOException {
		DatagramBuffer buffer = getDatagram((byte)0x4f);
		
		assertEquals(QualityFactor.getSerialNumber(buffer.byteBufferData).getU(), 212);
		int nBeams = QualityFactor.getBeamCount(buffer.byteBufferData).getU();
		assertEquals(nBeams,256);
		assertEquals(QualityFactor.getNumberParamsPerBeam(buffer.byteBufferData).getU(),1);
		assertEquals(QualityFactor.getAllSamples(buffer.byteBufferData)[0],3.008775, 0.00001);
		assertEquals(QualityFactor.getAllSamples(buffer.byteBufferData)[nBeams-1],2.731718, 0.00001);
	}
}
