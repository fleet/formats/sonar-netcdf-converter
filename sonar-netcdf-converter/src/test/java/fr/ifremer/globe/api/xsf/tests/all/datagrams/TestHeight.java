package fr.ifremer.globe.api.xsf.tests.all.datagrams;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.height.Height;

public class TestHeight extends DatagramTest {

	@Test
	public void testGetBody() throws IOException {
		DatagramBuffer buffer = getDatagram((byte)0x68);
		
		assertEquals(Height.getSerialNumber(buffer.byteBufferData).getU(), 212);
		assertEquals(Height.getHeight(buffer.byteBufferData).get(), 119);
		assertEquals(Height.getHeightType(buffer.byteBufferData).getU(), 0);
	}
}
