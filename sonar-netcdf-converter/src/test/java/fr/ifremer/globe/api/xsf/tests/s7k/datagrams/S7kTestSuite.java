package fr.ifremer.globe.api.xsf.tests.s7k.datagrams;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ Test1003.class, Test1008.class, Test1010.class, Test1015.class, Test1016.class, Test7000.class,
		Test7001.class, Test7004.class, Test7006.class, Test7007.class, Test7009.class, Test7027.class, Test7030.class,
		Test7041.class, Test7057.class, Test7058.class, Test7200.class, TestS7KBaseDatagram.class })
public class S7kTestSuite {

}
