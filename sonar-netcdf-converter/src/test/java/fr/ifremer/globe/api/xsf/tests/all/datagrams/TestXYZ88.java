package fr.ifremer.globe.api.xsf.tests.all.datagrams;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.xyz88.XYZ88Depth;

public class TestXYZ88 extends DatagramTest {

	@Test
	public void testGetBody() throws IOException {
		DatagramBuffer buffer = getDatagram((byte)0x58);
		
		assertEquals(XYZ88Depth.getSerialNumber(buffer.byteBufferData).getU(), 212);
		assertEquals(XYZ88Depth.getSamplingFrequency(buffer.byteBufferData).get(), 30637.255859, 0.000001);

		// Test du premier block de beams.
		int nBeams = 1;
		assertEquals(XYZ88Depth.getAcrossDistance(buffer.byteBufferData, nBeams-1).get(), -59.421162, 0.000001);
		assertEquals(XYZ88Depth.getReflectivity(buffer.byteBufferData, nBeams-1).get(), -278);

		// Test du dernier block de beams.
		nBeams = XYZ88Depth.getValidDetectionCount(buffer.byteBufferData).getU();
		assertEquals(nBeams, 256);
		assertEquals(XYZ88Depth.getAcrossDistance(buffer.byteBufferData, nBeams-1).get(), 1.011422, 0.000001);
		assertEquals(XYZ88Depth.getReflectivity(buffer.byteBufferData, nBeams-1).get(), -244);
		
		
	}
}
