package fr.ifremer.globe.api.xsf.tests.s7k.datagrams;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramParser;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K7009;

public class Test7009 extends TestDatagram {
	
	@Test
	public void testDecode() throws IOException {
		DatagramBuffer buffer = getDataBuffer(7009);
		
		assertEquals(7009, DatagramParser.getDatagramType(buffer));
		
		assertEquals(100000.0, S7K7009.getFrequency(buffer).get(), 0.0);
		assertEquals(110267, S7K7009.getPingNumber(buffer).getU());
		assertEquals(0, S7K7009.getMultiPingSequence(buffer).getU());
		assertEquals(0.84, S7K7009.getLatitude(buffer).get(), 0.01);
		assertEquals(-0.086, S7K7009.getLongitude(buffer).get(), 0.01);
		assertEquals(0.031, S7K7009.getHeading(buffer).get(), 0.01);
		assertEquals(47.46, S7K7009.getAlongTrackDistance(buffer).get(), 0.01);
		assertEquals(-0.019, S7K7009.getAcrossTrackDistance(buffer).get(), 0.01);
		assertEquals(68.456, S7K7009.getDepth(buffer).get(), 0.01);
	}

}
