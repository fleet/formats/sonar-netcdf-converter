package fr.ifremer.globe.api.xsf.tests.all.datagrams;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.depth68.Depth68;

public class TestDepth68 extends DatagramTest {

	@Test
	public void testGetBody() throws IOException {
		DatagramBuffer buffer = getDatagram((byte)0x44);
		
		assertEquals(Depth68.getSerialNumber(buffer.byteBufferData).getU(), 213);
		assertEquals(Depth68.getTransmitTransducerDepth(buffer.byteBufferData).getU(), 148);
		assertEquals(Depth68.getValidDetectionCount(buffer.byteBufferData).getU(), 111);
		assertEquals(Depth68.getZResolution(buffer.byteBufferData).getU(), 1);
		assertEquals(Depth68.getXYResolution(buffer.byteBufferData).getU(), 1);
		assertEquals(Depth68.getSamplingFrequency(buffer.byteBufferData).getU(), 11973);

		// Test du premier block de beams.
		int nBeams = 1;
		assertEquals(Depth68.getDepth2S(buffer.byteBufferData, nBeams-1).get(), 2363);
		assertEquals(Depth68.getAlongDistance(buffer.byteBufferData, nBeams-1).get(), 128);
		assertEquals(Depth68.getAcrossDistance(buffer.byteBufferData, nBeams-1).get(), -10247);
		assertEquals(Depth68.getLengthOfDetectionWindow(buffer.byteBufferData, nBeams-1).getU(), 127);
		assertEquals(Depth68.getReflectivity(buffer.byteBufferData, nBeams-1).get(), -52);

		// Test du dernier block de beams.
		nBeams = Depth68.getValidDetectionCount(buffer.byteBufferData).getU();
		assertEquals(Depth68.getAcrossDistance(buffer.byteBufferData, nBeams-1).get(), 7644, 0.000001);
		assertEquals(Depth68.getReflectivity(buffer.byteBufferData, nBeams-1).get(), -62);
		
		
	}
}
