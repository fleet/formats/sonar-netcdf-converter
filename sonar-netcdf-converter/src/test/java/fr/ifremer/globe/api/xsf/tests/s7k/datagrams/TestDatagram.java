package fr.ifremer.globe.api.xsf.tests.s7k.datagrams;

import java.io.IOException;
import java.io.RandomAccessFile;

import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.tests.all.utils.FileFinder;

public abstract class TestDatagram {

	public DatagramBuffer getDataBuffer(long datagramType) throws IOException {
		final String path = FileFinder.getPath(String.format("s7k/datagrams/%d.s7k", datagramType));
		final RandomAccessFile raf = new RandomAccessFile(path, "r");
		
		DatagramBuffer buffer = new DatagramBuffer();
		DatagramReader.readDatagram(raf, buffer);
		raf.close();
		return buffer;
	}
}
