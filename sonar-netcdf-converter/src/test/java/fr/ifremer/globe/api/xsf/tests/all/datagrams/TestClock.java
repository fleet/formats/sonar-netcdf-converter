package fr.ifremer.globe.api.xsf.tests.all.datagrams;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.clock.Clock;


public class TestClock extends DatagramTest {

	@Test
	public void testGetBody() throws IOException {
		DatagramBuffer buffer = getDatagram((byte)0x43);
		
		assertEquals(Clock.getSerialNumber(buffer.byteBufferData).getU(), 212);
		assertEquals(Clock.getDateFromExternalClock(buffer.byteBufferData).getU(), 20141031);
		assertEquals(Clock.getTimeFromExternalClock(buffer.byteBufferData).getU(), 48974000);
		assertEquals(Clock.getPPS(buffer.byteBufferData).getInt(), 2);
	}
}
