package fr.ifremer.globe.api.xsf.tests.all.datagrams;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.attitude.Attitude;

public class TestAttitude extends DatagramTest {

	@Test
	public void testRead() throws Exception {
		DatagramBuffer buffer = getDatagram((byte)0x41);

		// check heading
		assertEquals(24053, Attitude.getCounter(buffer.byteBufferData).getU());
		assertEquals(212, Attitude.getSerialNumber(buffer.byteBufferData).getU());
		assertEquals(101, Attitude.getNumberEntries(buffer.byteBufferData).getU());
		
		// check one of the N entries
		assertEquals(50, Attitude.getTimeSinceRecordStart(buffer.byteBufferData, 5).getU());
		assertEquals(0x9090, Attitude.getSensorStatus(buffer.byteBufferData, 5).getU());
		assertEquals(91, Attitude.getRoll(buffer.byteBufferData, 5).get());
		assertEquals(-111, Attitude.getPitch(buffer.byteBufferData, 5).get());
		assertEquals(27, Attitude.getHeave(buffer.byteBufferData, 5).get());
		assertEquals(637, Attitude.getHeading(buffer.byteBufferData, 5).getU());
		
		assertEquals(1000, Attitude.getTimeSinceRecordStart(buffer.byteBufferData, 100).getU());
		assertEquals(0x9090, Attitude.getSensorStatus(buffer.byteBufferData, 100).getU());
		assertEquals(347, Attitude.getRoll(buffer.byteBufferData, 100).get());
		assertEquals(-123, Attitude.getPitch(buffer.byteBufferData, 100).get());
		assertEquals(16, Attitude.getHeave(buffer.byteBufferData, 100).get());
		assertEquals(561, Attitude.getHeading(buffer.byteBufferData, 100).getU());
		
		assertEquals(1, Attitude.getSensorDescriptor(buffer.byteBufferData).getU());
	}
}
