package fr.ifremer.globe.api.xsf.tests.kmall.datagrams;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.kmall.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.SKM;

public class TestSKMDatagrams extends DatagramTest {

    @Test
    public void testRead() throws IOException {
    	DatagramBuffer buffer = getDatagram("SKM");
        
        // part common to all datagrams
        assertEquals("#SKM", SKM.getDgmType(buffer.byteBufferData));       
        assertEquals(140, SKM.getSystemId(buffer.byteBufferData).getU());
        assertEquals(0, SKM.getDgmVersion(buffer.byteBufferData).getU());
        assertEquals(2040, SKM.getEchoSounderId(buffer.byteBufferData).getU());
        assertEquals(1473163103, SKM.getTimeSec(buffer.byteBufferData).getU());
        assertEquals(582174311, SKM.getTimeNano(buffer.byteBufferData).getU());
        
        // SKMInfo
        assertEquals(12, SKM.getNumBytesInfoPart(buffer.byteBufferData).getU());
        assertEquals(0, SKM.getSensorSystem(buffer.byteBufferData).getU());
        assertEquals(0, SKM.getSensorStatus(buffer.byteBufferData).getU());
        assertEquals(2, SKM.getSensorInputFormat(buffer.byteBufferData).getU());
        assertEquals(102, SKM.getNumSamplesArray(buffer.byteBufferData).getU());
        assertEquals(132, SKM.getNumBytesPerSample(buffer.byteBufferData).getU());
        
        // Profil 1
        // KM Binary - Generic
        int indexSample = 0;
        assertEquals("#KMB", SKM.getDgmTypeFromKMBinary(buffer.byteBufferData, indexSample));
        assertEquals(0, SKM.getNumBytesDgmFromKMBinary(buffer.byteBufferData, indexSample).getU());
        assertEquals(1, SKM.getDgmVersionFromKMBinary(buffer.byteBufferData, indexSample).getU());
        assertEquals(1473163103, SKM.getTimeFromKMBinary_sec(buffer.byteBufferData, indexSample).getU());
        assertEquals(582092736, SKM.getTimeFromKMBinary_nanosec(buffer.byteBufferData, indexSample).getU());
        assertEquals(0, SKM.getStatusFromKMBinary(buffer.byteBufferData, indexSample).getU());
        // KM Binary - Position
        assertEquals(0, SKM.getLatitude_deg(buffer.byteBufferData, indexSample).get(), 0.0001);
        assertEquals(0, SKM.getLongitude_deg(buffer.byteBufferData, indexSample).get(), 0.0001);
        assertEquals(0, SKM.getEllipsoidHeight_m(buffer.byteBufferData, indexSample).get(), 0.0001);
        // KM Binary - Attitude
        assertEquals(4, SKM.getRoll_deg(buffer.byteBufferData, indexSample).get(), 0.0001);
        assertEquals(-0.47, SKM.getPitch_deg(buffer.byteBufferData, indexSample).get(), 0.0001);
        assertEquals(3.1020001e+02, SKM.getHeading_deg(buffer.byteBufferData, indexSample).get(), 0.0001);
        assertEquals(0.02, SKM.getHeave_m(buffer.byteBufferData, indexSample).get(), 0.0001);
        // KM Binary - Rates
        assertEquals(0, SKM.getRollRate(buffer.byteBufferData, indexSample).get(), 0.0001);
        assertEquals(0, SKM.getPitchRate(buffer.byteBufferData, indexSample).get(), 0.0001);
        assertEquals(0, SKM.getYawRate(buffer.byteBufferData, indexSample).get(), 0.0001);
        // KM Binary - Velocities
        assertEquals(0, SKM.getVelNorth(buffer.byteBufferData, indexSample).get(), 0.0001);
        assertEquals(0, SKM.getVelEast(buffer.byteBufferData, indexSample).get(), 0.0001);
        assertEquals(0, SKM.getVelDown(buffer.byteBufferData, indexSample).get(), 0.0001);
        // KM Binary - Errors
        assertEquals(0, SKM.getLatitudeError_m(buffer.byteBufferData, indexSample).get(), 0.0001);
        assertEquals(0, SKM.getLongitudeError_m(buffer.byteBufferData, indexSample).get(), 0.0001);
        assertEquals(0, SKM.getEllipsoidHeightError_m(buffer.byteBufferData, indexSample).get(), 0.0001);
        assertEquals(0, SKM.getRollError_deg(buffer.byteBufferData, indexSample).get(), 0.0001);
        assertEquals(0, SKM.getPitchError_deg(buffer.byteBufferData, indexSample).get(), 0.0001);
        assertEquals(0, SKM.getHeadingError_deg(buffer.byteBufferData, indexSample).get(), 0.0001);
        assertEquals(0, SKM.getHeaveError_m(buffer.byteBufferData, indexSample).get(), 0.0001);
        // KM Binary - Acceleration
        assertEquals(0, SKM.getNorthAcceleration(buffer.byteBufferData, indexSample).get(), 0.0001);
        assertEquals(0, SKM.getEastAcceleration(buffer.byteBufferData, indexSample).get(), 0.0001);
        assertEquals(0, SKM.getDownAcceleration(buffer.byteBufferData, indexSample).get(), 0.0001);
        
        
        // KM Delayed Heave
        assertEquals(0, SKM.getTimeFromKMDelayedHeave_sec(buffer.byteBufferData, indexSample).getU());
        assertEquals(0, SKM.getTimeFromKMDelayedHeave_nanosec(buffer.byteBufferData, indexSample).getU());
        assertEquals(0, SKM.getDelayedHeave(buffer.byteBufferData, indexSample).get(), 0.0001);
      
        // Profil N
        // KM Binary - Generic
        indexSample = 101;
        assertEquals("#KMB", SKM.getDgmTypeFromKMBinary(buffer.byteBufferData, indexSample));
        assertEquals(0, SKM.getNumBytesDgmFromKMBinary(buffer.byteBufferData, indexSample).getU());
        assertEquals(1, SKM.getDgmVersionFromKMBinary(buffer.byteBufferData, indexSample).getU());
        assertEquals(1473163104, SKM.getTimeFromKMBinary_sec(buffer.byteBufferData, indexSample).getU());
        assertEquals(592096941, SKM.getTimeFromKMBinary_nanosec(buffer.byteBufferData, indexSample).getU());
        assertEquals(0, SKM.getStatusFromKMBinary(buffer.byteBufferData, indexSample).getU());
        // KM Binary - Position
        assertEquals(0, SKM.getLatitude_deg(buffer.byteBufferData, indexSample).get(), 0.0001);
        assertEquals(0, SKM.getLongitude_deg(buffer.byteBufferData, indexSample).get(), 0.0001);
        assertEquals(0, SKM.getEllipsoidHeight_m(buffer.byteBufferData, indexSample).get(), 0.0001);
        // KM Binary - Attitude
        assertEquals(3.71, SKM.getRoll_deg(buffer.byteBufferData, indexSample).get(), 0.0001);
        assertEquals(-0.6799999, SKM.getPitch_deg(buffer.byteBufferData, indexSample).get(), 0.0001);
        assertEquals(3.0989999e+02, SKM.getHeading_deg(buffer.byteBufferData, indexSample).get(), 0.0001);
        assertEquals(0.04, SKM.getHeave_m(buffer.byteBufferData, indexSample).get(), 0.0001);
        // KM Binary - Rates
        assertEquals(0, SKM.getRollRate(buffer.byteBufferData, indexSample).get(), 0.0001);
        assertEquals(0, SKM.getPitchRate(buffer.byteBufferData, indexSample).get(), 0.0001);
        assertEquals(0, SKM.getYawRate(buffer.byteBufferData, indexSample).get(), 0.0001);
        // KM Binary - Velocities
        assertEquals(0, SKM.getVelNorth(buffer.byteBufferData, indexSample).get(), 0.0001);
        assertEquals(0, SKM.getVelEast(buffer.byteBufferData, indexSample).get(), 0.0001);
        assertEquals(0, SKM.getVelDown(buffer.byteBufferData, indexSample).get(), 0.0001);
        // KM Binary - Errors
        assertEquals(0, SKM.getLatitudeError_m(buffer.byteBufferData, indexSample).get(), 0.0001);
        assertEquals(0, SKM.getLongitudeError_m(buffer.byteBufferData, indexSample).get(), 0.0001);
        assertEquals(0, SKM.getEllipsoidHeightError_m(buffer.byteBufferData, indexSample).get(), 0.0001);
        assertEquals(0, SKM.getRollError_deg(buffer.byteBufferData, indexSample).get(), 0.0001);
        assertEquals(0, SKM.getPitchError_deg(buffer.byteBufferData, indexSample).get(), 0.0001);
        assertEquals(0, SKM.getHeadingError_deg(buffer.byteBufferData, indexSample).get(), 0.0001);
        assertEquals(0, SKM.getHeaveError_m(buffer.byteBufferData, indexSample).get(), 0.0001);
        // KM Binary - Acceleration
        assertEquals(0, SKM.getNorthAcceleration(buffer.byteBufferData, indexSample).get(), 0.0001);
        assertEquals(0, SKM.getEastAcceleration(buffer.byteBufferData, indexSample).get(), 0.0001);
        assertEquals(0, SKM.getDownAcceleration(buffer.byteBufferData, indexSample).get(), 0.0001);
        
        
        // KM Delayed Heave
        // ----------------
        assertEquals(0, SKM.getTimeFromKMDelayedHeave_sec(buffer.byteBufferData, indexSample).getU());
        assertEquals(0, SKM.getTimeFromKMDelayedHeave_nanosec(buffer.byteBufferData, indexSample).getU());
        assertEquals(0, SKM.getDelayedHeave(buffer.byteBufferData, indexSample).get(), 0.0001);
 
    }

}


