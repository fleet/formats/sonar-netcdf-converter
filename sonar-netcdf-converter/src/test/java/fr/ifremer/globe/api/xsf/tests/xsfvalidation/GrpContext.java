package fr.ifremer.globe.api.xsf.tests.xsfvalidation;

import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.VendorSpecificGrp;
import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCGroup;

public class GrpContext {
	NCGroup beamGrp;
	NCGroup bathyGrp;
	NCDimension dimDetection;
	NCDimension dimBeam ;
	NCDimension dimSwath;
	NCDimension dimSector;
	NCGroup beamVendor;
	public GrpContext(NCGroup beamGrp,NCGroup bathyGrp, NCDimension dimDetection, NCDimension dimBeam, NCDimension dimSwath,
			NCDimension dimSector) throws NCException {
		super();
		this.beamGrp = beamGrp;
		this.bathyGrp = bathyGrp;
		this.dimDetection = dimDetection;
		this.dimBeam = dimBeam;
		this.dimSwath = dimSwath;
		this.dimSector = dimSector;
		beamVendor=beamGrp.getGroup(VendorSpecificGrp.GROUP_NAME);

	}
}
