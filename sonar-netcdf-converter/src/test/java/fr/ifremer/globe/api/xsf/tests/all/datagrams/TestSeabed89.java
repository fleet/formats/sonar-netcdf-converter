package fr.ifremer.globe.api.xsf.tests.all.datagrams;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.seabed89.Seabed89;

public class TestSeabed89 extends DatagramTest {

	@Test
	public void testGetBody() throws IOException {
		DatagramBuffer buffer = getDatagram((byte)0x59);
		
		assertEquals(Seabed89.getSerialNumber(buffer.byteBufferData).getU(), 212);
		assertEquals(Seabed89.getSamplingFn(buffer.byteBufferData).get(), 30637.2559, 0.0001);
		assertEquals(Seabed89.getRangeToNormalIncidence(buffer.byteBufferData).getU(), 636);
		assertEquals(Seabed89.getNormalIncidenceBS(buffer.byteBufferData).get(), -177);
		assertEquals(Seabed89.getObliqueBS(buffer.byteBufferData).get(), -254);
		assertEquals(Seabed89.getTxBeamWidth(buffer.byteBufferData).getU(), 10);
		assertEquals(Seabed89.getTVGLawCrossOver(buffer.byteBufferData).getU(), 100);
		int nBeams = Seabed89.getReceivedBeamsCounter(buffer.byteBufferData).getU();
		assertEquals(nBeams, 256);
		
		// Check Beam Block
		int iBeam = 0;
		assertEquals(Seabed89.getSortingDirection(buffer.byteBufferData, iBeam).get(), -1);
		assertEquals(Seabed89.getDetectionInfo(buffer.byteBufferData, iBeam).getU(), 1);
		assertEquals(Seabed89.getSampleCount(buffer.byteBufferData, iBeam).getU(), 153);
		// the center sample is rectified according to the sorting direction field
		assertEquals(Seabed89.getCenterSample(buffer.byteBufferData, iBeam).get(), 24);

		// Check Beam Block
		iBeam = nBeams-1;
		assertEquals(Seabed89.getSortingDirection(buffer.byteBufferData, iBeam).get(), 1);
		assertEquals(Seabed89.getDetectionInfo(buffer.byteBufferData, iBeam).getU(), 1);
		assertEquals(Seabed89.getSampleCount(buffer.byteBufferData, iBeam).getU(), 3);
		assertEquals(Seabed89.getCenterSample(buffer.byteBufferData, iBeam).get(), 2);

		// Check Sample Amplitude (1st) :
		
		// TODO: finaliser les valeurs, pas en rapport avec les valeurs du parser.
		// int offset = 32 + nBeams * 6;
		// assertEquals(Seabed89.getSampleAmplitude(buffer.byteBufferData, offset).get(), -168);
		
		/*int sumNbSamples = 0;		
		for (int k=0; k < Seabed89.getReceivedBeamsCounter(buffer.byteBufferData).getU(); k++)
		{
			sumNbSamples += Seabed89.getSampleCount(buffer.byteBufferData, k).getU();
			// System.out.println("NbSamples(" + k + ")=" +Seabed89.getSamplesPerBeamCounter(buffer.byteBufferData, k).getU());
		}*/
		
		
		// offset += (sumNbSamples-1)*2;
		// TODO: finaliser les valeurs, pas en rapport avec les valeurs du parser.
		// assertEquals(Seabed89.getSampleAmplitude(buffer.byteBufferData, offset).get(), -337);

	}
}
