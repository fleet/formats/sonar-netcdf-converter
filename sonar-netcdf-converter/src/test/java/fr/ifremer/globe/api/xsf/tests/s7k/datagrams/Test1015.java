package fr.ifremer.globe.api.xsf.tests.s7k.datagrams;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramParser;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K1015;

public class Test1015 extends TestDatagram {
	
	@Test
	public void testDecode() throws IOException {
		DatagramBuffer buffer = getDataBuffer(1015);
		
		assertEquals(1015, DatagramParser.getDatagramType(buffer));
		
		assertEquals(1, S7K1015.getVerticalReference(buffer).getU());
		assertEquals(0.84, S7K1015.getLatitude(buffer).get(), 0.01);
		assertEquals(-0.08, S7K1015.getLongitude(buffer).get(), 0.01);
		assertEquals(0.0, S7K1015.getHorizontalPositionAccuracy(buffer).get(), 0.0);
		assertEquals(0.44, S7K1015.getVesselHeight(buffer).get(), 0.01);
		assertEquals(1.0, S7K1015.getHeightAccuracy(buffer).get(), 0.01);
		assertEquals(4.51, S7K1015.getSpeedOverGround(buffer).get(), 0.01);
		assertTrue(Float.isNaN(S7K1015.getCourseOverGround(buffer).get()));
		assertEquals(0.03, S7K1015.getHeading(buffer).get(), 0.01);
	}
	
}
