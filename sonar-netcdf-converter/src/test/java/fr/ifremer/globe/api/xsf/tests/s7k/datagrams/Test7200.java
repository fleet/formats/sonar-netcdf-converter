package fr.ifremer.globe.api.xsf.tests.s7k.datagrams;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramParser;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K7200;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K7200.Device;

public class Test7200 extends TestDatagram {

	@Test
	public void testDecode() throws IOException {
		DatagramBuffer buffer = getDataBuffer(7200);
		
		assertEquals(7200, DatagramParser.getDatagramType(buffer));
		
		byte[] fileId = {125, 87, -33, 51, -20, 42, 62, -87, 111, 77, -80, -49, 67, 47, 48, -13};
		assertArrayEquals(fileId, S7K7200.getFileIdentifier(buffer));
		assertEquals(1, S7K7200.getVersionNumber(buffer).getU());
		byte[] sessionId = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		assertArrayEquals(sessionId, S7K7200.getSessionIdentifier(buffer));
		assertEquals(278, S7K7200.getRecordDataSize(buffer).getU());
		assertEquals(2, S7K7200.getNumberOfDevices(buffer).getU());
		assertEquals("PDS2000", S7K7200.getRecordingName(buffer));
		assertEquals("3,7,0,30", S7K7200.getRecordingProgramVersionNumber(buffer));
		assertEquals("", S7K7200.getUserDefinedName(buffer));
		assertEquals("", S7K7200.getNotes(buffer));
		
		final Device[] devices = S7K7200.getDevices(buffer);
		assertEquals(2, devices.length);
		assertEquals(7003, devices[0].deviceIdentifier.getU());
		assertEquals(0, devices[0].systemEnumerator.getU());
		assertEquals(7111, devices[1].deviceIdentifier.getU());
		assertEquals(0, devices[1].systemEnumerator.getU());
	}
}
