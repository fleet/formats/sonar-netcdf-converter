package fr.ifremer.globe.api.xsf.tests.all.datagrams;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.position.Position;

public class TestPosition extends DatagramTest {

	@Test
	public void testGetBody() throws IOException {
		DatagramBuffer buffer = getDatagram((byte)0x50);
		
		assertEquals(212, Position.getSerialNumber(buffer.byteBufferData).getU());
		assertEquals(624, Position.getHeading(buffer.byteBufferData).getU());
		assertEquals("GPGGA,133613.00,4819.79097,N,00440.87219,W,5,14,00.2,000.000,M,0.0,M,0.0,0000*57",
				Position.getDataFromSensor(buffer.byteBufferData));
		assertEquals(5, Position.getSensorQualityIndicator(buffer.byteBufferData).get());
	}
}
