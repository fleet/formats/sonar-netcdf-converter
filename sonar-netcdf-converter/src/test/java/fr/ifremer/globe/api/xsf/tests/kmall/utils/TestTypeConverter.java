package fr.ifremer.globe.api.xsf.tests.kmall.utils;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.common.datagram.PulseLengthModeMapper;
import fr.ifremer.globe.api.xsf.converter.common.datagram.PulseLengthModeMapper.AllPulseLengthMode;
import fr.ifremer.globe.utils.exception.GIOException;

public class TestTypeConverter {

	@Test
	public void testPulseLengthMode2040C() throws GIOException {
		assertEquals(AllPulseLengthMode.VERY_SHORT_CW.value,
				PulseLengthModeMapper.AllPulseLengthMode.from2040CValue(0x2).value);
		assertEquals(AllPulseLengthMode.VERY_SHORT_CW.value,
				PulseLengthModeMapper.AllPulseLengthMode.from2040CValue(0x0).value);
		assertEquals(AllPulseLengthMode.SHORT_CW.value,
				PulseLengthModeMapper.AllPulseLengthMode.from2040CValue(0x10).value);
		assertEquals(AllPulseLengthMode.SHORT_CW.value,
				PulseLengthModeMapper.AllPulseLengthMode.from2040CValue(0x12).value);
		assertEquals(AllPulseLengthMode.MEDIUM_CW.value,
				PulseLengthModeMapper.AllPulseLengthMode.from2040CValue(0x20).value);
		assertEquals(AllPulseLengthMode.LONG_CW.value,
				PulseLengthModeMapper.AllPulseLengthMode.from2040CValue(0x30).value);
		assertEquals(AllPulseLengthMode.VERY_LONG_CW.value,
				PulseLengthModeMapper.AllPulseLengthMode.from2040CValue(0x40).value);
		assertEquals(AllPulseLengthMode.EXTRA_LONG_CW.value,
				PulseLengthModeMapper.AllPulseLengthMode.from2040CValue(0x50).value);
		assertEquals(AllPulseLengthMode.SHORT_FM.value,
				PulseLengthModeMapper.AllPulseLengthMode.from2040CValue(0x6F).value);
		assertEquals(AllPulseLengthMode.SHORT_FM.value,
				PulseLengthModeMapper.AllPulseLengthMode.from2040CValue(0x60).value);
		assertEquals(AllPulseLengthMode.LONG_FM.value,
				PulseLengthModeMapper.AllPulseLengthMode.from2040CValue(0x70).value);
		assertEquals(AllPulseLengthMode.LONG_FM.value,
				PulseLengthModeMapper.AllPulseLengthMode.from2040CValue(0xFF).value);
	}

	@Test
	public void testPulseLengthMode2040() {
		assertEquals(AllPulseLengthMode.SHORT_CW.value,
				PulseLengthModeMapper.AllPulseLengthMode.from2040Value(0xF3).value);
		assertEquals(AllPulseLengthMode.SHORT_CW.value,
				PulseLengthModeMapper.AllPulseLengthMode.from2040Value(0x0).value);
		assertEquals(AllPulseLengthMode.MEDIUM_CW.value,
				PulseLengthModeMapper.AllPulseLengthMode.from2040Value(0x4).value);
		assertEquals(AllPulseLengthMode.MEDIUM_CW.value,
				PulseLengthModeMapper.AllPulseLengthMode.from2040Value(0xF5).value);
		assertEquals(AllPulseLengthMode.LONG_CW.value,
				PulseLengthModeMapper.AllPulseLengthMode.from2040Value(0x8).value);
		assertEquals(AllPulseLengthMode.SHORT_FM.value,
				PulseLengthModeMapper.AllPulseLengthMode.from2040Value(0xC).value);
	}

}
