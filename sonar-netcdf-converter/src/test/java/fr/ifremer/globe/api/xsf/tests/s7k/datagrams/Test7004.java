package fr.ifremer.globe.api.xsf.tests.s7k.datagrams;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramParser;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K7004;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K7004.ReceivedBeamWidthAndSteering;

public class Test7004 extends TestDatagram {
	
	@Test
	public void testDecode() throws IOException {
		DatagramBuffer buffer = getDataBuffer(7004);
		
		assertEquals(7004, DatagramParser.getDatagramType(buffer));
		
		assertEquals(0, S7K7004.getSonarSerialNumber(buffer).getLong());
		assertEquals(301, S7K7004.getNumberOfReceivedBeams(buffer).getU());
		
		ReceivedBeamWidthAndSteering[] beams = S7K7004.getReceivedBeamWidthAndSteering(buffer);
		assertEquals(0.0, beams[0].beamVerticalDirectionAngle, 0.0);
		assertEquals(-1.3274, beams[0].beamHorizontalDirectionAngle, 0.0001);
		assertEquals(0.0174, beams[0].minus3dBBeamWidthY, 0.0001);
		assertEquals(0.0088, beams[0].minux3dBBeamWidthX, 0.0001);
		assertEquals(0.0, beams[300].beamVerticalDirectionAngle, 0.0);
		assertEquals(1.32742, beams[300].beamHorizontalDirectionAngle, 0.0001);
		assertEquals(0.0174, beams[300].minus3dBBeamWidthY, 0.0001);
		assertEquals(0.0088, beams[300].minux3dBBeamWidthX, 0.0001);
	}

}
