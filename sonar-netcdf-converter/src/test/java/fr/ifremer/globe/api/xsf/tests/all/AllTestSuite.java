package fr.ifremer.globe.api.xsf.tests.all;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import fr.ifremer.globe.api.xsf.tests.all.converter.TestSampleALLFiles;



@RunWith(Suite.class)
@SuiteClasses({TestSampleALLFiles.class})
public class AllTestSuite {

}
