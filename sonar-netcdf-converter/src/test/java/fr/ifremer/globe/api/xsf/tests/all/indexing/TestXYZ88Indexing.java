package fr.ifremer.globe.api.xsf.tests.all.indexing;

import static org.junit.Assert.assertEquals;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.all.datagram.xyz88.XYZ88DepthCompletePingMetadata;
import fr.ifremer.globe.api.xsf.converter.common.exceptions.UnsupportedSounderException;
import fr.ifremer.globe.api.xsf.tests.all.utils.FileFinder;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;

public class TestXYZ88Indexing {

	@Test
	public void testWCIndexing() throws FileNotFoundException, IOException, UnsupportedSounderException {
		AllFile metadata = new AllFile(FileFinder.getPath("/all/depth88.all"));

		assertEquals(7, metadata.getXyzdepth().datagramCount);
		assertEquals(4, metadata.getXyzdepth().getPings().size());
		assertEquals(512, metadata.getXyzdepth().getMaxBeamCount());

		XYZ88DepthCompletePingMetadata ping = metadata.getXyzdepth().getPing(new SwathIdentifier(49736, 0));
		assertEquals(true, ping.isComplete(metadata.getGlobalMetadata().getSerialNumbers()));

		ping = metadata.getXyzdepth().getPing(new SwathIdentifier(49737, 0));
		assertEquals(true, ping.isComplete(metadata.getGlobalMetadata().getSerialNumbers()));

		ping = metadata.getXyzdepth().getPing(new SwathIdentifier(49738, 0));
		assertEquals(true, ping.isComplete(metadata.getGlobalMetadata().getSerialNumbers()));

		ping = metadata.getXyzdepth().getPing(new SwathIdentifier(49739, 0));
		// the last ping misses one datagram
		assertEquals(false, ping.isComplete(metadata.getGlobalMetadata().getSerialNumbers()));
	}
}
