package fr.ifremer.globe.api.xsf.tests.api;

import static java.lang.System.out;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfBase;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.api.NetcdfVariable;
import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCFile.Mode;

/**
 * This class provides tests to verify the read functionalities of the XSF API.
 */
public class ReadingTests {

	private static final String TEST_DIR = "file" + File.separator + "generated" + File.separator;

	@BeforeClass
	public static void before() throws IOException {
		FileUtils.forceMkdir(Paths.get(TEST_DIR).toFile());
	}

	@AfterClass
	public static void after() throws IOException {
		FileUtils.cleanDirectory(Paths.get(TEST_DIR).toFile());
	}

	/** Test simple file with a 2D integer variable (=layer) */
	@Test
	public void testSimpleFile() throws NCException, FileNotFoundException, IOException {
		String filePath = TEST_DIR + "test_read_simple" + XsfBase.EXTENSION_XSF;
		if (!Files.exists(Paths.get(filePath)))
			FakeFileGenerator.makeFile(filePath, 20, 20, DataType.INT);
		readingTest("testSimpleFile", filePath);

	}

	/** Test 1Go file with a 2D integer variable (=layer) */
	@Test
	public void test1GoFile() throws NCException, FileNotFoundException, IOException {
		String filePath = TEST_DIR + "test_read_1go" + XsfBase.EXTENSION_XSF;
		if (!Files.exists(Paths.get(filePath)))
			FakeFileGenerator.makeFile(filePath, 16384, 16384, DataType.INT);
		readingTest("test1GoFile", filePath);
	}

	/** Test file with add offset */
	@Test
	public void testAddOffsetFloat() throws NCException, FileNotFoundException, IOException {
		// Test with a float offset
		String filePath = TEST_DIR + "test_read_add_offset_f" + XsfBase.EXTENSION_XSF;
		if (!Files.exists(Paths.get(filePath)))
			FakeFileGenerator.makeFile(filePath, 200, 200, DataType.INT, 5f, null);
		readingTest("testAddOffsetFloat", filePath);
	}

	/** Test file with add offset (double value) */
	@Test
	public void testAddOffsetDouble() throws NCException, FileNotFoundException, IOException {
		String filePath = TEST_DIR + "test_read_add_offset_d" + XsfBase.EXTENSION_XSF;
		if (!Files.exists(Paths.get(filePath)))
			FakeFileGenerator.makeFile(filePath, 200, 200, DataType.INT, 5d, null);
		readingTest("testAddOffsetDouble", filePath);
	}

	/** Test file with scale factor */
	@Test
	public void testScaleFactorFloat() throws NCException, FileNotFoundException, IOException {
		String filePath = TEST_DIR + "test_read_scale_factor_f" + XsfBase.EXTENSION_XSF;
		if (!Files.exists(Paths.get(filePath)))
			FakeFileGenerator.makeFile(filePath, 200, 200, DataType.INT, null, 0.5f);
		readingTest("testScaleFactorFloat", filePath);
	}

	/** Test file with scale factor (double value) */
	@Test
	public void testScaleFactorDouble() throws NCException, FileNotFoundException, IOException {
		String filePath = TEST_DIR + "test_read_scale_factor_d" + XsfBase.EXTENSION_XSF;
		if (!Files.exists(Paths.get(filePath)))
			FakeFileGenerator.makeFile(filePath, 200, 200, DataType.INT, null, 0.5d);
		readingTest("testScaleFactorDouble", filePath);
	}

	/** Test file with add offset and scale factor */
	@Test
	public void testScaleFactorAndAddOffset() throws NCException, FileNotFoundException, IOException {
		String filePath = TEST_DIR + "test_read_add_offset_scale_factor_f" + XsfBase.EXTENSION_XSF;
		if (!Files.exists(Paths.get(filePath)))
			FakeFileGenerator.makeFile(filePath, 200, 200, DataType.INT, 5f, 2f);
		readingTest("testScaleFactorAndAddOffset", filePath);
	}

	/** Test file with add offset and scale factor (double values) */
	@Test
	public void testScaleFactorAndAddOffsetDoubles() throws NCException, FileNotFoundException, IOException {
		String filePath = TEST_DIR + "test_read_add_offset_scale_factor_d" + XsfBase.EXTENSION_XSF;
		if (!Files.exists(Paths.get(filePath)))
			FakeFileGenerator.makeFile(filePath, 200, 200, DataType.INT, 5d, 2d);
		readingTest("testScaleFactorAndAddOffsetDoubles", filePath);
	}

	/**
	 * Reading test: expect a test file which contains a variable (TEST_VAR) with values equal to their indexes (before
	 * the unpacking with scale factor and add offset).
	 */
	@SuppressWarnings("incomplete-switch")
	private void readingTest(String testName, String filePath) throws FileNotFoundException, IOException, NCException {

		long startTime = System.currentTimeMillis();
		MappedByteBuffer mappedFile = null;

		try (NetcdfFile file = NetcdfFile.open(filePath, Mode.readwrite)) {

			// Retrieve tested file's dimensions
			long dimX = file.getDimension("TEST_DIM_X").getLength();
			long dimY = file.getDimension("TEST_DIM_Y").getLength();

			NetcdfVariable var = file.getVariable("TEST_VAR");

			// Get scale factor and add offset
			double scaleFactor = 1d;
			double addOffset = 0d;
			List<String> attributes = Arrays.asList(var.getAttributeNames());
			DataType destBufferType = var.getRawDataType();

			if (attributes.contains("scale_factor")) {
				switch (var.getAttributeDataType("scale_factor")) {
				case FLOAT:
					scaleFactor = var.getAttributeFloat("scale_factor");
					if (destBufferType.getSize() <= DataType.FLOAT.getSize())
						destBufferType = DataType.FLOAT;
					break;
				case DOUBLE:
					scaleFactor = var.getAttributeDouble("scale_factor");
					if (destBufferType.getSize() < DataType.DOUBLE.getSize())
						destBufferType = DataType.DOUBLE;
					break;
				}
			}

			if (attributes.contains("add_offset")) {
				switch (var.getAttributeDataType("add_offset")) {
				case FLOAT:
					addOffset = var.getAttributeFloat("add_offset");
					if (destBufferType.getSize() <= DataType.FLOAT.getSize())
						destBufferType = DataType.FLOAT;
					break;
				case DOUBLE:
					addOffset = var.getAttributeDouble("add_offset");
					if (destBufferType.getSize() < DataType.DOUBLE.getSize())
						destBufferType = DataType.DOUBLE;
					break;
				}
			}

			// Create mapped byte buffer
			mappedFile = FakeFileGenerator.buildMappedByteBuffer(dimX * dimY * destBufferType.getSize());

			// Read XSF variable
			var.read(new long[] { dimX, dimY }, mappedFile, destBufferType, Optional.empty());

			// Print reading duration (avoid / by 0)
			long bytesReadPerSec = (System.currentTimeMillis() - startTime != 0)
					? mappedFile.limit() * 1000L / (System.currentTimeMillis() - startTime)
					: -1;
			out.format(
					"[%s] read -> speed :  %,d bytes/sec , duration : %,d ms, buffer size: %,d bytes, data type: %s \n",
					testName, bytesReadPerSec, System.currentTimeMillis() - startTime, mappedFile.limit(),
					destBufferType);

			// Check values
			// Expected: index * scaleFactor + addOffset = mappedFile.get(index)
			mappedFile.position(0);
			while (mappedFile.hasRemaining()) {
				int index = mappedFile.position() / destBufferType.getSize();
				switch (destBufferType) {
				case FLOAT:
					float fValue = mappedFile.getFloat();
					assertTrue("For index : " + index + ", expected: " + (float) (index * scaleFactor + addOffset)
							+ " , got:  " + fValue, (float) (index * scaleFactor + addOffset) == fValue);
					break;
				case DOUBLE:
					double dValue = mappedFile.getDouble();
					assertTrue("For index : " + index + ", expected: " + (index * scaleFactor + addOffset) + " , got:  "
							+ dValue, index * scaleFactor + addOffset == dValue);
					break;
				case BYTE:
					byte bValue = mappedFile.get();
					assertTrue("For index : " + index + ", expected: " + index + " , got:  " + bValue, index == bValue);
					break;
				case SHORT:
					short sValue = mappedFile.getShort();
					assertTrue("For index : " + index + ", expected: " + index + " , got:  " + sValue, index == sValue);
					break;
				case INT:
					int iValue = mappedFile.getInt();
					assertTrue("For index : " + index + ", expected: " + index + " , got:  " + iValue, index == iValue);
					break;
				case LONG:
					long lValue = mappedFile.getLong();
					assertTrue("For index : " + index + ", expected: " + index + " , got:  " + lValue, index == lValue);
					break;
				}
			}

		} finally {
			mappedFile = null;
			System.gc();
		}

	}
}
