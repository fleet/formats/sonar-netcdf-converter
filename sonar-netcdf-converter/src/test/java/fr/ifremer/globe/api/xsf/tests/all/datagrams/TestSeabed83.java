package fr.ifremer.globe.api.xsf.tests.all.datagrams;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.seabed83.Seabed83;

public class TestSeabed83 extends DatagramTest {

	@Test
	public void testGetBody() throws IOException {
		DatagramBuffer buffer = getDatagram((byte)0x53);
		
		assertEquals(Seabed83.getSerialNumber(buffer.byteBufferData).getU(), 213);
		assertEquals(Seabed83.getMeanAbsorptionCoefficient(buffer.byteBufferData).getU(), 1738);
		assertEquals(Seabed83.getPulseLength(buffer.byteBufferData).getU(), 336);
		assertEquals(Seabed83.getRangeToNormalIncidence(buffer.byteBufferData).getU(), 335);
		assertEquals(Seabed83.getStartRangeSampleTVGRamp(buffer.byteBufferData).getU(), 0);
		assertEquals(Seabed83.getStopRangeSampleTVGRamp(buffer.byteBufferData).getU(), 2901);
		assertEquals(Seabed83.getNormalIncidenceBS(buffer.byteBufferData).get(), -9);
		assertEquals(Seabed83.getObliqueBS(buffer.byteBufferData).get(), -14);
		assertEquals(Seabed83.getTxBeamWidth(buffer.byteBufferData).getU(), 32);
		assertEquals(Seabed83.getTVGLawCrossOver(buffer.byteBufferData).getU(), 249);
		int nBeams = Seabed83.getReceivedBeamsCounter(buffer.byteBufferData).getU();
		assertEquals(nBeams, 111);
		
		// Check Beam Block
		int iBeam = 0;
		assertEquals(Seabed83.getBeamIndex(buffer.byteBufferData, iBeam).getU(), 0);
		assertEquals(Seabed83.getSortingDirection(buffer.byteBufferData, iBeam).get(), -1);
		assertEquals(Seabed83.getSampleCount(buffer.byteBufferData, iBeam).getU(), 106);
	    if (Seabed83.getSortingDirection(buffer.byteBufferData, iBeam).get() > 0) {
	    	assertEquals(Seabed83.getCenterSample(buffer.byteBufferData, iBeam).get(), 84);
	    }
	    else
	    {
	    	assertEquals(Seabed83.getCenterSample(buffer.byteBufferData, iBeam).get(), 106-84);	    	
	    }
		// Check Beam Block
		iBeam = nBeams-1;
		assertEquals(Seabed83.getBeamIndex(buffer.byteBufferData, iBeam).getU(), nBeams-1);
		assertEquals(Seabed83.getSortingDirection(buffer.byteBufferData, iBeam).get(), -1);
		assertEquals(Seabed83.getSampleCount(buffer.byteBufferData, iBeam).getU(), 24);
	    if (Seabed83.getSortingDirection(buffer.byteBufferData, iBeam).get() > 0) {
			assertEquals(Seabed83.getCenterSample(buffer.byteBufferData, iBeam).get(), 23);
	    }
	    else
	    {
	    	assertEquals(Seabed83.getCenterSample(buffer.byteBufferData, iBeam).get(), 24-23);	    	
	    }
	    
		// Check Sample Amplitude (1st) :
	    /*
		int offset = 32 + nBeams * 6;
		
		// TODO: finaliser les valeurs, pas en rapport avec les valeurs du parser.
		// assertEquals(Seabed89.getSampleAmplitude(buffer.byteBufferData, offset).get(), -168);
		
		int sumNbSamples = 0;		
		for (int k=0; k < Seabed83.getReceivedBeamsCounter(buffer.byteBufferData).getU(); k++)
		{
			sumNbSamples += Seabed83.getSampleCount(buffer.byteBufferData, k).getU();
			// System.out.println("NbSamples(" + k + ")=" +Seabed89.getSamplesPerBeamCounter(buffer.byteBufferData, k).getU());
		}*/
		
		//offset += (sumNbSamples-1)*2;
		// TODO: finaliser les valeurs, pas en rapport avec les valeurs du parser.
		// assertEquals(Seabed89.getSampleAmplitude(buffer.byteBufferData, offset).get(), -337);
		

	}
}
