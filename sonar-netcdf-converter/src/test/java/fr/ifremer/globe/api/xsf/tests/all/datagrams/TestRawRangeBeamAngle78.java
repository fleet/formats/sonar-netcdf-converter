package fr.ifremer.globe.api.xsf.tests.all.datagrams;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.rawrange78.RawRange78;

public class TestRawRangeBeamAngle78 extends DatagramTest {

	@Test
	public void testGetBody() throws IOException {
		DatagramBuffer buffer = getDatagram((byte)0x4e);
		
		assertEquals(RawRange78.getSerialNumber(buffer.byteBufferData).getU(), 212);
		int nxTxSectors = RawRange78.getNTxTransmitSectorCounter(buffer.byteBufferData).getU();
		assertEquals(nxTxSectors, 2);
		assertEquals(RawRange78.getValidBeamCounter(buffer.byteBufferData).getU(), 256);
		int nBeams = RawRange78.getTotalBeamCount(buffer.byteBufferData).getU();
		assertEquals(nBeams, 256);
		assertEquals(RawRange78.getSamplingFn(buffer.byteBufferData).get(), 30637.255859, 0.00001);
		assertEquals(RawRange78.getDscale(buffer.byteBufferData).getU(), 0);
		
		// Check Tx Sector
		int iTxSector = 0;
		assertEquals(RawRange78.getTxTilt(buffer.byteBufferData, iTxSector).get(), 12);
		assertEquals(RawRange78.getFocusRange(buffer.byteBufferData, iTxSector).getU(), 252);
		assertEquals(RawRange78.getSignalLength(buffer.byteBufferData, iTxSector).get(), 0.000101, 0.00001);
		assertEquals(RawRange78.getSectorTransmitDelay(buffer.byteBufferData, iTxSector).get(), 2.18279e-11, 0.00001);
		assertEquals(RawRange78.getTxCenterFrequency(buffer.byteBufferData, iTxSector).get(), 190000, 0.00001);
		assertEquals(RawRange78.getMeanAbsorptionCoef(buffer.byteBufferData, iTxSector).getU(), 6586);
		assertEquals(RawRange78.getWaveFormIdentifier(buffer.byteBufferData, iTxSector).getU(), 0);
		assertEquals(RawRange78.getTxSectorCounter(buffer.byteBufferData, iTxSector).getU(), 0);
		assertEquals(RawRange78.getSignalBandwidth(buffer.byteBufferData, iTxSector).get(), 14286, 0.00001);

		// Check Tx Sector
		iTxSector = nxTxSectors - 1;
		assertEquals(RawRange78.getTxTilt(buffer.byteBufferData, iTxSector).get(), 12);
		assertEquals(RawRange78.getFocusRange(buffer.byteBufferData, iTxSector).getU(), 253);
		assertEquals(RawRange78.getSignalLength(buffer.byteBufferData, iTxSector).get(), 0.000101, 0.00001);
		assertEquals(RawRange78.getSectorTransmitDelay(buffer.byteBufferData, iTxSector).get(), 2.18279e-11, 0.00001);
		assertEquals(RawRange78.getTxCenterFrequency(buffer.byteBufferData, iTxSector).get(), 220000, 0.00001);
		assertEquals(RawRange78.getMeanAbsorptionCoef(buffer.byteBufferData, iTxSector).getU(), 7197);
		assertEquals(RawRange78.getWaveFormIdentifier(buffer.byteBufferData, iTxSector).getU(), 0);
		assertEquals(RawRange78.getTxSectorCounter(buffer.byteBufferData, iTxSector).getU(), 2);
		assertEquals(RawRange78.getSignalBandwidth(buffer.byteBufferData, iTxSector).get(), 14286, 0.00001);

		// Check Entries Block (Samples)
		int iBeam = 0;
		int offset = 32 + nxTxSectors * 24 + iBeam * 16;	
		assertEquals(RawRange78.getBeamPointingAngle(buffer.byteBufferData, offset).get(), 3533);
		assertEquals(RawRange78.getTxSectorIndex(buffer.byteBufferData, offset).getU(), 0);
		assertEquals(RawRange78.getDetectionInfo(buffer.byteBufferData, offset).getU(), 1);
		assertEquals(RawRange78.getDetectionLength(buffer.byteBufferData, offset).getU(), 496);
		assertEquals(RawRange78.getQualityFactor(buffer.byteBufferData, offset).getU(), 16);
		assertEquals(RawRange78.getDcorr(buffer.byteBufferData, offset).get(), 0);
		assertEquals(RawRange78.getRange(buffer.byteBufferData, offset).get(), 0.0811928, 0.00001);
		assertEquals(RawRange78.getReflectivity(buffer.byteBufferData, offset).get(), -278);

		iBeam = nBeams - 1;
		offset = 32 + nxTxSectors * 24 + iBeam * 16;	
		assertEquals(RawRange78.getBeamPointingAngle(buffer.byteBufferData, offset).get(), -4450);
		assertEquals(RawRange78.getTxSectorIndex(buffer.byteBufferData, offset).getU(), 1);
		assertEquals(RawRange78.getDetectionInfo(buffer.byteBufferData, offset).getU(), 1);
		assertEquals(RawRange78.getDetectionLength(buffer.byteBufferData, offset).getU(), 10);
		assertEquals(RawRange78.getQualityFactor(buffer.byteBufferData, offset).getU(), 6);
		assertEquals(RawRange78.getDcorr(buffer.byteBufferData, offset).get(), 0);
		assertEquals(RawRange78.getRange(buffer.byteBufferData, offset).get(), 0.020868, 0.00001);
		assertEquals(RawRange78.getReflectivity(buffer.byteBufferData, offset).get(), -244);

	}
}
