package fr.ifremer.globe.api.xsf.tests.s7k.datagrams;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramParser;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K7030;

public class Test7030 extends TestDatagram {
	
	@Test
	public void testDecode() throws IOException {
		DatagramBuffer buff = getDataBuffer(7030);
		
		assertEquals(7030, DatagramParser.getDatagramType(buff));
		
		assertEquals(100000.0, S7K7030.getFrequency(buff).get(), 0.0);
		assertEquals("Version x.xx", S7K7030.getFirmwareVersion(buff));
		assertEquals("3,7,0,30", S7K7030.getSoftwareVersionInfo(buff));
		assertEquals("Version z.zz", S7K7030.get7KSoftwareVersionInfo(buff));
		assertEquals("0.54", S7K7030.getRecordProtocolVersionInfo(buff));
		assertEquals(0.02, S7K7030.getTransmitArrayX(buff).get(), 0.001);
		assertEquals(47.47, S7K7030.getTransmitArrayY(buff).get(), 0.001);
		assertEquals(-6.45, S7K7030.getTransmitArrayZ(buff).get(), 0.001);
		assertEquals(-0.003, S7K7030.getTransmitArrayRoll(buff).get(), 0.001);
		assertEquals(0.008, S7K7030.getTransmitArrayPitch(buff).get(), 0.001);
		assertEquals(0.003, S7K7030.getTransmitArrayHeading(buff).get(), 0.001);
		assertEquals(0.02, S7K7030.getReceiveArrayX(buff).get(), 0.001);
		assertEquals(47.47, S7K7030.getReceiveArrayY(buff).get(), 0.001);
		assertEquals(-6.45, S7K7030.getReceiveArrayZ(buff).get(), 0.001);
		assertEquals(-0.003, S7K7030.getReceiveArrayRoll(buff).get(), 0.001);
		assertEquals(0.008, S7K7030.getReceiveArrayPitch(buff).get(), 0.001);
		assertEquals(0.003, S7K7030.getReceiveArrayHeading(buff).get(), 0.001);
		assertEquals(0.0, S7K7030.getMotionSensorX(buff).get(), 0.0);
		assertEquals(0.0, S7K7030.getMotionSensorY(buff).get(), 0.0);
		assertEquals(0.0, S7K7030.getMotionSensorZ(buff).get(), 0.0);
		assertEquals(0.0, S7K7030.getMotionSensorRollCalibration(buff).get(), 0.001);
		assertEquals(0.0, S7K7030.getMotionSensorPitchCalibration(buff).get(), 0.001);
		assertEquals(0.0, S7K7030.getMotionSensorHeadingCalibration(buff).get(), 0.001);
		assertEquals(12, S7K7030.getMotionSensorTimeDelay(buff).getU());
		assertEquals(0.0, S7K7030.getPositionSensorX(buff).get(), 0.0);
		assertEquals(0.0, S7K7030.getPositionSensorY(buff).get(), 0.0);
		assertEquals(0.0, S7K7030.getPositionSensorZ(buff).get(), 0.0);
		assertEquals(0, S7K7030.getPositionSensorTimeDelay(buff).getU());
		assertEquals(-0.85, S7K7030.getWaterLineVerticalOffset(buff).get(), 0.001);
	}

}
