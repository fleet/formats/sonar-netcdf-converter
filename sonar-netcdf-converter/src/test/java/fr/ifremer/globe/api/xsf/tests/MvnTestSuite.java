package fr.ifremer.globe.api.xsf.tests;

import fr.ifremer.globe.api.xsf.tests.all.AllTestSuite;
import fr.ifremer.globe.api.xsf.tests.all.converter.TestConvertAllToMbg;
import fr.ifremer.globe.api.xsf.tests.all.utils.TestSwathIndexGenerator;
import fr.ifremer.globe.api.xsf.tests.kmall.datagrams.KmallTestSuite;
import fr.ifremer.globe.api.xsf.tests.s7k.converter.TestConvertS7kToMbg;
import fr.ifremer.globe.api.xsf.tests.s7k.datagrams.S7kTestSuite;
import fr.ifremer.globe.api.xsf.tests.xsfvalidation.XSFKongsbergFormatValidation;
import fr.ifremer.globe.api.xsf.tests.xsfvalidation.XSFResonValidation;
import fr.ifremer.globe.soundingsfileconverter.SonarNetcdfConverter;
import fr.ifremer.globe.utils.cache.TemporaryCache;
import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import java.io.IOException;

/**
 * Test suite for maven , declared test will be launched while compiling with command mvn integration-test
 */
@RunWith(Suite.class)
@SuiteClasses({
        // remove benchmark classes
        // ReadingTests.class,
        // WritingTests.class,
        TestSwathIndexGenerator.class, //
        // FIXME these tests have been disable the 20/07/2020 due to issue with developments in progress
        XSFResonValidation.class,
        XSFKongsbergFormatValidation.class,
        S7kTestSuite.class, //
        KmallTestSuite.class, //
        AllTestSuite.class, //
        // test MBG converters FIXME: add test files to GIT
        TestConvertAllToMbg.class, TestConvertS7kToMbg.class})
public class MvnTestSuite {
    @BeforeClass
    public static void init() throws IOException {
        SonarNetcdfConverter.extractNativeLibs(FileUtils.getTempDirectory());
    }

    @AfterClass
    public static void cleanUp() {
        TemporaryCache.cleanAllFiles();
    }
}
