package fr.ifremer.globe.api.xsf.tests.s7k.datagrams;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramParser;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K7041;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K7041.WCData;

public class Test7041 extends TestDatagram {

	@Test
	public void testDecode() throws IOException {
		DatagramBuffer buffer = getDataBuffer(7041);

		assertEquals(7041, DatagramParser.getDatagramType(buffer));
		assertEquals(0, S7K7041.getSonarId(buffer).getLong());
		assertEquals(367, S7K7041.getPingNumber(buffer).getU());
		assertEquals(1, S7K7041.getMultipingSequence(buffer).getU());
		assertEquals(880, S7K7041.getNumberOfBeams(buffer).getU());
		assertEquals(0x0D, S7K7041.getFlags(buffer).getU());
		assertEquals(430.16, S7K7041.getSampleRate(buffer).get(), 0.01);

		WCData dat = S7K7041.getData(buffer);
		assertEquals(880, dat.beams.length);

		assertEquals(0, dat.beams[0].beamNumber);
		assertEquals(512, dat.beams[0].numberOfSample);

		assertEquals(-2, dat.beams[0].samples[1]);
		assertEquals(-5, dat.beams[0].samples[2]);
		int length=dat.beams[0].samples.length;
		assertEquals(-25, dat.beams[0].samples[length-1]);

		assertEquals(20, dat.beams[20].beamNumber);
		assertEquals(512, dat.beams[20].numberOfSample);

		length=dat.beams[20].samples.length;
		assertEquals(-2, dat.beams[20].samples[1]);
		assertEquals(-5, dat.beams[20].samples[2]);
		assertEquals(-27, dat.beams[20].samples[length-1]);

	}
}
