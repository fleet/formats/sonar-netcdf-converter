package fr.ifremer.globe.api.xsf.tests.all.datagrams;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.surfacesoundspeed.SurfaceSoundSpeed;

public class TestSurfaceSoundSpeed extends DatagramTest {

	@Test
	public void testGetBody() throws IOException {
		DatagramBuffer buffer = getDatagram((byte)0x47);
		
		assertEquals(SurfaceSoundSpeed.getSerialNumber(buffer.byteBufferData).getU(), 212);
		int N = SurfaceSoundSpeed.getNumberEntries(buffer.byteBufferData).getU();
		assertEquals(N, 100);
		assertEquals(SurfaceSoundSpeed.getTimeSinceRecordStart(buffer.byteBufferData, 0).getU(), 0);
		assertEquals(SurfaceSoundSpeed.getSurfaceSoundSpeed(buffer.byteBufferData, 0).getU(), 15101);
		assertEquals(SurfaceSoundSpeed.getTimeSinceRecordStart(buffer.byteBufferData, N-1).getU(), 9);
		assertEquals(SurfaceSoundSpeed.getSurfaceSoundSpeed(buffer.byteBufferData, N-1).getU(), 15101);
	}
}
