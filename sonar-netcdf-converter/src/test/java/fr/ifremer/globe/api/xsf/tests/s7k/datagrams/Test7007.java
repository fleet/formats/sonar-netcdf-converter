package fr.ifremer.globe.api.xsf.tests.s7k.datagrams;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramParser;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K7007;
import fr.ifremer.globe.api.xsf.util.types.UShort;

public class Test7007 extends TestDatagram {

	@Test
	public void testDecode() throws IOException {
		DatagramBuffer buffer = getDataBuffer(7007);
		
		assertEquals(7007, DatagramParser.getDatagramType(buffer));
		
		assertEquals(0, S7K7007.getSonarSerialNumber(buffer).getLong());
		assertEquals(334910, S7K7007.getPingNumber(buffer).getU());
		assertEquals(0, S7K7007.getMultiPingSequence(buffer).getU());
		assertEquals(0.0, S7K7007.getBeamPosition(buffer).get(), 0.01);
		//assertEquals(16, S7K7007.getControlFlags(buffer).getU());
		assertEquals(2001, S7K7007.getNumberOfSamplesPerSide(buffer).getU());
		
		final UShort[] portBeams = S7K7007.getPortBeams(buffer);
		final UShort[] starboardBeams = S7K7007.getStarboardBeams(buffer);
		
		assertEquals(2001, portBeams.length);
		assertEquals(2001, starboardBeams.length);
		
		assertEquals(17, portBeams[0].getU());
		assertEquals(0, portBeams[100].getU());
		assertEquals(11, portBeams[2000].getU());
		
		final int optionalDataOffset = (int) S7K7007.getOptionalDataOffset(buffer).getU();
		assertEquals(8132, optionalDataOffset);
		assertEquals(396000.0, S7K7007.getFrequency(buffer, optionalDataOffset).get(), 0.01);
		assertEquals(0.87, S7K7007.getLatitude(buffer, optionalDataOffset).get(), 0.01);
		assertEquals(-0.072, S7K7007.getLongitude(buffer, optionalDataOffset).get(), 0.01);
		assertEquals(0.70, S7K7007.getHeading(buffer, optionalDataOffset).get(), 0.01);
		assertEquals(10.67, S7K7007.getDepth(buffer, optionalDataOffset).get(), 0.01);
	}
}
