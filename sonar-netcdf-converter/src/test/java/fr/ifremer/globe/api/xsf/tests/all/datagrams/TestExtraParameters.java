package fr.ifremer.globe.api.xsf.tests.all.datagrams;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.extraparameters.ExtraParameters;

public class TestExtraParameters extends DatagramTest {

	@Test
	public void testRead() throws Exception {
		DatagramBuffer buffer = getDatagram((byte)0x33);

		// check Header
		assertEquals(48, ExtraParameters.getCounter(buffer.byteBufferData).getU());
		assertEquals(101, ExtraParameters.getSerialNumber(buffer.byteBufferData).getU());
		assertEquals(6, ExtraParameters.getContentIdentifier(buffer.byteBufferData).getU());
		
		//int sz = buffer.byteBufferSizeHeader.getInt(0);
		//sz -= 22;
		//assertEquals(6, ExtraParameters.getInformationDatagram(buffer.byteBufferData;

	}
}
