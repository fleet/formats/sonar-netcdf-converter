package fr.ifremer.globe.api.xsf.tests.all.datagrams;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.soundspeedprofile.SoundSpeedProfile;

public class TestSoundSpeedProfile extends DatagramTest {

	@Test
	public void testGetBody() throws IOException {
		DatagramBuffer buffer = getDatagram((byte)0x55);
		
		assertEquals(SoundSpeedProfile.getSerialNumber(buffer.byteBufferData).getU(), 212);
		assertEquals(SoundSpeedProfile.getDepthResolution(buffer.byteBufferData).getU(), 1);
		int N = SoundSpeedProfile.getNumberEntries(buffer.byteBufferData).getU();
		assertEquals(N, 41);
		assertEquals(SoundSpeedProfile.getDepth(buffer.byteBufferData, 0).getU(), 0);
		assertEquals(SoundSpeedProfile.getSoundSpeedProfile(buffer.byteBufferData, 0).getU(), 15100);
		assertEquals(SoundSpeedProfile.getDepth(buffer.byteBufferData, N-1).getU(), 1200000);
		assertEquals(SoundSpeedProfile.getSoundSpeedProfile(buffer.byteBufferData, N-1).getU(), 16990);
	}
}
