package fr.ifremer.globe.api.xsf.tests.all.datagrams;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.heading.Heading;

public class TestHeading extends DatagramTest {

	@Test
	public void testGetBody() throws IOException {
		DatagramBuffer buffer = getDatagram((byte)0x48);
		
		assertEquals(Heading.getSerialNumber(buffer.byteBufferData).getU(), 109);
		assertEquals(Heading.getNumberEntries(buffer.byteBufferData).getU(), 3);
		// Numéro Entries= 1
		assertEquals(Heading.getTimeSinceRecordStart(buffer.byteBufferData, 1).getU(), 999);
		// StartingOffset = 30
		assertEquals(Heading.getHeadingIndicator(buffer.byteBufferData).getU(), 0);
	}
}
