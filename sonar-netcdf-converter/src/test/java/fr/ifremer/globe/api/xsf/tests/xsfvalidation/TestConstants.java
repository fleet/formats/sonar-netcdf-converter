package fr.ifremer.globe.api.xsf.tests.xsfvalidation;

import java.io.File;

import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfBase;
import fr.ifremer.globe.utils.test.GlobeTestUtil;

public class TestConstants {
	public final static String sourceDirectory = GlobeTestUtil.getTestDataPath();
	// public final static String sourceDirectory = "D:/data";
	public final static File inReson = new File(TestConstants.sourceDirectory + File.separator + "file" + File.separator
			+ "xsf" + File.separator + "XSFValidation" + File.separator + "20150902_041108_PP_7150_24kHz.s7k");

	public final static File xsfReson = new File(
			TestConstants.sourceDirectory + File.separator + "file" + File.separator + "xsf" + File.separator
					+ "XSFValidation" + File.separator + "out.s7k" + XsfBase.EXTENSION_XSF);

	public final static File mbgRefReson = new File(
			TestConstants.sourceDirectory + File.separator + "file" + File.separator + "xsf" + File.separator
					+ "XSFValidation" + File.separator + "20150902_041108_PP_7150_24kHz.mbg");

}
