package fr.ifremer.globe.api.xsf.tests.s7k.datagrams;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramParser;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K1003;

public class Test1003 extends TestDatagram {

	@Test
	public void testDecode() throws IOException {
		DatagramBuffer buffer = getDataBuffer(1003);
		
		assertEquals(1003, DatagramParser.getDatagramType(buffer));
		
		assertEquals(0, S7K1003.getDatumIdentifier(buffer).getU());
		assertEquals(0.0, S7K1003.getLatency(buffer).get(), 0.0);
		assertEquals(0.84, S7K1003.getLatitudeOrNorthing(buffer).get(), 0.01);
		assertEquals(-0.08, S7K1003.getLongitudeOrEasting(buffer).get(), 0.01);
		assertEquals(0.44, S7K1003.getHeight(buffer).get(), 0.01);
		assertEquals(0, S7K1003.getPositionTypeFlag(buffer).getU());
		assertEquals(0, S7K1003.getUTMZone(buffer).getU());
		assertEquals(0, S7K1003.getQualityFlag(buffer).getU());
		assertEquals(0, S7K1003.getPositioningMethod(buffer).getU());
	}
}
