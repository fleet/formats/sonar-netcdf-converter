package fr.ifremer.globe.api.xsf.tests.all.indexing;

import static org.junit.Assert.assertEquals;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.all.datagram.rawrange78.RawRange78CompletePingMetadata;
import fr.ifremer.globe.api.xsf.converter.common.exceptions.UnsupportedSounderException;
import fr.ifremer.globe.api.xsf.tests.all.utils.FileFinder;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;

public class TestRawRange78Indexing {

	@Test
	public void testWCIndexing() throws FileNotFoundException, IOException, UnsupportedSounderException {
		AllFile metadata = new AllFile(FileFinder.getPath("/all/rawrange78.all"));

		assertEquals(7, metadata.getRawRange78().datagramCount);
		assertEquals(512, metadata.getRawRange78().getMaxBeamCount());

		RawRange78CompletePingMetadata ping = metadata.getRawRange78().getPing(new SwathIdentifier(49736, 0));
		assertEquals(true, ping.isComplete(metadata.getGlobalMetadata().getSerialNumbers()));

		ping = metadata.getRawRange78().getPing(new SwathIdentifier(49739, 0));
		assertEquals(false, ping.isComplete(metadata.getGlobalMetadata().getSerialNumbers()));
	}
}
