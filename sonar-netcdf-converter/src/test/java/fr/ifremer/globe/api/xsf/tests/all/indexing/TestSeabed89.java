package fr.ifremer.globe.api.xsf.tests.all.indexing;

import static org.junit.Assert.assertEquals;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.all.datagram.seabed89.Seabed89CompletePingMetadata;
import fr.ifremer.globe.api.xsf.converter.common.exceptions.UnsupportedSounderException;
import fr.ifremer.globe.api.xsf.tests.all.utils.FileFinder;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;

public class TestSeabed89 {

	@Test
	public void testSeabed89Indexing() throws FileNotFoundException, IOException, UnsupportedSounderException {
		AllFile metadata = new AllFile(FileFinder.getPath("/all/seabed89.all"));

		assertEquals(metadata.getSeabed89().datagramCount, 11);
		assertEquals(metadata.getSeabed89().getPings().size(), 6);
		assertEquals(metadata.getSeabed89().getMaxAbstractSampleCount(), 23646);
		assertEquals(metadata.getSeabed89().getMaxBeamCount(), 512);

		// the first ping is complete
		Seabed89CompletePingMetadata ping = metadata.getSeabed89().getPing(new SwathIdentifier(49736, 0));
		assertEquals(ping.isComplete(metadata.getGlobalMetadata().getSerialNumbers()), true);
		assertEquals(ping.getBeamCount(), 512);
		assertEquals(ping.getMaxAbstractSampleCount(), 4721);

		// the last ping is not complete (one datagram missing)
		ping = metadata.getSeabed89().getPing(new SwathIdentifier(49741, 0));
		assertEquals(ping.isComplete(metadata.getGlobalMetadata().getSerialNumbers()), false);
	}
}
