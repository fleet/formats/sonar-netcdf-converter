package fr.ifremer.globe.api.xsf.tests.all.indexing;

import static org.junit.Assert.assertEquals;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.all.datagram.wc.WCCompletePingMetadata;
import fr.ifremer.globe.api.xsf.converter.common.exceptions.UnsupportedSounderException;
import fr.ifremer.globe.api.xsf.tests.all.utils.FileFinder;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;

public class TestWCIndexing {

	@Test
	public void testWCIndexing() throws FileNotFoundException, IOException, UnsupportedSounderException {
		AllFile metadata = new AllFile(FileFinder.getPath("/all/wc.all"));

		assertEquals(metadata.getWc().datagramCount, 11);
		assertEquals(metadata.getWc().getPings().size(), 2);
		assertEquals(metadata.getWc().getMaxAbstractSampleCount(), 275890);
		assertEquals(metadata.getWc().getMaxBeamCount(), 512);

		// the first ping is complete
		WCCompletePingMetadata ping = metadata.getWc().getPing(new SwathIdentifier(49736, 0));
		assertEquals(ping.isComplete(metadata.getGlobalMetadata().getSerialNumbers()), true);
		assertEquals(ping.getBeamCount(), 512);
		assertEquals(ping.getMaxAbstractSampleCount(), 275890);

		// the second ping is not complete (one datagram missing)
		ping = metadata.getWc().getPing(new SwathIdentifier(49737, 0));
		assertEquals(ping.isComplete(metadata.getGlobalMetadata().getSerialNumbers()), false);
	}
}
