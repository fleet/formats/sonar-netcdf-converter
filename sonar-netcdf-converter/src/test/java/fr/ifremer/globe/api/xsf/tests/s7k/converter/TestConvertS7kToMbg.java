package fr.ifremer.globe.api.xsf.tests.s7k.converter;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgConstants;
import fr.ifremer.globe.api.xsf.converter.s7k.S7KConverter;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCFile.Mode;
import fr.ifremer.globe.netcdf.util.comparator.NetcdfComparator;
import fr.ifremer.globe.netcdf.util.comparator.NetcdfComparatorHook;
import fr.ifremer.globe.utils.test.GlobeTestUtil;

public class TestConvertS7kToMbg {

	/** Logger **/
	private static final Logger LOGGER = LoggerFactory.getLogger(TestConvertS7kToMbg.class);

	/** Test directory **/
	private static final String TEST_DIR_PATH = GlobeTestUtil.getTestDataPath() + "/file/converter/s7k/mbg";
	// private static final String TEST_DIR_PATH = "F:\\01_format_s7k";//\\mariolis";

	/**
	 * Tests the new converter by comparison with the previous one.
	 */
	@Test
	public void testConvertS7kToMbg() throws NCException, IOException {
		StringBuilder resume = new StringBuilder("testConvertS7kToMbg result : \n");
		boolean testFailed = false;

		for (final File inputFile : new File(TEST_DIR_PATH).listFiles()) {
			if (!inputFile.getName().contains("6000"))
				continue;
			
			if (inputFile.getName().endsWith(".s7k")) {
				String referenceMbgPath = inputFile.getAbsolutePath().replace(".s7k", "_ref.mbg");
				Assert.assertTrue(new File(referenceMbgPath).exists());
				String testMbgPath = inputFile.getAbsolutePath().replace(".s7k", "_test.mbg");
				try {

					// convert s7k to mbg
					new S7KConverter().convert(inputFile.getAbsolutePath(), testMbgPath);
					Assert.assertTrue(new File(testMbgPath).exists());

					Optional<NetcdfComparatorHook> hook = buildHook();
					if (referenceMbgPath.contains("20140112_175404_PP-7150-12kHz-")) {
						// bug in old converter, installation is the one of a 24KHz
						hook.get().ignoreGlobalAttribute(MbgConstants.InstallParameters);
						hook.get().ignoreGlobalAttribute(MbgConstants.mbTxAntennaLeverArm);
					} else {
						// bug fixed in installation parameter reading ('Z' was not read with the correct sign, and Y -
						// X inverted)
						try (NetcdfFile refNcFile = NetcdfFile.open(referenceMbgPath, Mode.readonly);
								NetcdfFile testNcFile = NetcdfFile.open(testMbgPath, Mode.readonly)) {
							if (refNcFile.hasAttribute(MbgConstants.mbTxAntennaLeverArm)) {
								double[] refTxAntennaLvlArm = refNcFile
										.getAttributeDoubleArray(MbgConstants.mbTxAntennaLeverArm);
								double[] testTxAntennaLvlArm = testNcFile
										.getAttributeDoubleArray(MbgConstants.mbTxAntennaLeverArm);
								Assert.assertEquals("Test x ref = y test (file : " + testMbgPath + ")",
										refTxAntennaLvlArm[0], testTxAntennaLvlArm[1], 0.0);
								Assert.assertEquals("Test y ref =  x test (file : " + testMbgPath + ")",
										refTxAntennaLvlArm[1], testTxAntennaLvlArm[0], 0.0);
								Assert.assertEquals("Test z ref = - z test (file : " + testMbgPath + ")",
										refTxAntennaLvlArm[2], -testTxAntennaLvlArm[2], 0.0);
							}
						}
					}
					
					// compare with reference
					Optional<String> errors = NetcdfComparator.compareFiles(referenceMbgPath, testMbgPath, hook);

					// print result
					if (errors.isPresent()) {
						testFailed = true;
						resume.append(testMbgPath + " : FAILED \n");
						resume.append(errors.get() + "\n");
					} else {
						resume.append(testMbgPath + " : OK \n");
					}
				} catch (Exception e) {
					testFailed = true;
					resume.append(inputFile.getName() + " exception during convertion: " + e + "\n");
					LOGGER.error("Error with file " + inputFile + ": " + e.getMessage(), e);
				}
			}
		}
		LOGGER.debug(resume.toString());
		Assert.assertFalse(resume.toString(), testFailed);
	}

	/**
	 * Defines variable(s)/attribute(s) to ignore.
	 */
	private Optional<NetcdfComparatorHook> buildHook() {
		NetcdfComparatorHook hook = new NetcdfComparatorHook();

		// obviously : name, and date/time of file creation are different
		hook.ignoreGlobalAttribute("mbName");
		hook.ignoreVariable("mbHistDate");
		hook.ignoreVariable("mbHistTime");
		hook.ignoreVariable("mbHistAutor");

		// define an acceptable delta for geographic bound attributes
		hook.setToleranceForAttribute("mbEastLongitude", 1E-14);
		hook.setToleranceForAttribute("mbSouthLatitude", 1E-14);
		hook.setToleranceForAttribute("mbWestLongitude", 1E-14);
		hook.setToleranceForAttribute("mbNorthLatitude", 1E-14);

		// the attribute "_FillValue" has been introduced with the new converter
		hook.ignoreAttributeForAllVariables("_FillValue");

		// bug fixed in installation parameter reading ('Z' was not read with the correct sign, and Y - X inverted)
		hook.ignoreGlobalAttribute(MbgConstants.mbTxAntennaLeverArm);

		return Optional.of(hook);
	}

}
