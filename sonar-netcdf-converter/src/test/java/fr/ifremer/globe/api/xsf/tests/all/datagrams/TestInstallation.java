package fr.ifremer.globe.api.xsf.tests.all.datagrams;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.installation.Installation;

public class TestInstallation extends DatagramTest {

	@Test
	public void testGetBody() throws IOException {
		DatagramBuffer buffer = getDatagram((byte)0x49);
		
		assertEquals(Installation.getSystemSerialNumber(buffer.byteBufferData).getU(), 212);
		assertEquals(Installation.getSecondarySystemSerialNumber(buffer.byteBufferData).getU(), 10212);
		assertEquals(Installation.getEpochTime(buffer.byteBufferData), 1414762573084l);
		assertEquals(Installation.getModelNumber(buffer.byteBufferData), 2040);
		assertEquals(Installation.getBody(buffer.byteBufferData), "WLZ=-1.000,SMH=212,S1X=4.223,S1Y=0.010,S1Z=1.735,S1H=358.390,S1P=-0.040,S1R=-0.080,S1S=1,S2X=4.228,S2Y=-0.372,S2Z=1.652,S2H=0.220,S2P=-0.020,S2R=39.790,S2S=1,S3X=4.229,S3Y=0.389,S3Z=1.650,S3H=359.090,S3P=-0.260,S3R=-39.860,S3S=1,GO1=0.00,GO2=0.00,TXS=130,R1S=172,R2S=174,TSV=1.07 120222,RSV=1.02 110822,BSV=1.1.6 120208,PSV=1.2.7 121120,DSV=3.1.4 120508,DDS=3.5.4 120124,OSV=SIS 3.9.2,DSX=0.000,DSY=0.000,DSZ=0.000,DSD=0,DSO=0.000000,DSF=1.000000,DSH=NI,P1M=1,P1T=1,P1Q=1,P1X=0.000,P1Y=0.000,P1Z=0.000,P1D=0.000,P1G=WGS84,P2M=0,P2T=0,P2Q=1,P2X=0.000,P2Y=0.000,P2Z=0.000,P2D=0.000,P2G=WGS84,P3M=0,P3T=0,P3Q=1,P3X=0.000,P3Y=0.000,P3Z=0.000,P3D=0.000,P3G=WGS84,P3S=1,MSX=0.000,MSY=0.000,MSZ=0.000,MRP=RP,MSD=3,MSR=0.000,MSP=0.500,MSG=1.500,NSX=0.000,NSY=0.000,NSZ=0.000,NRP=RP,NSD=0,NSR=0.000,NSP=0.000,NSG=0.000,MAS=1.000,GCG=0.000,APS=0,AHS=2,ARO=2,AHE=2,CLS=1,CLO=0,PPS=2,VSN=1,VSE=2,VSU=3001,VTE=0,VTU=4001,VSI=157.237.16.1,VSM=255.255.255.0,SID=ETAL2040_02,RFN=0095_20141031_133613_Thalia.all,");
	}
}
