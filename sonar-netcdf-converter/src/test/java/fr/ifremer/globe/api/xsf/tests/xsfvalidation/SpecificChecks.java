package fr.ifremer.globe.api.xsf.tests.xsfvalidation;

import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCFile;

public interface SpecificChecks {
	public void testBeamGroup(NCFile src,GrpContext context) throws NCException;
}