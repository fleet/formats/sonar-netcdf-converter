package fr.ifremer.globe.api.xsf.tests.all.converter;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import fr.ifremer.globe.api.xsf.converter.all.KongsbergAllConverter;
import fr.ifremer.globe.api.xsf.tests.all.utils.FileFinder;
import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCFile;
import fr.ifremer.globe.netcdf.ucar.NCGroup;
import fr.ifremer.globe.netcdf.ucar.NCVariable;
import fr.ifremer.globe.netcdf.ucar.NCFile.Mode;



public class TestConvertWC {

    @Rule
    public TemporaryFolder tmp = new TemporaryFolder();
    
    @Test
    public void testConvertWC() throws Exception {
        final String wc_ref_file = FileFinder.getPath("/all/wc.all");
        final File hsfFile = tmp.newFile("out.hsf");
        
        new KongsbergAllConverter().convert(wc_ref_file, hsfFile.getAbsolutePath());
        
        try (NCFile reader = NCFile.open(hsfFile.getAbsolutePath(), Mode.readonly)) {
            assertNotNull(reader.getGroup("sounder"));
            
            // check for the structure
            NCGroup wcGroup = reader.getGroup("sounder").getGroup("water_column");
            assertNotNull(wcGroup);
            assertNotNull(wcGroup.getGroup("rx_info"));
            assertNotNull(wcGroup.getGroup("tx_info"));
            
            NCDimension swathDim = reader.getGroup("sounder").getDimension("swath_dim"); 
            assertNotNull(swathDim);
            assertEquals(2, swathDim.getLength());
            
            NCVariable swathValidity = wcGroup.getVariable("swath_validity");
            assertNotNull(swathValidity);
            // first swath valid for both antenna, second invalid (for both antenna)
            assertArrayEquals(new int[] {1, 1, 0, 0}, swathValidity.get_int(new long[] { 0,  0}, new long[] { 2, 2 }));
        }
    }
}
