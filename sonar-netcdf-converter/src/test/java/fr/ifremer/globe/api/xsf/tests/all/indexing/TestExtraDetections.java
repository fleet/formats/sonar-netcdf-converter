package fr.ifremer.globe.api.xsf.tests.all.indexing;

import static org.junit.Assert.assertEquals;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.all.datagram.extradetections.ExtraDetectionsCompletePingMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.common.exceptions.UnsupportedSounderException;
import fr.ifremer.globe.api.xsf.tests.all.utils.FileFinder;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;

public class TestExtraDetections {

    @Test
    public void testExtraDetectionsIndexing() throws FileNotFoundException, IOException, UnsupportedSounderException {
        AllFile metadata = new AllFile(FileFinder.getPath("/all/extradetections.all"));
        
        assertEquals(metadata.getExtraDetections().datagramCount, 10);
        assertEquals(metadata.getExtraDetections().getEntry(new SwathIdentifier(11769, 0)).getExtradetectionsCount(), 117);
        assertEquals(metadata.getExtraDetections().getEntry(new SwathIdentifier(11769, 0)).getDetectionClassesCount(), 0);
                        
        // the first ping is complete
        ExtraDetectionsCompletePingMetadata ping = metadata.getExtraDetections().getPing(new SwathIdentifier(11769, 0));
        assertEquals(ping.getExtradetectionsCount(), 117);
        assertEquals(ping.getDetectionClassesCount(), 0);
        assertEquals(ping.getSampleCount(), 0);
        

        // the last ping is not complete (one datagram missing)
        ping = metadata.getExtraDetections().getPing(new SwathIdentifier(11774,0));
        assertEquals(ping.getExtradetectionsCount(), 142);
        assertEquals(ping.getDetectionClassesCount(), 8);
        assertEquals(ping.getSampleCount(), 1278);
    }
}
