package fr.ifremer.globe.api.xsf.tests.kmall.datagrams;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.kmall.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.MRZ;

public class TestMRZDatagram extends DatagramTest {

    @Test
    public void testRead() throws IOException {
        DatagramBuffer buffer = getDatagram("MRZ");
        
        // part common to all datagrams
        assertEquals("#MRZ", MRZ.getDgmType(buffer.byteBufferData));
        assertEquals("#MRZ", MRZ.getDgmType(buffer.byteBufferData));
        assertEquals(0, MRZ.getDgmVersion(buffer.byteBufferData).getU());
        assertEquals(140, MRZ.getSystemId(buffer.byteBufferData).getU());
        assertEquals(2040, MRZ.getEchoSounderId(buffer.byteBufferData).getU());
        assertEquals(1473163103, MRZ.getTimeSec(buffer.byteBufferData).getU());
        assertEquals(686603163, MRZ.getTimeNano(buffer.byteBufferData).getU());
        assertEquals(1473163103686l, MRZ.getEpochTimeMilli(buffer.byteBufferData));
        assertEquals(603163l, MRZ.getRemainderTimeNano(buffer.byteBufferData));
        
        // M datagram related part
        assertEquals(1, MRZ.getNumOfDgms(buffer.byteBufferData));
        assertEquals(1, MRZ.getDgmNum(buffer.byteBufferData));
        
        // EMdgmMbody_def part
        assertEquals(12, MRZ.getNumByteCmnPart(buffer.byteBufferData));
        assertEquals(3058, MRZ.getPingCnt(buffer.byteBufferData));
        assertEquals(1, MRZ.getRxFansPerPing(buffer.byteBufferData).getU());
        assertEquals(0, MRZ.getRxFanIndex(buffer.byteBufferData));
        assertEquals(1, MRZ.getSwathPerPing(buffer.byteBufferData).getU());
        assertEquals(0, MRZ.getSwathAlongPosition(buffer.byteBufferData).getU());
        assertEquals(0, MRZ.getTxTransducerInd(buffer.byteBufferData).getU());
        assertEquals(0, MRZ.getRxTransducerInd(buffer.byteBufferData).getU());
        assertEquals(1, MRZ.getNumRxTransducers(buffer.byteBufferData).getU());
        assertEquals(0, MRZ.getAlgorithmType(buffer.byteBufferData).getU());
        
        // pingInfo
        assertEquals(144, MRZ.getNumBytesInfoData(buffer.byteBufferData));
        assertEquals(50.0, MRZ.getPingRate_Hz(buffer.byteBufferData).get(), 0.001);
        assertEquals(2, MRZ.getBeamSpacing(buffer.byteBufferData).getU());
        assertEquals(1, MRZ.getDepthMode(buffer.byteBufferData).getU());
        assertEquals(0, MRZ.getSubDepthMode(buffer.byteBufferData).getU());
        assertEquals(0, MRZ.getDistanceBtwSwath(buffer.byteBufferData).getU());
        assertEquals(0, MRZ.getDetectionMode(buffer.byteBufferData).getU());
        assertEquals(0, MRZ.getPulseForm(buffer.byteBufferData).getU());
        assertEquals(300000.0, MRZ.getFrequencyMode_Hz(buffer.byteBufferData).get(), 0.001);
        assertEquals(260000.0, MRZ.getFreqRangeLowLim_Hz(buffer.byteBufferData).get(), 0.001);
        assertEquals(320000.0, MRZ.getFreqRangeHighLim_Hz(buffer.byteBufferData).get(), 0.001);
        assertEquals(0.000101, MRZ.getMaxTotalTxPulseLength_sec(buffer.byteBufferData).get(), 0.000001);
        assertEquals(0.000069997, MRZ.getMaxEffTxPulseLength_sec(buffer.byteBufferData).get(), 0.000001);
        assertEquals(14286.0, MRZ.getMaxEffTxBandWidth_Hz(buffer.byteBufferData).get(), 0.001);
        assertEquals(0.0, MRZ.getAbsCoeff_dBPerkm(buffer.byteBufferData).get(), 0.001);
        assertEquals(75.0, MRZ.getPortSectorEdge_deg(buffer.byteBufferData).get(), 0.001);
        assertEquals(-15.0, MRZ.getStarbSectorEdge_deg(buffer.byteBufferData).get(), 0.001);
        assertEquals(75.1082, MRZ.getPortMeanCov_deg(buffer.byteBufferData).get(), 0.001);
        assertEquals(-12.2839, MRZ.getStarbMeanCov_deg(buffer.byteBufferData).get(), 0.001);
        assertEquals(-73, MRZ.getPortMeanCov_m(buffer.byteBufferData).get());
        assertEquals(4, MRZ.getStarbMeanCov_m(buffer.byteBufferData).get());
        assertEquals(0, MRZ.getModeAndStabilisation(buffer.byteBufferData).getU());
        assertEquals(0, MRZ.getRuntimeFilter1(buffer.byteBufferData).getU());
        assertEquals(0, MRZ.getRuntimeFilter2(buffer.byteBufferData).getU());
        assertEquals(1, MRZ.getPipeTrackingStatus(buffer.byteBufferData).getU());
        assertEquals(0.5, MRZ.getTransmitArraySizeUsed_deg(buffer.byteBufferData).get(), 0.001);
        assertEquals(1.0, MRZ.getReceiveArraySizeUsed_deg(buffer.byteBufferData).get(), 0.001);
        assertEquals(0.0, MRZ.getTransmitPower_dB(buffer.byteBufferData).get(), 0.001);
        assertEquals(0, MRZ.getSLrampUpTimeRemaining(buffer.byteBufferData).getU());
        assertEquals(0.0, MRZ.getYawAngle_deg(buffer.byteBufferData).get(), 0.001);
        assertEquals(3, MRZ.getNumTxSectors(buffer.byteBufferData).getU());
        assertEquals(36, MRZ.getNumBytesPerTxSector(buffer.byteBufferData).getU());
        assertEquals(310.1799, MRZ.getHeadingVessel_deg(buffer.byteBufferData).get(), 0.001);
        assertEquals(1460.002, MRZ.getSoundSpeedAtTxDepth_mPerSec(buffer.byteBufferData).get(), 0.001);
        assertEquals(0.6080, MRZ.getTxTransducerDepth_m(buffer.byteBufferData).get(), 0.001);
        assertEquals(-0.0199, MRZ.getZ_waterLevelReRefPoint_m(buffer.byteBufferData).get(), 0.001);
        assertEquals(5.0323, MRZ.getX_kmallToall_m(buffer.byteBufferData).get(), 0.001);
        assertEquals(0.6309, MRZ.getY_kmallToall_m(buffer.byteBufferData).get(), 0.001);
        assertEquals(1, MRZ.getLatLongInfo(buffer.byteBufferData).getU());
        assertEquals(1, MRZ.getPosSensorStatus(buffer.byteBufferData).getU());
        assertEquals(0, MRZ.getAttitudeSensorStatus(buffer.byteBufferData).getU());
        assertEquals(59.4352, MRZ.getLatitude_deg(buffer.byteBufferData).get(), 0.001);
        assertEquals(10.4733, MRZ.getLongitude_deg(buffer.byteBufferData).get(), 0.001);
        assertEquals(38.99, MRZ.getEllipsoidHeightReRefPoint_m(buffer.byteBufferData).get(), 0.001);
        
        // txSectorInfo
        // sector 0
        assertEquals(0, MRZ.getTxSectorNumb(buffer.byteBufferData, 0).getU());
        assertEquals(0, MRZ.getTxArrNumber(buffer.byteBufferData, 0).getU());
        assertEquals(0, MRZ.getTxSubArray(buffer.byteBufferData, 0).getU());
        assertEquals(0.0, MRZ.getSectorTransmitDelay_sec(buffer.byteBufferData, 0).get(), 0.001);
        assertEquals(-0.317, MRZ.getTiltAngleReTx_deg(buffer.byteBufferData, 0).get(), 0.001);
        assertEquals(218.199, MRZ.getTxNominalSourceLevel_dB(buffer.byteBufferData, 0).get(), 0.001);
        assertEquals(43.437, MRZ.getTxFocusRange_m(buffer.byteBufferData, 0).get(), 0.001);
        assertEquals(260000.0, MRZ.getCentreFreq_Hz(buffer.byteBufferData, 0).get(), 0.001);
        assertEquals(14286.0, MRZ.getSignalBandWidth_Hz(buffer.byteBufferData, 0).get(), 0.001);
        assertEquals(0.000101, MRZ.getTotalSignalLength_sec(buffer.byteBufferData, 0).get(), 0.000001);
        assertEquals(100, MRZ.getPulseShading(buffer.byteBufferData, 0).getU());
        assertEquals(0, MRZ.getSignalWaveForm(buffer.byteBufferData, 0).getU());
        
        // sector 1
        assertEquals(1, MRZ.getTxSectorNumb(buffer.byteBufferData, 1).getU());
        assertEquals(0, MRZ.getTxArrNumber(buffer.byteBufferData, 1).getU());
        assertEquals(1, MRZ.getTxSubArray(buffer.byteBufferData, 1).getU());
        assertEquals(0.0, MRZ.getSectorTransmitDelay_sec(buffer.byteBufferData, 1).get(), 0.001);
        assertEquals(-0.74, MRZ.getTiltAngleReTx_deg(buffer.byteBufferData, 1).get(), 0.001);
        assertEquals(217.399, MRZ.getTxNominalSourceLevel_dB(buffer.byteBufferData, 1).get(), 0.001);
        assertEquals(18.809, MRZ.getTxFocusRange_m(buffer.byteBufferData, 1).get(), 0.001);
        assertEquals(320000.0, MRZ.getCentreFreq_Hz(buffer.byteBufferData, 1).get(), 0.001);
        assertEquals(14286.0, MRZ.getSignalBandWidth_Hz(buffer.byteBufferData, 1).get(), 0.001);
        assertEquals(0.000101, MRZ.getTotalSignalLength_sec(buffer.byteBufferData, 1).get(), 0.000001);
        assertEquals(100, MRZ.getPulseShading(buffer.byteBufferData, 1).getU());
        assertEquals(0, MRZ.getSignalWaveForm(buffer.byteBufferData, 1).getU());
        
        // sector 2
        assertEquals(2, MRZ.getTxSectorNumb(buffer.byteBufferData, 2).getU());
        assertEquals(0, MRZ.getTxArrNumber(buffer.byteBufferData, 2).getU());
        assertEquals(2, MRZ.getTxSubArray(buffer.byteBufferData, 2).getU());
        assertEquals(0.0, MRZ.getSectorTransmitDelay_sec(buffer.byteBufferData, 2).get(), 0.001);
        assertEquals(-0.801, MRZ.getTiltAngleReTx_deg(buffer.byteBufferData, 2).get(), 0.001);
        assertEquals(221.199, MRZ.getTxNominalSourceLevel_dB(buffer.byteBufferData, 2).get(), 0.001);
        assertEquals(16.679, MRZ.getTxFocusRange_m(buffer.byteBufferData, 2).get(), 0.001);
        assertEquals(290000.0, MRZ.getCentreFreq_Hz(buffer.byteBufferData, 2).get(), 0.001);
        assertEquals(14286.0, MRZ.getSignalBandWidth_Hz(buffer.byteBufferData, 2).get(), 0.001);
        assertEquals(0.000101, MRZ.getTotalSignalLength_sec(buffer.byteBufferData, 2).get(), 0.000001);
        assertEquals(100, MRZ.getPulseShading(buffer.byteBufferData, 2).getU());
        assertEquals(0, MRZ.getSignalWaveForm(buffer.byteBufferData, 2).getU());
        
        // rxInfo
        int offset = MRZ.getRxInfoOffset(buffer.byteBufferData);
        assertEquals(32, MRZ.getNumBytesRxInfo(buffer.byteBufferData, offset));
        assertEquals(400, MRZ.getNumSoundingsMaxMain(buffer.byteBufferData, offset).getU());
        assertEquals(398, MRZ.getNumSoundingsValidMain(buffer.byteBufferData, offset).getU());
        assertEquals(120, MRZ.getNumBytesPerSounding(buffer.byteBufferData, offset));
        assertEquals(15318.627, MRZ.getWCSampleRate(buffer.byteBufferData, offset).get(), 0.001);
        assertEquals(30637.255, MRZ.getSeabedImageSampleRate(buffer.byteBufferData, offset).get(), 0.001);
        assertEquals(-23.409, MRZ.getBSnormal_dB(buffer.byteBufferData, offset).get(), 0.001);
        assertEquals(-28.52, MRZ.getBSoblique_dB(buffer.byteBufferData, offset).get(), 0.001);
        assertEquals(0, MRZ.getExtraDetectionAlarmFlag(buffer.byteBufferData, offset).getU());
        assertEquals(0, MRZ.getNumExtraDetections(buffer.byteBufferData, offset).getU());
        assertEquals(0, MRZ.getNumExtraDetectionClasses(buffer.byteBufferData, offset).getU());
        assertEquals(4, MRZ.getNumBytesPerClass(buffer.byteBufferData, offset).getU());
        
        // no extraDectClassInfo
        
        // sounding data
        offset = MRZ.getSoundingOffset(buffer.byteBufferData, offset);
        assertEquals(316, offset);
        assertEquals(0, MRZ.getSoundingIndex(buffer.byteBufferData, offset).getU());
        assertEquals(0, MRZ.getCorrTxSectorNumb(buffer.byteBufferData, offset).getU());
        assertEquals(0, MRZ.getDetectionType(buffer.byteBufferData, offset).getU());
        assertEquals(2, MRZ.getDetectionMethod(buffer.byteBufferData, offset).getU());
        assertEquals(0, MRZ.getRejectionInfo1(buffer.byteBufferData, offset).getU());
        assertEquals(0, MRZ.getRejectionInfo2(buffer.byteBufferData, offset).getU());
        assertEquals(0, MRZ.getPostProcessingInfo(buffer.byteBufferData, offset).getU());
        assertEquals(95, MRZ.getDetectionClass(buffer.byteBufferData, 0).getU());
        assertEquals(0, MRZ.getDetectionConfidenceLevel(buffer.byteBufferData, offset).getU());
        assertEquals(100.0, MRZ.getRangeFactor(buffer.byteBufferData, offset).get(), 0.001);
        assertEquals(0.105, MRZ.getQualityFactor(buffer.byteBufferData, offset).get(), 0.001);
        assertEquals(0.02, MRZ.getDetectionUncertaintyVer_m(buffer.byteBufferData, offset).get(), 0.001);
        assertEquals(0.0, MRZ.getDetectionUncertaintyHor_m(buffer.byteBufferData, offset).get(), 0.001);
        assertEquals(0.0003, MRZ.getDetectionWindowLength_sec(buffer.byteBufferData, offset).get(), 0.001);
        assertEquals(0.011, MRZ.getEchoLength_sec(buffer.byteBufferData, offset).get(), 0.001);
        assertEquals(0, MRZ.getWCBeamNumb(buffer.byteBufferData, offset).getU());
        assertEquals(1589, MRZ.getWCrange_samples(buffer.byteBufferData, offset).getU());
        assertEquals(0.0, MRZ.getWCNomBeamAngleAcross_deg(buffer.byteBufferData, offset).get(), 0.001);
        assertEquals(55.236, MRZ.getMeanAbsCoeff_dBPerkm(buffer.byteBufferData, offset).get(), 0.001);
        assertEquals(-32.106, MRZ.getReflectivity1_dB(buffer.byteBufferData, offset).get(), 0.001);
        assertEquals(0.0, MRZ.getReflectivity2_dB(buffer.byteBufferData, offset).get(), 0.001);
        assertEquals(0.0, MRZ.getReceiverSensitivityApplied_dB(buffer.byteBufferData, offset).get(), 0.001);
        assertEquals(0.0, MRZ.getSourceLevelApplied_dB(buffer.byteBufferData, offset).get(), 0.001);
        assertEquals(0.0, MRZ.getBScalibration_dB(buffer.byteBufferData, offset).get(), 0.001);
        assertEquals(73.829, MRZ.getTVG_dB(buffer.byteBufferData, offset).get(), 0.001);
        assertEquals(-33.13, MRZ.getBeamAngleReRx_deg(buffer.byteBufferData, offset).get(), 0.001);
        assertEquals(0.0, MRZ.getBeamAngleCorrection_deg(buffer.byteBufferData, offset).get(), 0.001);
        assertEquals(0.103, MRZ.getTwoWayTravelTime_sec(buffer.byteBufferData, offset).get(), 0.001);
        assertEquals(0.0, MRZ.getTwoWayTravelTimeCorrection_sec(buffer.byteBufferData, offset).get(), 0.001);
        assertEquals(0.0, MRZ.getDeltaLatitude_deg(buffer.byteBufferData, offset).get(), 0.001);
        assertEquals(-0.0008, MRZ.getDeltaLongitude_deg(buffer.byteBufferData, offset).get(), 0.001);
        assertEquals(20.127, MRZ.getZ_reRefPoint_m(buffer.byteBufferData, offset).get(), 0.001);
        assertEquals(-72.8465, MRZ.getY_reRefPoint_m(buffer.byteBufferData, offset).get(), 0.001);
        assertEquals(4.903, MRZ.getX_reRefPoint_m(buffer.byteBufferData, offset).get(), 0.001);
        assertEquals(0.0, MRZ.getBeamIncAngleAdj_deg(buffer.byteBufferData, offset).get(), 0.001);
        assertEquals(0, MRZ.getRealTimeCleanInfo(buffer.byteBufferData, offset).getU());
        assertEquals(3192, MRZ.getSIstartRange_samples(buffer.byteBufferData, offset).getU());
        assertEquals(2, MRZ.getSIcentreSample(buffer.byteBufferData, offset).getU());
        assertEquals(3, MRZ.getSInumSamples(buffer.byteBufferData, offset).getU());
        
        // go few datagrams forward
        offset = MRZ.getNextSoundingOffset(buffer.byteBufferData, offset);
        assertEquals(1, MRZ.getSoundingIndex(buffer.byteBufferData, offset).getU());
        offset = MRZ.getNextSoundingOffset(buffer.byteBufferData, offset);
        assertEquals(2, MRZ.getSoundingIndex(buffer.byteBufferData, offset).getU());
        
        assertEquals(2960, MRZ.getSIsample_desidB(buffer.byteBufferData).length);
    }
    
    @Test
    public void testValidOverallSize() throws IOException {
        DatagramBuffer buffer = getDatagram("MRZ");
        
        final int rxInfoOffset = MRZ.getRxInfoOffset(buffer.byteBufferData);
        int soundingOffset = MRZ.getSoundingOffset(buffer.byteBufferData, rxInfoOffset);
        int nSamples = 0;
        int nSoundins = MRZ.getNumSoundingsMaxMain(buffer.byteBufferData, rxInfoOffset).getU() + MRZ.getNumExtraDetections(buffer.byteBufferData, rxInfoOffset).getU();
        for (int i = 0; i <  nSoundins; i++) {
            nSamples += MRZ.getSInumSamples(buffer.byteBufferData, soundingOffset).getU();
            soundingOffset = MRZ.getNextSoundingOffset(buffer.byteBufferData, soundingOffset);
        }
        assertEquals(2960, nSamples);
        
        // check that after the all the sounding block and nSamples uint16, we find the repeated size of the datagram
        assertEquals(Integer.toUnsignedLong(buffer.byteBufferSizeHeader.getInt(0)),
                Integer.toUnsignedLong(buffer.byteBufferData.getInt(soundingOffset + 2 * nSamples)));
    }
}
