package fr.ifremer.globe.api.xsf.tests.all.utils;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.common.exceptions.IndexingException;
import fr.ifremer.globe.api.xsf.util.SwathIdentifier;
import fr.ifremer.globe.api.xsf.util.SwathIndexContainer;
import fr.ifremer.globe.api.xsf.util.SwathIndexGenerator;

public class TestSwathIndexGenerator {

	/**
	 * Check a simple sequence of pings
	 * 
	 * @throws IndexingException
	 */
	@Test
	public void testSimpleSequence() throws IndexingException {
		SwathIndexGenerator gen = new SwathIndexGenerator();
		SwathIndexContainer cont = new SwathIndexContainer();

		SwathIdentifier id1 = gen.addId(1, 1, 0, 1);
		SwathIdentifier id2 = gen.addId(2, 1, 0, 2);
		SwathIdentifier id3 = gen.addId(3, 1, 0, 3);
		cont.generateIndex(gen, t -> true);

		assertEquals(0, (int) cont.getIndex(id1).get());
		assertEquals(1, (int) cont.getIndex(id2).get());
		assertEquals(2, (int) cont.getIndex(id3).get());
		assertEquals(3, cont.getIndexSize());
	}

	/**
	 * Check the behaviour when ping do not come in the right order
	 * 
	 * @throws IndexingException
	 */
	@Test
	public void testIncorrectIncomingOrder() throws IndexingException {
		SwathIndexGenerator gen = new SwathIndexGenerator();
		SwathIndexContainer cont = new SwathIndexContainer();

		SwathIdentifier id2 = gen.addId(2, 1, 0, 2);
		SwathIdentifier id1 = gen.addId(1, 1, 0, 1);
		SwathIdentifier id3 = gen.addId(3, 1, 0, 3);
		cont.generateIndex(gen, t -> true);

		assertEquals(0, (int) cont.getIndex(id1).get());
		assertEquals(1, (int) cont.getIndex(id2).get());
		assertEquals(2, (int) cont.getIndex(id3).get());
		assertEquals(3, cont.getIndexSize());
	}

	/**
	 * Check behaviour when ping number is resetted to zero
	 * 
	 * @throws IndexingException
	 * 
	 */
	@Test
	public void testOverflow() throws IndexingException {
		SwathIndexGenerator gen = new SwathIndexGenerator();
		SwathIndexContainer cont = new SwathIndexContainer();

		SwathIdentifier id1 = gen.addId(65534, 1, 0, 1);
		SwathIdentifier id2 = gen.addId(0, 1, 0, 2);
		SwathIdentifier id3 = gen.addId(1, 1, 0, 3);

		cont.generateIndex(gen, t -> true);

		assertEquals(0, (int) cont.getIndex(id1).get());
		assertEquals(1, (int) cont.getIndex(id2).get());
		assertEquals(2, (int) cont.getIndex(id3).get());
		assertEquals(3, cont.getIndexSize());
	}

	/**
	 * Check behaviour when several datagram have the same ping number
	 * 
	 * @throws IndexingException
	 * 
	 */
	@Test
	public void testMultipleDef() throws IndexingException {
		SwathIndexGenerator gen = new SwathIndexGenerator();
		SwathIndexContainer cont = new SwathIndexContainer();

		SwathIdentifier id1 = gen.addId(1, 1, 0, 1);

		@SuppressWarnings("unused")
		SwathIdentifier id1_bis = gen.addId(1, 1, 0, 1);
		SwathIdentifier id2 = gen.addId(2, 1, 0, 2);
		SwathIdentifier id3 = gen.addId(3, 1, 0, 3);
		gen.addId(1, 1, 0, 1);
		gen.addId(3, 1, 0, 3);
		gen.addId(3, 1, 0, 3);
		cont.generateIndex(gen, t -> true);
		assertEquals(3, cont.getIndexSize());

		assertEquals(0, (int) cont.getIndex(id1).get());
		assertEquals(1, (int) cont.getIndex(id2).get());
		assertEquals(2, (int) cont.getIndex(id3).get());
	}
}
