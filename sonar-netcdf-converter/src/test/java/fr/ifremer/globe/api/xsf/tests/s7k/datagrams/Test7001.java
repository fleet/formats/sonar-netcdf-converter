package fr.ifremer.globe.api.xsf.tests.s7k.datagrams;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramParser;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K7001;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K7001.DeviceInformation;

public class Test7001 extends TestDatagram {
	
	@Test
	public void testDecode() throws Exception {
		DatagramBuffer buffer = getDataBuffer(7001);
	
		assertEquals(7001, DatagramParser.getDatagramType(buffer));
		assertEquals(0l, S7K7001.getSonarSerialnumber(buffer).getLong());
		assertEquals(1l, S7K7001.getNumberOfDevices(buffer).getU());
		
		DeviceInformation[] dis = S7K7001.getDeviceInformations(buffer);
		assertEquals(1, dis.length);
		
		assertEquals(0l, dis[0].uniqueIdentifier.getU());
		assertEquals("n/a", dis[0].description);
		assertEquals(0l, dis[0].serialNumber.getLong());
		assertEquals("<?xml version=\"1.0\" encoding=\"US-ASCII\"?>\r\n" + 
				"\r\n" + 
				"<SB7150HF>\r\n" + 
				"  <Name deviceid=\"7150\" subsystemid=\"1\" enumerator=\"0\">7150 (24kHz)</Name>\r\n" + 
				"  <SystemInfo auv=\"no\" projectid=\"ifremer\" name=\"7150\">7150 Sonar</SystemInfo>\r\n" + 
				"  <SonarType type=\"0\" unit=\"nb\">Bathymetric sonar.</SonarType>\r\n" + 
				"  <ArrayType type=\"1\" mounting=\"fixed\">Flat array.</ArrayType>\r\n" + 
				"  <RxElements min=\"0\" max=\"287\"  actual=\"288\" reversed=\"yes\" spacing=\"0.0270833\" unit=\"meters\" input_mux=\"2\">Receive ceramics.</RxElements>\r\n" + 
				"  <RxBeams min=\"0\" max=\"879\" unit=\"nb\" max_sin=\"80\">Receive beams.</RxBeams>\r\n" + 
				"  <RxBeamSpacing uniform=\"no\">Receiver beamspacing.</RxBeamSpacing>\r\n" + 
				"  <RxBeamWidth center_beamwidth=\"0.5\" uniformacross=\"yes\" uniformalong=\"yes\" across=\"0.008726646\" along=\"0.471238898\" unit=\"rad\" proj_beamwidth=\"1.0\">Receiver beam width.</RxBeamWidth>\r\n" + 
				"  <RxBeamWedge coverage=\"150.78\" min_coverage=\"120.0\" max_coverage=\"151.0\" unit=\"deg\">Receiver beam wedge info.</RxBeamWedge>\r\n" + 
				"  <RxCoverageSet size=\"8\" _1=\"150.78,880\" _2=\"140.0,630\" _3=\"135.0,554\" _4=\"130.0,492\" _5=\"125.0,442\" _6=\"120.0,398\" _7=\"115.0,360\" _8=\"120.0,880\">Valid wedge coverages</RxCoverageSet>\r\n" + 
				"  <RxBeamStabilization type=\"0\">Receive beam stabilization.</RxBeamStabilization>\r\n" + 
				"  <TxElements min=\"0\" max=\"179\" spacing=\"0.04333\" unit=\"meters\">Transmit ceramics.</TxElements>\r\n" + 
				"  <TxBeams min=\"0\" max=\"0\" unit=\"nb\" >Transmit beams.</TxBeams>\r\n" + 
				"  <TxBeamSteering steerable=\"no\" StaticSteering=\"-0.0349\" maxx=\"0.0\" minx=\"0.0\" maxz=\"0.0\" minz=\"0.0\" unit=\"rad\">Transmit beam steering.</TxBeamSteering>\r\n" + 
				"  <TxBeamSpacing uniform=\"yes\" angles=\"0.0\" unit=\"rad\" >Transmit beamspacing.</TxBeamSpacing>\r\n" + 
				"  <TxBeamWidth variable=\"no\" maxx=\"0.06981315\" minx=\"0.008726643\" maxz=\"2.094395102\" minz=\"2.094395102\"  unit=\"rad\">Transmit beamwidth.</TxBeamWidth>\r\n" + 
				"  <TxBeamStabilization type=\"0\">Transmit beam stabilization.</TxBeamStabilization>\r\n" + 
				"  <TxPulseLength min=\"0.00057\" max=\"0.02000\" type=\"Rectangular\" unit=\"s\" measured=\"10e-7\">Transmit pulse length.</TxPulseLength>\r\n" + 
				"  <TxRxDelayOffset base=\"-2292\" unit=\"1/256th sample\">Rx-Rx Delay Offset.</TxRxDelayOffset>\r\n" + 
				"  <Multiping mode=\"MP4\" DisableMPPitchSteering=\"no\" ManualFreq=\"24500,22500,25500,23500\" DeltaT=\"0,0,0,0\" RxSkip=\"0,0,0,0\">Multiping mode.</Multiping>\r\n" + 
				"  <Frequency chirp=\"no\" min=\"23975\" max=\"24025\" center=\"24000.0\" unit=\"hz\">Transmit frequency.</Frequency>\r\n" + 
				"  <SampleRate rate=\"6022.282445\" unit=\"hz\">Receiver sample rate.</SampleRate>\r\n" + 
				"  <Power min=\"170.0\" max=\"236.0\" tx_power_tweak=\"0\" shared=\"yes\" unit=\"dB//uPa\">Transmit power.</Power>\r\n" + 
				"  <Gain min=\"-70.0\" max=\"70.0\" unit=\"dB\" tvg_limit=\"70\">Receiver gain.</Gain>\r\n" + 
				"  <Range min=\"100.0\" max=\"9600.0\" unit=\"m\">Operating range.</Range>\r\n" + 
				"  <RangeSet size=\"12\" _1=\"100\" _2=\"200\" _3=\"400\" _4=\"600\" _5=\"800\" _6=\"1200\" _7=\"1600\" _8=\"2400\" _9=\"3200\" _10=\"4800\" _11=\"6400\" _12=\"9600\" unit=\"m\">Valid Range Set.</RangeSet> \r\n" + 
				"  <PingRate min=\"0.0\" max=\"15.0\" ratio=\"1.0\" freerun=\"yes\" unit=\"p/s\">Ping rate.</PingRate>\r\n" + 
				"  <Motion rollable=\"yes\" pitchable=\"yes\" heavable=\"no\" roll=\"-1\" pitch=\"26354\" heave=\"0\"  roll_ON=\"yes\" pitch_ON=\"yes\">Motion compensation factor.</Motion>\r\n" + 
				"  <FWInfo type=\"dual\" bite=\"new\" bf_upm_level=\"1\" TimingScheme=\"0\">Firmware Info.</FWInfo>\r\n" + 
				"  <FPGA lo_if=\"455285.0\" tx_addr=\"0x2B\" bitfile0=\"bf288_20100222b.bit\" bitfile1=\"coprocISE82.bit\">FPGA Sonar Specific Values.</FPGA>\r\n" + 
				"  <DownLink register=\"yes\" remote=\"no\">DownLink</DownLink>\r\n" + 
				"  <TxType type=\"freq_shift\" number=\"0\">TxType</TxType>\r\n" + 
				"  <RDR limitsize=\"yes\" maxsize=\"400000000\" units=\"Bytes\" format=\"short\">Raw data recording.</RDR>\r\n" + 
				"  <StartState APTable=\"7150_24MP_May11_XL.apc\" maxpower=\"0.0\" ping=\"yes\" selected=\"yes\" calibrate=\"apply\" pingmode=\"MP4\">Initial overwrite values.</StartState>\r\n" + 
				"  <BottomDetection method=\"IF1\" min_phase_correl=\"0.65\" threshold=\"20\">BD Method (G1_Simple, G1_BlendFilt, G2, IF1).</BottomDetection>\r\n" + 
				"  <Trigger type=\"hardware\">External trigger setting</Trigger>\r\n" + 
				"  <ShadingVector RxShading=\"40.0\">Shading vector setting.</ShadingVector>\r\n" + 
				"  <Beamformer AngleCorrection=\"1.0\">Beamformer parameters.</Beamformer>\r\n" + 
				"</SB7150HF>", dis[0].info);
	}
}
