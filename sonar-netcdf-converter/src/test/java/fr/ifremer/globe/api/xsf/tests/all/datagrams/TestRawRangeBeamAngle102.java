package fr.ifremer.globe.api.xsf.tests.all.datagrams;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.rawrange102.RawRange102;

public class TestRawRangeBeamAngle102 extends DatagramTest {

	@Test
	public void testGetBody() throws IOException {
		DatagramBuffer buffer = getDatagram((byte)0x66);
		
		assertEquals(RawRange102.getSerialNumber(buffer.byteBufferData).getU(), 593);
		int nxTxSectors = RawRange102.getNTxTransmitSectorCounter(buffer.byteBufferData).getU();
		assertEquals(nxTxSectors, 1);
		int nBeams = RawRange102.getReceivedBeamCounter(buffer.byteBufferData).getU();
		assertEquals(nBeams, 254);
		assertEquals(RawRange102.getSamplingFn(buffer.byteBufferData).getU(), 1395600);
		assertEquals(RawRange102.getROVDepth(buffer.byteBufferData).get(), 0, 0.00001);
		assertEquals(RawRange102.getSoundSpeed(buffer.byteBufferData).getU(), 15050);
		assertEquals(RawRange102.getTotalBeamCounter(buffer.byteBufferData).getU(), 254);
		
		// Check Tx Sector
		int iTxSector = 0;
		assertEquals(RawRange102.getTxTilt(buffer.byteBufferData, iTxSector).get(), 0);
		assertEquals(RawRange102.getFocusRange(buffer.byteBufferData, iTxSector).getU(), 0);
		assertEquals(RawRange102.getSignalLength(buffer.byteBufferData, iTxSector).getU(), 150);
		assertEquals(RawRange102.getTransmitTimeOffset(buffer.byteBufferData, iTxSector).getU(), 0);
		assertEquals(RawRange102.getTxCenterFrequency(buffer.byteBufferData, iTxSector).getU(), 293068);
		assertEquals(RawRange102.getSignalBandwidth(buffer.byteBufferData, iTxSector).getU(), 667);
		assertEquals(RawRange102.getWaveFormIdentifier(buffer.byteBufferData, iTxSector).getU(), 0);
		assertEquals(RawRange102.getTxSectorCounter(buffer.byteBufferData, iTxSector).getU(), 0);


		// Check Entries Block (Samples)
		int iBeam = 0;
		int offset = 56 + (nxTxSectors-1) * 20 + iBeam * 12;	
		assertEquals(RawRange102.getBeamPointingAngle(buffer.byteBufferData, offset).get(), 4211);
		assertEquals(RawRange102.getRange(buffer.byteBufferData, offset).getU(), 6022);
		assertEquals(RawRange102.getTxSectorIndex(buffer.byteBufferData, offset).getU(), 0);
		assertEquals(RawRange102.getReflectivity(buffer.byteBufferData, offset).get(), -45);
		assertEquals(RawRange102.getQualityFactor(buffer.byteBufferData, offset).getU(), 130);
		assertEquals(RawRange102.getDetectionLength(buffer.byteBufferData, offset).getU(), 87);
		assertEquals(RawRange102.getBeamIndex(buffer.byteBufferData, offset).get(), 0);

		iBeam = nBeams - 1;
		offset = 56 + (nxTxSectors-1) * 20 + iBeam * 12;	
		assertEquals(RawRange102.getBeamPointingAngle(buffer.byteBufferData, offset).get(), -5788);
		assertEquals(RawRange102.getRange(buffer.byteBufferData, offset).getU(), 1709);
		assertEquals(RawRange102.getTxSectorIndex(buffer.byteBufferData, offset).getU(), 0);
		assertEquals(RawRange102.getReflectivity(buffer.byteBufferData, offset).get(), -36);
		assertEquals(RawRange102.getQualityFactor(buffer.byteBufferData, offset).getU(), 12);
		assertEquals(RawRange102.getDetectionLength(buffer.byteBufferData, offset).getU(), 27);
		assertEquals(RawRange102.getBeamIndex(buffer.byteBufferData, offset).get(), 253);

	}
}
