package fr.ifremer.globe.api.xsf.tests.xsfvalidation;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.api.xsf.converter.SounderFileConverter;
import fr.ifremer.globe.api.xsf.converter.XsfConverterParameters;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.BathymetryGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.BeamGroup1Grp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.VendorSpecificGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.BeamGroup1VendorSpecificGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.types.DetectionTypeHelper;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.PlatformLayers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.BeamGroup1Layers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.SonarDetectionStatus;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.beam_group1.BathymetryLayers;
import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCFile;
import fr.ifremer.globe.netcdf.ucar.NCFile.Mode;
import fr.ifremer.globe.netcdf.ucar.NCGroup;
import fr.ifremer.globe.netcdf.ucar.NCVariable;
import fr.ifremer.globe.utils.exception.GIOException;

public class XSFResonValidation {
	protected Logger logger = LoggerFactory.getLogger(XSFResonValidation.class);

	private final File in = TestConstants.inReson;
	private final File xsf = TestConstants.xsfReson;

	private final static boolean cleanupAfter= true;
	
	@Rule
	public TemporaryFolder tmp = new TemporaryFolder();
	private NCFile reader;

	@After
	public void DeleteFiles() {
		try {
			destroy();
			Files.delete(xsf.toPath());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Before
	public void CreateFiles() throws GIOException, IOException, NCException {

		boolean regenerate = true;
		if (regenerate) {
			if (xsf.exists())
				Files.delete(xsf.toPath());
			new SounderFileConverter()
					.convertToXsf(new XsfConverterParameters(in.getCanonicalPath(), xsf.getCanonicalPath()));
		}
	}

	@Test
	public void testXSF() throws NCException {
		reader = NCFile.open(xsf.getAbsolutePath(), Mode.readonly);
		testBeamGroup(reader);
		testPlatformAndPosition(reader);
		testBeamGroupWC(reader);
	}

	public void destroy() {
		if(!cleanupAfter)
			return;
		// perform cleanup
		if (reader != null)
			reader.close();
	}

	/**
	 * Test function for wc data
	 * 
	 * @param src
	 */
	private void testBeamGroupWC(NCFile src) throws NCException {
		// TODO
		// equivalent_beam_angle

	}

	/**
	 * Test for beam detection variables
	 * 
	 */
	private void testBeamGroup(NCFile src) throws NCException {
		// detection count
		NCGroup bathyGrp = src.getGroup(BathymetryLayers.GROUP);
		NCGroup beamGrp = src.getGroup(BeamGroup1Layers.GROUP);
		NCGroup beamVendor = beamGrp.getGroup(VendorSpecificGrp.GROUP_NAME);
		// check group is found (might have null pointer exception if one of them is not found
		Assert.assertNotNull(beamGrp);
		NCDimension dimDetection = bathyGrp.getDimension(BathymetryGrp.DETECTION_DIM_NAME);
		Assert.assertEquals(880, dimDetection.getLength());

		NCDimension dimBeam = beamGrp.getDimension(BeamGroup1Grp.BEAM_DIM_NAME);
		Assert.assertEquals(880, dimBeam.getLength());

		NCDimension dimSwath = beamGrp.getDimension(BeamGroup1Grp.PING_TIME_DIM_NAME);
		NCDimension dimSector = beamGrp.getDimension(BeamGroup1Grp.TX_BEAM_DIM_NAME);

		Assert.assertEquals(540, dimSwath.getLength());
		Assert.assertEquals(1, dimSector.getLength());

		// check beam_direction

		/*
		 * check beam witdh, verify that they are around the expected values (0.6 deg for Tx and 1.0 deg for Rx) Real
		 * values depends on beam pointing angle
		 */
		{
			long[] start = { 0, 0 };

			{
				long[] count = { dimSwath.getLength(), dimBeam.getLength() };
				float[] rxmin = beamGrp.getVariable(BeamGroup1Grp.BEAMWIDTH_RECEIVE_MINOR).get_float(start, count);
				float[] rxmaj = beamGrp.getVariable(BeamGroup1Grp.BEAMWIDTH_RECEIVE_MAJOR).get_float(start, count);

				int swath = 2;
				{
					for (int beam = 0; beam < dimBeam.getLength(); beam++) {
						Assert.assertEquals("Expecting beam width error (swath,beam)[" + swath + "," + beam + "]", 1.0f,
								rxmin[(int) (swath * dimBeam.getLength() + beam)], 10e-4f);
						Assert.assertEquals("Expecting beam width error (swath,beam)[" + swath + "," + beam + "]",
								0.136519f, rxmaj[(int) (swath * dimBeam.getLength() + beam)], 10e-4f);
					}
				}
			}
			{
				// long[] count = { dimSwath.getLength(), dimSector.getLength() };
				// float[] txmin = grp.getVariable("beamwidth_transmit_minor").get_float(start, count);
				// float[] txmaj = grp.getVariable("beamwidth_transmit_major").get_float(start, count);
				// int swath = 2;
				// {
				// for (int sector = 0; sector < dimSector.getLength(); sector++) {
				// // no info about beam width for tx
				// Assert.assertTrue(Float.isNaN(txmin[(int) (swath * dimSector.getLength() + sector)]));
				// Assert.assertTrue(Float.isNaN(txmaj[(int) (swath * dimSector.getLength() + sector)]));
				// }
				// }

			}
		}
		// detection angle
		{
			long[] start = { 0, 0 };
			long[] count = { dimSwath.getLength(), dimDetection.getLength() };
			int swath = 2;
			{
				float[] angle = bathyGrp.getVariable(BathymetryGrp.DETECTION_BEAM_POINTING_ANGLE).get_float(start,
						count);
				for (int detection = 0; detection < dimDetection.getLength(); detection++) {
					float angleRead = angle[(int) (swath * dimDetection.getLength() + detection)];
					Assert.assertTrue(-60.1 < angleRead && angleRead < 60.1);
				}
				float angleRead = angle[(int) (swath * dimDetection.getLength() + 0)];
				Assert.assertEquals(59.97f, angleRead, 10 - 3);
			}
		}
		// lat/long
		{
			long[] start = { 0, 0 };
			long[] count = { dimSwath.getLength(), dimDetection.getLength() };
			double[] lat = bathyGrp.getVariable(BathymetryGrp.DETECTION_LATITUDE).get_double(start, count);
			double[] lon = bathyGrp.getVariable(BathymetryGrp.DETECTION_LONGITUDE).get_double(start, count);
			// no data in case of .all files
			for (int swath = 0; swath < dimSwath.getLength(); swath++) {
				for (int detection = 0; detection < dimBeam.getLength(); detection++) {
					double valueLat = lat[(int) (swath * dimBeam.getLength() + detection)];
					double valueLong = lon[(int) (swath * dimBeam.getLength() + detection)];

					// check that value is roughly equals to 47.5 deg
					Assert.assertEquals(43.9, valueLat, 0.2);

					// check that value is roughly equals to 47.5 deg
					Assert.assertEquals(30.8, valueLong, 0.1);

				}
			}
		}
//		// detection_reflectivity : computed with snippets values
//		{
//			long[] start = { 0, 0 };
//			long[] count = { dimSwath.getLength(), dimDetection.getLength() };
//			float[] bs_c = bathyGrp.getVariable(BathymetryGrp.DETECTION_BACKSCATTER_R).get_float(start, count);
//			// no data in case of .all files
//			for (int swath = 0; swath < dimSwath.getLength(); swath++) {
//				for (int detection = 0; detection < dimBeam.getLength(); detection++) {
//					float value = bs_c[(int) (swath * dimBeam.getLength() + detection)];
//					Assert.assertTrue(Float.isNaN(value));
//				}
//			}
//		}
		// detection_rx_transducer_index
		{
			long[] start = { 0, 0 };
			long[] count = { dimSwath.getLength(), dimDetection.getLength() };
			short[] values = bathyGrp.getVariable(BathymetryGrp.DETECTION_RX_TRANSDUCER_INDEX).get_ushort(start, count);
			// no data in case of .all files
			for (int swath = 0; swath < dimSwath.getLength(); swath++) {
				for (int detection = 0; detection < dimBeam.getLength(); detection++) {
					short value = values[(int) (swath * dimBeam.getLength() + detection)];
					int index = Short.toUnsignedInt(value);
					Assert.assertTrue(index == 1);
				}
			}
		}

		// detection_two_way_travel_time, check first value extracted from datagram viewer
		{
			long[] start = { 0, 0 };
			long[] count = { 1, 1 };
			float[] values = bathyGrp.getVariable(BathymetryGrp.DETECTION_TWO_WAY_TRAVEL_TIME).get_float(start, count);
			float value = values[0];
			Assert.assertEquals(2.062028, value, 10e-3);
		}

		// detection_tx_sector, 1 sectors in this file
		{
			long[] start = { 0, 0 };
			long[] count = { dimSwath.getLength(), dimDetection.getLength() };
			short[] values = bathyGrp.getVariable(BathymetryGrp.DETECTION_TX_BEAM).get_short(start, count);
			// no data in case of .all files
			for (int swath = 0; swath < dimSwath.getLength(); swath++) {
				for (int detection = 0; detection < dimBeam.getLength(); detection++) {
					short value = values[(int) (swath * dimBeam.getLength() + detection)];
					Assert.assertTrue(value == 0);
				}
			}
		}
		// detection_tx_transducer_index, on single tx
		{
			long[] start = { 0, 0 };
			long[] count = { dimSwath.getLength(), dimDetection.getLength() };
			short[] values = bathyGrp.getVariable(BathymetryGrp.DETECTION_TX_TRANSDUCER_INDEX).get_ushort(start, count);
			// no data in case of .all files
			for (int swath = 0; swath < dimSwath.getLength(); swath++) {
				for (int detection = 0; detection < dimBeam.getLength(); detection++) {
					short value = values[(int) (swath * dimBeam.getLength() + detection)];
					int index = Short.toUnsignedInt(value);
					Assert.assertTrue(index == 0);
				}
			}
		}

		// detection x y z
		{
			NCVariable xVar = bathyGrp.getVariable(BathymetryGrp.DETECTION_X);
			NCVariable yVar = bathyGrp.getVariable(BathymetryGrp.DETECTION_Y);
			NCVariable zVar = bathyGrp.getVariable(BathymetryGrp.DETECTION_Z);
			long[] start = { 0, 0 };
			long[] count = { 1, 1 };
			float[] x = xVar.get_float(start, count);
			float[] y = yVar.get_float(start, count);
			float[] z = zVar.get_float(start, count);

			// check global values for detection
			Assert.assertEquals(23.68, x[0], .1);
			Assert.assertEquals(-1285.21, y[0], 200.0);
			Assert.assertEquals(822.84, z[0], .1);
		}
		// detection_type
		{
			long[] start = { 0, 0 };
			long[] count = { dimSwath.getLength(), dimDetection.getLength() };
			byte[] values = bathyGrp.getVariable(BathymetryGrp.DETECTION_TYPE).get_byte(start, count);

			// from datagram viewer
			int swath = 0;
			int detection = 374;
			byte value = values[(int) (swath * dimDetection.getLength() + detection)];
			Assert.assertEquals(DetectionTypeHelper.INVALID.get(), value);

			// phase
			swath = 0;
			detection = 104;
			value = values[(int) (swath * dimDetection.getLength() + detection)];
			Assert.assertEquals(DetectionTypeHelper.PHASE.get(), value);

			// amplitude
			swath = 0;
			detection = 446;
			value = values[(int) (swath * dimDetection.getLength() + detection)];
			Assert.assertEquals(DetectionTypeHelper.AMPLITUDE.get(), value);
		}

		// platform_heading s7k dataview is not very helpfull here
		{

			long[] start = { 0 };
			long[] count = { dimSwath.getLength() };
			float[] values = beamGrp.getVariable(BeamGroup1Grp.PLATFORM_HEADING).get_float(start, count);
			float[] expected = { 215.6277f };
			for (int swath = 0; swath < expected.length; swath++) {
				Assert.assertEquals(String.format("Error comparing heading for swath [%d]", swath), expected[swath],
						values[swath], 10e-3);
			}
		}

		// platform_latitude
		// platform_longitude
		{
			long[] start = { 0 };
			long[] count = { dimSwath.getLength() };
			double[] valuesLong = beamGrp.getVariable(BeamGroup1Grp.PLATFORM_LONGITUDE).get_double(start, count);
			double[] valuesLat = beamGrp.getVariable(BeamGroup1Grp.PLATFORM_LATITUDE).get_double(start, count);

			// interpolated, just test if around specific value
			for (int swath = 0; swath < dimSwath.getLength(); swath++) {
				Assert.assertEquals(43.934, valuesLat[swath], 10e-1);
				Assert.assertEquals(30.8645, valuesLong[swath], 10e-1);
			}

			// according to Sonar record viewer, pitch is roughly 0 at the time of the second ping and 0.15 at the time
			// for the roll
			double[] valuesPitch = beamGrp.getVariable(BeamGroup1Grp.PLATFORM_PITCH).get_double(start, count);
			double[] valuesRoll = beamGrp.getVariable(BeamGroup1Grp.PLATFORM_ROLL).get_double(start, count);
			Assert.assertEquals(0.192999, valuesRoll[1], 10e-3);
			Assert.assertEquals(0.522999, valuesPitch[1], 10e-3);

		}

		// TODO
		// sound_speed_at_transducer
		{
			long[] start = { 0 };
			long[] count = { 1 };
			float[] values = beamGrp.getVariable(BeamGroup1Grp.SOUND_SPEED_AT_TRANSDUCER).get_float(start, count);
			Assert.assertEquals(1514.5799f, values[0], 10e-3);

		}
		// status
		// status_detail
		{
			long[] start = { 0, 0 };
			long[] count = { dimSwath.getLength(), dimDetection.getLength() };
			byte[] values = bathyGrp.getVariable(BathymetryGrp.STATUS).get_byte(start, count);
			// status_detail not checked
			@SuppressWarnings("unused")
			byte[] status_detail = bathyGrp.getVariable(BathymetryGrp.STATUS_DETAIL).get_byte(start, count);
			// values extracted from datagram viewer
			Assert.assertTrue(!SonarDetectionStatus.isValid(values[374]));
			Assert.assertTrue(
					SonarDetectionStatus.getStatus(values[374]).contains(SonarDetectionStatus.INVALID_ACQUISITION));
			Assert.assertTrue(SonarDetectionStatus.isValid(values[0]));

		}
		// swath_dynamic_draught
		// swath_tide
		{
			long[] start = { 0 };
			long[] count = { dimSwath.getLength() };
			float[] values = beamGrp.getVariable(BeamGroup1Grp.WATERLINE_TO_CHART_DATUM).get_float(start, count);
			for (int i = 0; i < dimSwath.getLength(); i++) {
				Assert.assertEquals(0, values[i], 10e-6);
			}
		}
		// swath_time
		{
			long[] start = { 0 };
			long[] count = { dimSwath.getLength() };
			long[] values = beamGrp.getVariable(BeamGroup1Grp.PING_TIME).get_ulong(start, count);

			// check first value
			int i = 0;
			long timeMilli = Long.divideUnsigned(values[i], 1_000_000);

			LocalDateTime t = LocalDateTime.ofInstant(Instant.ofEpochMilli(timeMilli), ZoneId.of("UTC"));
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss.SSS");
			Assert.assertEquals("02/09/2015 04:11:08.760", t.format(formatter));
			// we do not check nano second, since they are missing from .all file format
			long first = 0;
			// check that values are increasing
			for (int swath = 0; swath < dimSwath.getLength(); swath++) {
				Assert.assertTrue(Long.compareUnsigned(values[swath], first) >= 0);
				first = values[swath];
			}
		}
		// tx_center_frequency
		{
			long[] start = { 0, 0 };
			long[] count = { dimSwath.getLength(), dimSector.getLength() };
			float[] values = beamGrp.getVariable(BeamGroup1Grp.TRANSMIT_FREQUENCY_START).get_float(start, count);
			float[] expected = { 24500 };
			for (int i = 0; i < dimSector.getLength(); i++) {
				Assert.assertEquals(expected[i], values[i], 10e-3f);
			}
		}

		// beam_rx_transducer_index

		// ifremer flag
		{
			long[] start = { 0, 0 };
			long[] count = { dimSwath.getLength(), dimDetection.getLength() };
			float[] bs_c = bathyGrp.getVariable(BathymetryGrp.DETECTION_QUALITY_FACTOR).get_float(start, count);
			// no data in case of .all files
			for (int swath = 0; swath < dimSwath.getLength(); swath++) {
				for (int detection = 0; detection < dimBeam.getLength(); detection++) {
					float value = bs_c[(int) (swath * dimBeam.getLength() + detection)];
					Assert.assertTrue(!Float.isNaN(value));
				}
			}
		}
		// backscatter_calibration
		{
			// long[] start = { 0, 0 };
			// long[] count = { dimSwath.getLength(), dimDetection.getLength() };
			// float[] bs_c = grp.getVariable("detection_receiver_sensitivity_applied").get_float(start, count);
			// // no data in case of .all files
			// for (int swath = 0; swath < dimSwath.getLength(); swath++) {
			// for (int detection = 0; detection < dimBeam.getLength(); detection++) {
			// float value = bs_c[(int) (swath * dimBeam.getLength() + detection)];
			// Assert.assertTrue(Float.isNaN(value));
			// }
			// }
		}
		// backscatter_calibration
		{
			// long[] start = { 0, 0 };
			// long[] count = { dimSwath.getLength(), dimDetection.getLength() };
			// float[] bs_c = grp.getVariable("detection_backscatter_calibration").get_float(start, count);
			// // no data in case of .all files
			// for (int swath = 0; swath < dimSwath.getLength(); swath++) {
			// for (int detection = 0; detection < dimBeam.getLength(); detection++) {
			// float value = bs_c[(int) (swath * dimBeam.getLength() + detection)];
			// Assert.assertTrue(Float.isNaN(value));
			// }
			// }
		}
		// detection_mean_absorption_coefficient
		{
			// long[] start = { 0, 0 };
			// long[] count = { dimSwath.getLength(), dimDetection.getLength() };
			// float[] bs_c = grp.getVariable("detection_mean_absorption_coefficient").get_float(start, count);
			// // no data in case of .all files
			// for (int swath = 0; swath < dimSwath.getLength(); swath++) {
			// for (int detection = 0; detection < dimBeam.getLength(); detection++) {
			// float value = bs_c[(int) (swath * dimBeam.getLength() + detection)];
			// Assert.assertTrue(Float.isNaN(value));
			// }
			// }
		}
		// detection_source_level_applied, not available for .all files
		{
			// long[] start = { 0, 0 };
			// long[] count = { dimSwath.getLength(), dimDetection.getLength() };
			// float[] values = grp.getVariable("detection_source_level_applied").get_float(start, count);
			// // no data in case of .all files
			// for (int swath = 0; swath < dimSwath.getLength(); swath++) {
			// for (int detection = 0; detection < dimBeam.getLength(); detection++) {
			// float value = values[(int) (swath * dimBeam.getLength() + detection)];
			// Assert.assertTrue(Float.isNaN(value));
			// }
			// }
		}
		// detection_time_varying_gain, not available for .all files
		{
			// long[] start = { 0, 0 };
			// long[] count = { dimSwath.getLength(), dimDetection.getLength() };
			// float[] values = grp.getVariable("detection_time_varying_gain").get_float(start, count);
			// // no data in case of .all files
			// for (int swath = 0; swath < dimSwath.getLength(); swath++) {
			// for (int detection = 0; detection < dimBeam.getLength(); detection++) {
			// float value = values[(int) (swath * dimBeam.getLength() + detection)];
			// Assert.assertTrue(Float.isNaN(value));
			// }
			// }
		}
		// ping variable, check that it is increasing
		{
			long[] start = { 0 };
			long[] count = { dimSwath.getLength() };
			int[] values = beamVendor.getVariable(BeamGroup1VendorSpecificGrp.PING_RAW_COUNT).get_int(start, count);
			int last = 0;
			for (int swath = 0; swath < dimSwath.getLength(); swath++) {
				Assert.assertTrue(last <= values[swath]);
				last = values[swath];
			}
		}

		// this value is supposed to be equals to the vehicule depth
		{
			long[] start = { 0 };
			long[] count = { dimSwath.getLength() };
			double[] valuesVertical = beamGrp.getVariable(BeamGroup1Grp.PLATFORM_VERTICAL_OFFSET).get_double(start,
					count);
			Assert.assertEquals(0.87, valuesVertical[42], 10e-2);
		}

		// beam_sector
		{
			long[] start = { 0,0 };
			long[] count = { dimSwath.getLength(), dimBeam.getLength() };
			int[] values = beamGrp.getVariable(BeamGroup1Grp.TRANSMIT_BEAM_INDEX).get_int(start, count);
			for (int i = 0; i < dimBeam.getLength()*dimSwath.getLength(); i++) {
				Assert.assertEquals(0, values[i]);
			}
		}
		// tx_tilt_angle
		{
			long[] start = { 0, 0 };
			long[] count = { dimSwath.getLength(), dimSector.getLength() };
			float[] tilt = beamGrp.getVariable(BeamGroup1Grp.TX_BEAM_ROTATION_THETA).get_float(start, count);

			float[] expectedFirstSwath = { -0.031154f, -0.039881f, -0.048607f };
			for (int i = 0; i < expectedFirstSwath.length; i++) {
				double angle = tilt[i];

				Assert.assertEquals(Math.toDegrees(-1*expectedFirstSwath[i]), angle, 10e-3f); //Angle sign convention is reversed
			}
		}
		// tx_transducer_zdepth
		{
			long[] start = { 0 };
			long[] count = { dimSwath.getLength() };
			float[] values = beamGrp.getVariable(BeamGroup1Grp.TX_TRANSDUCER_DEPTH).get_float(start, count);
			// this variable will be check by a comparaison test with mbg
			Assert.assertFalse(Float.isNaN(values[0]));
		}

		// WC DATA TO ADD

		// sample_interval
		{
			long[] start = { 0 };
			long[] count = { dimSwath.getLength() };
			float[] values = beamGrp.getVariable(BeamGroup1Grp.SAMPLE_INTERVAL).get_float(start, count);
			Assert.assertEquals(1 / 197.451874, values[0], 10e-3);
		}
		// sample_time_offset
		{
			long[] start = { 0, 0 };
			long[] count = { dimSwath.getLength(), 1 };
			float[] values = beamGrp.getVariable(BeamGroup1Grp.SAMPLE_TIME_OFFSET).get_float(start, count);
			float[] expected = { 0 };

			for (int i = 0; i < dimSwath.getLength(); i++) {
				//
				for (int sector = 0; sector < dimSector.getLength(); sector++) {
					Assert.assertEquals(expected[sector], values[(int) (i * dimSector.getLength() + sector)], 10e-3);
				}
			}

		}

		// beam_detection_range
		// NOT FILLED (OPTIONAL)

	}

	private void testPlatformAndPosition(NCFile src) throws NCException {
		NCGroup grp = src.getGroup(PlatformLayers.GROUP);
		String names[] = grp.getAttributeNames();
		for (String name : names) {
			String att = grp.getAttributeAsString(name);
			System.out.println(name + ":" + att);
		}
	}
}
