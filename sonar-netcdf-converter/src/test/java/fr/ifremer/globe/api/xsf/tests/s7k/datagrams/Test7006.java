package fr.ifremer.globe.api.xsf.tests.s7k.datagrams;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramParser;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K7006;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K7006.OptionalBeamData;
import fr.ifremer.globe.api.xsf.util.types.UByte;

public class Test7006 extends TestDatagram {

	@Test
	public void testDecode() throws IOException {
		DatagramBuffer buffer = getDataBuffer(7006);
		
		assertEquals(7006, DatagramParser.getDatagramType(buffer));
		assertEquals(5205, S7K7006.getOptionalDataOffset(buffer).getU());

		assertEquals(0, S7K7006.getSonarSerialNumber(buffer).getLong());
		assertEquals(110267, S7K7006.getPingNumber(buffer).getU());
		assertEquals(0, S7K7006.getMultiPingSequence(buffer).getU());
		assertEquals(301, S7K7006.getNumberOfReceivedBeams(buffer).getU());
		assertEquals(0, S7K7006.getLayerCompensationFlag(buffer).getU());
		assertEquals(1494.76, S7K7006.getSoundVelocity(buffer).get(), 0.01);
		
		float[] ranges = S7K7006.getRange(buffer);
		assertEquals(0.35, ranges[0], 0.01);
		assertEquals(0.14, ranges[100], 0.01);
		assertEquals(0.34, ranges[300], 0.01);
		
		UByte[] qualities = S7K7006.getQuality(buffer);
		assertEquals(10, qualities[0].getU());
		assertEquals(11, qualities[100].getU());
		assertEquals(10, qualities[300].getU());
		
		float[] intensities = S7K7006.getIntensity(buffer);
		assertEquals(2.74, intensities[0], 0.01);
		assertEquals(2.84, intensities[100], 0.01);
		assertEquals(2.63, intensities[300], 0.01);
		
		float[] minFilterInfo = S7K7006.getMinFilterInfo(buffer);
		assertEquals(0.29, minFilterInfo[0], 0.01);
		assertEquals(0.12, minFilterInfo[100], 0.01);
		assertEquals(0.26, minFilterInfo[300], 0.01);
		
		float[] maxFilterInfo = S7K7006.getMaxFilterInfo(buffer);
		assertEquals(0.38, maxFilterInfo[0], 0.01);
		assertEquals(0.15, maxFilterInfo[100], 0.01);
		assertEquals(0.35, maxFilterInfo[300], 0.01);
		
		// testOptional data section
		final int optionalDataOffset = (int) S7K7006.getOptionalDataOffset(buffer).getU();
		
		assertEquals(100000.0, S7K7006.getFrequency(buffer, optionalDataOffset).get(), 0.0);
		assertEquals(0.84, S7K7006.getLatitude(buffer, optionalDataOffset).get(), 0.01);
		assertEquals(-0.08, S7K7006.getLongitude(buffer, optionalDataOffset).get(), 0.01);
		assertEquals(0.03, S7K7006.getHeading(buffer, optionalDataOffset).get(), 0.01);
		assertEquals(0, S7K7006.getHeightSource(buffer, optionalDataOffset).getU());
		assertEquals(0.0, S7K7006.getTide(buffer, optionalDataOffset).get(), 0.0);
		assertEquals(0.006, S7K7006.getRoll(buffer, optionalDataOffset).get(), 0.001);
		assertEquals(0.0, S7K7006.getPitch(buffer, optionalDataOffset).get(), 0.001);
		assertEquals(-0.47, S7K7006.getHeave(buffer, optionalDataOffset).get(), 0.01);
		assertEquals(-0.41, S7K7006.getVehicleDepth(buffer, optionalDataOffset).get(), 0.01);
		
		final OptionalBeamData[] optionalData = S7K7006.getOptionalBeamData(buffer, optionalDataOffset);
		assertEquals(301, optionalData.length);
		assertEquals(68.27, optionalData[0].depth, 0.01);
		assertEquals(49.04, optionalData[0].alongTrackDistance, 0.01);
		assertEquals(-255.10, optionalData[0].acrossTrackDistance, 0.01);
		assertEquals(1.33, optionalData[0].pointingAngle, 0.01);
		assertEquals(4.71, optionalData[0].azimuthAngle, 0.01);
		
		assertEquals(67.89, optionalData[300].depth, 0.01);
		assertEquals(47.25, optionalData[300].alongTrackDistance, 0.01);
		assertEquals(247.83, optionalData[300].acrossTrackDistance, 0.01);
		//ANGLES ARE IN RADIAN, they should be converted to degrees to compare with 
		assertEquals(1.32, optionalData[300].pointingAngle, 0.01);
		assertEquals(1.56, optionalData[300].azimuthAngle, 0.01);
	}
}
