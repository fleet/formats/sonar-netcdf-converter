package fr.ifremer.globe.api.xsf.tests.all.datagrams;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.rawrange70.RawRange70;

public class TestRawRangeBeamAngle70 extends DatagramTest {

	@Test
	public void testGetBody() throws IOException {
		DatagramBuffer buffer = getDatagram((byte)0x46);
		
		assertEquals(RawRange70.getSerialNumber(buffer.byteBufferData).getU(), 213);
		assertEquals(RawRange70.getMaxBeamsNumber(buffer.byteBufferData).getU(), 111);
		int nBeams = RawRange70.getValidBeamsCounter(buffer.byteBufferData).getU();
		assertEquals(nBeams, 111);
		assertEquals(RawRange70.getSoundSpeed(buffer.byteBufferData).getU(), 14334);
		
		// Find in a for EM3000
		int iBeam = 0;
		int offsetBuf = iBeam;
		assertEquals(RawRange70.getBeamPointingAngle(buffer.byteBufferData, offsetBuf).get(), 7186);
		assertEquals(RawRange70.getTxTilt(buffer.byteBufferData, offsetBuf).get(), 0);
		assertEquals(RawRange70.getRange(buffer.byteBufferData, offsetBuf).getU(), 6954);
		assertEquals(RawRange70.getReflectivity(buffer.byteBufferData, offsetBuf).get(), -52);
		assertEquals(RawRange70.getBeamNumber(buffer.byteBufferData, offsetBuf).getU(), 0);

		// last beam
		offsetBuf = (nBeams-1);
		assertEquals(RawRange70.getBeamPointingAngle(buffer.byteBufferData, offsetBuf).get(), -7704);
		assertEquals(RawRange70.getTxTilt(buffer.byteBufferData, offsetBuf).get(), 0);
		assertEquals(RawRange70.getRange(buffer.byteBufferData, offsetBuf).getU(), 5216);
		assertEquals(RawRange70.getReflectivity(buffer.byteBufferData, offsetBuf).get(), -62);
		assertEquals(RawRange70.getBeamNumber(buffer.byteBufferData, offsetBuf).getU(), 110);

	}
}
