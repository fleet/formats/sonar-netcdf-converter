package fr.ifremer.globe.api.xsf.tests.kmall.datagrams;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


@RunWith(Suite.class)
@SuiteClasses({TestIIPDatagrams.class,
	TestIOPDatagram.class,
	TestMRZDatagram.class,
	TestMWCDatagram.class,
	TestSCLDatagrams.class,
	TestSKMDatagrams.class,
	TestSPODatagrams.class})
public class KmallTestSuite {

}
