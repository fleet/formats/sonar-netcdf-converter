package fr.ifremer.globe.api.xsf.tests.xsfvalidation;

import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfBase;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.BathymetryGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.BeamGroup1Grp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.PlatformGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.SonarGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.BeamGroup1VendorSpecificGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.types.DetectionTypeHelper;
import fr.ifremer.globe.api.xsf.converter.common.xsf.types.transducer_type_t;
import fr.ifremer.globe.api.xsf.converter.kmall.KongsbergKmAllConverter;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.SonarDetectionStatus;
import fr.ifremer.globe.netcdf.ucar.*;
import fr.ifremer.globe.netcdf.ucar.NCFile.Mode;
import org.junit.*;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

/**
 * Perform XSF checking on a given file and verify if expecting values are encountered
 */
public class XSFKongsbergFormatValidation {
    // String sourceDirectory=GlobeTestUtil.getTestDataPath();

    public final static File in = new File(TestConstants.sourceDirectory + File.separator + "file" + File.separator + "xsf" + File.separator
            + "XSFValidation" + File.separator + "0039_20180905_222154.all");
    public final static File xsf = new File(TestConstants.sourceDirectory + File.separator + "file" + File.separator + "xsf"
            + File.separator + "XSFValidation" + File.separator + "out.all" + XsfBase.EXTENSION_XSF);
    public final static File inKmall = new File(TestConstants.sourceDirectory + File.separator + "file" + File.separator + "xsf"
            + File.separator + "XSFValidation" + File.separator + "0039_20180905_222154_raw.kmall");
    public final static File xsfKmall = new File(TestConstants.sourceDirectory + File.separator + "file" + File.separator + "xsf"
            + File.separator + "XSFValidation" + File.separator + "out.kmall" + XsfBase.EXTENSION_XSF);

    @Rule
    public TemporaryFolder tmp = new TemporaryFolder();
    private NCFile readerAll;
    private NCFile readerKmall;

    @Before
    public void CreateFiles() throws NCException, IOException {

        boolean regenerate = true;
        if (regenerate || !xsf.exists() || !xsfKmall.exists()) {
            xsf.delete();
            xsfKmall.delete();

            //new KongsbergAllConverter().convert(in.getAbsolutePath(), xsf.getAbsolutePath());
            new KongsbergKmAllConverter().convert(inKmall.getAbsolutePath(), xsfKmall.getAbsolutePath());
        }
    }

    @Test
    public void testKmall() throws NCException {
        readerKmall = NCFile.open(xsfKmall.getAbsolutePath(), Mode.readonly);
        runTestSequence(readerKmall, new KmallSpecificCheck());
    }

    private void runTestSequence(NCFile src, SpecificChecks checker) throws NCException {
        testBeamGroup(src, checker);
        testPlatformAndPosition(src);
        testBeamGroupWC(src);
    }

    @Test
    public void testAll() throws NCException {
//		readerAll = NCFile.open(xsf.getAbsolutePath(), Mode.readonly);
//		runTestSequence(readerAll, new AllSpecificCheck());
    }

    @After
    public void destroy() {
        // perform cleanup
        if (readerAll != null)
            readerAll.close();
        if (readerKmall != null)
            readerKmall.close();
//		xsf.delete();
//		xsfKmall.delete();
    }

    /**
     * Test function for wc data
     *
     * @param src
     */
    private void testBeamGroupWC(NCFile src) throws NCException {
        // TODO
        // equivalent_beam_angle

    }

    /**
     * Test for beam detection variables
     */
    private void testBeamGroup(NCFile src, SpecificChecks checker) throws NCException {
        // detection count
        NCGroup grpBeam = src.getGroup(SonarGrp.GROUP_NAME).getGroup(XsfBase.DEFAULT_BEAM_GROUP_NAME);
        NCGroup grpBeamVendor = grpBeam.getGroup(BeamGroup1VendorSpecificGrp.GROUP_NAME);
        NCGroup grpDetection = grpBeam.getGroup(BathymetryGrp.GROUP_NAME);

        // check group is found (might have null pointer exception if one of them is not found
        Assert.assertNotNull(grpBeam);
        NCDimension dimDetection = grpDetection.getDimension(BathymetryGrp.DETECTION_DIM_NAME);
        Assert.assertEquals(800, dimDetection.getLength());

        NCDimension dimBeam = grpBeam.getDimension(BeamGroup1Grp.BEAM_DIM_NAME);
        Assert.assertEquals(512, dimBeam.getLength());

        NCDimension dimSwath = grpBeam.getDimension(BeamGroup1Grp.PING_TIME_DIM_NAME);
        NCDimension dimSector = grpBeam.getDimension(BeamGroup1Grp.TX_BEAM_DIM_NAME);

        Assert.assertEquals(84, dimSwath.getLength());

        // call specific tests
        checker.testBeamGroup(src, new GrpContext(grpBeam, grpDetection, dimDetection, dimBeam, dimSwath, dimSector));

        // check beam_direction

        /*
         * check beam witdh, verify that they are around the expected values (0.6 deg for Tx and 1.0 deg for Rx) Real
         * values depends on beam pointing angle
         */
        {
            long[] start = {0, 0};
            {

                long[] count = {dimSwath.getLength(), dimBeam.getLength()};
                float[] rxmin = null;
                if (grpBeam.getVariable(BeamGroup1Grp.BEAMWIDTH_RECEIVE_MINOR) != null)
                    rxmin = grpBeam.getVariable(BeamGroup1Grp.BEAMWIDTH_RECEIVE_MINOR).get_float(start, count);
                float[] rxmaj = grpBeam.getVariable(BeamGroup1Grp.BEAMWIDTH_RECEIVE_MAJOR).get_float(start, count);

                int swath = 2;
                {
                    for (int beam = 0; beam < dimBeam.getLength(); beam++) {
                        // no info about along beam width for rx
                        if (rxmin != null)
                            Assert.assertTrue(Float.isNaN(rxmin[(int) (swath * dimBeam.getLength() + beam)]));
                        // max pointing angle for this file is 60 deg
                        Assert.assertEquals("Expecting beam width error (swath,beam)[" + swath + "," + beam + "]", 1.1f,
                                rxmaj[(int) (swath * dimBeam.getLength() + beam)], 1.1f / Math.cos(Math.toRadians(60)));


                    }
                }
            }
            {
                long[] count = {dimSwath.getLength(), dimSector.getLength()};
                float[] txmin = grpBeam.getVariable(BeamGroup1Grp.BEAMWIDTH_TRANSMIT_MINOR).get_float(start, count);
                float[] txmaj = null;
                if (grpBeam.getVariable(BeamGroup1Grp.BEAMWIDTH_TRANSMIT_MAJOR) != null)
                    txmaj = grpBeam.getVariable(BeamGroup1Grp.BEAMWIDTH_TRANSMIT_MAJOR).get_float(start, count);
                int swath = 2;
                {
                    for (int beam = 0; beam < dimSector.getLength(); beam++) {
                        // no info about along beam width for rx
                        Assert.assertEquals("Expecting beam width error (swath,beam)[" + swath + "," + beam + "]", 0.6f,
                                txmin[(int) (swath * dimSector.getLength() + beam)], 0.1f);
                        // no info about accross beam width for tx
                        if (txmaj != null)
                            Assert.assertTrue(Float.isNaN(txmaj[(int) (swath * dimSector.getLength() + beam)]));
                    }
                }
            }
        }

        // detection angle
        {
            long[] start = {0, 0};
            long[] count = {dimSwath.getLength(), dimDetection.getLength()};
            int swath = 2;
            {
                float[] angle = grpDetection.getVariable(BathymetryGrp.DETECTION_BEAM_POINTING_ANGLE).get_float(start, count);
                for (int detection = 0; detection < dimDetection.getLength(); detection++) {
                    float angleRead = angle[(int) (swath * dimDetection.getLength() + detection)];
                    Assert.assertTrue(-60 < angleRead && angleRead < 60);
                }
                float angleRead = angle[(int) (swath * dimDetection.getLength())];
                Assert.assertEquals(52.75f, angleRead, 10 - 3);
            }
        }

        // lat/long
        {
            long[] start = {0, 0};
            long[] count = {dimSwath.getLength(), dimDetection.getLength()};
            double[] lat = grpDetection.getVariable(BathymetryGrp.DETECTION_LATITUDE).get_double(start, count);
            double[] lon = grpDetection.getVariable(BathymetryGrp.DETECTION_LONGITUDE).get_double(start, count);
            // no data in case of .all files
            for (int swath = 0; swath < dimSwath.getLength(); swath++) {
                for (int detection = 0; detection < dimBeam.getLength(); detection++) {
                    double valueLat = lat[(int) (swath * dimBeam.getLength() + detection)];
                    double valueLong = lon[(int) (swath * dimBeam.getLength() + detection)];

                    // check that value is roughly equals to 47.5 deg
                    Assert.assertEquals(47.5, valueLat, 0.2);

                    // check that value is roughly equals to 47.5 deg
                    Assert.assertEquals(-8.5, valueLong, 0.1);

                }
            }
        }


        // detection_reflectivity
        {
            long[] start = {0, 0};
            long[] count = {dimSwath.getLength(), dimDetection.getLength()};
            float[] bs_c = grpDetection.getVariable(BathymetryGrp.DETECTION_BACKSCATTER_R).get_float(start, count);
            // no data in case of .all files
            for (int swath = 0; swath < dimSwath.getLength(); swath++) {
                for (int detection = 0; detection < dimBeam.getLength(); detection++) {
                    float value = bs_c[(int) (swath * dimBeam.getLength() + detection)];
                    if (!Float.isNaN(value)) {

                        Assert.assertTrue(value < 2 && value > -120);
                    }
                }
            }
        }
        // detection_rx_transducer_index
        {
            long[] start = {0, 0};
            long[] count = {dimSwath.getLength(), dimDetection.getLength()};
            short[] values = grpDetection.getVariable(BathymetryGrp.DETECTION_RX_TRANSDUCER_INDEX).get_ushort(start, count);
            for (int swath = 0; swath < dimSwath.getLength(); swath++) {
                for (int detection = 0; detection < dimBeam.getLength(); detection++) {
                    short value = values[(int) (swath * dimBeam.getLength() + detection)];
                    int index = Short.toUnsignedInt(value);
                    Assert.assertEquals(0, index);
                }
            }
        }

        // detection_two_way_travel_time, check first value extracted from datagram viewer
        {
            long[] start = {0, 0};
            long[] count = {1, 1};
            float[] values = grpDetection.getVariable(BathymetryGrp.DETECTION_TWO_WAY_TRAVEL_TIME).get_float(start, count);
            float value = values[0];
            Assert.assertEquals(4.78306, value, 10e-3);
        }

        // detection_tx_sector, 8 sectors in this file
        {
            long[] start = {0, 0};
            long[] count = {dimSwath.getLength(), dimDetection.getLength()};
            short[] values = grpDetection.getVariable(BathymetryGrp.DETECTION_TX_BEAM).get_short(start, count);
            // no data in case of .all files
            for (int swath = 0; swath < dimSwath.getLength(); swath++) {
                for (int detection = 0; detection < dimBeam.getLength(); detection++) {
                    short value = values[(int) (swath * dimBeam.getLength() + detection)];
                    Assert.assertTrue(value >= 0 && value <= 7);
                }
            }
        }
        // detection_tx_transducer_index, on single tx
        {
            long[] start = {0, 0};
            long[] count = {dimSwath.getLength(), dimDetection.getLength()};
            short[] values = grpDetection.getVariable(BathymetryGrp.DETECTION_TX_TRANSDUCER_INDEX).get_ushort(start, count);
            // no data in case of .all files
            for (int swath = 0; swath < dimSwath.getLength(); swath++) {
                for (int detection = 0; detection < dimBeam.getLength(); detection++) {
                    short value = values[(int) (swath * dimBeam.getLength() + detection)];
                    int index = Short.toUnsignedInt(value);
                    Assert.assertEquals(1, index);
                }
            }
        }

        // detection x y z
        {
            NCVariable xVar = grpDetection.getVariable(BathymetryGrp.DETECTION_X);
            NCVariable yVar = grpDetection.getVariable(BathymetryGrp.DETECTION_Y);
            NCVariable zVar = grpDetection.getVariable(BathymetryGrp.DETECTION_Z);
            long[] start = {0, 0};
            long[] count = {dimSwath.getLength(), dimDetection.getLength()};
            float[] x = xVar.get_float(start, count);
            float[] y = yVar.get_float(start, count);
            float[] z = zVar.get_float(start, count);

            for (int swath = 0; swath < dimSwath.getLength(); swath++) {
                for (int detection = 0; detection < dimBeam.getLength(); detection++) {
                    // check global values for detection
                    Assert.assertEquals(25., x[(int) (swath * dimBeam.getLength() + detection)], 20.0); // between 5 and
                    // 45
                    Assert.assertEquals(0., y[(int) (swath * dimBeam.getLength() + detection)], 3050.0); // between
                    // -3050 and
                    // 3050
                    Assert.assertEquals(2000., z[(int) (swath * dimBeam.getLength() + detection)], 200.0); // around
                    // -2000m
                }
            }
        }
        // detection_type
        {
            long[] start = {0, 0};
            long[] count = {dimSwath.getLength(), dimDetection.getLength()};
            byte[] values = grpDetection.getVariable(BathymetryGrp.DETECTION_TYPE).get_byte(start, count);

            // [0,0] is invalid (from datagram viewer
            int swath = 0;
            int detection = 0;
            byte value = values[(int) (swath * dimDetection.getLength() + detection)];
            Assert.assertEquals(DetectionTypeHelper.INVALID.get(), value);

            // [0,16] is phase
            swath = 0;
            detection = 16;
            value = values[(int) (swath * dimDetection.getLength() + detection)];
            Assert.assertEquals(DetectionTypeHelper.PHASE.get(), value);

            // [0,425] is amplitude
            swath = 0;
            detection = 425;
            value = values[(int) (swath * dimDetection.getLength() + detection)];
            Assert.assertEquals(DetectionTypeHelper.AMPLITUDE.get(), value);
            // [1,401] is amplitude
            swath = 1;
            detection = 401;
            value = values[(int) (swath * dimDetection.getLength() + detection)];
            Assert.assertEquals(DetectionTypeHelper.AMPLITUDE.get(), value);
        }

        // platform_heading
        {
            long[] start = {0};
            long[] count = {dimSwath.getLength()};
            float[] values = grpBeam.getVariable(BeamGroup1Grp.PLATFORM_HEADING).get_float(start, count);
            float[] expected = {33774 * (10e-3f), 33250 * 10e-3f, 32927 * 10e-3f, 32875 * 10e-3f, 32997 * 10e-3f,
                    33036 * 10e-3f, 33064 * 10e-3f, 33012 * 10e-3f, 33047 * 10e-3f, 33083 * 10e-3f, 33172 * 10e-3f,
                    33160 * 10e-3f, 33267 * 10e-3f, 33209 * 10e-3f, 33144 * 10e-3f, 33128 * 10e-3f, 33187 * 10e-3f,
                    33476 * 10e-3f, 34001 * 10e-3f, 34589 * 10e-3f, 35229 * 10e-3f, 35775 * 10e-3f, 237 * 10e-3f,
                    927 * 10e-3f};
            for (int swath = 0; swath < expected.length; swath++) {
                Assert.assertEquals(String.format("Error comparing heading for swath [%d]", swath), expected[swath],
                        values[swath], 10e-3);
            }
        }

        // platform_latitude
        // platform_longitude
        {
            long[] start = {0};
            long[] count = {dimSwath.getLength()};
            double[] valuesLong = grpBeam.getVariable(BeamGroup1Grp.PLATFORM_LONGITUDE).get_double(start, count);
            double[] valuesLat = grpBeam.getVariable(BeamGroup1Grp.PLATFORM_LATITUDE).get_double(start, count);
            double[] valuesPitch = grpBeam.getVariable(BeamGroup1Grp.PLATFORM_PITCH).get_double(start, count);
            double[] valuesRoll = grpBeam.getVariable(BeamGroup1Grp.PLATFORM_ROLL).get_double(start, count);
            double[] valuesVertical = grpBeam.getVariable(BeamGroup1Grp.PLATFORM_VERTICAL_OFFSET).get_double(start, count);

            // interpolated, just test if around specific value
            for (int swath = 0; swath < dimSwath.getLength(); swath++) {
                Assert.assertEquals(47.5, valuesLat[swath], 10e-3);
                Assert.assertEquals(-8.53, valuesLong[swath], 10e-3);
            }

            // according to Sonar record viewer, pitch is roughly 0 at the time of the second ping and 0.15 at the time
            // for the roll
            Assert.assertEquals(0.15, valuesRoll[1], 10e-3);
            Assert.assertEquals(0, valuesPitch[1], 10e-3);

            // pitch is 0 for swath 1, so no need for level arm compensation value= transducer location-txZdepth
            // Assert.assertEquals(6.19165 - 6.989, valuesVertical[1], 10e-3);
            Assert.assertEquals(0.797405, valuesVertical[1], 10e-3);

        }

        // TODO
        // sound_speed_at_transducer
        {
            long[] start = {0};
            long[] count = {dimSwath.getLength()};
            float[] values = grpBeam.getVariable(BeamGroup1Grp.SOUND_SPEED_AT_TRANSDUCER).get_float(start, count);
            Assert.assertEquals(1517.f, values[0], 10e-3);
            Assert.assertEquals(1517.1f, values[30], 10e-3);
            Assert.assertEquals(1517.2f, values[80], 10e-3);

        }
        // status
        // status_detail
        {
            long[] start = {0, 0};
            long[] count = {dimSwath.getLength(), dimDetection.getLength()};
            byte[] values = grpDetection.getVariable(BathymetryGrp.STATUS).get_byte(start, count);
            // status_detail not checked
            @SuppressWarnings("unused")
            byte[] status_detail = grpDetection.getVariable(BathymetryGrp.STATUS_DETAIL).get_byte(start, count);
            // values extracted from datagram viewer
            Assert.assertFalse(SonarDetectionStatus.isValid(values[0]));
            Assert.assertTrue(
                    SonarDetectionStatus.getStatus(values[0]).contains(SonarDetectionStatus.INVALID_ACQUISITION));
            Assert.assertTrue(SonarDetectionStatus.isValid(values[22]));

        }
        // swath_dynamic_draught
        // swath_tide
        {
            long[] start = {0};
            long[] count = {dimSwath.getLength()};
            float[] valuesTide = grpBeam.getVariable(BeamGroup1Grp.WATERLINE_TO_CHART_DATUM).get_float(start, count);
            for (int i = 0; i < dimSwath.getLength(); i++) {
                Assert.assertEquals(0, valuesTide[i], 10e-6);
            }
        }
        // swath_time
        {
            long[] start = {0};
            long[] count = {dimSwath.getLength()};
            long[] values = grpBeam.getVariable(BeamGroup1Grp.PING_TIME).get_ulong(start, count);

            // check first value
            int i = 0;
            long timeMilli = Long.divideUnsigned(values[i], 1_000_000);

            LocalDateTime t = LocalDateTime.ofInstant(Instant.ofEpochMilli(timeMilli), ZoneId.of("UTC"));
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss.SSS");
            Assert.assertEquals("05/09/2018 22:21:50.235", t.format(formatter));
            // we do not check nano second, since they are missing from .all file format
            long first = 0;
            // check that values are increasing
            for (int swath = 0; swath < dimSwath.getLength(); swath++) {
                Assert.assertTrue(Long.compareUnsigned(values[swath], first) >= 0);
                first = values[swath];
            }
        }
        // tx_center_frequency
        {
            long[] start = {0, 0};
            long[] count = {dimSwath.getLength(), dimSector.getLength()};
            float[] values = grpBeam.getVariable(BeamGroup1Grp.TRANSMIT_FREQUENCY_START).get_float(start, count);
            float[] expected = {26500f, 27500f, 28500f, 29500f, 30000f, 29000f, 28000f, 27000f};
            for (int i = 0; i < dimSector.getLength(); i++) {
                Assert.assertEquals(expected[i], values[i], 10e-3f);
            }
        }
        // tx_tilt_angle
        {
            long[] start = {0, 0};
            long[] count = {dimSwath.getLength(), dimSector.getLength()};
            long[] start_1 = {0};
            long[] count_1 = {dimSwath.getLength()};
            float[] valuesTilt = grpBeam.getVariable(BeamGroup1Grp.TX_BEAM_ROTATION_THETA).get_float(start, count);
            float[] values_pitch = grpBeam.getVariable(BeamGroup1Grp.PLATFORM_PITCH).get_float(start_1, count_1);


            float[] expectedFirstSwath = {-0.49f, -0.56f, -0.70f, -0.80f, -0.80f, -0.70f, -0.56f, -0.51f};
            float[] expectedLastSwath = {0.16f, 0.17f, 0.22f, 0.25f, 0.25f, 0.22f, 0.17f, 0.15f};
            //use only third sector, which is really a ping time (tx_sector delay = 0)
            for (int i = 3; i < 4/*dimSector.getLength()*/; i++) {

                //PITCH is Added with the pitch value at time of the tx emission to switch to an absolute referential
                double angle = valuesTilt[i];
                double pitch = values_pitch[(int) (dimSwath.getLength() - 1)];
                //cannot use first ping since MRU values are missing
//				Assert.assertEquals(expectedFirstSwath[i], angle-pitch, 10e-3f); 
                int index = (int) ((dimSwath.getLength() - 1) * dimSector.getLength() + i);
                angle = valuesTilt[index];

                //Pitch is approximated : does not take into account for tx sector delay
                //We check that angle ~= expectedLastSwath[i]+pitch+0.24 where 0.24 is the installation parameter
                Assert.assertEquals(expectedLastSwath[i] + pitch + 0.24, angle, 10e-2f);
            }
        }
        // tx_transducer_zdepth
        {
            long[] start = {0};
            long[] count = {dimSwath.getLength()};
            float[] values = grpBeam.getVariable(BeamGroup1Grp.TX_TRANSDUCER_DEPTH).get_float(start, count);
            Assert.assertEquals(5.68341f, values[0], 10e-3);
            Assert.assertEquals(6.19165f, values[1], 10e-3);
            Assert.assertEquals(6.36239f, values[(int) (dimSwath.getLength() - 1)], 10e-3);
        }

        // sample_interval
        {
            long[] start = {0};
            long[] count = {dimSwath.getLength()};
            float[] values = grpBeam.getVariable(BeamGroup1Grp.SAMPLE_INTERVAL).get_float(start, count);
            Assert.assertEquals(1 / 416.83, values[0], 10e-3);
        }
//		// sample_time_offset
//		{
//			long[] start = { 0, 0 };
//			long[] count = { dimSwath.getLength(), dimSector.getLength() };
//			float[] values = grpBeam.getVariable(BeamGroup1Grp.TRANSMIT_TIME_DELAY).get_float(start, count);
//			float[] expected = { 0.069703f, 0.0464687f, 0.0232344f, 0f, 0.0116172f, 0.0348515f, 0.0580859f,
//					0.0813202f };
//
//			for (int i = 0; i < dimSwath.getLength(); i++) {
//				//
//				for (int sector = 0; sector < dimSector.getLength(); sector++) {
//					Assert.assertEquals(expected[sector], values[(int) (i * dimSector.getLength() + sector)], 10e-3);
//				}
//			}
//
//		}


        // beam_detection_range
        {
            long[] start = {0, 0};
            long[] count = {dimSwath.getLength(), dimBeam.getLength()};
            int[] values = grpBeamVendor.getVariable(BeamGroup1VendorSpecificGrp.BEAM_DETECTION_RANGE).get_int(start, count);

            // check some values extracted from datagram viewer
            Assert.assertEquals(1897, values[(int) ((dimSwath.getLength() - 1) * dimBeam.getLength()) + 3]);
            Assert.assertEquals(0, values[(int) ((dimSwath.getLength() - 1) * dimBeam.getLength())]); // invalid value
            int[] expected = {0, 0, 0, 0, 0, 1949, 1937, 1927, 1920, 1914, 1902, 1893, 1884, 1876, 1869, 1860, 1852,
                    1845, 1837, 1828, 1820, 1812, 1806, 1798, 1792, 1784, 1777, 1772, 1763, 1756, 1749, 1743, 1737,
                    1729, 1722, 1715, 1709, 1703, 1699, 1693, 1686, 1679, 1674, 1667, 1662, 1656, 1652, 1645, 1639,
                    1632, 1627, 1623, 1619, 1615, 1608, 1603, 1598, 1592, 1588, 1582, 1577, 1572, 1567, 1563, 1559,
                    1554, 1549, 1545, 1541, 1537, 1531, 1526, 1523, 1519, 1514, 1509, 1505, 1501, 1497, 1492, 1488,
                    1484, 1481, 1477, 1473, 1469, 1466, 1463, 1457, 1452, 1448, 1446, 1443, 1441, 1436, 1433, 1430,
                    1425, 1423, 1420, 1417, 1412, 1409, 1406, 1403, 1400, 1394, 1393, 1391, 1387, 1385, 1382, 1378,
                    1374, 1372, 1370, 1367, 1362, 1360, 1357, 1355, 1355, 1349, 1347, 1346, 1342, 1339, 1336, 1334,
                    1332, 1329, 1326, 1324, 1322, 1321, 1317, 1314, 1313, 1310, 1308, 1306, 1303, 1301, 1299, 1297,
                    1294, 1293, 1290, 1287, 1286, 1284, 1282, 1280, 1278, 1277, 1275, 1273, 1272, 1270, 1268, 1266,
                    1264, 1263, 1260, 1260, 1258, 1258, 1255, 1253, 1251, 1249, 1248, 1246, 1245, 1244, 1242, 1241,
                    1239, 1238, 1237, 1235, 1234, 1232, 1231, 1230, 1229, 1228, 1225, 1225, 1224, 1222, 1222, 1221,
                    1220, 1219, 1218, 1216, 1215, 1214, 1213, 1212, 1211, 1210, 1209, 1209, 1208, 1207, 1206, 1205,
                    1205, 1204, 1203, 1202, 1201, 1200, 1200, 1199, 1199, 1198, 1198, 1194, 1195, 1195, 1194, 1195,
                    1193, 1194, 1192, 1192, 1191, 1190, 1190, 1190, 1189, 1189, 1188, 1188, 1188, 1187, 1187, 1186,
                    1186, 1185, 1185, 1184, 1184, 1183, 1183, 1182, 1184, 1184, 1184, 1183, 1184, 1184, 1184, 1182,
                    1182, 1182, 1182, 1182, 1182, 1183, 1182, 1182, 1183, 1180, 1181, 1183, 1184, 1184, 1184, 1185,
                    1185, 1184, 1184, 1183, 1184, 1185, 1184, 1184, 1184, 1184, 1185, 1185, 1185, 1186, 1186, 1186,
                    1186, 1187, 1189, 1188, 1189, 1190, 1190, 1189, 1191, 1192, 1192, 1192, 1193, 1193, 1194, 1195,
                    1195, 1196, 1197, 1197, 1198, 1199, 1201, 1202, 1203, 1203, 1203, 1204, 1204, 1205, 1206, 1206,
                    1208, 1209, 1210, 1211, 1211, 1212, 1213, 1214, 1216, 1217, 1218, 1219, 1220, 1222, 1222, 1223,
                    1224, 1225, 1227, 1228, 1229, 1231, 1232, 1233, 1234, 1236, 1237, 1239, 1239, 1242, 1243, 1244,
                    1246, 1246, 1248, 1250, 1251, 1253, 1255, 1256, 1258, 1260, 1262, 1263, 1264, 1266, 1268, 1270,
                    1273, 1273, 1273, 1277, 1279, 1280, 1283, 1284, 1287, 1289, 1291, 1293, 1294, 1295, 1299, 1301,
                    1303, 1306, 1307, 1309, 1312, 1314, 1317, 1319, 1322, 1324, 1326, 1329, 1331, 1334, 1337, 1338,
                    1340, 1344, 1346, 1350, 1351, 1353, 1357, 1360, 1363, 1366, 1367, 1370, 1373, 1376, 1380, 1382,
                    1385, 1388, 1391, 1395, 1398, 1401, 1404, 1406, 1409, 1413, 1418, 1420, 1424, 1427, 1431, 1434,
                    1437, 1441, 1444, 1448, 1451, 1454, 1458, 1462, 1466, 1469, 1474, 1478, 1482, 1486, 1489, 1493,
                    1498, 1501, 1506, 1510, 1514, 1519, 1523, 1528, 1532, 1536, 1541, 1545, 1550, 1555, 1560, 1565,
                    1569, 1573, 1578, 1583, 1589, 1596, 1600, 1604, 1609, 1615, 1621, 1626, 1631, 1636, 1641, 1649,
                    1654, 1659, 1666, 1671, 1676, 1682, 1690, 1696, 1702, 1709, 1715, 1720, 1728, 1733, 1741, 1746,
                    1753, 1761, 1767, 1776, 1783, 1789, 1796, 1804, 1812, 1818, 1825, 1833, 1842, 1851, 1859};
            for (int i = 0; i < dimBeam.getLength(); i++) {
                Assert.assertEquals(expected[i], values[i]);
            }
        }
        // beam_sector
        {
            long[] start = {0, 0};
            long[] count = {dimSwath.getLength(), dimBeam.getLength()};
            short[] values = grpBeam.getVariable(BeamGroup1Grp.TRANSMIT_BEAM_INDEX).get_short(start, count);
            // check some values extracted from datagram viewer
            short[] expected = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2,
                    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
                    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
                    2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
                    3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
                    3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 4,
                    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
                    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
                    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5,
                    5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5,
                    5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5,
                    5, 5, 5, 5, 5, 5, 5, 5, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
                    6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
                    6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 7, 7, 7, 7, 7, 7, 7};
            for (int i = 0; i < dimBeam.getLength(); i++) {
                Assert.assertEquals(expected[i], values[i]);
            }
        }
        // beam_rx_transducer_index

    }

    private void testPlatformAndPosition(NCFile src) throws NCException {

        NCGroup grp = src.getGroup(PlatformGrp.GROUP_NAME);
        String[] names = grp.getAttributeNames();
        for (String name : names) {
            String att = grp.getAttributeAsString(name);
            System.out.println(name + ":" + att);
        }

        // VALIDATE EXPECTED ANTENNA OFFSET AND POSITION
        NCDimension dim = grp.getDimension(PlatformGrp.TRANSDUCER_DIM_NAME);
        Assert.assertNotNull(dim);
        Assert.assertEquals(dim.getLength(), 2);

        // check antennas
        {

            NCVariable transducer_type = grp.getVariable(PlatformGrp.TRANSDUCER_FUNCTION);
            long[] start = {0};
            long[] count = {dim.getLength()};
            byte[] type = transducer_type.get_byte(start, count);
            // check that we have RxAntenna first
            Assert.assertEquals(type[0], transducer_type_t.Rx_Transducer.getValue());
            Assert.assertEquals(type[1], transducer_type_t.Tx_Transducer.getValue());
        }

        // check transducer position
        {
            NCVariable transducer_x = grp.getVariable(PlatformGrp.TRANSDUCER_OFFSET_X);
            NCVariable transducer_y = grp.getVariable(PlatformGrp.TRANSDUCER_OFFSET_Y);
            NCVariable transducer_z = grp.getVariable(PlatformGrp.TRANSDUCER_OFFSET_Z);
            long[] start = {0};
            long[] count = {dim.getLength()};
            float[] x = transducer_x.get_float(start, count);
            float[] y = transducer_y.get_float(start, count);
            float[] z = transducer_z.get_float(start, count);
            // Rx position extracted from datagram viewer
            Assert.assertEquals(28.812f, x[0], 10e-4);
            Assert.assertEquals(-0.004, y[0], 10e-4);
            Assert.assertEquals(6.975, z[0], 10e-4);
            // Tx position extracted from datagram viewer
            Assert.assertEquals(25.629f, x[1], 10e-4);
            Assert.assertEquals(-0.003, y[1], 10e-4);
            Assert.assertEquals(6.989, z[1], 10e-4);
        }
        // check transducer angle
        {
            NCVariable transducer_x = grp.getVariable(PlatformGrp.TRANSDUCER_ROTATION_X);
            NCVariable transducer_y = grp.getVariable(PlatformGrp.TRANSDUCER_ROTATION_Y);
            NCVariable transducer_z = grp.getVariable(PlatformGrp.TRANSDUCER_ROTATION_Z);
            long[] start = {0};
            long[] count = {dim.getLength()};
            float[] x = transducer_x.get_float(start, count);
            float[] y = transducer_y.get_float(start, count);
            float[] z = transducer_z.get_float(start, count);
            // Rx position extracted from datagram viewer
            Assert.assertEquals(-0.080f, x[0], 10e-4);
            Assert.assertEquals(0.33, y[0], 10e-4);
            Assert.assertEquals(0.09, z[0], 10e-4);
            // Tx position extracted from datagram viewer
            Assert.assertEquals(-0.130f, x[1], 10e-4);
            Assert.assertEquals(0.240f, y[1], 10e-4);
            Assert.assertEquals(0.060f, z[1], 10e-4);
        }
        // check serial number
        {
            // Rx = 160
            // Tx= ?
            NCVariable transducer = grp.getVariable(PlatformGrp.TRANSDUCER_IDS);
            long[] start = {0};
            long[] count = {dim.getLength()};
            String[] id = transducer.get_string(start, count);
            // for all files if id is less than 100, we add 100
            int computeRxdId = Integer.valueOf(id[0]);
            if (computeRxdId < 100)
                computeRxdId += 100;
            Assert.assertEquals(160, computeRxdId);
            Assert.assertEquals("0", id[1]);
        }
        // check MRU offsets and ids
        {
            int MRU_count = 2;
            long[] start = {0};
            long[] count = {MRU_count};
            // check dim
            NCDimension mruDim = grp.getDimension(PlatformGrp.MRU_DIM_NAME);
            Assert.assertEquals(MRU_count, mruDim.getLength());

            NCVariable ids = grp.getVariable(PlatformGrp.MRU_IDS);
            String[] id = ids.get_string(start, count);
            Assert.assertEquals("001", id[0]);
            Assert.assertEquals("002", id[1]);

            NCVariable xV = grp.getVariable(PlatformGrp.MRU_OFFSET_X);
            float[] values = xV.get_float(start, count);
            Assert.assertEquals(0.0f, values[0], 10e-3);
            Assert.assertEquals(0f, values[1], 10e-3);

            NCVariable yV = grp.getVariable(PlatformGrp.MRU_OFFSET_Y);
            values = yV.get_float(start, count);
            Assert.assertEquals(0f, values[0], 10e-3);
            Assert.assertEquals(0f, values[1], 10e-3);

            NCVariable zV = grp.getVariable(PlatformGrp.MRU_OFFSET_Z);
            values = zV.get_float(start, count);
            Assert.assertEquals(0f, values[0], 10e-3);
            Assert.assertEquals(0f, values[1], 10e-3);

            NCVariable rotxV = grp.getVariable(PlatformGrp.MRU_ROTATION_X);
            values = rotxV.get_float(start, count);
            Assert.assertEquals(0.06f, values[0], 10e-3);
            Assert.assertEquals(0f, values[1], 10e-3);

            NCVariable rotyV = grp.getVariable(PlatformGrp.MRU_ROTATION_Y);
            values = rotyV.get_float(start, count);
            Assert.assertEquals(0f, values[0], 10e-3);
            Assert.assertEquals(0f, values[1], 10e-3);

            NCVariable rotzV = grp.getVariable(PlatformGrp.MRU_ROTATION_Z);
            values = rotzV.get_float(start, count);
            Assert.assertEquals(0f, values[0], 10e-3);
            Assert.assertEquals(0f, values[1], 10e-3);

        }
        // check position offsets and ids
        {
            int positionDim = 1;
            long[] start = {0};
            long[] count = {positionDim};
            NCDimension posDim = grp.getDimension(PlatformGrp.POSITION_DIM_NAME);
            Assert.assertEquals(positionDim, posDim.getLength());
            NCVariable ids = grp.getVariable(PlatformGrp.POSITION_IDS);
            String[] id = ids.get_string(start, count);
            Assert.assertEquals("001", id[0]);
            NCVariable x = grp.getVariable(PlatformGrp.POSITION_OFFSET_X);
            float[] values = x.get_float(start, count);
            Assert.assertEquals(0f, values[0], 10e-3);

            NCVariable y = grp.getVariable(PlatformGrp.POSITION_OFFSET_Y);
            values = y.get_float(start, count);
            Assert.assertEquals(0f, values[0], 10e-3);

            NCVariable z = grp.getVariable(PlatformGrp.POSITION_OFFSET_Z);
            values = z.get_float(start, count);
            Assert.assertEquals(0f, values[0], 10e-3);
        }
        // check water level
        {
            NCVariable v = grp.getVariable(PlatformGrp.WATER_LEVEL);
            float value = v.getScalar_float();
            Assert.assertEquals(0.910f, value, 10e-4);
        }
    }


}
