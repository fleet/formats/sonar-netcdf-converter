package fr.ifremer.globe.api.xsf.tests.all.datagrams;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.networkattitudevelocity.NetworkAttitudeVelocity;

public class TestNetworkAttitudeVelocity extends DatagramTest {

	@Test
	public void testRead() throws Exception {
		DatagramBuffer buffer = getDatagram((byte)0x6e);

		// check heading
		assertEquals(54064, NetworkAttitudeVelocity.getCounter(buffer.byteBufferData).getU());
		assertEquals(212, NetworkAttitudeVelocity.getSerialNumber(buffer.byteBufferData).getU());
		int nEntries = NetworkAttitudeVelocity.getNumberEntries(buffer.byteBufferData).getU();
		assertEquals(10, nEntries);
		assertEquals(0x6e, NetworkAttitudeVelocity.getSensorDescriptor(buffer.byteBufferData).getU());
		
		// check one of the N entries
		int offset = NetworkAttitudeVelocity.getFirstInputOffset();
		assertEquals(0, NetworkAttitudeVelocity.getTimeSinceRecordStart(buffer.byteBufferData, offset).getU());
		assertEquals(103, NetworkAttitudeVelocity.getRoll(buffer.byteBufferData, offset).get());
		assertEquals(-114, NetworkAttitudeVelocity.getPitch(buffer.byteBufferData, offset).get());
		assertEquals(8, NetworkAttitudeVelocity.getHeave(buffer.byteBufferData, offset).get());
		assertEquals(635, NetworkAttitudeVelocity.getHeading(buffer.byteBufferData, offset).getU());
		assertEquals(43, NetworkAttitudeVelocity.getNumberOfBytesInInputDgm(buffer.byteBufferData, offset).getU());
		
		// check last Block the N entries
		offset = NetworkAttitudeVelocity.getFirstInputOffset();
		for (int k=0; k< nEntries-1; k++) {
			offset 		= NetworkAttitudeVelocity.getNextInputOffset(buffer.byteBufferData, offset);
			//System.out.println("Offset(" + k + ")= " + offset + " / LenDgm  : " + nbLenInput);
		}
		// check last of the N entries
		assertEquals(90, NetworkAttitudeVelocity.getTimeSinceRecordStart(buffer.byteBufferData, offset).getU());
		assertEquals(131, NetworkAttitudeVelocity.getRoll(buffer.byteBufferData, offset).get());
		assertEquals(-120, NetworkAttitudeVelocity.getPitch(buffer.byteBufferData, offset).get());
		assertEquals(9, NetworkAttitudeVelocity.getHeave(buffer.byteBufferData, offset).get());
		assertEquals(628, NetworkAttitudeVelocity.getHeading(buffer.byteBufferData, offset).getU());
		
		//String value = NetworkAttitudeVelocity.getInputDatagram(buffer.byteBufferData, offset);
		assertEquals(NetworkAttitudeVelocity.getNumberOfBytesInInputDgm(buffer.byteBufferData, offset).getU(), NetworkAttitudeVelocity.getInputDatagram(buffer.byteBufferData, offset).length);
	}
}
