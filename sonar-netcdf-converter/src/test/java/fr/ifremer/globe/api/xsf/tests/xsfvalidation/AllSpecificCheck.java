package fr.ifremer.globe.api.xsf.tests.xsfvalidation;

import org.junit.Assert;

import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.BathymetryGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.vendor_specific.BeamGroup1VendorSpecificGrp;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCFile;

/**
 * Class containing checks specifics to kmall 
 * typically variable defined only in kmall
 * */
public class AllSpecificCheck implements SpecificChecks {

	public void testBeamGroup(NCFile src,GrpContext context) throws NCException
	{
		// ifremer flag
		// could be not declared 
//		{
//			long[] start = { 0, 0 };
//			long[] count = { context.dimSwath.getLength(), context.dimDetection.getLength() };
//			float[] bs_c = context.grp.getVariable("detection_ifremer_quality_factor").get_float(start, count);
//			// no data in case of .all files
//			for (int swath = 0; swath < context.dimSwath.getLength(); swath++) {
//				for (int detection = 0; detection < context.dimBeam.getLength(); detection++) {
//					float value = bs_c[(int) (swath * context.dimBeam.getLength() + detection)];
//					Assert.assertTrue(Float.isNaN(value));
//				}
//			}
//		}
		// backscatter_calibration
		// could be not declared 

		{
//			long[] start = { 0, 0 };
//			long[] count = { context.dimSwath.getLength(), context.dimDetection.getLength() };
//			float[] bs_c = context.grp.getVariable("detection_receiver_sensitivity_applied").get_float(start, count);
//			// no data in case of .all files
//			for (int swath = 0; swath < context.dimSwath.getLength(); swath++) {
//				for (int detection = 0; detection < context.dimBeam.getLength(); detection++) {
//					float value = bs_c[(int) (swath * context.dimBeam.getLength() + detection)];
//					Assert.assertTrue(Float.isNaN(value));
//				}
//			}
		}
		// backscatter_calibration
		{
			long[] start = { 0, 0 };
			long[] count = { context.dimSwath.getLength(), context.dimDetection.getLength() };
			float[] bs_c = context.bathyGrp.getVariable(BathymetryGrp.DETECTION_BACKSCATTER_CALIBRATION).get_float(start, count);
			// no data in case of .all files
			for (int swath = 0; swath < context.dimSwath.getLength(); swath++) {
				for (int detection = 0; detection < context.dimBeam.getLength(); detection++) {
					float value = bs_c[(int) (swath * context.dimBeam.getLength() + detection)];
					Assert.assertTrue(Float.isNaN(value));
				}
			}
		}
		// detection_mean_absorption_coefficient
		// could be not declared 

		{
//			long[] start = { 0, 0 };
//			long[] count = { context.dimSwath.getLength(), context.dimDetection.getLength() };
//			float[] bs_c = context.grp.getVariable("detection_mean_absorption_coefficient").get_float(start, count);
//			// no data in case of .all files
//			for (int swath = 0; swath < context.dimSwath.getLength(); swath++) {
//				for (int detection = 0; detection < context.dimBeam.getLength(); detection++) {
//					float value = bs_c[(int) (swath * context.dimBeam.getLength() + detection)];
//					Assert.assertTrue(Float.isNaN(value));
//				}
//			}
		}
		// detection_source_level_applied, not available for .all files
		{
//			long[] start = { 0, 0 };
//			long[] count = { context.dimSwath.getLength(), context.dimDetection.getLength() };
//			float[] values = context.grp.getVariable("detection_source_level_applied").get_float(start, count);
//			// no data in case of .all files
//			for (int swath = 0; swath < context.dimSwath.getLength(); swath++) {
//				for (int detection = 0; detection < context.dimBeam.getLength(); detection++) {
//					float value = values[(int) (swath * context.dimBeam.getLength() + detection)];
//					Assert.assertTrue(Float.isNaN(value));
//				}
//			}
		}
		// detection_time_varying_gain, not available for .all files
		{
//			long[] start = { 0, 0 };
//			long[] count = { context.dimSwath.getLength(), context.dimDetection.getLength() };
//			float[] values = context.grp.getVariable("detection_time_varying_gain").get_float(start, count);
//			// no data in case of .all files
//			for (int swath = 0; swath < context.dimSwath.getLength(); swath++) {
//				for (int detection = 0; detection < context.dimBeam.getLength(); detection++) {
//					float value = values[(int) (swath * context.dimBeam.getLength() + detection)];
//					Assert.assertTrue(Float.isNaN(value));
//				}
//			}
		}
		// ping variable, in this file, we are in single swath, so ping=ping raw count
		{
			long[] start = { 0 };
			long[] count = { context.dimSwath.getLength() };
			int[] values = context.beamVendor.getVariable(BeamGroup1VendorSpecificGrp.PING_RAW_COUNT).get_int(start, count);
			for (int swath = 0; swath < context.dimSwath.getLength(); swath++) {
				// ping start with number 741 for .all file, 1168 for kmall
				Assert.assertEquals("Error at index "+swath,swath + 741, values[swath]);
			}
		}
	}
}
