package fr.ifremer.globe.api.xsf.tests.s7k.datagrams;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramParser;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K1008;

public class Test1008 extends TestDatagram {
	
	@Test
	public void testDecode() throws IOException {
		DatagramBuffer buffer = getDataBuffer(1008);
		
		assertEquals(1008, DatagramParser.getDatagramType(buffer));
		
		assertEquals(1, S7K1008.getDepthDescriptor(buffer).getU());
		assertEquals(1, S7K1008.getCorrectionFlag(buffer).getU());
		assertEquals(1.0, S7K1008.getCorrectionFlag(buffer).getU(), 0.0);
	}

}
