package fr.ifremer.globe.api.xsf.tests.s7k.datagrams;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramParser;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K7058;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K7058.BeamOptionalData;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K7058.BeamSnippets;

public class Test7058 extends TestDatagram {

	@Test
	public void testDecode() throws IOException {
		DatagramBuffer buffer = getDataBuffer(7058);
		
		assertEquals(7058, DatagramParser.getDatagramType(buffer));
		
		assertEquals(0, S7K7058.getSonarId(buffer).getLong());
		assertEquals(110267, S7K7058.getPingNumber(buffer).getU());
		assertEquals(0, S7K7058.getMultiPingSequence(buffer).getU());
		assertEquals(301, S7K7058.getNumberOfBeams(buffer).getU());
		assertEquals(0, S7K7058.getErrorFlag(buffer).getU());
		assertEquals(0, S7K7058.getControlFlags(buffer).getU());
		
		BeamSnippets[] snippets = S7K7058.getSnippets(buffer);
		assertEquals(301, snippets.length);
		
		assertEquals(0, snippets[0].beamNumber.getU());
		assertEquals(2097, snippets[0].beginSample.getU());
		assertEquals(2129, snippets[0].endSample.getU());
		assertEquals(2113, snippets[0].bottomDetection.getU());
		assertEquals(16, snippets[0].bottomDetectionIndex);
		assertEquals(-23.69, snippets[0].snippets[0], 0.01);
		assertEquals(-23.27, snippets[0].snippets[snippets[0].bottomDetectionIndex], 0.01);
		assertEquals(-26.78, snippets[0].snippets[32], 0.01);
		
		assertEquals(150, snippets[150].beamNumber.getU());
		assertEquals(495, snippets[150].beginSample.getU());
		assertEquals(505, snippets[150].endSample.getU());
		assertEquals(500, snippets[150].bottomDetection.getU());
		assertEquals(5, snippets[150].bottomDetectionIndex);
		assertEquals(-78.13, snippets[150].snippets[0], 0.01);
		assertEquals(-25.79, snippets[150].snippets[snippets[150].bottomDetectionIndex], 0.01);
		assertEquals(-47.04, snippets[150].snippets[10], 0.01);
		
		int numOfSnippets = 0;
		for (int i = 0; i < snippets.length; i++) {
			numOfSnippets += snippets[i].snippets.length;
		}
		assertEquals(6816, numOfSnippets);
		assertEquals(numOfSnippets, S7K7058.getNumOfSnippets(buffer));
		
		final int optionalOffset = (int) S7K7058.getOptionalDataOffset(buffer).getU();
		assertEquals(100000.0, S7K7058.getFrequency(buffer, optionalOffset).get(), 0.001);
		assertEquals(0.84, S7K7058.getLatitude(buffer, optionalOffset).get(), 0.01);
		assertEquals(-0.086, S7K7058.getLongitude(buffer, optionalOffset).get(), 0.01);
		assertEquals(0.03, S7K7058.getHeading(buffer, optionalOffset).get(), 0.01);
		
		final BeamOptionalData[] optionalData = S7K7058.getBeamOptionalData(buffer, optionalOffset);
		assertEquals(301, optionalData.length);
		assertEquals(49.04, optionalData[0].alongTrackDistance, 0.01);
		assertEquals(-255.11, optionalData[0].acrossTrackDistance, 0.01);
		assertEquals(2113, optionalData[0].centerCenterNumber.getU(), 0.01);
		assertEquals(47.99, optionalData[150].alongTrackDistance, 0.01);
		assertEquals(-0.17, optionalData[150].acrossTrackDistance, 0.01);
		assertEquals(500, optionalData[150].centerCenterNumber.getU(), 0.01);
	}
}
