package fr.ifremer.globe.api.xsf.tests.kmall.datagrams;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.kmall.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.SPO;

public class TestSPODatagrams extends DatagramTest {

    int szDgmHeader 	= 20;
    int szDgm 			= 40;
    int szDgmCommon 	= 8;

    @Test
    public void testRead() throws IOException {
    	DatagramBuffer buffer = getDatagram("SPO");
        
        // part common to all datagrams
        assertEquals("#SPO", SPO.getDgmType(buffer.byteBufferData));       
        assertEquals(140, SPO.getSystemId(buffer.byteBufferData).getU());
        assertEquals(0, SPO.getDgmVersion(buffer.byteBufferData).getU());
        assertEquals(2040, SPO.getEchoSounderId(buffer.byteBufferData).getU());
        assertEquals(1473163103, SPO.getTimeSec(buffer.byteBufferData).getU());
        assertEquals(688048878, SPO.getTimeNano(buffer.byteBufferData).getU());
        
        // Sensor Common data  datagram related part
        assertEquals(130, SPO.getNumBytesCmnPart(buffer.byteBufferData).getU());
        assertEquals(0, SPO.getSensorSystem(buffer.byteBufferData).getU());
        assertEquals(1, SPO.getSensorStatus(buffer.byteBufferData).getU());
        
        // Sensor Data
        assertEquals(1473163103, SPO.getTimeFromSensor_sec(buffer.byteBufferData).getU(), 0.0001);
        assertEquals(659999990, SPO.getTimeFromSensor_nanosec(buffer.byteBufferData).getU());
        assertEquals(0.0700000, SPO.getPosFixQuality_m(buffer.byteBufferData).get(), 0.0001);
        assertEquals(59.435226416666666, SPO.getCorrectedLat_deg(buffer.byteBufferData).get(), 0.001);
        assertEquals(10.473356599999999, SPO.getCorrectedLong_deg(buffer.byteBufferData).get(), 0.001);
        assertEquals(1.4404445, SPO.getSpeedOverGround_mPerSec(buffer.byteBufferData).get(), 0.001);
        assertEquals(3.1564999e+02, SPO.getCourseOverGround_deg(buffer.byteBufferData).get(), 0.001);
        assertEquals(38.9900017, SPO.getEllipsoidHeightReRefPoint_m(buffer.byteBufferData).get(), 0.001);
        
         
        //int nBytes;
        //nBytes = buffer.byteBufferData.capacity() -  szDgmHeader - szDgm - szDgmCommon;
        assertEquals("$INGGA,115823.66,5926.113585,N,01028.401396,E,4,12,0.7,-1.50,M,40.49,M,2.0,0000*7C", SPO.getDataFromSensor(buffer.byteBufferData));
		assertEquals(4, SPO.getSensorQualityIndicator(buffer.byteBufferData).getU());

    }
}
