package fr.ifremer.globe.api.xsf.tests.s7k.datagrams;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramParser;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K7000;

public class Test7000 extends TestDatagram {

	@Test
	public void testDecode() throws IOException {
		DatagramBuffer buffer = getDataBuffer(7000);
		
		assertEquals(7000, DatagramParser.getDatagramType(buffer));
		
		assertEquals(0, S7K7000.getSonarSerialNumber(buffer).getLong());
		assertEquals(110267, S7K7000.getPingNumber(buffer).getU());
		assertEquals(0, S7K7000.getMultiPingSequence(buffer).getU());
		assertEquals(100000.0, S7K7000.getFrequency(buffer).get(), 0.0);
		assertEquals(6022.28, S7K7000.getSampleRate(buffer).get(), 0.01);
		assertEquals(0.000556, S7K7000.getReceiverBandwidth(buffer).get(), 0.00001);
		assertEquals(0.0006, S7K7000.getTxPulseWidth(buffer).get(), 0.0001);
		assertEquals(0, S7K7000.getTxPulseTypeIdentifier(buffer).getU());
		assertEquals(0, S7K7000.getTxPulseEnvelopeIdentifier(buffer).getU());
		assertEquals(0.0, S7K7000.getTxPulseEnvelopeParameter(buffer).get(), 0.0);
		assertEquals(0, S7K7000.getTxPulseReserved(buffer).getU());
		assertEquals(20.0, S7K7000.getMaxPingRate(buffer).get(), 0.0);
		assertEquals(0.467, S7K7000.getPingPeriod(buffer).get(), 0.001);
		assertEquals(300.0, S7K7000.getRangeSelection(buffer).get(), 0.001);
		assertEquals(209.0, S7K7000.getPowerSelection(buffer).get(), 0.001);
		assertEquals(-4.0, S7K7000.getGainSelection(buffer).get(), 0.001);
		assertEquals(17465856, S7K7000.getControlFlag(buffer).getU());
		assertEquals(0, S7K7000.getProjectorIdentifier(buffer).getU());
		assertEquals(0.000709, S7K7000.getProjectorBeamSteeringAngleVertical(buffer).get(), 0.0001);
		assertEquals(0.0, S7K7000.getProjectorBeamSteeringAngleHorizontal(buffer).get(), 0.0);
		assertEquals(0.026180, S7K7000.getProjectorBeamMinus3dBBeamWidthVertical(buffer).get(), 0.0001);
		assertEquals(0.471239, S7K7000.getProjectorBeamMinus3dBBeamWidthHorizontal(buffer).get(), 0.0001);
		assertEquals(10000000.0, S7K7000.getProjectorBeamFocalPoint(buffer).get(), 0.0001);
		assertEquals(0, S7K7000.getProjectorBeamWeightingWindowType(buffer).getU());
		assertEquals(0.0, S7K7000.getProjectorBeamWeightingWindowParameter(buffer).get(), 0.0);
		assertEquals(1, S7K7000.getTransmitFlags(buffer).getU());
		assertEquals(0, S7K7000.getHydrophoneIdentifier(buffer).getU());
		assertEquals(0, S7K7000.getReceiveBeamWeightingWindow(buffer).getU());
		assertEquals(0.0, S7K7000.getReceiveBeamWeightingParameter(buffer).get(), 0.0);
		assertEquals(0, S7K7000.getReceiveFlags(buffer).getU());
		assertEquals(0.02618, S7K7000.getReceiveBeamWidth(buffer).get(), 0.00001);
		assertEquals(275.5, S7K7000.getBottomDetectionFilterInfoMinRange(buffer).get(), 0.01);
		assertEquals(600.0, S7K7000.getBottomDetectionFilterInfoMaxRange(buffer).get(), 0.01);
		assertEquals(44.2, S7K7000.getBottomDetectionFilterInfoMinDepth(buffer).get(), 0.01);
		assertEquals(109.6, S7K7000.getBottomDetectionFilterInfoMaxDepth(buffer).get(), 0.01);
		assertEquals(35.0, S7K7000.getAbsorption(buffer).get(), 0.0);
		assertEquals(1494.76, S7K7000.getSoundVelocity(buffer).get(), 0.01);
		assertEquals(30.0, S7K7000.getSpreading(buffer).get(), 0.0);
	}
}
