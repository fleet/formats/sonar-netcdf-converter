package fr.ifremer.globe.api.xsf.tests.xsfvalidation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import fr.ifremer.globe.api.xsf.converter.all.KongsbergAllConverter;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.perfcounter.PerfoCounter;

/**
 * Test Vlen in real condition
 * 
 * */
public class TestBenchVLen {
	public final static String sourceDirectory = "d://data";

	public final static File in = new File("D://tmp//panache//EM122_mayobs//0243_20190511_174932_EM122_Marion_Dufresne.all");
	public final static File xsf_vlen = new File("d://tmp//OUT.all_vlen.xsf");

	@Rule
	public TemporaryFolder tmp = new TemporaryFolder();

	@Test
	public void run() throws FileNotFoundException, NCException, IOException
	{
		//test with vlen
		PerfoCounter v=new PerfoCounter("VLEN");
		v.start();
		CreateFiles(xsf_vlen);
		v.stop().print();
	}
	public void CreateFiles(File xsf) throws FileNotFoundException, NCException, IOException {

		if ( !xsf.exists() ) {
			xsf.delete();
		}
		new KongsbergAllConverter().convert(in.getAbsolutePath(), xsf.getAbsolutePath());

	}
}
