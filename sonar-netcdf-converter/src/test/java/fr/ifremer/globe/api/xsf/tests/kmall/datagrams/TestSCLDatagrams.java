package fr.ifremer.globe.api.xsf.tests.kmall.datagrams;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.kmall.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.SCL;

public class TestSCLDatagrams extends DatagramTest {

    int szDgmHeader 	= 20;
    int szDgm 			= 8;
    int szDgmCommon 	= 8;

    @Test
    public void testRead() throws IOException {
    	DatagramBuffer buffer = getDatagram("SCL");
        
        // part common to all datagrams
        assertEquals("#SCL", SCL.getDgmType(buffer.byteBufferData));       
        assertEquals(140, SCL.getSystemId(buffer.byteBufferData).getU());
        assertEquals(0, SCL.getDgmVersion(buffer.byteBufferData).getU());
        assertEquals(2040, SCL.getEchoSounderId(buffer.byteBufferData).getU());
        assertEquals(1473163104, SCL.getTimeSec(buffer.byteBufferData).getU());
        assertEquals(652638090, SCL.getTimeNano(buffer.byteBufferData).getU());
        
        // Sensor Common data  datagram related part
        assertEquals(8, SCL.getNumBytesCmnPart(buffer.byteBufferData).getU());
        assertEquals(5, SCL.getSensorSystem(buffer.byteBufferData).getU());
        assertEquals(65535, SCL.getSensorStatus(buffer.byteBufferData).getU());
        
        // Sensor Data
        assertEquals(0.0, SCL.getOffset_sec(buffer.byteBufferData).get(), 0.0001);
        assertEquals(-7361910, SCL.getClockDevPU_nanosec(buffer.byteBufferData).get());
         
        assertEquals("$INZDA,115824.66,06,09,2016,,*77", SCL.getDataFromSensor(buffer.byteBufferData));

    }
}
