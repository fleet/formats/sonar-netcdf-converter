package fr.ifremer.globe.api.xsf.tests.api;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteOrder;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCFile;
import fr.ifremer.globe.netcdf.ucar.NCVariable;
import fr.ifremer.globe.netcdf.ucar.NCFile.Mode;
import fr.ifremer.globe.utils.cache.TemporaryCache;

public class FakeFileGenerator {

	private static final String TEST_VAR_NAME = "TEST_VAR";

	private final String TEST_DIR = "F:\\data\\globe\\xsf_2017_XSF_TESTS\\";

	/**
	 * Main method (entry point of this fake file generator)
	 */
	public void main() throws NCException, FileNotFoundException, IOException {
		// makeFileWith2DLayer(TEST_FILE, DIMENSION, DIMENSION);
		makeFile(TEST_DIR + "test", 50, 50, DataType.INT, 1d, null);
	}

	/**
	 * Creates a file Which contains a 2D variable (= layer)
	 * 
	 * @param filePath
	 * @param dimX
	 *            first dimension of the 2D variable
	 * @param dimY
	 *            second dimension of the 2D variable
	 * @return created file (not closed)
	 */
	public static NCFile makeFile(String filePath, int dimX, int dimY, DataType layerType) throws NCException {

		// Create file
		try (NCFile file = NCFile.createNew(NCFile.Version.netcdf4, filePath)) {
			List<NCDimension> dimList = new ArrayList<>();

			file.addAttribute("description", "file generated for test at " + LocalDateTime.now().toString());

			// Add dimensions
			dimList.add(file.addDimension("TEST_DIM_X", dimX));
			dimList.add(file.addDimension("TEST_DIM_Y", dimY));

			// Create variable (= layer)
			NCVariable var = file.addVariable(TEST_VAR_NAME, layerType, dimList);

			// Add data
			switch (layerType) {
			case INT:
				int[] inputData = new int[dimX * dimY];
				for (int i = 0; i < inputData.length; i++)
					inputData[i] = i;

				var.put(new long[] { 0, 0 }, new long[] { dimX, dimY }, inputData);
				break;
			case BYTE:
				byte[] inputDataByte = new byte[dimX * dimY];
				for (int i = 0; i < inputDataByte.length; i++)
					inputDataByte[i] = (byte) i;

				var.put(new long[] { 0, 0 }, new long[] { dimX, dimY }, inputDataByte);
				break;
			default:
				throw new NCException("layertype = " + layerType + " not handled by FakeFileGenerator");
			}
			file.synchronize();
			return file;
		}
	}

	/**
	 * Creates a file Which contains a 2D variable (= layer) with scale factor
	 * and add offset attributes
	 * 
	 * @return created file (not closed)
	 */
	public static NCFile makeFile(String filePath, int dimX, int dimY, DataType layerType, Number addOffset,
			Number scaleFactor) throws NCException {
		makeFile(filePath, dimX, dimY, layerType);

		try (NCFile file = NCFile.open(filePath, Mode.readwrite)) {

			NCVariable var = file.getVariable(TEST_VAR_NAME);

			if (addOffset != null) {
				if (addOffset instanceof Float)
					var.addAttribute("add_offset", (float) addOffset);
				if (addOffset instanceof Double)
					var.addAttribute("add_offset", (double) addOffset);
			}
			if (scaleFactor != null) {
				if (scaleFactor instanceof Float)
					var.addAttribute("scale_factor", (float) scaleFactor);
				if (scaleFactor instanceof Double)
					var.addAttribute("scale_factor", (double) scaleFactor);
			}
			file.synchronize();
			return file;
		}
	}

	/**
	 * Provides a MappedByteBuffer
	 */
	public static MappedByteBuffer buildMappedByteBuffer(long l) throws FileNotFoundException, IOException {
		File tmpFile = TemporaryCache.createTemporaryFile("GLOBE_TEST_READ_XSF_", null);
		tmpFile.deleteOnExit();
		try (RandomAccessFile raf = new RandomAccessFile(tmpFile, "rw"); FileChannel channel = raf.getChannel()) {
			MappedByteBuffer mbb =  channel.map(MapMode.READ_WRITE, 0, l);
			mbb.order(ByteOrder.nativeOrder());
			return mbb;
		}
	}

}
