package fr.ifremer.globe.api.xsf.tests.all.datagrams;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.runtime.RunTimeDg;

public class TestRunTime extends DatagramTest {

	@Test
	public void testGetBody() throws IOException {
		DatagramBuffer buffer = getDatagram((byte)0x52);
		
		assertEquals(RunTimeDg.getSerialNumber(buffer.byteBufferData).getU(), 212);
		assertEquals(RunTimeDg.getTransmitPulseLength(buffer.byteBufferData).getU(), 70);
		assertEquals(RunTimeDg.getMaximumPortCoverage(buffer.byteBufferData).getInt(), 75);
		assertEquals(RunTimeDg.getMaximumStarboardCoverage(buffer.byteBufferData).getInt(), 75);
		assertEquals(RunTimeDg.getMaximumPortSwathWidth(buffer.byteBufferData).getU(), 500);
		assertEquals(RunTimeDg.getMaximumStarboardSwath(buffer.byteBufferData).getU(), 500);
	}
}
