package fr.ifremer.globe.api.xsf.tests.s7k.datagrams;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramParser;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K1010;

public class Test1010 extends TestDatagram {
	
	@Test
	public void testDecode() throws IOException {
		DatagramBuffer buffer = getDataBuffer(1010);
		
		assertEquals(1010, DatagramParser.getDatagramType(buffer));
		assertEquals(100000.0, S7K1010.getFrequency(buffer).get(), 0.0);
		assertEquals(1, S7K1010.getSoundVelocitySourceFlag(buffer).getU());
		assertEquals(0, S7K1010.getSoundVelocityAlgorithm(buffer).getU());
		assertEquals(1, S7K1010.getConductivityFlag(buffer).getU());
		assertEquals(1, S7K1010.getPressureFlag(buffer).getU());
		assertEquals(0, S7K1010.getPositionFlag(buffer).getU());
		assertEquals(31, S7K1010.getSampleContentValidity(buffer).getU());
		assertEquals(0.0, S7K1010.getLatitude(buffer).get(), 0.0);
		assertEquals(0.0, S7K1010.getLongitude(buffer).get(), 0.0);
		assertEquals(0.0, S7K1010.getSampleRate(buffer).get(), 0.0);
		assertEquals(100, S7K1010.getNumberOfSamples(buffer).getU());
		
		S7K1010.CTDSample[] samples = S7K1010.getSmples(buffer);
		assertEquals(100, samples.length);
		assertEquals(35.34, samples[0].condictivity_salinity, 0.01);
		assertEquals(11.27, samples[0].water_temperature, 0.01);
		assertEquals(0.0, samples[0].pressure_depth, 0.0);
		assertEquals(1494.71, samples[0].sound_velocity, 0.01);
		assertEquals(39.02, samples[0].absorption, 0.01);
		assertEquals(35.72, samples[99].condictivity_salinity, 0.01);
		assertEquals(9.56, samples[99].water_temperature, 0.01);
		assertEquals(1000.0, samples[99].pressure_depth, 0.0);
		assertEquals(1508.67, samples[99].sound_velocity, 0.01);
		assertEquals(34.90, samples[99].absorption, 0.01);
	}

}
