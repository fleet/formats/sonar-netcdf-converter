package fr.ifremer.globe.api.xsf.tests.all.datagrams;

import static org.junit.Assert.assertEquals;

import java.nio.ByteBuffer;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.wc.WC;

public class TestWC extends DatagramTest {

    @Test
    public void testWC() throws Exception {
        final DatagramBuffer buffer = getDatagram((byte)0x6B);
        final ByteBuffer datagram = buffer.byteBufferData;

        assertEquals(3, WC.getDatagramCount(datagram).getU());
        assertEquals(1, WC.getDatagramNumber(datagram).getU());
        
        assertEquals(2, WC.getNTxTransmitSectorCount(datagram).getU());
        assertEquals(256, WC.getTotalBeamCount(datagram).getU());
        assertEquals(74, WC.getReceivedBeamCount(datagram).getU());
        
        assertEquals(15101, WC.getSoundSpeed(datagram).getU());
        assertEquals(1531863, WC.getSamplingFreq(datagram).getU());
        assertEquals(22, WC.getTxTimeHeave(datagram).get());
        assertEquals(30, WC.getTVGFunction(datagram).getU());
        assertEquals(25, WC.getTVGOffset(datagram).get());
        assertEquals(0x00, WC.getScanningInfo(datagram).getU());
        
        // Parse tx entries
        assertEquals(12, WC.getTxTilt(datagram, 0).get());
        assertEquals(19000, WC.getTxCenterFq(datagram, 0).getU());
        assertEquals(0, WC.getTxSectorIndex(datagram, 0).getU());
        assertEquals(12, WC.getTxTilt(datagram, 1).get());
        assertEquals(22000, WC.getTxCenterFq(datagram, 1).getU());
        assertEquals(1, WC.getTxSectorIndex(datagram, 1).getU());
        
        // Parse first entries
        int rxOffset = WC.getRXOffset(datagram);
        assertEquals(52, rxOffset);
        assertEquals(7499, WC.getBeamPointingAngle(datagram, rxOffset).get());
        assertEquals(0, WC.getStartRange(datagram, rxOffset).getU());
        assertEquals(1372, WC.getSampleCount(datagram, rxOffset).getU());
        assertEquals(1243, WC.getDetectedRangeInSample(datagram, rxOffset).getU());
        assertEquals(0, WC.getTransmitSectorNumber(datagram, rxOffset).getU());
        assertEquals(255, WC.getBeamNumber(datagram, rxOffset).getU());
        assertEquals(-128, WC.getSamples(datagram, rxOffset)[0]);
        
        // move forward a few RX entries
        for (int i = 0; i < 10; i++) {
            rxOffset = WC.getNextRxOffset(datagram, rxOffset);
        }
        assertEquals(12764, rxOffset);
        assertEquals(7186, WC.getBeamPointingAngle(datagram, rxOffset).get());
        assertEquals(0, WC.getStartRange(datagram, rxOffset).getU());
        assertEquals(1142, WC.getSampleCount(datagram, rxOffset).getU());
        assertEquals(1033, WC.getDetectedRangeInSample(datagram, rxOffset).getU());
        assertEquals(0, WC.getTransmitSectorNumber(datagram, rxOffset).getU());
        assertEquals(255, WC.getBeamNumber(datagram, rxOffset).getU());
        assertEquals(-128, WC.getSamples(datagram, rxOffset)[0]);
    }
}
