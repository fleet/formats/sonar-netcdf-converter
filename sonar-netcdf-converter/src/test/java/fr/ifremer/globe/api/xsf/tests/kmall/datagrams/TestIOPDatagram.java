package fr.ifremer.globe.api.xsf.tests.kmall.datagrams;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.kmall.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.IOP;

public class TestIOPDatagram extends DatagramTest {

    @Test
    public void testRead() throws IOException {
        DatagramBuffer buffer = getDatagram("IOP");

        assertEquals(0, IOP.getInfo(buffer.byteBufferData).getU());
        assertEquals(0, IOP.getStatus(buffer.byteBufferData).getU());
        assertEquals("Sector Coverage\n" + 
                "Max Angle (deg): Port      75.000\n" + 
                "Max Angle (deg): Starboard 15.000\n" + 
                "Max Coverage (m): Port      500.000\n" + 
                "Max Coverage (m): Starboard 500.000\n" + 
                "Angular Coverage Mode: AUTO\n" + 
                "Sector Mode: NORMAL\n" + 
                "Beam Spacing: HD EQUIDISTANCE\n" + 
                "RX in use: RX_1 ONLY\n" + 
                "\n" + 
                "Depth Settings\n" + 
                "Forced Depth (m): 0.000\n" + 
                "Min. Depth (m): 1.000\n" + 
                "Max. Depth (m): 150.000\n" + 
                "Ping Mode: 300kHz\n" + 
                "Pulse Mode: AUTO\n" + 
                "FM disable: OFF\n" + 
                "Detector Mode: NORMAL\n" + 
                "\n" + 
                "Transmit Control\n" + 
                "Pitch stabilization: ON\n" + 
                "Along Direction (deg): 0.000\n" + 
                "Yaw Stabilisation Mode: OFF\n" + 
                "Yaw Stabilisation Heading: 0.000\n" + 
                "Yaw Stabilisation Heading Filter: MEDIUM\n" + 
                "Max. Ping Freq. (Hz): 50.000\n" + 
                "External Trigger: OFF\n" + 
                "3D Scanning Enable: OFF\n" + 
                "\n" + 
                "Sound Speed at Transducer\n" + 
                "Sound Speed Manual (m/s): 1500.000\n" + 
                "Temperature Manual (C): 0.000\n" + 
                "Sound Speed Source: PROFILE\n" + 
                "\n" + 
                "Absorption Coefficient\n" + 
                "200.0 kHz  70.000\n" + 
                "250.0 kHz  85.000\n" + 
                "300.0 kHz  90.000\n" + 
                "350.0 kHz  100.000\n" + 
                "400.0 kHz  110.000\n" + 
                "\n" + 
                "Filtering\n" + 
                "Spike Filter Strength: MEDIUM\n" + 
                "Range Gate: NORMAL\n" + 
                "Phase ramp: NORMAL\n" + 
                "Penetration Filter Strength: OFF\n" + 
                "Special TVG: OFF\n" + 
                "Slope: ON\n" + 
                "Aeration: OFF\n" + 
                "Sector Tracking: OFF\n" + 
                "Interference: OFF\n" + 
                "Special amp detection: OFF\n" + 
                "\n" + 
                "Backscatter Adjustment\n" + 
                "Normal Incidence corr. (deg): 10.000\n" + 
                "Use Lambert's law: OFF\n" + 
                "\n" + 
                "TX Power Level: Max.\n" + 
                "\n" + 
                "Special Mode\n" + 
                "Sonar: OFF\n" + 
                "\n" + 
                "Water Column: ON\n" + 
                "Water Column: 30 log R\n" + 
                "Water Column: 197374737 dB Offset\n" + 
                "\n" + 
                "Scope Display\n" + 
                "Beam no. (Display): 0\n" + 
                "Swath no.: 0\n" + 
                "|", IOP.getRuntimeTxt(buffer.byteBufferData));
        
        assertEquals(10.0f, IOP.getTvgLawCrossOverAngle(buffer.byteBufferData), 0.f);
    }
}
