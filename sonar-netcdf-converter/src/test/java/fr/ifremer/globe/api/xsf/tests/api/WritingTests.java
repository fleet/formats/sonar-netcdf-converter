package fr.ifremer.globe.api.xsf.tests.api;

import static java.lang.System.out;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteOrder;
import java.nio.MappedByteBuffer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfBase;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.api.NetcdfVariable;
import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCFile.Mode;

/**
 * This class provides tests to verify the read functionalities of the XSF API.
 */
public class WritingTests {
	private static final String TEST_DIR = "file" + File.separator + "generated" + File.separator;

	@BeforeClass
	public static void before() throws IOException {
		FileUtils.forceMkdir(Paths.get(TEST_DIR).toFile());
	}

	@AfterClass
	public static void after() throws IOException {
		FileUtils.cleanDirectory(Paths.get(TEST_DIR).toFile());
	}

	/** Test simple file with a 2D integer variable (=layer) */
	@Test
	public void testSimpleFile() throws NCException, FileNotFoundException, IOException {
		String filePath = TEST_DIR + "test_write_simple" + XsfBase.EXTENSION_XSF;
		if (!Files.exists(Paths.get(filePath)))
			FakeFileGenerator.makeFile(filePath, 20, 20, DataType.INT);
		writingTest("testSimpleFile", filePath);

	}

	/** Test 1Go file with a 2D integer variable (=layer) */
	@Test
	public void test1GoFile() throws NCException, FileNotFoundException, IOException {
		String filePath = TEST_DIR + "test_write_1go" + XsfBase.EXTENSION_XSF;
		if (!Files.exists(Paths.get(filePath)))
			FakeFileGenerator.makeFile(filePath, 16384, 16384, DataType.INT);
		writingTest("test1GoFile", filePath);
	}

	/** Test file with add offset */
	@Test
	public void testAddOffsetFloat() throws NCException, FileNotFoundException, IOException {
		// Test with a float offset
		String filePath = TEST_DIR + "test_write_add_offset_f" + XsfBase.EXTENSION_XSF;
		if (!Files.exists(Paths.get(filePath)))
			FakeFileGenerator.makeFile(filePath, 200, 200, DataType.INT, 5f, null);
		writingTest("testAddOffsetFloat", filePath);
	}

	/** Test file with add offset (double value) */
	@Test
	public void testAddOffsetDouble() throws NCException, FileNotFoundException, IOException {
		String filePath = TEST_DIR + "test_write_add_offset_d" + XsfBase.EXTENSION_XSF;
		if (!Files.exists(Paths.get(filePath)))
			FakeFileGenerator.makeFile(filePath, 200, 200, DataType.INT, 5d, null);
		writingTest("testAddOffsetDouble", filePath);
	}

	/** Test file with scale factor */
	@Test
	public void testScaleFactorFloat() throws NCException, FileNotFoundException, IOException {
		String filePath = TEST_DIR + "test_write_scale_factor_f" + XsfBase.EXTENSION_XSF;
		if (!Files.exists(Paths.get(filePath)))
			FakeFileGenerator.makeFile(filePath, 200, 200, DataType.INT, null, 0.5f);
		writingTest("testScaleFactorFloat", filePath);
	}

	/** Test file with scale factor (double value) */
	@Test
	public void testScaleFactorDouble() throws NCException, FileNotFoundException, IOException {
		String filePath = TEST_DIR + "test_write_scale_factor_d" + XsfBase.EXTENSION_XSF;
		if (!Files.exists(Paths.get(filePath)))
			FakeFileGenerator.makeFile(filePath, 200, 200, DataType.INT, null, 0.5d);
		writingTest("testScaleFactorDouble", filePath);
	}

	/** Test file with add offset and scale factor */
	// @Test
	public void testScaleFactorAndAddOffset() throws NCException, FileNotFoundException, IOException {
		String filePath = TEST_DIR + "test_write_add_offset_scale_factor_f" + XsfBase.EXTENSION_XSF;
		if (!Files.exists(Paths.get(filePath)))
			FakeFileGenerator.makeFile(filePath, 200, 200, DataType.INT, 5f, 2f);
		writingTest("testScaleFactorAndAddOffset", filePath);
	}

	/** Test file with add offset and scale factor (double values) */
	@Test
	public void testScaleFactorAndAddOffsetDoubles() throws NCException, FileNotFoundException, IOException {
		String filePath = TEST_DIR + "test_write_add_offset_scale_factor_d" + XsfBase.EXTENSION_XSF;
		if (!Files.exists(Paths.get(filePath)))
			FakeFileGenerator.makeFile(filePath, 200, 200, DataType.INT, 5d, 2d);
		writingTest("testScaleFactorAndAddOffsetDoubles", filePath);
	}

	/**
	 * Reading test: expect a test file which contains a variable (TEST_VAR) with values equal to their indexes (before
	 * the unpacking with scale factor and add offset).
	 */
	@SuppressWarnings("incomplete-switch")
	private void writingTest(String testName, String filePath) throws FileNotFoundException, IOException, NCException {

		long startTime = System.currentTimeMillis();
		MappedByteBuffer outputMappedFile = null;

		try (NetcdfFile file = NetcdfFile.open(filePath, Mode.readwrite)) {

			// Retrieve tested file's dimensions
			long dimX = file.getDimension("TEST_DIM_X").getLength();
			long dimY = file.getDimension("TEST_DIM_Y").getLength();

			NetcdfVariable var = file.getVariable("TEST_VAR");

			// Get scale factor and add offset
			double scaleFactor = 1d;
			double addOffset = 0d;
			List<String> attributes = Arrays.asList(var.getAttributeNames());
			DataType inputBufferDataType = var.getRawDataType();
			DataType variableDataType = var.getRawDataType();

			if (attributes.contains("scale_factor")) {
				switch (var.getAttributeDataType("scale_factor")) {
				case FLOAT:
					scaleFactor = var.getAttributeFloat("scale_factor");
					if (inputBufferDataType.getSize() <= DataType.FLOAT.getSize())
						inputBufferDataType = DataType.FLOAT;
					break;
				case DOUBLE:
					scaleFactor = var.getAttributeDouble("scale_factor");
					if (inputBufferDataType.getSize() < DataType.DOUBLE.getSize())
						inputBufferDataType = DataType.DOUBLE;
					break;
				}
			}

			if (attributes.contains("add_offset")) {
				switch (var.getAttributeDataType("add_offset")) {
				case FLOAT:
					addOffset = var.getAttributeFloat("add_offset");
					if (inputBufferDataType.getSize() <= DataType.FLOAT.getSize())
						inputBufferDataType = DataType.FLOAT;
					break;
				case DOUBLE:
					addOffset = var.getAttributeDouble("add_offset");
					if (inputBufferDataType.getSize() < DataType.DOUBLE.getSize())
						inputBufferDataType = DataType.DOUBLE;
					break;
				}
			}

			// Create mapped byte buffer
			MappedByteBuffer inputMappedFile = FakeFileGenerator
					.buildMappedByteBuffer(dimX * dimY * inputBufferDataType.getSize());
			inputMappedFile.order(ByteOrder.nativeOrder());

			// Set a random offset which will determinate the values to write
			int randomIntOffset = (int) (Math.random() * 100);
			var.addAttribute("Random int offset use to write values", randomIntOffset);

			int j = randomIntOffset;
			while (inputMappedFile.hasRemaining()) {
				switch (inputBufferDataType) {
				case INT:
					inputMappedFile.putInt(j);
					break;
				case FLOAT:
					inputMappedFile.putFloat(j);
					break;
				case DOUBLE:
					inputMappedFile.putDouble(j);
					break;
				default:
					throw new NCException("Wrong inputMappedFile data type");
				}
				j++;
			}

			// Write XSF variable
			// var.write(inputMappedFile, "", "");
			var.write(new long[] { dimX, dimY }, inputMappedFile, inputBufferDataType, Optional.empty());
			file.synchronize();

			// Print reading duration (avoid / by 0)
			long bytesReadPerSec = (System.currentTimeMillis() - startTime != 0)
					? inputMappedFile.limit() * 1000L / (System.currentTimeMillis() - startTime)
					: -1;
			out.format(
					"[%s] write -> speed :  %,d bytes/sec , duration : %,d ms, buffer size: %,d bytes, data type: %s \n",
					testName, bytesReadPerSec, System.currentTimeMillis() - startTime, inputMappedFile.limit(),
					inputBufferDataType);

			// Check the

			// Read XSF variables
			outputMappedFile = FakeFileGenerator.buildMappedByteBuffer(dimX * dimY * variableDataType.getSize());

			// Read bytes (get raw values (packed))
			var.get_byte(new long[] { 0, 0 }, new long[] { dimX, dimY }, outputMappedFile);

			// Expected == packed value : ((index + randomIntOffset) -
			// addOffset)/scaleFactor
			outputMappedFile.position(0);
			while (outputMappedFile.hasRemaining()) {
				int index = (outputMappedFile.position() / variableDataType.getSize());
				double expected = ((index + randomIntOffset) - addOffset) / scaleFactor;
				switch (variableDataType) {
				case FLOAT:
					float fValue = outputMappedFile.getFloat();
					assertTrue("For index : " + index + ", expected: " + (float) (expected) + " , got:  " + fValue,
							(float) (expected) == fValue);
					break;
				case DOUBLE:
					double dValue = outputMappedFile.getDouble();
					assertTrue("For index : " + index + ", expected: " + (expected) + " , got:  " + dValue,
							(expected) == dValue);
					break;
				case BYTE:
					byte bValue = outputMappedFile.get();
					assertTrue("For index : " + index + ", expected: " + (byte) (expected) + " , got:  " + bValue,
							(byte) (expected) == bValue);
					break;
				case SHORT:
					short sValue = outputMappedFile.getShort();
					assertTrue("For index : " + index + ", expected: " + (short) Math.round(expected) + " , got:  "
							+ sValue, (short) Math.round(expected) == sValue);
					break;
				case INT:
					int iValue = outputMappedFile.getInt();
					assertTrue(
							"For index : " + index + ", expected: " + (int) Math.round(expected) + " , got:  " + iValue,
							(int) Math.round(expected) == iValue);
					break;
				case LONG:
					long lValue = outputMappedFile.getLong();
					assertTrue("For index : " + index + ", expected: " + (long) (expected) + " , got:  " + lValue,
							(long) (expected) == lValue);
					break;
				}
			}

		} finally {
			outputMappedFile = null;
			System.gc();
		}

	}
}
