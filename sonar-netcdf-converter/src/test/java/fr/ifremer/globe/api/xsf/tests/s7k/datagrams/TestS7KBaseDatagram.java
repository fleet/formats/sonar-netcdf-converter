package fr.ifremer.globe.api.xsf.tests.s7k.datagrams;

import static org.junit.Assert.*;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7KBaseDatagram;
import fr.ifremer.globe.utils.date.DateUtils;

public class TestS7KBaseDatagram extends TestDatagram {

	@Test
	public void testDecode() throws IOException {
		DatagramBuffer buffer = getDataBuffer(1015);
		
		assertEquals(5, S7KBaseDatagram.getProtocolVersion(buffer).getU());
		assertEquals(60, S7KBaseDatagram.getOffset(buffer).getU());
		assertEquals(65535, S7KBaseDatagram.getSyncPattern(buffer).getU());
		assertEquals(109, S7KBaseDatagram.getSize(buffer).getU());
		assertEquals(0, S7KBaseDatagram.getOptionalDataOffset(buffer).getU());
		assertEquals(191, S7KBaseDatagram.getOptionalDataIdentifier(buffer).getU());
		assertEquals(LocalDateTime.of(2012, 4, 30, 12, 45, 45).toInstant(ZoneOffset.UTC).toEpochMilli(), S7KBaseDatagram.getTimeMilli(buffer));
		assertEquals(DateUtils.milliSecondToNano(1335789945000l), S7KBaseDatagram.getTimestampNano(buffer));
		assertEquals(1015, S7KBaseDatagram.getRecordType(buffer).getU());
		assertEquals(7003, S7KBaseDatagram.getDeviceIdentifier(buffer).getU());
		assertEquals(0, S7KBaseDatagram.getSystemEnumerator(buffer).getU());
		assertEquals(1, S7KBaseDatagram.getFlags(buffer).getU());
		assertEquals(0, S7KBaseDatagram.getRecordsInFragmentedSet(buffer).getU());
		assertEquals(0, S7KBaseDatagram.getFragmentNumber(buffer).getU());
		assertEquals(6996, S7KBaseDatagram.getChecksum(buffer).getU());
		assertEquals(6996, S7KBaseDatagram.computeChecksum(buffer).getU());
		assertTrue(S7KBaseDatagram.validChecksum(buffer));
	}
}
