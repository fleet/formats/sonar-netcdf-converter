package fr.ifremer.globe.api.xsf.tests.kmall.datagrams;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.IOException;
import java.util.List;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.kmall.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.IIP;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.IIPMetadata;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.IIPMetadata.Tray;

public class TestIIPDatagrams extends DatagramTest {

	@Test
	public void testRead() throws IOException {
		DatagramBuffer buffer = getDatagram("IIP");

		assertEquals("OSCV:OSV=\"SIS UNKNOWN\";ROP=\"KS user\",EMXV: EM2040,\n" + "PU_0,\n" + "SN=9001,\n"
				+ "IP=157.237.20.40:0xffff0000,\n" + "UDP=1997,\n" + "TYPE=CON_TECH_PP_833_SMP_CPU,\n" + "VERSIONS:,\n"
				+ "CPU:REV A,\n" + "VXW:6.9 SMP Nov 23 2015,\n" + "FILTER:2.2.0 160426,\n" + "CBMF:1.11 15.01.22 ,\n"
				+ "TX:1.07   Jan 23 2014 ,\n" + "RX:1.02   Nov 12 2012 ,\n" + "VERSIONS-END,\n" + "SERIALno:\n"
				+ "TX:101,\n" + "RX:9001,\n" + "SERIALno-END,\n"
				+ " TRAI_TX1:N=101;X=5.023;Y=0.707;Z=0.624;R=-30.000;P=0.000;H=0.000;S=0.4;IPX=.0.000000;IPY=-0.05540;IPZ=-0.01200;ICX=0.00000;ICY=0.01315;ICZ=-0.00600;ISX=0.00000;ISY=0.05540;ISZ=-0.01200,TRAI_RX1:N=9001;X=4.998;Y=0.053;Z=0.667;R=-40.000;P=0.000;H=180.000;G=0;IX=0.01100;IY=0.00000;IZ=-0.00600,POSI_1:X=0.000;Y=0.000;Z=0.000;D=0.000;G=WGS84;T=PU;C=OFF;F=GGA,;Q=ON;I=COM_1;U=ACTIVE,POSI_2:U=NOT_SET,POSI_3:U=NOT_SET,ATTI_1:X=0.000;Y=0.000;Z=0.000;R=-2.000;P=1.300;M=RP;D=0.000;H=0.000;D=0.000;U=ACTIVE_MRU+H,ATTI_2:U=NOT_SET,CLCK:F=ZDA;S=CLK;A=ON_FALL;O=0.000;I=COM_1;Q=OK,GYRI:U=NOT_SET,DPHI:U=NOT_SET,EMXI:SSNL=NORMAL;SWLZ=0.000,",
				IIP.getInstallTxt(buffer.byteBufferData));
	}

	@Test
	public void testParseInMetadata() throws IOException {
		IIPMetadata metadata = new IIPMetadata();
		DatagramBuffer buffer = getDatagram("IIP");
		metadata.setInstallationTxt(IIP.getInstallTxt(buffer.byteBufferData),
				IIP.getSystemId(buffer.byteBufferData).getU());

		assertEquals(101, metadata.getS_no_tx());
		assertEquals(9001, metadata.getS_no_rx());
		assertEquals("EM2040", metadata.getModel());

		List<Tray> trays = metadata.getTrays();

		assertEquals(2, trays.size());

		assertEquals(1, trays.get(0).index);
		assertEquals(9001, trays.get(0).sn);
		assertEquals(4.998, trays.get(0).x, 0.001);
		assertEquals(0.0529, trays.get(0).y, 0.0001);
		assertEquals(0.667, trays.get(0).z, 0.0001);
		assertEquals(-40.0, trays.get(0).r, 0.0001);
		assertEquals(0.0, trays.get(0).p, 0.0001);
		assertEquals(180.0, trays.get(0).h, 0.0001);
		assertEquals(0.0, trays.get(0).g, 0.0001);
		assertEquals(0.0, trays.get(0).s, 0.0001);
		assertNull(trays.get(0).ip);
		assertNull(trays.get(0).ic);
		assertNull(trays.get(0).is);
		assertNull(trays.get(0).it);
		assertNull(trays.get(0).ir);
		assertEquals(0.011, trays.get(0).i.x, 0.001);
		assertEquals(0.0, trays.get(0).i.y, 0.001);
		assertEquals(-0.006, trays.get(0).i.z, 0.001);

		assertEquals(1, trays.get(1).index);
		assertEquals(101, trays.get(1).sn);
		assertEquals(5.023, trays.get(1).x, 0.001);
		assertEquals(0.707, trays.get(1).y, 0.0001);
		assertEquals(0.624, trays.get(1).z, 0.0001);
		assertEquals(-30.0, trays.get(1).r, 0.0001);
		assertEquals(0.0, trays.get(1).p, 0.0001);
		assertEquals(0, trays.get(1).h, 0.0001);
		assertEquals(0.0, trays.get(1).g, 0.0001);
		assertEquals(0.4, trays.get(1).s, 0.0001);
		assertNotNull(trays.get(1).ip);
		assertNotNull(trays.get(1).ic);
		assertNotNull(trays.get(1).is);
		assertNull(trays.get(1).i);
		assertNull(trays.get(1).it);
		assertNull(trays.get(1).ir);

	}
}
