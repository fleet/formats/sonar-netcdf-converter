package fr.ifremer.globe.api.xsf.tests.all.converter;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.api.xsf.converter.all.KongsbergAllConverter;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.util.comparator.NetcdfComparator;
import fr.ifremer.globe.netcdf.util.comparator.NetcdfComparatorHook;
import fr.ifremer.globe.utils.test.GlobeTestUtil;

public class TestConvertAllToMbg {

	/** Logger **/
	private static final Logger LOGGER = LoggerFactory.getLogger(TestConvertAllToMbg.class);

	/** Test directory **/
	private static final String TEST_DIR_PATH = GlobeTestUtil.getTestDataPath() + "/file/converter/all/mbg";
	//private static final String TEST_DIR_PATH = "F:\\data\\test_all_to_mbg";

	/**
	 * Tests the new converter by comparison with the previous one.
	 */
	@Test
	public void testConvertAllToMbg() throws NCException, IOException {
		StringBuilder resume = new StringBuilder("testConvertAllToMbg result : \n");
		boolean testFailed = false;
		for (final File inputFile : new File(TEST_DIR_PATH).listFiles()) {
			if (inputFile.getName().endsWith(".all")) {
				String referenceMbgPath = inputFile.getAbsolutePath().replace(".all", "_ref.mbg");
				Assert.assertTrue(new File(referenceMbgPath).exists());
				String testMbgPath = inputFile.getAbsolutePath().replace(".all", "_test.mbg");
				try {
					new KongsbergAllConverter().convert(inputFile.getAbsolutePath(), testMbgPath);
					Assert.assertTrue(new File(testMbgPath).exists());

					NetcdfComparatorHook hook = buildHook();
					if(inputFile.getName().contains("0014_20121105_103947_SimradEch"))
					{
						//bug in old converter when tx antenna heading was around 180. Sign -1 was applyied to angles (should be but only for rx angle)
						hook.ignoreVariable("mbAcrossBeamAngle");  
					}
					hook.setDefaultValues("mbDepth", Double.valueOf(0), Double.valueOf(-2147483648));
					hook.setDefaultValues("mbTime", Double.valueOf(0), Double.valueOf(-2147483648));
					hook.setDefaultValues("mbDate", Double.valueOf(0), Double.NaN);
					// For this 2 files, across beam angle is not comparable with reference file.
					// In fact, reference files have been generated with the previous converter which didn't
					// handle correctly antenna for configuration STC = 1 (Single Head) & STC = 2 (Dual Head)
					if (inputFile.getName().contains("Cormoran") || inputFile.getName().contains("TerStreep"))
						hook.ignoreVariable("mbAcrossBeamAngle");

					// Old .all file with Depth68 datagrams
					if (inputFile.getName().contains("0028_20110913_152710_raw")) {
						hook.setToleranceForAttribute("mbMinDepth", 1E-4); // acceptable delta
						hook.setToleranceForAttribute("mbMaxDepth", 1E-4); // acceptable delta
						hook.ignoreVariable("mbRange");  // minor delta: TODO define a tolerance
						hook.ignoreVariable("mbSounderMode"); // sounder mode seems to not handled by old converter (=ref file)
					}

					Optional<String> errors = NetcdfComparator.compareFiles(referenceMbgPath, testMbgPath,
							Optional.of(hook));

					if (errors.isPresent()) {
						testFailed = true;
						resume.append(testMbgPath + " : FAILED \n");
						resume.append(errors.get() + "\n");
					} else {
						resume.append(testMbgPath + " : OK \n");
					}
				} catch (Exception e) {
					testFailed = true;
					resume.append(inputFile.getName() + " exception during convertion: " + e + "\n");
					LOGGER.error(inputFile.getName() + " exception during convertion: " + e, e);
				}
			}
		}
		LOGGER.debug(resume.toString());
		Assert.assertFalse(resume.toString(), testFailed);
	}

	/**
	 * Defines variable(s)/attribute(s) to ignore.
	 */
	private NetcdfComparatorHook buildHook() {
		NetcdfComparatorHook hook = new NetcdfComparatorHook();

		// obviously : name, and date/time of file creation are different
		hook.ignoreGlobalAttribute("mbName");
		hook.ignoreVariable("mbHistAutor");
		hook.ignoreVariable("mbHistDate");
		hook.ignoreVariable("mbHistTime");

		// velocity profile variables : a bug when there is several profile has been fixed
		hook.ignoreVariable("mbVelProfilDate");
		hook.ignoreVariable("mbVelProfilTime");
		hook.ignoreVariable("mbVelProfilIdx");

		// the attribute "_FillValue" has been introduced with the new converter
		hook.ignoreAttributeForAllVariables("_FillValue");

		return hook;
	}

}
