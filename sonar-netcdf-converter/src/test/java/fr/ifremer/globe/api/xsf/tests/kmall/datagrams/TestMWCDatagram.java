package fr.ifremer.globe.api.xsf.tests.kmall.datagrams;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.kmall.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.MWC;

public class TestMWCDatagram extends DatagramTest {

    @Test
    public void testMWCDatagram() throws IOException {
        DatagramBuffer buffer = getDatagram("MWC");

        assertEquals(12, MWC.getNumBytesTxInfo(buffer.byteBufferData));
        assertEquals(3, MWC.getNumTxSectors(buffer.byteBufferData).getU());
        assertEquals(16, MWC.getNumBytesPerTxSector(buffer.byteBufferData));
        assertEquals(0.019, MWC.getHeave_m(buffer.byteBufferData).get(), 0.001);
       
        // TX sector data
        assertEquals(-0.317, MWC.getTiltAngleReTx_deg(buffer.byteBufferData, 0).get(), 0.001);
        assertEquals(260000.0, MWC.getCentreFreq_Hz(buffer.byteBufferData, 0).get(), 0.001);
        assertEquals(0.576, MWC.getTxBeamWidthAlong_deg(buffer.byteBufferData, 0).get(), 0.001);
        assertEquals(0, MWC.getTxSectorNum(buffer.byteBufferData, 0).getU(), 0.001);
        
        assertEquals(-0.74, MWC.getTiltAngleReTx_deg(buffer.byteBufferData, 1).get(), 0.001);
        assertEquals(320000.0, MWC.getCentreFreq_Hz(buffer.byteBufferData, 1).get(), 0.001);
        assertEquals(0.468, MWC.getTxBeamWidthAlong_deg(buffer.byteBufferData, 1).get(), 0.001);
        assertEquals(1, MWC.getTxSectorNum(buffer.byteBufferData, 1).getU(), 0.001);
        
        assertEquals(-0.801, MWC.getTiltAngleReTx_deg(buffer.byteBufferData, 2).get(), 0.001);
        assertEquals(290000.0, MWC.getCentreFreq_Hz(buffer.byteBufferData, 2).get(), 0.001);
        assertEquals(0.517, MWC.getTxBeamWidthAlong_deg(buffer.byteBufferData, 2).get(), 0.001);
        assertEquals(2, MWC.getTxSectorNum(buffer.byteBufferData, 2).getU(), 0.001);

        assertEquals(16, MWC.getNumBytesRxInfo(buffer.byteBufferData));
        assertEquals(256, MWC.getNumBeams(buffer.byteBufferData).getU());
        assertEquals(15, MWC.getNumBytesPerBeamEntry(buffer.byteBufferData));
        assertEquals(0, MWC.getPhaseFlag(buffer.byteBufferData).getU());
        assertEquals(30, MWC.getTVGfunctionApplied(buffer.byteBufferData).getU());
        assertEquals(20, MWC.getTVGoffset_dB(buffer.byteBufferData).get());
        assertEquals(15318.627, MWC.getSampleFreq_Hz(buffer.byteBufferData).get(), 0.001);
        assertEquals(1460.002, MWC.getSoundVelocity_mPerSec(buffer.byteBufferData).get(), 0.001);
        
        int offset = MWC.getInitialRxBeamOffset(buffer.byteBufferData);
        assertEquals(75.057, MWC.getBeamPointAngReVertical_deg(buffer.byteBufferData, offset).get(), 0.001);
        assertEquals(0, MWC.getStartRangeSampleNum(buffer.byteBufferData, offset).getU());
        assertEquals(1594, MWC.getDetectedRangeInSamples(buffer.byteBufferData, offset).getU());
        assertEquals(0, MWC.getBeamTxSectorNum(buffer.byteBufferData, offset).getU());
        assertEquals(1792, MWC.getNumSampleData(buffer.byteBufferData, offset).getU());
		int datagramVersion = MWC.getDgmVersion(buffer.byteBufferData).getU();

        for (int i = 0; i < MWC.getNumBeams(buffer.byteBufferData).getU(); i++) {
            assertTrue(MWC.getBeamTxSectorNum(buffer.byteBufferData, offset).getU() < MWC.getNumTxSectors(buffer.byteBufferData).getU());
            assertEquals(MWC.getNumSampleData(buffer.byteBufferData, offset).getU(), MWC.getSampleAmplitude05dB_p(buffer.byteBufferData, offset,datagramVersion).length);
            offset = MWC.getNextRxBeamOffset(buffer.byteBufferData, offset,datagramVersion);
        }
        // after all the iterations, we reached the end of the datagram (just before the repeated size)
        assertEquals(buffer.byteBufferSizeHeader.getInt(0), buffer.byteBufferData.getInt(offset));
        assertEquals(buffer.byteBufferSizeHeader.getInt(0) - 8, offset);
    }
}
