package fr.ifremer.globe.api.xsf.tests.s7k.datagrams;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramParser;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K7057;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K7057.FloatSamples;

public class Test7057 extends TestDatagram {
	
	@Test
	public void testDecode() throws IOException {
		DatagramBuffer buffer = getDataBuffer(7057);
		
		assertEquals(7057, DatagramParser.getDatagramType(buffer));
		assertEquals(0,S7K7057.getSonarId(buffer).getLong());
		assertEquals(110267, S7K7057.getPingNumber(buffer).getU());
		assertEquals(0, S7K7057.getMultiPingSequence(buffer).getU());
		assertEquals(0.0, S7K7057.getBeamPosition(buffer).get(), 0.0);
		assertEquals(272, S7K7057.getControlFlags(buffer).getU());
		assertEquals(2417, S7K7057.getNumOfSamplesPerSide(buffer).getU());
		assertEquals(1, S7K7057.getNumberOfBeamsPerSide(buffer).getU());
		assertEquals(1, S7K7057.getCurrentBeamNumber(buffer).getU());
		assertEquals(4, S7K7057.getNumberOfBytesPerSample(buffer).getU());
		assertEquals(0, S7K7057.getDataType(buffer).getU());
		assertEquals(0, S7K7057.getErrorFlags(buffer).getU());
		
		FloatSamples samples = S7K7057.getSamples4(buffer);
		assertEquals(2417, samples.portBeams.length);
		assertEquals(2417, samples.starboardBeams.length);
		assertEquals(2417, samples.portBeamNumbers.length);
		assertEquals(2417, samples.starboardsBeamNumbers.length);
		
		assertEquals(3, samples.portBeamNumbers[0].getU());
		assertEquals(86, samples.portBeamNumbers[1000].getU());
		assertEquals(5, samples.portBeamNumbers[2000].getU());
		
		assertEquals(300, samples.starboardsBeamNumbers[0].getU());
		assertEquals(215, samples.starboardsBeamNumbers[1000].getU());
		assertEquals(296, samples.starboardsBeamNumbers[2000].getU());
		
		assertEquals(-8.09, samples.portBeams[0], 0.01);
		assertEquals(-26.49, samples.portBeams[1000], 0.01);
		assertEquals(-6.17, samples.starboardBeams[0], 0.01);
		assertEquals(-34.80, samples.starboardBeams[1000], 0.01);

		assertEquals(29133, S7K7057.getOptionalDataOffset(buffer).getU());
		final int optOffset = (int) S7K7057.getOptionalDataOffset(buffer).getU();
		assertEquals(100000.0, S7K7057.getFrequency(buffer, optOffset).get(), 0.0);
		assertEquals(0.842, S7K7057.getLatitude(buffer, optOffset).get(), 0.001);
		assertEquals(-0.086, S7K7057.getLongitude(buffer, optOffset).get(), 0.001);
		assertEquals(0.031, S7K7057.getHeading(buffer, optOffset).get(), 0.001);
		assertEquals(68.45, S7K7057.getDepth(buffer, optOffset).get(), 0.001);
	}

}
