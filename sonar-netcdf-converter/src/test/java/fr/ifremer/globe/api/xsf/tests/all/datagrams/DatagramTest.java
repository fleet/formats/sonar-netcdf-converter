package fr.ifremer.globe.api.xsf.tests.all.datagrams;

import java.io.IOException;
import java.io.RandomAccessFile;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.tests.all.utils.FileFinder;

public abstract class DatagramTest {

	protected DatagramBuffer getDatagram(byte datagramType) throws IOException {
		final String path = FileFinder.getPath(String.format("/all/datagrams/0x%2x/datagram.all", datagramType));
		final RandomAccessFile raf = new RandomAccessFile(path, "r");

		DatagramBuffer buffer = new DatagramBuffer(DatagramReader.getByteOrder(raf));
		DatagramReader.readDatagram(raf, buffer);
		raf.close();

		return buffer;
	}
}
