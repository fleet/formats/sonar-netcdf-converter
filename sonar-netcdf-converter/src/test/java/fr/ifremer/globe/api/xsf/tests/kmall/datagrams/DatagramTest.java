package fr.ifremer.globe.api.xsf.tests.kmall.datagrams;

import java.io.IOException;
import java.io.RandomAccessFile;

import fr.ifremer.globe.api.xsf.converter.kmall.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.tests.all.utils.FileFinder;

public abstract class DatagramTest {
    protected DatagramBuffer getDatagram(String datagramType) throws IOException {
        final String path = FileFinder.getPath(String.format("/kmall/datagrams/%s/datagram.kmall", datagramType));
        final RandomAccessFile raf = new RandomAccessFile(path, "r");

        DatagramBuffer buffer = new DatagramBuffer();
        DatagramReader.readDatagram(raf, buffer);
        raf.close();

        return buffer;
    }
}
