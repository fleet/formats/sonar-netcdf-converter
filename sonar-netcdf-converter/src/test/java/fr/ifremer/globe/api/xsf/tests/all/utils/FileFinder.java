package fr.ifremer.globe.api.xsf.tests.all.utils;

import fr.ifremer.globe.utils.test.GlobeTestUtil;

/**
 * Helper class used to  locate a file in the class path.
 */
public class FileFinder {
    
    private FileFinder() {}
    
    /**
     * Get the full path to a file usually placed under the src/test/resources directory
     * @param fileName
     * @return
     */
    public static String getPath(String fileName) {
    	return GlobeTestUtil.getTestDataPath() + "/file/xsf/xsfUnitTests/"+ fileName;
       // return get().getClass().getResource(fileName).getFile();
    }

}
