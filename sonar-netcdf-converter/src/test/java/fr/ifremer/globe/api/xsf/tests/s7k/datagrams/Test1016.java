package fr.ifremer.globe.api.xsf.tests.s7k.datagrams;

import static org.junit.Assert.*;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramParser;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K1016;
import fr.ifremer.globe.utils.date.DateUtils;

public class Test1016 extends TestDatagram {
	
	@Test
	public void testDecode() throws IOException {
		DatagramBuffer buffer = getDataBuffer(1016);
		
		assertEquals(1016, DatagramParser.getDatagramType(buffer));
		
		assertEquals(4, S7K1016.getNumberOfAttitudeData(buffer).getU());
		
		final S7K1016.AttitudeData[] attitudeData = S7K1016.getAttitudeDataRecords(buffer);
		assertEquals(4, attitudeData.length);
		assertEquals(0, attitudeData[0].timeDifferenceFromRecord.getU());
		assertEquals(LocalDateTime.of(2012, 4, 30, 12, 45, 44, 646919250).toInstant(ZoneOffset.UTC).toEpochMilli(), DateUtils.nanoSecondToMilli(attitudeData[0].time));
		assertEquals(0.0064, attitudeData[0].roll, 0.0001);
		assertEquals(0.0, attitudeData[0].pitch, 0.01);
		assertEquals(-0.46, attitudeData[0].heave, 0.01);
		assertEquals(0.032, attitudeData[0].heading, 0.01);
		
		assertEquals(63, attitudeData[3].timeDifferenceFromRecord.getU());
	//	assertEquals(LocalDateTime.of(2012, 4, 30, 12, 46, 47, 646919250).toInstant(ZoneOffset.UTC).toEpochMilli(), DateUtils.nanoSecondToMilli(attitudeData[3].time));
		assertEquals(0.0064, attitudeData[0].roll, 0.0001);
		assertEquals(0.0, attitudeData[0].pitch, 0.01);
		assertEquals(-0.46, attitudeData[0].heave, 0.01);
		assertEquals(0.032, attitudeData[0].heading, 0.01);
	}

}
