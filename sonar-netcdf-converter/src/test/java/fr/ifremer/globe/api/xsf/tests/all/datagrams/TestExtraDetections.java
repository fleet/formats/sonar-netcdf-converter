package fr.ifremer.globe.api.xsf.tests.all.datagrams;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.extradetections.ExtraDetections;

public class TestExtraDetections extends DatagramTest {

	@Test
	public void testGetBody() throws IOException {
		DatagramBuffer buffer = getDatagram((byte)0x6C);
		
		assertEquals(ExtraDetections.getSerialNumber(buffer.byteBufferData).getU(), 147);
		assertEquals(ExtraDetections.getDatagramVersionID(buffer.byteBufferData).getU(), 1);
		assertEquals(ExtraDetections.getSwathCounter(buffer.byteBufferData).getU(), 11771);
		assertEquals(ExtraDetections.getSwathIndex(buffer.byteBufferData).getU(), 1);
		assertEquals(ExtraDetections.getHeading(buffer.byteBufferData).getU(), 16989);
		assertEquals(ExtraDetections.getSoundVelocityAtTx(buffer.byteBufferData).getU(), 14760);
		assertEquals(ExtraDetections.getDepthOfReferencePoint(buffer.byteBufferData).get(), 2.313415, 0.0001);
		assertEquals(ExtraDetections.getWaterColumnSampleRate(buffer.byteBufferData).get(), 56561.085938,  0.0001);
		assertEquals(ExtraDetections.getRawAmplitudeSampleRate(buffer.byteBufferData).get(), 56561.085938,  0.0001);
		assertEquals(ExtraDetections.getRXTransducerIndex(buffer.byteBufferData).getU(), 1);
		int nD = ExtraDetections.getExtraDetectionsCounter(buffer.byteBufferData).getU();
		assertEquals(nD, 117);
		int nC = ExtraDetections.getDetectionClassesCounter(buffer.byteBufferData).getU();
		assertEquals(nC, 8);
		assertEquals(ExtraDetections.getBytePerClassCounter(buffer.byteBufferData).getU(), 16);
		assertEquals(ExtraDetections.getAlarmFlagsCounter(buffer.byteBufferData).getU(), 2);
		assertEquals(ExtraDetections.getBytesPerDetectionCounter(buffer.byteBufferData).getU(), 68);
		
		// Check detection classes
		int iNc = 0;
		assertEquals(ExtraDetections.getStartDepth(buffer.byteBufferData, iNc).getU(), 5);
		assertEquals(ExtraDetections.getStopDepth(buffer.byteBufferData, iNc).getU(), 10);
		assertEquals(ExtraDetections.getQFThreshold(buffer.byteBufferData, iNc).getU(), 100);
		assertEquals(ExtraDetections.getBSThreshold(buffer.byteBufferData, iNc).get(), -50);
		assertEquals(ExtraDetections.getSNRThreshold(buffer.byteBufferData, iNc).getU(), 12);
		assertEquals(ExtraDetections.getAlarmThreshold(buffer.byteBufferData, iNc).getU(), 5);
		assertEquals(ExtraDetections.getExtraDetectionsIndex(buffer.byteBufferData, iNc).getU(), 0);
		assertEquals(ExtraDetections.getShowClass(buffer.byteBufferData, iNc).getU(), 1);
		assertEquals(ExtraDetections.getAlarmFlag1(buffer.byteBufferData, iNc).getU(), 0);
		
	    // Check Last Detection class
		iNc = nC-1;
		assertEquals(ExtraDetections.getStartDepth(buffer.byteBufferData, iNc).getU(), 1);
		assertEquals(ExtraDetections.getStopDepth(buffer.byteBufferData, iNc).getU(), 300);
		assertEquals(ExtraDetections.getQFThreshold(buffer.byteBufferData, iNc).getU(), 0);
		assertEquals(ExtraDetections.getBSThreshold(buffer.byteBufferData, iNc).get(), 0);
		assertEquals(ExtraDetections.getSNRThreshold(buffer.byteBufferData, iNc).getU(), 0);
		assertEquals(ExtraDetections.getAlarmThreshold(buffer.byteBufferData, iNc).getU(), 9999);
		assertEquals(ExtraDetections.getExtraDetectionsIndex(buffer.byteBufferData, iNc).getU(), 76);
		assertEquals(ExtraDetections.getShowClass(buffer.byteBufferData, iNc).getU(), 0);
		assertEquals(ExtraDetections.getAlarmFlag1(buffer.byteBufferData, iNc).getU(), 0);

		// Check extra detection
		int iNdIndex = 0;
		// 68 : erreur dans la documentation av.
		int offset = 52 + nC*16 + iNdIndex * 68;
		assertEquals(ExtraDetections.getDepth(buffer.byteBufferData, offset).get(), 11.588612, 0000.1);
		assertEquals(ExtraDetections.getAcross(buffer.byteBufferData, offset).get(), 1.904164, 0000.1);
		assertEquals(ExtraDetections.getAlong(buffer.byteBufferData, offset).get(), 8.307334, 0000.1);
		assertEquals(ExtraDetections.getDeltaLatitude(buffer.byteBufferData, offset).get(), -0.000077, 0000.1);
		assertEquals(ExtraDetections.getDeltaLongitude(buffer.byteBufferData, offset).get(), -0.000004, 0000.1);
		assertEquals(ExtraDetections.getBeamPointingAngle(buffer.byteBufferData, offset).get(), 32.880001, 0000.1);
		assertEquals(ExtraDetections.getAppliedPointingAngleCorrection(buffer.byteBufferData, offset).get(), 0.0000, 0000.1);
		assertEquals(ExtraDetections.getTwoTimeTravelTime(buffer.byteBufferData, offset).get(), 0.015766, 0.0001);
		assertEquals(ExtraDetections.getAppliedTwoWayTravelTimeCorrections(buffer.byteBufferData, offset).get(), 0.000, 0.0001);
		assertEquals(ExtraDetections.getBS(buffer.byteBufferData, offset).get(), -401);
		assertEquals(ExtraDetections.getBeamIncidenceAngleAdjustment(buffer.byteBufferData, offset).get(), 0);
		assertEquals(ExtraDetections.getDetectionInfo(buffer.byteBufferData, offset).getU(), 0);
		// assertEquals(ExtraDetections.getSpare2(buffer.byteBufferData, offset).get(), 0);
		assertEquals(ExtraDetections.getTXSectorIndex(buffer.byteBufferData, offset).getU(), 0);
		assertEquals(ExtraDetections.getQualityFactor(buffer.byteBufferData, offset).getU(), 46);
		assertEquals(ExtraDetections.getDetectionWindowLength(buffer.byteBufferData, offset).getU(), 37);
		assertEquals(ExtraDetections.getRealTimeCleaningInfo(buffer.byteBufferData, offset).getU(), 0);
		assertEquals(ExtraDetections.getRangeFactor(buffer.byteBufferData, offset).getU(), 101);
		assertEquals(ExtraDetections.getDetectionClassIndex(buffer.byteBufferData, offset).getU(), 5);
		assertEquals(ExtraDetections.getConfidenceLevel(buffer.byteBufferData, offset).getU(), 25);
		assertEquals(ExtraDetections.getQFIfr(buffer.byteBufferData, offset).getU(), 53);
		assertEquals(ExtraDetections.getWaterColumnBeamIndex(buffer.byteBufferData, offset).getU(),20);
		assertEquals(ExtraDetections.getBeamAngleAcrossReVertical(buffer.byteBufferData, offset).get(), -6.631663, 0.0001);
		assertEquals(ExtraDetections.getDetectedRange(buffer.byteBufferData, offset).getU(), 891);
		assertEquals(ExtraDetections.getRawAmplitudeSamplesCounter(buffer.byteBufferData, offset).getU(), 4);
		
		// Check last extra detection
		iNdIndex = nD-1;
		offset = 68 + (nC-1) * 16 + iNdIndex * 68;
		assertEquals(ExtraDetections.getDepth(buffer.byteBufferData, offset).get(), 11.883780, 0000.1);
		assertEquals(ExtraDetections.getAcross(buffer.byteBufferData, offset).get(), 2.274180, 0000.1);
		assertEquals(ExtraDetections.getAlong(buffer.byteBufferData, offset).get(), 8.307334, 0000.1);
		assertEquals(ExtraDetections.getDeltaLatitude(buffer.byteBufferData, offset).get(), -0.000077, 0000.1);
		assertEquals(ExtraDetections.getDeltaLongitude(buffer.byteBufferData, offset).get(), -0.000004, 0000.1);
		assertEquals(ExtraDetections.getBeamPointingAngle(buffer.byteBufferData, offset).get(), 31.290001, 0000.1);
		assertEquals(ExtraDetections.getAppliedPointingAngleCorrection(buffer.byteBufferData, offset).get(), 0.0000, 0000.1);
		assertEquals(ExtraDetections.getTwoTimeTravelTime(buffer.byteBufferData, offset).get(), 0.016226, 0.0001);
		assertEquals(ExtraDetections.getAppliedTwoWayTravelTimeCorrections(buffer.byteBufferData, offset).get(), 0.000, 0.0001);
		assertEquals(ExtraDetections.getBS(buffer.byteBufferData, offset).get(), -381);
		assertEquals(ExtraDetections.getBeamIncidenceAngleAdjustment(buffer.byteBufferData, offset).get(), 0);
		assertEquals(ExtraDetections.getDetectionInfo(buffer.byteBufferData, offset).getU(), 0);
		// assertEquals(ExtraDetections.getSpare2(buffer.byteBufferData, offset).get(), 0);
		assertEquals(ExtraDetections.getTXSectorIndex(buffer.byteBufferData, offset).getU(), 0);
		assertEquals(ExtraDetections.getQualityFactor(buffer.byteBufferData, offset).getU(), 75);
		assertEquals(ExtraDetections.getDetectionWindowLength(buffer.byteBufferData, offset).getU(), 38);
		assertEquals(ExtraDetections.getRealTimeCleaningInfo(buffer.byteBufferData, offset).getU(), 0);
		assertEquals(ExtraDetections.getRangeFactor(buffer.byteBufferData, offset).getU(), 110);
		assertEquals(ExtraDetections.getDetectionClassIndex(buffer.byteBufferData, offset).getU(), 6);
		assertEquals(ExtraDetections.getConfidenceLevel(buffer.byteBufferData, offset).getU(), 17);
		assertEquals(ExtraDetections.getQFIfr(buffer.byteBufferData, offset).getU(), 66);
		assertEquals(ExtraDetections.getWaterColumnBeamIndex(buffer.byteBufferData, offset).getU(),27);
		assertEquals(ExtraDetections.getBeamAngleAcrossReVertical(buffer.byteBufferData, offset).get(), -8.221359, 0.0001);
		assertEquals(ExtraDetections.getDetectedRange(buffer.byteBufferData, offset).getU(), 917);
		assertEquals(ExtraDetections.getRawAmplitudeSamplesCounter(buffer.byteBufferData, offset).getU(), 4);

		// Check 1st Samples
		offset = 52 + nC*16 +  nD*68;
		assertEquals(ExtraDetections.getRawAmplitudeSample(buffer.byteBufferData, offset).get(), -500);

		// Check 2nd Sample
		offset = 52 + nC*16 +  nD*68 + 2;
		assertEquals(ExtraDetections.getRawAmplitudeSample(buffer.byteBufferData, offset).get(), -513);
		offset = 52 + nC*16 +  nD*68 + 4;
		assertEquals(ExtraDetections.getRawAmplitudeSample(buffer.byteBufferData, offset).get(), -436);
		offset = 52 + nC*16 +  nD*68 + 6;
		assertEquals(ExtraDetections.getRawAmplitudeSample(buffer.byteBufferData, offset).get(), -483);
		offset = 52 + nC*16 +  nD*68 + 8;
		assertEquals(ExtraDetections.getRawAmplitudeSample(buffer.byteBufferData, offset).get(), -420);
		offset = 52 + nC*16 +  nD*68 + 10;
		assertEquals(ExtraDetections.getRawAmplitudeSample(buffer.byteBufferData, offset).get(), -389);
		offset = 52 + nC*16 +  nD*68 + 12;
		assertEquals(ExtraDetections.getRawAmplitudeSample(buffer.byteBufferData, offset).get(), -350);
		offset = 52 + nC*16 +  nD*68 + 14;
		assertEquals(ExtraDetections.getRawAmplitudeSample(buffer.byteBufferData, offset).get(), -341);
		offset = 52 + nC*16 +  nD*68 + 16;
		assertEquals(ExtraDetections.getRawAmplitudeSample(buffer.byteBufferData, offset).get(), -357);

		int nS = 4;
		int iS = 2;
		offset = 52 + nC*16 +  nD*68 + (iS-1)*((2*nS+ 1)*2);
		assertEquals(ExtraDetections.getRawAmplitudeSample(buffer.byteBufferData, offset).get(), -309);

		iS = 3;
		offset = 52 + nC*16 +  nD*68 + (iS-1)*((2*nS+ 1)*2);
		assertEquals(ExtraDetections.getRawAmplitudeSample(buffer.byteBufferData, offset).get(), -562);

		iS = 117;
		// 1er échantillon du dernier bloc
		offset = 52 + nC*16 +  nD*68 + (iS-1)*((2*nS+ 1)*2);
		assertEquals(ExtraDetections.getRawAmplitudeSample(buffer.byteBufferData, offset).get(), -346);
		// dernier échantillon du dernier bloc
		offset = 52 + nC*16 +  nD*68 + (iS-1)*((2*nS+ 1)*2) + 16;
		assertEquals(ExtraDetections.getRawAmplitudeSample(buffer.byteBufferData, offset).get(), -368);


	}
}
