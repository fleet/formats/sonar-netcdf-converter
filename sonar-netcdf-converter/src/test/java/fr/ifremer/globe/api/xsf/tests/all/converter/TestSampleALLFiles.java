package fr.ifremer.globe.api.xsf.tests.all.converter;

import static org.junit.Assert.assertTrue;

import java.io.File;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import fr.ifremer.globe.api.xsf.converter.all.KongsbergAllConverter;
import fr.ifremer.globe.api.xsf.tests.all.utils.FileFinder;
import fr.ifremer.globe.api.xsf.util.tools.XSFChecker;

public class TestSampleALLFiles {
	@Rule
	public TemporaryFolder tmpFolder = TemporaryFolder.builder().assureDeletion().build();
	
    @Test
    public void test_0095_20141031_133613_Thalia_small() throws Exception {
		final String outFileName = new File(tmpFolder.getRoot(), "0095_20141031_133613_Thalia_small.xsf.nc")
				.getAbsolutePath();
        new KongsbergAllConverter().convert(FileFinder.getPath("/all/0095_20141031_133613_Thalia_small.all"), outFileName);
        
        assertTrue(new XSFChecker().check(outFileName));
    }
 
    @Test
    public void test_0059_20130203_134006_Thalia_small() throws Exception {
		final String outFileName = new File(tmpFolder.getRoot(), "0059_20130203_134006_Thalia_small.xsf.nc")
				.getAbsolutePath();
        new KongsbergAllConverter().convert(FileFinder.getPath("/all/0059_20130203_134006_Thalia_small.all"), outFileName);
       
        assertTrue(new XSFChecker().check(outFileName));
    }

    // give random invalid memory access
/**
    @Test
    public void test_0004_20030314_103209_raw_small() throws Exception {
        final String outFileName = tmp.newFile("0004_20030314_103209_raw_small.hsf").getAbsolutePath();
        KongsbergAllConverter.convert(FileFinder.getPath("/all/0004_20030314_103209_raw_small.all"), outFileName);
        
        assertTrue(new XSFChecker().check(outFileName));
    }
 */
    
    @Test
    public void test_0008_20150611_095724_SE_ext_det_small() throws Exception {
		final String outFileName = new File(tmpFolder.getRoot(), "0008_20150611_095724_SE_ext_det_small.xsf.nc")
				.getAbsolutePath();
        new KongsbergAllConverter().convert(FileFinder.getPath("/all/0008_20150611_095724_SE_ext_det_small.all"), outFileName);
        
        assertTrue(new XSFChecker().check(outFileName));
    }
    
    // give random invalid memory access
//    @Test
//    public void test_0009_20150203_210705_Belgica_small() throws Exception {
//        final String outFileName = tmp.newFile("0009_20150203_210705_Belgica_small.hsf").getAbsolutePath();
//        KongsbergAllConverter.convert(FileFinder.getPath("/all/0009_20150203_210705_Belgica_small.all"), outFileName);
//        
//        assertTrue(new XSFChecker().check(outFileName));
//    }
    
    @Test
    public void test_0014_20121105_103947_SimradEcho_small() throws Exception {
		final String outFileName = new File(tmpFolder.getRoot(), "0014_20121105_103947_SimradEcho_small.xsf.nc")
				.getAbsolutePath();
        new KongsbergAllConverter().convert(FileFinder.getPath("/all/0014_20121105_103947_SimradEcho_small.all"), outFileName);

        assertTrue(new XSFChecker().check(outFileName));
    }
}
