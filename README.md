# SonarNetcdf converter

## How to build an embeddable jar in GWS

- Open a terminal in the root folder of the project
- Use Gradle to assemble the project : gradlew.bat shadowJar
- The resulting executable jar is in build\libs
- Rename sonarnetcdf-converter-[VERSION]-all.jar to sonarnetcdf-converter-[VERSION]-all.jar and 
move it to GWS source folder (GWS\src\main\resources\java-services\xsf-converter)

## How to build the converter as an exe

- Open a terminal in the root folder of the project
- Use Gradle to assemble the project : gradlew.bat jpackage
- The resulting executable application is in build\jpackage\sonarnetcdf-converter
- Use sonarnetcdf-converter.exe to run the application

## How to run the tests

- Open a terminal in the root folder of the project
- Use Gradle to test the project : gradlew.bat test
- To ignore previous execution and force relaunching all tests, use :  gradlew.bat test --rerun-tasks. 


